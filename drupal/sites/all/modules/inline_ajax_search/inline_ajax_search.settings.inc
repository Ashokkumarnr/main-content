<?php
// $Id$

/**
 * The actual settings form.
 */
function inline_ajax_search_settings() {
  $form['inline_ajax_search_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of results shown'),
    '#default_value' => variable_get('inline_ajax_search_count', 10),
    '#size' => 3,
    '#maxlength' => 3,
    '#element_validate' => array('inline_ajax_form_validate'),
  );
  $form['inline_ajax_search_snippet'] = array(
    '#type' => 'select',
    '#title' => t('Show/hide snippet or teaser'),
    '#default_value' => variable_get('inline_ajax_search_snippet', 0),
    '#options' => array(0 => t('Show none'), 1 => t('Show snippet'), 2 => t('Show teaser')),
  );
  $form['inline_ajax_search_target'] = array(
    '#type' => 'select',
    '#title' => t('Target of the links'),
    '#default_value' => variable_get('inline_ajax_search_target', '_self'),
    '#options' => array('_self' => t('Self'), '_blank' => t('Blank'), '_top' => t('Top'), '_parent' => t('Parent')),
  );
  $form['inline_ajax_show_more'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show the show more results link'),
      '#default_value' => variable_get('inline_ajax_show_more', 0),
  );
  $form['inline_ajax_lucene'] = array(
      '#type' => 'fieldset',
      '#title' => t('Lucene API support'),
      '#collapsible' => TRUE,
    );
  if (module_exists('luceneapi') && module_exists('luceneapi_node')) {
    $form['inline_ajax_lucene']['inline_ajax_use_lucene'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the Search Lucene API'),
      '#default_value' => variable_get('inline_ajax_use_lucene', 0),
    );
    $form['inline_ajax_lucene']['inline_ajax_use_lucene_on_enter'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the core search when hitting return.'),
      '#default_value' => variable_get('inline_ajax_use_lucene_on_enter', 0),
    );
    $form['inline_ajax_lucene']['inline_ajax_wildcard'] = array(
      '#type' => 'select',
      '#title' => t('Location of the wildcard'),
      '#default_value' => variable_get('inline_ajax_wildcard', 0),
      '#options' => array(0 => t('End of the word'), 1 => t('Start of the word'), 2 => t('On both start and end')),
    );
  }
    else {
      $form['inline_ajax_lucene']['inline_ajax_use_lucene'] = array(
        '#type' => 'item',
        '#title' => 'Search Lucene API',
        '#value' => t('If you install the Search Lucene API this module uses wildcards to search with. This has as benefit that you don\'t have to search on a whole word, but on a part of a string.'),
      );
    }
  if (module_exists('views')) {
    $form['inline_ajax_views'] = array(
      '#type' => 'fieldset',
      '#title' => t('Views support'),
      '#description' => t('Enabling this feature means that if the Lucene API is enabled it won\'t be used aymore'),
      '#collapsible' => TRUE,
    );
    $form['inline_ajax_views']['inline_ajax_use_views'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use views for the search results'),
      '#default_value' => variable_get('inline_ajax_use_views', 0),
    );
    $opts = array(
      0 => t('Don\'t us a view'),
    );
    $views = views_get_all_views();
    foreach ($views as $view) {
      if (empty($view->disabled)) {
        $opts[$view->name] = $view->description;
      }
    }
    $form['inline_ajax_views']['inline_ajax_use_views_name'] = array(
      '#type' => 'select',
      '#title' => t('The view to use'),
      '#default_value' => variable_get('inline_ajax_use_views_name', 0),
      '#options' => $opts,
    );
  }
  $form = system_settings_form($form);
  return $form;
}

function inline_ajax_form_validate($element, &$form_state) {
  if (!is_numeric($element['#value'])) {
    form_error($element, t('This has to be a number.'));
  }
}