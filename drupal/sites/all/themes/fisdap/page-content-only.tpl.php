<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
  <head>
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body>
      <?php include('google_analytics.php'); ?>
      <!-- add CSS in #content to fix flash video weird margin -->
      <div id="content"><div id="content-inner">
  
          <div id="content-area">
            <?php print $content; ?>
          </div>
  
      </div></div> <!-- /#content-inner, /#content -->
      
    <?php if ($footer): ?>
      <div id="footer"><div id="footer-inner" class="region region-footer">
        <?php print $footer; ?>
      </div></div> <!-- /#footer-inner, /#footer -->
    <?php endif; ?>
  
    <?php print $closure; ?>
  
  </body>
</html>
