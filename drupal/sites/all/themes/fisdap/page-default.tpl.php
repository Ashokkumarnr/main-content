<?php
// $Id: page.tpl.php,v 1.14.2.10 2009/11/05 14:26:26 johnalbin Exp $

/**
 * @file page.tpl.php
 *
 * Theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the theme is located in, e.g. themes/garland or
 *   themes/garland/minelli.
 * - $is_front: TRUE if the current page is the front page. Used to toggle the mission statement.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $body_classes: A set of CSS classes for the BODY tag. This contains flags
 *   indicating the current layout (multiple columns, single column), the current
 *   path, whether the user is logged in, and so on.
 * - $body_classes_array: An array of the body classes. This is easier to
 *   manipulate then the string in $body_classes.
 * - $node: Full node object. Contains data that may not be safe. This is only
 *   available if the current page is on the node's primary url.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been disabled.
 * - $primary_links (array): An array containing primary navigation links for the
 *   site, if they have been configured.
 * - $secondary_links (array): An array containing secondary navigation links for
 *   the site, if they have been configured.
 *
 * Page content (in order of occurrance in the default page.tpl.php):
 * - $left: The HTML for the left sidebar.
 *
 * - $breadcrumb: The breadcrumb trail for the current page.
 * - $title: The page title, for use in the actual HTML content.
 * - $help: Dynamic help text, mostly for admin pages.
 * - $messages: HTML for status and error messages. Should be displayed prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the view
 *   and edit tabs when displaying a node).
 *
 * - $content: The main content of the current Drupal page.
 *
 * - $right: The HTML for the right sidebar.
 *
 * Footer/closing data:
 * - $feed_icons: A string of all feed icons for the current page.
 * - $footer_message: The footer message as defined in the admin settings.
 * - $footer : The footer region.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic content.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 */
?>

<?php

// Get Fisdap members URL root fusing menu_fisdap_getFisdapMembersUrlRoot() 
#$fisdap_members_url_root = menu_fisdap_getFisdapMembersUrlRoot();

// get node's first term
$node_first_term = array_shift(taxonomy_node_get_terms($node));

// Hack page title and breadcrumb as needed
if ($node->type == 'announcement') {
    $title = 'Announcements';
} elseif ($title == 'Jobs' || $node->type == 'job') {
    $title = 'About FISDAP';
} elseif ($title == 'Glossary') {
    $bc = array();
    if (preg_match("/resources/",request_uri())) {
        $title = 'Resources';

	$bc[] = l('Home', '<front>');
	$bc[] = l('Resources', 'resources/about');
    } elseif (preg_match("/help/",request_uri())) {
        $title = 'Help';

        $bc[] = l('Home', NULL);
        $bc[] = l('Help', 'help');
    }
    $bc[] = t(drupal_get_title());
} elseif ($title == 'Frequently Asked Questions') {
    // replace Home crumb with links to Home and Help
    $bc = drupal_get_breadcrumb();
    $crumbs[] = l('Home', NULL);
    $crumbs[] = l('Help', 'help');
    array_splice($bc, 0, 1, $crumbs);
} elseif ($node->type == 'book') {
    $title = array_shift(book_toc($node->book['bid'], array(), 1)); // use book title
    
    // replace Home crumb with links to Home and Help
    $bc = drupal_get_breadcrumb();
    $crumbs[] = l('Home', NULL);
    $crumbs[] = l('Help', 'help');
    array_splice($bc, 0, 1, $crumbs);
} elseif (preg_match("/Open Airways/", $title)) {
    // remove title in favor of header image  
    unset($title);
     /*
    // replace Home crumb with links to Home and Podcasts
    $bc = drupal_get_breadcrumb();
    $crumbs[] = l('Home', NULL);
    $crumbs[] = t('Podcasts');
    $crumbs[] = l('Open Airways', 'podcasts/open_airways');
    array_splice($bc, 0, 1, $crumbs);
    */
} elseif ($node_first_term->name == 'webinar') {
	$title = 'Webinars';
	
	$bc[] = l('Home', '<front>');
	$bc[] = l('Resources', 'resources/about');
    $bc[] = l('Presentations', 'resources/presentations');
	$bc[] = l('Webinars', 'resources/presentations/webinars');
    $bc[] = t(drupal_get_title());
} elseif (!empty($node_first_term)) {
    // if a node other than a podcast has been tagged, use the first term as title (for 'site section' taxonomy)
    if ($node->type != 'podcast') {
		$title = $node_first_term->name;
    }
}

// set breadcrumb
drupal_set_breadcrumb($bc);
if ($bc) {
    $breadcrumb = '<div class="breadcrumb">' . implode (' › ', $bc) . '</div>';
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
    <head>
	<title><?php print $head_title; ?></title>
	<?php print $head; ?>
	<?php print $styles; ?>
	<?php print $scripts; ?>
	<script src="<?php print $fisdap_members_url_root; ?>phputil/info_menu_functions.js" type="text/javascript"></script>
    </head>
    <body>
	
	<div id="FISDAPsideNav">
	    <?php
            // Include the menu html
            #print file_get_contents($fisdap_members_url_root . 'info/infoMenu.html');
	    ?>

	    <?php if ($left): ?>
	    <div id="sidebar-left"><div id="sidebar-left-inner" class="region region-left">
		<?php print $left; ?>
	    </div></div> <!-- /#sidebar-left-inner, /#sidebar-left -->
	    <?php endif; ?>

	</div>

	<div id="main"><div id="main-inner" class="clear-block<?php if ($search_box || $primary_links || $secondary_links || $navbar) { print ' with-navbar'; } ?>">
	    <div id="content"><div id="content-inner">

		<?php if ($header): ?>
		<div id="header-blocks" class="region region-header">
		    <?php print $header; ?>
		</div> <!-- /#header-blocks -->
		<?php endif; ?>

		<?php if ($breadcrumb || $title || $tabs || $help || $messages): ?>
		<div id="content-header">
		    <?php if ($title): ?>
			<h1 class="title">
			    <?php
				if ($node->type == 'podcast') {
				    print "<em>$title</em>";
				    if ($node->field_podcast_speaker[0]['safe']) {
					print ' by ' . $node->field_podcast_speaker[0]['safe'];
				    }
				} else {
				    print $title;
				}
			    ?>
			</h1>
		    <?php endif; ?>
		    <?php print $breadcrumb; ?>
		    <?php # title was here # ?>
		    <?php print $messages; ?>
		    <?php if ($tabs): ?>
		    <div class="tabs"><?php print $tabs; ?></div>
		    <?php endif; ?>
		    <?php print $help; ?>
		</div> <!-- /#content-header -->
		<?php endif; ?>

		<?php if ($content_top): ?>
		<div id="content-top" class="region region-content_top">
		    <?php print $content_top; ?>
		</div> <!-- /#content-top -->
		<?php endif; ?>

		<div id="content-area">
		    <?php print $content; ?>
		</div>

		<?php if ($feed_icons): ?>
		    <div class="feed-icons"><?php print $feed_icons; ?></div>
		<?php endif; ?>

		<?php if ($content_bottom): ?>
		<div id="content-bottom" class="region region-content_bottom">
		    <?php print $content_bottom; ?>
		</div> <!-- /#content-bottom -->
		<?php endif; ?>

		<?php if ($footer || $footer_message): ?>
		<div id="footer"><div id="footer-inner" class="region region-footer">
		<?php if ($footer_message): ?>
		    <div id="footer-message"><?php print $footer_message; ?></div>
		<?php endif; ?>
		    <?php print $footer; ?>
		</div></div> <!-- /#footer-inner, /#footer -->
		<?php endif; ?>

	    </div></div> <!-- /#content-inner, /#content -->

	    <?php if ($right): ?>
	    <div id="sidebar-right"><div id="sidebar-right-inner" class="region region-right">
		<?php print $right; ?>
	    </div></div> <!-- /#sidebar-right-inner, /#sidebar-right -->
	    <?php endif; ?>

	</div></div> <!-- /#main-inner, /#main -->

    <?php print $closure; ?>

    </body>
</html>
