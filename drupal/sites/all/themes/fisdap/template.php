<?php
// $Id: template.php,v 1.17.2.1 2009/02/13 06:47:44 johnalbin Exp $

/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can add new regions for block content, modify
 *   or override Drupal's theme functions, intercept or make additional
 *   variables available to your theme, and create custom PHP logic. For more
 *   information, please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to fisdap_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: fisdap_breadcrumb()
 *
 *   where fisdap is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override any of the theme functions used in Zen core,
 *   you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_item_link()   in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */


/*
 * Add any conditional stylesheets you will need for this sub-theme.
 *
 * To add stylesheets that ALWAYS need to be included, you should add them to
 * your .info file instead. Only use this section if you are including
 * stylesheets based on certain conditions.
 */
/* -- Delete this line if you want to use and modify this code
// Example: optionally add a fixed width CSS file.
if (theme_get_setting('fisdap_fixed')) {
  drupal_add_css(path_to_theme() . '/layout-fixed.css', 'theme', 'all');
}
// */


/**
 * Implementation of HOOK_theme().
 */
function fisdap_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:
  /*
  $hooks['hook_name_here'] = array( // Details go here );
  */
  // @TODO: Needs detailed comments. Patches welcome!
  return $hooks;
}

/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function fisdap_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function fisdap_preprocess_page(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

function fisdap_preprocess_page(&$vars, $hook) {
  if (isset($vars['node'])) {
   // If the node type is "blog" the template suggestion will be "page-blog.tpl.php".
   $vars['template_files'][] = 'page-'. str_replace('_', '-', $vars['node']->type);
  }
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function fisdap_preprocess_node(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function fisdap_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function fisdap_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/*
 Override the plus1 module voting widget
*/
function fisdap_plus1_widget($node, $score = 0, $logged_in = FALSE, $is_author = FALSE, $voted = FALSE, $teaser = FALSE, $page = TRUE) {
  static $javascript_added = FALSE;

  // Defining CSS hooks to be used in the JavaScript.
  $widget_class = check_plain(variable_get('plus1_widget_class', 'plus1-widget'));
  $link_class = check_plain(variable_get('plus1_link_class', 'plus1-link'));
  $message_class = check_plain(variable_get('plus1_msg_class', 'plus1-msg'));
  $score_class = check_plain(variable_get('plus1_score_class', 'plus1-score'));
  $vote_class = 'plus1-vote-class';  // Is this used any more?

  if (!$javascript_added) {
    // Load the JavaScript and CSS files.
    // You are free to load your own JavaScript files in your theming function to override.
    drupal_add_js(drupal_get_path('module', 'plus1') . '/jquery.plus1.js');
    drupal_add_css(drupal_get_path('module', 'plus1') . '/plus1.css');

    // Attaching these hooks names to the Drupal.settings.plus1 JavaScript object.
    // So these class names are NOT hard-coded in the JavaScript.
    drupal_add_js(array('plus1' => array('widget_class' => $widget_class, 'vote_class' => $vote_class, 'message_class' => $message_class, 'score_class' => $score_class)), 'setting');

    $javascript_added = TRUE;
  }

  // And now we output the HTML.
  $output_content = '';
  if (!$logged_in && !user_access('vote on content')) {
    if ($login_text = variable_get('plus1_login_to_vote', 'Log in to vote')) {
      $output_content .= l(t($login_text), 'user', array('html' => TRUE, 'query' => drupal_get_destination()));
     }
  }
  elseif ($voted) { // User already voted.
    if ($voted_text = variable_get('plus1_you_voted', 'You voted')) {
      $output_content .= check_plain(t(filter_xss_admin($voted_text)));
    }
  }
  elseif (user_access('vote on content')) {
    // Is this the user's own content and do we allow them to vote on it?
    if (!$is_author || ($is_author && variable_get('plus1_vote_on_own', 1))) {
   
      $vote_text = variable_get('plus1_vote', 'Vote');
      if ($vote_text) {
        // User is eligible to vote.
        // The class name provided by Drupal.settings.plus1.vote_class what
        // we will search for in our jQuery later.
        $output_content .= '<form class="' . $vote_class . '" action="'
          . url("plus1/vote/$node->nid",
            array('query' => 'token=' . drupal_get_token($node->nid) . '&' . drupal_get_destination()))
          . '" method="post"><div>';
		
		// make submit button "awesome"
        $output_content .= '<button type="submit" class="awesome small blue"><span>' . t(filter_xss_admin($vote_text)) . '</span></button>';

        $output_content .= '</div></form>';
      }
     
    }
    else if($is_author && !variable_get('plus1_vote_on_own', 1)) {
        $author_text = variable_get('plus1_author_text', 'Your content');
        if ($author_text) {
          $output_content .= '<div class="plus1-author-text">' . t(filter_xss_admin($author_text)) . '</div>';
        }
    }
  }

  if ($output_content) {
    $output_content = '<div class="' . $message_class . '">' . $output_content . '</div>';
  }

  $output = '<div class="' . $widget_class . '">';
  $output .= '<div class="' . $score_class . '">' . $score . '</div>';
  $output .= $output_content;
  $output .= '</div>';
  return $output;
}


/* ROUNDED CORNERS */

$commands = array();

$commands[] = array('selector' => '.pcrf-podcast-sponsors .content',
					'corners' => 'top',
					'width' => 10);

$commands[] = array('selector' => '#sidebar-left .block .content',
					'corners' => 'bottom',
					'width' => 10);

$commands[] = array('selector' => '#sidebar-left .block h2.title',
					'corners' => 'top',
					'width' => 10);

$commands[] = array('selector' => '.open-airways-sponsors .content',
					'corners' => 'top',
					'width' => 10);

$commands[] = array('selector' => '#sidebar-right .block .content',
					'corners' => 'bottom',
					'width' => 10);

$commands[] = array('selector' => '#sidebar-right .block h2.title',
					'corners' => 'top',
					'width' => 10);

$commands[] = array('selector' => '#block-block-16 .content',
					'width' => 10);

$commands[] = array('selector' => '#block-block-17 .content',
					'width' => 10);

$commands[] = array('selector' => '#block-block-18 .content',
					'width' => 10);

$commands[] = array('selector' => '#inline_ajax_search_results',
					'width' => 10);

$commands[] = array('selector' => '.testimonial',
					'width' => 10);

$commands[] = array('selector' => '.upgrade-box',
					'width' => 5);

$commands[] = array('selector' => '.menutab',
					'corners' => 'top',
					'width' => 8);

$commands[] = array('selector' => '.fisdap-button',
					'width' => 5);

$commands[] = array('selector' => '.landing-button',
					'width' => 5);

$commands[] = array('selector' => '#landing2-sidebar-right .block .content',
					'corners' => 'bottom',
					'width' => 10);

$commands[] = array('selector' => '#landing2-sidebar-right .block h2.title',
					'corners' => 'top',
					'width' => 10);

$commands[] = array('selector' => '.landing-block-blue h2.title',
					'corners' => 'top',
					'width' => 10);


rounded_corners_add_corners($commands);


jquery_ui_add('ui.accordion');
