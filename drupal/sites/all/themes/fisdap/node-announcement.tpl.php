<?php
// $Id: node.tpl.php,v 1.4.2.1 2009/05/12 18:41:54 johnalbin Exp $

/**
 * @file node.tpl.php
 *
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $picture: The authors picture of the node output from
 *   theme_user_picture().
 * - $date: Formatted creation date (use $created to reformat with
 *   format_date()).
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $name: Themed username of node author output from theme_user().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $submitted: themed submission information output from
 *   theme_node_submitted().
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $teaser: Flag for the teaser state.
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */
 
/* Get current path for later analysis
   ...based on drupal_get_destination(), but without urlencode()
*/
if (isset($_REQUEST['destination'])) {
	$current_path = $_REQUEST['destination'];
} else {
	// Use $_GET here to retrieve the original path in source form.
	$current_path = isset($_GET['q']) ? $_GET['q'] : '';
	$query = drupal_query_string_encode($_GET, array('q'));
	if ($query != '') {
		$current_path .= '?'. $query;
    }
}

if (!empty($_SERVER['HTTPS'])) {
	  $root = 'https://';
	} else {
	  $root = 'http://';  
	}

	// reverse domain part order to work backwards from the TLD
	$domain_parts = array_reverse(explode('.', $_SERVER['SERVER_NAME']));
	
	$host_info = array();
	
	$host_info['tld'] = $domain_parts[0];
	$host_info['domain'] = $domain_parts[1];
	
	$fqdn = "content.{$host_info['domain']}.{$host_info['tld']}";

	$fisdap_content_url_root = $root . $fqdn;

?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"><div class="node-inner">

	<?php if ($current_path != 'news/brief'): ?>
	<h2 class="title"><a name="<?php print $id; ?>"><?php print $title; ?></a></h2>
	<?php else: ?> 
	   	<strong style="font-size:18px;"><a href="<?php print "$fisdap_content_url_root/news#$id"; ?>" title="<?php print $title ?>" style="text-decoration:none;"><?php print $title; ?></a></strong> 
	<?php endif; ?>

  <?php if ($unpublished): ?>
    <div class="unpublished"><?php print t('Unpublished'); ?></div>
  <?php endif; ?>
  
 <?php if ($current_path != 'news/brief'): ?> 
  <?php if ($date || $terms): ?>
    <div class="meta">
      <?php if ($created): ?>
        <div class="submitted date-line">
          <?php print format_date($created, 'custom', 'D, M j, Y'); ?>
        </div>
      <?php endif; ?>

      <?php if ($terms): ?>
        <div class="terms terms-inline"><?php print t(' in ') . $terms; ?></div>
      <?php endif; ?>
    </div>
  <?php endif; ?>
 <?php endif; ?>

 <?php //if ($current_path != 'news/brief'): ?>
  <div class="content">
    <?php print preg_replace('%<div class="read-more">.+</div>%m', '', $content); ?>
  </div>
  
  <?php print $links; ?>
 
 <?php //endif; ?>

</div></div> <!-- /node-inner, /node -->

<?php if ($current_path == 'news/brief'): ?>
<p style="text-align:right;"><a href="<?php print "content/news#$id"; ?>" title="<?php print $title ?>">Read More</a></p>
<br/>
<?php endif; ?>