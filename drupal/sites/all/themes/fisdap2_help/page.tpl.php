<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $setting_styles; ?>
  <!--[if IE 8]>
  <?php print $ie8_styles; ?>
  <![endif]-->
  <!--[if IE 7]>
  <?php print $ie7_styles; ?>
  <![endif]-->
  <!--[if lte IE 6]>
  <?php print $ie6_styles; ?>
  <![endif]-->
  <?php print $local_styles; ?>
  <?php print $scripts; ?>
</head>

<body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>">
  <a name='top'></a>
  <div id="header-wrapper">
	<div id="header-group" class="header-group row clearfix <?php print $grid_width; ?>">
	  <?php print $header; ?>
	</div><!-- /header-group -->
  </div><!-- /header-wrapper -->
  <div id="page" class="page">
	<div id="nav-bar-container">
	  <h1 class="site-name"><a href="/">FISDAP</a> <span class="slogan">Online Software for EMS Education</span></h1>
	  <div id="nav-bar-breadcrumbs"><?php echo $help_breadcrumbs; ?></div>
	  <?php if (!$is_help_homepage) { ?>
		<div id="nav-bar-wrapper" class="row">
		  <?php print $nav_bar; ?>
		</div>
	  <?php } ?>
	  <div id="nav-bar-page-title"><h2><?php echo $title; ?></h2></div>
	</div>

	<!-- display nav bar border break on pages that don't have banner -->
	<div id="nav-bar-break"></div>

	<div id="preface-container">
      <!-- preface-top row: width = grid_width -->
      <?php print theme('grid_row', $preface, 'preface', 'full-width', $grid_width); ?>
	  
	  <?php print theme('grid_row', $help, 'content-help', $grid_width); ?>
	  <?php print theme('grid_row', $messages, 'content-messages', $grid_width); ?>
    </div><!-- /preface-container -->

	<div id="main-wrapper" class="main-wrapper full-width">
	  <div id="main" class="main row clearfix <?php print $grid_width; ?>">
		<?php if ($sidebar_first): ?>
		<div id="sidebar-first" class="sidebar-first row nested <?php print $sidebar_first_width; ?>">
		  <div id="sidebar-first-inner">
		  <?php print $sidebar_first; ?>
		  </div>
		</div><!-- /sidebar-first -->
		<?php endif; ?>
		<div id="main-group" class="main-group row nested <?php print $main_group_width; ?>">
		  <div id="content-group" class="content-group row nested <?php print $content_group_width; ?>">
			<?php //print theme('grid_block', $breadcrumb, 'breadcrumbs'); ?>
			<!-- help and messages were here -->
			<a name="main-content-area" id="main-content-area"></a>
			<?php print theme('grid_block', $tabs, 'content-tabs'); ?>

			<div id="content-inner" class="content-inner block">
			  <div id="content-inner-inner" class="content-inner-inner inner">
				
				<?php if ($content_top): ?>
				<div id="content-top" class="row">
				  <?php print $content_top; ?>
				</div>
				<?php endif; ?>
				
				<?php if ($content): ?>
				<div id="content-content" class="content-content">
				  <?php print $content; ?>
				  <?php print $feed_icons; ?>
				</div><!-- /content-content -->
				<?php endif; ?>
			  </div><!-- /content-inner-inner -->
			</div><!-- /content-inner -->
		  </div><!-- /content-group -->

		  <?php if ($sidebar_last): ?>
		  <div id="sidebar-last" class="sidebar-last row nested <?php print $sidebar_last_width; ?>">
			<?php print $sidebar_last; ?>
		  </div><!-- /sidebar-last -->
		  <?php endif; ?>
		</div><!-- /main-group -->
	  </div><!-- /main -->
	</div><!-- /main-wrapper -->

	<!-- rectangles row: width = grid_width -->
    <?php print theme('grid_row', $rectangles, 'rectangles', 'full-width', $grid_width); ?>
	
	<!-- right and left section columns -->
	<?php if ($section_left || $section_right) { ?>
	  <div id="section-columns-wrapper" class="full-width">
		<div id="section-columns" class="row grid12-12">
		  <div id="section-left" class="grid12-6">
			<?php print $section_left; ?>
		  </div>
		  <div id="section-right" class="grid12-6">
			<?php print $section_right; ?>
		  </div>
		</div>
	  </div>
	<?php } ?>
	  
	<!-- <div id="ff-break" style="width:980px; height: 10px; margin: 5px auto 0; background: repeat-x url('<?php print base_path() . path_to_theme(); ?>/images/footergrad.png');"></div> -->

	<?php if ($fat_footer): ?>
	<div id="fat-footer-wrapper">
	  <div id="fat-footer" class="fat-footer row <?php print $grid_width; ?>">
		<?php print $fat_footer; ?>
	  </div><!-- /fat-footer -->
	</div>
    <?php endif; ?>

    <?php if ($footer): ?>
	<div id="footer-wrapper">
	  <div id="footer" class="footer row <?php print $grid_width; ?>">
		<?php print $footer; ?>
	  </div><!-- /footer -->
	</div><!-- /footer-wrapper -->
    <?php endif; ?>

    <?php if ($footer_message): ?>
    <div id="footer-message" class="footer-message row <?php print $grid_width; ?>">
      <?php print theme('grid_block', $footer_message, 'footer-message-text'); ?>
    </div><!-- /footer-message -->
    <?php endif; ?>
	
	<?php if ($utilities): ?>
	<div id="utilities-wrapper">
	  <div id="utilities" class="utilities row <?php print $grid_width; ?>">
		<?php print $utilities; ?>
	  </div><!-- /footer -->
	</div>
    <?php endif; ?>
	
	
  </div><!-- /page -->
  <?php print $closure; ?>
</body>
</html>
