<?php
?>

<div id="node-<?php print $node->nid; ?>" class="node <?php print $node_classes; ?>">
  <div class="inner">
    <?php if ($fisdap_byline) { ?>
      <?php print $fisdap_byline; ?>
    <?php } else if ($teaser) { ?>
      <h2 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php } ?>


    <div class="published-date"><?php print format_date($node->created, 'custom', 'F j, Y'); ?></div>


    <div class="content clearfix">
      <?php print $content ?>
    </div>

    <?php if ($teaser) { ?>
      <div class="read-more orange-button">
	<a href="<?php print $node_url; ?>" title="Read the rest of <?php print $title; ?>">Read more &gt;</a>
      </div>
    <?php } ?>
  </div><!-- /inner -->


        <div class="dotted"><hr /></div>

</div><!-- /node-<?php print $node->nid; ?> -->


