<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $setting_styles; ?>
  <!--[if IE 8]>
  <?php print $ie8_styles; ?>
  <![endif]-->
  <!--[if IE 7]>
  <?php print $ie7_styles; ?>
  <![endif]-->
  <!--[if lte IE 6]>
  <?php print $ie6_styles; ?>
  <![endif]-->
  <?php print $local_styles; ?>
  <?php print $scripts; ?>
</head>

<body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>">
  <a name='top'></a>
  <div id="page" class="page">
    <div id="header-wrapper" class='landing_page'>
      <div id="header-site-info-inner" class="header-site-info-inner inner" style="position:relative; top: 30px;">
        <div id="landing-logo" class="left">
          <a href="/" title="<?php print t('Home'); ?>"><img src="<?php print base_path() . path_to_theme(); ?>/images/Fisdap_logo_new_med.png" alt="<?php print t('Home'); ?>" /></a>
        </div>
        <div id="landing-title" class="left" style="width:200px"><?php print $title; ?></div>
      </div><!-- /header-site-info-inner -->
    </div><!-- /header-wrapper -->

	<div id="pl-break" style="width:980px; height: 10px; margin: 0 auto 5px; background: repeat-x url('<?php print base_path() . path_to_theme(); ?>/images/light_dropshadow_grad.gif');"></div>

	<div id="preface-container">
      <!-- preface-top row: width = grid_width -->
      <?php print theme('grid_row', $preface, 'preface', 'full-width', $grid_width); ?>
	  <?php print theme('grid_row', $help, 'content-help', $grid_width); ?>
	  <?php print theme('grid_row', $messages, 'content-messages', $grid_width); ?>
    </div><!-- /preface-container -->

	<div id="main-wrapper" class="main-wrapper full-width">
	  <div id="main" class="main row clearfix <?php print $grid_width; ?>">
		<?php if ($sidebar_first): ?>
		<div id="sidebar-first" class="sidebar-first row nested <?php print $sidebar_first_width; ?>">
		  <?php print $sidebar_first; ?>
		</div><!-- /sidebar-first -->
		<?php endif; ?>
		<div id="main-group" class="main-group row nested <?php print $main_group_width; ?>">
		  <div id="content-group" class="content-group row nested <?php print $content_group_width; ?>">
			<?php //print theme('grid_block', $breadcrumb, 'breadcrumbs'); ?>
			<!-- help and messages were here -->
			<a name="main-content-area" id="main-content-area"></a>
			<?php print theme('grid_block', $tabs, 'content-tabs'); ?>

			<div id="content-inner" class="content-inner block">
			  <div id="content-inner-inner" class="content-inner-inner inner">

				<?php if ($content): ?>
				<div id="content-content" class="content-content">
				  <?php print $content; ?>
				  <?php print $feed_icons; ?>
				</div><!-- /content-content -->
				<?php endif; ?>
			  </div><!-- /content-inner-inner -->
			</div><!-- /content-inner -->
		  </div><!-- /content-group -->

		  <?php if ($sidebar_last): ?>
		  <div id="sidebar-last" class="sidebar-last row nested <?php print $sidebar_last_width; ?>">
			<?php print $sidebar_last; ?>
		  </div><!-- /sidebar-last -->
		  <?php endif; ?>
		</div><!-- /main-group -->
	  </div><!-- /main -->
	</div><!-- /main-wrapper -->

	<!-- postscript row: width = grid_width -->
    <?php print theme('grid_row', $postscript, 'postscript', 'full-width', $grid_width); ?>

    <?php if ($footer): ?>
        <div id="footer-wrapper">
          <div id="footer" class="footer row <?php print $grid_width; ?>">
                <?php print $footer; ?>
          </div><!-- /footer -->
        </div>
    <?php endif; ?>
	
    <?php if ($utilities): ?>
	<div id="utilities-wrapper">
	  <div id="utilities" class="utilities row <?php print $grid_width; ?>">
		<?php print $utilities; ?>
	  </div><!-- /footer -->
	</div>
    <?php endif; ?>
	
	
  </div><!-- /page -->
  <?php print $closure; ?>
</body>
</html>
