<?php
?>

<div class="comment <?php print $comment_classes;?> clear-block">
  <?php print $picture ?>
  
  <?php if ($comment->new): ?>
  <a id="new"></a>
  <span class="new"><?php print $new ?></span>
  <?php endif; ?>

  
  <div class="content">
    <span class="comment-author"><?php print $comment->name; ?> says</span> <?php print $content ?>
    
    <?php if ($signature): ?>
    <div class="signature">
      <?php print $signature ?>
    </div>
    <?php endif; ?>
  </div>

  <?php if (user_access('administer comments') && $links): ?>
  <div class="links">
    <?php print $links ?>
  </div>
  <?php endif; ?>
  
</div><!-- /comment -->