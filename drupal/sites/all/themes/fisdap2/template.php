<?php

jquery_ui_add('ui.button');
jquery_ui_add('ui.accordion');

/**
 * Node preprocessing
 */
function fisdap2_preprocess_node(&$vars) {
  if ($vars['type'] == 'story') {
    if ($vars['field_author'][0]['nid'] > 0) {
      $author_node = node_load($vars['field_author'][0]['nid']);
      //print '<pre>' . print_r($author_node, TRUE) . '</pre>'; exit;

      if ($vars['teaser']) {
        $vars['fisdap_byline'] = '<h2 class="title"><span class="actual-title">' . l($vars['node']->title, 'node/' . $vars['node']->nid) .  '</span><span class="author-name">By ' . l($author_node->title, 'node/' . $author_node->nid) . '</span></h2>';
      } else {
        $vars['fisdap_byline'] = '<h2 class="title"><span class="actual-title">' . $vars['node']->title .  '</span><span class="author-name">By ' . l($author_node->title, 'node/' . $author_node->nid) . '</span></h2>';
      }

      // Not using author photo anymore
      //if ($author_node->field_author_photo[0]['filepath']) {
      //  $vars['fisdap_byline'] .= l(theme('imagecache', 'author-photo-byline', $author_node->field_author_photo[0]['filepath']), 'node/' . $author_node->nid, array('html' => TRUE));
      //}
    }
  }
}

/**
 * Maintenance page preprocessing
 */
function fisdap2_preprocess_maintenance_page(&$vars) {
  fisdap2_preprocess_page($vars);
}

/**
 * Page preprocessing
 */
function fisdap2_preprocess_page(&$vars) { 
  // Set grid info & row widths
  $grid_name = substr(theme_get_setting('theme_grid'), 0, 7);
  $grid_type = substr(theme_get_setting('theme_grid'), 7);
  $grid_width = (int)substr($grid_name, 4, 2);
  $vars['grid_width'] = $grid_name . $grid_width;
  
  // custom sidebar widths per page/path
  if (preg_match('/open_airways/', request_uri())) {
	$sidebar_first_width = 0;
	$sidebar_last_width = 4;
  } else {
	$sidebar_first_width = ($vars['sidebar_first']) ? theme_get_setting('sidebar_first_width') : 0;
	$sidebar_last_width = ($vars['sidebar_last']) ? theme_get_setting('sidebar_last_width') : 0;
  }
  
  $vars['sidebar_first_width'] = $grid_name . $sidebar_first_width;
  $vars['main_group_width'] = $grid_name . ($grid_width - $sidebar_first_width);
  // For nested elements in a fluid grid calculate % widths & add inline
  if ($grid_type == 'fluid') {
	$sidebar_last_width = round(($sidebar_last_width/($grid_width - $sidebar_first_width)) * 100, 2);
	$vars['content_group_width'] = '" style="width:' . (100 - $sidebar_last_width) . '%';
	$vars['sidebar_last_width'] = '" style="width:' . $sidebar_last_width . '%';
  }
  else {
	$vars['content_group_width'] = $grid_name . ($grid_width - ($sidebar_first_width + $sidebar_last_width));
	$vars['sidebar_last_width'] = $grid_name . $sidebar_last_width;
  }
  
  // Add body classes for custom design options
  $body_classes = explode(' ', $vars['body_classes']);
  $body_classes[] = theme_get_setting('fisdap2_corners');
  
   // Add body classes per page/path
  if (preg_match('/whats_new/', request_uri())) {
	$body_classes[] = 'whats-new';
  }
  if (preg_match('/blog/', request_uri())) {
     $body_classes[] = 'blog';
  }
  if (preg_match('/author/', request_uri())) {
      $body_classes[] = 'blog';  // author nodes are assimilated into the blog
  }
  if (preg_match('/podcasts/', request_uri())) {
      $body_classes[] = 'blog'; // podcasts have been assimilated into the blog
  }

  $body_classes = array_filter($body_classes);   
  $vars['body_classes'] = implode(' ', $body_classes);
  
  // allow for page templates per content type
  if (isset($vars['node'])) {
	// If the node type is "blog" the template suggestion will be "page-blog.tpl.php".
	$vars['template_files'][] = 'page-'. str_replace('_', '-', $vars['node']->type);
  }
  
  // Replace page title as Drupal core does, but strip tags from site slogan.
  // Site name and slogan do not need to be sanitized because the permission
  // 'administer site configuration' is required to set and should be given to
  // trusted users only.
  if (drupal_get_title()) {
	// Just the page title if we already have one (don't add "Fisdap" to it)
    $head_title = array(strip_tags(drupal_get_title()));
  }
  else {
    $head_title = array(variable_get('site_name', 'Drupal'));
    if (variable_get('site_slogan', '')) {
      $head_title[] = strip_tags(variable_get('site_slogan', ''));
    }
  }
  $vars['head_title'] = implode(' | ', $head_title);
}
