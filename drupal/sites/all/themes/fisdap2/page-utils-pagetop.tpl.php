<body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>">
  <div id="page" class="page" style="margin-top: 30px;">
	<div id="nav-bar-container">
	<?php
	  $primary_nav = preg_replace('%href="/%m', 'href="' . utils_get_root_url(false) . '/', theme('grid_block', $primary_links_tree, 'primary-menu'));
	  
	  print preg_replace('%>Home<%m',
						   '><img src="' . utils_get_root_url(true) . '/sites/all/themes/fisdap2/images/Fisdap_logo_new_small.png" alt="Home" style="height:35px;"/><',
						   $primary_nav
						   );
	?>
	  <div id="nav-bar-wrapper" class="row">
		<?php
		### Search box probably won't work from remote site ###
		//print preg_replace('%action="/%m', 'action="' . utils_get_root_url() . '/', $nav_bar);
		?>
	  </div>
	  
	</div>
  
	<div id="nav-bar-break"></div>
	
	<div id="main-wrapper" class="main-wrapper full-width">
	  <div id="main" class="main row clearfix <?php print $grid_width; ?>">
		<div id="main-group" class="main-group row nested <?php print $main_group_width; ?>">
	