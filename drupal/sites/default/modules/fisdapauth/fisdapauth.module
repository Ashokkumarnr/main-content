<?php
/**
 * @file
 * fisdapauth provides authentication against fisdap server.
 */

//////////////////////////////////////////////////////////////////////////////

define('FISDAPAUTH_AUTH_MIXED',           0);
define('FISDAPAUTH_AUTH_EXCLUSIVED',      1);
define('FISDAPAUTH_CONFLICT_LOG',         0);
define('FISDAPAUTH_CONFLICT_RESOLVE',     1);

define('FISDAPAUTH_SSOUT_ENABLE',		  0);
define('FISDAPAUTH_SSOUT_DISABLE',		  1);

define('FISDAPAUTH_EMAIL_FIELD_NO',       0);
define('FISDAPAUTH_EMAIL_FIELD_REMOVE',   1);
define('FISDAPAUTH_EMAIL_FIELD_DISABLE',  2);
define('FISDAPAUTH_PROFILE',              'FISDAP authentication');
define('FISDAPAUTH_PROFILE_WEIGHT',       4);

define('FISDAPAUTH_LOGIN_PROCESS',       variable_get('fisdapauth_login_process', FISDAPAUTH_AUTH_MIXED));
define('FISDAPAUTH_LOGIN_CONFLICT',      variable_get('fisdapauth_login_conflict', FISDAPAUTH_CONFLICT_LOG));
define('FISDAPAUTH_FORGET_PASSWORDS',    variable_get('fisdapauth_forget_passwords', TRUE));
define('FISDAPAUTH_SYNC_PASSWORDS',      variable_get('fisdapauth_sync_passwords', FALSE));
define('FISDAPAUTH_DISABLE_PASS_CHANGE', variable_get('fisdapauth_disable_pass_change', FALSE));
define('FISDAPAUTH_ALTER_EMAIL_FIELD',   variable_get('fisdapauth_alter_email_field', FISDAPAUTH_EMAIL_FIELD_NO));

define('FISDAPAUTH_SINGLE_SIGN_OUT',	 variable_get('fisdapauth_single_sign_out', FISDAPAUTH_SSOUT_DISABLE));

//////////////////////////////////////////////////////////////////////////////
// Core API hooks


/**
 * Implements hook_init().
 */
function fisdapauth_init() {
	// Add FISDAP libraries to the include path

	$dir = '/var/www/html/drupal/sites/default/modules/fisdapauth';
	$paths = array(get_include_path());
	$paths[] = $dir;
	#$paths[] = $dir . 'phputil/Zend/library';
	set_include_path(implode(PATH_SEPARATOR, $paths));


	// require FISDAP user functions
	require_once('phputil/user_utils.php');
}

/**
 * Implementation of hook_help().
 */
function fisdapauth_help($path, $arg) {
	switch ($path) {
		case 'admin/settings/fisdapauth':
			return '<p>'. t('FISDAP authentication settings.') .'</p>';
	}
}

/**
 * Implements hook_menu().
 */
function fisdapauth_menu() {
	return array(
				 'admin/settings/fisdap' => array(
												  'title' => 'FISDAP',
												  'description' => 'Configure FISDAP integration settings.',
												  'page callback' => 'fisdapauth_admin_menu_block_page',
												  'access arguments' => array('administer fisdap modules'),
												  'file' => 'fisdapauth.admin.inc',
												  ),
				 'admin/settings/fisdap/fisdapauth' => array(
															 'title' => 'Authentication',
															 'description' => 'Configure FISDAP authentication settings.',
															 'page callback' => 'drupal_get_form',
															 'page arguments' => array('fisdapauth_admin_settings'),
															 'access arguments' => array('administer fisdap modules'),
															 'file' => 'fisdapauth.admin.inc',
															 ),
				 'admin/settings/fisdap/fisdapauth/configure' => array(
																	   'title' => 'Settings',
																	   'type' => MENU_DEFAULT_LOCAL_TASK,
																	   ),
				 );
}


/**
 * Implements hook_user().
 */
function fisdapauth_user($op, &$edit, &$account, $category = NULL) {
	//xdebug_break();

	switch ($op) {
		case 'login':
			if (isset($account->fisdap_authentified)) {
				// get current FISDAP user info
				$fisdap_user = _fisdapauth_user_lookup($account->fisdap_username);

				// update profile
				$record = array(
									'profile_firstname' => $fisdap_user['firstname'],
									'profile_lastname' => $fisdap_user['lastname']
									);
				profile_save_profile($record, $account, $category = 'Contact Info', $register = FALSE);

				// set roles
				if ($fisdap_user['instructor'] == 1) {
					_fisdapauth_grant_role($account, 'FISDAP Instructor');
				} else {
					_fisdapauth_grant_role($account, 'FISDAP Student');
				}
				if ($fisdap_user['Reviewer'] == 1) {
					_fisdapauth_grant_role($account, 'FISDAP Reviewer');
				}

				if (FISDAPAUTH_SINGLE_SIGN_OUT == FISDAPAUTH_SSOUT_ENABLE) {
					//set special cookie to be used by FISDAP for checking if the user is logged in
					setcookie('FISDAP_DRUPAL', 'logged_in', 0, '/');
				}
			}
			break;

		case 'logout':
			if (isset($account->fisdap_authentified)) {
				if (FISDAPAUTH_SINGLE_SIGN_OUT == FISDAPAUTH_SSOUT_ENABLE) {
					// force logout from FISDAP
					setcookie('FISDAP_DRUPAL', '', time()-42000, '/');
					drupal_goto('https://www.fisdap.net/auth/logout.php');
				}
			}
			break;

		case 'update':
			if ($category == 'account') {

				// If authentication is being done in "FISDAP only" mode, passwords
				// should not be written to the database, or users would be able
				// to log in even after removing their FISDAP entry.
				if (isset($account->fisdap_authentified) && (FISDAPAUTH_LOGIN_PROCESS == FISDAPAUTH_AUTH_EXCLUSIVED || !FISDAPAUTH_SYNC_PASSWORDS))
					$edit['pass'] = NULL;
			}

			if (FISDAPAUTH_ALTER_EMAIL_FIELD == FISDAPAUTH_EMAIL_FIELD_REMOVE)
				unset($edit['mail']);
			break;

		case 'validate':
			/*
			IF THE MODULE IS ENABLED AFTER INITIAL USER CREATION,
			THE FOLLOWING PREVENTS LOCAL DATABASE USERS WITH USERNAMES
			IDENTICAL TO THOSE IN THE FISDAP DATABASE FROM BEING EDITABLE,
			UNTIL THEY AUTHENTICATE WITH THEIR FISDAP PASSWORD!
			*/
			// check for existing username in FISDAP database
			if (isset($edit['name']) && is_null($account->fisdap_authentified)) {
				$fisdap_user = _fisdapauth_user_lookup($edit['name']);
				if ($edit['name'] == $fisdap_user['email']) {
					form_set_error('name', t('The username %name is already taken.', array('%name' => $edit['name'])));
				}
			}
			break;

		case 'view':
			if (user_access('administer users') && isset($account->fisdap_authentified)) {
				$account->content[t(FISDAPAUTH_PROFILE)] = array(
										'#type' => 'user_profile_category',
										'#title' => t(FISDAPAUTH_PROFILE),
										'#attributes' => array('class' => 'fisdapauth-entry'),
										'#weight' => FISDAPAUTH_PROFILE_WEIGHT,
										'fisdap_username' => array('#type' => 'user_profile_item', '#title' => t('FISDAP username'), '#value' => $account->fisdap_username, '#weight' => 1),
										);
			}
			break;
	}
}

/**
 * Implementation of hook_menu_alter().
 */
function fisdapauth_menu_alter(&$callbacks) {
	$callbacks['user/%user_uid_optional']['access callback'] = 'fisdapauth_user_view_access';
	/*
	// THIS WILL BREAK THE PASSWORD REQUEST PAGE FOR ALL USERS
	if (variable_get('fisdapauth_disable_pass_change', FALSE)) {
		unset($callbacks['user/password']);
	}
	*/
}


function fisdapauth_user_view_access($account) {
	$access = $account && $account->uid &&
	(
	 // Administrators can view all accounts.
	 user_access('administer users') ||

	 // Always let users view their own profile
	 ($GLOBALS['user']->uid == $account->uid) ||
	 // , as long as they're not a FISDAP user
	 #(($GLOBALS['user']->uid == $account->uid) && is_null($GLOBALS['user']->fisdap_authentified)) ||

	 // The user is not blocked and logged in at least once.
	 ($account->access && $account->status && user_access('access user profiles'))
	);

	return $access;
}


/**
 * Implements hook_perm().
 */
function fisdapauth_perm() {
	return array('administer fisdap modules');
}

/**
 * Implements hook_form_alter().
 */
function fisdapauth_form_alter(&$form, $form_state, $form_id) {
	global $user;

	// Replace the drupal authenticate function if it's used as validation.
	if (isset($form['#validate']) && is_array($form['#validate']) && ($key = array_search('user_login_authenticate_validate', $form['#validate'])))
		$form['#validate'][$key] = 'fisdapauth_login_authenticate_validate';

	switch ($form_id) {
		case 'user_login_block':
			if (FISDAPAUTH_DISABLE_PASS_CHANGE)
				unset($form['links']);
			break;
		case 'user_profile_form':
			$account = $form["_account"]["#value"];

			if (isset($account->fisdap_authentified)) {
				$form['account']['name']['#attributes']['READONLY'] = 'READONLY';
			}

			if ($user->uid != 1 && isset($account->fisdap_authentified)) {
				if (FISDAPAUTH_DISABLE_PASS_CHANGE)
					unset($form['account']['pass']);

				switch (FISDAPAUTH_ALTER_EMAIL_FIELD) {
					case FISDAPAUTH_EMAIL_FIELD_REMOVE :
						$form['account']['mail']['#type'] = 'hidden';
						$form['account']['mail']['#attributes']['READONLY'] = 'READONLY';
						break;
					case FISDAPAUTH_EMAIL_FIELD_DISABLE :
						$form['account']['mail']['#attributes']['READONLY'] = 'READONLY';
						break;
				}

				// Remove fieldset if empty.
				if (isset($form['account']) && !isset($form['account']['pass']) && $form['account']['mail']['#type'] == 'hidden' && count(array_filter($form['account'], create_function('$a', 'return is_array($a) ? TRUE : FALSE;'))) == 1) {
					$form['mail'] = $form['account']['mail'];
					unset($form['account']);
				}
			}
			break;
	}
}

/**
 * Implements hook_cron().
 */
function fisdapauth_cron() {
  cache_clear_all(NULL, 'cache_filter');
}

/**
 * Implements hook_exit().
 */
function fisdapauth_exit() {
  // We delete the login info here, instead of just not storing it at
  // _fisdapauth_auth(), so at least fisdapgroups can use it at login time.
  if (FISDAPAUTH_FORGET_PASSWORDS && isset($_SESSION['fisdap_login'])) {
    unset($_SESSION['fisdap_login']);
  }
}

//////////////////////////////////////////////////////////////////////////////
// Login process functions

/**
 * Main user validation function.
 *
 * If successful, sets the global $user object.
 */
function fisdapauth_login_authenticate_validate($form, &$form_state) {
  fisdapauth_authenticate($form_state['values']);
}

/**
 * Main user authentication function.
 *
 * If successful, sets the global $user object.
 */
function fisdapauth_authenticate($form_values = array()) {
	//xdebug_break();

	global $user;

	$name = $form_values['name'];
	$pass = trim($form_values['pass']);

	// The user_login_name_validate() is not called if the user is being authenticated
	// from the httpauth or services modules, therefore call it here.
	$form_state['values'] = $form_values;
	user_login_name_validate(NULL, $form_state);

	// (Design decision) uid=1 (admin user) must always authenticate to local database
	// this user is critical for all drupal admin and upgrade operations so it is best
	// left with drupal's native authentication.
	$result = db_query("SELECT uid FROM {users} WHERE name = '%s' AND uid = '1'", $name);
	if ($account = db_fetch_object($result)) {
		user_authenticate($form_values);
		return;
	}

	if (FISDAPAUTH_LOGIN_PROCESS == FISDAPAUTH_AUTH_MIXED) {
		// Authenticate local users first.
		$result = db_query("SELECT name, data FROM {users} WHERE name='%s'", $name);
		if ($row = db_fetch_array($result)) {
			$data = unserialize($row['data']);
			if (!isset($data['fisdap_authentified']) || $data['fisdap_authentified'] == 0) {
				// A local user $name exists - authenticate that user.
				if (user_authenticate($form_values))
					return;
			}
		}
	}

	$account = user_load(array('name' => $name, 'status' => 1));
	if ($account && drupal_is_denied('mail', $account->mail)) {
		form_set_error('name', t('The name %name is registered using a reserved e-mail address and therefore could not be logged in.', array('%name' => $account->name)));
	}

	// If there is any validations errors, we do not query FISDAP.
	if (form_get_errors())
		return;

	// Authenticate FISDAP user.
	if (!($fisdap_username = _fisdapauth_auth($name, $pass)))
		return;

	if (!$account) {
		// Register this new user.
		if ($fisdap_user = _fisdapauth_user_lookup($name)) {
			// If mail attribute is missing, set the name as mail.
			$init = $mail = $fisdap_user['emailaddress'] ? $fisdap_user['emailaddress'] : $name;

			// Check if the e-mail is not denied.
			if (drupal_is_denied('mail', $mail)) {
				form_set_error('name', t('The name %name is registered using a reserved e-mail address and therefore could not be logged in.', array('%name' => $name)));
				return;
			}

			// Generate a random drupal password. FISDAP password will be used anyways.
			$pass_new = (FISDAPAUTH_LOGIN_PROCESS == FISDAPAUTH_AUTH_EXCLUSIVED || !FISDAPAUTH_SYNC_PASSWORDS) ? user_password(20) : $pass;

			$userinfo = array(
							  'name' => $name,
							  'pass' => $pass_new,
							  'mail' => $mail,
							  'init' => $init,
							  'status' => 1,
							  'authname_fisdapauth' => $name,
							  'fisdap_authentified' => TRUE,
							  'fisdap_username' => $fisdap_user['email'],
							  );

			$user = user_save('', $userinfo);

			watchdog('fisdapauth', 'New external user %name created from FISDAP database.', array('%name' => $name), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $user->uid .'/edit'));
		}
	} else {
		// Login existing user.
		$data = array('fisdap_username' => $fisdap_username);

		if (!isset($account->fisdap_authentified)) {
			// FISDAP and local user conflict.
			if (FISDAPAUTH_LOGIN_CONFLICT == FISDAPAUTH_CONFLICT_LOG) {
				watchdog('fisdapauth', 'FISDAP user named %fisdap_username has a naming conflict with a local drupal user %name', array('%fisdap_username' => $fisdap_username, '%name' => $account->name), WATCHDOG_ERROR);
				drupal_set_message(t('Another user already exists in the system with the same login name. You should contact the system administrator in order to solve this conflict.'), 'error');
				return;
			}
			else {
				$data['fisdap_authentified'] = TRUE;
				$data['authname_fisdapauth'] = $name;
			}
		}

		// Successfull login.
		// Save the new login data.
		if (FISDAPAUTH_LOGIN_PROCESS == FISDAPAUTH_AUTH_MIXED && FISDAPAUTH_SYNC_PASSWORDS)
			$data['pass'] = $pass;

		$user = user_save($account, $data);
	}

	// Save user's authentication data to the session.
	$_SESSION['fisdap_login']['username'] = $fisdap_username;
	$_SESSION['fisdap_login']['pass'] = $pass;

	user_authenticate_finalize($form_values);
	return $user;
}

/**
 * Authenticate the user against FISDAP server.
 *
 * @param $name
 *   A username.
 * @param $pass
 *   A password.
 *
 * @return
 *  User's FISDAP username ("email") on success, FALSE otherwise.
 */
function _fisdapauth_auth($name, $pass) {
	// Don't allow empty passwords because they cause problems on some setups.
	// http://drupal.org/node/87831
	if (empty($pass)) return FALSE;

	// Look up the user in FISDAP.
	// If they do not exist (or are missing a value for 'email') don't bother trying to authenticate.
	if ( !($fisdap = _fisdapauth_user_lookup($name)) || !isset($fisdap['email']) ) return FALSE;

	// Try to authenticate.
	if (check_user_password($fisdap['email'], $pass)) {
		return $fisdap['email'];
	} else {
		return FALSE;
	}
}

/**
 * Queries FISDAP for the user.
 *
 * @param $name
 *   A login name.
 *
 * @return
 *   An array with user's FISDAP data or NULL if not found.
 */
function _fisdapauth_user_lookup($name) {
	$fisdap_user = get_user_info($name);

	if (is_array($fisdap_user)) {
		return $fisdap_user;
	} else {
		return NULL;
	}
}


/**
 * Grant a user with a role.
 *
 * @param $user
 *   A user object.
 * @param $rolename
 *   A name of the role.
 *
 * @return
 */
function _fisdapauth_grant_role($user, $rolename) {
  $result = db_query("SELECT * FROM {role} WHERE name = '%s'", $rolename);
  if ($row = db_fetch_object($result)) {
    $result = db_query("SELECT * FROM {users_roles} WHERE uid = %d AND rid = %d", $user->uid, $row->rid);
    if (!db_fetch_object($result)) {
      db_query("INSERT INTO {users_roles} (uid, rid) VALUES (%d, %d)", $user->uid, $row->rid);
    }
  }
}

/**
 * Deny a user with a role.
 *
 * @param $user
 *   A user object.
 * @param $rolename
 *   A name of the role.
 *
 * @return
 */
function _fisdapauth_deny_role($user, $rolename) {
  $result = db_query("SELECT * FROM {role} WHERE name = '%s'", $rolename);
  if ($row = db_fetch_object($result)) {
    $result = db_query("SELECT * FROM {users_roles} WHERE uid = %d AND rid = %d", $user->uid, $row->rid);
    if (db_fetch_object($result)) {
      db_query("DELETE FROM {users_roles} WHERE uid = %d AND rid = %d", $user->uid, $row->rid);
    }
  }
}