<?php

/**
 * This is a support library for dumping a consistent database with our
 * test program's data to either SQL (e.g., for populating a test server
 * deployment's database) or XML (e.g., for use with dbunit).
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 *                                                                           *
 * @package default                                                          *
 * @author Sam Martin <smartin@fisdap.net>                                   *
 * @copyright 1996-2007 Headwaters Software, Inc.                            *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 */


/**
 * Dump the tables for which we only need the schemas.
 *
 * @param array $tables the tables to dump
 * @param string $database the database to use
 */
function dump_schema_only_tables($tables, $database='FISDAP') 
{
    $dump_command = 'mysqldump -e -d --skip-add-drop-table ' 
        .           $database 
        .           ' ' 
        .           join(' ', $tables);
    echo `$dump_command`;
}//dump_schema_only_tables


/**
 * Dump the tables for which we need data where a given key matches a
 * given value.
 *
 * @param array $tables_and_keys the tables to dump, with the desired keys
 * @param string $database the database to use
 */
function dump_simple_tables($tables_and_keys, 
                            $value,
                            $database='FISDAP') 
{
    foreach ( $tables_and_keys as $table => $key ) 
    {
        $where_clause = $key . '=' . $value;
        $dump_command = 'mysqldump -e -t '
            .           '-u root '
            .           '-w "'. $where_clause . '" ' 
            .           '--skip-add-drop-table '
            .           $database . ' '
            .           $table;
        echo `$dump_command`;
    }//foreach
}//dump_simple_tables


/**
 * Dump the tables for which we need all existing records.
 *
 * @param array $tables the tables to dump
 * @param string $database the database to use
 */
function dump_data_tables($tables, $database='FISDAP') 
{
    $dump_command = 'mysqldump -e -t '
        .           '-u root '
        .           '--skip-add-drop-table '
        .           $database . ' '
        .           join(' ', $tables);
    echo `$dump_command`;
}//dump_data_tables


/**
 * Dump the tables for which we need our program's data
 *
 * @param array $tables_and_params the tables to dump
 * @param string $database the database to use
 */
function dump_joined_tables($tables_and_params, $database='FISDAP') 
{
    foreach ( $tables_and_params as $table => $params ) 
    {
        // fetch the IDs of the records we need
        $id_field = $params['id_field'];
        $join_to  = (is_array($params['join_to'])) ? 
            join(', ', $params['join_to']) :
            $params['join_to'];
        $where    = $params['where'];
        $full_id_field = $table . '.' . $id_field;
        $query = 'SELECT ' . $full_id_field . ' '
               . 'FROM ' . $table . ', ' . $join_to . ' '
               . 'WHERE ' . $where;

        $connection =& FISDAPDatabaseConnection::get_instance($database);
        $result = $connection->query($query);
        if ( !$result || !is_array($result) ) 
        {
            return;
        }//if

        $selected_ids = array();
        foreach ( $result as $row ) 
        {
            $selected_ids[] = $row[$id_field];
        }//foreach

        // build a where clause with the ids of all the selected records
        $dump_where  = $full_id_field . '=';
        $dump_where .= join(' OR ' . $full_id_field . '=', $selected_ids);

        // split the where clauses into manageable chunks
        $dump_where_clauses = split_where_clause($dump_where);

        // dump the selected records
        if ( $dump_where_clauses && is_array($dump_where_clauses) ) 
        {
            foreach ( $dump_where_clauses as $where_clause ) 
            {
                $dump_command = 'mysqldump -e -t '
                    .           '-u root '
                    .           '-w "' . $where_clause . '" '
                    .           '--skip-add-drop-table '
                    .           $database . ' '
                    .           $table;
                echo `$dump_command`;
            }//foreach

            // build a where clause with the ids of all the selected records
            $dump_where  = $full_id_field . '=';
            $dump_where .= join(' OR ' . $full_id_field . '=', $selected_ids);

            // split the where clauses into manageable chunks
            $dump_where_clauses = split_where_clause($dump_where);

            // dump the selected records
            if ( $dump_where_clauses && is_array($dump_where_clauses) ) {
                foreach ( $dump_where_clauses as $where_clause ) {
                    $dump_command = 'mysqldump -n -e -t '
                                  . '-w "' . $where_clause . '" '
                                  . $database . ' '
                                  . $table;
                    echo `$dump_command`;
                }//foreach
            }//if
        }//foreach
    }//foreach
}//dump_joined_tables


/**
 * Print out CREATE statements for all tables in the given database
 * (defaulting to 'FISDAP').
 *
 * @param string $database the database whose tables we want to dump
 */
function dump_create_statements($database='FISDAP') 
{
    $create_command = 'mysqldump -d --skip-add-drop-table ' . $database;
    echo `$create_command`;
}//dump_create_statements


/**
 * Return an array of where clauses, splitting the given where clause
 * (which is assumed to be a disjunction of predicates) into chunks that'll fit
 * in one mysqldump command.
 *
 * Note: it is assumed that there are no extra occurrences of the string " OR "
 * in the where clause (e.g., 'A=5 OR B=64 OR C="A OR B"' will fail)
 */
function split_where_clause($where, $threshold=30) 
{
    if ( !$where ) 
    {
        return array();
    }//if
    
    $chunks = split(' OR ', $where);
    $where_clauses = array();
    $num_remaining_chunks = count($chunks);
    while ( $num_remaining_chunks > $threshold ) 
    {
        $where_clauses[] = join(
            ' OR ',
            array_slice(
                $chunks,
                0,
                $threshold
            )
        );
        $chunks = array_slice($chunks, $threshold);
        $num_remaining_chunks -= $threshold;
    }//while

    // if there are any chunks left, add 'em to the list
    if ( $chunks ) 
    {
        $where_clauses[] = join(' OR ', $chunks);
    }//if

    return $where_clauses;
}//split_where_clause


/**
 *
 */
function dump_evaluation_assets($database='FISDAP') 
{
    $joined_tables = array(
        'Asset_def' => array(
            'id_field' => 'AssetDef_id',
            'join_to'  => 'Eval_def',
            'where'    =>       'Asset_def.DataType_id '
                          .     '= "' . ASSET_EVALUATION . '" '
                          . 'AND Asset_def.Data_id '
                          .     '= Eval_def.EvalDef_id'
        )
    );

    dump_joined_tables($joined_tables, $database);
}//dump_evaluation_assets


/**
 *
 */
function dump_program_data($programs, $database='FISDAP') 
{
    foreach ( $programs as $program_id ) 
    {
        $joined_tables = array(
            'UserAuthData' => array(
                'id_field'=> 'idx',
                'join_to' => 'InstructorData',
                'where'   => 'InstructorData.ProgramId '
                    .        '= "' . $program_id . '" '
                    . 'AND UserAuthData.email = InstructorData.UserName'
            ),
            'InstPermHistory' => array(
                'id_field'=> 'PermHist_id',
                'join_to' => 'InstructorData',
                'where'   => 'InstructorData.ProgramId '
                    .        '= "' . $program_id . '" '
                    . 'AND (    InstructorData.Instructor_id '
                    .          '= InstPermHistory.Changed_Inst_id '
                    .       'OR InstructorData.Instructor_id '
                    .          '= InstPermHistory.Changing_Inst_id '
                    .     ') '
            ),
            'SectInstructors' => array(
                'id_field'=> 'SectInst_id',
                'join_to' => 'InstructorData',
                'where'   => 'InstructorData.ProgramId '
                    .        '= "' . $program_id . '" '
                    . 'AND SectInstructors.Instructor_id '
                    .     '= InstructorData.Instructor_id'
            ),
            'AmbulanceServices' => array(
                'id_field'=> 'AmbServ_id',
                'join_to' => 'ProgramSiteAssoc',
                'where'   => 'ProgramSiteAssoc.Program_id '
                    .        '= "' . $program_id . '" '
                    . 'AND ProgramSiteAssoc.Site_id '
                    .     '= AmbulanceServices.AmbServ_id'
            ),
            'AmbServ_Bases' => array(
                'id_field'=> 'Base_id',
                'join_to' => 'ProgramSiteAssoc',
                'where'   => 'ProgramSiteAssoc.Program_id '
                    .        '= "' . $program_id . '" '
                    . 'AND ProgramSiteAssoc.Site_id = AmbServ_Bases.AmbServ_id'
            )
        );
        dump_joined_tables($joined_tables, $database);

        $simple_tables = array(
            'ClassSections'        => 'Program_id',
            'EventCSAccess'        => 'Program_id',
            'InstructorData'       => 'ProgramId',
            'JRCPrefsData'         => 'Program_id',
            'PriceDiscount'        => 'Program_id',
            'ProgEventPrefs'       => 'Program_id',
            'ProgramBaseData'      => 'Program_id',
            'ProgramData'          => 'Program_id',
            'ProgramPreceptorData' => 'Program_id',
            'ProgramSiteAssoc'     => 'Program_id',
            'ProgramSiteData'      => 'Program_id',
            'SerialNumbers'        => 'Program_id',
            'StudentData'          => 'Program_id'
        );
        dump_simple_tables($simple_tables, $program_id, $database);
    }//foreach
}//dump_program_data


/**
 * Dump sql for the given student's evaluation sessions (including
 * data-eval links).
 */
function dump_student_evaluations($student_id, $database='FISDAP') 
{
    $joined_tables = array(
        'DataEvalLinks' => array(
            'id_field' => 'DataEvalLink_id',
            'join_to'  => array('StudentData', 'Eval_Session'),
            'where'    => 'DataEvalLinks.EvalSession_id '
                .         '= Eval_Session.EvalSession_id '
                . 'AND StudentData.Student_id = "' . $student_id . '" '
                . 'AND (    Eval_Session.Evaluator = StudentData.UserName '
                .      ' OR Eval_Session.Subject = StudentData.UserName '
                .     ')'    
        ),
        'Eval_CriticalCriteriaSessions' => array(
            'id_field'=> 'CritCriteriaSes_id',
            'join_to' => array('Eval_Session', 'StudentData'),
            'where'    => 'StudentData.Student_id '
                .         '= "' . $student_id . '" '
                . 'AND (    Eval_Session.Evaluator = StudentData.UserName '
                .      ' OR Eval_Session.Subject = StudentData.UserName '
                .     ')'    
                . 'AND Eval_CriticalCriteriaSessions.EvalSession_id '
                .     '= Eval_Session.EvalSession_id'
        ),
        'Eval_ItemSessions' => array(
            'id_field'=> 'ItemSession_id',
            'join_to' => array('Eval_Session', 'StudentData'),
            'where'    => 'StudentData.Student_id '
                .         '= "' . $student_id . '" '
                . 'AND (    Eval_Session.Evaluator = StudentData.UserName '
                .      ' OR Eval_Session.Subject = StudentData.UserName '
                .     ')'    
                . 'AND Eval_ItemSessions.EvalSession_id '
                .     '= Eval_Session.EvalSession_id'
        ),
        'Eval_Comment_Session' => array(
            'id_field'=> 'CommentSession_id',
            'join_to' => array('Eval_Session', 'StudentData'),
            'where'    => 'StudentData.Student_id '
                .         '= "' . $student_id . '" '
                . 'AND (    Eval_Session.Evaluator = StudentData.UserName '
                .      ' OR Eval_Session.Subject = StudentData.UserName '
                .     ')'    
                . 'AND Eval_Comment_Session.EvalSession_id '
                .     '= Eval_Session.EvalSession_id'
        ),
        'Eval_Session' => array(
            'id_field'=> 'EvalSession_id',
            'join_to' => 'StudentData',
            'where'    => 'StudentData.Student_id '
                .         '= "' . $student_id . '" '
                . 'AND (    Eval_Session.Evaluator = StudentData.UserName '
                .      ' OR Eval_Session.Subject = StudentData.UserName '
                .     ')'    
        ),
    );

    dump_joined_tables($joined_tables, $database);
}//dump_student_evaluations


/**
 *
 */
function dump_student_data($students, $database='FISDAP') 
{
    foreach ( $students as $student_id ) 
    {
        dump_student_evaluations($student_id, $database);

        $joined_tables = array(
            'UserAuthData' => array(
                'id_field'=> 'idx',
                'join_to' => 'StudentData',
                'where'   => 'StudentData.Student_id="' . $student_id . '" '
                    . 'AND UserAuthData.email = StudentData.UserName '
            ),
            'SectStudents' => array(
                'id_field'=> 'SectStud_id',
                'join_to' => 'StudentData',
                'where'   => 'StudentData.Student_id= "' . $student_id . '" '
                    . 'AND SectStudents.Student_id = StudentData.Student_id'
            ),
            'UserSettings' => array(
                'id_field'=> 'UserAuth_id',
                'join_to' => array('StudentData', 'UserAuthData'),
                'where'   => 'StudentData.Student_id="' . $student_id . '" '
                    . 'AND UserAuthData.idx=UserSettings.UserAuth_id '
                    . 'AND UserAuthData.email=StudentData.UserName'
            )
        );
        dump_joined_tables($joined_tables, $database);

        $simple_tables = array(
            'SectStudents' => 'Student_id',
            'ALSAirwayData' => 'Student_id',
            'AssesmentData' => 'Student_id',
            'BLSSkillsData' => 'Student_id',
            'CCTransactionData' => 'Student_id',
            'EKGData' => 'Student_id',
            'IVData' => 'Student_id',
            'MedData' => 'Student_id',
            'OtherALSData' => 'Student_id',
            'PreceptorData' => 'Student_id',
            'PtComplaintData' => 'Student_id',
            'RunData' => 'Student_id',
            'ShiftData' => 'Student_id',
            'TestScores' => 'Student_id'
        );
        dump_simple_tables($simple_tables, $student_id, $database);
    }//foreach
}//dump_student_data

?>
