<?php

/**
 * This file defines constants for the IDs of the various ALS Airway types
 */

define('ALS_AIR_ENDOTRACHEAL_TUBE'            , '10');  
define('ALS_AIR_ESOPHAGEAL_OBTURATOR'         ,  '1');  
define('ALS_AIR_ESOPHAGEAL_GASTRIC_OBTURATOR' ,  '2');  
define('ALS_AIR_COMBITUBE'                    ,  '3');  
define('ALS_AIR_PHARYNGEOTRACHEAL_LUMEN'      ,  '4');  
define('ALS_AIR_NASOTRACHEAL_INTUBATION'      ,  '5');  
define('ALS_AIR_DIGITAL_ET_INTUBATION'        ,  '6');  
define('ALS_AIR_VALSALVA_MANEUVER'            ,  '7');  
define('ALS_AIR_CAROTID_SINUS_MASSAGE'        ,  '8');  
define('ALS_AIR_LARYNGEAL_MASK_AIRWAY'        ,  '9');  

?>
