<?php

require_once( "get_inst_perm.html");

function can_inst_change($shift_prog,$shift_type,$edit_field,$edit_clinical,$edit_lab,$username,$db)
{
    $select = "SELECT Instructor_id, ProgramId FROM InstructorData WHERE UserName='$username'";
    $result = mysql_query($select,$db);
    $count = mysql_num_rows($result);
    if($count==1)
    {
        $inst_id = mysql_result($result, 0, "Instructor_id");
        $user_program = mysql_result($result,0,"ProgramId");
        if($user_program != $shift_prog)
        {
            return false;
        }
        else
        {
            if(($shift_type== 'field' && $edit_field) ||
               ($shift_type == 'clinical' && $edit_clinical) ||
               ($shift_type == 'lab' && $edit_lab))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

function can_stu_change($username,$shiftstu,$shift_complete,$db)
{
    $select = "SELECT Student_id FROM StudentData where UserName = '$username'";
    $result = mysql_query($select,$db);
    $count = mysql_num_rows($result);
    if($count==1)
    {
        $user_student_id = mysql_result($result,0,"Student_id");
    }
    if($user_student_id!=$shiftstu)
    {
        return false;
    }
    else
    {
        if($shift_complete == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

function can_change_shift($shift,$instructor,$user,$db)
{
    global $canEditFieldSkills;
    global $canEditClinicSkills;
    global $canEditLabSkills;
//return false;
  if(isset($shift) && $shift>0)
  {
    $shift_sel = "SELECT ShiftData.Student_id, ShiftData.Type, ShiftData.Completed, StudentData.Program_id FROM ShiftData, "
	. "StudentData WHERE ShiftData.Shift_id = $shift AND ShiftData.Student_id = StudentData.Student_id";
    $shift_res = mysql_query($shift_sel,$db);
    $shift_count = mysql_num_rows($shift_res);
    if($shift_count == 1)
    {
	$shift_student = mysql_result($shift_res,0,"Student_id");
	$shift_type = mysql_result($shift_res,0,"Type");
//	echo "type is: $shift_type<br>\n";
	$shift_program = mysql_result($shift_res,0,"Program_id");
	$shift_completed = mysql_result($shift_res,0,"Completed");
    }
    if($instructor==1)
    {
	return can_inst_change(	$shift_program,
			       	$shift_type,
			        $canEditFieldSkills,
				$canEditClinicSkills,
				$canEditLabSkills,
				$user,
				$db);
    }
    else
    {
	return can_stu_change(	$user,
				$shift_student,
				$shift_completed,
				$db);
    }
  }
  else
  {
    echo "There has been an error processing your request.  Please try again.<br>\n";
    $errmsg = "Shift unset, shift: $shift, instructor: $instructor, user: $user\n";
    $errmsg .= "Browser: " . $_SERVER['HTTP_USER_AGENT'] . "\n";
    $errmsg .= "File: " . $_SERVER['SCRIPT_NAME'] . "\n";
    $errmsg .= "Referer: " . $_SERVER['HTTP_REFERER'] . "\n";
    $errmsg .= "QString: " . $_SERVER['QUERY_STRING'] . "\n";
    mail('erikh@fisdap.net','Shift ID Unset',$errmsg);
    exit;
  }

}//can_cnage_shift

function check_for_change_shift($shift,$instructor,$user,$db)
{
   $can_change = can_change_shift($shift,$instructor,$user,$db);
   if(($can_change))
   {
	echo "You are not able to access this shift.";
	exit;
   }
}//check_for_change_shift

?>
