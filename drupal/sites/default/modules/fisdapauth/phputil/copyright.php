<?php

#
# copyright.php - Display a copyright notice via HTML comments
#

if (defined('FISDAP_ROOT') && defined('DOCTYPE')) {
  include_once(FISDAP_ROOT.'phputil/doctypes.php');
  echo constant(DOCTYPE) . "\n";
}

#
# Set the year to display in the copyright notice below
#

$CurYear = Date("Y"); # Displayed below in the copyright notice

#
# Display the copyright notice
#

?>

<!--**********************************************************************-->
<!--                                                                      -->
<!--      Copyright (C) 1996-<?php echo $CurYear;?>.  This is an unpublished work of        -->
<!--                       Headwaters Software, Inc.                      -->
<!--                          ALL RIGHTS RESERVED                         -->
<!--      This program is a trade secret of Headwaters Software, Inc.     -->
<!--      and it is not to be copied, distributed, reproduced, published, -->
<!--      or adapted without prior authorization                          -->
<!--      of Headwaters Software, Inc.                                    -->
<!--                                                                      -->
<!--**********************************************************************-->

