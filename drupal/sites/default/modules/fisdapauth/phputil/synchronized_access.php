<?php

/**
 * This file contains functions which manage synchronized access to a given 
 * block of code. 
 * 
 * @author Sam Martin <smartin@fisdap.net>
 */


/** 
 * this is the identifier for the semaphore we'll create when locking 
 * access to a given section of code (note that only one semaphore may
 * be used at a given time by a given script)
 */
$semaphore_key = 0;


/** 
 * Wait for an exclusive lock on the current file before
 * continuing program execution.
 */
function start_exclusive_access() {
    global $semaphore_key; 

    // get the Sys V resource id
    $resource_id = ftok(__FILE__,'A'); // 'A' is just a dummy project id
    if ( !$resource_id ) {
        log_debug_message('Error getting Sys V resource id');
        die('Error getting Sys V resource id');
    }//if

    // get the semaphore key (creating it if necessary)
    $semaphore_key = sem_get( $resource_id );
    if ( !$semaphore_key ) {
        log_debug_message('Error getting semaphore key');
        die('Error getting semaphore key');
    }//if

    // wait until we can acquire a lock on the semaphore, then continue
    $acquired_semaphore = sem_acquire( $semaphore_key );
    if ( !$acquired_semaphore ) {
        log_debug_message('Error acquiring semaphore');
        die('Error acquiring semaphore');
    }//if
}//start_exclusive_access


/** 
 * Unlock exclusive access to the current file (managed by 
 * a semaphore, and enforced by calling start_exclusive_access)
 */
function stop_exclusive_access() {
    global $semaphore_key;

    if ( $semaphore_key ) {
        sem_release( $semaphore_key );
    }//if
}//stop_exclusive_access

?>
