<?php

$connection = FisdapDatabaseConnection::get_instance();

/**
 * Function to generate a list of all the phases from the database
 *
 * @author Sam Tape stape@fisdap.net
 */
function gen_phase_list($selected_id = -1) {
	global $connection;

	$query = "SELECT * FROM PhaseTable";
	$result = $connection->query($query);

	if (count($result) <= 0) {
		echo "<option value=-1>No Data Available</option>";
	}

	foreach ($result as $phase) {
		$id = $phase['Phase_id'];
		$title = $phase['PhaseTitle'];

		if ($id == $selected_id) {
			$selected = 'selected';
		} else {
			$selected = null;
		}

		echo "<option value='$id' $selected>$title</option>";

	}
}







?>
