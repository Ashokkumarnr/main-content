<?php
/**
 * session_data_functions - Check, Get, and Set Session Values
 *
 * These functions permit session values to be set and retrieved.
 * Additionally, some session values persist from one 'session' to
 * the next as defined by a hard-coded list.
 *
 */

require_once('phputil/settings_functions.php');
require_once('phputil/session_functions.php');
require_once('phputil/classes/common_user.inc');

class SessionData
{
    /**
     * Retrieve the mapping between session vars and DB vars.
     * @return array session_var_name => db_var_name.
     */
    public static function get_stored_field_map() {
        static $map = array(
	        'class_year' => 'Class_Year',
            'class_month' => 'ClassMonth',

            'class2_year' => 'Class2_Year',
            'class2_month' => 'Class2_Month',

            'scheduler_class_year' => 'Sch_Class_Year',
            'scheduler_class_month' => 'Sch_ClassMonth'
        );

	    return $map;
    }
}

/**
* FUNCTION check_session_value - Check to see if a session variable is set, if it is not set then set it.
*/

function check_session_value($value,$value_name) {

	$stored_field_map = SessionData::get_stored_field_map();

	if (isset($value) && ($value >= -1 || $value != null)) {
		write_session_field($value_name,$value);
		if (in_array($value_name,array_keys($stored_field_map))) {
			$user = new common_user(null);
			if ($user->is_instructor()) {	
				set_user_setting($stored_field_map[$value_name],$value,$user->get_UserAuthData_idx());
			}
		}
	}
	else {
		$value = read_session_field($value_name);
		if ($value == null and in_array($value_name,array_keys($stored_field_map))) {
			$user = new common_user(null);
			if ($user->is_instructor()) {
				$value = get_user_setting($user->get_UserAuthData_idx(),$stored_field_map[$value_name]);
			}
		}
	}

	return $value;

}

/**
* FUNCTION set_session_value - Set a session variable
*/

function set_session_value($value_name,$value) {
	$stored_field_map = SessionData::get_stored_field_map();

	if (in_array($value_name,array_keys($stored_field_map))) {
		$user = new common_user(null);
		if ($user->is_instructor()) {
			set_user_setting($stored_field_map[$value_name],$value,$user->get_UserAuthData_idx());
		}
	}

	write_session_field($value_name,$value);

	return 1;

}

/**
* FUNCTION get_session_value - Get a session variable
*/

function get_session_value($value_name) {
	$stored_field_map = SessionData::get_stored_field_map();

	$value = read_session_field($value_name);

	if (in_array($value_name,array_keys($stored_field_map)) ) {
		$user = new common_user(null);
		if ($user->is_instructor()) {
			$value = get_user_setting($user->get_UserAuthData_idx(),$stored_field_map[$value_name]);
		}
	}

	return $value;

}

/**
* FUNCTION is_session_value - Is there a session value for this name?
*/

function is_session_value($value_name) {

	$stored_field_map = SessionData::get_stored_field_map();

	$isset = is_session_field($value_name);

	if (in_array($value_name,array_keys($stored_field_map)) ) {
		$user = new common_user(null);
		if ($user->is_instructor()) {
			$isset = is_user_setting($user->get_UserAuthData_idx(),$stored_field_map[$value_name]);
		}
	}

	return $isset;

}

/**
* FUNCTION delete_session_value - Remove this session value
*/

function delete_session_value($value_name) {
	$stored_field_map = SessionData::get_stored_field_map();

	delete_session_field($value_name);

	if (in_array($value_name,array_keys($stored_field_map)) ) {
		$user = new common_user(null);
		if ($user->is_instructor()) {
			delete_user_setting($user->get_UserAuthData_idx(),$stored_field_map[$value_name]);
		}
	}
}

?>
