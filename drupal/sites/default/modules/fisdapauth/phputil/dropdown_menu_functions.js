/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

function GenGenderList(SelectedGender)
{	
    var GenderList = new Array();
    GenderList[0] = "U";
    GenderList[1] = "M";
    GenderList[2] = "F";

    var i;
    var TempStr;
    for( i=0; i<3; i++)
    {	if( GenderList[i] == "U")
        {
            var menuString = "Unspecified";
        }
        else if( GenderList[i] == "M")
        {
            var menuString = "Male";
        }
        else
        {
            var menuString = "Female";
        } 
        
        TempStr = "<OPTION ";
        if( SelectedGender == GenderList[i] ) 
        {
            TempStr += "SELECTED";
        }
        TempStr += " VALUE=" + GenderList[i] + ">" + menuString;
        document.write(TempStr);
    }
}

function GenMonthList( SpecifiedMonth)
{	
    var SelectedMonth = SpecifiedMonth-1;
    var MonthList = new Array();
    MonthList[0] = "Jan";
    MonthList[1] = "Feb";
    MonthList[2] = "Mar";
    MonthList[3] = "Apr";
    MonthList[4] = "May";
    MonthList[5] = "Jun";
    MonthList[6] = "Jul";
    MonthList[7] = "Aug";
    MonthList[8] = "Sep";
    MonthList[9] = "Oct";
    MonthList[10] = "Nov";
    MonthList[11] = "Dec";

    var i;
    var TempStr;
    for( i=0; i<12; i++)
    {	TempStr = "<OPTION ";
        if( SelectedMonth == i 
            || (SelectedMonth < 0 && i == 5))
            TempStr += "SELECTED";
        TempStr += " VALUE=" + (i+1) + ">" + MonthList[i];
        document.write(TempStr);
    }
}

