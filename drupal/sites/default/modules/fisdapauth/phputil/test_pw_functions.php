<?php
/**
 * Utilities for automatically setting exam passwords
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @package default
 * @author Erik Hanson
 * @copyright 1996-2007 Headwaters Software, Inc.
 *
 */

//Define constants for the exams in question
define('BLUEExam',1);
define('BLUERe',28);
define('ERE',21);
define('ERERe',22);
define('REDExam',13);
define('OSPERe',25);
define('OSPE',18);
define('BLUEExamRe',26);
define('PC3',10001);
define('BC3',10002);
define('PC3Re',10005);
define('BC3Re',10004);
define('PC3P',10011);
define('PC3ReP',10012);
define('BC2P',10014);
define('BC2ReP',10015);

function get_test_password_for_date($date,$exam)
{
	//check to see if date is valide, checkdate arguments are month, day, year
	$date_parts = explode("-",$date);
	//make sure the date parts array has 3 elements, month, day and year
	if(sizeof($date_parts)!=3)
	{
		return false;
	}
	if(checkdate($date_parts[1],$date_parts[2],$date_parts[0]))
	{
		//Setup db connection
		$connection =& FISDAPDatabaseConnection::get_instance();
		$dbConnect = $connection->get_link_resource();
		//See if a password is already set for the date and exam specified
		$pwselect = "SELECT password FROM TestPasswordData WHERE testdate='$date' AND test_id=$exam";
		$pwresult = $connection->query($pwselect);
		//if it is, return the password
		if($pwresult[0]["password"]!==null)
		{
			return $pwresult[0]["password"];
		}
		//if it is not...
		else
		{
			//Select a new random password
			$newpwselect = "SELECT password FROM TestPasswordTable ORDER BY rand() LIMIT 1";
			$newpwresult = $connection->query($newpwselect);
			$newpw = $newpwresult[0]["password"];
			//Insert the new password into the password data table for the givin date and exam
			$newpwinsert = "INSERT INTO TestPasswordData SET testdate='$date',test_id=$exam,password='$newpw'";
			$newpwinsres = mysql_query($newpwinsert,$dbConnect);
			if($newpwinsres==true)
			{
				//if the insert works, return the new password
				return $newpw;
			}
			else
			{
				//if the insert fails, return false
				return false;
			}
		}
	}
	//if date given is not valid, return false
	else
	{
		return false;
	}
}//function

function get_created_pw_for_date($date,$exam)
{
	//check to see if date is valid, check date arguments are month, day, year
	$date_parts = explode("-",$date);
	//make sure the date parts array has 3 elements, month, day and year
	if(sizeof($date_parts)!=3)
	{
		return false;
	}
	if(checkdate($date_parts[1],$date_parts[2],$date_parts[0]))
	{
		//Setup db connection
		$connection =& FISDAPDatabaseConnection::get_instance();
		$dbConnect = $connection->get_link_resource();
		//See if a password is already set for the date and exam specified
		$pwselect = "SELECT password FROM TestPasswordData WHERE testdate='$date' AND test_id=$exam";
		$pwresult = $connection->query($pwselect);
		//if it is, return the password
		if($pwresult[0]["password"]!==null)
		{
			return $pwresult[0]["password"];
		}
		//if it is not, return -1
		else
		{
			return -1;
		}
	}
	//if date given is not valid, return false
	else
	{
		return false;
	}
}//function

?>
