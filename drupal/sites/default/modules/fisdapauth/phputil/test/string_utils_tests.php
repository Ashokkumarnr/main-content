<?php

require_once('phputil/string_utils.php');

/**
 *
 */
class StrEndsWithTester extends UnitTestCase {
    function test_str_ends_with() {
        $short_str = 'this is';

        $long_str_1 = 'this is a test ending 1';
        $long_str_2 = 'this is a test ending 2';

        $ending_1 = 'test ending 1';
        $ending_2 = 'test ending 2';

        // the right endings
        $this->assertTrue(str_ends_with($long_str_1, $ending_1));
        $this->assertTrue(str_ends_with($long_str_2, $ending_2));

        // the wrong endings
        $this->assertFalse(str_ends_with($long_str_1, $ending_2));
        $this->assertFalse(str_ends_with($long_str_2, $ending_1));

        // the too-short starting string
        $this->assertFalse(str_ends_with($short_str,  $ending_1));
        $this->assertFalse(str_ends_with($short_str,  $ending_2));
    }//test_str_ends_with
}//class StrEndsWithTester

?>
