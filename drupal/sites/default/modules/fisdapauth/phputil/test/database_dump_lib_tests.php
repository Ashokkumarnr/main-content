<?php

require_once('phputil/database_dump_lib.php');

/**
 *
 */
class DatabaseDumpLibTester extends UnitTestCase {
    function setUp() {
    }//setUp

    function test_split_where_clause() {
        $empty_clause = '';
        $single_clause = 'A=1';
        $long_clause = 'A=1 OR B=2 OR C=3 OR D=4 OR E=5';

        $threshold = 2;
        $empty_split = split_where_clause($empty_clause, $threshold);
        $single_split = split_where_clause($single_clause, $threshold);
        $long_split = split_where_clause($long_clause, $threshold);

        $this->assertEqual(
            $empty_split,
            array(),
            'error on empty clause'
        );

        $this->assertEqual(
            $single_split,
            array($single_clause),
            'error on single clause'
        );

        $this->assertEqual(
            $long_split,
            array('A=1 OR B=2', 'C=3 OR D=4', 'E=5'),
            'error on long clause'
        );
    }//test_split_where_clause
}//class DatabaseDumpLibTester

?>
