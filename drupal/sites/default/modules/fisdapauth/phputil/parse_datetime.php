<?php

/**
 * Returns an array representing the given mysql datetime string 
 *
 * @param string $datetime a datetime format string (e.g., from mysql)
 * @return array 
 */
function parse_datetime($datetime) {
    $datetime_regex = '/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/';
    $matches = array();
    preg_match($datetime_regex, $datetime, $matches);
    list($year,$month,$day,$hour,$minute,$second) = array_slice($matches,1);
    return array(
        'year'   => $year,
        'month'  => $month,
        'day'    => $day,
        'hour'   => $hour,
        'minute' => $minute,
        'second' => $second
    );
}//parse_datetime

?>
