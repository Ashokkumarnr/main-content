<?php

/**
 * This file contains helper functions to deal with 
 * HTML forms 
 */

/**
 * Returns a string of properly formatted html attribute pairs,
 * using the keys/values from the given associative array
 *
 * @param  array  $attributes  a hash of attribute name/value pairs
 * @return string contains key='value' pairs for each given attribute
 */
function get_tag_attributes( $attributes ) {
    $html = '';
    if ( $attributes ) {
        foreach( $attributes as $key=>$value ) {
            $html .= $key.'="'.$value.'" ';
        }//foreach
    }//if
    return $html;
}//get_tag_attributes


/**
 * Return a string with the html for a radio button with the given attribute 
 * pairs
 *
 * @param array $attributes  a hash of attribute name/value pairs
 * @param int   $pre_checked (optional) true iff the radio button should
 *                           default to being checked
 * @return string the radio button html
 */
function radio_button( $attributes, $pre_checked=false ) {
    $html = '<input type="radio" ';
    $html .= get_tag_attributes($attributes);
    $html .= '>';
    return $html;
}//radio_button


/**
 * Returns a string with the html for a radio group with the 
 * given attributes, name, and values, with the given index
 * checked, if any
 */
function radio_group( $name, $common_attributes, $values, $checked_index=-1 ) {
}//radio_group


/**
 * Returns the html for a yes-no radio group with the given name
 * 
 * @param  string $name     the name to give the radio group
 * @param  int    $selected (optional) the index to select by default 
 * @return string           the html for the generated radio group
 */
function yes_no_radio_group( $name, $selected=-1 ) {
    $html = radio_button( 
        array( 
            'name'=>$name,
            'value'=>'1'
        ),
        ($selected === 0)
    );
    $html .= 'yes';

    $html .= radio_button( 
        array( 
            'name'=>$name,
            'value'=>'0'
        ),
        ($selected === 1)
    );
    $html .= 'no';
    return $html;
}//yes_no_radio_group


/**
 * Display a checkbox with the given attribute pairs
 *
 * @param array $attributes  a hash of attribute name/value pairs
 * @param int   $pre_check   (optional) true iff the checkbox should
 *                           default to being checked
 * @return string the checkbox html
 */
function checkbox( $attributes, $pre_check=false ) {
    $html = '<input type="checkbox" ';
    $html .= get_tag_attributes($attributes);
    $html .= '>';
    return $html;
}//checkbox


/**
 * Display a text input with the given attribute pairs
 *
 * @param array  $attributes   a hash of attribute name/value pairs
 * @param string $default_text (optional) the initial text to display in the box
 * @return string the text input html
 */
function text_input( $attributes, $default_text='' ) {
    $html = '<input type="text" ';
    $html .= get_tag_attributes($attributes);
    $html .= '>';
    return $html;
}//text_input

?>
