<?php

// these constants map to the ValidStatus field in the Item_def table
define('UNDETERMINED_ITEM_VALIDITY',  '0');
define('FAILED_ITEM_VALIDITY',        '1');
define('NEEDS_REVISION_ITEM_VALIDITY','2');
define('VALIDATED_ITEM_VALIDITY',     '3');

?>
