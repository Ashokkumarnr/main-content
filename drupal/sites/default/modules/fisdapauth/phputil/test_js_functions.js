/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 **/

function deleteEvent(id, date, name) {
	var confirmMsg = "Are you sure you want to delete the " + name + " scheduled for " + date + "?";
	confirmMsg = confirmMsg + "\nAny student scores associated with this test will NOT be deleted.";

	if(confirm(confirmMsg)) {
		document.deleteform.do_delete.value=1;
		document.deleteform.delete_test_id.value=id;
		document.deleteform.submit();
	}
}

function post_to_sendScores(emailTo, subject, message) {
	document.emailForm.emailTo.value = emailTo;
//alert(document.emailForm.emailTo.value);
	document.emailForm.subject.value = subject;
//alert(document.emailForm.subject.value);
	document.emailForm.message.value = message;
//alert(document.emailForm.message.value);
	document.emailForm.submit();
}
