<?php

/**
 * From the PHP manual, mimics PHP5's version of microtime
 * that returns a float.
 */
function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}//microtime_float

?>
