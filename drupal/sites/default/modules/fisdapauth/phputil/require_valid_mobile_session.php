<?php
	require_once('phputil/session_config.php');
	// Let's add the Zend and Graph includes to our path
	$zf = FISDAP_ROOT . '/phputil/Zend/library';
	$graph = FISDAP_ROOT . '/phputil/Structures_Graph';
	set_include_path(get_include_path() . PATH_SEPARATOR . $zf . PATH_SEPARATOR . $graph);

	// Now set up the error handler
    require_once('phputil/classes/FisdapErrorHandler.inc');
    FisdapErrorHandler::init();
	// And load up the DB connection handler
    require_once('phputil/classes/FISDAPDatabaseConnection.php');

    require_once('phputil/session_functions.php');

    //check to see if we're trying to access a mobile
    require_valid_session(false,true);
?>
