<?php

$connection = FisdapDatabaseConnection::get_instance();

/**
 * Function to generate a list of all the LOC options from the database
 *
 * @author Sam Tape stape@fisdap.net
 */
function gen_preceptor_list($site_id,$program_id,$selected_id = -1) {
	global $connection;

	$query = "SELECT * FROM PreceptorData pd, ProgramPreceptorData ppd WHERE pd.Preceptor_id = ppd.Preceptor_id AND Program_id = $program_id AND AmbServ_id = $site_id AND ppd.Active = 1 ORDER BY LastName";
	$result = $connection->query($query);

	if (count($result) <= 0) {
		echo "<option value=-1>No Data Available</option>";
	} else {
		echo "<option value=-1>Select One</option>";
	}

	foreach ($result as $preceptor) {
		$id = $preceptor['Preceptor_id'];
		$full_name = $preceptor['LastName'].", ".$preceptor['FirstName'];

		if ($id == $selected_id) {
			$selected = 'selected';
		} else {
			$selected = null;
		}

		echo "<option value='$id' $selected>$full_name</option>";

	}
}

function gen_preceptor_array($site_id,$program_id) {
	global $connection;

	$query = "SELECT * FROM PreceptorData pd, ProgramPreceptorData ppd WHERE pd.Preceptor_id = ppd.Preceptor_id AND Program_id = $program_id AND AmbServ_id = $site_id AND ppd.Active = 1 ORDER BY LastName";
	$result = $connection->query($query);

	return $result;
}





?>
