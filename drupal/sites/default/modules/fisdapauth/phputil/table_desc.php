<?php

/**
 * This file contains the table_desc() function, which is a simple
 * wrapper for the mysql DESC statement.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @package default
 * @author Sam Martin <smartin@fisdap.net>
 * @copyright 1996-2007 Headwaters Software, Inc.
 *
 */


/**
 * Returns an array of associative arrays containing the result of the
 * SQL statement "DESC <tablename>"
 *
 * @param string $table the name of the table to describe
 * @return array an array of associative arrays containing the result
 *         set of the DESC query
 */
function table_desc($table) 
{
    // sanitize the table name (valid chars: a-z, 0-9, _)
    if ( !preg_match('/^[a-zA-Z0-9_]+$/',$table) ) 
    {
        return false;
    }//if
    $connection =& FISDAPDatabaseConnection::get_instance();
    $query = "DESC $table";
    $result = $connection->query($query);
    return $result;
}//table_desc

?>
