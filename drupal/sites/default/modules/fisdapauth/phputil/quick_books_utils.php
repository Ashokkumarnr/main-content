<?php 

/**
 * These functions are used to produce files containing transaction data
 * that can be improted into Quickbooks.  The order they are used is 
 * as follows:
 *    QB_OpenFile             - Open the output file
 *    QB_WriteHeaders      - Creates the file and writes the header.
 *    QB_WriteTransaction - Writes the transaction line.
 *    QB_WriteDistribution - Writes a distribution for the transaction.
 *    QB_EndTransaction    - Ends the transaction.
 *    QB_CloseFile            - Writes the footer and closes the file.
 *    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @package default
 * @author Mike Johnson
 * @copyright 1996-2007 Headwaters Software, Inc.
 *
 */

 
 /** the acount for receiving funds from Credit Card transactions */
define('QB_CREDIT_CARD_ACCOUNT', 'Credit Card Receivables');

 /** Items that can be purchased */
define('QB_ITEM_TRACKING_ALS',				'FA-FISDAP ALS');
define('QB_ITEM_TRACKING_BLS',				'FB-FISDAP BLS');
define('QB_ITEM_SCHEDULER_ALS', 			'SA-Scheduler ALS Add-on');
define('QB_ITEM_SCHEDULER_BLS', 			'SB-Scheduler BLS Add-on');
define('QB_ITEM_SCHEDULER_UNLIMITED', 'SSA-Scheduler Stand-Alone ALS');
define('QB_ITEM_SCHEDULER_LIMITED',	'SSB-Scheduler Stand-Alone BLS');
define('QB_ITEM_PDA',					 		'PA-PDA Access');
define('QB_ITEM_STUDYTOOLS_ALS','TA-Study Tools Access');

/** Sales accounts for booking splits */
define('QB_SALES_TRACKING_ALS', 'FISDAP Sales:Skills Tracking Sales:EMT ALS Access');
define('QB_SALES_TRACKING_BLS', 'FISDAP Sales:Skills Tracking Sales:EMT BLS Access');
define('QB_SALES_SCHEDULER_ALS', 'FISDAP Sales:Scheduler Sales:ALS Add-on');
define('QB_SALES_SCHEDULER_BLS', 'FISDAP Sales:Scheduler Sales:BLS Add-on');
define('QB_SALES_SCHEDULER_UNLIMITED', 'FISDAP Sales:Scheduler Sales:Unlimited');
define('QB_SALES_SCHEDULER_LIMITED', 'FISDAP Sales:Scheduler Sales:Limited');
define('QB_SALES_PDA', 'FISDAP Sales:PDA Sales:PDA Access');
define('QB_SALES_TESTING_BASIC', 'FISDAP Sales:Testing:Basic');
define('QB_SALES_TESTING_PARAMEDIC', 'FISDAP Sales:Testing:Paramedic');
define('QB_SALES_STUDYTOOLS_ALS','FISDAP Sales:Study Tools Sales:Study Tools Access');


 /**
 * Open the file to wirte the transaction.
 *
 * @param string $FileName The name of the file to be opened.
 * 
 */

    function QB_OpenFile(  $FileName )
    {
        
           
        if (!($FilePntr = fopen( $FileName, 'w')))
        {    return NULL;
        }
        return $FilePntr;
    }
 
    
    
/**
 * Write the transaction Headers to the open file.
 *
 * @param mixed $FilePnter Points to open file ready for writing. 
 * 
 */

    function QB_WriteHeaders(  $FilePntr )
    {
        /*  Write the transaction Heading  */
        fwrite( $FilePntr, "!TRNS	TRNSID	TRNSTYPE	DATE	ACCNT	");
        fwrite( $FilePntr, "NAME	CLASS	AMOUNT	DOCNUM	MEMO	");
        fwrite( $FilePntr, "CLEAR	TOPRINT	NAMEISTAXABLE	ADDR1	");
        fwrite( $FilePntr, "PAYMETH	ADDR3	ADDR4	ADDR5	DUEDATE	");
        fwrite( $FilePntr, "TERMS\n");

        /* Write the Distribution Heading   */
        fwrite( $FilePntr, "!SPL	SPLID	TRNSTYPE	DATE	ACCNT	");
        fwrite( $FilePntr, "NAME	CLASS	AMOUNT	DOCNUM	MEMO	");
        fwrite( $FilePntr, "CLEAR	QNTY	PRICE	INVITEM	PAYMETH	");
        fwrite( $FilePntr, "TAXABLE	VALADJ	REIMBEXP	SERVICEDATE	");
        fwrite( $FilePntr, "OTHER2\n");
        
        /* End the Transaction Heading */
        fwrite( $FilePntr, "!ENDTRNS\n");
        
    }

    
    
    /**
     * Writes the lead line of the transaction.
     *
     * @param mixed $FilePnter Points to open file ready for writing.
     * @param mixed $Date Transaction date format: 1/11/2007
     * @param mixed $Account The account where funds will be going in QB.
     * @param mixed $Customer The customer number and Name. IE. '401-Inver Hills'.
     * @param mixed $Ammount The total ammount of the transaction.
     * @param mixed $VTransId Verisign transaction id.
     * @param mixed $PaymentType Visa, Mastercard... may be empty string.
     *
     */

    function QB_WriteTransaction( $FilePntr, $Date, $Account, $Customer, 
        $Ammount, $VTransId, $PaymentType)
    {
        fwrite( $FilePntr, "TRNS		CASH SALE	$Date	$Account	");
        fwrite( $FilePntr, "$Customer		$Ammount		$VTransId	N	N	");
        fwrite( $FilePntr, "N		$PaymentType				$Date\n");
    }
    
    
    
     /**
     * Writes one distribution line of the transaction.  There may be multiple
     * distribution lines for each transaction.  These tell what made up the 
     * transaction.  For example if a student purchased an account that 
     * included both tracking and scheduler, they would each have their own
     * distribution line.
     *
     * @param $FilePnter Points to open file ready for writing.
     * @param $Date Transaction date format: 1/11/2007
     * @param $ItemName The name of the item in QB, ie. FA-FISDAP ALS     
     * @param $Price The price or each of the item.
     * @param $Quanity The number of items purchased.
     * @param $SalesAccount The QB account where the sale will be booked.
     *
     */
    function QB_WriteDistribution( $FilePntr, $Date, $ItemName, $Price, 
          $Quanity, $SalesAccount)
    {
        $Amount = $Price * $Quanity;
        
        fwrite( $FilePntr, "SPL		CASH SALE	$Date	$SalesAccount			");
        fwrite( $FilePntr, "-$Amount			N	-$Quanity	$Price	$ItemName");
        fwrite( $FilePntr, "		N	N	NOTHING	0/0/0\n");        
    }

    
    
    
    /**
     * Write the transaction End statment.
     *
     * @param mixed $FilePnter Points to open file ready for writing. 
     * 
     */
    function QB_EndTransaction( $FilePntr)
    {
        fwrite( $FilePntr, "ENDTRNS\n");
    }

    
    
    
     /**
     * Close the file.
     *
     * @param mixed $FilePnter Points to open file ready for writing. 
     * 
     */   
    function QB_CloseFile( $FilePntr)
    {
        fclose( $FilePntr);
    }


?>
