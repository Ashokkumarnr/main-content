<?php

require_once("Transaction.inc");
require_once('PreceptorTransaction.inc');
require_once("WorkshopTransaction.inc");
require_once("phputil/classes/common_page.inc");
require_once("utils.php");
require_once("phputil/price.php");

/**
   basic logic:

   this can be called by verisign or by core once things are finished, depending
   on whether we had an invoice or a credit transaction.

   to review, the paths we take to get here:
   interfaces --POST> verisign (--POST> core) --POST> this page
   OR
   interfaces --POST> core -->this page

   verisign will post information, and we can process things from there.
   core can't send information since we're just doing a header redirect, so our dirty
   work around hack is to put the current trans ID somewhere standard in their 
   session. we'll always know where to look to get it. then we can access the 
   acct_ session value using that info.

   from there on, we just use the object to get the information we need (first
   checking for an error and displaying that screen if necessary, then going on to
   put up success info.

   so that the object doesn't try to redisplay or reprocess or anything, we have to
   do a few things to tie up loose ends:
   set the finished flag on the object.
   write to the file to tell it we're done, as well as dumping the variables in
   there so we have a record of what went down.
   clear the trans ID on the session, if it exists.

 */

$thisPage = new common_page("Upgrade Confirmation", null);
$thisPage->add_stylesheetsrc("../../admin/orderpages.css");

// Get the object for CREDIT CARD transaction

if (isset($_POST["USER1"])) {
	$tmp_id = $_POST["USER1"];
	// Check all files for this transaction for errors, since it's likely they 
	// won't have seen an error screen for previous errors.
	$i = 2;
	while (file_exists(get_file_name($tmp_id))) {
		$id = $tmp_id;
		if (read_acct_data($id, "error0") && !read_acct_data($id, "displayed")) {
			if (read_acct_data($id, "displayable-error")) {
				$displayably_errored = true;
			} else {
				$errored = true;
			}
		}
		$tmp_id = substr($id, 0, 17) . "_$i";
		$i++;
	}

	$thisTrans = read_acct_data($id, "transaction_object");
	if ($thisTrans == null) {
		reportError("The transaction file does not contain a value for 'transaction_object'.");
		print_no_admittance_message();
	}
	if (!($thisTrans instanceof Transaction)) {
		reportError("The object stored in the file is not a child of the Transaction class.");
		print_no_admittance_message();
	}
} else {
	// Get the object for INVOICE transaction
	
	$id = get_session_value("Account_Ordering_FISDAP_ID");
	//echo "id: $id<br>";
	$serializedTrans = get_session_value("acct_" . $id);
	if ($serializedTrans == null) {
		reportError("There was no transaction stored in the session. Nowhere left to go from there.");
		print_no_admittance_message();
	}
	$thisTrans = unserialize($serializedTrans);
	if ($thisTrans == false) {
		reportError("Unable to be unserialize the object. The retrieved string was \"$serializedTrans\".");
		print_no_admittance_message();
	}
	//var_export($thisTrans);
	if (!($thisTrans instanceof Transaction)) {
		reportError("The object stored in the session is not a child of the Transaction class.");
		print_no_admittance_message();
	}
}

// See if there's an error and decide if you should display prettiness or display the error

if (!$thisTrans->is_processed()) {
	reportError("The transaction has not been processed yet.");
	print_no_admittance_message();
}//end "if (is finished)"

if ($thisTrans->already_displayed()) {
	echo "<center>This page has expired.<br>";
	echo "<p>Your FISDAP account or upgrade order has already been processed. ";
	echo "Please check your email for a verification email. <br><br> ";
	echo "If you continue to experience difficulty, please email ";
	echo "support@fisdap.net and supply ";
	echo "them with your name, your program name, and the following information: </p>";
	echo "<ul><li> Fisdap ID: ";
	if ($id) {
		echo "$id\n";
	} else {
		echo "99999\n";
	}
	$vid = $thisTrans->get_verisign_id();
	if ($vid) echo "<li>Verisign ID: $vid\n";
	echo "</ul>";

	echo "</center>";
	clean_up();
	exit();
}//end "if (was displayed)"

 
// We display an error message if the current transaction has an error associated with it. 
// We also check the $errored variable to see if any previous attempts on this ID had an 
// error. If there was an error, we check to see if they get the displayable error message
// or the generic one. Non-displayable errors override displayable ones, so 
// if the displayable error was on a previous attempt, we make sure that there wasn't a non-
// displayable one on a previous attempt, also.

if ($errored == true) {
	echo "<center class='mediumtext'>";
	echo "<p>We were unable to complete your transaction at this time.</p>";
	echo "<p>Please contact FISDAP support at support@fisdap.net and supply ";
	echo "them with your name, your program name, and the following information: </p>";
	echo "<ul><li> Fisdap ID: ";
	if ($id) {
		echo "$id\n";
	} else {
		echo "99999\n";
	}

	$vid = $thisTrans->get_verisign_id();
	if ($vid) echo "<li>Verisign ID: $vid\n";
	echo "</ul>";
	echo "<p>We apologize for an inconvenience this may have caused.</p>";
	echo "</center>";
	clean_up();
	die();
} else if ($displayable_error) {
	echo "<center class='mediumtext'>";
	echo "<h4>Credit Transaction Declined.</h4>";
	echo "<p>We were unable to complete your transaction because your credit card";
	echo "could not be processed. Some common reasons for this include:</p>\n";
	echo "<ul>\n";
	echo "<li>The name, address, and/or zip code provided do not match the information on the card.</li>\n";
	echo "<li>Your bank declined the transaction due to insufficient funds.</li>\n";
	echo "<p>Please correct these errors and try your transaction again. NOTE TO DEBIT CARD USERS: Some banks ";
	echo "will place a hold on funds, even if the transaction isn't approved. We suggest that you";
	echo "check your available funds before reordering.<p>\n";
	if ($UserType == "instructor") {
		$url = FISDAP_WEB_ROOT . 'admin/instAccountsUpgrade.php';
	} else {
		$url = FISDAP_WEB_ROOT . 'admin/studentAccountsUpgrade.php';
	}
	echo "<p>Please <a href='$url'>go back and try again.</a></p>\n"; 
	echo "<p>If you are still having trouble, please contact FISDAP Support at support@fisdap.net";
	echo "and supply them with your name, your program name, and the following information:<\p>\n";
	echo "<blockquote>\n";
	if ($id) {
		echo "$id\n";
	} else {
		echo "99999\n";
	}

	$vid = $thisTrans->get_verisign_id();
	if ($vid) echo "<li>Verisign ID: $vid\n";
	echo "</blockquote>\n";
	clean_up();
	die();
}//end "if (was error)"

// If we're reached this point, everything is happy happy, joy joy! 
// The transaction went through core.php, it passed Verisign if that was necessary, 
// and nothing went wrong. We just need to display a success screen. 

$TotalCost = $thisTrans->get_total_price();
$UserType = $thisTrans->get_usertype();
$EmailAddress = $thisTrans->get_email_address();

if ($thisTrans instanceof PreceptorTransaction) {
	$isPreceptor = true;
	if ($thisTrans->is_upgrading_own_account()) {
		$isUpgradeOwn = true;
	} else {
		$isUpgradeOwn = false;
	}
}

if ($thisTrans instanceof NewTransaction || ($isPreceptor && $thisTrans->is_new_transaction())) {
	$isNew = true; 
} else if ($thisTrans instanceof UpgradeTransaction || ($isPreceptor && $thisTrans->is_upgrade_transaction())) {
	$isUpgrade = true;
}

if ($isNew) {
	$Quantity = $thisTrans->get_number_of_accounts();
	$AccountType = $thisTrans->get_acct_type();
	$Configuration = $thisTrans->get_config_code();
	$includesTesting = get_access_by_configuration($Configuration, 'testing');
} else if ($isUpgrade) {
	$upgradesArray = $thisTrans->get_upgrades();
}

// print the page for new transactions

if ($isNew) {
?>

		<html>
		<head>
		<title>Serial Numbers</title>
		<link rel='stylesheet' type='text/css' href='../../common.css'>
		<link rel='stylesheet' type='text/css' href='../../admin/orderpages.css'>
		<!--[if lte IE 6]>
		<link rel='stylesheet' type='text/css' href='../../admin/ie_orderpages.css'>
		<![endif]-->
		</head>

		<BODY style="text-align: center">
		<div class='content'>
		<h1>Thank You!</h1>

		<p class='mediumtext'>You have ordered:</p>

<?php
	echo "<center class='mediumtext'>\n";

	// display the order summary box
	
	if ($Quantity == 1) {
		$SNstr = "serial number";
		$SNverb = "has";
		echo "<div class='product_description'><span class='emphasis'>\n";
		echo "$Quantity ".account_type_display($AccountType)." account with:<br> ";
	} else {
		$SNstr = "serial numbers";
		$SNverb = "have";
		echo "<div class='product_description'><span class='emphasis'>\n";
		echo "$Quantity ".account_type_display($AccountType)." accounts with:<br> ";
	}

	$productArray = get_product_description($Configuration, 'array');
	foreach ( $productArray as $product ) {
		echo "$product<br>";
	}
	echo "</span><br>";
	printf("Total cost: <span class='emphasis'>\$%.2f</span></div>\n", $TotalCost);
	echo "</center>\n";

	// Diplay message
	
	echo "<div class='mediumtext'>\n";
	echo "Your $SNstr $SNverb been generated and emailed to you at $EmailAddress. ";
	echo "Please check your email for this message.<br><br>\n";

	if ($UserType == 'student' || ($isPreceptor && $thisTrans->is_buying_own_account())) {
		//		echo "The email message will contain your serial number and a link.  Go to this link ";
		//		echo "to complete the setup of your FISDAP account. ";
		echo "The email will serve as your receipt.<br><br>\n";
		if ($includesTesting) {
			echo "<span style='color:red;'>\n";
			echo "Your new account includes access to FISDAP Testing. ";
			echo "We STRONGLY recommend that you set up your ";
			echo "FISDAP account AT LEAST 72 hours prior to the scheduled test date.\n";
			echo "<br><br>\n";
			echo "</span>\n";
		}
		$stu_numbers = $thisTrans->get_serNumbersArray();
		$stu_number = $stu_numbers[0];
		if ($isPreceptor) {
			$url = FISDAP_WEB_ROOT . "addstudent.html?SerialNumber=".$stu_number."&Instructor=true&Submit=SerialNumber";
		} else {
			$url = FISDAP_WEB_ROOT . "addstudent.html?SerialNumber=".$stu_number."&Submit=SerialNumber";
		}
		echo "<br><center><font size=+1><a href='$url'>Set up your account now.</a></font></center><br><br>\n";
	} else if ($UserType == 'instructor') {
		$person = ($isPreceptor) ? 'instructor' : 'student';
		echo "The email message will contain your $SNstr and transaction details. ";
		echo "Assign one serial number per $person, then direct $person";
		echo "s to set up their accounts by clicking on the ";
		if ($isPreceptor) echo "'I am an Educator' link ";
		else echo "'I am a Student' link ";
		echo "on the FISDAP homepage.<br><br>\n";
		if ($includesTesting) {
			echo "<span style='color:red;'>\n";
			echo "Your new account(s) include access to FISDAP Testing. ";
			echo "We STRONGLY recommend that your students set up their ";
			echo "FISDAP accounts AT LEAST 72 hours prior to the scheduled test date.";
			echo "<br><br>\n";
			echo "</span>\n";
		}
	}
	//	echo "If you do not receive the email within 24 hours please contact ";
	//	echo "<A href='mailto:support@fisdap.net'>support@fisdap.net</a>.<br><br>\n";

	echo "</div>\n";
} else if ($isUpgrade) {
	// Or print the page for an upgrade transaction

	// print out the upgrade summary
	$thisPage->display_header();

	echo "<div class='content mediumtext'>\n";
	echo "<h1>Thank You!</h1>\n";
	echo "<p>You have ordered the following upgrades:</p>\n";
	if ($UserType == 'instructor' && !$isUpgradeOwn) {
		echo "<center>\n";
		echo "<div class='product_description' style='text-align:left;'>\n";
		foreach ($upgradesArray as $stu_uname=>$code) {
			$description = get_product_description($code);
			$userObj = new common_user($stu_uname);
			if ($isPreceptor) {
				$indivObj = new common_instructor($userObj->get_instructor_id());
			} else {
				$indivObj = new common_student($userObj->get_student_id());
			}	
			$indiv_name = $indivObj->get_firstname()." ".$indivObj->get_lastname();
			echo "$indiv_name: $description<br>\n";
		}
		echo "</div>\n";
		echo "</center>\n";
	} else {
		$description = get_product_description(current($upgradesArray));
		echo "<center>\n";
		echo "<span class='emphasis'>$description</span>\n";
		echo "</center>\n";
		echo "<br><br>\n";
	}

	// Diplay message
	
	echo "<div>\n";
	echo "Your upgrades have been processed, and a confirmation email has been ";
	echo "sent to you at $EmailAddress. ";

	if ($UserType == 'student' || ($isPreceptor && $thisTrans->is_buying_own_account())) {
		echo "The email will also serve as your receipt.<br><br>\n";
	} else if ($UserType == 'instructor' && !$isUpgradeOwn) {
		$persons = ($isPreceptor) ? 'instructors' : 'students';
		echo "Each of the $persons listed above have also been emailed to notify them ";
		echo "of the upgrades to their accounts.";
		echo "<br><br>\n";
	}
	
	if ($isUpgradeOwn) {
		$link = PreceptorUtils::get_preceptor_training_url();
		echo "You can access Clinical Educator Training <a href='$link'>here.</a><br><br>";
	}

	echo "</div>\n";
} else if ($thisTrans instanceof WorkshopTransaction) {

	// print out the workshop transaction summary
	
	$thisPage = new common_page("Workshop Transaction",null);
	$thisPage->add_stylesheetsrc("../../admin/orderpages.css");
	$thisPage->display_header();

	echo "<div class='content'>\n";
	echo "<h1>Thank You!</h1>\n";
	echo "<p>You are now registered for the following workshop:</p>\n";

	echo "<center>\n";
	echo "<div class='product_description' style='text-align:center;'>\n";
	$location = $thisTrans->get_location();
	$duration = $thisTrans->get_duration() -1;
	if ($duration > 0) {
		$date1 = date("F j-",strtotime($thisTrans->get_workshop_date()));
		$date2 = date("j, Y",strtotime($thisTrans->get_workshop_date()."+$duration days"));
		$workshop_date = $date1.$date2;
	} else {
		$workshop_date = date("F j, Y",strtotime($thisTrans->get_workshop_date()));
	}
	//$workshop_date = date("F j, Y",strtotime($thisTrans->get_workshop_date()));
	echo $workshop_date.' - '.$location;
	echo "</div>";
	echo "</center>";

	echo "<div>\n";
	echo "A confirmation email has been sent to you at ".$thisTrans->get_email().".<br>";
	echo "</div>";

} else {
	// Otherwise, they have a weird transaction going on (neither new nor upgrade)

	reportError('Object wasn\'t a recognized Transaction type, so I don\'t know how to handle it. If it existed at all, the object was of type ' . get_class($thisTrans) . '.');
	print_no_admittance_message();
}

// Close the page
if (!($thisTrans instanceof NewTransaction && $UserType == 'student')
	&& !($isPreceptor && $thisTrans->is_buying_own_account())) {
		echo "<div>\n";
		echo "<center>\n";
		echo "<a href='".FISDAP_WEB_ROOT."index.html?target_pagename=admin/index.html' target='_top' class='mediumtext'>Return to Admin</a>\n";
		echo "</center>\n";
		//echo "<br>FISDAP TRANSACTION ID: $id<br>";
		echo "</div>\n";
	}
echo "</div>\n";
echo "</BODY>\n";
echo "</html>\n";
clean_up();

// END MAINLINE 


/**
 * tells the user they got here by mistake and gently leads them elsewhere
 */
function print_no_admittance_message() {
	global $id;
	global $thisTrans;
	global $thisPage;

	if ($thisTrans) {
		$vid = $thisTrans->get_verisign_id();
	}

	$thisPage->display_header();
	echo "<center class='mediumtext'>You have reached this page in error.<br>";
	if ($id === null) {
		echo "If you would like to order a FISDAP account or ";
		echo "upgrade an existing account, please view our help section ";
		echo "for instructions.<br><br>";
		echo "If you continue to experience difficulty, please email ";
		echo "support@fisdap.net";
	} else {
		echo "<p>Please contact FISDAP support at support@fisdap.net and supply ";
		echo "them with your name, your program name, and the following information: </p>";
		echo "<p>Fisdap ID: $id</p>\n";
		if ($vid) { 
			echo "<p>Verisign ID: $vid</p>\n";
		}
		echo "<p>We apologize for an inconvenience this may have caused.</p>";
	}
	//DEBUG	echo "<pre>Post:\n"; print_r($_POST); echo "</pre>";
	echo "</center>";
	$thisPage->display_footer();
	clean_up();
	die();
}

/**
 * A util function to call error_dump.
 */
function reportError($error_msg) {
	global $id;

	if ($id === null) {
		$id = 'results_'.time();
	}
	error_dump($id, "order_results.php", $error_msg);
}

function clean_up() {
	global $id;
	global $thisTrans;

	if ($id != null) {
		write_acct_data($id, "displayed", date("D M j G:i:s T Y"));
	}
	if ($thisTrans != null) {
		$thisTrans->set_displayed();
		write_acct_data($id, "final_object", $thisTrans);
	} else {
		write_acct_data($id, "final_object", "no object found");
	}

	session_unregister("Account_Ordering_FISDAP_ID");
	unset($_SESSION["Account_Ordering_FISDAP_ID"]);
	unset($Account_Ordering_FISDAP_ID);
	session_unregister("acct_$id");
	unset($_SESSION["acct_$id"]);
	unset(${"acct_$id"});
}
?>
