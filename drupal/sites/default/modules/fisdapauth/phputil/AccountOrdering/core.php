<?php

require_once("utils.php");
require_once("Transaction.inc");
require_once("genSerialNumber.php");
require_once("../session_data_functions.php");
require_once("quickbooks_utils.php");
require_once("../dbconnect.html");
require_once("WorkshopTransaction.inc");
require_once('PreceptorTransaction.inc');

/**
 * PHP CORE FOR ACCOUNT ORDERS
 *
 * FIVE HIGH-LEVEL STEPS FOR THIS PAGE:
 *
 * 1. Process the incoming data. Decide where to find the transaction object and maybe add POST info to it.
 *	This probably makes use of protected functions-- get_transaction_type(), process_upgrade(), etc etc.
 *	This step is also responsible for verifying that the information we get makes sense. For example, 
 *	that a file has been created for the user on our drive, that the totals in the file and from Verisign 
 *	match, and so on.
 *	The object stores information and methods so that we don't have to think about implementation.  
 *	Except for the ability to write to the user's file when an error occurs, no methods will have
 *	side effects or will echo/write to stdout.
 * 2. Perform the main transaction action. At this time, that means upgrading or generating serial numbers.
 * 3. Write a quickbooks file.
 *	Creates the Quickbooks formatted file for Mike. If we don't need a Quickbooks file for the particular
 *	transaction, it will just return null.
 * 4. Email everyone involved
 *	Since we don't want this page to have to think about who needs to be emailed, this will be one call
 *	that's something like "object->send_email_to_all_involved()". The object will decide who needs emails
 *	and generate those emails.
 * 5. Generate HTML and display a success page
 *	The transaction objects are not allowed to have side effects, so this page will have to ask the objects
 *	for whatever information it needs and then contain the logic to display an appropriate page.
 *      Since this page has to exit to Verisign, this page won't do the displaying. What it can do is put 
 *      information in the acct_ file so the display page can look there and show the right thing. 
 *
 * Awesome things:
 *	No need for global variables!
 *	This core is generic enough that we can create new types of transactions in the future
 *		(No, I can't think of any either, but if we thought of everything we needed before
 *		we needed it, we wouldn't have to rewrite this right now.)
 *	Even though this core is pretty high level abstract, it is in control of everything that happens.
 *		That is, everything that changes or displays something is triggered by this page. As
 *		long as we keep rigid about pre and post conditions, this will be like butter to debug
 *		and maintain down the road. (The exception to this is when another script encounters an error
 *      that warrants calling reportError. This script is still the only one that can call 
 *      reportErrorAndDie().)
 *	
 **/

$id = get_passed_id();
if ($id == null) {
	reportErrorAndDie(
		"A valid transaction id was not supplied. Processing of the " .
		"entire transaction has been stopped and nothing has been completed: " .
		"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
		"emails were never sent out. However, if they went through Verisign, " .
		"they may have been charged. This transaction requires close attention." 
	);
}
write_acct_data($id, "core_id", "core.php found the id");

write_acct_data($id, "post_data", $_POST);

$method = get_passed_type();
if ($method == null) {
	reportErrorAndDie(
		"A valid transaction method was not supplied. Processing of the " .
		"entire transaction has been stopped and nothing has been completed: " .
		"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
		"emails were never sent out. However, if they went through Verisign, " .
		"they may have been charged. This transaction requires close attention."
	);
}
write_acct_data($id, "core_type", "core.php found the method: $method");

if ($method == "credit") { 
	$thisTrans = read_acct_data($id, "transaction_object");

	if (already_been_futzed_with()) {
		change_transaction_file();
	}
	write_acct_data($id, "object-found", "core-credit");

	if ($thisTrans == null) {
		reportErrorAndDie(
			"The transaction file does not contain a value for 'transaction_object'. Processing of the " .
			"entire transaction has been stopped and nothing has been completed: " .
			"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
			"emails were never sent out. However, if they went through Verisign, " .
			"they may have been charged. This transaction requires close attention."
		);
	}
	if (!($thisTrans instanceof Transaction)) {
		reportErrorAndDie(
			"The object stored in the file is not a child of the Transaction class. Parent: ". get_parent_class($thisTrans) .
			". Processing of the " .
			"entire transaction has been stopped and nothing has been completed: " .
			"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
			"emails were never sent out. However, if they went through Verisign, " .
			"they may have been charged. This transaction requires close attention."
		);
	}
	if ($_POST["RESULT"] != 0) {
		write_acct_data($id, "displayable-error", "credit declined");
		reportErrorAndDie(
			"The credit card transaction was not approved. Verisign error " .  $_POST["RESULT"] . ": " . $_POST["RESPMSG"] . " . " .
			"Processing of the entire transaction has been stopped and nothing has been completed: " .
			"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
			"emails were never sent out."
		);
	} else if ($_POST["RESPMSG"] == "AVSDECLINED") {
		write_acct_data($id, "displayable-error", "AVS declined");
		reportErrorAndDie(
			"The credit card transaction was approved, but then voided because the AVS check did not pass. This " . 
			"means that the AVS security declined this transaction because the address, zipcode, and/or state provided didn't " .
			"match the card. AVS Code: '" . $_POST["AVSDATA"] . "'. " .
			"Processing of the entire transaction has been stopped and nothing has been completed: " .
			"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
			"emails were never sent out, but that's probably what should have happened."
		);
	} else if ($_POST["RESPMSG"] == "CVSDECLINED") {
		write_acct_data($id, "displayable-error", "CVS declined");
		reportErrorAndDie(
			"The credit card transation was approved, but then voided because the CVS check did not pass. This means " .
			"the customer wasn't able to correctly provide that little code on the back of the card. " .
			"Processing of the entire transaction has been stopped and nothing has been completed: " .
			"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
			"emails were never sent out, which is probably what should have happened."
		);
	} else {
		write_acct_data($id, "credit_status", $_POST["RESULT"] . ": " . $_POST["RESPMSG"]);
	}
	$thisTrans->set_verisign_id($_POST["PNREF"]);
	if ($thisTrans->get_total_price() != $_POST["AMOUNT"]) {
		reportErrorAndDie(
			"Price mismatch. Verisign charged them " . $_POST["AMOUNT"] . 
			", but the object says to charge them " . $thisTrans->get_total_price() . 
			". Someone could be trying to scam us. Processing of the " .
			"entire transaction has been stopped and nothing has been completed: " .
			"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
			"emails were never sent out. However, if they went through Verisign, " .
			"they may have been charged. This transaction requires close attention."
		);
	}
	if (($thisTrans->get_usertype() == "student" && $thisTrans instanceof NewTransaction)
		|| ($thisTrans instanceof PreceptorTransaction && $thisTrans->is_buying_own_account())) {

			$thisTrans->set_email_address($_POST["EMAIL"]);
		}
	$thisTrans->set_cred_name(stripslashes($_POST["NAME"]));
} else if ($method == "invoice") {
	$thisTrans = get_session_value("acct_$id"); 
	$thisTrans = unserialize($thisTrans);

	if (already_been_futzed_with()) {
		change_transaction_file();
	}
	write_acct_data($id, "object-found", "core_invoice");
	if (!($thisTrans instanceof Transaction)) {
		reportErrorAndDie(
			"The object stored in the session is not a child of the Transaction class. Parent: " .
			get_parent_class($thisTrans) . " Processing of the " .
			"entire transaction has been stopped and nothing has been completed: " .
			"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
			"emails were never sent out. However, if they went through Verisign, " .
			"they may have been charged. This transaction requires close attention."
		);
	}
	$thisTrans->set_ponumber($_POST["INVOICE"]);
	if ($thisTrans->get_total_price() < 0) {
		reportErrorAndDie(
			"The price on this transaction is NEGATIVE, specifically " . $thisTrans->get_total_price() . 
			". Make sure someone isn't trying to scam us. Processing of the " .
			"entire transaction has been stopped and nothing has been completed: " .
			"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
			"emails were never sent out. However, if they went through Verisign, " .
			"they may have been charged. This transaction requires close attention."
		);
	}
	if (isset($_POST["EMAIL"])) {
		//the address should be coming from the student creating a free account
		//STUB Validate the address! Trust no one!
		$thisTrans->set_email_address($_POST["EMAIL"]);
	}
	write_acct_data($id, "core-email", "done");
} else {
	reportErrorAndDie(
		"A valid transaction type was not supplied. Was given $type. Processing of the " .
		"entire transaction has been stopped and nothing has been completed: " .
		"upgrades/SN's haven't happened, Quickbooks wasn't generated, and " .
		"emails were never sent out. However, if they went through Verisign, " .
		"they may have been charged. This transaction requires close attention."
	);
}

write_acct_data($id, "core-types", "done");

if ($thisTrans->get_user() != null) {
	write_acct_data($id, "core-user", $thisTrans->get_user());
	if (isset($_POST["CUSTID"]) && $_POST["CUSTID"] != $thisTrans->get_user_name()) {
		reportErrorAndDie(
			"The user sent via post does not match the user in the Transaction object. " . 
			"Nothing has been done in this transaction, including applying upgrades/making new SN's, emailing people, and generating " .
			"a quickbooks file."
		);
	}
}
write_acct_data($id, "core-username", "done");


/**
 * Now, for the doing things part.
 *
 * If something goes wrong, these next three object methods will return
 * a string explaining what went wrong. So if we get a string, we report the
 * error and die. 
 */


/* Doing Stuff stuff */
$success = $thisTrans->apply_action();
if (is_string($success)) {
	reportErrorAndDie(
		$success . " All processing after this point has been stopped, " .
		"(which means emails and quickbooks), and this transaction requires very close attention."
	);
}
write_acct_data($id, "core_aa", "core.php applied the action");

$orderer = '';
if (isset($_POST['NAME'])) {
	$FirstName = sanitize(substr($_POST['NAME'], 0, strrpos($_POST['NAME'], ' ')));
	$LastName = sanitize(substr($_POST['NAME'], strrpos($_POST['NAME'], ' '))); //this leaves the space between the names but sanitize trim()s.
	$orderer = $FirstName.' '.$LastName;
}

//this part will decide if we need to get information about a new student or instructor credit transaction
//It breaks OO four ways from Tuesday, but this is the least of all evils.

if ($thisTrans instanceof NewTransaction && $thisTrans->get_usertype() == "student" && $thisTrans->get_method() == "credit") {
	$Program_id = $thisTrans->get_program_id();
	$Address = sanitize($_POST["ADDRESS"]);
	$City = sanitize($_POST["CITY"]);
	$State = sanitize($_POST["STATE"]);
	$ZipCode = sanitize($_POST["ZIP"]);
	$HomePhone = sanitize($_POST["PHONE"]);
	$EmailAddress = $thisTrans->get_email_address();

	//put it in the table!
	$query = "INSERT into StudentData SET ".
		"UserName = 'NotActiveYet', ".
		"Gender = 'X'";

	if (!is_null($FirstName)) $query .= ", FirstName='$FirstName'";
	if (!is_null($LastName)) $query .= ", LastName='$LastName'";
	if (!is_null($Program_id)) $query .= ", Program_id='$Program_id'";
	if (!is_null($Address)) $query .= ", Address='$Address'";
	if (!is_null($City)) $query .= ", City='$City'";
	if (!is_null($State)) $query .= ", State='$State'";
	if (!is_null($ZipCode)) $query .= ", ZipCode='$ZipCode'";
	if (!is_null($HomePhone)) $query .= ", HomePhone='$HomePhone'";
	if (!is_null($EmailAddress)) $query .= ", EmailAddress='$EmailAddress'";

	$connection =& FISDAPDatabaseConnection::get_instance();
	$connection->query($query);

	/**
	 * Then update the SerialNumber table, now that we have the student id for
	 * this student
	 */
	$newStudent_id = ($connection->get_last_insert_id()) * -1;

	$SerialNumberArray = $thisTrans->get_serNumbersArray();
	if (count($SerialNumberArray) == 1) {
		$SerialNumber = $SerialNumberArray[0];

		$update = "UPDATE SerialNumbers ".
			"SET Student_id = $newStudent_id ".
			"WHERE Number = '$SerialNumber' ".
			"LIMIT 1";
		$connection->query($update);
	} else {
		reportErrorAndDie(
			"Unable to update student id in serial number table. All processing after this point has been stopped " .
			"(no emails, no quickbooks, and no information about the student in the serial number table). This " .
			"transaction requires close attention."
		);
	}	
}// end of StudentData Insert for new student credit card transactions	

/* Quickbooks Stuff */
if ($thisTrans->get_method() == "credit") {
	if ($thisTrans->get_total_price() > 0) {
		$qbFileName = get_quickbooks_filename();
		write_acct_data($id, "quickbooks_filename", $qbFileName);

		$qbFile = QB_OpenFile($qbFileName);
		write_acct_data($id, "qb_open", 1);
		if ($qbFile == false) {
			reportError(
				"Unable to get a handler from Quickbooks. This means the QuickBooks " .
				"invoice will not be generated correctly and Mike will have to add this ".
				"transaction by hand. The filename should have been " . get_quickbooks_filename() . 
				". As long as there are no other errors reported, all upgrade/serial " .
				"number stuff should have completed fine, and anyone who was supposed " .
				"to be emailed about this transaction got an email."
			);
		}

		$success = QB_WriteHeaders($qbFile);
		write_acct_data($id, "qb_headers", 1);
		if (!$success) {
			reportError(
				"Unable to write the header to the QB file. This means the QuickBooks " .
				"invoice will not be generated correctly and Mike will have to add this " .
				"transaction by hand.  He should keep an eye out for the file " . get_quickbooks_filename() . 
				". As long as there are no other errors reported, all upgrade/serial " . 
				"number stuff should have completed fine, and anyone who was supposed " .
				"to be emailed about this transaction got an email."
			);
		}

		$success = $thisTrans->generate_quickbooks_report($qbFile);
		write_acct_data($id, "qb_report", 1);
		if (!$success || is_string($success)) { 
			reportError(
				$success . " The QuickBooks invoice will not be generated " .
				"correctly and Mike will have to add this transaction by hand. " .
				"He should keep an eye out for the file " . get_quickbooks_filename() . 
				". As long as there are no other errors reported, all upgrade/serial " . 
				"number stuff should have completed fine, and anyone who was supposed " .
				"to be emailed about this transaction got an email."
			);	
		}

		$success = QB_EndTransaction($qbFile);
		write_acct_data($id, "qb_end", 1);
		if (!$success) {
			reportError(
				"Unable to add \"ENDTRNS<newline>\" to the end of the file. This " .
				"means the QuickBooks invoice will not be generated correctly and " .
				"Mike will have to add this transaction by hand." . 
				"He should keep an eye out for the file " . get_quickbooks_filename() . 
				". As long as there are no other errors reported, all upgrade/serial " . 
				"number stuff should have completed fine, and anyone who was supposed " .
				"to be emailed about this transaction got an email."
			);
		}

		$success = QB_CloseFile($qbFile);
		write_acct_data($id, "qb_close", 1);
		if (!$success) {
			reportError(
				"Unable to close the QB file. This means the QuickBooks invoice " .
				"will not be generated correctly and Mike will have to add this transaction by hand. " .
				"He should keep an eye out for the file " . get_quickbooks_filename() . 
				". As long as there are no other errors reported, all upgrade/serial " . 
				"number stuff should have completed fine, and anyone who was supposed " .
				"to be emailed about this transaction got an email."
			);
		}
		write_acct_data($id, "core_qb", "core.php did qb");
	}   
}


/* Emailing stuff */
$success = $thisTrans->email_everyone($orderer);
if (is_string($success)) {
	reportError($success);
}
write_acct_data($id, "core_ee", "core.php emailed");


/* Display Logic stuff */
$thisTrans->set_processed();
write_acct_data($id, "processed", "true");
Transaction::store_object($thisTrans);

if ($thisTrans->get_method() == "invoice") {
	//	set_session_value("Account_Ordering_FISDAP_ID", $id);
	header("Location: order_results.php");
}



/*****************************************************************************
 * Private Functions
 *****************************************************************************/

function get_passed_id() {
	return (isset($_POST["USER1"])) ? $_POST["USER1"] : null;
}

function get_passed_type() {
	return (isset($_POST["USER2"])) ? $_POST["USER2"] : null;
}

function sanitize($input) {
	if ($input != null) {
		return mysql_real_escape_string(trim(stripslashes($input)));
	} else  return null;
}


/**
 * Does some error logging, displays a generic message to the user asking them
 * to contact us, and then kills the script.
 * 
 * @param message The error message to place in the logs (will not be seen by
 *                the user)
 */
function reportErrorAndDie($error) {
	global $thisTrans;

	if (is_object($thisTrans)) {
		$thisTrans->flag_error();
		$thisTrans->is_processed();
		error_dump($thisTrans->get_fisdap_id(), "core.php", $error, array($_POST, $_SESSION, $thisTrans));
		Transaction::store_object($thisTrans);
	} else {
		global $id;
		if (is_null($id)) {
			error_dump("no_id_" . time(), "core.php", $error, array($_POST, $_SESSION, $thisTrans));
		} else {
			error_dump($id, "core.php", $error, array($_POST, $_SESSION, $thisTrans));
		}
	}
	if (is_object($thisTrans) && $thisTrans->get_method() == "invoice") {
		header("Location: order_results.php");
	}
	die();
}

/*
 * Checks whether a transaction has already been looked at by core. It looks to see if 
 * it's been processed, displayed, or errored, or if the core-types line has been written
 * to the file. 
 *
 * We check because if any of this is true, we don't want to keep working with this 
 * transaction because we'll mess with the log.
 *
 * @pre This function is only reliable if no functions earlier in the script can flag
 *      an error without killing the script (it, reportError is fine, reportErrorAndDie
 *      is not).
 */ 
function already_been_futzed_with () {
	global $id;
	global $thisTrans;

	$futzed = false;
	if ($thisTrans->is_processed()) {
		$futzed = true;
	} else if ($thisTrans->already_displayed()) {
		$futzed = true;
	} else if ($thisTrans->was_error()) {
		$futzed = true;
	}
	return $futzed;
	echo "futzed.";
}

/*
 * Changes a transaction id and file to reflect multiple attempts.
 */
function change_transaction_file() {
	global $id;
	global $thisTrans;

	write_acct_data($id, "futzed", "futzed");
	$log = read_acct_data_to_array($id);
	$attempt = $log["submission_attempt"];
	($attempt < 2)?$attempt = 2:$attempt++;
	$id = substr($id, 0, 17) . "_" . $attempt;
	while (file_exists(get_file_name($id))) {
		write_acct_data($id, "futzed", "futzed");
		$attempt++;
		$id = substr($id, 0, 17) . "_" . $attempt;
	}

	foreach ($log as $key => $value) {
		if ($key == "object-found") {
			break;
		} else {
			write_acct_data($id, $key, $value);
		}
	}
	write_acct_data($id, "submission_attempt", $attempt);

	$thisTrans->update_fisdap_id($attempt);
}
?>
