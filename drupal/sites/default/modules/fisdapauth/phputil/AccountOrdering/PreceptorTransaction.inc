<?php

require_once('phputil/AccountOrdering/Transaction.inc');
require_once('phputil/AccountOrdering/utils.php');
require_once('phputil/classes/Assert.inc');
require_once('phputil/handy_utils.inc');
require_once('phputil/price.php');
require_once('phputil/session_data_functions.php');
require_once('phputil/classes/model/CommonInstructorFactory.inc');

/**
 * 
 * An object to sell Preceptor Training accounts (via serial numbers and upgrades).
 *
 * When using, you must:
 *    Call either set_upgrade_transaction() or set_new_transaction().
 *    Set the number of accounts wanted OR give the usernames to be upgraded.
 *    Call is_buying_own_account() or is_buying_third_party_accounts() as applicable.
 */
class PreceptorTransaction extends Transaction {

	/*********
	 * FIELDS
	 *********/

	const PRECEPTOR_TABLE = 'moodle_PrecepTrainingEnroll';
	const PRECEPTOR_COURSE = 2; 
	const ACCT_TYPE = 'instructor';
	const CONFIG_CODE = 64;
	const LIMIT_CODE = 0;
	protected $serial_numbers;
	protected $num_of_accts = 0;
	private $modifying_own_account = false;
	protected $transaction_type = "";

	/**************
	 * CONSTRUCTOR
	 **************/

	/*
	 * @param string _method Credit or Invoice
	 * @param common_instructor/null  _user The user ordering the accounts. Null if an instructor is ordering their own new account (and thus has no object yet).
	 */
	public function __construct($_method, $_user=null) {
		$_usertype = 'instructor';

		// We have to set this here, or the constructor will be mad that there's no user object
		if (is_null($_user)) { 
			$this->set_is_buying_own_account(); 
		} else {
			$this->set_is_buying_third_party_accounts();
		}

		$error = parent::__construct($_method, $_usertype, $_user);
		if (is_string($error)) {
			echo $error;
			return $error;
		}
	}

	/**
	 * Setters
	 */

	public function set_number_of_accounts($num) {
		Assert::is_true(Test::is_int($num));
		$num = Convert::to_int($num);

		$this->num_of_accts = $num;
	}

	public function set_upgrade_transaction() {
		$this->transaction_type = 'upgrade';
		$this->usernames = array();
	}

	public function set_new_transaction() {
		$this->transaction_type = 'new';
		$this->serial_numbers = array();
	}

	public function set_is_buying_own_account() {
		$this->modifying_own_account = true;
	}

	private function set_is_buying_third_party_accounts() {
		$this->modifying_own_account = false;
	}

	/**
	 * Getters
	 */

	public function is_upgrade_transaction() {
		if ($this->transaction_type == 'upgrade') {
			return true;
		} else {
			return false;
		}
	}

	public function is_new_transaction() {
		if ($this->transaction_type == 'new') {
			return true;
		} else {
			return false;
		}
	}

	public function get_number_of_accounts() {
		return $this->num_of_accts;
	}

	public function is_buying_own_account() {
		return $this->modifying_own_account;
	}

	public function is_buying_third_party_accounts() {
		if (!$this->modifying_own_account) {
			return true;
		}
	}

	public function is_upgrading_own_account() {
		if ($this->modifying_own_account && $this->is_upgrade_transaction()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Sorry for the inconsistent "acct"/"account" between functions
	 */
	public function get_acct_type() {
		return self::ACCT_TYPE;
	}

	public function get_config_code() {
		return self::CONFIG_CODE;
	}

	public function get_serNumbersArray() {
		return $this->serial_numbers;
	}

	public function get_upgrades() {
		$upgradesArray = array();
		foreach ($this->usernames as $username) {
			$upgradesArray[$username] = self::CONFIG_CODE;
		}
		return $upgradesArray;
	}
	/**
	 * Adds a username to object to be upgraded. 
	 *
	 * @param username The username (not id) of the user to be upgraded
	 */
	function add_upgrade($username) {
		if (is_string($username)
			&& array_search($username, $this->usernames) === false) {
				$this->usernames[] = $username;
			}

		return true;
	}

	/**
	 * Generates serial numbers
	 * If a user is creating his or her own account, enters them in the UserAuthData table using PayPal info
	 */
	public function apply_action() {
		if ($this->is_upgrade_transaction()) {
			return $this->upgrade_accounts();
		} else if ($this->is_new_transaction()) {
			return $this->create_new_accounts();
		}
	}

	public function email_everyone($orderer = null) {
		//STUB waiting on louise	

		$emailAddress = $this->email_address;

		if ($orderer != null) { 
			$name = stripslashes($orderer);
		} else {
			$name = stripslashes($this->user->get_firstname()." ".$this->user->get_lastname());
		}

		$program_id = $this->program;
		$price = show_as_currency($this->get_total_price());

		$fromEmailAddr = "fisdap-sales@fisdap.net";
		$additionalHeaders = "From: $fromEmailAddr\n";

		if ($this->method == 'invoice') {
			$isinvoice = true;
		} else {
			$isinvoice = false;
		}

		if ($this->is_new_transaction()) {
			if ($this->is_buying_own_account()) {

				//email the person buying thier thingy
				$subject = 'FISDAP Serial Numbers';
				$mailText = "Hello $name,\n\n" . 
					"Thank you for purchasing a FISDAP account:\n".
					"Your FISDAP serial number is ".$this->serial_numbers[0].".\n\n" .
					"if you have not yet activated your account, go to the following link and follow the instructions there:\n\n";

				$sn = $this->serial_numbers[0];
				$setupLink = 'https://'.$_SERVER['SERVER_NAME'].
					get_url_for('addstudent.html?SerialNumber='.$sn.'&Submit=SerialNumber&Instructor=true');
				$mailText .= "$setupLink\n\n";

				$vID = $this->verisign_id;

				$mailText .= "This email will serve as your receipt. ".
					"Here are the transaction details:\n";
				$mailText .= "Ordered by: $name\n";
				$mailText .= "VeriSign Ref: $vID\n";
				$mailText .= "Amount: $price\n\n";

				$mailText .= "We appreciate your support!";

				$successful = $this->mail_to_fisdap($mailText, $name);
				if (!$successful) {
					$this->reportError("Unable to email FISDAP about upgrade. As long as there ".
						"were no other errors, everything else should have worked fine.");
				}

				return mail($emailAddress, $subject, $mailText, $additionalHeaders);
			} else {
				//email the instructor who bought all the account(s)
				$emailAddress = $this->email_address;
				$subject = 'FISDAP Serial Numbers';

				$mailText = "Hello $name,\n\n" . 
					"Thank you for your order of:\n\n".
					"$this->num_of_accts FISDAP Clinical Educator Account(s)\n\n" .
					"Each serial number can be used only one time. ".
					"Assign one serial number below to each clinical educator, ".
					"and give them these directions to set up their accounts:\n".
					"1.  Go to www.fisdap.net\n".
					'2.  Click the "I am an educator" link under the "First Time at FISDAP" heading.'."\n".
					'3.  On the right side of the next page, click the "create an account" button in the yellow box.'."\n".
					'4.  Enter your serial number in the appropriate space and follow the prompts to activate your account.'."\n\n";


				if ($isinvoice) {
					$po = $this->ponumber;
					if ($po != null) {
						$mailText .= "The purchase order number is: $po\n\n";
					} else {
						$mailText .= "You did not enter a PO number for this purchase.\n\n";
					}
					$mailText .= "An invoice for $price will be sent to:\n\n";
					$mailText .= get_billing_address($program_id);
					$mailText .= "\n\n";

				} else {
					$vID = $this->verisign_id;

					$mailText .= "This email will serve as your receipt. ".
						"Here are the transaction details:\n";
					$mailText .= "Ordered by: $name\n";
					$mailText .= "VeriSign Ref: $vID\n";
					$mailText .= "Amount: $price\n\n";
				}

				$mailText .= "Here are your serial numbers:\n";
				foreach ($this->serial_numbers as $sn) {
					$mailText .= "$sn\n";
				}

				$successful = $this->mail_to_fisdap($mailText, $name);
				if (!$successful) {
					$this->reportError("Unable to email FISDAP about upgrade. As long as there ".
						"were no other errors, everything else should have worked fine.");
				}

				return mail($emailAddress, $subject, $mailText, $additionalHeaders);
			}
		} else if ($this->is_upgrade_transaction()) {
			if ($this->is_buying_own_account()) {
				//email the person about their account
				$subject = "FISDAP Account Upgrades";
				$mailText = "Hello $name,\n\n".
					"Thank you for your order.\n\n".
					"You now have access to FISDAP Clinical Educator Training.\n".
					"To get started, go to www.fisdap.net and log in. On the MyFISDAP\n".
					"page, you will see a link to FISDAP's Clinical Educator Trainting.\n\n";

				if ($isinvoice) {
					$po = $this->ponumber;
					if ($po != null) {
						$mailText .= "The purchase order number is: $po\n\n";
					} else {
						$mailText .= "You did not enter a PO number for this purchase.\n\n";
					}

					$mailText .= "An invoice for $price will be sent to:\n\n";
					$mailText .= get_billing_address($program_id);
					$mailText .= "\n\n";

				} else {
					$vID = $this->verisign_id;

					$mailText .= "This email will serve as your receipt. ".
						"Here are the transaction details:\n";
					$mailText .= "Ordered by: $name\n";
					$mailText .= "VeriSign Ref: $vID\n";
					$mailText .= "Amount: $price\n\n";

					$mailText .= "If you have any questions, please contact FISDAP support at support@fisdap.net.  We appreciate your support!\n\n";

				}

				$successful = $this->mail_to_fisdap($mailText, $name);
				if (!$successful) {
					$this->reportError("Unable to email FISDAP about upgrade. As long as there ".
						"were no other errors, everything else should have worked fine.");
				}

				return mail($emailAddress, $subject, $mailText, $additionalHeaders);


			} else {
				//email the instructor who ordered and all affected instructors

				$subject = "FISDAP Account Upgrades";
				$mailText = "Hello $name,\n\n".
					"Thank you for your order.\n\n";

				if ($isinvoice) {
					$po = $this->ponumber;
					if ($po != null) {
						$mailText .= "The purchase order number is: $po\n\n";
					} else {
						$mailText .= "You did not enter a PO number for this purchase.\n\n";
					}

					$mailText .= "An invoice for $price will be sent to:\n\n";
					$mailText .= get_billing_address($program_id);
					$mailText .= "\n\n";

				} else {
					$vID = $this->verisign_id;

					$mailText .= "This email will serve as your receipt. ".
						"Here are the transaction details:\n";
					$mailText .= "Ordered by: $name\n";
					$mailText .= "VeriSign Ref: $vID\n";
					$mailText .= "Amount: $price\n\n";

					$mailText .= "If you have any questions, please contact FISDAP support at support@fisdap.net.  We appreciate your support!\n\n";
				}

				$mailText .= "The following accounts now include access to FISDAP Clinical Educator Training:\n";

				require_once('phputil/classes/model/CommonUserFactory.inc');
				$user_factory = CommonUserFactory::get_instance();
				foreach ($this->usernames as $username) {
					$upgraded_user = $user_factory->get_by_source($username);
					$mailText .= $upgraded_user->get_firstname() . ' ' . $upgraded_user->get_lastname() . " ($username)\n";
				}

				//email affected instructors
				$successful = $this->mail_upgrades_to_inst($this->usernames, $name);
				if (!$successful) {
					$this->reportError("Mail failed to send to instructor accounts being upgraded");
				}

				$successful = $this->mail_to_fisdap($mailText, $name);
				if (!$successful) {
					$this->reportError("Unable to email FISDAP about upgrade. As long as there ".
						"were no other errors, everything else should have worked fine.");
				}

				return mail($emailAddress, $subject, $mailText, $additionalHeaders);

			}

		}
		return true;
	}

	public function generate_quickbooks_report(&$fileH) {

		if ($this->method == 'credit') {

			// Prepare our variables
			// $custID = get_customer_id($this->get_program_id()) . '-' . fetch_program_name($this->program);
			$custID = get_quickbooks_id($this->program);
			$vID = $this->verisign_id . '-' . $this->credname;

			// Write to the file
			$success = QB_WriteTransaction($fileH, $this->date, QB_CREDIT_CARD_ACCOUNT, $custID, $this->total_price, $vID, '');
			if (!$success) {
				$this->reportError('Unable to write the first line of the transaction. '.
					'Invoice inport file will be malformed. Tell Mike! ' .
					'Quickbooks is totally messed up, but as long as there ' .
					'were no other errors, everything else worked fine.');
				return $success;
			}

			// Prepare our variables
			$constants = get_qb_constant('preceptortraining', 'instructor'); 
			$qb_item = $constants[0];
			$qb_sales = $constants[1];
			$price = 0;
			$str = ''; 
			GetPriceAndDisplayName($this->get_program_id(), 'instructor', 64, $price, $str);
			$total_accounts = ($this->transaction_type == 'new') ? $this->get_number_of_accounts() : count($this->usernames);

			// Write to the file
			$success = QB_WriteDistribution($fileH, $this->date, $qb_item, $price, $total_accounts, $qb_sales);
			if (!$success) {
				$this->reportError("Unable to write a line of the QB report. Import " .
					"file will be malformed. Tell Mike! " .
					"Quickbooks is totally messed up, but as long as there " .
					"were no other errors, everything else worked fine.");
			}
			return $success;
		}

		return true;
	}

	private function mail_upgrades_to_inst($usernames, $upgrader) {

		foreach ($usernames as $username) {
			$factory = CommonInstructorFactory::get_instance();
			$user = $factory->get_by_source($username);
			$toEmailAddr = $user->get_emailaddress();
			$first_name = stripslashes($user->get_firstname());

			$fromEmailAddr = "fisdap-sales@fisdap.net";
			$additionalHeaders = "From: $fromEmailAddr";
			$subject = "Your FISDAP account has been upgraded.";

			$mailText = "";
			$mailText .= "Hello $first_name,\n\n".
				"$upgrader has upgraded your FISDAP account.\n".
				"You now have access to FISDAP Clinical Educator Training.\n".
				"To get started, go to www.fisdap.net and log in. On the MyFISDAP\n".
				"page, you will see a link to FISDAP's Clinical Educator Training.\n\n";

			$mailText .= "If you have any questions, please contact $upgrader.\n";

			$success = mail($toEmailAddr, $subject, $mailText, $additionalHeaders);
			if (!$success) {
				$this->reportError("Mail didn't go through to $toEmailAddr.");
				return false;
			}
		}
		return true;
	}


	private function upgrade_accounts() {
		foreach ($this->usernames as $username) {
			$factory = CommonInstructorFactory::get_instance();
			$user = $factory->get_by_source($username);
			$successful = $user->set_product_access('preceptortraining', 'on');
			if (!$successful) {
				$this->reportError("set_product_access() was unable to give " .
					"$username access to Preceptor Training for some reason. " .
					"This doesn't stop other upgrades from being applied, " .
					"but it does mean this user probably never got what they paid for " .
					"and the instructor never got an email about it. You may " .
					"want to apply this upgrade by hand.");

			}
		}
		return true;
	}

	private function create_new_accounts() {
		for ($i = 0; $i < $this->get_number_of_accounts(); $i++) {
			$returnString = '';
			if (GenFISDAPSerialNumber(self::ACCT_TYPE, self::CONFIG_CODE, self::LIMIT_CODE, $returnString)) {
				//we made a thing!
				$this->serial_numbers[] = $returnString;

				$serialnumber = $returnString;
				if ($this->method == 'credit') {
					$distMethod = 'Online:'.$this->verisign_id;
					$purchaseOrder = 'Online Payment';
				} else {
					$distMethod = 'lemail';
					$purchaseOrder = $this->ponumber;
				}

				insertSNInDatabase($serialnumber, $distMethod, $this->get_program_id(), $purchaseOrder,
					self::ACCT_TYPE, self::LIMIT_CODE, self::CONFIG_CODE);
			} else {
				$this->reportError("Successfully created $i serial numbers before receiving this error: \"$returnString\". " .
					"Generation will continue, so you may see more errors.");
			}
		}
		return true;
	}

}

/**
 * Utilites for preceptor accounts and ordering
 */

class PreceptorUtils {

	/**
	 * Allow $username to access the Preceptor Training Moodle.
	 * @return boolean true on success, false on error
	 */

	public static function enrol_in_moodle($username) {
		Assert::is_string($username);

		$connection = FISDAPDatabaseConnection::get_instance();
		$dbConnect = $connection->get_link_resource();

		//STUB use the util to add them to the course, delete most or all of the rest of this block
		$checksel = 'SELECT * from ' . PreceptorTransaction::PRECEPTOR_TABLE . ' WHERE moodlecourse=' . PreceptorTransaction::PRECEPTOR_COURSE . " AND username='$username'";
		$checkres = mysql_query($checksel,$dbConnect);
		$checkrows = mysql_num_rows($checkres);
		if ($checkrows==0) {   
			$prep_insert = 'INSERT INTO ' . PreceptorTransaction::PRECEPTOR_TABLE . ' SET moodlecourse=' . PreceptorTransaction::PRECEPTOR_COURSE . ", username='$username'";
			$prep_result = mysql_query($prep_insert,$dbConnect);
			if ($prep_result == false) {
				return false;
			}
		}
		return true;
	}

	public static function unenrol_in_moodle($username) {
		Assert::is_string($username);

		$connection = FISDAPDatabaseConnection::get_instance();

		$checksel = 'SELECT * from ' . PreceptorTransaction::PRECEPTOR_TABLE . ' WHERE moodlecourse=' . PreceptorTransaction::PRECEPTOR_COURSE . " AND username='$username'";
		$checkres = $connection->query($checksel,$dbConnect);
		if ($checkres) {
			$prep_del = 'DELETE FROM ' . PreceptorTransaction::PRECEPTOR_TABLE . ' WHERE moodlecourse=' . PreceptorTransaction::PRECEPTOR_COURSE . " AND username='$username' LIMIT 1";
			$prep_result = $connection->purge($prep_del);
			if ($prep_result == false) {
				return false;
			}
		}
		return true;
	}

	public static function get_preceptor_training_url() {

		$environment = HandyServerUtils::get_environment();

		if ($environment == 'development' || $environment == 'testing') {
			return 'http://rctest.fisdap.net/preceptorcorner/';
		} else {
			return 'http://www2.fisdap.net/preceptorcorner/';
		}
	}
}

?>
