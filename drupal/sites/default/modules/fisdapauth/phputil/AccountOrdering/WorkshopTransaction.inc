<?php

require_once("phputil/AccountOrdering/Transaction.inc");

$connection = FISDAPDatabaseConnection::get_instance();

/*
* workshop_transaction.inc
*
* Creates objects to help with registering for workshops
*
*/

class WorkshopTransaction extends Transaction {

	/********
	 * Fields
	 ********/

	var $number_of_accounts; //in
	var $acct_type;
	var $config_code;
	var $limit_code;
	var $serNumberArray;

	//vars I created
	var $first_name;
	var $last_name;
	var $userID;
	var $email;
	var $location;
	var $workshop_date;
	var $program;
	var $duration;

	/*************
	 * Constructor
	 * ***********/
	function WorkshopTransaction($_first,$_last,$_userID,$_email,$_location, $_date, $_price, $_program, $_duration, $_user=null) {
		
		$error = parent::__construct('credit', 'workshop', $_user);
		if (is_string($error))
			return $error;

		$this->first_name = $_first;
		$this->last_name = $_last;
		$this->userID = $_userID;
		$this->email = $_email;
		$this->location = $_location;
		$this->workshop_date = $_date;
		$this->total_price = $_price;
		$this->program = $_program;
		$this->duration = $_duration;

		$this->number_of_accounts = 0;
		$this->acct_type = null;
		$this->config_code = 0;
		$this->limit_code = 0;
		$this->serNumbersArray = array();
	}

	/****************
	 * Public Methods
	 *
	 * core.php needs these methods
	 * **************/

	/* 
	 * This function updates the db and sets the paid flag to true
	 */
	function apply_action() {
		global $connection;
		$query = "UPDATE ws_attendees SET paid=1 WHERE userID=$this->userID LIMIT 1";
		$connection->query($query);

		write_acct_data($this->fisdap_id,'workshop_object',$this);

	}




	function email_everyone($orderer) {
		global $connection;
		$query = "SELECT w.email_text,w.email_subject FROM ws_attendees a,ws_workshops w WHERE a.userID = $this->userID AND a.workshop = w.workshopID";
		$result = $connection->query($query);

		$email_text = $result[0]['email_text'];
		$email_subject = $result[0]['email_subject'];
		$vID = $this->verisign_id;
		$price = show_as_currency($this->total_price);



		$toEmailAddr = $this->email;

		if ($orderer != null)
			$name = $orderer;
		else
			$name = $this->first_name.' '.$this->last_name;
		
		
		$price = show_as_currency($this->get_total_price());
		$fromEmailAddr = "fisdap-sales@fisdap.net";
		$additionalHeaders = "From: $fromEmailAddr\n";
		$subject = $email_subject;
		$vstransref = $this->verisign_id;
		$body = $email_text."\n\n";
		$body .= "This email will serve as your receipt. ".
			"Here are the transaction details:\n";
		$body .= "Ordered by:   $name\n";
		$body .= "VeriSign Ref: $vID\n";
		$body .= "Amount:       $price\n";


		$result = mail($toEmailAddr,$subject, $body, $additionalHeaders);
		if (!$result) {
			$this->reportError("Was unable to email the order information to FISDAP. ".
					"As long as there are no other errors, everything else ".
					"worked out fine.");
		}
		return true;
	}




	function generate_quickbooks_report(&$fileHandler) {
		write_acct_data($this->fisdap_id, "trans-qb", "started");
		//$custID = get_customer_id($this->program). "-" .fetch_program_name($this->program);
		$custID = get_quickbooks_id($this->program);
		$vID = $this->verisign_id . "-" . $this->credname;
		write_acct_data($this->fisdap_id, "transaction-cust", $custID);
		$success = QB_WriteTransaction($fileHandler, $this->date, QB_CREDIT_CARD_ACCOUNT,
				$custID, $this->total_price,
				$vID, "");
		write_acct_data($this->fisdap_id,"trans-qb-trans","wrote transaction");
		if (!$success) {
			$this->reportError("Unable to write the first line of the transaction. ".
					"Invoice inport file will be malformed. Tell Mike! " .
					"Quickbooks is totally messed up, but as long as there " .
					"were no other errors, everything else worked fine.");
		}
		//foreach ($this->productsArray as $product=>$row) {
			$success = QB_WriteDistribution($fileHandler, $this->date, 'ER-Event Registration',
					$this->total_price, "1",
					'Special Projects:Event Registration');
			if (!$success) {
				$this->reportError("Unable to write a line of the QB report. Import " .
						"file will be malformed. Tell Mike! " .
						"Quickbooks is totally messed up, but as long as there " .
						"were no other errors, everything else worked fine.");
			}
		//}
		return true;
	}

	function get_location() {
		return $this->location;
	}

	function get_workshop_date() {
		return $this->workshop_date;
	}

	function get_email() {
		return $this->email;
	}

	function get_total_price() {
		return $this->total_price;
	}

	function get_duration() {
		return $this->duration;
	}



}


?>
