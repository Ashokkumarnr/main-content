<?php

/*
 * CONSTANTS
 */
define('TRACKING_CODE', 1);
define('SCHEDULER_CODE', 2);
define('PDA_CODE', 4);
define('TESTING_CODE', 8);
define('PREP_CODE', 16);
define('CLASSROOM_CODE', 32);
define('PRECEPTOR_CODE', 64);
define('TRACKING_STRING', 'tracking');
define('SCHEDULER_STRING', 'scheduler');
define('PDA_STRING', 'pda');
define('TESTING_STRING', 'testing');
define('PREP_STRING', 'prep');
define('CLASSROOM_STRING', 'classroom');
define('PRECEPTOR_STRING', 'preceptortraining');

//require_once("Transaction.inc");

function get_all_possible_account_types() {
	return array('emt-b', 'emt-i', 'paramedic', 'instructor');
}

function get_all_possible_payment_methods() {
	return array('credit', 'invoice', 'staff');
}

function get_all_possible_user_types() {
	return array('instructor', 'staff', 'student', 'workshop');
}

function get_product_array() {
	return array(TRACKING_STRING=>TRACKING_CODE,
		SCHEDULER_STRING=>SCHEDULER_CODE,
		PDA_STRING=>PDA_CODE,
		TESTING_STRING=>TESTING_CODE,
		PREP_STRING=>PREP_CODE,
		CLASSROOM_STRING=>CLASSROOM_CODE,
		PRECEPTOR_STRING=>PRECEPTOR_CODE);
}

function get_instructor_product_array() {
	return array(PRECEPTOR_STRING=>PRECEPTOR_CODE);
}

function get_student_product_array() {
	return array(TRACKING_STRING=>TRACKING_CODE,
		SCHEDULER_STRING=>SCHEDULER_CODE,
		PDA_STRING=>PDA_CODE,
		TESTING_STRING=>TESTING_CODE,
		PREP_STRING=>PREP_CODE,
		CLASSROOM_STRING=>CLASSROOM_CODE,
	);
}
function get_product_code($product) {
	$productArray = get_product_array();
	return $productArray[$product];
}//function get_product_code

/*
 * Returns an array of product names based on the $code.
 * If the code is larger than the largest code in use, it also returns the
 * code. Most likely, this suggests an error in the php code that developed
 * the configuration code.
 */
function get_full_config_by_code($code) {
	$array = array();

	//To add another product, put the if statement right here.

	if ($code > 63) {
		$code = $code - 64;
		array_push($array, PRECEPTOR_STRING);
	}
	if ($code > 31) {
		$code = $code - 32;
		array_push($array, CLASSROOM_STRING);
	}
	if ($code > 15) {
		$code = $code - 16;
		array_push($array, PREP_STRING);
	}
	if ($code > 7) {
		$code = $code - 8;
		array_push($array, TESTING_STRING);
	}
	if ($code > 3) {
		$code = $code - 4;
		array_push($array, PDA_STRING);
	}
	if ($code > 1) {
		$code = $code - 2;
		array_push($array, SCHEDULER_STRING);
	}
	if ($code > 0) {
		$code = $code - 1;
		array_push($array, TRACKING_STRING);
	}
	if ($code > 0) {
		array_push($array, $code);
	}
	return $array;
}

function get_full_limit_by_code($code) {
	$array = array();
	//To add another product, put the if statement right here.
	if ($code > 63) {
		$code = $code - 64;
		array_push($array, PRECEPTOR_STRING);
	}
	if ($code > 31) {
		$code = $code - 32;
		array_push($array, CLASSROOM_STRING);
	}
	if ($code > 15) {
		$code = $code - 16;
		array_push($array, PREP_STRING);
	}
	if ($code > 7) {
		$code = $code - 8;
		array_push($array, TESTING_STRING);
	}
	if ($code > 3) {
		$code = $code - 4;
		array_push($array, PDA_STRING);
	}
	if ($code > 1) {
		$code = $code - 2;
		array_push($array, SCHEDULER_STRING);
	}
	if ($code > 0) {
		$code = $code - 1;
		array_push($array, TRACKING_STRING);
	}
	if ($code > 0) {
		array_push($array, $code);
	}
	return $array;
}

/**
 * Returns a two element array containing the item constant and the sales
 * constant for the given product-acctType-limit combination.
 *
 * @param product A string representing the product (ex, "testing")
 * @param acctType A string representing the account type (ex, "emt-b")
 * @param limit A boolean describing whether or not this product is limited
 */
function get_qb_constant($product, $acctType, $limit=false) {
	require_once('quickbooks_utils.php');
	$constantArray = array('trackingALS' => array(QB_ITEM_TRACKING_ALS, QB_SALES_TRACKING_ALS),
		'trackingBLS' => array(QB_ITEM_TRACKING_BLS, QB_SALES_TRACKING_BLS),
		'schedulerALS' => array(QB_ITEM_SCHEDULER_ALS, QB_SALES_SCHEDULER_ALS),
		'schedulerBLS' => array(QB_ITEM_SCHEDULER_BLS, QB_SALES_SCHEDULER_BLS),
		'testingParamedic' => array(QB_ITEM_TESTING_PARAMEDIC, QB_SALES_TESTING_PARAMEDIC), 
		'testingBasic' => array(QB_ITEM_TESTING_BASIC, QB_SALES_TESTING_BASIC),
		'pda' => array(QB_ITEM_PDA, QB_SALES_PDA),
		'prepALS' => array(QB_ITEM_STUDYTOOLS_ALS, QB_SALES_STUDYTOOLS_ALS),
		'prepBLS' => array(QB_ITEM_STUDYTOOLS_BLS, QB_SALES_STUDYTOOLS_BLS),
		'workshop' => array(QB_ITEM_EVENT_REGISTRATION, QB_SALES_EVENT_REGISTRATION),
		'preceptortraining' => array(QB_ITEM_PRECEPTOR_TRAINING, QB_SALES_PRECEPTOR_TRAINING)
	);

	if ($product == 'pda') {
		return $constantArray['pda'];
	}
	if ($product == 'preceptortraining') {
		return $constantArray['preceptortraining'];
	} else if ($product == 'testing') {
		if ($acctType == 'emt-b') {
			$level = 'Basic';
		} else {
			$level = 'Paramedic';
		}
	} else {
		if ($acctType == 'emt-b') {
			$level = 'BLS'; 
		} else {
			$level = 'ALS';
		}
	}
	return $constantArray[$product . $level];

	//STUB preceptor training needs to be here!
}

/**
 * Write key value pairs to a file based on the ID. Serializes all values.
 * @pre key cannot contain spaces
 * @return boolean True if the write was successful, and false if it wasn't.
 */
function write_acct_data($transID, $key, $value) {
	$return = false;
	$fname = get_file_name($transID);
	$mode = 'w+';
	$fileData = array();

	//	if (is_object($value) || is_array($value)) {
	$value = serialize($value);
	//	}

	if (file_exists($fname)) {
		$keyExists = read_acct_data($transID, $key);
		if ($keyExists === $value) {
			return true;
		}
		$fileData = file($fname);
		$openFile = fopen($fname, $mode);
		flock($openFile, LOCK_EX);
		if (!($keyExists === false)) {
			foreach ($fileData as $linenum => $line) {
				if (get_key($line) == $key) {
					$fileData[$linenum] = "$key $value\n";
					break;
				}
			}
		} else {
			array_push($fileData, "$key $value\n");
		}
	} else {
		$fileData[0] = "$key $value\n";
		$openFile = fopen($fname, $mode);
		flock($openFile, LOCK_EX);
	}

	//Write everything to the file
	//	chmod($fname, "777");
	foreach ($fileData as $linenum => $line) {
		$success = fwrite($openFile, $line);
		if ($success) {
			$return = true; 
		} else {
			$return = false;
			break;
		}
	}
	fclose($openFile);

	return $return;
}//end of function "write_acct_data"

/**
 * @pre key cannot contain any spaces
 * @return The data associated with the given transID and key, 
 *         or false if that data does not exist.
 */
function read_acct_data($transID, $key) {
	$return = false;
	$fname = get_file_name($transID);
	$exists = file_exists($fname);
	if ($exists) {
		$fileArray = file($fname);
		foreach ($fileArray as $rowStr) {
			$currentKey = get_key($rowStr);
			if ($key == $currentKey) {
				$return = unserialize(get_value($rowStr));
				if ($return === false) {
					$return = get_value($rowStr);
				}
				break;
			}
		}
	}
	return $return;
}//end of function "read_acct_data"

function read_acct_data_to_array($transID) {
	$result = array();
	$fname = get_file_name($transID);

	$rawcontents = file_get_contents($fname);
	$rawarray = explode("\n", $rawcontents);

	foreach ($rawarray as $row) {
		$key = get_key($row);
		$value = unserialize(get_value($row));
		if ($value == null) {
			$value = get_value($row);
		}
		$result[$key] = $value;
	}
	return $result;
}//end of function read_acct_data_to_array

function get_file_name($transID) {
	//$fname = "/var/www/html/FISDAP/temp/acct_" . $transID;
	$path = "/var/www/orderlogs/";
	$fname = $path . "acct_" . $transID;
	return $fname;
}//end of function "get_file_name"

/**
 * Gets the billing address for a given program
 * @param program_id the id of the program in question
 */
function get_billing_address($program_id)
{
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$query = "SELECT BillingPhone, BillingFax, BillingContact, BillingAddress, " .
		"BillingAddress2, BillingAddress3, BillingCity, BillingState, BillingZip, " .
		"BillProgramCountry, ProgramAddress, ProgramAddress2, ProgramAddress3, " .
		"ProgramCity, ProgramState, ProgramZip " .
		"FROM ProgramData where Program_id = $program_id";
	//echo $query;
	$result = mysql_query($query, $dbConnect);

	if ($result == false) {
			return "MySQL Error: " . mysql_error();
	}

	//this section is borrowed from newStudents.html
	$i = 0;
	$BillAddr = mysql_result($result, $i, "BillingAddress");
	$BillAddrVal = $BillAddr;
	$BillAddr2 = mysql_result($result, $i, "BillingAddress2");
	$BillAddr3 = mysql_result($result, $i, "BillingAddress3");
	if ($BillAddr2 != "") {
		$BillAddr = $BillAddr . "\n" . $BillAddr2;
	}
	if ($BillAddr3 != "") {
		$BillAddr = $BillAddr . "\n" . $BillAddr3;
	}

	$BillingCity = mysql_result($result, $i, "BillingCity");
	$BillingState = mysql_result($result, $i, "BillingState");
	$BillingZip = mysql_result($result, $i, "BillingZip");
	$BillingCountry = mysql_result($result, $i, "BillProgramCountry");
	//$BillingCountry

	$BillAddr = $BillAddr . "\n" . $BillingCity . ", " . $BillingState . " " . $BillingZip;

	$BillingContact = mysql_result($result, $i, "BillingContact");
	$BillingPhone = mysql_result($result, $i, "BillingPhone");
	$BillingFax = mysql_result($result, $i, "BillingFax");

	$AddrVal = mysql_result($result, $i, "ProgramAddress");
	$Addr = $AddrVal;
	$Addr2 = mysql_result($result, $i, "ProgramAddress2");
	$Addr3 = mysql_result($result, $i, "ProgramAddress3");
	if ($Addr2 != "") {
		$Addr = $Addr . "\n" . $Addr2;
	}
	if ($Addr3 != "") {
		$Addr = $Addr . "\n" . $Addr3;
	}
	$Addr = $Addr . ",\n" . mysql_result($result, $i, "ProgramCity");
	$Addr = $Addr . ", " . mysql_result($result, $i, "ProgramState");
	$Addr = $Addr . " " . mysql_result($result, $i, "ProgramZip");
	$Address="";

	if ($BillAddrVal == "" || strcasecmp($BillAddrVal, "same") == 0 || strcasecmp($BillAddrVal, "as above") == 0) {
		$Address = $Addr;
	} else {
		$Address = $BillAddr;
	}

	if ($BillingContact != null) {
		$Address = $BillingContact."\n".$Address;
	}
	if ($BillingPhone != null) {
		$Address = $Address."\nPhone:".$BillingPhone;
	}
	if ($BillingFax != null) {
		$Address = $Address."\nFax:".$BillingFax;
	}
	return $Address;
}

/*
 * If something goes wrong, this function can be called to write information
 * to the file associated with $transID.
 *
 * Specifically, everything in $array will be written (if $array holds more
 * arrays, those will be written recursively), plus the text in $error, under
 * the key "error" and (eventually) the name of the calling file.
 */ 
function error_dump($transID, $filename, $error, $array=null) {
	if ($filename == "order_results.php") {
		$errorPrefix = "results_";
	}
	$count = read_acct_data($transID, "errorcount");
	if (!($count > 0)) {
		$count = 0;
	}
	$errorPostfix = $count;
	write_acct_data($transID, $errorPrefix . "error_dump" . $errorPostfix, $array);
	write_acct_data($transID, $errorPrefix . "error_file" . $errorPostfix, $filename);
	write_acct_data($transID, $errorPrefix . "error" . $errorPostfix, $error);
	write_acct_data($transID, $errorPrefix . "error_time" . $errorPostfix, date("D M j G:i:s T Y"));

	$count = $count + 1;
	write_acct_data($transID, "errorcount", $count);
}//end of function error_dump

function get_quickbooks_filename() {
	global $id;
	global $thisTrans;
	$return = "/usr/local/tmp/CCTrans/" . $thisTrans->get_verisign_id() . ".iif";
	return $return;
}   


/*****************************************************************************
 * Functions from here on should be treated as PRIVATE functions. 
 * Oh, if only PHP would let me designated them as such.
 *
 * That means if you use them in your code, and I change them, and it breaks
 * your code... well, I told you not to use them.
 ******************************************************************************/
function get_key($row) {
	$key = substr($row, 0, strpos($row, " "));
	return $key;
}//end of function "get_key"

function get_value($row) {
	$sppos = strpos($row, " ");
	$value = rtrim(substr($row, $sppos + 1));
	return $value;
}//end of function "get_value"	


