<?php
//--------------------------------------------------------------------------
//--                                                                      --
//--      Copyright (C) 1996-2007.  This is an unpublished work of        --
//--                       Headwaters Software, Inc.	                  --
//--                          ALL RIGHTS RESERVED                         --
//--      This program is a trade secret of Headwaters Software, Inc.     --
//--      and it is not to be copied, distributed, reproduced, published, --
//--      or adapted without prior authorization of			  --	
//--      Headwaters Software, Inc.					  --
//--                                                                      --
//--------------------------------------------------------------------------


//============================================
// These lines were for debuggin and should stay commented out
//============================================
// ini_set('include_path','.:..');
// $connection =& FISDAPDatabaseConnection::get_instance();
// $dbConnect = $connection->get_link_resource();
// require_once('price.php');
//============================================

define("START_COUNTER_VAL", 4959);
define("MAX_COUNTER_VAL", 39000);

$Counter = START_COUNTER_VAL;

class SerialNumberUtils {

	function isInstructorSerialNumber($sn) {
		if (self::isValidSerialNumber($sn)) {
			return in_array(substr($sn, 0, 2), array('91', '90'));
		}
		return false;
	}

	function isValidSerialNumber($sn) {
		//STUB
		if (in_array($sn[0], array(0, 1, 2, 9))
				&& in_array($sn[1], array(1, 2))
				&& $sn[2] == '-'
				&& preg_match('/[ABCDEFabcdef0-9]{13}/', substr($sn, 3, 13))
				&& $sn[16] == '-'
				&& preg_match('/[ABCDEFabcdef0-9]{4}/', substr($sn, 17, 4))
		   ) {
			return true;
		}

	}
}


function GenFISDAPSerialNumber( $AccountType, $ConfigCode, $LimitCode, &$ReturnStr) { 

	global $Counter, $dbConnect;

	$SchedulerFlag = -999;
	$NewSerialNumber = '';
	$randVal1 = 0;
	$randVal2 = 0;
	$randVal3 = 0;

	// Check inputs
	if( isset($ConfigCode) && $ConfigCode > 0 && $ConfigCode < 5000)
	{	$SchedulerFlag =  get_access_by_configuration( $ConfigCode, 'scheduler');
	}
	else
	{	$ReturnStr = "Error: Missing or bad Config Code:'" . $ConfigCode . "'.";
		return 0;
	}
	if( $SchedulerFlag < 0 || $SchedulerFlag > 1)
	{	$ReturnStr =  "Bad Scheduler Flag";
		return 0;
	}


	if( !isset( $AccountType) 
			|| $AccountType == NULL 
			|| $AccountType == "")
	{	$ReturnStr =  "Missing Account Type";
		return 0;
	}
	if ( $AccountType == "SS" || $AccountType == "SS Limited")    
	{	$AccountTypeId = 1;
		if($SchedulerFlag != 1)  
		{	$ReturnStr =  "Invalid Scheduler Flag";
			return 0;
		}
	}
	else if( $AccountType == "paramedic")
	{	$AccountTypeId = 11;
	}
	else if ( $AccountType == "emt-b")
	{	$AccountTypeId = 21;
	}
	else if ( $AccountType == "emt-i")
	{	$AccountTypeId = 31;
	}
	else if ( $AccountType == 'instructor') 
	{	$AccountTypeId = 91;
	}
	else
	{	$ReturnStr =  "Bad or Missing Account Type of '". $AccountType . "'.";
		return 0;
	}


	// Generate Numbers
	$Done = 0;
	$Count = 0;

	while( $Done != 1)
	{	$randVal1 = rand( 4096, 39000);
		$randVal2 = rand( 4096, 60535);
		$randVal3 = rand( 4096, 60535);

		$Number1 = $randVal1 * 4096 + date('z');
		$Number2 = $randVal2 * 256 + date('Y');

		if( $SchedulerFlag == 1)
		{	$AccountTypeId++;
		}

		$NewSerialNumber = sprintf(  "%02d-%x%x-%x", $AccountTypeId, $Number1, $Number2, $Counter); 
		$Counter++;

		// Check to see if this number is already in the database.

		$QueryStr = "select Number from SerialNumbers where Number='" . $NewSerialNumber . "'";
		$result = mysql_query($QueryStr,$dbConnect);
		$RowCount = mysql_num_rows( $result);
		if( $RowCount == 0)
		{	$Done = 1;  // This one is unique.
		}

		$Count++;
		if($Count > 1000)
		{	$ReturnStr =  "Unique test overflow.";
			return 0;
		}

		if( $Counter > MAX_COUNTER_VAL)
		{	$Counter = START_COUNTER_VAL;
		}
	}
	$ReturnStr =  $NewSerialNumber;
	return 1;
}

//================================================
//	These lines were used for debuging and should stay commented out.
//================================================
//
//	echo "<html><head></head><body>\n";
//	
//	function CheckNumber( $SerialNumber)
//	{ global $DBStr;
//	
//		$length = strlen( $SerialNumber);
//		
//		if( $length != 21)
//		{	echo "ERROR>>>>>> Bad Length = $length; $SerialNumber; $DBStr<br>";
//		}
//		else
//		{
//		echo "$SerialNumber <br>\n";
//		}
//	}
//
//	$SerialNumber = '';
//
//	// Testing the Test
//	CheckNumber( "Hi There");
//
//	$LoopCount = 0;
//
//	for ( $i=0; $i<10; $i++)
//	{	
//		// Paramedic
//		GenFISDAPSerialNumber( 'paramedic', 1, 0, $SerialNumber);
//		CheckNumber( $SerialNumber);
//		GenFISDAPSerialNumber( 'paramedic', 3, 0, $SerialNumber);
//		CheckNumber( $SerialNumber);
//	
//		// EMT-I
//		GenFISDAPSerialNumber( 'emt-i', 1, 0, $SerialNumber);
//		CheckNumber( $SerialNumber);
//		GenFISDAPSerialNumber( 'emt-i', 3, 0, $SerialNumber);
//		CheckNumber( $SerialNumber);
//	
//		// EMT-b
//		GenFISDAPSerialNumber( 'emt-b', 1, 0, $SerialNumber);
//		CheckNumber( $SerialNumber);
//		GenFISDAPSerialNumber( 'emt-b', 3, 2, $SerialNumber);
//		CheckNumber( $SerialNumber);
//	
//		// SS
//		GenFISDAPSerialNumber( 'SS', 2, 2, $SerialNumber);
//		CheckNumber( $SerialNumber);
//	
//		// SS Limited
//		GenFISDAPSerialNumber( 'SS Limited', 2, 2, $SerialNumber);	
//		CheckNumber( $SerialNumber);
//		$LoopCount++;
//		
//	}
//	
//	echo "Processed $LoopCount loops of Serial Numbers<br>";
//	echo "</body></html>";
//================================================

/**
 * FUNCTION insertSNInDatabase - inserts the serial number into the SerialNumber table.
 * Returns true on success, false on failure
 */
function insertSNInDatabase($SerialNumber, $DistMethod, $Program_id, $PurchaseOrder, $AccountType, $ConfigLimit, $Configuration) {
	//global $dbConnect;
	$connection = FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();
	$insert = "INSERT INTO SerialNumbers ".
		"(Number, Student_id, DistMethod, Program_id, OrderDate, ".
		" PurchaseOrder, AccountType, ConfigLimit, Configuration) ".
		"VALUES ('$SerialNumber', '-15', '$DistMethod', '$Program_id', ".
		"'".date('Y-m-d H:i:s')."', ".
		" '$PurchaseOrder', '$AccountType', '$ConfigLimit', '$Configuration')";
	//$result = $connection->query($insert);
	$result = mysql_query($insert,$dbConnect);
	return $result;
}//end function insertSNInDatabase

?>
