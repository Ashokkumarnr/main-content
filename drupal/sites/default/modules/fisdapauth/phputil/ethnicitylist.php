<?php

$connection = FisdapDatabaseConnection::get_instance();

/**
 * Function to generate a list of all the Ethnicities from the database
 *
 * @author Sam Tape stape@fisdap.net
 */
function gen_ethnicity_list($selected_id = -1) {
	global $connection;

	$query = "SELECT * FROM EthnicityTable ORDER BY EthnicTitle";
	$result = $connection->query($query);

	if (count($result) <= 0) {
		echo "<option value=-1>No Data Available</option>";
	}

	foreach ($result as $ethnicity) {
		$id = $ethnicity['Ethnic_id'];
		$title = $ethnicity['EthnicTitle'];

		if ($id == $selected_id) {
			$selected = 'selected';
		} else {
			$selected = null;
		}

		echo "<option value='$id' $selected>$title</option>";

	}
}







?>
