<?php
/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2007.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/
//This utility returns an array of Student Ids that are associated with a given instructor
function getInstSectionStudents($Instructor_id,$SectYear=-1,$ClassYear=-1,$ClassMonth=-1)
{
    require_once('classes/FISDAPDatabaseConnection.php');
	
    if(!is_numeric($Instructor_id) || $Instructor_id <= 0)
    {
        return false;
    }

    $connection =& FISDAPDatabaseConnection::get_instance();
    
    $selectFromQuery="SELECT DISTINCT SS.Student_id FROM SectInstructors SI, SectStudents SS";
    $whereQuery=" WHERE SI.Instructor_id = $Instructor_id AND SI.Section_id = SS.Section_id";
        
    if($SectYear!=-1)
    {   
        $selectFromQuery .= ", ClassSections CS";
        $whereQuery .= " AND CS.Year = $SectYear AND CS.Sect_id = SI.Section_id";
    }//if Section Year is designated

    if($ClassYear!=-1 || $ClassMonth!=-1)
    {
        $selectFromQuery .= ", StudentData SD";
        $whereQuery .= " AND SD.Student_id = SS.Student_id";
    }//if Class Year or Month are designated, link to StudentData

    if($ClassYear!=-1)
    {
        $whereQuery .= " AND SD.Class_Year = $ClassYear";
    }//if Class Year is designated

    if($ClassMonth!=-1)
    {
        $whereQuery .= " AND SD.ClassMonth = $ClassMonth";
    }//if Class Month is designated

    $query = $selectFromQuery . $whereQuery;

    $result = $connection->query($query);
    if ( !is_array($result) )
    {
        return false;
    }//if
    
    //echo $query . "<br>";

    $students_ids = array();
    foreach ( $result as $row )
    {
        $student_ids[]=$row['Student_id'];
    }//foreach
    
    return $student_ids;
}
?>
