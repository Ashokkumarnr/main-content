<?php

/**
 * This file defines constants that define global settings for this FISDAP
 * deployment.  We also set any global configuration headers necessary for all
 * php scripts (e.g., disabling caching).
 * 
 * @author Sam Martin <smartin@fisdap.net>
 */

////////////////////////////////////////////////////////////////////////////////
// The almighty FISDAP_ROOT.  This is the one immutable configuration constant.
////////////////////////////////////////////////////////////////////////////////

/** the path from the WWW root to the current FISDAP sources */
define('FISDAP_ROOT', realpath(dirname(__FILE__) . '/../') . '/');


////////////////////////////////////////////////////////////////////////////////
// Include any local configuration overrides (e.g., for test deployments)
////////////////////////////////////////////////////////////////////////////////

if ( is_file(FISDAP_ROOT . 'phputil/config_local.php') ) {
    include_once('phputil/config_local.php');
}//if


////////////////////////////////////////////////////////////////////////////////
// Global configuration settings.  Change these at your own risk.  Local
// modifications should be made in phputil/config_local.php.
////////////////////////////////////////////////////////////////////////////////

/** the path from the WWW root to the current FISDAP sources */
if ( !defined('FISDAP_WEB_ROOT') ) 
{
    define('FISDAP_WEB_ROOT', '/');
}//if

/** the path from the WWW root to the Moodle root */
if ( !defined('FISDAP_MOODLE_PATH') ) 
{
    define('FISDAP_MOODLE_PATH', '/moodle/');
}//if

/** set this to true to enable Moodle single sign-on (SSO) */
if ( !defined('ENABLE_FISDAP_MOODLE_SSO') ) 
{
    define('ENABLE_FISDAP_MOODLE_SSO', false);
}//if

?>
