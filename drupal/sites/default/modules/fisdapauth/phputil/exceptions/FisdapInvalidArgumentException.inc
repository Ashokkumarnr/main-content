<?php
require_once('phputil/exceptions/FisdapException.inc');

/**
 * An invalid argument has occurred. 
 */
final class FisdapInvalidArgumentException extends FisdapException {
    /**
     * Constructor.
     * @param string|null $message An indication of what caused the problem.
     * @param Exception|null $cause The underlying cause.
     */
    public function __construct($message=null, $cause=null) {
        if (is_null($message)) {
            $message = 'invalid argument';
        }

        parent::__construct($message, $cause);
    }
}
?>
