/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
 
 /**
  * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
   */
 
// Declaring valid date character, minimum year and maximum year
var dtCh= "-";
var today = new Date();
var year = today.getFullYear();
var minYear = year-100;
//var maxYear= year-10;
   
function isInteger(s)
{
    var i;
    for (i = 0; i < s.length; i++)
    {   // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}//isInteger(s)
                                            
function stripCharsInBag(s, bag)
{
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}//stripCharsInBag(s, bag)

function daysInFebruary (year)
{
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}//daysInFebruary(year)

function DaysArray(n) 
{
    for (var i = 1; i <= n; i++) 
    {
        this[i] = 31
        if (i==4 || i==6 || i==9 || i==11) 
        {
            this[i] = 30
        }
        if (i==2) 
        {   
            this[i] = 29
        }
    } 
    return this
}//DaysArray(n)

function isDate(dtStr_element, dateType,maxYear)
{
    var dtStr=dtStr_element.value
    var daysInMonth = DaysArray(12)
    var pos1=dtStr.indexOf(dtCh)
    var pos2=dtStr.indexOf(dtCh,pos1+1)
    var strYear=dtStr.substring(0,pos1)
    var strMonth=dtStr.substring(pos1+1,pos2)
    var strDay=dtStr.substring(pos2+1)
    if(typeof maxYear=='undefined')
    {
	maxYear = today.getFullYear() - 10;
    }
    strYr=strYear
    if (strDay.charAt(0)=="0" && strDay.length>1) 
    {
        strDay=strDay.substring(1)
    }
    if (strMonth.charAt(0)=="0" && strMonth.length>1) 
    {
        strMonth=strMonth.substring(1)
    }
    for (var i = 1; i <= 3; i++) 
    {
        if (strYr.charAt(0)=="0" && strYr.length>1) 
        {
            strYr=strYr.substring(1)
        }
    }//for
    month=parseInt(strMonth)
    day=parseInt(strDay)
    year=parseInt(strYr)
    if (pos1==-1 || pos2==-1)
    {
        alert('The ' + dateType + ' format should be: yyyy-mm-dd')
        return false;
    }
    if (strMonth.length<1 || month<1 || month>12)
    {
        alert("Please enter a valid month for the " + dateType + ".")
        return false;
    }
    if (strDay.length<1 || day<1 || day>31 
                        || (month==2 && day>daysInFebruary(year)) 
                        || day > daysInMonth[month])
    {
        alert("Please enter a valid day for the " + dateType + ".")
        return false;
    }
    if (strYear.length != 4 || year==0 || year<minYear || year>maxYear)
    {
        alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear+" for the " + dateType + ".")
        return false;
    }
    if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false)
    {
        alert("Please enter a valid date for the" + dateType + ".")
        return false;
    }
    else 
    {
        if (day<10)
        {
            strDay = "0" + strDay
        }
        if (month<10)
        {
            strMonth = "0" + strMonth
        }
        dtStr_element.value=strYear+"-"+strMonth+"-"+strDay;
        return true;
    }
}//isDate(dtStr)

function ValidateForm()
{
    var dt=document.frmSample.txtDate
    if (isDate(dt.value)==false)
    {
        dt.focus()
        return false
    }
    return true
}//ValidateForm()
