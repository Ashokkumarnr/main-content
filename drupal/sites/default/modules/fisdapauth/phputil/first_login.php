<?php

/**
 * This file contains functions used when an account is first used.  Currently,
 * the only behavior we need is to take new students to the consent form when
 * they log in for the first time.
 */

require_once('phputil/session_functions.php');


/**
 * The first time a student logs in, take 'em to the consent form.
 */
function handle_first_student_login() {
    // make sure the user has a session, is logged in, and has just logged in
    // for the first time
    if ( isset($_SESSION) 
         && isset($_SESSION[SESSION_KEY_PREFIX . 'username'])
         && isset($_SESSION[SESSION_KEY_PREFIX . 'first_student_login'])
         && $_SESSION[SESSION_KEY_PREFIX . 'first_student_login'] ) {
        $_SESSION[SESSION_KEY_PREFIX . 'first_student_login'] = 0;
        unset($_SESSION[SESSION_KEY_PREFIX . 'first_student_login']);

        redirect_to('admin/consent.html?FirstTime=1');
    }//if
}//handle_first_student_login

?>
