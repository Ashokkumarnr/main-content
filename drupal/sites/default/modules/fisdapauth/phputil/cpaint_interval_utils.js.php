<?php

/**
 * This file allows client-side interval cpaint calls to be executed
 * (e.g., checking the time every 20 minutes).
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @package default
 * @author Sam Martin <smartin@fisdap.net>
 * @copyright 1996-2007 Headwaters Software, Inc.
 *
 */

header('Content-Type: text/javascript');

?>


var cpaint_interval_call_timeout = false;


/**
 * Make sure that a global variable of the given name exists,
 * initializing it to a new cpaint object if it isn't found.
 */
function ensure_cpaint_initialization(variable_name)
{
    var cpaint_var = (variable_name) ? variable_name : 'cp';
    if ( typeof window[cpaint_var] == 'undefined' )
    {
        window[cpaint_var] = new cpaint();
        window[cpaint_var].set_response_type('XML');
        window[cpaint_var].set_transfer_mode('POST');
    }//if
}//ensure_cpaint_initialization


/**
 * Fetch the current time from the Web server at the given interval,
 * calling the given callback as the response handler for the remote
 * cpaint call.  A non-empty ping interval is required.
 * 
 * By default, we simply call the cpaint_CurrentTime function in
 * shift/evals/eval_rpc_cpaint.php, which just returns the current
 * time on the server.  If no callback is specified, no action will be
 * taken when the server response is received.
 */
function exec_cpaint_call_at_interval(params)
{
    var default_backend = '<?php echo get_url_for('shift/evals/eval_rpc_cpaint.php'); ?>';
    var callback        = params['callback'];
    var confirmation    = params['confirmation'];
    var cpaint_var      = (params['cpaint_var']) ? params['cpaint_var'] : 'cp';
    var hours           = (params['hours'])    ? params['hours']    : 0;
    var minutes         = (params['minutes'])  ? params['minutes']  : 0;
    var seconds         = (params['seconds'])  ? params['seconds']  : 0;
    var remote_function = (params['function']) ? params['function'] : 'cpaint_CurrentTime';
    var remote_file     = (params['file'])     ? params['file']     : default_backend;

    var ping_interval  = (seconds * 1000) + (minutes * 60000) + (hours * 3600000);
    var ping_limit = parseInt(params['ping_limit']);
    ping_limit     = (isNaN(ping_limit)) ? -1 : ping_limit;
    if (    (ping_limit != -1 && ping_limit <= 0)
         || (!hours && !minutes && !seconds) 
       )
    {
        return;
    }//if

    cpaint_interval_call_timeout = setTimeout(
        function()
        {
            if ( !confirmation || confirm(confirmation) )
            {
                ensure_cpaint_initialization(cpaint_var);
                window[cpaint_var].call(remote_file, remote_function, callback);
                exec_cpaint_call_at_interval(
                    { 
                        'confirmation' : confirmation,
                        'cpaint_var'   : cpaint_var,
                        'file'         : remote_file,
                        'function'     : remote_function,
                        'ping_limit'   : ((ping_limit == -1) ? -1 : ping_limit - 1),
                        'callback'     : callback,
                        'hours'        : hours,
                        'minutes'      : minutes,
                        'seconds'      : seconds
                    }
                );
            }//if
        },
        ping_interval
    );
}//exec_cpaint_call_at_interval


/**
 *
 */
function stop_interval_cpaint_call()
{
    if ( cpaint_interval_call_timeout ) 
    {
        clearTimeout(cpaint_interval_call_timeout);
    }//if
}//stop_interval_cpaint_call


/**
 * Extremely simple example session-ping function.  This exists
 * primarily as a historical artifact.  The
 * exec_cpaint_call_at_interval() function is vastly more useful.
 */
function ping_session(callback)
{
    if ( typeof window.cp == 'undefined' )
    {
        window.cp = new cpaint();
        cp.set_response_type('XML');
        cp.set_transfer_mode('POST');
    }//if

    cp.call( 
        'eval_rpc_cpaint.php',
        'cpaint_CurrentTime',
        callback
    );
}//ping_session
