<?php 

function get_stored_item_reviews( $item_id, $connection ) {
	if ( !is_numeric($item_id) ) {
		return false;
	}//if

	$reviews = array();
	$query = 'SELECT * FROM ItemReviews WHERE item_id="'.$item_id.'"';
	$result = mysql_query($query,$connection);
	if ( !$result ) {
		return false;
	}//if

	$rows = array();
	while ($row = mysql_fetch_assoc($result)) {
		$rows[] = $row;
	}//while
	return $rows;
}//get_stored_reviews


function get_item_review( $review_id, $connection ) {
	if ( !is_numeric($review_id) ) {
		return false;
	}//if

	$reviews = array();
	$query = 'SELECT * FROM ItemReviews WHERE id="'.$review_id.'"';
	$result = mysql_query($query,$connection);
	if ( !$result ) {
		return false;
	}//if

	return mysql_fetch_assoc($result);
}//get_item_review


/**
 *
 */
function get_test_item_plaintext($item_id,$connection,$show_scenario=false) {
	$output = '';
	$item = get_test_item($item_id,$connection);
	if ( $show_scenario && $item['ScenarioText'] ) {
		$output .= 'Scenario:'."\n";
		$output .= $item['ScenarioText'];
		$output .= "\n---------\n";
	}//if

	if ( $show_scenario ) {
		$output .= 'Stem:'."\n";
	}//if

	$output .= $item['Stem']; 
	$output .= "\n---------\n\n";

	$distractors = get_test_item_distractors( $item['Item_id'], $connection );
	$num_distractors = count($distractors);

	// hopefully, there won't be more than 26 distractors on an item
	$LETTERS = array(
		'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
		'P','Q','R','S','T','U','V','W','X','Y','Z'
	);

	for ($i=0;$i < $num_distractors;$i++) {
		$distractor = $distractors[$i];
		if ( $distractor['CorrectAnswerFlag'] ) {
			$output .= '*';
		} else {
			$output .= ' ';
		}//else
		$output .= ' '.$LETTERS[$i].'. ';
		$output .= $distractor['Distractor_Text'];
		$output .= "\n\n";
	}//for

	return $output;
}//get_test_item_plaintext


/**
 * Fetch a test item from the database, as an assoc array. 
 *
 * @todo use a constant from asset_functions.html instead of $ASSET_TEST_ITEM
 */
function get_test_item( $item_id, $connection ) {
	$ASSET_TEST_ITEM = 17;

	if ( !is_numeric($item_id) ) {
		return false;
	}//if

	$query = 'SELECT items.*, '.
		'scenarios.ScenarioText, '.
		'projects.ProjectName, '.
		'assets.AssetDef_id '.

		'FROM (Item_def items, '.
		'Asset_def assets) '.

		// get the [possibly null] scenario
		'LEFT JOIN ScenarioData scenarios '.
		'ON items.Scenario_id=scenarios.Scenario_id '.

		// get the [possibly null] project name
		'LEFT JOIN ProjectTable projects '.
		'ON projects.Project_id=items.Project_id '.

		'WHERE items.Item_id="'.$item_id.'" '.
		'AND   assets.Data_id=items.Item_id '.
		'AND   assets.DataType_id="' . $ASSET_TEST_ITEM . '"';
	$result = mysql_query($query, $connection);
	if ( !$result || mysql_num_rows($result) != 1 ) {
		return false;
	}//if

	$item = mysql_fetch_assoc($result); 
	$item['Stem'] = stripslashes($item['Stem']);
	$item['ProjectName'] = stripslashes($item['ProjectName']);
	$item['ScenarioText'] = stripslashes($item['ScenarioText']);

	return $item;
}//get_test_item


function get_test_item_distractors( $item_id, $connection ) {
	$query = 'SELECT * FROM Item_Distractors '.
		'WHERE Item_id="'.$item_id.'" '.
		'ORDER BY DistractorOrder';
	$result = mysql_query($query,$connection);
	if ( !$result ) {
		return false;
	}//if

	$rows = array();
	while ($row = mysql_fetch_assoc($result)) {
		$row['Distractor_Text'] = stripslashes($row['Distractor_Text']);
		$row['Feedback'] = stripslashes($row['Feedback']);
		$rows[] = $row;
	}//while

	return $rows;
}//get_test_item_distractors

/**
 * Get the chapter(s) associated with the given asset
 *
 * @todo put $BOOK_PARENT_ASSET_ID in a shared constant somewhere
 */
function get_asset_chapters($asset_id, $connection) {
	$BOOK_PARENT_ASSET_ID = '4149';

	if ( !is_numeric($asset_id) ) {
		return false;
	}//if

	$query = 'SELECT A.AssetName as Chapter_Title '
		. 'FROM Asset_def A, CategoryData C '
		. 'WHERE C.Asset1_id="' . $asset_id . '" '
		. 'AND   C.Asset2_id=A.AssetDef_id '
		. 'AND A.Tree_id LIKE "' . $BOOK_PARENT_ASSET_ID . '.%"';
	$result = mysql_query($query, $connection);
	if ( !$result || !mysql_num_rows($result) ) {
		return false;
	}//if

	$chapters = array();
	while ( $row = mysql_fetch_assoc($result) ) {
		$chapters[] = $row['Chapter_Title'];
	}//while
	return $chapters;
}//get_asset_chapters 


/**
 * Dump a static HTML view of a test item.
 */
function display_test_item($item_id,$connection,$mleft = '15%',$mright = '15%') {
	$has_feedback = 0;
	echo "<script language='JavaScript'>\n";
	echo "function getElementsByClassName(classname, node)\n";
	//adapted from http://snipplr.com/view/1696/get-elements-by-class-name/
	echo "{\n";
	echo "  if(!node)\n";
	echo "	{\n";
	echo "	  node = document.getElementsByTagName(\"body\")[0];\n";
	echo "  }\n";
	echo "  var a = new Array();\n";
	echo "  var els = node.getElementsByTagName(\"*\");\n";
	echo "  for(var i=0,j=els.length; i<j; i++) {\n";
	echo "    if(els[i].className == classname) {\n";
	echo "      a.push(els[i]);\n";
	echo "    }\n";
	echo "  }\n";
	echo "  return a;\n";
	echo "}\n";
	echo "function toggle_feedback() {\n";
	echo "	var elems;\n";
	echo "	elems = getElementsByClassName('distractor_feedback');\n";
	echo 	"t_button=document.getElementById('togglefeedback');\n";
	echo "	var style='block';\n";
	echo "	if(elems[0].style.display=='none')\n";
	echo "	{\n";
	echo "		style='block';\n";
	echo "		t_button.value='Hide Feedback';\n";
	echo "	}\n";
	echo "	else\n";
	echo "	{\n";
	echo "		style='none';\n";
	echo "		t_button.value='Show Feedback';\n";	
	echo "	}\n";
	echo "	for(i=0;i<elems.length;i++)\n";
	echo "	{\n";
	echo "		elems[i].style.display=style;\n";
	echo "	}\n";
	echo "}\n";
	echo "</script>\n";
	echo "<div class='item_container' style='margin-left:$mleft; margin-right:$mright;'>";

	// get the item and its distractors
	$item = get_test_item($item_id,$connection);
	$distractors = get_test_item_distractors( $item['Item_id'], $connection );

	// for now, the item type can only be determined by the number of
	// distractors
	$num_distractors = count($distractors);
	if ( $num_distractors == 1 ) {
		$item_type = 'short_answer';
		$item_type_string = 'Short Answer';
	} else if ( $num_distractors == 2 ) {
		$item_type = 'true_false';
		$item_type_string = 'True/False';
	} else {
		$item_type = 'multiple_choice';
		$item_type_string = 'Multiple Choice';
	}//else

	// add a display of chapters the item's linked to
	$chapters = get_asset_chapters($item['AssetDef_id'], $connection);
	$chapter_string = '';
	if ( $chapters ) {
		$chapter_string .= '<p class="item_chapter_linkage">(linked to: ';
		$num_chapters = count($chapters);
		for ($i=0;$i < $num_chapters;$i++) {
			// add commas for successive entries
			if ( $i > 0 ) {
				$chapter_string .= ', ';
			}//if

			$chapter_string .= htmlentities(
				stripslashes(
					$chapters[$i]
				)
			);
		}//for
		$chapter_string .= ')</p>';
	}//if

	$project_string = (isset($item['ProjectName'])&&$item['ProjectName']) ?
		'Project: ' . htmlentities($item['ProjectName']) :
		'Project: n/a';

	echo '<strong>'
		. 'Item #' . $item['Item_id']
		. '</strong> '
		//. 'Type: ' . $item_type_string 
		. $project_string
		. '<br>'
		. $chapter_string;

	if ( $item['ScenarioText'] ) {
		echo '<div class="item_scenario">';
		echo '<div class="item_scenario_heading">';
		echo 'Scenario:';
		echo '</div>';
		echo '<div class="item_scenario_text">';
		echo $item['ScenarioText'];
		echo '</div>';
		echo '</div>';
	}//if

	echo '<div class="item_stem">';
	echo '<div class="item_stem_heading">';
	echo 'Stem:';
	echo '</div>';
	echo '<div class="item_stem_text">';
	echo $item['Stem']; 
	echo '</div>';
	echo '</div>';

	// hopefully, there won't be more than 26 distractors on an item
	$LETTERS = array(
		'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
		'P','Q','R','S','T','U','V','W','X','Y','Z'
	);

	$i = 0;
	for ($i=0;$i < $num_distractors;$i++) {
		$distractor = $distractors[$i];
		echo '<div class="distractor">';
		echo '<div class="spacer_row"></div>';
		echo '<div class="distractor_text';
		if ( $distractor['CorrectAnswerFlag'] ) {
			echo ' correct_answer';
		}//if 
		echo '">';

		// only multiple choice items get letters on the distractors
		if ( $item_type == 'multiple_choice' ) {
			echo $LETTERS[$i].'. ';
		}//if

		echo $distractor['Distractor_Text'];
		echo '</div>';

		if ( $distractor['Feedback'] ) {
			$has_feedback = 1;
			echo '<div class="distractor_feedback" style="display:none">';
			echo '<strong>Feedback:</strong>&nbsp;&nbsp;';
			echo $distractor['Feedback'];
			echo '</div>';
		}//if

		echo '<div class="spacer_row"></div>';
		echo '</div>';
	}//for
	if ($has_feedback == 1) {
		echo "<div><input type='button' name='togglefeedback' id='togglefeedback' value='Show Feedback' onClick='toggle_feedback()'></div>\n";
		echo '<div class="spacer_row"></div>';
	}
	echo '</div>';
}//display_test_item


/**
 * Get the average value reviewers entered for the
 * percent_minimally_qualified field for the given test item, or -1 if
 * there was an error.  The value returned *should* be the average projected cut
 * score for the test item (but many items were originally given
 * skewed percent_minimally_qualified values, so our mileage may vary).
 *
 * @TODO rename this function... it's not terribly descriptive as is
 *
 * @param int $item_id the item whose reviews we want to check
 * @param mysql_link $connection the db connection to use
 * @return int the average percent_minimally_qualified value
 */
function get_test_item_average_percent_mac($item_id, $connection) {

	$query = 'SELECT Sum(percent_minimally_qualified) as Sum, count(*) as Count '.
		'FROM ItemReviews '.
		'WHERE item_id="'.$item_id.'"';

	$result = mysql_query($query, $connection);
	if (!$result || mysql_num_rows($result) != 1) {
		return -1;
	}//if

	$Sum = mysql_result( $result, 0, "Sum");
	$Count = mysql_result( $result, 0, "Count");
	$Percent = -1;

	if ($Count > 0) {	
		$Percent = $Sum / $Count;
	}//if

	return $Percent;
}//get_test_item_average_percent_mac

?>
