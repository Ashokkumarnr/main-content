<?php

/**
 * This file defines constants that affect how the FISDAP session management
 * features work.
 * 
 * @author Sam Martin <smartin@fisdap.net>
 */

//////////////////////////////////////////////////
// ensure inclusion of the global configuration settings
//////////////////////////////////////////////////

require_once('phputil/config.php');


//////////////////////////////////////////////////
// the session expiry time settings
//////////////////////////////////////////////////

define('SESSION_EXPIRATION_INTERVAL_HOURS',   24);
define('SESSION_EXPIRATION_INTERVAL_MINUTES',  0);
define('SESSION_EXPIRATION_INTERVAL_SECONDS',  0);


//////////////////////////////////////////////////
// the session database properties
//////////////////////////////////////////////////

/** the database table where FISDAP sessions should be stored */
define('FISDAP_SESSION_TABLE', 'Sessions');


//////////////////////////////////////////////////
// session data storage properties
//////////////////////////////////////////////////

/** the prefix for session keys */
define('SESSION_KEY_PREFIX', 'FISDAP_SESSION_');

?>
