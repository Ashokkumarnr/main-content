/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

function handleKeystroke(e, funct, argString)
{
    var keyPressed;

    //Browser compatibility check
    if (document.all) 
    {
        //Browser used: Internet Explorer 6
        keyPressed = e.keyCode;
        //alert('handleKeystroke: IE property: keyCode');
    } 
    else 
    {
        //Browser used: Firefox
        keyPressed = e.which;
        //alert('handleKeystroke: FF property: which');
    }
   // alert('handleKeystroke: key=' + keyPressed);

    //13 = ASCII code for Enter key
    if (keyPressed == 13) 
    { 
        // alert('handleKeystroke: pressed Enter');
        funct(argString); 
    } 
    else 
    {
        //alert('handleKeystroke: pressed another key');
    }
}
