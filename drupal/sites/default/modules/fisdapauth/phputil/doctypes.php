<?php

/**
 * This file defines constants for the most common XHTML/HTML/XML
 * document types.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 *                                                                           *
 * @package FISDAP_Constants                                                 *
 * @author Sam Martin <smartin@fisdap.net>                                   *
 * @copyright 1996-2007 Headwaters Software, Inc.                            *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 */


//strict HTML 4.01, the default standard for HTML documents
define(
    'HTML_401_STRICT',
    '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" '
    . '"http://www.w3.org/TR/html4/strict.dtd">'
);

//transitional HTML 4.01, for documents using frames
define(
    'HTML_401_TRANSITIONAL',
    '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" '
    . '"http://www.w3.org/TR/html4/loose.dtd">'
);

//a variant of HTML 4.01 transitional that allows framesets
define(
    'HTML_401_FRAMESET',
    '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" '
    . '"http://www.w3.org/TR/html4/frameset.dtd">'
);

//the XHTML equivalent of the HTML 4.01 strict standard
define(
    'XHTML_10_STRICT',
    '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" '
    . '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'
);

?>
