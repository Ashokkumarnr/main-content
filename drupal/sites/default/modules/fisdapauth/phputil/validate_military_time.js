/**
 * Validate a 4-digit military-format time entry.
 */
function validate_military_time(time_string) {
    if ( time_string == "" ) {
        throw new Error(
            'Please fill in the time field.'
        );
    }//if

    // bail out on > 4 digits
    if ( time_string.length > 4 ) {
        throw new Error(  
            'You entered more than 4 digits for a time. '+
            'Please try again, using military (24-hour) time.'
        );
    }//if

    // make the length match our expectations (pad to 4 digits)
    while ( time_string.length < 4 ) {
        time_string = '0' + time_string;
    }//while

    // parse the hours (must be between 00 and 23)
    validate_military_time_hours(time_string);
    
    // parse the minutes (must be between 00 and 59)
    validate_military_time_minutes(time_string);

    return time_string;
}//validate_military_time


function validate_military_time_hours(time_string) {
    var hours = parseInt(time_string.substring(0,2));
    if ( isNaN(hours) ||
         hours < 0    ||
         hours > 23
       ) {
        throw new Error(
            'The first two digits of a time field must be '+
            'between 00 and 23.  Please try again, using military '+
            '(24-hour) time.'
        );
    }//if
}//validate_military_time_hours


function validate_military_time_minutes(time_string) {
    var minutes = parseInt(time_string.substring(2));
    if ( isNaN(minutes) ||
         minutes < 0    ||
         minutes > 59 
       ) {
        throw new Error(
            'The last two digits of a time field must be '+
            'between 00 and 59.  Please try again, using military '+
            '(24-hour) time.'
        );
    }//if
}//validate_military_time_minutes
