<?php
require_once('phputil/handy_utils.inc');

/**
 * Determine if a user is a developer.
 * @param string|boolean $user The user name of FALSE for the default.
 * @return TRUE if the user is a developer.
 */
function is_fisdap_staff_member($user=false) {

	if (!$user) {
		$user = $_SERVER['PHP_AUTH_USER'];
	}

    $users = array(
        'csoucheray', 
        'mmayne', 
        'erikh', 
        'khanso2810',
        'lbriguglio', 
        'mjohnson', 
		'rbaird',
		'mbogucki',
	'sdesombre',
	'stape',
	'gphilip',
	'cpond'
    );

	return !!in_array(strtolower($user),$users);
}

/**
 * Determine if a user is a developer.
 * @param string|null $user The user name of NULL for the default.
 * @return TRUE if the user is a developer.
 */
function is_fisdap_developer($user=null) {

	if (is_null($user)) {
		$user = $_SERVER['PHP_AUTH_USER'];
	}

    $users = array(
        'eoneil', 
        'erikh', 
        'iyoung', 
	'khanso2810',
        'mjohnson', 
	'stape',
	'cpond'
    );

	return !!in_array(strtolower($user),$users);
}

/**
 * @param string $user the username to check for. If NULL, the current user will be used.
 * @param mixed $extras usernames or arrays to be checked against $user
 *
 * @return TRUE if $user or the current user exists in the additional arguments, FALSE otherwise.
 */
function is_user_in($user, $extras) {

	if (!$user) {
		$user = $_SERVER['PHP_AUTH_USER'];
	}

	$args = func_get_args();
	// Don't include $user in the array
	array_shift($args);

	return HandyArrayUtils::in_array_recursive($user, $args);
}

// Like is_fisdap_staff_member() except for just Mike and Erik
function is_fisdap_management($user=false) {

	if (!$user) {
		$user = $_SERVER['PHP_AUTH_USER'];
	}

	$topdogs = array( 'erikh', 'mjohnson');

	if (in_array(strtolower($user), $topdogs)) {
		return true;
	}
	return false;
}

/*
 * FUNCTION require_staff_member - Turn away any user not listed as a FISDAP staff member.
 * @param array $extras array of usernames to be included
*/

function require_staff_member($extras = array()) {

	if (!(is_fisdap_staff_member() || is_user_in(null,$extras))) {
		die('Only FISDAP staff members are allowed to access the content you requested.  We apologize for the inconvenience.');
	}

}

?>
