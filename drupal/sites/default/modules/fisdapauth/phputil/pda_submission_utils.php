<?php 

/**
 *
 */
function check_manifest_completion($manifest_submission) {
    $manifest = unserialize($manifest_submission['postdata']);
    $manifest_created_at = $manifest_submission['manifest_created_at'];
    $remote_user = $manifest_submission['user_name'];
    $avantgo_device_id = $manifest_submission['avantgo_device_id'];

    $connection =& FISDAPDatabaseConnection::get_instance();

    $crossreference = array();

    // consult the manifest to see if we're done collecting form submissions
    $entities = array(
        'Preceptor','Shift','Run','Assess','IV','EKG','Med',
        'Airway','BLS','Narrative','Other','PtComp',
        'EvalSession'
    );
    foreach( $entities as $entity ) {
        $entity_count = count_stored_pda_submissions(
            $entity, 
            $remote_user, 
            $manifest_created_at,
            $avantgo_device_id,
            $connection->get_link_resource() 
        );
        if ( !isset( $manifest['Manifest-1'.$entity.'_count'] ) ) {
            return false;
        } else {
            $crossreference[$entity] = array(
                'found'    => $entity_count,
                'expected' => $manifest['Manifest-1'.$entity.'_count']
            );
        }//else
    }//foreach

    return $crossreference; 
}//check_manifest_completion


/**
 *
 */
function get_pda_submission($submission_id) {
    if ( !is_numeric($submission_id) ) {
        return false;
    }//if

    $connection =& FISDAPDatabaseConnection::get_instance();
    $query = 'SELECT * '
        . 'FROM PDAFormSubmissions '
        . 'WHERE id="' . $submission_id . '"';
    $result = $connection->query($query);

    if ( !is_array($result) || count($result) != 1 ) {
        return false;
    }//if

    return $result[0];
}//get_pda_submission


/**
 *  
 */
function get_pda_id_map($submission_id) {
    if ( !is_numeric($submission_id) ) {
        return false;
    }//if

    $submission = get_pda_submission($submission_id);
    if ( is_array($submission) && isset($submission['id_map']) ) {
        // decode the id map
        $raw_map = $submission['id_map'];
        $decoded_map = base64_decode($raw_map);
        $serialized_id_map = gzuncompress($decoded_map);
        $id_map = unserialize($serialized_id_map);

        return $id_map;
    } else {
        return false;
    }//else
}//get_pda_id_map


/**
 *
 */
function count_stored_pda_submissions( $entity_name, 
                                       $user_name, 
                                       $manifest_created_at,
                                       $avantgo_device_id,
                                       $connection ) {
    $query = 'SELECT COUNT(*) FROM PDAFormSubmissions WHERE '.
             'entity_name=\''.$entity_name.'\' AND '.
             'user_name=\''.$user_name.'\' AND '.
             'manifest_created_at=\''.$manifest_created_at.'\' AND '.
             'avantgo_device_id=\''.$avantgo_device_id.'\'';
    $result = mysql_query($query,$connection);
    if ( !$result ) {
        return -1;
    }//if

    $row = mysql_fetch_assoc($result);
    return $row['COUNT(*)'];
}//count_stored_pda_submissions


/**
 * 
 */
function get_stored_pda_postdata( $entity_name, 
                                  $entity_id, 
                                  $user_name, 
                                  $manifest_created_at,
                                  $avantgo_device_id,
                                  $connection ) {
    $query = 'SELECT postdata FROM PDAFormSubmissions WHERE '.
             'entity_name=\''.$entity_name.'\' AND '.
             'entity_id=\''.$entity_id.'\' AND '.
             'user_name=\''.$user_name.'\' AND '.
             'manifest_created_at=\''.$manifest_created_at.'\' AND '.
             'avantgo_device_id=\''.$avantgo_device_id.'\'';
    //log_debug_message('executing query: '."\n".$query."\n");
    $result = mysql_query($query,$connection);
    if ( !$result ) {
        log_debug_message('mysql_query: error: '.mysql_error($connection));
        return $result;
    }//if

    $row = mysql_fetch_assoc($result);
    $serialized_postdata = $row['postdata'];
    //log_debug_message('read postdata from db: '."\n".$serialized_postdata);
    return unserialize($serialized_postdata);
}//get_stored_pda_postdata


function get_stored_pda_entity_ids( $entity_name, 
                                    $user_name, 
                                    $manifest_created_at,
                                    $avantgo_device_id,
                                    $connection ) {
    $query = 'SELECT entity_id FROM PDAFormSubmissions WHERE '.
             'entity_name=\''.$entity_name.'\' AND '.
             'user_name=\''.$user_name.'\' AND '.
             'manifest_created_at=\''.$manifest_created_at.'\' AND '.
             'avantgo_device_id=\''.$avantgo_device_id.'\'';
    $result = mysql_query($query,$connection);
    if ( !$result ) {
        return $result;
    }//if

    $entity_ids = array();
    while( $row = mysql_fetch_assoc($result) ) {
        $entity_ids[] = $row['entity_id'];
    }//while
    return $entity_ids;
}//get_stored_pda_entity_ids


?>
