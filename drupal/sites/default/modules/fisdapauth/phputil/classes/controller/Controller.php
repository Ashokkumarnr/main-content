<?php

/**
 * This is the base class for all controllers
 */
class Controller {

    ////////////////////////////////////////////////////////////
    // Protected instance variables
    ////////////////////////////////////////////////////////////

    /** a list of actions requests are allowed to invoke */
    var $allowed_actions;


    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     *
     */
    function Controller() {
        $this->allowed_actions = array();
    }//Controller


    ////////////////////////////////////////////////////////////
    // Public instance methods
    ////////////////////////////////////////////////////////////

    /**
     * Dispatch the given incoming request to the appropriate 
     * action
     *
     * @param Request $request the incoming request 
     */
    function dispatch($request) {
        $action = $request->get_action();
        $input = $request->get_input();
        if ( method_exists($this,$action) && 
             in_array($action,$this->allowed_actions) ) {
            $this->$action($input);
        } else {
            return false;
        }//else
        
        return true;
    }//dispatch


    ////////////////////////////////////////////////////////////
    // Public static methods
    ////////////////////////////////////////////////////////////

    /**
     *
     */
    function strip_slashes_from_postdata($input) {
        return array_map('stripslashes',$input);
    }//strip_slashes_from_postdata
}//class Controller

?>
