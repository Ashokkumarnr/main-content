<?php


/**
 * This is a controller to dispatch actions concerning 
 * the item review form
 */
class ConsensusTestItemReviewFormController extends Controller {

    ////////////////////////////////////////////////////////////
    // Private instance variables
    ////////////////////////////////////////////////////////////

    /** the InputValidationSet to use on the input */
    var $input_validation_set;

    /** the mysql link resource to use for database connectivity */
    var $connection;


    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     * Create a form controller for the given form and input.
     */
    function ConsensusTestItemReviewFormController() {
        $this->Controller();

        // get the database connection
        $connection_obj =& FISDAPDatabaseConnection::get_instance();
        $this->connection = $connection_obj->get_link_resource();

        // set the actions requests are allowed to invoke
        $this->allowed_actions = array('display','submit','edit');
    }//ConsensusTestItemReviewFormController


    ////////////////////////////////////////////////////////////
    // Public instance methods
    ////////////////////////////////////////////////////////////

    /**
     * Display a blank review form
     */
    function display($request) {
        $item_id = 0;

        // validate the item id
        $item_id_validator = new IntegerValidator('item_id');
        $item_id_validator->validate($request);

        if ( $item_id_validator->has_errors() ) {
            // display the first error message
            $errors = $item_id_validator->get_errors();
            $first_error = $errors[0];
            die($first_error->get_message());
        } else {
            $item_id = $request['item_id'];
        }//else

        $review_form =& new ConsensusTestItemReviewForm($item_id);
        $review_form->populate_with_defaults();
        $item_review_page =& new TestItemReviewPage($item_id,$review_form);
        $item_review_page->display();
    }//display


    /**
     * Display an existing review form, and allow edits
     */
    function edit($request) {
        // display the form, filled in
        $review_id_validator = new IntegerValidator('review_id');
        $review_id_validator->validate($request);

        if ( $review_id_validator->has_errors() ) {
            // display the first error message
            $errors = $review_id_validator->get_errors();
            $first_error = $errors[0];
            die($first_error->get_message());
        } else {
            $review_id = $request['review_id'];
            $review = get_item_review($review_id,$this->connection);
            $item_id = $review['item_id'];
        }//else

        $review_form =& new ConsensusTestItemReviewForm($item_id);
        $item_review_page =& new TestItemReviewPage($item_id,$review_form);
        $review_form->populate($review);
        $item_review_page->display();
    }//edit


    function submit($request) {
        $review_form =& new ConsensusTestItemReviewForm($request['item_id']);
        $item_review =& TestItemReview::from_assoc_array($request);

        if( $review_id = $item_review->save() ) {
            // success!  display a success notification
            $request = Controller::strip_slashes_from_postdata($request);
            $review_page = new TestItemReviewPage($request['item_id'],$review_form);
            $message = 'Successfully '.(($request['id'])?'updated':'created').' the item review.';
            $request['id'] = $review_id;
            $review_form->populate($request);
	    $review_form->add_generic_error(new ValidationError($message));
	    //$review_form->add_javascript('window.open("","_self"); setTimeout("window.close();",2000);');
            $review_form->add_javascript('setTimeout("window.location=\'groupReviewAssignments.php\';",2000);');
            $review_page->display();
        } else {
            // failure!  display the form with error messages
            $request = Controller::strip_slashes_from_postdata($request);
            $review_page = new TestItemReviewPage($request['item_id'],$review_form);
            $review_form->populate($request);
            $review_form->add_generic_error(new ValidationError('Your submission could not be processed.  Please check the form below for any errors.'));
            $review_form->add_errors($item_review->get_errors());
            $review_page->display();
        }//else
    }//submit


    /**
     * Get the InputValidationSet for the form
     * 
     * @return InputValidationSet the validation set for the form
     */
    function &get_input_validation_set() {
        return $this->input_validation_set;
    }//get_input_validation_set


    /**
     * Set the InputValidationSet for the form
     *
     * @param InputValidationSet $set the validation set to use
     */
    function set_input_validation_set(&$set) {
        $this->input_validation_set =& $set;
    }//set_input_validation_set


    /**
     * Make sure the user has access to the consensus form.
     */
    function verify_consensus_access($request) {
        // for the time being, don't restrict access to the consensus review form
        return true;

        // @todo  we need a way to limit access to this controller
        //        (perhaps as part of our upcoming context-sensitive
        //        role-based authentication scheme?)

        //return in_array(
        //    $_SERVER['PHP_AUTH_USER'],
        //    array(
        //        'itest276','lbriguglio','dpage'
        //    )
        //);
    }//verify_consensus_access
}//class ConsensusTestItemReviewFormController

?>
