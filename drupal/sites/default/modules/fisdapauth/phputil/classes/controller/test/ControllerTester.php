<?php


require_once('phputil/classes/controller/Controller.php');
require_once('phputil/classes/http/Request.php');


/**
 * This is a test controller subclass to see if the dispatch method calls the 
 * appropriate actions, if any.
 */
class DummyController extends Controller {
    var $test_action_1_has_been_called;
    var $test_action_2_has_been_called;
    var $secret_action_has_been_called;
    
    /**
     *
     */
    function DummyController() {
        $this->test_action_1_has_been_called = false;
        $this->test_action_2_has_been_called = false;
        $this->secret_action_has_been_called = false;
        $this->allowed_actions = array('test_action_1','test_action_2');
    }//DummyController
    
    
    /**
     * 
     */
    function test_action_1($input) {
        $this->test_action_1_has_been_called = true;
    }//test_action_1
    
    
    /**
     *
     */
    function test_action_2($input) {
        $this->test_action_2_has_been_called = true;
    }//test_action_2
    
    /**
     *
     */
    function secret_action($input) {
        $this->secret_action_has_been_called = true;
    }//secret_action
}//class DummyController


/**
 * This class contains unit tests for the Controller class
 */
class ControllerTester extends UnitTestCase {
    
    function setUp() {
    }//setUp

   
    function tearDown() {
    }//tearDown

    
    function test_dispatch() {
        // try test action 1
        $dummy_controller1 =& new DummyController();
        $test_action_1_input = array(
            'action'=>'test_action_1',
            'test_input'=>'test_input_val'
        );

        $_GET = $test_action_1_input;
        $request1 =& new Request();
        $dummy_controller1->dispatch($request1);
        $this->assertTrue($dummy_controller1->test_action_1_has_been_called,'1: action wasn\'t called');
        $this->assertFalse($dummy_controller1->test_action_2_has_been_called,'1: 2nd test action was called');
        $this->assertFalse($dummy_controller1->secret_action_has_been_called,'1: secret action was called');
        $_GET = array();
        
        // try test action 2
        $dummy_controller2 =& new DummyController();
        $test_action_2_input = array(
            'action'=>'test_action_2',
            'test_input'=>'test_input_val'
        );
        $_GET = $test_action_2_input;
        $request2 =& new Request();
        $dummy_controller2->dispatch($request2);
        $this->assertFalse($dummy_controller2->test_action_1_has_been_called,'2: 1st test action was called');
        $this->assertTrue($dummy_controller2->test_action_2_has_been_called,'2: action wasn\'t called');
        $this->assertFalse($dummy_controller2->secret_action_has_been_called,'2: secret action was called');
        $_GET = array();
        
        // try the secret action (should be disallowed)
        $dummy_controller3 =& new DummyController();
        $secret_action_input = array(
            'action'=>'secret_action',
            'test_input'=>'test_input_val'
        );
        $_GET = $secret_action_input;
        $request3 =& new Request();
        $this->assertFalse($dummy_controller3->dispatch($request3),'returned true on invalid action');
        $this->assertFalse($dummy_controller3->test_action_1_has_been_called,'3: 1st test action was called');
        $this->assertFalse($dummy_controller3->test_action_2_has_been_called,'3: 2nd test action was called');
        $this->assertFalse($dummy_controller3->secret_action_has_been_called,'3: secret action was called');
        $_GET = array();
    }//test_dispatch
    
}//class ControllerTester

?>
