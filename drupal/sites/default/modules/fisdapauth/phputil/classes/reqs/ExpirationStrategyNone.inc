<?php
require_once('phputil/classes/reqs/ExpirationStrategy.inc');

/**
 * An expiration date strategy that indicates there is no expiration date.
 */
final class ExpirationStrategyNone extends ExpirationStrategy {
    public function get_expiration_date($timestamp) {
        // Invoking the parent ensures the validation is done.
        parent::get_expiration_date($timestamp);

        return FisdapDateTime::not_set();
    }
}
?>
