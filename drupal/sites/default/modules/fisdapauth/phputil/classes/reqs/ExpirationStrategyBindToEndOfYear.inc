<?php
require_once('phputil/classes/DateTime.inc');
require_once('phputil/classes/reqs/ExpirationStrategy.inc');

/**
 * An expiration date strategy that binds to the last day of the year.
 */
final class ExpirationStrategyBindToEndOfYear extends ExpirationStrategy {
    public function get_expiration_date($timestamp) {
        $date = parent::get_expiration_date($timestamp);
		if (!$date->is_set()) return $date;

        // Bind it.
		return new FisdapDateTime(
			FisdapDate::create_from_ymd(
				$date->get_year(), 
				12, 
				31),
			$date->get_time());
    }
}
?>
