<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FisdapDateTime.inc');
require_once('phputil/classes/FisdapLogger.inc');
require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/classes/model/reqs/ReqApplicationModel.inc');
require_once('phputil/classes/model/reqs/ReqApplicationHistoryModel.inc');
require_once('phputil/classes/model/reqs/ReqHistoryModel.inc');
require_once('phputil/classes/model/reqs/ReqRelationshipModel.inc');
require_once('phputil/classes/model/reqs/ReqRelationshipHistoryModel.inc');
require_once('phputil/classes/reqs/ReqApplication.inc');
require_once('phputil/classes/reqs/ReqRelationship.inc');
require_once('phputil/classes/reqs/ReqAllStats.inc');
require_once('phputil/classes/reqs/ReqUtils.inc');
require_once('phputil/classes/common_user.inc');
require_once('phputil/string_utils.php');

/**
 * The business logic for a requirement.
 * No security is performed here.  If you call the method you get to use it.
 */
final class Req {
    private $logger;
    private $requirement;
    private $user;

    const TYPE_NAME = 'Requirement';

	/**
	 * Constructor.
	 * @param ReqModel $requirement The underlying requirement.
	 * @param common_user $user The user making the change.
	 */
	public function __construct(ReqModel $requirement, common_user $user) {
        $this->requirement = $requirement;
        $this->user = $user;
        $this->logger = FisdapLogger::get_logger();
    }

    private function get_log_prefix() {
        return 'Req[' . $this->requirement->get_id() . ']';
    }

	/**
	 * Create the requirement in the DB.
	 * @param string | null $history_text The text to use for the history entries.
     * @return ReqAllStats Statistics.
	 * @throws FisdapDatabaseException if a problem occurs.
	 */
	public function create($history_text=null) {
        ReqUtils::verify_does_not_exist($this->requirement, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Created');

        $stats = new ReqAllStats();

        $this->requirement->set_creation_date(FisdapDateTime::now());
        $this->requirement->save();

        $stats->get_req_stats()->increment_created_count(1);
        $stats->merge($this->add_history($history_text));

        $this->logger->debug($this->get_log_prefix() . ': created');
        return $stats;
    }

    /**
     * Delete the requirement from the DB.
     * The requirement is simply flagged as deleted.
     * No other tables are modified.
	 * @param string | null $history_text The text to use for the history entries.
     * @return ReqAllStats Statistics.
     */
    public function delete($history_text=null) {
        ReqUtils::verify_exists($this->requirement, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Deleted');

        $this->logger->debug($this->get_log_prefix() . ': delete started');

        $stats = new ReqAllStats();

        $this->requirement->set_deleted(true);
        $this->requirement->save();

        $stats->get_req_stats()->increment_marked_as_deleted_count(1);
        $stats->merge($this->add_history($history_text));

        $this->logger->debug($this->get_log_prefix() . ': delete complete');
        return $stats;
    }

	/**
	 * Delete the requirement from the DB in a cascading fashion.
     * Users that reference this requirement but have NOT completed it will have
     * their requirement entry removed.
     * Any other deletions are marked as deleted but still exist.
	 * @param string | null $history_text The text to use for the history entries.
     * @return ReqAllStats Statistics.
	 * @throws FisdapDatabaseException if a problem occurs.
	 */
	public function cascade_delete($history_text=null) {
        ReqUtils::verify_exists($this->requirement, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Deleted');

        $this->logger->debug($this->get_log_prefix() . ': cascading delete started');

        $id = $this->requirement->get_id();

		$text = "Requirement[$id] was deleted";
		$stats = ReqApplication::delete_by_req_id($id, $this->user, $text);
		$stats->merge(ReqRelationship::delete_by_req_id($id, $this->user, $text));

        // Delete the requirement.
        $this->logger->debug($this->get_log_prefix() . ': marking as deleted');

        $this->requirement->set_deleted(true);
        $this->requirement->save();

        $stats->merge($this->add_history($history_text));
        $this->logger->debug($this->get_log_prefix() . ': cascading delete complete');

        $stats->get_req_stats()->increment_marked_as_deleted_count(1);
        return $stats;
    }

	/**
	 * Update the requirement in the DB.
     * If you need a new version of the requirement, you should set the version
     * number prior to calling this method.
	 * @param string | null $history_text The text to use for the history entries.
     * @return ReqAllStats Statistics.
	 * @throws FisdapDatabaseException if a problem occurs.
	 */
    public function update($history_text=null) {
        ReqUtils::verify_exists($this->requirement, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Modified');

        $this->logger->debug($this->get_log_prefix() . ': updating started');

        $this->requirement->save();

        $stats = new ReqAllStats();
        $stats->get_req_stats()->increment_modified_count(1);
        $stats->merge($this->add_history($history_text));

        $this->logger->debug($this->get_log_prefix() . ': updating completed');
        return $stats;
    }

    private function add_history($description) {
        $history = new ReqHistoryModel();
        $history->set_description($description);
        $history->set_entry_time(FisdapDateTime::now());
        $history->set_req_id($this->requirement->get_id());
        $history->set_user_id($this->user->get_UserAuthData_idx());
        $history->save();

		$stats = new ReqAllStats();
        $stats->get_req_stats()->increment_history_created_count(1);
		return $stats;
    }

	/**
	 * The requirement has been completed.
	 * The completion date will be filled in if it is not set.
	 * The expiration date will be filled based on the completion date.
	 * @param common_user | mixed $user The user that completed the requirement.
	 * @param FisdapDateTime $completed_date The completion date.
	 * @param FisdapDate | null $expiration_date The expiration date or null to 
	 * compute.
     * @return ReqAllStats Statistics.
	 */
	public function completed(common_user $user, FisdapDateTime $completed_date, 
		$expiration_date) {

	    Assert::is_true(
			Test::is_null($expiration_date) ||
			Test::is_a($expiration_date, 'FisdapDate'));

		$user_id = $user->get_UserAuthData_idx();
		$this->logger->debug($this->get_log_prefix() . 
			": completed by user ID[$user_id] started");

		// Loop over the applications that reference this requirement and effect 
		// the user.
		$models = ReqApplication::find_applications_effecting_user($user, 
			array(), null);
		$stats = new ReqAllStats();
		foreach ($models as $model) {
			$application = new ReqApplication($model, $this->user);
			$stats->merge($application->completed($user, $completed_date,
				$expiration_date));
		}

		$this->logger->debug($this->get_log_prefix() . 
			": completed by user ID[$user_id] complete");
		return $stats;
	}
}
?>
