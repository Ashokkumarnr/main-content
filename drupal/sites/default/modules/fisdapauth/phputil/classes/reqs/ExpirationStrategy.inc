<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/DateTime.inc');

/**
 * This class models a simple expiration date strategy.
 */
class ExpirationStrategy {
    private $days;
    private $months;
    private $years;

    /**
     * Retrieve the number of days to increment the date.
     * @return int The number of days.
     */
    public function get_day_increment() {
        return $this->days;
    }

    /**
     * Set the number of days to increment the date.
     * @param int $days The number of days.
     */
    public function set_day_increment($days) {
        Assert::is_int($days);
        $this->days = $days;
    }

    /**
     * Retrieve the number of months to increment the date.
     * @return int The number of months.
     */
    public function get_month_increment() {
        return $this->months;
    }

    /**
     * Set the number of months to increment the date.
     * @param int $months The number of months.
     */
    public function set_month_increment($months) {
        Assert::is_int($months);
        $this->months = $months;
    }

    /**
     * Retrieve the number of years to increment the date.
     * @return int The number of years.
     */
    public function get_year_increment() {
        return $this->years;
    }

    /**
     * Set the number of years to increment the date.
     * @param int $years The number of years.
     */
    public function set_year_increment($years) {
        Assert::is_int($years);
        $this->years = $years;
    }

    /**
     * Retrieve the expiration date.
     * Simply adds the increments to the specified time.
     * Override this method to bind the date to a particular interval, like year end.
	 * The date is bound to the end of day.
     * @param int | null $timestamp The time stamp to start with or NULL.
     * @return FisdapDateTime The expiration date or FisdapDateTime::not_set() for none.
     * @throws FisdapException if the expiration date can not be computed.
     */
    public function get_expiration_date($timestamp) {
        if (is_null($timestamp)) return FisdapDateTime::not_set();

        Assert::is_int($timestamp);
        Assert::is_true($timestamp >= 0);

        // Form the increment as a string.
        $increment = $this->get_day_increment() . ' days ' .
			$this->get_month_increment() . ' months ' .
			$this->get_year_increment() . ' years';

        // In PHP 5.3.0 we can use the 'add' and 'getTimestamp' methods.
        $date = date('Y-m-d H:i:s', $timestamp);

        $date_time = new DateTime($date);
        $date_time->modify($increment);
        $expiration_date = $date_time->format('Y-m-d');
        if ($expiration_date === false) {
            throw new FisdapException('Unable to compute expiration date from' .
                " date[$date] timestamp[$timestamp] increment[$increment]");
        }

		return FisdapDateTime::end_of_day(new FisdapDate($expiration_date));
    }
}
?>
