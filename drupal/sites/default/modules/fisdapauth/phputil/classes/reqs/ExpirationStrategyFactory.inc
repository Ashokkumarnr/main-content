<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/reqs/ExpirationStrategy.inc');
require_once('phputil/classes/reqs/ExpirationStrategyBindToEndOfMonth.inc');
require_once('phputil/classes/reqs/ExpirationStrategyBindToEndOfYear.inc');
require_once('phputil/classes/reqs/ExpirationStrategyNone.inc');

/**
 * This class is a factory for creating expiration strategies.
 */
final class ExpirationStrategyFactory {
    /**
     * Create an expiration strategy.
     * @param string | null $class_name The class name or NULL for no expiration date; no directory or path info please.
     * @return ExpirationStrategy The strategy.
     */
    public static function create($class_name) {
        if (is_null($class_name)) return new ExpirationStrategyNone();

        Assert::is_string($class_name);
        Assert::is_true(class_exists($class_name));

        $strategy =  new $class_name();
        Assert::is_a($strategy, 'ExpirationStrategy');
        return $strategy;
    }
}
?>
