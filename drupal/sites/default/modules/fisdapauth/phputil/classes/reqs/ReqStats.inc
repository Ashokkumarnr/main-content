<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FisdapLogger.inc');
require_once('phputil/string_utils.php');

/**
 * A container for statistics surrounding requirement operations.
 */
final class ReqStats {
    private $created = 0;
    private $deleted = 0;
    private $history_created = 0;
    private $history_deleted = 0;
    private $marked_as_deleted =0;
    private $modified = 0;

    private $debug;
    private $logger;
    private $package;

    private static $default_debug = false;

    /**
     * Constructor.
     * @param string $package The (singular) name of the package containing the stats.
     */
    public function __construct($package) {
        Assert::is_not_empty_trimmed_string($package);
        $this->package = strtolower(trim($package));
        $this->logger = FisdapLogger::get_logger();
        $this->debug = self::$default_debug;
    }

    /**
     * Indicate whether debug mode is on.
     * @param boolean $debug TRUE if debug mode is on.
     */
    public function set_debug($debug) {
        Assert::is_boolean($debug);
        $this->debug = $debug;
    }

    /**
     * Indicate whether debug mode is on by default.
     * @param boolean $debug TRUE if debug mode is on.
     */
    public static function set_default_debug($debug) {
        Assert::is_boolean($debug);
        self::$default_debug = $debug;
    }

    /**
     * Retrieve the number of entries created.
     * @return int The number of entries created.
     */
    public function get_created_count() {
        return $this->created;
    }

    /**
     * Increment the number of entries created.
     * @param int $n The count.
     */
    public function increment_created_count($n) {
        Assert::is_int($n);
        $this->set_created_count($this->get_created_count()+$n);
    }

    /**
     * Indicate the number of entries created.
     * @param int $n The number of entries created.
     */
    public function set_created_count($n) {
        Assert::is_int($n);
        $this->created = $n;
        $this->state_change();
    }

    /**
     * Retrieve the number of entries deleted.
     * @return int The number of entries deleted.
     */
    public function get_deleted_count() {
        return $this->deleted;
    }

    /**
     * Increment the number of entries deleted.
     * @param int $n The count.
     */
    public function increment_deleted_count($n) {
        Assert::is_int($n);
        $this->set_deleted_count($this->get_deleted_count()+$n);
    }

    /**
     * Indicate the number of entries deleted.
     * @param int $n The number of entries deleted.
     */
    public function set_deleted_count($n) {
        Assert::is_int($n);
        $this->deleted = $n;
        $this->state_change();
    }

    /**
     * Retrieve the number of history entries created.
     * @return int The number of history entries created.
     */
    public function get_history_created_count() {
        return $this->history_created;
    }

    /**
     * Increment the number of history entries created.
     * @param int $n The count.
     */
    public function increment_history_created_count($n) {
        Assert::is_int($n);
        $this->set_history_created_count($this->get_history_created_count()+$n);
    }

    /**
     * Indicate the number of entries history entries created.
     * @param int $n The number of entries history entries created.
     */
    public function set_history_created_count($n) {
        Assert::is_int($n);
        $this->history_created = $n;
        $this->state_change();
    }

    /**
     * Retrieve the number of history entries deleted.
     * @return int The number of history entries deleted.
     */
    public function get_history_deleted_count() {
        return $this->history_deleted;
    }

    /**
     * Increment the number of history entries deleted.
     * @param int $n The count.
     */
    public function increment_history_deleted_count($n) {
        Assert::is_int($n);
        $this->set_history_deleted_count($this->get_history_deleted_count()+$n);
    }

    /**
     * Indicate the number of entries history entries deleted.
     * @param int $n The number of entries history entries deleted.
     */
    public function set_history_deleted_count($n) {
        Assert::is_int($n);
        $this->history_deleted = $n;
        $this->state_change();
    }

    /**
     * Retrieve the number of entries modified.
     * @return int The number of entries modified.
     */
    public function get_modified_count() {
        return $this->modified;
    }

    /**
     * Increment the number of entries modified.
     * @param int $n The count.
     */
    public function increment_modified_count($n) {
        Assert::is_int($n);
        $this->set_modified_count($this->get_modified_count()+$n);
    }

    /**
     * Indicate the number of entries modified.
     * @param int $n The number of entries modified.
     */
    public function set_modified_count($n) {
        Assert::is_int($n);
        $this->modified = $n;
        $this->state_change();
    }

    /**
     * Retrieve the number of entries marked as deleted.
     * @return int The number of entries marked as deleted.
     */
    public function get_marked_as_deleted_count() {
        return $this->marked_as_deleted;
    }

    /**
     * Increment the number of entries marked as deleted.
     * @param int $n The count.
     */
    public function increment_marked_as_deleted_count($n) {
        Assert::is_int($n);
        $this->set_marked_as_deleted_count($this->get_marked_as_deleted_count()+$n);
    }

    /**
     * Indicate the number of entries marked as deleted.
     * @param int $n The number of entries marked as deleted.
     */
    public function set_marked_as_deleted_count($n) {
        Assert::is_int($n);
        $this->marked_as_deleted = $n;
        $this->state_change();
    }

    /**
     * Merge these stats with another.
     * @param ReqStats $stats The stats to merge.
     */
    public function merge(ReqStats $stats) {
        $old_debug = $this->debug;
        $this->set_debug(false);

        $this->increment_created_count($stats->get_created_count());
        $this->increment_deleted_count($stats->get_deleted_count());
        $this->increment_history_created_count($stats->get_history_created_count());
        $this->increment_history_deleted_count($stats->get_history_deleted_count());
        $this->increment_marked_as_deleted_count($stats->get_marked_as_deleted_count());
        $this->increment_modified_count($stats->get_modified_count());

        $this->set_debug($old_debug);
        $this->state_change();
    }

    /**
     * Retrieve messages describing the counts.
     * @param boolean $include_zero_counts TRUE if zero counts should be 
     * included.
     */
    public function get_messages($include_zero_counts) {
        $msgs = array();

        $this->add_msg($msgs, $include_zero_counts, 'Created <n> <text>',
            $this->get_created_count(), $this->package); 
        $this->add_msg($msgs, $include_zero_counts, 'Deleted <n> <text>',
            $this->get_deleted_count(), $this->package); 
        $this->add_msg($msgs, $include_zero_counts, 'Marked <n> <text> as deleted',
            $this->get_marked_as_deleted_count(), $this->package); 
        $this->add_msg($msgs, $include_zero_counts, 'Modified <n> <text>',
            $this->get_modified_count(), $this->package); 
        $this->add_msg($msgs, $include_zero_counts, "Created <n> $this->package history <text>",
            $this->get_history_created_count(), 'entry'); 
        $this->add_msg($msgs, $include_zero_counts, "Deleted <n> $this->package history <text>",
            $this->get_history_deleted_count(), 'entry'); 

        return $msgs;
    }

    public function __toString() {
        $data = array(
            'created:' . $this->get_created_count(),
            'deleted:' . $this->get_deleted_count(),
            'historyCreated:' . $this->get_history_created_count(),
            'historyDeleted:' . $this->get_history_deleted_count(),
            'markedAsDeleted:' . $this->get_marked_as_deleted_count(),
            'modified:' . $this->get_modified_count()
        );

        return 'ReqStats[' . $this->package . ': ' . join(',', $data) . ']';
    }

    private function add_msg(&$msgs, $include_zero_counts, $format, $count, $pluralize) {
        if (!$include_zero_counts && !$count) return;

        $text = pluralize($pluralize, $count);
        $s = str_replace('<n>', $count, $format);
        $s = str_replace('<text>', $text, $s);
        $s .= '.';

        $msgs[] = $s;
    }

    private function state_change() {
        if (!$this->debug) return;

        $this->logger->debug($this);
    }
}
?>
