<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/common_user.inc');
require_once('phputil/classes/FisdapDateTime.inc');
require_once('phputil/classes/FisdapLogger.inc');
require_once('phputil/classes/model/reqs/ReqRelationshipModel.inc');
require_once('phputil/classes/model/reqs/ReqRelationshipHistoryModel.inc');
require_once('phputil/classes/reqs/ReqAllStats.inc');
require_once('phputil/classes/reqs/ReqUtils.inc');

/**
 * The business logic for a requirement relationship.
 * No security is performed here.  If you call the method you get to use it.
 */
final class ReqRelationship {
    private $relationship;
    private $user;

    const TYPE_NAME = 'RequirementRelationship';

	/**
	 * Constructor.
	 * @param ReqRelationshipModel $relationship The underlying relationship.
	 * @param common_user $user The user making the changes.
	 */
	public function __construct(ReqRelationshipModel $relationship, common_user $user) {
        $this->relationship = $relationship;
        $this->user = $user;

        $this->logger = FisdapLogger::get_logger();
    }

    private function get_log_prefix() {
		return 'ReqRelationship[' . $this->relationship->get_id() . 
			']: application[' . $this->relationship->get_application_id() . 
			']: alternate req[' . $this->relationship->get_alternate_req_id() . ']';
    }

	/**
	 * Create the relationship in the DB.
	 * @param string | null $history_text The text to use for the history entries.
     * @return ReqAllStats Statistics.
	 * @throws FisdapDatabaseException if a problem occurs.
	 */
	public function create($history_text=null) {
        ReqUtils::verify_does_not_exist($this->relationship, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Created');

        $this->relationship->set_approved($this->relationship->is_approved());
        $this->relationship->save();

        $stats = new ReqAllStats();
        $stats->get_relationship_stats()->increment_created_count(1);
        $stats->merge($this->add_history($history_text));

        $this->logger->debug($this->get_log_prefix() . ': created');
        return $stats;
    }

	/**
	 * Delete the relationship from the DB.
	 * The entry is simply marked as deleted in the DB.
	 * @param string | null $history_text The text to use for the history entries.
     * @return ReqAllStats The statistics.
	 * @throws FisdapDatabaseException if a problem occurs.
	 */
	public function delete($history_text=null) {
        ReqUtils::verify_exists($this->relationship, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Deleted');

        $this->logger->debug($this->get_log_prefix() . ': delete started');

        $stats = new ReqAllStats();

        $this->relationship->set_deleted(true);
        $this->relationship->save();

        $stats->get_relationship_stats()->increment_marked_as_deleted_count(1);
        $stats->merge($this->add_history($history_text));

        $this->logger->debug($this->get_log_prefix() . ': delete complete');
        return $stats;
    }

	/**
	 * Delete relationships.
	 * The deleted items are simply marked as deleted.
	 * @param int $req_id The requirement ID.
	 * @param common_user $user The user making the change.
	 * @param string $history_text The text to use for the history entries.
	 * @return ReqAllStats The statistics.
	 */
    public static function delete_by_req_id($req_id, common_user $user, $history_text) {
		Assert::is_int($req_id);
		Assert::is_string($history_text);

		$logger = FisdapLogger::get_logger();
		$logger->debug("Delete req[$req_id] relationships started");

        // As there won't be many relationships, we don't need highly optimized 
        // SQL.
        $statement = <<<EOI
SELECT DISTINCT R.id FROM req_Relationship R 
INNER JOIN req_Application A ON A.id=R.Application_id
WHERE
    R.Deleted=0
    AND
    (
        A.Requirement_id=$req_id
        OR R.AlternateRequirement_id=$req_id
    )
EOI;

        $connection = FISDAPDatabaseConnection::get_instance();
        $relationships = ReqRelationshipModel::getBySQL($connection->query($statement));

        // Get the relationships that reference the requirements via the 
        // application.
		$stats = new ReqAllStats();

        foreach ($relationships as $relationship) {
            $r = new ReqRelationship($relationship, $user);
            $stats->merge($r->delete($history_text));
        }

		$logger->debug("Delete req[$req_id] relationships complete");
		return $stats;
    }

	/**
	 * Remove any relationships.
	 * The deleted items are simply marked as deleted.
	 * @param int $application_id The application ID.
	 * @param common_user $user The user making the change.
	 * @param string $history_text The text to use for the history entries.
	 * @return ReqAllStats The statistics.
	 */
	public static function delete_by_application_id($application_id, common_user $user,
		$history_text) {

		Assert::is_int($application_id);
		Assert::is_string($history_text);

		$logger = FisdapLogger::get_logger();
        $logger->debug("Delete application[$application_id] relationships started");

        $criteria = new ModelCriteria(new ReqRelationshipModel());
        $criteria->set_application_id($application_id);
        $criteria->set_deleted(false);

        $connection = FISDAPDatabaseConnection::get_instance();
        $relationships = $connection->get_by_criteria($criteria);

		$stats = new ReqAllStats();

        foreach ($relationships as $relationship) {
            $r = new ReqRelationship($relationship, $user);
            $stats->merge($r->delete($history_text));
        }

        $logger->debug("Delete application[$application_id] relationships complete");
		return $stats;
    }

	/**
	 * Update the relationship in the DB.
	 * @param string | null $history_text The text to use for the history entries.
     * @return ReqAllStats Statistics.
	 * @throws FisdapDatabaseException if a problem occurs.
	*/
    public function update($history_text=null) {
        ReqUtils::verify_exists($this->relationship, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Modified');

        $this->logger->debug($this->get_log_prefix() . ': update started');

        $stats = new ReqAllStats();
        $this->relationship->save();

        $stats->get_relationship_stats()->increment_modified_count(1);
        $stats->merge($this->add_history($history_text));

        $this->logger->debug($this->get_log_prefix() . ': update complete');
        return $stats;
    }

    private function add_history($description) {
        $description = trim($description);

        $history = new ReqRelationshipHistoryModel();
        $history->set_description($description);
        $history->set_entry_time(FisdapDateTime::now());
        $history->set_relationship_id($this->relationship->get_id());
        $history->set_user_id($this->user->get_UserAuthData_idx());
        $history->save();

		$stats = new ReqAllStats();
        $stats->get_relationship_stats()->increment_history_created_count(1);
		return $stats;
    }
}
?>
