<?php
require_once('phputil/classes/common_page.inc');
require_once('phputil/classes/common_report.inc');

/**
 * Functons common to report generators.
 */
class report_utilities
{
    var $page;

    /**
     * Constructor.
     * @param page The common page.
     */
    function report_utilities(&$page)
    {
        $this->page = &$page;
    }

    /**
     * Display a page indicating the report is not ready yet, and cycle on it.
     * You should NOT invoke this method for the following 
     * destinations: HTML.
     * @param destination The destination, i.e. PDF, TEXT (case insensitive).
     * @param redirect_url The URL to refresh.
     */
    function display_nowgenerating_page($destination, $redirect_url) 
    {
        // Set up so that the page automatically refreshes while
        // waiting for the report.
        $this->page->set_redirect_url($redirect_url);
        $this->page->display_header();
?>
<img src="<?php echo FISDAP_WEB_ROOT;?>reports/images/report.gif"
 alt="Reports" width="180" height="34"><br />
<?php
        $this->page->display_pagetitle();

        $destinations = array(
            'pdf' => 'PDF',
            'text' => 'tab-delimited text'
        );

        $lc_destination = strtolower($destination);
        if(isset($destinations[$lc_destination]))
        {
            $document_type = ' ' . $destinations[$lc_destination];
        }
        else
        {
            $document_type = '';
        }

        $msg = "Your{$document_type} document should be ready" .
            ' in a couple of seconds.  Please be patient.';
        $textblock = new common_textblock(array('', $msg), 'large', 'center');

        $common_dest = null; 
        $calledby = null;
        echo $textblock->display('html', $common_dest, $calledby);

        $this->page->display_footer();
    }

    /**
     * Display a page indicating the user is not authorized to view reports.
     * @param contact The person to contact for help, may be null or empty.
     */
    function display_unauthorized_user_page($contact)
    {
        $this->page->display_header();
?>
<img src="images/report.gif" alt="Reports" width="180" height="34"><br>
<?php
        $this->page->display_pagetitle();

        if(is_null($contact) || (trim($contact) == ''))
        {
            $contact = 'an instructor';
        }

        $msg = 'Currently you do not have access to view this report.' .
            " Please contact {$contact} for access.";
        $textblock = new common_textblock(array('', $msg), 'large', 'center');

        $common_destination = null;
        $called_by = null;
        echo $textblock->display('html', $common_destination, $called_by); 

        $this->page->display_footer();
    }
}
?>
