<?php
/**
 * The standard common_form class
 *
 * @package CommonInclude
 * @author Warren Jacobson
 * @author Ian Young
 */

require_once("phputil/classes/common_form_base.inc");

class common_form extends common_form_base {
	/**
	 * The name of the calling routine's display function
	 * @deprecated
	 */
	protected $display_function;
	/**
	 * An array containing the column layout of this form
	 */
	protected $column_layout;
	/**
	 * The entire HTML rendered form output
	 */
	protected $generated_html_form;

	public function __construct() {
		parent::__construct();
		$this->set_display_function(null); // Set the calling routine's display function name
		$this->set_column_layout(array(1,1,1));
		$this->generated_html_form = null; // Initialize the HTML form output to null
	}

	/**
	 * Set the calling routine's display function name
	 * @deprecated Use get_generated_html_form() instead
	 */

	function set_display_function($display_function) {

		$this->display_function = $display_function;

	}

	/**
	 * Set the column layout: Either (1), (2), (3), (1,1), (2,1), (1,2), or (1,1,1)
	 */

	function set_column_layout($column_layout) {

		if (!is_array($column_layout)) $column_layout = array($column_layout);

		$total_column_width = 0;
		foreach ($column_layout as $this_column_width) {
			if ($this_column_width != 1 and $this_column_width != 2 and $this_column_width != 3) {
				common_utilities::displayFunctionError('Invalid layout');
			}
			$total_column_width += $this_column_width;
		}
		if ($total_column_width < 1 or $total_column_width > 3) {
			common_utilities::displayFunctionError('Invalid layout');
		}

		$this->column_layout = $column_layout;

	}

	/**
	 * Return the HTML output for the form, if any
	 *
	 * Example:
	 * <code>
	 * $myform =& define_form(); # Define the form and the prompts, etc
	 * $validated = $myform->process();
	 * if ($validated) {
	 *	// Do something meaningful with the submitted form input
	 *  // ...
	 * }
	 * else if ($myform->get_generated_html_form() != null) {
	 *	$page->display_header(); # Display the page header
	 *	echo $myform->get_generated_html_form(); # Display the form
	 *	$page->display_footer(); # Display the page footer
	 * }
	 * </code>
	 *
	 * @return string If the form does NOT need to be displayed, then null.
	 * If the form NEEDS to be displayed, then the rendered HTML form output
	 */
	function get_generated_html_form() {

		return $this->generated_html_form;

	}

	/**
	 * Add a display object (e.g., prompt, section) to the form
	 *
	 * @param common_form_component The component to add
	 * @param int The column index in which to add this
	 */
	function add(&$objectref,$column) {

		// Make sure we're trying to add this display object to a valid column
		$valid_column = true;
		if ($column != "1" and $column != "2" and $column != "3") $valid_column = false;
		if ($column > count($this->column_layout)) $valid_column = false;
		if (!$valid_column) {
			common_utilities::displayFunctionError('Invalid column number');
		}

		// Make sure the object we're adding is actually an object of the right class
		$valid_object = false;
		if ($objectref instanceof common_form_component) {
			$valid_object = true;
			if ($objectref instanceof common_upload) {
				$this->contains_upload = true;
			}
		}
		if (!$valid_object) {
			common_utilities::displayFunctionError('Objects added to common_form must implement common_form_component');
		}

		// Make sure the display objct is the right width
		$valid_width = true;
		if ($objectref->get_column_width() != $this->column_layout[$column - 1]) {
			common_utilities::displayFunctionError('Invalid column width'); $valid_width = false;
		}

		// If this object has passed all the validations then add it to the form. Also, tell the
		// object a little about the form it belongs to.

		if ($valid_column and $valid_object and $valid_width) {
			$objectref->set_form_state($this->state);
			$this->displayobjects[] = array("object"=>$objectref,"column"=>$column);
		}
	}

	function display($ajax_mode) {

		$output = null;

		if (!$ajax_mode) {
			$this_width = common_form_utilities::get_column_width(array_sum($this->column_layout) - 1);
			$inline_css = "display: block; border: 0px; margin: 0 auto 0 auto; padding: 0px; width: " . $this_width . "px; height: auto;";
			$output .= '<div id="' . $this->name . '_form_div" style="' . $inline_css . '">'; // The entire HTML form with tables and prompts
		}

		// Establish the Dispatch ID we're going to use later on

		$dispatch_id = $this->get_dispatch_id();

		// Setup $tablestart and $tablefinish for use in the queues

		$tablestart = '<table width="100%" cellspacing="0" cellpadding="0" border="0">';
		$tablefinish = '</table>';

		$output .= $this->get_form_header_display() . "\n";

		// Build the encompassing <table> structure

		$this_width = common_form_utilities::get_column_width(array_sum($this->column_layout) - 1);
		$inline_css = "display: block; top: 0px; bottom: 0px; left: auto; right: auto; margin: 0px; padding: 0px; width: " . $this_width . "px;";
		$tabletag = '<table border="0" align="center" cellspacing="0" cellpadding="0" style="' . $inline_css . '">';

		$output .= $tabletag . "\n";

		// Loop through each enclosed display object

		$columnqueues = array(); $hiddenqueue = null;

		reset($this->displayobjects);
		while (list($key,$thisdisplayobjects) = each($this->displayobjects)) {

			// Call the display method for this prompt and retrieve the prompt HTML

			$prompthtml = '<div id="' . $this->displayobjects[$key]['object']->get_name() . '_div' . '">';
			$prompthtml .= $this->displayobjects[$key]['object']->display();
			$prompthtml .= '</div>';

			$ishidden = false;
			if ($this->displayobjects[$key]['object'] instanceof common_form_prompt_component) {
				if ($this->displayobjects[$key]['object']->get_type() == 'hidden') {
					$ishidden = true;
				}
			}

			// If this prompt is a hidden prompt then queue the prompt html for the very end of the form.
			if ($ishidden) {
				$hiddenqueue .= $prompthtml;
			}
			else {

				// This is not a hidden prompt. Build a nice table row for the prompt.

				$beforeprompthtml = '<tr><td valign="top" align="left">';
				$afterprompthtml = '</td></tr>' . "\n";
				$prompthtml = $beforeprompthtml . $prompthtml . $afterprompthtml;

				// What column are we trying to put a prompt in?

				$column = $thisdisplayobjects['column'];

				// Place the output of the display object into the appropriate queue

				if ($columnqueues[$column - 1] == null) {
					$columnqueues[$column - 1] = $tablestart;
				}
				$columnqueues[$column - 1] .= $prompthtml;

			}

		}

		// Flush the contents of the queues

		$fwr = FISDAP_WEB_ROOT; // Absolute path to web root so that this code is portable

		$spacer = '<img src="' . $fwr . 'images/pixel.gif" width="9" height="1" alt="">'; if ($columnqueues == array()) $spacer = null;

		$output .= '<tr>';

		$column_count = count($this->column_layout);
		for ($colidx = 0; $colidx < $column_count; $colidx++) {
			if ($columnqueues[$colidx] != null) $columnqueues[$colidx] .= $tablefinish;
			if ($colidx >= 1) {
				$output .= '<td width="9" valign="top" align="left">' . $spacer . '</td>';
			}
			$this_width = common_form_utilities::get_column_width($this->column_layout[$colidx] - 1);
			$output .= '<td width="' . $this_width . '" valign="top" align="left">';
			$output .= $columnqueues[$colidx];
			if ($colidx == $column_count - 1) {
				$output .= $hiddenqueue;
			}
			$output .= '</td>';
		}

		$output .= '</tr></table>' . "\n";

		$output .= $this->get_form_footer_display();
		$output .= '</div>'; // stuff with inline css

		// Call the calling routine's display function for stuff above the form
		if ($this->display_function != null and $ajax_mode == false) {
			$display_function = $this->display_function;
			if (function_exists($display_function)) {
				$display_function("above"); // Call the function
			}
		}

		// Display the form itself

		if ($this->display_function != null or $ajax_mode == true) {
			echo $output; // Display the form to the browser
		}

		// Call the calling routine's display function for stuff below the form

		if ($this->display_function != null and $ajax_mode == false) {
			$display_function = $this->display_function;
			if (function_exists($display_function)) {
				$display_function("below"); // Call the function
			}
		}

		// We're done. Last task is to tuck the generated HTML form away in case the calling app wants it

		if ($ajax_mode == false) $this->generated_html_form = $output;

	}

	// Overridden from common_form_base
	protected function get_lower_form_display() {

		$formtag = '<form action="' . $this->action . '"';
		$formtag .= ' id="' . $this->name . 'Buttons"';
		$formtag .= ' method="GET">';

		$this_width = common_form_utilities::get_column_width(array_sum($this->column_layout) - 1);
		$inline_css = "display: block; top: 0px; bottom: 0px; left: auto; right: auto; margin: 0px; padding: 0px; width: " . $this_width . "px;";
		$tabletag = '<table border="0" align="center" cellspacing="0" cellpadding="0" style="' . $inline_css . '">';

		$output .= $formtag . "\n";
		$output .= $tabletag . "\n";
		$output .= '<tr><td width="' . $this_width . '" align="right" valign="top"><div class="common_section"></div>';

		$output .= $this->get_form_buttons_display();

		$output .= '</td></tr></table>' . "\n";
		$output .= '</form>' . "\n";

		return $output;
	}

}

