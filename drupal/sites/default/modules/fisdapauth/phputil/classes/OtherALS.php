<?php 

require_once('phputil/other_als_skill_constants.php');

/**
 * This class models a FISDAP Other ALS Skill entry
 */
class FISDAP_OtherALS extends DataEntryDbObj {    

    /** 
     * Create a new Other ALS skill with the specified id and the
     * specified db link
     */
    function FISDAP_OtherALS( $id, $connection ) {
        $this->pda_id_field = 'Other_id';
        $this->DbObj(
            'OtherALSData',        //table name
            $id,                   //id
            'OtherALS_id',         //id field
            'Shift',               //parent entity name
            array('EvalSession'),  //children
            $connection            //database connection
        );  

        $field_map = array( 
            'Student_id' => 'Student_id',
            'Shift_id' => 'Shift_id',
            'Run_id' => 'Run_id',
            'PerformedBy' => 'PerformedBy',
            'OtherSkill' => 'OtherSkill',
            'SubjectType_id' => 'SubjectType_id'
        );
        $this->set_field_map( $field_map );
    }//constructor FISDAP_OtherALS


    function postprocess_pda_submission(){
        if ( intval($this->get_field('OtherSkill')) == 0 ) {
            $this->set_field('OtherSkill', OTHER_CHEST_DECOMPRESSION);
        }//if

        parent::postprocess_pda_submission();
    }//postprocess_pda_submission    
}//class FISDAP_OtherALS

?>
