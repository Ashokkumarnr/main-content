<?php

require_once('AbstractModel.inc');

/**
 * #######################################################################
 * This class only handles get functions until all fields are implemented.
 * #######################################################################
 */
class EventDataModel extends AbstractModel {


/*
is_a($condition, $class)
is_array($condition)
is_in_array($value, $list, $strict=false)
is_boolean($condition)
is_false($condition)
is_not_null($condition)
is_null($condition)
is_object($condition)
is_int($condition)
is_float($condition)
is_string($condition)
is_true($condition)
is_not_empty_string($condition)
is_not_empty_trimmed_string($condition)
*/

		protected $student_id;
		protected $ambserv_id;
		protected $startbase_id;
		protected $start_date;
		protected $start_time;
		protected $end_time;
		protected $hours;
		protected $type;
		protected $program_id;
		protected $open_slots;
		protected $available;
		protected $trade;
		protected $location;
		protected $repeat_code;
		protected $expiration_date;
		protected $preceptor_id;
		protected $notes;
		protected $email_list;
		protected $drop_permissions;
		protected $trade_permissions;
		protected $release_date;
		protected $total_slots;
		protected $student_notes;
		protected $class_section;
		protected $limited;

		const DB_TABLE_NAME = 'EventData';
		const ID_FIELD = 'Event_id';
	
		public function __construct($_id=null) {
			if (!$this->construct_helper($_id, self::DB_TABLE_NAME, self::ID_FIELD)) {
				return false;
			}
		}

		public function get_fieldmap() {
			$map = array(
			"Student_id"=>"student_id",
			"AmbServ_id"=>"ambserv_id",
			"StartBase_id"=>"startbase_id",
			"StartDate"=>"start_date",
			"StartTime"=>"start_time",
			"EndTime"=>"end_time",
			"Hours"=>"hours",
			"Type"=>"type",
			"Program_id"=>"program_id",
			"OpenSlots"=>"open_slots",
			"Available"=>"available",
			"Trade"=>"trade",
			"Location"=>"location",
			"RepeatCode"=>"repeat_code",
			"ExpirationDate"=>"expiration_date",
			"Preceptor_id"=>"preceptor_id",
			"Notes"=>"notes",
			"EmailList"=>"email_list",
			"DropPermissions"=>"drop_permissions",
			"TradePermissions"=>"trade_permissions",
			"ReleaseDate"=>"release_date",
			"TotalSlots"=>"total_slots",
			"StudentNotes"=>"student_notes",
			"ClassSection"=>"class_section",
			"Limited"=>"limited"
	    );
			return $map;
		}

		public function get_summary() {
			return $this->get_string();
		}

		public static function getBySQL($result) {
			return parent::sql_helper($result, __CLASS__, self::ID_FIELD);
		}

// Student_id       | mediumint(8)
		public function set_student_id($student_id) {
			Assert::is_int($student_id);
		  $this->set_helper('string', $student_id, 'student_id');
		}	  
// AmbServ_id       | mediumint(8)
		public function set_ambserv_id($ambserv_id) {
			Assert::is_int($ambserv_id);
		  $this->set_helper('string', $ambserv_id, 'ambserv_id');
		}	  
// StartBase_id     | mediumint(8)
		public function set_startbase_id($startbase_id) {
			Assert::is_int($startbase_id);
		  $this->set_helper('string', $startbase_id, 'startbase_id');
		}	  
// StartDate        | date        
		public function set_start_date($start_date) {
			Assert::is_string($start_date);
		  $this->set_helper('string', $start_date, 'start_date');
		}	  
// StartTime        | varchar(8)  
		public function set_start_time($start_time) {
			Assert::is_string($start_time);
		  $this->set_helper('string', $start_time, 'start_time');
		}	  
// EndTime          | varchar(8)  
		public function set_end_time($end_time) {
			Assert::is_string($end_time);
		  $this->set_helper('string', $end_time, 'end_time');
		}	  
// Hours            | float(10,2) 
		public function set_hours($hours) {
			Assert::is_float($hours);
		  $this->set_helper('string', $hours, 'hours');
		}	  
// Type             | varchar(10) 
		public function set_type($type) {
			Assert::is_string($type);
		  $this->set_helper('string', $type, 'type');
		}	  
// Program_id       | mediumint(8)
		public function set_program_id($program_id) {
			Assert::is_int($program_id);
		  $this->set_helper('string', $program_id, 'program_id');
		}	  
// OpenSlots        | smallint(3) 
		public function set_open_slots($open_slots) {
			Assert::is_int($open_slots);
		  $this->set_helper('string', $open_slots, 'open_slots');
		}	  
// Available        | tinyint(1)  
		public function set_available($available) {
			Assert::is_int($available);
		  $this->set_helper('string', $available, 'available');
		}	  
// Trade            | tinyint(1)  
		public function set_trade($trade) {
			Assert::is_int($trade);
		  $this->set_helper('string', $trade, 'trade');
		}	  
// Location         | varchar(50) 
		public function set_location($location) {
			Assert::is_string($location);
		  $this->set_helper('string', $location, 'location');
		}	  
// RepeatCode       | mediumint(8)
		public function set_repeat_code($repeat_code) {
			Assert::is_int($repeat_code);
		  $this->set_helper('string', $repeat_code, 'repeat_code');
		}	  
// ExpirationDate   | datetime    
		public function set_expiration_date($expiration_date) {
			Assert::is_string($expiration_date);
		  $this->set_helper('string', $expiration_date, 'expiration_date');
		}	  
// Preceptor_id     | mediumint(8)
		public function set_preceptor_id($preceptor_id) {
			Assert::is_int($preceptor_id);
		  $this->set_helper('string', $preceptor_id, 'preceptor_id');
		}	  
// Notes            | varchar(200)
		public function set_notes($notes) {
			Assert::is_string($notes);
		  $this->set_helper('string', $notes, 'notes');
		}	  
// EmailList        | varchar(200)
		public function set_email_list($email_list) {
			Assert::is_string($email_list);
		  $this->set_helper('string', $email_list, 'email_list');
		}	  
// DropPermissions  | tinyint(1)  
		public function set_drop_permissions($drop_permissions) {
			Assert::is_int($drop_permissions);
		  $this->set_helper('string', $drop_permissions, 'drop_permissions');
		}	  
// TradePermissions | tinyint(1)  
		public function set_trade_permissions($trade_permissions) {
			Assert::is_int($trade_permissions);
		  $this->set_helper('string', $trade_permissions, 'trade_permissions');
		}	  
// ReleaseDate      | date        
		public function set_release_date($release_date) {
			Assert::is_string($release_date);
		  $this->set_helper('string', $release_date, 'release_date');
		}	  
// TotalSlots       | smallint(3) 
		public function set_total_slots($total_slots) {
			Assert::is_int($total_slots);
		  $this->set_helper('string', $total_slots, 'total_slots');
		}	  
// StudentNotes     | varchar(200)
		public function set_student_notes($student_notes) {
			Assert::is_string($student_notes);
		  $this->set_helper('string', $student_notes, 'student_notes');
		}	  
// ClassSection     | mediumint(8)
		public function set_class_section($class_section) {
			Assert::is_int($class_section);
		  $this->set_helper('string', $class_section, 'class_section');
		}	  
// Limited          | tinyint(1)  
		public function set_limited($limited) {
			Assert::is_int($limited);
		  $this->set_helper('string', $limited, 'limited');
		}
}
?>

