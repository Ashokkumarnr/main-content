<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/model/AbstractHistoryModel.inc');

/**
 * This class models the req_UserRequirementHistory table in the DB.
 */
final class UserReqHistoryModel extends AbstractHistoryModel {
    protected $user_req_id;

    const DB_TABLE = 'req_UserRequirementHistory';
    const USER_REQ_ID_FIELD = 'user_req_id';

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    /**
     * Retrieve the user requirement ID.
     * @return int A reference into the req_UserRequirement table.
     */
    public function get_user_req_id() {
        return $this->user_req_id;
    }

    /**
     * Set the requirement user requirement ID.
     * @param int $id A reference into the req_UserRequirement table.
     */
    public function set_user_req_id($id) {
        Assert::is_int($id);
		$this->set_helper('int', $id, 'user_req_id');
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'UserReqHistory[' . $this->get_id() . ']';
    }

    public function get_fieldmap() {
        $map = array(
            'UserRequirement_id' => self::USER_REQ_ID_FIELD
        );

        return array_merge($map, parent::get_fieldmap());
    }

    /**
     * Retrieve all the history entries.
     * @return array A list of objects sorted on entry time ascending. 
     */
    public static function get_all_entries() {
        return parent::do_get_all_entries(__CLASS__);
    }

	/**
	 * Retrive all the history entries by ID.
	 * @param int $id The ID.
     * @return array A list of objects sorted on entry time ascending. 
	 */
	public static function get_all_by_user_req_id($id) {
		Assert::is_int($id);
		
        return parent::do_get_all_by_id(__CLASS__, self::USER_REQ_ID_FIELD, $id);
	}
}
?>
