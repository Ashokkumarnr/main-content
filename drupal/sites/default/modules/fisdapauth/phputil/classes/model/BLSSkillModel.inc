<?php

require_once("DataModel.inc");
require_once("phputil/classes/common_utilities.inc");

class BLSSkill extends DataModel { 

	/*********
	 * FIELDS
	 *********/

	protected $skill;
	protected $modifier;
	protected $assessment_id;
	protected $subject_type;


	protected $field_hook_map = array(1 => 3,
					2 => 12,
					3 => 9,
					4 => 4,
					5 => 13,
					6 => 20,
					7 => 5,
					8 => 14,
					9 => 21,
					10 => 6,
					11 => 15,
					12 => 106,
					13 => 7,
					14 => array(10, 17, 107, 108),
					15 => 16,
					16 => 18
				);

	protected $clinical_hook_map = array(1 => 39,
					2 => 48,
					3 => 47,
					4 => 22,
					5 => 49,
					6 => 54,
					7 => 43,
					8 => 50,
					9 => 55,
					10 => 44,
					11 => 51,
					12 => 99,
					13 => 45,
					14 => array(101, 102, 112),
					15 => 52,
					16 => 53
				);

	protected $lab_hook_map = array(1 => 65,
					2 => 70,
					3 => 69,
					4 => 64,
					5 => 71,
					6 => 76,
					7 => 66,
					8 => 72,
					9 => 77,
					10 => 67,
					11 => 73,
					12 => 100,
					13 => 68,
					14 => array(103, 104, 111),
					15 => 74,
					16 => 75
		);

	const dbtable = "BLSSkillsData";
	const id_field = "Skill_id";

	// Some specific codes in the DB
	const HOSPITAL_NOTIFY_CODE = 1;
	const MD_CONSULT_CODE = 2;
	const PATIENT_INTERVIEW_CODE = 3;
	const PATIENT_EXAM_CODE = 4;
	const VITAL_SIGNS_CODE = 5;
	const SUCTION_CODE = 6;
	const L_BOARD_IMMOBILIZATION_CODE = 7;
	const PATIENT_MOVED_CODE = 8;
	const VENTILATION_CODE = 9;
	const TRACTION_SPLINT_CODE = 10;
	const BANDAGING_CODE = 11;
	const CHEST_COMPRESSIONS_CODE = 12;
	const JOINT_IMMOBILIZATION_CODE = 13;
	const BLS_AIRWAY_CODE = 14;
	const C_SPINE_IMMOBILIZATION_CODE = 15;
	const LONG_BONE_IMMOBILIZATION_CODE = 16;


	/**************
	 * CONSTRUCTOR
	 **************/
	public function __construct($id=null) {
		parent::__construct();
		$this->set_helper('int', -1, 'assessment_id');
		if (!$this->construct_helper($id, self::dbtable, self::id_field)) {
			return false;
		}
	}


	/**********
	 * METHODS
	 **********/

	public function set_skill($skill) {
	$result = $this->id_or_string_helper($skill, "get_bls_skills");
		if (!is_null($result)) {
			return $this->set_helper("int", $result, "skill");
		} else {
			return false;
		}
	}

	/* 
	 * Needs to override the parent method because it's stored differently.
	 */

	public function get_performed_by() {
		if ($this->performed_by == 2) {
			return "Observed";
		} else if ($this->performed_by == 1) {
			return "Performed";
		} else {
			return false;
		}
	}

	public function get_performed_by_id() {
		return $this->performed_by;
	}

	public function set_performed_by($setting) {
		if (is_numeric($setting)) {
			return $this->set_helper("int", $setting, "performed_by");
		} else {
			return false;
		}
	}

	public function set_assessment_id($id) {
		if (is_numeric($id)) {
			return $this->set_helper("int", $id, "assessment_id");
		}
	}

	public function set_modifier($_modifier) {
		$modifiers = self::get_bls_modifiers();
		$modifiers = $modifiers[$this->skill];

		if ($_modifier == 0) {
			return $this->set_helper('int',$_modifier,'modifier');
		} else if (is_numeric($_modifier)) {
			if (array_key_exists($_modifier,$modifiers)) {
				return $this->set_helper('int',$_modifier,'modifier');
			}
		} else if (is_string($_modifier)) {
			foreach ($modifiers as $id => $modifier) {
				if (strcasecmp($_modifier, $modifier) === 0) {
					return $this->set_helper('int',$id,'modifier');
				}
			}
		}
		return false;
	}


	public static function get_performed_by_strings() {
		return array(2 => "observed", 1 => "performed");
	}

	public function get_summary() {
		$skills = self::get_bls_skills();
		$s = $skills[$this->skill];
		if ($this->modifier > 0) {
			$modifiers = self::get_bls_modifiers();
			$s .= " - " . $modifiers[$this->skill][$this->modifier];
		}
		$s .= " (" . $this->get_performed_by() . ")";// . print_r($this, true);
		return $s;
	}

	public function get_mobile_summary() {
		//STUB
	}

	public static function get_bls_skills() {
		return self::data_table_helper("BLSSkillsTable", "BLSSkill_id", "BLSSkill");
	}

	public static function get_bls_modifiers() {
		$modifiers =  self::data_table_helper("BLSModifierTable", "BLSMod_id", "BLSModifier");
		$skills =  self::data_table_helper("BLSModifierTable", "BLSMod_id", "SkillCode");

		$return_array = array();
		foreach ($skills as $BLSMod_id=>$SkillCode) {
			$return_array[$SkillCode][$BLSMod_id] = $modifiers[$BLSMod_id];	
		}
		return $return_array;
	}

	public function get_hook_ids() { 
		Assert::is_true($this->shift_id > 0);
		Assert::is_true($this->skill > 0);

		$shift = new Shift($this->shift_id);
		$shift_type = $shift->get_type();

		if ($shift_type == 'field') {
			return common_utilities::array_flatten(array($this->field_hook_map[$this->skill]), array());
		} else if ($shift_type == 'clinical') {
			return common_utilities::array_flatten(array($this->clinical_hook_map[$this->skill]), array());
		} else if ($shift_type == 'lab') {
			return common_utilities::array_flatten(array($this->lab_hook_map[$this->skill]), array());
		} else {
			return array();
		}	
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "blsskill", self::id_field);
	}

	public function get_fieldmap() {
		return array(
			"Run_id" => "run_id",
			"Student_id" => "student_id",
			"SkillCode" => "skill",
			"PerformedBy" => "performed_by",
			"SkillModifier" => "modifier",
			"Shift_id" => "shift_id",
			"Assesment_id" => "assessment_id",
			"SubjectType_id" => "subject_type");
	}
}

?>
