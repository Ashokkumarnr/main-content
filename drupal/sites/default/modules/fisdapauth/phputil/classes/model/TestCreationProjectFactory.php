<?php

require_once('DefaultTestCreationProjectFactory.php');
require_once('SingletonTestCreationProjectFactory.php');

class TestCreationProjectFactory {
    var $factories;

    function TestCreationProjectFactory() {
        $this->factories = array(
            new SingletonTestCreationProjectFactory(),
            new DefaultTestCreationProjectFactory()
        );
    }//TestCreationProjectFactory


    /**
     *
     * @todo refactor this to run the query from within the TestCreationProject
     *       class itself.
     */
    function &create_project($id) {
        if ( !is_numeric($id) ) {
            die(
                'create_project: error: could not create project... '
                . 'invalid ID received.'
            );
        }//if

        $connection =& FISDAPDatabaseConnection::get_instance();

        // AAAAAH!  Inside information!  This, and the query it goes in, should
        // be put somewhere inside the TestCreationProject class
        $table = 'ProjectTable';
        $id_field = 'Project_id';
        $result_set = $connection->query(
            'SELECT * FROM '.$table.' WHERE '.$id_field.'="'.$id.'"'
        );
        if ( is_array($result_set[0]) ) {
            $project_row = $result_set[0];
            return $this->create_project_from_contents($project_row);
        } else {
            die(
                'create_project: error: project #'.$id.' not found in '
                . 'the database'
            );
        }//else
    }//create_project


    /**
     * Interpret the associative array (assumed to be of the 
     * same format as a database row) as an appropriate type
     * of review object
     */
    function &create_project_from_contents($assoc_array) {
        foreach ( $this->factories as $factory ) {
            $project =& $factory->create_project($assoc_array);
            if ( $project ) {
                return $project;
            }//if
        }//foreach

        die('None of the available factories was able to create a project');
    }//create_project_from_contents
}//TestCreationProjectFactory

?>
