<?php
/**
 * This class models an abstract DB table.
 * Not much here yet, but there will be.
 */
class AbstractTable
{
    var $connection;
    var $tableName;

    /**
     * Constructor.
     * @param connection The DB connection.
     * @param tableName The name of the table.
     */
    function AbstractTable(&$connection, $tableName)
    {
        $this->connection =& $connection;
        $this->tableName = $tableName;
    }

    /**
     * Retrieve the table name.
     * @return The table name.
     */
    function getTableName()
    {
        return $this->tableName;
    }

    /**
     * Retrieve the connection.
     * @return The DB connection.
     */
    function &getConnection()
    {
        return $this->connection;
    }
}
?>
