<?php

require_once('TestCreationProject.php');

/**
 * This class models testing projects that only require one assignment
 * review for a validation designation.
 */
class SingletonTestCreationProject extends TestCreationProject {
    function SingletonTestCreationProject() {
        parent::TestCreationProject();
        $this->assignment_review_type = SINGLETON_ASSIGNMENT_REVIEW;
    }//SingletonTestCreationProject


    /**
     * @todo find a clean way to sort the reviews by date, so we can 
     *       avoid relying on the IDs being sequential
     */
    function update_item_review_status(&$item,$email_warnings=true) {
        // get all of the reviews 
        $reviews = $item->get_reviews();

        if ( $newest_singleton = $this->find_newest_singleton_review(
                 $reviews
             ) ) {
            // update the status from the latest singleton review
            $updated_status = ($newest_singleton->is_favorable()) ? 
                VALIDATED_ITEM_VALIDITY :
                NEEDS_REVISION_ITEM_VALIDITY;
            $item->set_validity($updated_status);
            $item->save();
            $this->logger->log(
                'TestItem#'.$item->get_id().': singleton review! set '.
                'status to '.$updated_status,
                $this->logger->DEBUG
            );
            return $updated_status;
        } else {
            // no singleton reviews... set the status to "undetermined"
            $item->set_validity(UNDETERMINED_ITEM_VALIDITY);
            $item->save();
            $this->logger->log(
                'TestItem#'.$item->get_id().': no singleton reviews... '
                . 'setting status to undetermined',
                $this->logger->DEBUG
            );
            return UNDETERMINED_ITEM_VALIDITY;
        }//else
    }//update_item_review_status


    /**
     * Returns the newest singleton review in the given list, if any.
     */
    function find_newest_singleton_review($reviews) {
        $newest_singleton = null;
        if ( $reviews ) {
            foreach( $reviews as $review ) {
                if ( $review->get_assignment_review_type() == 
                     SINGLETON_ASSIGNMENT_REVIEW ) {
                    if ( !$newest_singleton ) {
                        $newest_singleton = $review;
                    } else if ( $review->get_id() > 
                                $newest_singleton->get_id() ) {
                        $newest_singleton = $review;
                    }//if
                }//if
            }//foreach
        }//if

        return $newest_singleton;
    }//find_newest_singleton_review


    --++-- Begin Module --++--
    This module provides the static methods find_by_id and from_assoc_array.
    
    File: test_creation_project_db_layer.php
    Class Name: SingletonTestCreationProject
    --++--- End Module ---++--

}//class SingletonTestCreationProject

?>
