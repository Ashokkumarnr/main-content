<?php
require_once('AbstractModel.inc');

/**
 * @todo unfinished
 * @todo document
 * @todo test
 */
class Asset extends AbstractModel {

	const dbtable = "Asset_def";
	const idfield = "AssetDef_id";

	/**
	 * @todo document all these
	 */
	protected $name;
	protected $description;
	protected $approved;
	protected $date_approved;
	protected $data_type_id;
	protected $data_id;
	protected $tree_id;
	protected $parent_id;
	protected $reference_type_id;
	protected $reference_data_id;
	protected $date_published;
	/**
	 * Determines the order of this asset relative to its siblings.
	 * By default assets receive the order in which they were inserted.
	 * @var int
	 */
	protected $sibling_order;
	protected $paid;
	protected $staff_rating;
	protected $point_eligibility;

	public function __construct($id=null) {
		self::construct_helper($id, self::dbtable, self::idfield);
	}

	public function save() {

		$new = ($this->get_id() === null);

		//TODO move this into set_parent_id
		if ($new && !in_array('sibling_order', $this->changed_fields)) {
			// Find all siblings and get the max sibling order number
			$class = get_class($this);
			$c = new ModelCriteria(new $class());
			$c->set_parent_id($this->get_parent_id());
			$c->add_order_by(new OrderBy('sibling_order', false));
			$c->set_statement_limit(1);
			$db = FISDAPDatabaseConnection::get_instance();
			$siblings = $db->get_by_criteria($c);
			$max_order = ($siblings ? $siblings[0]->get_sibling_order() : -1);
			// By default, we insert at the end of the order
			$this->set_sibling_order($max_order + 1);
		}

		parent::save();

		// If we're new, we've just now picked up an id, so calculate our new 
		// tree id and save it
		if ($new) {
			$this->calculate_tree_id();
			parent::save();
		}

	}

	public function set_parent_id($id) {
		$this->set_helper('int', $id, 'parent_id');
		// Recalculate our tree id, since it depends on the parent
		$this->calculate_tree_id();
	}

	/**
	 * Determine the appropriate tree id and set it
	 *
	 * Tree ids are the string of asset ids of the asset and each of its parents,
	 * joined by periods. This function uses the parent's tree id and our own id to set
	 * the appropriate tree id.
	 */
	private function calculate_tree_id() {
		if ($this->get_parent_id() > 0) {
			$class = get_class($this);
			$parent = new $class($this->get_parent_id());
			$tree_id = $parent->get_tree_id() . '.' . $this->get_id();
		} else {
			$tree_id = $this->get_id();
		}
		$this->set_helper('string', $tree_id, 'tree_id');
	}

	public function get_settable_fields() {
		return array(
			'name' => 'string',
			'description' => 'string',
			'approved' => 'bool',
			'date_approved' => 'datetime',
			'data_type_id' => 'int',
			'data_id' => 'int',
			'parent_id' => 'int',
			'reference_type_id' => 'int',
			'reference_data_id' => 'int',
			'date_published' => 'datetime',
			'sibling_order' => 'int',
			'paid' => 'bool',
			'staff_rating' => 'int',
			'point_eligibility' => 'int',
		);
	}

	public function get_fieldmap() {
		return array(
			'AssetName' => 'name',
			'AssetDescription' => 'description',
			'Approved' => 'approved',
			'DateApproved' => 'date_approved',
			'DataType_id' => 'data_type_id',
			'Data_id' => 'data_id',
			'Tree_id' => 'tree_id',
			'Parent_id' => 'parent_id',
			'ReferenceType_id' => 'reference_type_id',
			'ReferenceData_id' => 'reference_data_id',
			'DatePublished' => 'date_published',
			'SiblingOrder' => 'sibling_order',
			'PaidFlag' => 'paid',
			'StaffRating' => 'staff_rating',
			'PointEligibility' => 'point_eligibility',
		);
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "Asset", self::idfield);
	}

	public function get_summary() {
		//TODO STUB
		return 'Asset';
	}
}
