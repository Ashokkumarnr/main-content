<?php

require_once('phputil/classes/model/DefaultTestCreationProject.php');

class DefaultTestCreationProjectFactory {
    function &create_project($assoc_array) {
        if ( $assoc_array['AssignmentReviewType'] == DEFAULT_ASSIGNMENT_REVIEW ) {
            return DefaultTestCreationProject::from_assoc_array($assoc_array);
        }//if 

        return false;
    }//create_project
}//class DefaultTestCreationProjectFactory
?>
