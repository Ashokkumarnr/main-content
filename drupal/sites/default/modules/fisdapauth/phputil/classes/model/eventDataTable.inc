<?php
require_once('phputil/classes/model/abstractTable.inc');
require_once('phputil/classes/model/eventData.inc');

/**
 * This class models the DB table EventData.
 */
class EventDataTable extends AbstractTable
{
    /**
     * Constructor.
     * @param connection The DB connection.
     */
    function EventDataTable(&$connection)
    {
        parent::AbstractTable($connection, 'EventData');
    }

    /**
     * Retrieve an event.
     * @param id The event ID.
     * @return A single EventData object, or null if the event was not found.
     */
    function retrieveByEventId($id)
    {
        if(is_null($id)) return null;

        $statement = 'SELECT * FROM ' . $this->getTableName() .
            " WHERE Event_id = {$id}";
        $connection =& $this->getConnection();
        $results = $connection->query($statement);
        if(count($results) != 1) return null;

        return EventData::createFromResult($connection, $results[0]);
    }
}
?>
