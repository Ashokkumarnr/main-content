<?php 

/**
 * An object that displays on a page and responds to AJAX events.
 */
interface AjaxComponent {

	/**
	 * Return the HTML code for this object.
	 * @return string
	 */
	public function getHTML();

	/**
	 * Respond to an AJAX request that has been routed to this object.
	 *
	 * All parameters can be found in $_POST.  Should use echo_ajax() to send any output.
	 */
	public function respondAJAX();
}

?>
