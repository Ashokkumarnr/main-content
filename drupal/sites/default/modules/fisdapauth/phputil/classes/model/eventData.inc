<?php
/**
 * This class models the a row in the DB table EventData.
 * It is important to use the getters as some data is not
 * retrieved until it is accessed.
 */
class EventData
{
    // Straight from the table.
    var $ambulanceServiceId;
    var $available;     // not used ?
    var $classSection;
    var $dropPermissions;
    var $emails = array();
    var $endTime;
    var $eventId;
    var $expirationDate;
    var $hours;
    var $limited;
    var $location;
    var $notes;
    var $openSlots = 0;
    var $preceptorIds = array();
    var $programId;
    var $releaseDate;
    var $repeatCode;
    var $startBaseId;
    var $startDate;
    var $startTime;
    var $studentId;
    var $studentNotes;
    var $totalSlots;
    var $trade;         // not used ?
    var $tradePermissions;
    var $type;

    // Internals.
    var $connection;
    var $preceptorIdsComputed = false;

    /**
     * Constructor.
     * Don't rely on the data being set to anything reasonable.
     * @param connection The DB connection.
     */
    function EventData($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Create the objects from an array of results.
     * @param connection The DB connection.
     * @param results An array of SQL results sets.
     * @return An array of objects, may be zero length.
     */
    function &createFromResults(&$connection, &$results)
    {
        if(is_null($results)) return array();

        $list = array();
        $n = count($results);

        for ($i = 0; $i < $n; ++$i)
        {
            $list[] = &EventData::createFromResult($connection, $results[$i]);
        }

        return $list;
    }

    /**
     * Create an object from a result.
     * @param connection The DB connection.
     * @param result An SQL result set (no array).
     * @return The object, or null if result is null.
     */
    function &createFromResult(&$connection, &$result)
    {
        if(is_null($result)) return null;

        $data = new EventData($connection);
        $data->ambulanceServiceId = $result['AmbServ_id'];
        $data->available = $result['Available'];
        $data->classSection = $result['ClassSection'];
        $data->dropPermissions = $result['DropPermissions'];
        $data->endTime = $result['EndTime'];
        $data->eventId = $result['Event_id'];
        $data->expirationDate =  $result['ExpirationDate'];
        $data->hours =  $result['Hours'];
        $data->limited =  $result['Limited'];
        $data->location = $result['Location'];
        $data->notes = $result['Notes'];
        $data->programId = $result['Program_id'];
        $data->releaseDate =  $result['ReleaseDate'];
        $data->repeatCode = $result['RepeatCode'];
        $data->startBaseId = $result['StartBase_id'];
        $data->startDate =  $result['StartDate'];
        $data->startTime =  $result['StartTime'];
        $data->studentId = $result['Student_id'];
        $data->studentNotes = $result['StudentNotes'];
        $data->totalSlots = $result['TotalSlots'];
        $data->trade = $result['Trade'];
        $data->tradePermissions = $result['TradePermissions'];
        $data->type = $result['Type'];

        // Break the emails into an array.
        // They are either comma or semi-colon separated.
        $emails = $result['EmailList'];
        if(!is_null($emails))
        {
            $emails = trim($emails);
            if($emails != '')
            {
                $data->emails = preg_split('/\,|\;]/', $emails, 
                    PREG_SPLIT_NO_EMPTY);
            }
        }

        return $data;
    }

    /**
     * Retrieve the ambulance service ID.
     * @return The ambulance service ID.
     */
    function getAmbulanceServiceId()
    {
        return $this->ambulanceServiceId;
    }

    /**
     * Retrieve the class section.
     * @return The class section.
     */
    function getClassSection()
    {
        return $this->classSection;
    }

    /**
     * Retrieve the email addresses.
     * @return An array of email addresses.
     */
    function getEmails()
    {
        return $this->emails;
    }

    /**
     * Retrieve the end time.
     * @return The end time.
     */
    function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Retrieve the event ID.
     * @return The event ID.
     */
    function getEventId()
    {
        return $this->eventId;
    }

    /**
     * Retrieve the expiration data.
     * @return The expiration data.
     */
    function getExpirationData()
    {
        return $this->expirationDate;
    }

    /**
     * Retrieve the hours.
     * @return The hours.
     */
    function getHours()
    {
        return $this->hours;
    }

    /**
     * Retrieve the location.
     * @return The location or NULL.
     */
    function getLocation()
    {
        return $this->location;
    }

    /**
     * Retrieve the notes.
     * @return The notes or NULL.
     */
    function getNotes()
    {
        return $this->notes;
    }

    /**
     * Retrieve the number of open slots.
     * This is NOT cached.
     * @return The number of open slots.
     */
    function getOpenSlots()
    {
        // Find the number of used slots.
        $statement = 'SELECT count(*) as N FROM ShiftData' .
            " WHERE Event_id={$this->eventId} AND Student_id > 0";
        $results = $this->connection->query($statement);
        $nUsed = count($results) ? $results[0]['N'] : 0;

        $this->openSlots =  $this->totalSlots - $nUsed;
        return $this->openSlots;
    }

    /**
     * Retrieve the preceptor ID's.
     * This is cached.
     * @return An array of the ID's, may be zero length.
     */
    function getPreceptorIds()
    {
        if(!$this->preceptorIdsComputed)
        {
            $statement = 'SELECT Preceptor_id FROM EventPreceptorData' .
                " WHERE Event_id={$this->eventId}";
            $results = $this->connection->query($statement);
            $n = count($results);

            $this->preceptorIds = array();
            for( $i = 0; $i < $n; ++$i)
            {
                $this->preceptorIds[] = $results[$i]['Preceptor_id'];
            }

            $this->preceptorIdsComputed = true;
        }

        return $this->preceptorIds;
    }

    /**
     * Retrieve the program ID.
     * @return The program ID.
     */
    function getProgramId()
    {
        return $this->programId;
    }

    /**
     * Retrieve the release date.
     * @return The release date.
     */
    function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * Retrieve the repeat code.
     * @return The repeat code.
     */
    function getRepeatCode()
    {
        return $this->repeatCode;
    }

    /**
     * Retrieve the start base ID.
     * @return The start base ID.
     */
    function getStartBaseId()
    {
        return $this->startBaseId;
    }

    /**
     * Retrieve the start date.
     * @return The start date.
     */
    function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Retrieve the start time.
     * @return The start time.
     */
    function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Retrieve the student ID.
     * @return The student ID.
     */
    function getStudentId()
    {
        return $this->studentId;
    }

    /**
     * Retrieve the student notes.
     * @return The student notes or NULL.
     */
    function getStudentNotes()
    {
        return $this->studentNotes;
    }

    /**
     * Retrieve the total number of slots.
     * @return The total number of slots.
     */
    function getTotalSlots()
    {
        return $this->totalSlots;
    }

    /**
     * Retrieve the type.
     * @return The type.
     */
    function getType()
    {
        return $this->type;
    }

    /**
     * Retrieve the drop permissions.
     * @return The drop permissions.
     */
    function getDropPermissions()
    {
        return $this->dropPermissions;
    }

    /**
     * Retrieve the trade permissions.
     * @return The trade permissions.
     */
    function getTradePermissions()
    {
        return $this->tradePermissions;
    }

    /**
     * Determine if there are any limits on the event.
     * @return TRUE if there are limits.
     */
    function isLimited()
    {
        return ($this->limited != 0);
    }
}
?>
