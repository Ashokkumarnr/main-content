<?php
 
require_once('AbstractModel.inc');

/**
 * Represents a program in the DB.
 * #######################################################################
 * This class only handles get functions until all fields are implemented.
 * #######################################################################
 */
class MiniEval extends AbstractModel {

		// protected MiniEval_id;
		protected $minieval_desc;

		const DB_TABLE_NAME = 'MiniEval';
		const ID_FIELD = 'MiniEval_id';

	public function __construct($data) {
		$this->construct_helper($data, self::DB_TABLE_NAME, self::ID_FIELD);
	}

	public function get_fieldmap() {
		$map = array(
					// 'MiniEval_id' => 'not used'
					'MiniEval_desc' => 'minieval_desc'
        );
		return $map;
	}
	
	public function get_summary() {
		return $this->get_string();
	}

	/**
	 * Creates objects based on a MySQL result set so the database doesn't need to be hit more than once.
	 *
	 * @param result A result set containing one or more complete rows. The rows should be complete records, or we have to hit the DB anyway, which completely and totally defeats the purpose.
	 * @pre The result set CANNOT use alias names.
	 * @return array An array of objects.
	 * @todo can we move this to the parent class and use constants for the class-specific info (ID field name)?
	 */
	public static function getBySQL($result) {
		return parent::sql_helper($result, 'MiniEval', self::ID_FIELD);
	}

	/**
	 * Set the minieval description
	 * @param string $minieval_desc The description of the skill being evaluated
	 */
	public function set_minieval_desc($minieval_desc){
	  $this->set_helper("string", $minieval_desc, "minieval_desc");
  }
}
?>
