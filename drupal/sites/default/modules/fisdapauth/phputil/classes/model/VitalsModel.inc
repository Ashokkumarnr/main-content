<?php

/**
 * Model for VitalsData table in database
 *
 * @author Sam Tape stape@fisdap.net
 *
 */

require_once('ModelFactory.inc');
require_once('DataModel.inc');
require_once('VitalsLungSoundsModel.inc');
require_once('VitalsSkinModel.inc');

class Vitals extends DataModel {

	/*********
	 * FIELDS
	 *********/
	protected $assessment_id;
	protected $pulse_rate;
	protected $pulse_quality;
	protected $resp_rate;
	protected $resp_quality;
	protected $systolicbp;
	protected $diastolicbp;
	protected $sao2;
	protected $pupils_equal;
	protected $pupils_round;
	protected $pupils_reactive;
	protected $apgar;
	protected $gcs;
	protected $blood_glucose;
	protected $lung_sound_models;
	protected $skin_models;
	protected $time;
	
	const dbtable = "VitalsData";
	const id_field = "Vitals_id";

	/**************
	 * CONSTRUCTOR
	 **************/

	public function __construct($id=null) {
		parent::__construct();

		if (is_array($id)) {
			$id = $id[self::id_field];
		}

		if ($id == null) {
			$this->lung_sound_models = array();
			$this->skin_models = array();
		} else {
			$this->lung_sound_models = ModelFactory::getLungSoundsByVitals($id);
			$this->skin_models = ModelFactory::getSkinByVitals($id);
		}

		if (!$this->construct_helper($id, self::dbtable, self::id_field)) {
			return false;
		}
	}

	/************
	 * FUNCTIONS
	 ************/

	public function set_assessment_id($id) {
		return $this->set_helper('int', $id, 'assessment_id');
	}

	public function set_pulse_rate($pulse) {
		if ($pulse === null) {
			return $this->set_helper('int', -1, 'pulse_rate');
		} else if ($pulse >= -1 && $pulse <= 350) {
			return $this->set_helper('int', intval($pulse), 'pulse_rate');
		}
	}

	public function set_pulse_quality($quality) {
		if ($quality > 0 || is_string($quality)) {
			$quality = $this->id_or_string_helper($quality, 'get_pulse_quality_names');
		}

		return $this->set_helper('int', $quality, 'pulse_quality');
	}

	public function set_resp_rate($resp) {
		if ($resp === null) {
			return $this->set_helper('int', -1, 'resp_rate');
		} else if ($resp >= -1 && $resp <= 100) {
			return $this->set_helper('int', intval($resp), 'resp_rate');
		}
	}

	public function set_resp_quality($quality) {
		if ($quality > 0 || is_string($quality)) {
			$quality = $this->id_or_string_helper($quality, 'get_resp_quality_names');
		}

		return $this->set_helper('int', $quality, 'resp_quality');
	}

	public function set_systolicbp($_bp) {
		if (is_numeric($_bp)) {
			return $this->set_helper("int",$_bp,"systolicbp");
		} else {
			return false;
		}
	}

	public function set_diastolicbp($_bp) {
		if (strtoupper($_bp) == "P") {
			return $this->set_helper("int", -2, "diastolicbp");
		} else if (is_numeric($_bp)) {
			return $this->set_helper("int", $_bp, "diastolicbp");
		}
	}

	public function set_sao2($sao2) {
		$sao2 = str_replace("%", "", $sao2);

		if ($sao2 == null) {
			return $this->set_helper('int', -1, 'sao2');
		} else if ($sao2 >= -1 && $sao2 <= 100) {
			return $this->set_helper('int', intval($sao2), 'sao2');
		}
	}

	public function set_pupils_equal($pupils) {
		if ($pupils >= -1 && $pupils <= 1) {
			return $this->set_helper('int', $pupils, 'pupils_equal');
		} else {
			return false;
		}
	}
	
	public function set_pupils_round($pupils) {
		if ($pupils >= -1 && $pupils <= 1) {
			return $this->set_helper('int', $pupils, 'pupils_round');
		} else {
			return false;
		}
	}
	
	public function set_pupils_reactive($pupils) {
		if ($pupils >= -1 && $pupils <= 1) {
			return $this->set_helper('int', $pupils, 'pupils_reactive');
		} else {
			return false;
		}
	}

	public function set_apgar($apgar) {
		if ($apgar === null) {
			return $this->set_helper('int', -1, 'apgar');
		} else if ($apgar >= -1 && $apgar <= 10) {
			return $this->set_helper('int', intval($apgar), 'apgar');
		} 
	}

	public function set_gcs($gcs) {
		if ($gcs == null) {
			return $this->set_helper('int', -1, 'gcs');
		} else if ($gcs >= 3 && $gcs <= 15 || $gcs == -1) {
			return $this->set_helper('int', intval($gcs), 'gcs');
		} 
	}

	public function set_blood_glucose($glucose) {
		if ($glucose === null) {
			return $this->set_helper('int', -1, 'blood_glucose');
		} else if (strtoupper($glucose) == 'HIGH') {
			return $this->set_helper('int', 1001, 'blood_glucose');
		} else if (strtoupper($glucose) == 'LOW') {
			return $this->set_helper('int', 0, 'blood_glucose');
		} else if ($glucose >= 0 && $glucose <= 1000) {
			return $this->set_helper('int', intval($glucose), 'blood_glucose');
		}
	}

	public function add_lung_sounds($lung_sounds) {
		if (!$lung_sounds) {
			$lung_sounds = array();
		} else if (!is_array($lung_sounds)) {
			$lung_sounds = array($lung_sounds);
		}

		$lung_sound_ids = $this->get_lung_sounds_ids();

		foreach ($lung_sounds as $lung_sound) {
			if (!in_array($lung_sound, $lung_sound_ids)) {
				$lung_sound_model = new VitalsLungSounds();
				$lung_sound_model->set_lung_sound($lung_sound);
				if ($this->get_id() != null) {
					$lung_sound_model->set_vitals_id($this->get_id());
				}
				$this->lung_sound_models[] = $lung_sound_model;
			}
		}
	}

	public function remove_lung_sounds($lung_sounds) {
		if (!$lung_sounds) {
			$lung_sounds = array();
		} else if (!is_array($lung_sounds)) {
			$lung_sounds = array($lung_sounds);
		}

		foreach ($lung_sounds as $lung_sound) {
			foreach ($this->lung_sound_models as $key=>$model) {
				if ($model->get_lung_sound() == $lung_sound) {
					$model->remove();
					unset($this->lung_sound_models[$key]);
				}
			}
		}
	}

	public function set_lung_sounds($lung_sounds) {
		if (!$lung_sounds) {
			$lung_sounds = array();
		} else if (!is_array($lung_sounds)) {
			$lung_sounds = array($lung_sounds);
		}

		//get current ids
		$current_ids = $this->get_lung_sounds_ids();

		//delete lung_sounds that aren't in the new array
		$to_be_deleted = array_diff($current_ids, $lung_sounds);
		$this->remove_lung_sounds($to_be_deleted);

		foreach ($lung_sounds as $lung_sound) {
			$this->add_lung_sounds($lung_sound);
		}
	}

	public function get_lung_sounds_ids() {
		$lung_sound_ids = array();
		
		foreach ($this->lung_sound_models as $model) {
			$lung_sound_ids[] = $model->get_lung_sound();
		}

		return $lung_sound_ids;
	}

	public function add_skins($skins) {
		if (!$skins) {
			$skins = array();
		} else if (!is_array($skins)) {
			$skins = array($skins);
		}
		
		//get current skin ids
		$skin_ids = $this->get_skin_ids();

		foreach ($skins as $skin) {
			//we don't want to add a skin if it already exists
			if (!in_array($skin, $skin_ids)) {
				$skin_model = new VitalsSkin();
				$skin_model->set_skin($skin);
				if ($this->get_id() != null) {
					$skin_model->set_vitals_id($this->get_id());
				}
				$this->skin_models[] = $skin_model;
			}
		}
	}

	public function remove_skins($skins) {
		if (!$skins) {
			$skins = array();
		} else if (!is_array($skins)) {
			$skins = array($skins);
		}

		foreach ($skins as $skin) {
			foreach ($this->skin_models as $key=>$model) {
				if ($model->get_skin() == $skin) {
					$model->remove();
					unset($this->skin_models[$key]);
				}
			}
		}
	}

	public function set_skins($skins) {
		if (!$skins) {
			$skins = array();
		} else if (!is_array($skins)) {
			$skins = array($skins);
		}

		//get current ids
		$current_ids = $this->get_skin_ids();

		//delete skins that aren't in the new array
		$to_be_deleted = array_diff($current_ids, $skins);
		$this->remove_skins($to_be_deleted);

		foreach ($skins as $skin) {
			$this->add_skins($skin);
		}
	}

	public function get_skin_ids() {
		$skin_ids = array();
		
		foreach ($this->skin_models as $model) {
			$skin_ids[] = $model->get_skin();
		}

		return $skin_ids;
	}



	public function set_time($time) {
		$time_obj = new FisdapTime($time);	
		return $this->set_helper('time', $time_obj, 'time');
	}

	public function get_systolicbp() {
		if ($this->systolicbp < 0) {
			return null;
		} else {
			return $this->systolicbp;
		}
	}

	public function get_diastolicbp() {
		if ($this->diastolicbp == -2) {
			return "P";
		} else if ($this->diastolicbp == -1) {
			return null;
		} else {
			return $this->diastolicbp;
		}
	}

	public function get_bp() {
		$sys = 	$this->get_systolicbp();
		$dia = $this->get_diastolicbp();

		if ($sys && $dia) {
			return $sys."/".$dia;
		} else {
			return null;
		}
	}

	public function get_time() {
		$time = FisdapTime::create_from_hms_string($this->time);

		return $time->get_military_time();
	}

	public function get_pulse_rate() {
		if ($this->pulse_rate >= 0) {
			return $this->pulse_rate;
		} else {
			return;
		}
	}

	public function get_pulse_quality_desc() {
		$names = self::get_pulse_quality_names();
		$quality = $this->get_pulse_quality();

		if ($quality > 0) {
			return $names[$quality];
		}
	}

	public function get_resp_rate() {
		if ($this->resp_rate >= 0) {
			return $this->resp_rate;
		} else {
			return;
		}
	}

	public function get_resp_quality_desc() {
		$names = self::get_resp_quality_names();
		$quality = $this->get_resp_quality();

		if ($quality > 0) {
			return $names[$quality];
		}
	}

	public function get_sao2() {
		if ($this->sao2 >= 0) {
			return $this->sao2;
		} else {
			return;
		}
	}

	public function get_blood_glucose() {
		if ($this->blood_glucose >= 0) {
			return $this->blood_glucose;
		} else {
			return;
		}
	}

	public function get_apgar() {
		if ($this->apgar >= 0) {
			return $this->apgar;
		} else {
			return;
		}
	}

	public function get_gcs() {
		if ($this->gcs >= 0) {
			return $this->gcs;
		} else {
			return;
		}
	}

	public function get_perrl() {
		$equal = $this->get_pupils_equal();
		$round = $this->get_pupils_round();
		$reactive = $this->get_pupils_reactive();

		if ($equal == 1 && $round == 1 && $reactive == 1) {
			return "Yes";
		} else if ($equal >= 0 || $round >= 0 || $reactive >= 0) {
			return "No";
		} else {
			return "N/A";
		}
	}

	public static function get_pulse_quality_names() {
		$names = parent::data_table_helper("VitalsDataPulseQualityEnum","id","PulseQualityName");
		$names[-1] = 'Unset';
		return $names;
	}

	public static function get_resp_quality_names() {
		$names = parent::data_table_helper("VitalsDataRespQualityEnum","id","RespQualityName");
		$names[-1] = 'Unset';
		return $names;
	}

	public static function get_skin_names() {
		$names = parent::data_table_helper("VitalsDataSkinEnum","id","SkinName");
		return $names;
	}

	public static function get_lung_sounds_names() {
		$names = parent::data_table_helper("VitalsDataLungSoundsEnum","id","LungSoundsName");
		return $names;
	}

	public function get_summary($length = null) {
		$lung_sounds_names = self::get_lung_sounds_names();
		$skin_names = self::get_skin_names();
		$summary = "";
		$summary_info = array();
		$summary .= $this->get_time().": ";	

		if ($this->get_systolicbp()) {
			$summary_info[] = "BP ".$this->get_systolicbp()."/".$this->get_diastolicbp();
		}
		if ($this->get_pulse_rate()) {
			$summary_info[] = "P ".$this->get_pulse_rate();
		}
		if ($this->get_resp_rate()) {
			$summary_info[] = "R ".$this->get_resp_rate();	
		}
		if (count($this->get_lung_sounds_ids()) > 0) {
			$lung_sounds = array();
			foreach ($this->get_lung_sounds_ids() as $lung_id) {
				$lung_sounds[] = $lung_sounds_names[$lung_id];
			}

			$summary_info[] = implode(', ', $lung_sounds);
		}
		if ($this->get_sao2()) {
			$summary_info[] = "SpO2 ".$this->get_sao2()."%";
		}
		if (count($this->get_skin_ids()) > 0) {
			$skins = array();
			foreach ($this->get_skin_ids() as $skin_id) {
				$skins[] = $skin_names[$skin_id];
			}

			$summary_info[] = "Skin - ".implode(', ', $skins);
		}
		if ($this->get_pupils_equal() == 1 && $this->get_pupils_round() == 1 && $this->get_pupils_reactive() == 1) {
			$summary_info[] = "PERRL?-Yes";
		} else if ($this->get_pupils_equal() >= 0 || $this->get_pupils_round() >= 0 || $this->get_pupils_reactive() >= 0) {
			$summary_info[] = "PERRL?-No";
		}
		if ($this->get_blood_glucose()) {
			$summary_info[] = "Glucose ".$this->get_blood_glucose();
		}
		if ($this->get_apgar()) {
			$summary_info[] = "APGAR ".$this->get_apgar();
		}
		if ($this->get_gcs()) {
			$summary_info[] = "GCS ".$this->get_gcs();
		}

		$summary .= implode("; ", $summary_info);

		//If we specified a length, cut it off after so many characters
		if ($length) {
			$summary_length = strlen($summary);
			$summary = substr($summary, 0, $length);
			
			if ($length < $summary_length) {
				$summary .= "...";
			}
		}

		return $summary;
	}

	public function get_mobile_summary() {
		//STUB
		return "";
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "vitals", self::id_field);
	}

	public function get_hook_ids() {
		//STUB
		return array();
	}

	public function get_fieldmap() {
		return array(
			'Shift_id' => 'shift_id',
			'Student_id' => 'student_id',
			'Run_id' => 'run_id',
			'Assessment_id' => 'assessment_id',
			'PerformedBy' => 'performed_by',
			'SubjectType_id' => 'subject_type',
			'PulseRate' => 'pulse_rate',
			'PulseQuality' => 'pulse_quality',
			'RespRate' => 'resp_rate',
			'RespQuality' => 'resp_quality',
			'SystolicBP' => 'systolicbp',
			'DiastolicBP' => 'diastolicbp',
			'SpO2' => 'sao2',
			'SkinColor' => 'skin_color',
			'SkinTemp' => 'skin_temp',
			'SkinCondition' => 'skin_condition',
			'APGAR' => 'apgar',
			'BloodGlucose' => 'blood_glucose',
			'LungSounds' => 'lung_sounds',
			'VitalsTime' => 'time',
			'PupilsEqual' => 'pupils_equal',
			'PupilsRound' => 'pupils_round',
			'PupilsReactive' => 'pupils_reactive',
			'GlasgowComaScore' => 'gcs',
			"Assessment_id"=>"assessment_id",
			);
	}

	public function remove() {
		foreach ($this->lung_sound_models as $model) {
			$model->remove();
		}
		foreach ($this->skin_models as $model) {
			$model->remove();
		}
		parent::remove();
	}

	public function save() {
		parent::save();
		$vitals_id = $this->get_id();

		foreach ($this->lung_sound_models as $model) {
			$model->set_vitals_id($vitals_id);
			$model->save();
		}

		foreach ($this->skin_models as $model) {
			$model->set_vitals_id($vitals_id);
			$model->save();
		}
	}
}


?>
