<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/

require_once("AbstractModel.inc");

class AmbulanceBase extends AbstractModel {
	/**
	 * The name of the base
	 * @var string
	 */
	protected $name;
	protected $ambulance_service_id;
	protected $ambulance_service_abbreviation;
	protected $city;
	protected $state;
	/**
	 * @todo eh?!
	 */
	protected $ip_address;
	protected $type;
	/**
	 * @var boolean
	 */
	protected $active;

	const dbtable = "AmbServ_Bases";
	const idfield = "Base_id";

	public function __construct($id=null) {
		return $this->construct_helper($id, self::dbtable, self::idfield);
	}

	/**
	 * @todo Some of the names are prefixed by spaces in the db
	 * ({@link http://trac.fisdapoffice.int/trac/fisdap/ticket/1003 ticket 
	 * #1003}). So we fix that here.  If that problem gets resolved, we can
	 * get rid of this extra stuff.
	 */
	public function get_name() {
		return trim($this->name);
	}

	/**
	 * @todo This is a stub. We may want to have a modification of
	 * this field ripple through things like abbrev, city, state, and active?
	 */
	public function set_ambulance_service_id($id) {
		$this->set_helper('int', $id, 'ambulance_service_id');
		//STUB
	}

	public function get_summary() {
		return "Base: " . $this->get_name() . "("
			. $this->get_ambulance_service_abbreviation() . ")";
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "ambulance_base", self::idfield);
	}

	public function get_settable_fields() {
		return array('name' => 'string',
			'active' => 'bool',
			'type' => 'string');
	}

	public function get_fieldmap() {
		return array(
			"BaseName" => "name",
			"AmbServ_id" => "ambulance_service_id",
			"AmbServAbrev" => "ambulance_service_abbreviation",
			"City" => "city",
			"State" => "state",
			"IPAddress" => "ip_address",
			"Type" => "type",
			"Active" => "active");
	}
}
