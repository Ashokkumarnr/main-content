<?php

/* Done? */

require_once("DataModel.inc");

class Med extends DataModel {

	/*********
	 * FIELDS
	 *********/
	protected $medication;
	protected $dose;
	protected $route;
	const dbtable = "MedData";
	const id_field = "Med_id";

	/**************
	 * CONSTRUCTOR
	 **************/

	public function __construct($id=null) {
		parent::__construct();
		if (!$this->construct_helper($id, self::dbtable, self::id_field)) {
			return false;
		}
	}

	/************
	 * FUNCTIONS
	 ************/

	public function set_medication($med) {
		$id = $this->id_or_string_helper($med, "get_med_names");
		if ($id) {
			$this->set_helper("int", $id, "medication");
			return true;
		}
		return false;
	}

	public function set_med_route($route) {
		$id = $this->id_or_string_helper($route, "get_med_routes");
		if ($id) {
			$this->set_helper("int", $id, "route");
			return true;
		}
		return false;
	}

	public function get_dose() {
		return $this->dose;
	}

	public function set_dose($dose) {
		return $this->set_helper('string', $dose, "dose");
	}

	public function get_summary() {
		$names = self::get_med_names();
		return $names[$this->medication];
	}

	public function get_mobile_summary() {
		$names = self::get_med_names();
		$routes = self::get_med_routes();
		$summary = "<div style='font-weight:bold;'>";

		$summary .= $names[$this->medication]." ";	

		if ($this->get_performed_by() == 0) {
			$summary .= "(Performed)";
		} else if ($this->get_performed_by() == 1) {
			$summary .= "(Observed)";
		}

		$summary .= "</div><div>";
		$summary .= $this->dose.", ".$routes[$this->route]."</div>";
		return $summary;	
	}

	public static function get_med_names() {
		return self::data_table_helper("MedNameTable", "MedName_id", "MedName", "MedName");
	}

	public static function get_med_routes() {
		return self::data_table_helper("MedRouteTable", "Route_id", "RouteName", "RouteName");
	}

	public function get_hook_ids() {
		Assert::is_true($this->shift_id > 0);

		$shift = new Shift($this->shift_id);
		$shift_type = $shift->get_type();

		if ($shift_type == 'field') {
			return array(37);
		} else if ($shift_type == 'clinical') {
			return array(58);
		} else if ($shift_type == 'lab') {
			return array(81);
		} else {
			return array();
		}	
	}

	/*
	 * FUNCTION getBySQL
	 *
	 * Creates Med objects based on a MySQL result set so the database doesn't need to be hit more than once.
	 *
	 * @param result A result set containing on or more complete MedData rows. The rows should be complete MedData records, or we have to hit the DB anyway, which completely and totally defeats the purpose. 
	 * @pre The result set CANNOT use alias names.
	 * @return array An array of Meds.
	 * @TODO untested
	 * @TODO can we move this to the parent class and use constants for the class-specific info (ID field name)?
	 */
	public static function getBySQL($result) {
		return self::sql_helper($result, "med", self::id_field);
	}

	public function get_fieldmap() {
		return array("Run_id" => "run_id",
				"Student_id" => "student_id",
				"PerformedBy" => "performed_by",
				"Medication" => "medication",
				"Route" => "route",
				"Dose" => "dose",
				"Shift_id" => "shift_id",
				"SubjectType_id" => "subject_type",
				"Assessment_id"=>"assessment_id",
			);
	}

}

?>
