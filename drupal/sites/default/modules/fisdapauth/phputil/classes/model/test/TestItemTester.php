<?php

require_once('phputil/classes/model/TestItem.php');
require_once('phputil/classes/model/test/MockTestItem.php');
require_once('phputil/classes/model/TestItemReviewFactory.php');

/**
 *
 */
class TestItemTester extends UnitTestCase {
    function setUp() {
        $basic_review_row = array(
            'id'=>'35',
            'number_in_group'=>'1',
            'created_at'=>'2006-06-12',
            'stem_grammar_correct'=>'1',
            'distractors_match_grammar'=>'1',
            'distractors_less_correct'=>'1',
            'distractors_similar_yet_distinct'=>'1',
            'answer_always_correct'=>'1',
            'item_free_from_complication'=>'1',
            'item_unbiased'=>'1',
            'level_of_difficulty_matches_job'=>'1',
            'important_to_memorize'=>'1'
        );

        $this->review_factory = new TestItemReviewFactory();

        $assignment_review_input = $basic_review_row;
        $assignment_review_input['mode'] = 'assignment';
        $assignment_review_input['AssignmentReviewType'] = DEFAULT_ASSIGNMENT_REVIEW;

        $consensus_review_input = $basic_review_row;
        $consensus_review_input['mode'] = 'consensus';
        $assignment_review_input['AssignmentReviewType'] = DEFAULT_ASSIGNMENT_REVIEW;

        $consensus_review_input_3 = $consensus_review_input;
        $consensus_review_input_3['number_in_group'] = '3';

        $singleton_review_input = $basic_review_row;
        unset($singleton_review_input['level_of_difficulty_matches_job']);
        unset($singleton_review_input['important_to_memorize']);
        $singleton_review_input['mode'] = 'assignment';
        $singleton_review_input['AssignmentReviewType'] = SINGLETON_ASSIGNMENT_REVIEW;

        $singleton_review_input_later = $singleton_review_input;
        $singleton_review_input_later['id'] = '9999999';
        $singleton_review_input_later['created_at'] = '2006-06-15';

        // create some reviews (favorable)
        $this->r_a_good =& $this->review_factory->create_review_from_contents(
            $assignment_review_input
        );
        $this->r_s_good =& $this->review_factory->create_review_from_contents(
            $singleton_review_input
        );
        $this->r_s_good_later =& $this->review_factory->create_review_from_contents(
            $singleton_review_input_later
        );
        $this->r_c_1_good =& $this->review_factory->create_review_from_contents(
            $consensus_review_input
        );
        $this->r_c_3_good =& $this->review_factory->create_review_from_contents(
            $consensus_review_input_3
        );

        // make the reviews pick on the item's grammar
        $assignment_review_input['stem_grammar_correct'] = '0';
        $assignment_review_input['distractors_match_grammar'] = '0';
        $singleton_review_input['distractors_less_correct'] = '0';
        $singleton_review_input_later['distractors_less_correct'] = '0';
        $consensus_review_input['distractors_match_grammar'] = '0';
        $consensus_review_input['stem_grammar_correct'] = '0';
        $consensus_review_input_3['distractors_match_grammar'] = '0';
        $consensus_review_input_3['stem_grammar_correct'] = '0';

        // create some more reviews (unfavorable)
        $this->r_a_bad_grammar =& $this->review_factory->create_review_from_contents(
            $assignment_review_input
        );
        $this->r_s_bad_grammar =& $this->review_factory->create_review_from_contents(
            $singleton_review_input
        );
        $this->r_s_bad_grammar_later =& $this->review_factory->create_review_from_contents(
            $singleton_review_input_later
        );
        $this->r_c_1_bad_grammar =& $this->review_factory->create_review_from_contents(
            $consensus_review_input
        );
        $this->r_c_3_bad_grammar =& $this->review_factory->create_review_from_contents(
            $consensus_review_input_3
        );

        // make a couple of reviews that pick on the ~87% requirement questions
        $assignment_review_input['answer_always_correct'] = '0';
        $this->r_a_bad_incorrect =& $this->review_factory->create_review_from_contents(
            $assignment_review_input
        );
    }//setUp

    function test_update_without_reviews() {
        // no reviews (should be UNDETERMINED)
        $this->check_status_for_reviews(
            array(),
            DEFAULT_ASSIGNMENT_REVIEW,
            UNDETERMINED_ITEM_VALIDITY,
            __LINE__.' -- item with no reviews should\'ve been undetermined'
        );
    }//test_update_without_reviews


    function test_update_review_status_singleton() {
        // 1 favorable singleton review (should be VALIDATED)
        $this->check_status_for_reviews(
            array(
                $this->r_s_good
            ),
            SINGLETON_ASSIGNMENT_REVIEW,
            VALIDATED_ITEM_VALIDITY
        );

        // 1 unfavorable singleton review (should be NEEDS_REVISION)
        $this->check_status_for_reviews(
            array(
                $this->r_s_bad_grammar
            ),
            SINGLETON_ASSIGNMENT_REVIEW,
            NEEDS_REVISION_ITEM_VALIDITY
        );

        // no singleton reviews (should be UNDETERMINED)
        $this->check_status_for_reviews(
            array(),
            SINGLETON_ASSIGNMENT_REVIEW,
            UNDETERMINED_ITEM_VALIDITY
        );


        ///////////////////////////////////////////////////////////////////////////
        // test singleton project handling of other types of reviews
        ///////////////////////////////////////////////////////////////////////////


        // 1 favorable singleton, buncha other unfavorable reviews (should be VALIDATED)
        $this->check_status_for_reviews(
            array(
                $this->r_s_good,
                $this->r_c_3_bad_grammar,
                $this->r_c_3_bad_grammar,
                $this->r_a_bad_grammar,
                $this->r_a_bad_grammar,
                $this->r_a_bad_grammar,
                $this->r_a_bad_grammar,
                $this->r_a_bad_grammar
            ),
            SINGLETON_ASSIGNMENT_REVIEW,
            VALIDATED_ITEM_VALIDITY
        );

        // 1 unfavorable singleton, buncha other favorable reviews (should be INVALID)
        $this->check_status_for_reviews(
            array(
                $this->r_s_bad_grammar,
                $this->r_c_3_good,
                $this->r_c_3_good,
                $this->r_a_good,
                $this->r_a_good,
                $this->r_a_good,
                $this->r_a_good,
                $this->r_a_good
            ),
            SINGLETON_ASSIGNMENT_REVIEW,
            NEEDS_REVISION_ITEM_VALIDITY
        );


        ///////////////////////////////////////////////////////////////////////////
        // test default handling of singleton reviews
        ///////////////////////////////////////////////////////////////////////////

        // normal favorable reviews, buncha unfavorable singleton reviews (should be VALID)
        $this->check_status_for_reviews(
            array(
                $this->r_c_3_good,
                $this->r_s_bad_grammar,
                $this->r_s_bad_grammar,
                $this->r_s_bad_grammar,
                $this->r_s_bad_grammar,
                $this->r_s_bad_grammar,
                $this->r_s_bad_grammar,
                $this->r_s_bad_grammar,
                $this->r_s_bad_grammar
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            VALIDATED_ITEM_VALIDITY
        );

        // normal unfavorable reviews, buncha favorable singleton reviews (should be INVALID)
        $this->check_status_for_reviews(
            array(
                $this->r_c_3_bad_grammar,
                $this->r_s_good,
                $this->r_s_good,
                $this->r_s_good,
                $this->r_s_good,
                $this->r_s_good,
                $this->r_s_good,
                $this->r_s_good,
                $this->r_s_good
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            NEEDS_REVISION_ITEM_VALIDITY
        );


        ///////////////////////////////////////////////////////////////////////////
        // test ordering of singleton reviews (latest should take precedence)
        ///////////////////////////////////////////////////////////////////////////

        // early favorable, late unfavorable (should be INVALID)
        $this->check_status_for_reviews(
            array(
                $this->r_s_good,
                $this->r_s_bad_grammar_later,
                $this->r_s_good,
                $this->r_s_good
            ),
            SINGLETON_ASSIGNMENT_REVIEW,
            NEEDS_REVISION_ITEM_VALIDITY
        );

        // early unfavorable, late favorable (should be VALID)
        $this->check_status_for_reviews(
            array(
                $this->r_s_bad_grammar,
                $this->r_s_good_later,
                $this->r_s_bad_grammar,
                $this->r_s_bad_grammar,
                $this->r_s_bad_grammar,
                $this->r_s_bad_grammar
            ),
            SINGLETON_ASSIGNMENT_REVIEW,
            VALIDATED_ITEM_VALIDITY
        );

    }//test_update_review_status_singleton


    function test_update_review_status_consensus() {
        // 1 consensus review, 1 reviewer (should be UNDETERMINED)
        $this->check_status_for_reviews(
            array(
                $this->r_c_1_good
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            UNDETERMINED_ITEM_VALIDITY,
            __LINE__.' -- item with 1 reviewer should\'ve been untetermined'
        );


        // 1 consensus review, 3 reviewers 
        // should be valid
        $this->check_status_for_reviews(
            array(
                $this->r_c_3_good
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            VALIDATED_ITEM_VALIDITY,
            __LINE__.' -- item with 3 reviewers should\'ve been valid'
        );

        // should be needs revision
        $this->check_status_for_reviews(
            array(
                $this->r_c_3_bad_grammar
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            NEEDS_REVISION_ITEM_VALIDITY,
            __LINE__.' -- item with 3 reviewers should\'ve been invalid'
        );
    }//test_update_review_status_consensus


    /**
     * Test one or more assignments
     */
    function test_update_review_status_assignments() {
        // 1 assignment review (should be UNDETERMINED)
        $this->check_status_for_reviews(
            array(
                $this->r_a_good
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            UNDETERMINED_ITEM_VALIDITY,
            __LINE__.' -- item with 1 assignment review should\'ve been untetermined'
        );


        // 3 assignment reviews
        // should be valid (all good)
        $this->check_status_for_reviews(
            array(
                $this->r_a_good,
                $this->r_a_good,
                $this->r_a_good
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            VALIDATED_ITEM_VALIDITY,
            __LINE__.' -- item with 3 assignment reviews should\'ve been valid'
        );

        // should be valid (one said bad grammar)
        $this->check_status_for_reviews(
            array(
                $this->r_a_bad_grammar,
                $this->r_a_good,
                $this->r_a_good
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            VALIDATED_ITEM_VALIDITY,
            __LINE__.' -- item with 3 assignment reviews should\'ve been valid'
        );

        // should be needs revision (2 said bad grammar)
        $this->check_status_for_reviews(
            array(
                $this->r_a_bad_grammar,
                $this->r_a_bad_grammar,
                $this->r_a_good
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            NEEDS_REVISION_ITEM_VALIDITY,
            __LINE__.'item with 3 assignment reviews should\'ve been invalid'
        );
    }//test_update_review_status_assignments


    function test_update_review_status_mixed() {
        // should be needs undetermined (consensus of 1, 1 assignment)
        $this->check_status_for_reviews(
            array(
                $this->r_a_bad_grammar,
                $this->r_c_1_good
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            UNDETERMINED_ITEM_VALIDITY
        );

        // should be needs revision (consensus of 1, 2 assignments)
        $this->check_status_for_reviews(
            array(
                $this->r_a_bad_grammar,
                $this->r_a_bad_grammar,
                $this->r_c_1_good
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            NEEDS_REVISION_ITEM_VALIDITY
        );

        // should be needs revision (consensus of 1, 2 assignments)
        $this->check_status_for_reviews(
            array(
                $this->r_a_good,
                $this->r_a_bad_grammar,
                $this->r_c_1_bad_grammar
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            NEEDS_REVISION_ITEM_VALIDITY
        );

        // should be valid (consensus of 1, 2 assignments)
        $this->check_status_for_reviews(
            array(
                $this->r_a_good,
                $this->r_a_good,
                $this->r_c_1_good
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            VALIDATED_ITEM_VALIDITY
        );
    }//test_update_review_status_mixed


    /**
     * This function tests the cases where there are enough negative
     * reviews to make a validity determination, even though there are
     * not yet three reviews
     */
    function test_update_review_status_quick_failure() {
        // 2 negative reviews, where the complaints overlap on at least
        // one point (and the complaints are about 66% requirement 
        // questions)
        $this->check_status_for_reviews(
            array(
                $this->r_a_bad_grammar,
                $this->r_a_bad_grammar
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            NEEDS_REVISION_ITEM_VALIDITY
        );

        // 1 negative review, where the complaint is on one of the 
        // ~87% requirement questions)
        $this->check_status_for_reviews(
            array(
                $this->r_a_bad_incorrect
            ),
            DEFAULT_ASSIGNMENT_REVIEW,
            NEEDS_REVISION_ITEM_VALIDITY
        );
    }//test_update_review_status_quick_failure

    
    /**
     * Helper function to make sure a set of reviews produces the 
     * given status.
     */
    function check_status_for_reviews( $reviews,
                                       $assignment_review_type,
                                       $expected_status,
                                       $message='' ) {
        $item =& new MockTestItem($assignment_review_type);
        $item->set_reviews($reviews);
        $email_warnings = false;
        $found_status = $item->update_review_status($email_warnings);
        if ( !$message ) {
            $message = 'expected status "'.$expected_status.'", got "'.$found_status.'"';
        }//if
        $this->assertEqual(
            $found_status,
            $expected_status,
            $message
        );
    }//check_status_for_reviews
}//class TestItemTester

?>
