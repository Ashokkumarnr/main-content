<?php
/**
 * This class models the a row in the DB table ProgramSiteAssoc.
 * It is important to use the getters as some data may not
 * retrieved until it is accessed.
 */
class ProgramSiteAssoc
{
    // Straight from the table.
    var $ambulanceServiceId;
    var $canCreateSharedShifts;
    var $canShare;
    var $assocId;
    var $defaultShared;
    var $instructorId;
    var $programId;

    /**
     * Constructor.
     * Don't rely on the data being set to anything reasonable.
     * @param connection The DB connection.
     */
    function ProgramSiteAssoc(&$connection)
    {
    }

    /**
     * Create the objects from an array of results.
     * @param connection The DB connection.
     * @param results An array of SQL results sets.
     * @return An array of objects, may be zero length.
     */
    function &createFromResults(&$connection, &$results)
    {
        if(is_null($results)) return array();

        $list = array();
        $n = count($results);

        for ($i = 0; $i < $n; ++$i)
        {
            $list[] = &ProgramSiteAssoc::createFromResult($connection, $results[$i]);
        }

        return $list;
    }

    /**
     * Create an object from a result.
     * @param connection The DB connection.
     * @param result An SQL result set (no array).
     * @return The object, or null if result is null.
     */
    function &createFromResult(&$connection, &$result)
    {
        if(is_null($result)) return null;

        $data = new ProgramSiteAssoc($connection);
        $data->ambulanceServiceId =  $result['Site_id'];
        $data->assocId = $result['Assoc_id'];
        $data->canShare = ($result['Approved'] == 1);
        $data->defaultShared =  $result['DefaultShared'];
        $data->instructorId =  $result['Instructor_id'];
        $data->canCreateSharedShifts = ($result['Main'] == 1);
        $data->programId = $result['Program_id'];
        return $data;
    }

    /**
     * Retrieve the ambulance service ID.
     * @return The ambulance service ID.
     */
    function getAmbulanceServiceId()
    {
        return $this->ambulanceServiceId;
    }

    /**
     * Retrieve the assoc ID.
     * @return The assoc ID.
     */
    function getAssocId()
    {
        return $this->assocId;
    }

    /**
     * Determine whether the program can be shared.
     * @return TRUE if it can be shared.
     */
    function canShare()
    {
        return $this->canShare;
    }

    /**
     * Retrieve the default shared.
     * @return The default shared.
     */
    function getDefaultShared()
    {
        return $this->defaultShared;
    }

    /**
     * Retrieve the instructor ID.
     * @return The instructor ID.
     */
    function getInstructorId()
    {
        return $this->instructorId;
    }

    /**
     * Determine whether shared shifts can be created.
     * @return TRUE if shared shifts can be created.
     */
    function canCreateSharedShifts()
    {
        return $this->canCreateSharedShifts;
    }

    /**
     * Retrieve the program ID.
     * @return The program ID.
     */
    function getProgramId()
    {
        return $this->programId;
    }
}
?>
