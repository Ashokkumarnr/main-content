<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/model/EnumeratedModel.inc');
require_once('phputil/classes/model/AbstractModel.inc');

/**
 * Defines an enumerated type in the DB.
 * The enumerated type is defined so that it can be used to display information to the users.
 * Note that since the column name in the DB is 'id', and the instance var 'id' 
 * has magic meaning to the AbstractModel class, we have to kludge around and save the 
 * 'id' in a different var.
 * @todo there are some shady things going on here with the id field. 
 * Investigate and remedy.
 */
abstract class AbstractEnumeratedModel extends AbstractModel implements EnumeratedModel {
    protected $position;
    protected $real_id;
    protected $text;

    const ID_COLUMN = 'id';

    const POSITION_FIELD = 'position';
    const TEXT_FIELD = 'text';

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     * @param string $table_name The table name.
     */
    public function __construct($data, $table_name) {
        $this->construct_helper($data, $table_name, self::ID_COLUMN);
    }

    /**
     * Set the ID.
     * @param int $id The ID.
     */
    public function set_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'real_id');
    }

    /**
     * Retrieve the ID.
     * This is needed for criteria use.
     * @param int $id The ID.
     */
    public function get_real_id() {
        return $this->real_id;
    }

    /**
     * Set the ID.
     * This is needed for criteria use.
     * @param int $id The ID.
     */
    public function set_real_id($id) {
        $this->set_id($id);
    }

    /**
     * Retrieve the text describing the entry.
     * @return string The text.
     */
    public function get_text() {
        return $this->text;
    }

    /**
     * Set the text describing the entry.
     * @param string $text The text.
     */
    public function set_text($text) {
        Assert::is_not_empty_trimmed_string($text);

        $this->set_helper('string', trim($text), 'text');
    }

    /**
     * Retrieve the ordering position.
     * Smaller numbers should appear first to the user.
     * @return int The position, at least 1.
     */
    public function get_position() {
        return $this->position;
    }

    /**
     * Set the position.
     * Smaller numbers should appear first to the user.
     * @param int $postiion The position, at least 1.
     */
    public function set_position($position) {
        Assert::is_int($position);
        Assert::is_true($position > 0);

        $this->set_helper('int', $position, 'position');
    }

    public function get_fieldmap() {
        return array(
			'id' => 'real_id',
        'Position' => self::POSITION_FIELD,
        'Text' => self::TEXT_FIELD
        );
    }

    protected function get_insert_id($connection) {
        return $this->real_id;
    }

    /**
     * Retrieve information to create prompts.
     * @param string $class_name The class name.
     * @return array A list of all the models sorted by ascending position.
     */
    protected static function do_get_prompt_info($class_name) {
        Assert::is_not_empty_string($class_name);

        $model = new $class_name(null);
        $criteria = new ModelCriteria($model);
        $criteria->add_order_by(new OrderBy(self::POSITION_FIELD, true));

        $connection = FISDAPDatabaseConnection::get_instance();
        return $connection->get_by_criteria($criteria);
    }
}
?>
