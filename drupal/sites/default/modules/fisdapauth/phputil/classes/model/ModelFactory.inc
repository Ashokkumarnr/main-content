<?php
/*----------------------------------------------------------------------*
  |                                                                      |
  |     Copyright (C) 1996-2006.  This is an unpublished work of         |
  |                      Headwaters Software, Inc.                       |
  |                         ALL RIGHTS RESERVED                          |
  |     This program is a trade secret of Headwaters Software, Inc.      |
  |     and it is not to be copied, distributed, reproduced, published,  |
  |     or adapted without prior authorization                           |
  |     of Headwaters Software, Inc.                                     |
  |                                                                      |
 *----------------------------------------------------------------------*/

/**
 * Contains static methods for creating various models in a number of ways.
 * @TODO Break into multiple factory objects, organized by what they spit out?
 * @TODO ALL UNTESTED
 */
class ModelFactory {

	protected static function try_to_include($filename) {
		$success = @include_once $filename;
		if ($success) return true;
		else {
			require_once("common_utilities.inc");
			common_utilities::displayFunctionError("Could not include $filename.");
			return false;
		}
	}

	public static function getRunsByShift($shiftid) {
		ModelFactory::try_to_include("RunModel.inc");
		$result = self::queryByIdHelper("RunData", 'Shift_id', $shiftid);
		return Run::getBySQL($result);
	}

	/**
	 * @todo STUB
	 */
	public static function getByPt($model, $ptid) {
		Assert::is_true($ptid > 0);
		$funcname = "get" . $model . "sByPt";
		return self::$funcname($ptid);
	}

	public static function getByRun($model, $runid) {
		Assert::is_true($runid > 0);
		$funcname = "get" . $model . "sByRun";
		return self::$funcname($runid);
	}

	public static function getAirwaysByRun($runid) {
		Assert::is_true($runid > 0);
		ModelFactory::try_to_include("AirwayModel.inc");
		$result = self::runQueryHelper("ALSAirwayData", $runid);
		return Airway::getBySQL($result);
	}

	public static function getBLSSkillsByRun($runid) {
		Assert::is_true($runid > 0);
		ModelFactory::try_to_include("BLSSkillModel.inc");
		$result = self::runQueryHelper("BLSSkillsData", $runid);
		return BLSSkill::getBySQL($result);
	}

	public static function getComplaintsByRun($runid) {
		ModelFactory::try_to_include("ComplaintModel.inc");
		$result = self::runQueryHelper("PtComplaintData", $runid);
		return Complaint::getBySQL($result);
	}

	public static function getVitalssByRun($runid) {
		Assert::is_true($runid > 0);
		ModelFactory::try_to_include("VitalsModel.inc");
		$result = self::runQueryHelper("VitalsData", $runid);
		return Vitals::getBySQL($result);
	}

	public static function getECGsByRun($runid) {
		Assert::is_true($runid > 0);
		ModelFactory::try_to_include("ECGModel.inc");
		$result = self::runQueryHelper("EKGData", $runid);
		return ECG::getBySQL($result);
	}

	public static function getIVsByRun($runid) {
		Assert::is_true($runid > 0);
		ModelFactory::try_to_include("IVModel.inc");
		$result = self::runQueryHelper("IVData", $runid);
		return IV::getBySQL($result);
	}

	public static function getMedsByRun($runid) {
		Assert::is_true($runid > 0);
		ModelFactory::try_to_include("MedModel.inc");
		$result = self::runQueryHelper("MedData", $runid);
		return Med::getBySQL($result);
		//STUB
	}

	public static function getNarrativesByRun($runid) {
		Assert::is_true($runid > 0);
		ModelFactory::try_to_include("NarrativeModel.inc");
		$result = self::runQueryHelper("NarrativeData", $runid);
		return Narrative::getBySQL($result);	
	}

	public static function getOtherALSsByRun($runid) {
		Assert::is_true($runid > 0);
		ModelFactory::try_to_include("OtherALSModel.inc");
		$result = self::runQueryHelper("OtherALSData", $runid);
		return OtherALS::getBySQL($result);	
	}


	public static function getPatientsByRun($runid) {
		ModelFactory::try_to_include("PatientModel.inc");
		$result = self::runQueryHelper("PtComplaintData", $runid);
		//STUB this only works for pts. if we combine assessments in with pts, then we need a better factory, and it probably should take in shift id too.
		return Patient::getBySQL($result);
		//STUB
	}


	public static function getByShift($model, $shiftid) {
		$funcname = "get" . $model . "sByShift";
		return self::$funcname($shiftid);
	}

	public static function getAirwaysByShift($shiftid) {
		ModelFactory::try_to_include("AirwayModel.inc");
		$result = self::shiftQueryHelper("ALSAirwayData", $shiftid);
		return Airway::getBySQL($result);
	}

	public static function getAssessmentsByShift($shiftid) {
		ModelFactory::try_to_include("PatientModel.inc");
		$result = self::shiftQueryHelper("AssesmentData", $shiftid);
		return patient::getBySQL($result);
	}

	public static function getBLSSkillsByShift($shiftid) {
		ModelFactory::try_to_include("BLSSkillModel.inc");
		$result = self::shiftQueryHelper("BLSSkillsData", $shiftid);
		return BLSSkill::getBySQL($result);
	}

	public static function getVitalssByShift($shiftid) {
		ModelFactory::try_to_include("VitalsModel.inc");
		$result = self::shiftQueryHelper("VitalsData", $shiftid);
		return Vitals::getBySQL($result);
	}

	public static function getECGsByShift($shiftid) {
		ModelFactory::try_to_include("ECGModel.inc");
		$result = self::shiftQueryHelper("EKGData", $shiftid);
		return ECG::getBySQL($result);
	}

	public static function getIVsByShift($shiftid) {
		if (is_null($shiftid)) common_utilities::displayFunctionError("shiftid is null.", true);
		ModelFactory::try_to_include("IVModel.inc");
		$result = self::shiftQueryHelper("IVData", $shiftid);
		return IV::getBySQL($result);
	}

	public static function getMedsByShift($shiftid) {
		ModelFactory::try_to_include("MedModel.inc");
		$result = self::shiftQueryHelper("MedData", $shiftid);
		return Med::getBySQL($result);
	}

	public static function getNarrativesByShift($shiftid) {
		ModelFactory::try_to_include("NarrativeModel.inc");
		$result = self::shiftQueryHelper("NarrativeData", $shiftid);
		return Narrative::getBySQL($result);	
	}

	public static function getOtherALSsByShift($shiftid) {
		ModelFactory::try_to_include("OtherALSModel.inc");
		$result = self::shiftQueryHelper("OtherALSData", $shiftid);
		return OtherALS::getBySQL($result);	
	}

	public static function getBLSSkillsByAsses($assesid) {
		Assert::is_true($assesid > 0);
		ModelFactory::try_to_include("BLSSkillModel.inc");
		$result = self::assesQueryHelper("BLSSkillsData", $assesid);
		return BLSSkill::getBySQL($result);
	}
	public static function getComplaintsByAsses($assesid) {
		Assert::is_true($assesid > 0);
		ModelFactory::try_to_include("ComplaintModel.inc");
		$result = self::queryByIdHelper("PtComplaintData","Asses_id",$assesid);
		return Complaint::getBySQL($result);
	}

	public static function getShiftsByStudent($student_id) {
		ModelFactory::try_to_include('ShiftModel.inc');
		$result = self::studentQueryHelper('ShiftData', $student_id, 'ORDER BY StartDate DESC');
		return Shift::getBySQL($result);
	}

	public static function getProgramPreceptorsByPreceptor($preceptor_id) {
		ModelFactory::try_to_include('ProgramPreceptorModel.inc');
		$result = self::preceptorQueryHelper('ProgramPreceptorData', $preceptor_id);
		return ProgramPreceptor::getBySQL($result);
	}

	private static function studentQueryHelper($table, $student_id, $orderby) {
		return self::queryByIdHelper($table, 'Student_id', $student_id, $orderby);
	}

	private static function runQueryHelper($table, $runid) {
		return self::queryByIdHelper($table, 'Run_id', $runid);
	}

	private static function shiftQueryHelper($table, $shiftid) {
		return self::queryByIdHelper($table, 'Shift_id', $shiftid);
	}

	private static function assesQueryHelper($table, $assesid) {
		return self::queryByIdHelper($table, 'Assesment_id', $assesid);
	}

	private static function preceptorQueryHelper($table, $preceptor_id) {
		return self::queryByIdHelper($table, 'Preceptor_id', $preceptor_id);
	}

	private function patientQueryHelper($table, $ptid) {
		return self::queryByIdHelper($table, 'Patient_id', $ptid);
		$connection =& FISDAPDatabaseConnection::get_instance();
	}

	private static function queryByIdHelper($table, $idname, $idval, $orderby = '') {
		$connection = FISDAPDatabaseConnection::get_instance();
		$query = "SELECT * FROM $table WHERE $idname=$idval $orderby";
		$result = $connection->query($query);
		return $result;
	}

	public static function getLungSoundsByVitals($vitals_id) {
		ModelFactory::try_to_include('VitalsLungSoundsModel.inc');
		$result = self::queryByIdHelper('VitalsLungSoundsData', 'Vitals_id', $vitals_id);
		return VitalsLungSounds::getBySQL($result);
	}

	public static function getSkinByVitals($vitals_id) {
		ModelFactory::try_to_include('VitalsSkinModel.inc');
		$result = self::queryByIdHelper('VitalsSkinData', 'Vitals_id', $vitals_id);
		return VitalsSkin::getBySQL($result);
	}

	public static function getRatingsByLinkId($link_id, $linked_table) {
		ModelFactory::try_to_include('RateDataModel.inc');
		$connection = FISDAPDatabaseConnection::get_instance();
		$query = "SELECT * FROM rate_Data WHERE rate_link_id = $link_id AND rate_link_table = '$linked_table'";
		$result = $connection->query($query);
		return PilotRating::getBySQL($result);
	}

	public static function getPreceptorsByProgramSite($program_id, $site_id) {
		$connection = FISDAPDatabaseConnection::get_instance();
		$query = "SELECT pd.* FROM PreceptorData pd, ProgramPreceptorData ppd WHERE pd.Preceptor_id = ppd.Preceptor_id AND Program_id = $program_id AND AmbServ_id = $site_id AND ppd.Active = 1 ORDER BY LastName";
		$result = $connection->query($result);
		return Preceptor::getBySQL($result);

	}
}

?>
