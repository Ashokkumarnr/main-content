<?php
/*---------------------------------------------------------------------*
  |                                                                     |
  |      Copyright (C) 1996-2010.  This is an unpublished work of       |
  |                       Headwaters Software, Inc.                     |
  |                          ALL RIGHTS RESERVED                        |
  |      This program is a trade secret of Headwaters Software, Inc.    |
  |      and it is not to be copied, distributed, reproduced, published |
  |      or adapted without prior authorization                         |
  |      of Headwaters Software, Inc.                                   |
  |                                                                     |
* ---------------------------------------------------------------------*/
/* questions for Eryn or Eric
	marked in code: qtodos
 */

require_once('phputil/classes/model/UserAbstractModel.inc');
require_once('phputil/classes/DateTime.inc');
require_once('phputil/classes/model/AccountTypeModel.inc');
require_once('phputil/handy_utils.inc');
require_once('phputil/AccountOrdering/genSerialNumber.php');
/*
	SerialNumberUtils::isInstructorSerialNumber($sn)
					   isValidSerialNumber($sn)
	GenFISDAPSerialNumber( $AccountType, $ConfigCode, $LimitCode, &$ReturnStr) 
	insertSNInDatabase($SerialNumber, $DistMethod, $Program_id, $PurchaseOrder, $AccountType, $ConfigLimit, $Configuration) {
 */

/*		phputil/AccountOrdering/Transaction.inc  has  lot of code of creating new serials
 */

/* Business rules enforced / automated:
	- When instructor_id or student_id is set to anything else than -15:
		- the other field (instructor_id or student_id) is set to -15
		- if activation date is not set it will be set to today's date

 */

/*
	Notes: programming type checking that takes lot of overhead (ex: some database lookups) 
		are enabled only in developer's mode
 */
class SerialNumbersModel extends UserAbstractModel {
	protected $serial_number;
	protected $student_id;
	protected $dist_method;
	protected $program_id;
	protected $purchase_order;
	protected $account_type;
	protected $scheduler;
	protected $pda_access;
	protected $has_synced_pda;
	protected $config_limit;
	protected $configuration;
	protected $order_datetime;
	protected $activation_datetime;
	protected $instructor_id;

	protected $in_dev_mode;

	/* currently not checking for correct account types
	protected static $account_types = array(
		'paramedic'
	);
	 */

	const DB_TABLE = "SerialNumbers";
	const ID_FIELD = "SN_id";

	public function get_user_type() {
		return ($this->get_account_type() == 'instructor') ? 'instructor' : 'student';
	}

	public function __construct($id=null) {
		$ret = self::construct_helper($id, self::DB_TABLE, self::ID_FIELD);
		parent::__construct($id);
		return $ret;
	}


	protected function load_batch_validation_rules() {
		$this->fields = array(
			'serial_number' => array(
				'is_required' => true
			)
		);
	}


	public static function get_by_serial_number($serial_number) {
		$connection = FISDAPDatabaseConnection::get_instance();

		$query = "SELECT * FROM ".self::DB_TABLE." sn where sn.`Number`='$serial_number'";
		//	echo self::DB_TABLE.$query."\n";
		$res = $connection->query($query);
		if (count($res)==1) {
			return new self($res[0]);
		} else {
			return null;
		}
	}

	public function load_by_serial($serial_number) {
		$rawdb_serial_data = $this->connection->query("SELECT * FROM SerialNumbers sn WHERE sn.Number = '$serial_number'");

		if ($rawdb_serial_data) {
			$this->rawdb_serial_data = $rawdb_serial_data[0];
		} else {
			$this->rawdb_serial_data = null;

		}

		show_array($rawdb_serial_data);

		$this->use_serial_data();
	}

	public function is_activated() {
		//	var_dump($this->get_id());
		if (is_null($this->get_id()))
			return null;

		return ($this->get_student_id()<>-15 || $this->get_instructor_id()<>-15);
	}

	public function get_summary() {
		return "Serial Number";
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "SerialNumbersModel", self::ID_FIELD);
	}
	/*
	public static function getBySQL($result) {
		return self::sql_helper($result, "iv", self::id_field);
	}
	}
	 */

	/*
		protected function populate_child_models() {
			$this->child_model_helper('gender_id', 'gender', 'Gender', true);
		}
	 */
	/**
	 * comment: get_fieldmap() probably should be static in AbstractModel
	 */
	public function get_fieldmap() {
		return array(
			'Number' => 'serial_number',
			'Student_id' => 'student_id',
			'DistMethod' => 'dist_method',
			'Program_id' => 'program_id',
			'EntryTime' => 'entry_time',
			'PurchaseOrder' => 'purchase_order',
			'AccountType' => 'account_type',
			'Scheduler' => 'scheduler',
			'PDAAccess' => 'pda_access',
			'HasSyncedPDA' => 'has_synced_pda',
			'ConfigLimit' => 'config_limit',
			'Configuration' => 'configuration',
			'OrderDate' => 'order_datetime',
			'ActivationDate' => 'activation_datetime',
			'Instructor_id' => 'instructor_id'
		);
	}

	public function set_all_to_defaults() {

	}

	public function set_serial_number($serial_number) {
		return $this->set_helper("string", $serial_number, "serial_number");
		// return $this->set_helper("string", self::id_or_string_helper($serial_number, "get_iv_sites"), "site");
	}

	public function set_student_id($student_id) {
		if ($student_id <> -15) {
			$this->set_instructor_id(-15);
		}
		$ret = $this->set_helper("int", $student_id, "student_id");
		$this->sync_activationdate();

		//		echo "Student changed: ".$instructor_id." - ".$this->get_instructor_id()."\n";
		return $ret;
	}

	public function set_dist_method($dist_method) {
		return $this->set_helper("string", $dist_method, "dist_method");
	}

	public function set_program_id($program_id) {
		return $this->set_helper("int", $program_id, "program_id");
	}

	public function set_purchase_order($purchase_order) {
		return $this->set_helper("string", $purchase_order, "purchase_order");
	}

	public function set_account_type($account_type) {
		//if (!in_array($this->account_types, $account_type) && $account_type<>'') {
		//	throw new FisdapInvalidArgumentException("Invalid account type: $account_type");
		//} else {
		return $this->set_helper("string", $account_type, "account_type");
		//}
	}

	public function set_scheduler($scheduler) {
		return $this->set_helper("int", $scheduler, "scheduler");
	}

	public function set_pda_access($pda_access) {
		return $this->set_helper("int", $pda_access, "pda_access");
	}

	public function set_has_synced_pda($has_synced_pda) {
		return $this->set_helper("int", $has_synced_pda, "has_synced_pda");
	}

	public function set_configuration($configuration) {
		return $this->set_helper("int", $configuration, "configuration");
	}

	public function set_order_datetime(FisdapDateTime $order_datetime) {
		return $this->set_helper("datetime", $order_datetime, "order_datetime");
	}

	public function set_activation_datetime(FisdapDateTime $activation_datetime) {
		return $this->set_helper("datetime", $activation_datetime, "activation_datetime");
	}

	public function set_instructor_id($instructor_id) {
		if ($instructor_id <> -15) {
			$this->set_student_id(-15);
		}
		$ret = $this->set_helper("int", $instructor_id, "instructor_id");
		$this->sync_activationdate();

		return $ret;
	}


	// DATABASE CONSISTENCY METHODS

	/* set or unset activation date when student and instructor id's are changed (serial activated)
	 */
	protected function sync_activationdate() {
		// reset activation time
		if ($this->get_student_id() == -15 && $this->get_instructor_id() == -15) {
			$blank_date = FisdapDateTime::not_set();
			$this->set_activation_datetime($blank_date);
		} else {
			// set activation time
			if ($this->get_activation_datetime() == '0-0-0 00:00:00') {
				//				$this->set_activation_datetime(date("Y-m-d H:i:s"));
				$this->set_activation_datetime(new FisdapDateTime());
				//				echo "Activation datetime ".date("Y-m-d H:i:s")."\n";
			} else {
				echo "Time diff: $time_diff\n";
			}

		}
	}
}

?>
