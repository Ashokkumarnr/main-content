<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/

require_once("AbstractModel.inc");

class VitalsLungSounds extends AbstractModel {

	protected $vitals_id;
	protected $lung_sound;

	const dbtable = "VitalsLungSoundsData";
	const idfield = "VitalsLungSounds_id";

	public function __construct($id=null) {
		return $this->construct_helper($id, self::dbtable, self::idfield);
	}

	public function set_vitals_id($vitals_id) {
		return $this->set_helper('int', $vitals_id, 'vitals_id');
	}

	public function set_lung_sound($lung_sound) {
		return $this->set_helper('int', $lung_sound, 'lung_sound');
	}

	public function get_summary() {
		//STUB	
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "VitalsLungSounds", self::idfield);
	}

	public function get_fieldmap() {
		return array(
			'Vitals_id' => 'vitals_id',
			'LungSounds_id' => 'lung_sound',
			);
	}
}
