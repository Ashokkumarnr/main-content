<?php
/*----------------------------------------------------------------------*
  |                                                                      |
  |     Copyright (C) 1996-2006.  This is an unpublished work of         |
  |                      Headwaters Software, Inc.                       |
  |                         ALL RIGHTS RESERVED                          |
  |     This program is a trade secret of Headwaters Software, Inc.      |
  |     and it is not to be copied, distributed, reproduced, published,  |
  |     or adapted without prior authorization                           |
  |     of Headwaters Software, Inc.                                     |
  |                                                                      |
 *----------------------------------------------------------------------*/

require_once("AbstractModel.inc");
require_once("phputil/classes/DateTime.inc");
require_once("AmbulanceServiceModel.inc");
require_once("AmbulanceBaseModel.inc");
require_once("PreceptorModel.inc");

class Shift extends AbstractModel {

	protected $id;
	protected $student_id;
	protected $ambulance_service_id;
	protected $start_base_id;
	protected $start_date;
	protected $start_time;
	protected $end_time;
	protected $hours;
	protected $entry_time;
	protected $completed;
	protected $type;
	protected $event_id;
	protected $trade;
	protected $trade_status;
	protected $audited;
	protected $tardy;
	protected $first_completed;
	protected $completed_from;
	protected $pda_shift_id;

	const dbtable = "ShiftData";
	const idfield = "Shift_id";

	const IN_ATTENDANCE = 0;
	const TARDY = 1;
	const ABSENT_WITHOUT_PERMISSION = 2;
	const ABSENT_WITH_PERMISSION = 3;

	private static $valid_completed_froms = array('null', 'web', 'pda', 'mobile');
	private static $valid_types = array('clinical', 'field', 'lab');

	public function __construct($id=null) {
		$this->construct_helper($id, self::dbtable, self::idfield);
	}

	protected function set_id($id) {
		Assert::is_int($id);

		if ($id > 0) {
			return $this->set_helper("int", $id, "id");
		} else {
			return false;
		}
	}

	public function set_student_id($id) {
		Assert::is_int($id);
		return $this->set_helper("int", $id, "student_id");
	}

	public function set_ambulance_service_id($id) {
		Assert::is_int($id);
		return $this->set_helper("int", $id, "ambulance_service_id");
	}

	public function set_start_base_id($id) {
		Assert::is_int($id);
		return $this->set_helper("int", $id, "start_base_id");
	}

	public function set_start_date(FisdapDate $date) {
		return $this->set_helper("date", $date, "start_date");
	}

	public function set_start_time(FisdapTime $time) {
		$time = $time->get_military_time();
		return $this->set_helper("string", $time, "start_time");
	}

	public function set_end_time(FisdapTime $time) {
		$time = $time->get_military_time();
		return $this->set_helper("string", $time, "end_time");
	}

	public function set_hours($hours) {
		Assert::is_float($hours);
		$hours = Convert::to_float($hours);
		Assert::is_true($hours > 0);

		return $this->set_helper("float", $hours, "hours");
	}

	public function set_entry_time(FisdapDateTime $date_time) {
		return $this->set_helper("datetime", $date_time, "entry_time");
	}

	public function set_completed($c) {
		Assert::is_int($c);
		return $this->set_helper("int", $c, "completed");
	}

	public function set_type($type) {
		Assert::is_string($type);
		$type = strtolower($type);
		Assert::is_in_array($type, self::$valid_types);
		
		return $this->set_helper("string", $type, "type");
	}

	public function set_event_id($id) {
		Assert::is_int($id);
		return $this->set_helper("int", $id, "event_id");
	}

	public function set_trade($t) {
		return $this->set_helper("bool", $t, "trade");
	}

	public function set_trade_status($t) {
		Assert::is_int($t);
		Assert::is_true($t >= 0 && $t <= 3); 

		return $this->set_helper("int", $t, "trade_status");
	}

	public function set_audited($a) {
		return $this->set_helper("bool", $a, "audited");
	}

	public function set_tardy($t) {
		return $this->set_helper("bool", $t, "tardy");
	}

	public function set_first_completed(FisdapDateTime $date_time) {
		return $this->set_helper("datetime", $date_time, "first_completed");
	}

	public function set_completed_from($cf) {
		Assert::is_string($cf);
		$cf = strtolower($cf);
		Assert::is_in_array($cf, self::$valid_completed_froms);
		
		return $this->set_helper('string', $cf, "completed_from");
	}

	public function set_pda_shift_id($id) {
		Assert::is_int($id);
		return $this->set_helper("int", $id, "pda_shift_id");
	}

	public function get_attendance() {
		if ($this->completed == 2) {
			return self::ABSENT_WITHOUT_PERMISSION;
		} else if ($this->completed == 3) {
			return self::ABSENT_WITH_PERMISSION;
		} else if ($this->tardy == 1) {
			return self::TARDY;
		} else {
			return self::IN_ATTENDANCE;
		}
	}

	public function get_attendance_string() {
		switch ($this->get_attendance()) {
			case self::ABSENT_WITH_PERMISSION:
				return 'Absent w/ permission';
			case self::ABSENT_WITHOUT_PERMISSION:
				return 'Absent';
			case self::TARDY:
				return 'Tardy';
			case self::IN_ATTENDANCE:
				return 'Present';
		}
	}

	/**
	 * @todo I'm doing a bad thing here - querying the db directly.
	 * This needs to change.
	 * @return FisdapDateTime
	 */
	public function get_completion_deadline() {
		// Find when this shift started
		$startdate = $this->get_start_date();
		$starttime = $this->get_start_time();
		$start = new FisdapDateTime("$startdate $starttime");

		// Get the program-set completion period
		$type = ucfirst($this->get_type());
		if ($type == 'Field') $type = '';
		$student = new common_student($this->get_student_id());
		$program_id = $student->get_program_id();
		$select = "SELECT ${type}BigBroInc AS offset";
		$select .= " FROM ProgramData WHERE Program_id=$program_id";
		$connection =& FISDAPDatabaseConnection::get_instance();
		$result = $connection->query($select);
		$offset = $result[0]['offset'];

		// Adjust the object and return it
		$start->change_hours($offset);
		return $start;
	}

	public function get_completed_late() {
		$deadline = $this->get_completion_deadline();
		$completed = new FisdapDateTime($this->get_first_completed());
		return ($completed->compare($deadline) > 0);
	}

	public function is_student_created() {
		if ($this->event_id < 0) {
			return true;
		} else {
			return false;
		}
	}

	public function get_summary() {
		return 'Shift[' . $this->get_id() . ']';
	}

	public function get_hook_ids() {
		Assert::is_not_null($this->type);

		if ($this->type == 'field') {
			return array('113');
		} else if ($this->type == 'clinical') {
			return array('114');
		} else if ($this->type == 'lab') {
			return array('115');
		}

		return array();
	}

	public function has_data() {
		if ($this->get_type() == 'field') {
			$runs = ModelFactory::getRunsByShift($this->get_id());
			if (count($runs) > 0) {
				return true;
			}
		} else {
			$assessments = ModelFactory::getAssessmentsByShift($this->get_id());
			$bls_skills = ModelFactory::getBLSSkillsByShift($this->get_id());
			$ecgs = ModelFactory::getECGsByShift($this->get_id());
			$ivs = ModelFactory::getIVsByShift($this->get_id());
			$meds = ModelFactory::getMedsByShift($this->get_id());
			$airways = ModelFactory::getAirwaysByShift($this->get_id());
			$others = ModelFactory::getOtherALSsByShift($this->get_id());
			$narratives = ModelFactory::getNarrativesByShift($this->get_id());

			$shift_data = array_merge($assessments, $bls_skills, $ecgs, $ivs, $meds, $airways, $others, $narratives);

			if (count($shift_data) > 0) {
				return true;
			}
		}

		return false;
	}

	public function populate_child_models() {
		$this->child_model_helper('ambulance_service_id', 'ambulance_service', 'AmbulanceService', true);
		$this->child_model_helper('start_base_id', 'base', 'AmbulanceBase', true);
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "shift", self::idfield);
	}

	public function get_fieldmap() {
		return array(
				"Student_id" => "student_id",
				"AmbServ_id" => "ambulance_service_id",
				"StartBase_id" => "start_base_id",
				"StartDate" => "start_date",
				"StartTime" => "start_time",
				"EndTime" => "end_time",
				"Hours" => "hours",
				"EntryTime" => "entry_time",
				"Completed" => "completed",
				"Type" => "type",
				"Event_id" => "event_id",
				"Trade" => "trade",
				"TradeStatus" => "trade_status",
				"Audited" => "audited",
				"Tardy" => "tardy",
				"FirstCompleted" => "first_completed",
				"CompletedFrom" => "completed_from",
				"PDAShift_id" => "pda_shift_id",
				);
	}
}
?>
