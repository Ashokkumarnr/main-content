<?php 

require_once('TestItemReview.php');

/**
 * This class encapsulates test item reviews done by assignment.
 */
class AssignmentTestItemReview extends TestItemReview {
    var $assignment_id;

    function AssignmentTestItemReview() {
        parent::TestItemReview();

        $this->assignment_id = 0;
        $this->mode = 'assignment';

        $this->field_map['db']['assignment_id'] = 'assignment_id';
    }//AssignmentTestItemReview


    /**
     * Fill in the completed date for the associated assignment
     */
    function complete_related_assignment() {
        if( !is_numeric($this->assignment_id) || $this->assignment_id < 1 ) {
            // @todo  send an error report here
            die('Could not complete your assignment.');
        }//if
        
        $query = 'UPDATE ReviewAssignmentData '.
                 'SET DateReviewReceived=NOW() '.
                 'WHERE ReviewAssignment_id="'.$this->assignment_id.'"';
        $result_set = $this->connection_obj->query($query);

        if ( $result_set === false ) {
            // @todo  send an error report here
            die('There was an error completing your assignment.');
        }//if
    }//complete_related_assignment


    function set_assignment_id($assignment_id) {
        $this->assignment_id = $assignment_id;
    }//set_assignment_id


    function get_assignment_id($assignment_id) {
        return $this->assignment_id;
    }//get_assignment_id


    /**
     * For new assignment reviews, set the completion date of the 
     * assignment.
     */
    function post_insert() {
        $this->complete_related_assignment();
    }//post_insert


    ///////////////////////////////////////////////////////////////////////////
    // Module: test_item_review_db_layer.php
    //
    // This module provides the database layer functions for the test item
    // review model classes.  These are static methods, and must know the
    // name of the current class when they are invoked.  Without this
    // module, the TestItemReview subclasses would all generate
    // TestItemReview instances instead of instances of the appropriate
    // subclasses.
    ///////////////////////////////////////////////////////////////////////////
    
    
    /**
     * Get the review with the given id
     *
     * @param int $id a review id
     * @return AssignmentTestItemReview the review with the given id, if any,  
     *         or false on error.
     */
    function &find_by_id($id) {
        if ( !is_numeric($id) ) {
            die(
                'Error fetching the review.  Received an illegal ID value.  '.
                'Please try again.'
            );
        }//if
        
        $connection_obj = FISDAPDatabaseConnection::get_instance();
        $query = 'SELECT * '.
                 'FROM ItemReviews '.
                 'WHERE id="'.$id.'"';
        $result_set = $connection_obj->query($query);
        if ( !is_array($result_set) || count($result_set) != 1 ) {
            return false;
        }//if
    
        return AssignmentTestItemReview::from_assoc_array($result_set[0]);
    }//find_by_id
    
    
    /**
     * Returns an array of all the reviews for the given item
     *
     * @param int $item_id a test item id
     * @return array all reviews for the given item
     */
    function find_all_by_item($item_id) {
        $connection_obj = FISDAPDatabaseConnection::get_instance();
        $query = 'SELECT * '.
                 'FROM ItemReviews '.
                 'WHERE item_id="'.$item_id.'" '.
                 'ORDER BY created_at,id';
        $result_set = $connection_obj->query($query);
        $reviews = array();
        if ( is_array($result_set) ) {
            foreach($result_set as $row) {
                $review = AssignmentTestItemReview::from_assoc_array($row);
                $reviews[] = $review;
            }//foreach
        }//if
        return $reviews;
    }//find_all_by_item
    
    
    /** 
     * Create a new AssignmentTestItemReview from an associative array of key/value
     * pairs (e.g., from the database).  Note that the array's fields should
     * match the database column names, not the object fields.
     *
     * @param array $db_row an assoc array of input
     * @return AssignmentTestItemReview a reference to a review with the contents of 
     *                        the given input
     */
    function &from_assoc_array($db_row) {
        $DEBUG = false;
        $review = new AssignmentTestItemReview();
        foreach( $db_row as $field=>$value ) {
            $obj_field = $review->field_map['db'][$field];
            if ( $obj_field ) {
                $review->$obj_field = $value;
            } else {
                if ( $DEBUG ) {
                    die(
                        'from_assoc_array: empty object field for db field '
                        . '"' . $field . '"'
                    );
                }//if
            }//else
        }//foreach
        return $review;
    }//from_assoc_array
    
    
    ///////////////////////////////////////////////////////////////////////////
    // END MODULE
    ///////////////////////////////////////////////////////////////////////////

}//class AssignmentTestItemReview

?>
