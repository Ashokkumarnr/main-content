<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/model/AbstractHistoryModel.inc');

/**
 * This class models the req_RelationshipHistory table in the DB.
 */
final class ReqRelationshipHistoryModel extends AbstractHistoryModel {
    protected $relationship_id;

    const DB_TABLE = 'req_RelationshipHistory';
    const RELATIONSHIP_ID_FIELD = 'relationship_id';

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    /**
     * Retrieve the relationship ID.
     * @return int A reference into the req_Relationship table.
     */
    public function get_relationship_id() {
        return $this->relationship_id;
    }

    /**
     * Set the requirement relationship ID.
     * @param int $id A reference into the req_Relationship table.
     */
    public function set_relationship_id($id) {
        Assert::is_int($id);
		$this->set_helper('int', $id, 'relationship_id');
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'ReqRelationshipHistory[' . $this->get_id() . ']';
    }

    public function get_fieldmap() {
        $map = array(
            'Relationship_id' => self::RELATIONSHIP_ID_FIELD
        );

        return array_merge($map, parent::get_fieldmap());
    }

    /**
     * Retrieve all the history entries.
     * @return array A list of objects sorted on entry time ascending. 
     */
    public static function get_all_entries() {
        return parent::do_get_all_entries(__CLASS__);
    }

	/**
	 * Retrive all the history entries by ID.
	 * @param int $id The ID.
     * @return array A list of objects sorted on entry time ascending. 
	 */
	public static function get_all_by_relationship_id($id) {
		Assert::is_int($id);
		
        return parent::do_get_all_by_id(__CLASS__, self::RELATIONSHIP_ID_FIELD, $id);
	}
}
?>
