<?php

require_once('phputil/classes/model/TestItemReview.php');

/**
 *
 */
class TestItemReviewTester extends UnitTestCase {
    function setUp() {
        $this->input = array( 
            'stem_grammar_correct'=>'1',
            'distractors_match_grammar'=>'1',
            'distractors_less_correct'=>'1',
            'distractors_similar_yet_distinct'=>'1',
            'answer_always_correct'=>'1',
            'item_free_from_complication'=>'1',
            'item_unbiased'=>'1',
            'level_of_difficulty_matches_job'=>'1',
            'important_to_memorize'=>'1',
            'percent_minimally_qualified'=>'50',
            'item_id'=>'1',
            'number_in_group'=>'1',
            'reviewer'=>'test_person',
            'proposed_edit'=>'this is a proposed edit',
            'explain_rationale'=>'this is a rationale',
            'item_measures_k_a_p'=>'p',
            'mode'=>'assignment'
        );
    }//setUp

    function test_from_assoc_array() {
        // test without the id attribute
        $review =& TestItemReview::from_assoc_array($this->input);
        foreach( $this->input as $key=>$value ) {
            $review_field = $review->field_map['db'][$key];
            $this->assertEqual(
                $value,
                $review->$review_field,
                'value mismatch key "'.$key.'" ("'.$value.'" / "'.$review->$review_field.'")'
            );
        }//foreach
        $this->assertEqual(
            $review->id,
            0,
            'id incorrectly set to nonzero value ("'.$review->id.'")'
        );

        // test with the id attribute
        $this->input['id'] = '50';
        $review =& TestItemReview::from_assoc_array($this->input);
        foreach( $this->input as $key=>$value ) {
            $review_field = $review->field_map['db'][$key];
            $this->assertEqual(
                $value,
                $review->$review_field,
                'value mismatch key "'.$key.'" ("'.$value.'" / "'.$review->$review_field.'")'
            );
        }//foreach
    }//test_from_assoc_array


    function test_to_assoc_array() {
        // test without an id attribute
        $review =& TestItemReview::from_assoc_array($this->input);       
        $review_as_array = $review->to_assoc_array();
        unset($review_as_array['id']);
        unset($review_as_array['created_at']);
        unset($review_as_array['AssignmentReviewType']);
        $this->assertEqual($this->input,$review_as_array,'assoc array mismatch');

        // test with an id attribute
        $this->input['id'] = '50';
        $review =& TestItemReview::from_assoc_array($this->input);       
        $review_as_array = $review->to_assoc_array();
        unset($review_as_array['created_at']);
        unset($review_as_array['AssignmentReviewType']);
        $this->assertEqual($this->input,$review_as_array,'assoc array mismatch (with id element)');

        // test with created_at attribute
        $this->input['created_at'] = '2006-02-01';
        $review =& TestItemReview::from_assoc_array($this->input);       
        $review_as_array = $review->to_assoc_array();
        unset($review_as_array['AssignmentReviewType']);
        $this->assertEqual($this->input,$review_as_array,'assoc array mismatch (with created_at element)');
    }//test_to_assoc_array


    function test_validate() {
        // this should validate
        $review =& TestItemReview::from_assoc_array($this->input);
        $this->assertTrue($review->validate(),'valid input failed validation');

        // these should not validate
        foreach( $this->input as $key=>$value ) { 
            unset($this->input[$key]);
            $invalid_review =& TestItemReview::from_assoc_array($this->input);
            $this->assertFalse(
                $invalid_review->validate(),
                'invalid input validated (after removing field "'.$key.'")'
            );
        }//foreach
    }//test_validate
}//class TestItemReviewTester

?>
