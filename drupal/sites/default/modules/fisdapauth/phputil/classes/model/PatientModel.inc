<?php

/*----------------------------------------------------------------------*
  |                                                                      |
  |     Copyright (C) 1996-2006.  This is an unpublished work of         |
  |                      Headwaters Software, Inc.                       |
  |                         ALL RIGHTS RESERVED                          |
  |     This program is a trade secret of Headwaters Software, Inc.      |
  |     and it is not to be copied, distributed, reproduced, published,  |
  |     or adapted without prior authorization                           |
  |     of Headwaters Software, Inc.                                     |
  |                                                                      |
 *----------------------------------------------------------------------*/


require_once("Model.inc");
require_once("BLSSkillModel.inc");

class patient extends AbstractModel {

	protected $id;
	protected $shift_id;
	protected $student_id;
	protected $age;
	protected $gender;
	protected $ethnicity;
	protected $diagnosis;
	protected $entry_time;
	protected $loc;
	protected $systolicbp;
	protected $diastolicbp;
	protected $syncopal;
	protected $diag2;
	protected $moi;
	protected $months;
	protected $subject_type;

	const dbtable = "AssesmentData";
	const id_field = "Asses_id";

	public function __construct($id=null) {
		$this->set_helper('int', -1, 'student_id');
		$this->set_helper('int', -1, 'shift_id');
		return $this->construct_helper($id, self::dbtable, self::id_field);
	}

	public function set_shift_id($_shift) {
		return $this->set_helper("int", $_shift, "shift_id");
	}

	public function set_student_id($_student) {
		return $this->set_helper("int", $_student, "student_id");
	}	

	public function set_subject_type($_type) {
		return $this->set_helper("int", $this->id_or_string_helper($_type,"get_subject_types"),"subject_type");
	}

	public function set_age($_age) {
		if (is_numeric($_age)) {
			return $this->set_helper("int", $_age, "age");
		} else {
			return false;
		}
	}

	public function set_gender($_gender) {
		if ($_gender > 0 || is_string($_gender)) {
			$_gender = $this->id_or_string_helper($_gender,"get_gender_names");
		}	
		return $this->set_helper("int",$_gender,"gender");
	}

	public function set_ethnicity($_ethnicity) {	
		if ($_ethnicity > 0 || is_string($_ethnicity)) {
			$_ethnicity = $this->id_or_string_helper($_ethnicity,"get_ethnicity_names");
		} 
		return $this->set_helper("int",$_ethnicity,"ethnicity");
	}

	public function set_diagnosis($_diag) {	
		if ($_diag > 0 || is_string($_diag)) {
			$_diag = $this->id_or_string_helper($_diag,"get_diag_names");
		}
		return $this->set_helper("int",$_diag,"diagnosis");
	}

	public function set_loc($_loc) {
		if ($_loc > 0 || is_string($_loc)) {
			$_loc = $this->id_or_string_helper($_loc,"get_loc_names");
		}
		return $this->set_helper("int",$_loc,"loc");
	}

	public function set_systolicbp($_bp) {
		if (is_numeric($_bp)) {
			return $this->set_helper("int",$_bp,"systolicbp");
		} else {
			return false;
		}
	}

	public function set_diastolicbp($_bp) {
		if (strtoupper($_bp) == "P") {
			return $this->set_helper("int", -2, "diastolicbp");
		} else if (is_numeric($_bp)) {
			return $this->set_helper("int", $_bp, "diastolicbp");
		}
	}

	public function set_diag2($_diag) {
		if ($_diag > 0 || is_string($_diag)) {
			$_diag = $this->id_or_string_helper($_diag,"get_diag_names");
		} 
		return $this->set_helper("int",$_diag,"diag2");
	}

	public function set_moi($_moi) {
		if ($_moi > 0 || is_string($_moi)) {	
			$_moi = $this->id_or_string_helper($_moi,"get_diag_names");
		}
		return $this->set_helper("int", $_moi, "moi");
	}

	public function set_months($_months) {
		if ($_months == null) {
			return $this->set_helper("int", 0, "months");
		} else if (is_numeric($_months)) {
			return $this->set_helper("int", $_months, "months");
		} else {
			return false;
		}
	}

	public function get_systolicbp() {
		if ($this->systolicbp < 0) {
			return null;
		} else {
			return $this->systolicbp;
		}
	}

	public function get_diastolicbp() {
		if ($this->diastolicbp == -2) {
			return "P";
		} else if ($this->diastolicbp == -1) {
			return null;
		} else {
			return $this->diastolicbp;
		}
	}

	/*
	public function set_exam($_exam) {
		$this_exam = $this->get_exam('obj');
		if (is_object($this_exam)) {
			if ($_exam <= 0) {
				$this_exam->remove();
			} else {
				$this_exam->set_performed_by($_exam);
			}
		} else {
			$exam = new BLSSkill(null);
			$exam->set_performed_by($_exam);
			$exam->set_student_id($this->student_id);
			$exam->set_shift_id($this->shift_id);
			$exam->set_run_id($this->run_id);
		}
	}

	public function set_interview($_interview) {

	}
	 */
	
	public static function get_gender_names() {
		$names = parent::data_table_helper("GenderTable","Gender_id","GenderTitle");
		$names[-1] = 'Unset';
		return $names;
	}

	public static function get_ethnicity_names() {
		$names = parent::data_table_helper("EthnicityTable","Ethnic_id","EthnicTitle");
		$names[-1] = 'Unset';
		return $names;
	}

	public static function get_diag_names() {
		$names = parent::data_table_helper("DiagnosisTable","Diag_id","DiagTitle");
		$names[-1] = 'Unset';
		return $names;
	}

	public static function get_loc_names() {
		$names = parent::data_table_helper("LOCTable","LOC_id","LOCName");
		$names[-1] = 'Unset';
		return $names;
	}

        public static function get_subject_types() {
		if (!$_GLOBAL["subject_types_cache"]) {
			$_GLOBAL["subject_types_cache"] = self::data_table_helper("SubjectTypeTable", "SubjectType_id", "SubjectTypeName", "SubjectType_id");
		}

		return $_GLOBAL["subject_types_cache"];
	}

        public static function get_subject_type_group($id) {
                $query = "SELECT * FROM SubjectTypeTable WHERE SubjectType_id = $id";
                $connection = FISDAPDatabaseConnection::get_instance();
                $result = $connection->query($query);

                return $result[0]['SubjectTypeGroup'];
        }
	
	public function get_exam($return = 'str') {
		$blsskills = ModelFactory::getBLSSkillsByAsses($this->get_id());
		foreach ($blsskills as $skill) {

			if ($skill->get_skill() == 4) {
				if ($return == 'obj') {
					$exam = $skill;
				} else {
					$exam = $skill->get_performed_by_id();
				}
				return $exam;
			} else {
				$exam = -1;
			}
		}
		return -1;
	}	


	public function get_interview($return = 'str') {
		$blsskills = ModelFactory::getBLSSkillsByAsses($this->get_id());
		foreach ($blsskills as $skill) {
			if ($skill->get_skill() == 3) {
				if ($return == 'obj') {
					$interview = $skill;
				} else {
					$interview = $skill->get_performed_by_id();
				}
				return $interview;
			} else {
				$interview = -1;
			}
		}
		return -1;
	}	

	public function get_summary() {
		$g = $this->get_gender_names();
		$d = $this->get_diag_names();

		return $this->age." yo ".$g[$this->gender]." - ".$d[$this->diagnosis];
	}

	public function get_mobile_summary() {
		$g = self::get_gender_names();
		$d = self::get_diag_names();
		$l = self::get_loc_names();
		$summary = "<div style='font-weight:bold;'>";

		$summary .= $this->age." yo ".$g[$this->gender]." - ".$d[$this->diagnosis];
		if ($this->diag2 > 0) {
			$summary .= ", ".$d[$this->diag2];
		}
		$summary .= "</div>";

		$complaints = ModelFactory::getComplaintsByAsses($this->get_id());

		if (count($complaints) > 0) {
			$summary .= "<div>";
			foreach ($complaints as $complaint) {
				$summary .= $complaint->get_summary().", ";
			}
			$summary = substr($summary, 0, -2);
			$summary .= "</div>";
		}

		$other_info = array();
		if ($this->moi > 0) {
			$other_info[] = $d[$this->moi];
		}
		if ($this->loc > 0) {
			$other_info[] = $l[$this->loc];
		}

		if (count($other_info) > 0) {
			$summary .= "<div>".implode(', ', $other_info)."</div>";
		}

		$other_info = array();
		if ($this->get_interview() == 1) {
			$other_info[] = 'Interview';
		} 
		if ($this->get_exam() == 1) {
			$other_info[] = 'Exam';
		}

		if (count($other_info) > 0) {
			$summary .= "<div>Performed: ".implode(', ', $other_info)."</div>";
		}


		return $summary;	
	}

	public function get_hook_ids() {
		//Clinical
		$clin_array = array(19, 40, 41, 42, 83, 84, 85, 86, 87, 88, 89, 90);

		//Lab
		$lab_array = array(60, 61, 62, 63, 91, 92, 93, 94, 95, 96, 97, 98);

		if ($this->shift_id > 0) {
			$shift = new Shift($this->shift_id);
			$shift_type = $shift->get_type();
		}

		if ($shift_type == 'clinical') {
			return $clin_array;
		} else if ($shift_type == 'lab') {
			return $lab_array;
		} else {
			return array();
		}	
	}	


	public static function getBySQL($result) {
		return self::sql_helper($result, "patient", self::id_field);
	}

	
	protected function populate_child_models() {
		//$this->child_model_helper('gender_id', 'gender', 'Gender', true);
	}
	

	/**
	 * @todo I have no idea if we need all this stuff
	 */
	public function get_fieldmap() {
		return array(	'Shift_id' => 'shift_id',
				'Student_id' => 'student_id',
				'Age' => 'age',
				'Gender' => 'gender',
				'Ethnicity' => 'ethnicity',
				'Diagnosis' => 'diagnosis',
				'EntryTime' => 'entry_time',
				'LOC' => 'loc',
				'SystolicBP' => 'systolicbp',
				'DiastolicBP' => 'diastolicbp',
				'Syncopal' => 'syncopal',
				'Diag2' => 'diag2',
				'MOI' => 'moi',
				'Months' => 'months',
				'SubjectType_id' => 'subject_type'
				);
	}

}

?>
