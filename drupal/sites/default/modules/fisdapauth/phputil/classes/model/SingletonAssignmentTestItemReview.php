<?php 

require_once('TestItemReview.php');
require_once('AssignmentTestItemReview.php');

/**
 * This class encapsulates assignment test item reviews for projects
 * that require a less stringent review process than the standard 
 * review classes provide.
 */
class SingletonAssignmentTestItemReview extends AssignmentTestItemReview {

    function SingletonAssignmentTestItemReview() {
        parent::AssignmentTestItemReview();

        $this->validation_set =& new SingletonTestItemReviewValidationSet();
        $this->required_items_for_validation = array(
            'stem_grammar_correct', 'distractors_match_grammar',
            'distractors_less_correct', 'distractors_similar_yet_distinct',
            'answer_always_correct', 'item_free_from_complication',
            'item_unbiased' 
        );
    }//SingletonAssignmentTestItemReview


    ///////////////////////////////////////////////////////////////////////////
    // Module: test_item_review_db_layer.php
    //
    // This module provides the database layer functions for the test item
    // review model classes.  These are static methods, and must know the
    // name of the current class when they are invoked.  Without this
    // module, the TestItemReview subclasses would all generate
    // TestItemReview instances instead of instances of the appropriate
    // subclasses.
    ///////////////////////////////////////////////////////////////////////////
    
    
    /**
     * Get the review with the given id
     *
     * @param int $id a review id
     * @return SingletonAssignmentTestItemReview the review with the given id, if any,  
     *         or false on error.
     */
    function &find_by_id($id) {
        if ( !is_numeric($id) ) {
            die(
                'Error fetching the review.  Received an illegal ID value.  '.
                'Please try again.'
            );
        }//if
        
        $connection_obj = FISDAPDatabaseConnection::get_instance();
        $query = 'SELECT * '.
                 'FROM ItemReviews '.
                 'WHERE id="'.$id.'"';
        $result_set = $connection_obj->query($query);
        if ( !is_array($result_set) || count($result_set) != 1 ) {
            return false;
        }//if
    
        return SingletonAssignmentTestItemReview::from_assoc_array($result_set[0]);
    }//find_by_id
    
    
    /**
     * Returns an array of all the reviews for the given item
     *
     * @param int $item_id a test item id
     * @return array all reviews for the given item
     */
    function find_all_by_item($item_id) {
        $connection_obj = FISDAPDatabaseConnection::get_instance();
        $query = 'SELECT * '.
                 'FROM ItemReviews '.
                 'WHERE item_id="'.$item_id.'" '.
                 'ORDER BY created_at,id';
        $result_set = $connection_obj->query($query);
        $reviews = array();
        if ( is_array($result_set) ) {
            foreach($result_set as $row) {
                $review = SingletonAssignmentTestItemReview::from_assoc_array($row);
                $reviews[] = $review;
            }//foreach
        }//if
        return $reviews;
    }//find_all_by_item
    
    
    /** 
     * Create a new SingletonAssignmentTestItemReview from an associative array of key/value
     * pairs (e.g., from the database).  Note that the array's fields should
     * match the database column names, not the object fields.
     *
     * @param array $db_row an assoc array of input
     * @return SingletonAssignmentTestItemReview a reference to a review with the contents of 
     *                        the given input
     */
    function &from_assoc_array($db_row) {
        $DEBUG = false;
        $review = new SingletonAssignmentTestItemReview();
        foreach( $db_row as $field=>$value ) {
            $obj_field = $review->field_map['db'][$field];
            if ( $obj_field ) {
                $review->$obj_field = $value;
            } else {
                if ( $DEBUG ) {
                    die(
                        'from_assoc_array: empty object field for db field '
                        . '"' . $field . '"'
                    );
                }//if
            }//else
        }//foreach
        return $review;
    }//from_assoc_array
    
    
    ///////////////////////////////////////////////////////////////////////////
    // END MODULE
    ///////////////////////////////////////////////////////////////////////////

}//class SingletonAssignmentTestItemReview

?>
