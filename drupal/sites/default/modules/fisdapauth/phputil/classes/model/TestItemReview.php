<?php

require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once(
    'phputil/classes/validation/TestItemReviewInputValidationSet.php'
);
require_once('phputil/classes/model/TestItem.php');
require_once('phputil/classes/logger/Logger.php');

// Define the db column for cut score
define('CUT_SCORE_CERT', 'CS_AccountType_id');


/**
 * This class encapsulates reviews of test items
 */
class TestItemReview {

    ////////////////////////////////////////////////////////////
    // Private instance variables
    ////////////////////////////////////////////////////////////
    
    var $id;
    var $created_at;
    var $has_errors;
    var $validation_set;
    var $logger;
    var $connection_obj;
    var $expected_inputs;
    var $expected_ratios;
    var $required_items_for_validation;
    var $field_map;
    var $stem_grammar_correct;
    var $distractors_match_grammar;
    var $distractors_less_correct;
    var $distractors_similar_yet_distinct;
    var $answer_always_correct;
    var $item_free_from_complication;
    var $item_unbiased;
    var $level_of_difficulty_matches_job;
    var $important_to_memorize;
    var $percent_minimally_qualified;
    var $item_id;
    var $number_in_group;
    var $reviewer;
    var $proposed_edit;
    var $explain_rationale;
    var $item_measures_k_a_p;
    var $mode;
    var $assignment_review_type;
    
    
    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////
    
    /**
     * Create an empty test item review
     */
    function TestItemReview() {
        $this->validation_set =& new TestItemReviewInputValidationSet();
        $this->connection_obj =& FISDAPDatabaseConnection::get_instance();
        $this->logger =& Logger::get_instance();
        $this->id = 0;

        // these are the inputs allowed to be passed into the database.  any 
        // extraneous inputs will be ignored
        // Note: the 'id' input is special... it will be used to differentiate
        //       between INSERT and UPDATE queries, and should not appear in the
        //       list of expected inputs
        $this->expected_inputs = array(
            'stem_grammar_correct', 'distractors_match_grammar' , 
            'distractors_less_correct','distractors_similar_yet_distinct', 
            'answer_always_correct', 'item_free_from_complication' ,
            'item_unbiased' , 'level_of_difficulty_matches_job' , 
            'important_to_memorize','percent_minimally_qualified','item_id',
            'number_in_group','reviewer','proposed_edit','explain_rationale',
            'item_measures_k_a_p','mode','AssignmentReviewType', CUT_SCORE_CERT
        );

        // these form elements must all be numeric values >= 1 for the review 
        // to pass
        $this->required_items_for_validation = array(
            'stem_grammar_correct', 'distractors_match_grammar' , 
            'distractors_less_correct', 'distractors_similar_yet_distinct', 
            'answer_always_correct', 'item_free_from_complication',
            'item_unbiased' , 'level_of_difficulty_matches_job' , 
            'important_to_memorize' 
        );

        // check for the proper ratios of positive item responses
        $this->expected_ratios = array(
            'stem_grammar_correct'=>66,
            'distractors_match_grammar'=>66,
            'distractors_less_correct'=>66,
            'distractors_similar_yet_distinct'=>66,
            'item_free_from_complication'=>66,
            'answer_always_correct'=>87,
            'item_unbiased'=>87,
            'level_of_difficulty_matches_job'=>66,
            'important_to_memorize'=>66
        );

        $this->field_map = array(
            'db'=>array(
                'stem_grammar_correct'=>'stem_grammar_correct',
                'distractors_match_grammar'=>'distractors_match_grammar', 
                'distractors_less_correct'=>'distractors_less_correct',
                'distractors_similar_yet_distinct'=>'distractors_similar_yet_distinct', 
                'answer_always_correct'=>'answer_always_correct',
                'item_free_from_complication'=>'item_free_from_complication',
                'item_unbiased'=>'item_unbiased',
                'level_of_difficulty_matches_job'=>'level_of_difficulty_matches_job' , 
                'important_to_memorize'=>'important_to_memorize',
                'percent_minimally_qualified'=>'percent_minimally_qualified',
                'item_id'=>'item_id',
                'number_in_group'=>'number_in_group',
                'reviewer'=>'reviewer',
                'proposed_edit'=>'proposed_edit',
                'explain_rationale'=>'explain_rationale',
                'item_measures_k_a_p'=>'item_measures_k_a_p',
                'mode'=>'mode',
                'id'=>'id',
                'created_at'=>'created_at',
                CUT_SCORE_CERT=>'cut_score_cert_level',
                'AssignmentReviewType'=>'assignment_review_type'
            )
        );
    }//TestItemReview


    ////////////////////////////////////////////////////////////
    // Public instance methods
    ////////////////////////////////////////////////////////////

    /**
     * Validate the input, returning true if successful, false on error. 
     *
     * @return bool a success value
     */
    function validate() {
        $this->validation_set->validate($this->to_assoc_array());
        return !($this->validation_set->has_errors());
    }//validate


    /**
     * Adds a comment to the comments table
     */
    function addAssetComment() {
        $usel = "SELECT idx "
            . "FROM UserAuthData "
            . "WHERE email='" . $this->reviewer . "'";
        $ures = $this->connection_obj->query($usel);
        $user_id = $ures[0]["idx"];    

        $asel = "SELECT AssetDef_id "
            . "FROM Asset_def "
            . "WHERE DataType_id=17 " // is there a const for '17'?  ~sm
            . "AND   Data_id=" . $this->item_id;
        $ares = $this->connection_obj->query($asel);
        $asset = $ares[0]["AssetDef_id"];
        
        $insert = "INSERT INTO AssetComments "
            . "SET UserAuth_id=$user_id, "
            .     "AssetDef_id=$asset, "
            .     "CommentText='" 
            .         addslashes($this->explain_rationale) . "', "
            .     "CommentTime=NOW()";
        return $this->connection_obj->query($insert);
    }//addAssetComment
    
    
    /**
     * Generate an INSERT/UPDATE query for the review
     *
     * @return string an SQL query to save the review
     */
    function get_save_query() {
        if ( $this->id ) {
            $query = 'UPDATE ';
        } else {
            $query = 'INSERT INTO ';
        }//else

        $query .= 'ItemReviews SET ';
        foreach( $this->expected_inputs as $key ) {
            $my_field = $this->field_map['db'][$key];
            if ( !isset($this->$my_field) ) {
                continue;
            }//if
            $query .= $key.'="'.$this->$my_field.'", ';
        }//foreach

        // chop the extra two trailing characters (', ')
        $query = substr($query,0,strlen($query)-2);
        if ( $this->id ) {
            // on updates, only change the review with the given id
            $query .= ' WHERE id="'.$this->id.'"';
        } else {
            // on inserts, set the creation time
            $query .= ', created_at=NOW()';
        }//else

        return $query;
    }//get_save_query

    
    /**
     * Save the review to the database
     *
     * @return int the item id, if successful.  false otherwise
     */
    function save() {
        // make sure the input is valid
        if( !$this->validate() ) {
            return false;
        }//if

        // make sure there's actually an item with the given id
        $reviewed_item =& TestItem::find_by_id($this->item_id);
        if ( !$reviewed_item ) {
            $this->validation_set->add_error(
                new ValidationError(
                    'Couldn\'t find the test item in the database'
                )
            );
            return false;
        }//if

        // are we adding a new review, or updating an existing one? 
        $is_new_review = ($this->id) ? false : true;

        // build the insert/update query
        $query = $this->get_save_query();
        if ( !$query ) {
            return false;
        }//if

        // run the query
        $result = $this->connection_obj->query($query);

        // get the review id on new submisions
        if ( $is_new_review ) {
            $this->id = mysql_insert_id($this->connection_obj->get_link_resource());
        }//if

        if ( $is_new_review ) {
            $this->post_insert();
        }//if
        
        if($is_new_review) {
            //add the rational for giving a bad review to the comments for this asset
            if($this->explain_rationale != '') {
                $this->addAssetComment();
            }//if
        }//if

        // update the item's validity from its reviews.  if the validity
        // was changed to anything other than "undetermined," delete any
        // incomplete assignments for the item.
        $reviewed_item->update_review_status();
        if ( $reviewed_item->get_validity() != UNDETERMINED_ITEM_VALIDITY ) {
            TestItemReviewAssignment::delete_unnecessary_assignments_for_item(
                $reviewed_item->get_id()
            );
        }//if

        // send an email notification to louise on unfavorable reviews
        if ( !$this->is_favorable() ) {
            mail(
                'lbriguglio@fisdap.net,sdesombre@fisdap.net,mbowen@fisdap.net,erikh@fisdap.net',
                '*FISDAP* *Testing* Unfavorable review (#'.$this->item_id.','.$_SERVER['PHP_AUTH_USER'].')',
                'An unfavorable review was entered by '.$_SERVER['PHP_AUTH_USER'].
                ' for item #'.$this->item_id.', in '.$this->mode.' mode. '."\n".
                'You may view the review at: '."\n".
                'http://www.fisdap.net/shift/evals/item_review.php?action=edit&review_id='.$this->id.'&item_id='.$this->item_id
            );
        }//if

        return $this->id;
    }//save

  
    /**
     * This function is called immediately after a review is inserted.
     * Note that this will not be called if the review was simply 
     * updated.  By default, this function is a no-op.
     */
    function post_insert() {
    }//post_insert


    /**
     * Delete this review and update the associated test item's status
     * from its remaining reviews.
     */
    function delete() {
        if ( $this->id ) {
            $this->logger->log(
                'TestItemReview#'.$this->id.': deleted by '.$_SERVER['PHP_AUTH_USER'],
                $this->logger->INFO
            );

            $query = 'DELETE FROM ItemReviews WHERE id="'.$this->id.'" LIMIT 1';
            $this->connection_obj->query($query);
            
            $linked_test_item =& TestItem::find_by_id($this->item_id);
            if ( $linked_test_item ) {
                // when deleting, we assume the user (probably FISDAP staff) doesn't need 
                // an email every time the set of reviews is made inconsistent
                $email_warnings = false;

                $new_status = $linked_test_item->update_review_status($email_warnings);

                $this->logger->log(
                    'TestItemReview#'.$this->id.': updated item #'.$this->item_id.
                    ' status to "'.$new_status.'"',
                    $this->logger->DEBUG
                );
            } else {
                $this->logger->log(
                    'TestItemReview#'.$this->id.': '.$_SERVER['PHP_AUTH_USER'].
                    ' deleted review for item that doesn\'t exist (#'.$this->item_id.')',
                    $this->logger->WARN
                );
            }//else
        }//if
    }//delete

    
    /**
     * Returns true iff the review, were it a consensus, would validate the 
     * associated test item.
     *
     * @return bool true iff the review would validate the item were it a 
     *              consensus, false otherwise
     */
    function is_favorable() {
        $failed_validation = false;
        
        foreach( $this->required_items_for_validation as $item ) {
            if ( $this->$item < 1 ) {
                $failed_validation = true;
            }//if
        }//foreach
        return !$failed_validation;
    }//is_favorable


    /**
     * Return true iff the validation set has errors, false otherwise.
     *
     * @return bool true iff there were validation errors
     */
    function has_errors() {
        return $this->validation_set->has_errors();
    }//has_errors
    
    
    /**
     * Return any errors in the validation set
     * 
     * @return array see ValidationSet->get_errors
     */
    function get_errors() {
        return $this->validation_set->get_errors();
    }//get_errors


    /**
     * Returns a count of the reviews for the given item
     *
     * @param int $item_id a test item id
     * @return int the number of reviews for the given item, or
     *             false if an error occurred
     */
    function get_count_by_item($item_id) {
        $connection_obj =& FISDAPDatabaseConnection::get_instance();
        $query = 'SELECT COUNT(*) as count '.
                 'FROM ItemReviews '.
                 'WHERE item_id="'.$item_id.'"';
        $result_set = $connection_obj->query($query);
        if ( !is_array($result_set) ) {
            return false;
        }//if

        return $result_set[0]['count'];
    }//get_count_by_item


    /**
     * Returns an assoc array containing all the expected inputs
     * populated from this object, plus the id and created_at attributes.
     *
     * @todo carefully specify the correct behavior of this method, especially
     *       wrt the id and created_at attributes
     * @return array an input/db array corresponding to this object's attributes
     */
    function to_assoc_array() {
        $output = array();
        foreach( $this->expected_inputs as $input ) {
            $corresponding_field = $this->field_map['db'][$input];
            $output[$input] = $this->$corresponding_field;
        }//foreach
        
        // set the id attribute
        $output['id'] = $this->id;

        // set the created_at attribute
        $output['created_at'] = $this->created_at;

        return $output;
    }//to_assoc_array


    /**
     * Returns a new TestItemReview, by reference, that is an exact copy of
     * this review.
     *
     * @todo rewrite this method when we move to php5
     * @return TestItemReview an exact copy of this review (by reference)
     */
    function &php4_compat_clone() {
        if ( version_compare(phpversion(), '5.0') < 0 ) {
            // php4 assigns objects via copying 'em
            $copy = $this;
            return $copy;
        } else {
            // php5 assigns ojects via reference, but has a built-in clone
            // procedure
            return @clone($this);
        }//else
    }//php4_compat_clone



    ///////////////////////////////////////////////////////////////////////////
    // Mutators / Observers
    ///////////////////////////////////////////////////////////////////////////


    function get_expected_ratios() {
        return $this->expected_ratios;   
    }//get_expected_ratios


    function get_id() {
        return $this->id;
    }//get_id


    function get_item_id() {
        return $this->item_id;
    }//get_item_id


    function get_number_in_group() {
        return $this->number_in_group;
    }//get_number_in_group


    function get_answer_always_correct() {
        return $this->answer_always_correct;
    }//get_answer_always_correct


    function get_item_unbiased() {
        return $this->item_unbiased;
    }//get_item_unbiased


    function get_reviewer() {
        return $this->reviewer;
    }//get_reviewer


    function get_mode() {
        return $this->mode;
    }//get_mode


    function get_created_at() {
        return $this->created_at;
    }//get_created_at


    function get_assignment_review_type() {
        return $this->assignment_review_type;
    }//get_assignment_review_type


    ///////////////////////////////////////////////////////////////////////////
    // Module: test_item_review_db_layer.php
    //
    // This module provides the database layer functions for the test item
    // review model classes.  These are static methods, and must know the
    // name of the current class when they are invoked.  Without this
    // module, the TestItemReview subclasses would all generate
    // TestItemReview instances instead of instances of the appropriate
    // subclasses.
    ///////////////////////////////////////////////////////////////////////////
    
    
    /**
     * Get the review with the given id
     *
     * @param int $id a review id
     * @return TestItemReview the review with the given id, if any,  
     *         or false on error.
     */
    function &find_by_id($id) {
        if ( !is_numeric($id) ) {
            die(
                'Error fetching the review.  Received an illegal ID value.  '.
                'Please try again.'
            );
        }//if
        
        $connection_obj = FISDAPDatabaseConnection::get_instance();
        $query = 'SELECT * '.
                 'FROM ItemReviews '.
                 'WHERE id="'.$id.'"';
        $result_set = $connection_obj->query($query);
        if ( !is_array($result_set) || count($result_set) != 1 ) {
            return false;
        }//if
    
        return TestItemReview::from_assoc_array($result_set[0]);
    }//find_by_id
    
    
    /**
     * Returns an array of all the reviews for the given item
     *
     * @param int $item_id a test item id
     * @return array all reviews for the given item
     */
    function find_all_by_item($item_id) {
        $connection_obj = FISDAPDatabaseConnection::get_instance();
        $query = 'SELECT * '.
                 'FROM ItemReviews '.
                 'WHERE item_id="'.$item_id.'" '.
                 'ORDER BY created_at,id';
        $result_set = $connection_obj->query($query);
        $reviews = array();
        if ( is_array($result_set) ) {
            foreach($result_set as $row) {
                $review = TestItemReview::from_assoc_array($row);
                $reviews[] = $review;
            }//foreach
        }//if
        return $reviews;
    }//find_all_by_item
    
    
    /** 
     * Create a new TestItemReview from an associative array of key/value
     * pairs (e.g., from the database).  Note that the array's fields should
     * match the database column names, not the object fields.
     *
     * @param array $db_row an assoc array of input
     * @return TestItemReview a reference to a review with the contents of 
     *                        the given input
     */
    function &from_assoc_array($db_row) {
        $DEBUG = false;
        $review = new TestItemReview();
        foreach( $db_row as $field=>$value ) {
            $obj_field = $review->field_map['db'][$field];
            if ( $obj_field ) {
                $review->$obj_field = $value;
            } else {
                if ( $DEBUG ) {
                    die(
                        'from_assoc_array: empty object field for db field '
                        . '"' . $field . '"'
                    );
                }//if
            }//else
        }//foreach
        return $review;
    }//from_assoc_array
    
    
    ///////////////////////////////////////////////////////////////////////////
    // END MODULE
    ///////////////////////////////////////////////////////////////////////////

}//class TestItemReview
?>
