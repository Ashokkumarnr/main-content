<?php

require_once('phputil/classes/model/ConsensusTestItemReview.php');
require_once('phputil/classes/html_builder/form_builder/ConsensusTestItemReviewForm.php');

class ConsensusTestItemReviewFactory {
    function &create_review($assoc_array) {
        if ( $assoc_array['mode'] == 'consensus' ) {
            return ConsensusTestItemReview::from_assoc_array($assoc_array);
        }//if

        return false;
    }//create_review

    function &create_form($item,$mode) {
        if ( $mode == 'consensus' ) {
            return new ConsensusTestItemReviewForm($item->get_id());
        }//if

        return false;
    }//create_form
}//class ConsensusTestItemReviewFactory

?>
