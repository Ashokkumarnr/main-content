<?php

require_once('phputil/classes/FISDAPDatabaseConnection.php');


class FISDAPUser {
    var $first_name;
    var $last_name;
    var $account_type;
    var $username;
    var $program_id;

    function FISDAPUser() {
    }//FISDAPUser

    function fetch_user_program_id($username, $account_type) {
        $connection =& FISDAPDatabaseConnection::get_instance();
        if ( $account_type == 'instructor' ) {
            $query = 'SELECT ProgramId as program_id '
                   . 'FROM InstructorData '
                   . 'WHERE UserName="' . $username . '"';
        } else {
            $query = 'SELECT Program_id as program_id '
                   . 'FROM StudentData '
                   . 'WHERE UserName="' . $username . '"';
        }//else

        $result = $connection->query($query);
        if ( !is_array($result) 
             || count($result) != 1 ) {
            return false;
        }//if

        return $result[0]['program_id'];
    }//fetch_user_program_id


    function &find_by_username($username) {
        $connection =& FISDAPDatabaseConnection::get_instance();
        $query = 'SELECT * FROM UserAuthData WHERE email="'.$username.'"';
        $result_set = $connection->query($query);
        if ( !is_array($result_set) || count($result_set) != 1 ) {
            return false;
        }//if

        $user =& new FISDAPUser();
        $user->username = $username;
        $user->first_name = $result_set[0]['firstname'];
        $user->last_name = $result_set[0]['lastname'];
        $user->account_type = ($result_set[0]['instructor']) ? 
            'instructor' : 
            'student';

        $program_id = FISDAPUser::fetch_user_program_id(
            $username,
            $user->account_type
        );
        if ( !$program_id ) {
            return false;
        }//if

        $user->program_id = $program_id;

        return $user;
    }//find_by_username


    function set_username($username) {
        $this->username = $username;
    }//set_username


    function get_program_id() {
        return $this->program_id;
    }//get_program_id


    function get_username() {
        return strtolower($this->username);
    }//get_username


    function get_first_name() {
        return $this->first_name;
    }//get_first_name


    function get_last_name() {
        return $this->last_name;
    }//get_last_name


    function get_account_type() {
        return $this->account_type;
    }//get_account_type

}//class FISDAPUser
?>
