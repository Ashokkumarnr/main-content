<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/classes/model/AbstractModel.inc');
require_once('phputil/classes/model/SiteProxyTypeModel.inc');

/**
 * This class models the SiteProxies table.
 */
final class SiteProxiesModel extends AbstractModel {
    const DB_TABLE = 'SiteProxies';
    const ID_COLUMN = 'id';

    protected $ambulance_service_id;
    protected $deleted;
    protected $program_id;
    protected $proxy_type;

    const AMBULANCE_SERVICE_ID_FIELD = 'ambulance_service_id';
    const DELETED_FIELD = 'deleted';
    const PROGRAM_ID_FIELD = 'program_id';
    const PROXY_TYPE_FIELD = 'proxy_type';

	/**
	 * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
	 */
	public function __construct($data=null) {
        Assert::is_true(is_array($data) || Test::is_null($data) || Test::is_int($data));

        $this->construct_helper($data, self::DB_TABLE, self::ID_COLUMN);
    }

	/**
	 * Retrieve the ambulance service ID.
	 * @return int | null A reference to the AmbulanceServices table.
	 */
	public function get_ambulance_service_id() {
        return $this->ambulance_service_id;
    }

	/**
	 * Set the ambulance service ID.
	 * @param int | null $id A reference to the AmbulanceServices table.
	 */
	public function set_ambulance_service_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'ambulance_service_id');
    }

	/**
	 * Determine if the user requirement has been deleted.
	 * @return boolean | null TRUE if deleted.
	 */
	public function is_deleted() {
        return ($this->deleted != 0);
    }

	/**
	 * Determine if the user requirement has been deleted.
	 * @return int | null The deleted, 0 for not, otherwise deleted.
	 */
	public function get_deleted() {
        return $this->deleted;
    }

	/**
	 * Indicate whether the user requirement is deleted.
	 * @param boolean $deleted TRUE if deleted.
	 */
	public function set_deleted($deleted) {
        Assert::is_boolean($deleted);

        $this->set_helper('bool', $deleted, 'deleted');
    }

	/**
	 * Retrieve the program ID.
	 * @return int | null A reference to the ProgramData table.
	 */
	public function get_program_id() {
        return $this->program_id;
    }

	/**
	 * Set the program ID.
	 * @param int $id A reference to the ProgramData table.
	 */
	public function set_program_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'program_id');
    }

	/**
	 * Retrieve the proxy type ID.
	 * @return int | null A reference to the SiteProxyType table.
	 */
	public function get_proxy_type() {
        return $this->proxy_type;
    }

	/**
	 * Set the proxy type ID.
	 * @param int $type A reference to the SiteProxyType table.
	 */
	public function set_proxy_type($type) {
        Assert::is_int($type);

        $this->set_helper('int', $type, 'proxy_type');
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, self::ID_COLUMN);
    }

    public function get_summary() {
        return 'SiteProxies[' . $this->get_id() . ']';
    }

    public function get_fieldmap() {
        return array(
            'AmbServ_id' => self::AMBULANCE_SERVICE_ID_FIELD,
            'Deleted' => self::DELETED_FIELD,
            'Program_id' => self::PROGRAM_ID_FIELD,
            'ProxyType' => self::PROXY_TYPE_FIELD
        );
    }
}
?>
