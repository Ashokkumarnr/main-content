<?php
require_once('phputil/classes/model/abstractTable.inc');

/**
 * This class models the DB table ShiftData.
 */
class ShiftDataTable extends AbstractTable
{
    /**
     * Constructor.
     * @param connection The DB connection.
     */
    function ShiftDataTable(&$connection)
    {
        parent::AbstractTable($connection, 'ShiftData');
    }

    /**
     * Determine the tradable slots on a shift.
     * @param id The event ID.
     * @return The number of tradable slots.
     */
    function getTradableCountByEventId($id)
    {
        if(is_null($id)) return 0;

        $statement = 'SELECT Shift_id FROM ' . $this->getTableName() .
            " WHERE Event_id = {$id}" .
            '   AND Trade=1' .
            '   AND TradeStatus=3';
        $connection =& $this->getConnection();
        $results = $connection->query($statement);
        return count($results);
    }
}
?>
