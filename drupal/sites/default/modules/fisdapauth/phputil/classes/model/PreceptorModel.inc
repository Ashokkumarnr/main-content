<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/

require_once("AbstractModel.inc");
require_once("ProgramPreceptorModel.inc");
require_once("ModelFactory.inc");

class Preceptor extends AbstractModel {

	protected $lastname;
	protected $firstname;
	protected $title;
	protected $ambserv_id;
	protected $home_phone;
	protected $work_phone;
	protected $pager;
	protected $email_address;
	protected $student_id;
	protected $program_preceptor_models;


	const dbtable = "PreceptorData";
	const idfield = "Preceptor_id";

	public function __construct($id=null) {
		//construct ProgramPreceptor Models
		if ($id == null) {
			$this->program_preceptor_models = array();	
		} else {
			$this->program_preceptor_models = ModelFactory::getProgramPreceptorsByPreceptor($id);
		}

		return $this->construct_helper($id, self::dbtable, self::idfield);
	}

	public function set_firstname($first) {
		return $this->set_helper('string', $first, 'firstname');
	}

	public function set_lastname($last) {
		return $this->set_helper('string', $last, 'lastname');
	}

	public function set_title($title) {
		return $this->set_helper('string', $title, 'title');
	}

	public function set_ambserv_id($id) {
		if (is_numeric($id)) {
			return $this->set_helper('int', $id, 'ambserv_id');	
		} else {
			return false;
		}
	}

	public function set_home_phone($phone) {
		return $this->set_helper('string', $phone, 'home_phone');
	}

	public function set_work_phone($phone) {
		return $this->set_helper('string', $phone, 'work_phone');
	}

	public function set_pager($pager) {
		return $this->set_helper('string', $pager, 'pager');
	}

	public function set_email($email) {
		return $this->set_helper('string', $email, 'email_address');
	}

	public function set_student_id($id) {
		if (is_numeric($id)) {
			return $this->set_helper('int', $id, 'student_id');
		} else {
			return false;
		}
	}

	public function add_program_association($program_id) {
		foreach ($this->program_preceptor_models as $model) {
			$program_ids[] = $model->get_program_id();
		}

		if (!in_array($program_id, $program_ids)) {
			$program_assoc = new ProgramPreceptor(null);
			$program_assoc->set_program_id($program_id);
			if ($this->get_id() != null){
				$program_assoc->set_preceptor_id($this->get_id());
			}
			$this->program_preceptor_models[] = $program_assoc;
		}
	}

	public function remove_program_association($program_id) {
		foreach ($this->program_preceptor_models as $key=>$model) {
			if ($model->get_program_id() == $program_id) {
				$model->remove();
				unset($this->program_preceptor_models[$key]);
			} 
			//else {
			//$new_models[] = $model;
			//}
		}
		//$this->program_preceptor_models = $new_models;
	}

	public function get_program_ids() {
		$array = array();
		foreach ($this->program_preceptor_models as $key=>$model) {
			$array[] = $model->get_program_id();
		}

		return $array;
	}

	public function get_summary() {
		return "Preceptor: " . $this->get_lastname() . ", " . $this->get_firstname();
	}

	public function get_name($order = 'first') {
		if ($order == 'first') {
			return $this->get_firstname() . ' ' . $this->get_lastname();
		} else {
			return $this->get_lastname() . ", " . $this->get_firstname();
		}
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "preceptor", self::idfield);
	}

	public function get_fieldmap() {
		return array(
			//"Preceptor_id" => "id",
			'FirstName' => 'firstname',
			'LastName' => 'lastname',
			'Title' => 'title',
			'AmbServ_id' => 'ambserv_id',
			'DateSelected' => 'date_selected',
			'MainBase' => 'mainbase',
			'HPhone' => 'home_phone',
			'WPhone' => 'work_phone',
			'Pager' => 'pager',
			'EmailAddress' => 'email_address',
			'Status' => 'status',
			'AdvisorRNum' => 'advisor_rnum',
			'Student_id' => 'student_id',
			'Active' => 'active',
			'PDAPreceptor_id' => 'pda_preceptor_id',
			);
	}

	public function remove() {	
		foreach ($this->program_preceptor_models as $model) {
			$model->remove();
		}
		parent::remove();
	}

	public function save() {
		parent::save();
		$preceptor_id = $this->get_id();

		foreach ($this->program_preceptor_models as $model) {
			$model->set_preceptor_id($preceptor_id);
			$model->save();
		}
	}
}
