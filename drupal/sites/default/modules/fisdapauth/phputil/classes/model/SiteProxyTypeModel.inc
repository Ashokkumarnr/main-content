<?php
require_once('phputil/classes/model/AbstractEnumeratedModel.inc');

/**
 * This class models the SiteProxyType table.
 */
final class SiteProxyTypeModel extends AbstractEnumeratedModel {
    const DB_TABLE = 'SiteProxyType';

    const ID_REQUIREMENT = 1;

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    /**
     * Determine if this is a requirement.
     * @return boolean TRUE if this is a requirement.
     */
    public function is_requirement() {
        return $this->get_id() == self::ID_REQUIREMENT;
    }

    /**
     * Retrieve information to create prompts.
     * @return array A list of req_RequiredModel sorted by ascending position.
     */
    public static function get_prompt_info() {
        return self::do_get_prompt_info(__CLASS__);
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'SiteProxyType[' . $this->get_id() . ']';
    }

    public static function get_special_ids() {
        return array(
            'ID_REQUIREMENT' => self::ID_REQUIREMENT
        );
    }
}
?>
