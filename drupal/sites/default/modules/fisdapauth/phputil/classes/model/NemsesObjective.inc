<?php
require_once('AbstractModel.inc');
require_once('Asset.inc');
require_once('ModelCriteria.inc');
error_reporting(E_ALL);

/**
 * An item from the 2009 NEMSES educational guidelines
 *
 * Since this model actually spans two tables, it is actually built as two 
 * models, but the interface should act seamlessly like a single object.
 *
 * A note on the breadth/depth settings: In the database the breadth/depth and 
 * the "does this apply to the given level" are encoded together, in one column. 
 * In this model, those are treated separately, by {@link get_depth} and {@link 
 * set_depth} vs. {@link get_applies_to} and {@link set_applies_to}. It is my 
 * belief that this structure will make the model more understandable and less 
 * prone to programming errors.
 *
 * @todo Right now the account types are hardcoded in - maybe we should 
 * interface with account type enums or something?
 */
class NemsesObjective extends AbstractModel {

	const dbtable = "Objective_def";
	const idfield = "ObjectiveDef_id";

	/**
	 * A description of this objective
	 * @var string
	 */
	protected $description;
	/**
	 * @todo
	 * @var int
	 */
	protected $level_id;

	/**#@+
	 * Possible values:
	 *  # 0: Does not apply to this account type
	 *  # 1-3: Applies to this account type with the specified depth or breadth 
	 *   value
	 *  # -5 Applies to this account type but breadth or depth not set
	 *  # -1: Unknown (should only exist in db rows that are not NEMSES 09 
	 *   objectives)
	 * @var int
	 * @ignore
	 */
	protected $emr_depth;
	protected $emr_breadth;
	protected $emt_depth;
	protected $emt_breadth;
	protected $aemt_depth;
	protected $aemt_breadth;
	protected $paramedic_depth;
	protected $paramedic_breadth;
	/**#@-*/

	/**
	 * The underlying asset
	 * @var Asset
	 * @ignore
	 */
	protected $asset;

	/**
	 * @param int|Asset $id The id for this objective, or an Asset object that
	 * links to an objective
	 */
	public function __construct($id=null) {
		if ($id instanceof Asset) {
			$this->asset = $id;
			$id = $this->asset->get_data_id();
			if ($id == null) {
				//TODO we could probably handle this case too
				throw new FisdapInvalidArgumentException('You must pass in a Asset that has already been saved (sorry)');
			}
		} else if ($id === null) {
			$this->asset = new Asset();
			//TODO Hardcoding! Booooooooo!
			$this->asset->set_data_type_id(10);
			// By default we assume this doesn't apply to any of the levels
			foreach (array('emr', 'emt', 'aemt', 'paramedic') as $type) {
				$this->set_applies_to($type, false);
			}
		} else {
			$c = new ModelCriteria(new Asset());
			//TODO Hardcoding! Booooooooo!
			$c->set_data_type_id(10);
			$c->set_data_id($id);
			$db = FISDAPDatabaseConnection::get_instance();
			$result = $db->get_by_criteria($c);
			$this->asset = $result[0];
		}

		self::construct_helper($id, self::dbtable, self::idfield);

	}

	public function save() {
		// If we're newly created, we will need to let Asset know about our id
		$new = ($this->get_id() === null);

		// Save this table
		$this->save_helper();

		// Set the id if necessary
		if ($new) {
			$this->asset->set_data_id($this->get_id());
		}

		// Save the accompanying asset
		$this->asset->save();
	}

	public function __clone() {
		parent::__clone();
		// Clone our asset as well
		$this->asset = clone $this->asset;
	}

	public function remove() {
		// Delete the associated asset as well as our own data
		$this->asset->remove();
		parent::remove();
	}

	/**
	 * @return string
	 */
	public function get_description() {
		return $this->asset->get_description();
	}

	/**
	 * @param string $description
	 */
	public function set_description($description) {
		$this->set_helper('string', $description, 'description');
		// Duplicate the description in the Asset table
		$this->asset->set_description($description);
	}

	/**
	 * @return string
	 */
	public function get_name() {
		return $this->asset->get_name();
	}

	/**
	 * @param string $name
	 */
	public function set_name($name) {
		// Even though there is a name column in the objectives table, we ignore it
		$this->asset->set_name($name);
	}

	/**
	 * @return NemsesObjective this objective's parent
	 */
	public function get_parent() {
		return new NemsesObjective(new Asset($this->asset->get_parent_id()));
	}

	/**
	 * @param NemsesObjective this objective's parent
	 */
	public function set_parent($parent) {
		$this->asset->set_parent_id($parent->get_asset_id());
	}

	/**
	 * @todo provide for NemsesObjective at all?
	 * @return string the "tree id," a period-delineated list of the asset ids 
	 * of this asset and its parents
	 */
	public function get_tree_id() {
		return $this->asset->get_tree_id();
	}

	/**
	 * @return int the id of the Asset that underlies this objective
	 */
	public function get_asset_id() {
		return (int)$this->asset->get_id();
	}

	/**
	 * @return int
	 */
	public function get_sibling_order() {
		return $this->asset->get_sibling_order();
	}

	/**
	 * @param int $sibling_order a number to determine what order this objective will
	 * appear in relative to its siblings
	 */
	public function set_sibling_order($sibling_order) {
		$this->asset->set_sibling_order($sibling_order);
	}

	/**
	 * Set whether this objective applies to the given account type
	 * @param string $type the account type, one of:
	 *  - 'emr'
	 *  - 'emt'
	 *  - 'aemt'
	 *  - 'paramedic'
	 * @param boolean $applies
	 */
	public function get_applies_to($type) {
		$this->assert_is_account_type($type);
		$breadth = $type . '_breadth';
		$depth = $type . '_depth';
		// 0 means "not applicable"
		return (($this->$breadth != 0) && ($this->$depth != 0));
	}

	/**
	 * Set whether this objective applies to the given account type
	 *
	 * Please note that setting this to false will overwrite the breadth and 
	 * depth values for that account type
	 *
	 * @param string $type the account type, one of:
	 *  - 'emr'
	 *  - 'emt'
	 *  - 'aemt'
	 *  - 'paramedic'
	 * @param boolean $applies
	 */
	public function set_applies_to($type, $applies) {
		$this->assert_is_account_type($type);
		Assert::is_boolean($applies);

		if ($applies) {
			// -5 means "applicable but not set"
			if ($this->get_depth($type) === null) {
				$this->set_helper('int', -5, $type . '_depth');
			}
			if ($this->get_breadth($type) === null) {
				$this->set_helper('int', -5, $type . '_breadth');
			}
		} else {
			// 0 means "not applicable"
			$this->set_helper('int', 0, $type . '_depth');
			$this->set_helper('int', 0, $type . '_breadth');
		}
	}

	/**
	 * Get the depth value for this objective
	 * @param string $type the account type, one of:
	 *  - 'emr'
	 *  - 'emt'
	 *  - 'aemt'
	 *  - 'paramedic'
	 * @return int|null 1, 2, or 3 if a depth is set, null otherwise
	 */
	public function get_depth($type) {
		return $this->bd_get_filter($type, 'depth');
	}

	/**
	 * Get the breadth value for this objective
	 * @param string $type the account type, one of:
	 *  - 'emr'
	 *  - 'emt'
	 *  - 'aemt'
	 *  - 'paramedic'
	 * @return int|null 1, 2, or 3 if a breadth is set, null otherwise
	 */
	public function get_breadth($type) {
		return $this->bd_get_filter($type, 'breadth');
	}

	/**
	 * Set the depth value for this objective
	 * @param string $type the account type, one of:
	 *  - 'emr'
	 *  - 'emt'
	 *  - 'aemt'
	 *  - 'paramedic'
	 * @param int the depth: 1, 2, 3, or null
	 */
	public function set_depth($type, $depth) {
		return $this->bd_set_filter($type, 'depth', $depth);
	}

	/**
	 * Set the breadth value for this objective
	 * @param string $type the account type, one of:
	 *  - 'emr'
	 *  - 'emt'
	 *  - 'aemt'
	 *  - 'paramedic'
	 * @param int the breadth: 1, 2, 3, or null
	 */
	public function set_breadth($type, $breadth) {
		return $this->bd_set_filter($type, 'breadth', $breadth);
	}

	/**
	 * Handles the logic for breadth and depth settings
	 * @param string $account_type the type passed in, such as 'emt'
	 * @param string $field_type either 'breadth' or 'depth'
	 * @return int|null the breadth or depth setting
	 */
	private function bd_get_filter($account_type, $field_type) {
		$this->assert_is_account_type($account_type);

		$var = $account_type . '_' . $field_type;
		if ($this->$var > 0) {
			// the cast is because of bug #1614
			return (int)$this->$var;
		} else {
			return null;
		}
	}

	/**
	 * Handles the logic for breadth and depth settings
	 * @param string $account_type the type passed in, such as 'emt'
	 * @param string $field_type either 'breadth' or 'depth'
	 * @param int $value the value to set
	 * @return int|null the breadth or depth setting
	 */
	private function bd_set_filter($account_type, $field_type, $value) {
		$this->assert_is_account_type($account_type);
		if ($value === null || $value === '') {
			$value = -5;
		}
		Assert::is_int($value);
		Assert::is_true($value == -5 || ($value > 0 && $value <= 3));


		$var = $account_type . '_' . $field_type;
		$this->set_helper('int', $value, $var);
	}

	/**
	 * Verify that the string is a valid account type
	 * @param string
	 */
	private function assert_is_account_type($type) {
		Assert::is_string($type);
		Assert::is_true(in_array($type, array('emr', 'emt', 'aemt', 'paramedic')));
	}

	public function get_fieldmap() {
		// We ignore most of the db columns because these objectives don't use them
		return array(
			'Description' => 'description',
			'emr_depth' => 'emr_depth',
			'emr_breadth' => 'emr_breadth',
			'emt_depth' => 'emt_depth',
			'emt_breadth' => 'emt_breadth',
			'aemt_depth' => 'aemt_depth',
			'aemt_breadth' => 'aemt_breadth',
			'para_depth' => 'paramedic_depth',
			'para_breadth' => 'paramedic_breadth',
		);
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "NemsesObjective", self::idfield);
	}

	public function get_summary() {
		return $this->get_description();
	}
}
