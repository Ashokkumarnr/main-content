<?php

/**
 * This class models links between evaluation sessions and other
 * FISDAP data (BLS Skills, IVs, Shifts, etc).
 */
class DataEvalLink {
    /**
     *
     */
    function DataEvalLink() {
    }//DataEvalLink


    /** 
     * Fetch the DataEvalLinks with the given evaluation
     * session id, if any.
     *
     * @param int $session_id an evaluation session id
     * @return array the DataEvalLinks for the given 
     *               evaluation session id, if any
     */
    function &find_by_session_id( $session_id ) {
    }//find_by_session_id


    /**
     *
     */
    function &from_assoc_array( $db_row ) {
    }//from_assoc_array
}//class DataEvalLink

?>
