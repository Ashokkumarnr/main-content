<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/model/AccountTypeModel.inc');
require_once('phputil/classes/model/AmbulanceServiceModel.inc');
require_once('phputil/classes/model/AbstractModel.inc');
require_once('phputil/classes/model/reqs/ReqApplicationModel.inc');
require_once('phputil/classes/model/reqs/ReqFulfillmentModel.inc');
require_once('phputil/classes/model/reqs/ReqRequiredModel.inc');
require_once('phputil/classes/model/reqs/ReqTargetModel.inc');

/**
 * This class models the req_Application table.
 */
final class ReqApplicationModel extends AbstractModel {
    const DB_TABLE = 'req_Application';

    protected $account_type;
    protected $ambulance_service_id;
    protected $base_id;
    protected $creation_date;
    protected $deleted;
    protected $end_date;
    protected $fulfillment_id;
    protected $program_id;
    protected $req_id;
    protected $required_by_date;
    protected $required_id;
    protected $start_date;
    protected $target;

    const ACCOUNT_TYPE_FIELD = 'account_type';
    const AMBULANCE_SERVICE_ID_FIELD = 'ambulance_service_id';
    const BASE_ID_FIELD = 'base_id';
    const CREATION_DATE_FIELD = 'creation_date';
    const DELETED_FIELD = 'deleted';
    const END_DATE_FIELD = 'end_date';
    const ID_FIELD = 'id';
    const FULFILLMENT_ID_FIELD = 'fulfillment_id';
    const PROGRAM_ID_FIELD = 'program_id';
    const REQ_ID_FIELD = 'req_id';
    const REQUIRED_BY_DATE_FIELD = 'required_by_date';
    const REQUIRED_ID_FIELD = 'required_id';
    const START_DATE_FIELD = 'start_date';
    const TARGET_FIELD = 'target';

	/**
	 * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
	 */
	public function __construct($data=null) {
        Assert::is_true(is_array($data) || Test::is_null($data) || Test::is_int($data));

        $this->construct_helper($data, self::DB_TABLE, self::ID_FIELD);
    }

	/**
	 * Retrieve the requirement ID.
	 * @return int | null A reference to the req_Reqirement table.
	 */
	public function get_req_id() {
        return $this->req_id;
    }

	/**
	 * Set the requirement ID.
	 * @param int $id A reference to the req_Reqirement table.
	 */
	public function set_req_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'req_id');
    }

	/**
	 * Determine if the user requirement has been deleted.
	 * @return boolean | null TRUE if deleted.
	 */
	public function is_deleted() {
        return ($this->deleted != 0);
    }

	/**
	 * Determine if the user requirement has been deleted.
	 * @return int | null The deleted, 0 for not, otherwise deleted.
	 */
	public function get_deleted() {
        return $this->deleted;
    }

	/**
	 * Indicate whether the user requirement is deleted.
	 * @param boolean $deleted TRUE if deleted.
	 */
	public function set_deleted($deleted) {
        Assert::is_boolean($deleted);

        $this->set_helper('bool', $deleted, 'deleted');
    }

	/**
	 * Retrieve the ambulance service ID.
	 * @return int | null A reference to the AmbulanceServices table.
	 */
	public function get_ambulance_service_id() {
        return $this->ambulance_service_id;
    }

	/**
	 * Set the ambulance service ID.
	 * @param int $id A reference to the AmbulanceServices table.
	 */
	public function set_ambulance_service_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'ambulance_service_id');
    }

	/**
	 * Retrieve the ambulance service base ID.
	 * @return int | null A reference to the AmbServ_Bases table.
	 */
	public function get_base_id() {
        return $this->base_id;
    }

	/**
	 * Set the ambulance service base ID.
	 * @param int $id A reference to the AmbServ_Bases table.
	 */
	public function set_base_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'base_id');
    }

	/**
	 * Retrieve the program ID.
	 * @return int | null A reference to the ProgramData table.
	 */
	public function get_program_id() {
        return $this->program_id;
    }

	/**
	 * Set the program ID.
	 * @param int $id A reference to the ProgramData table.
	 */
	public function set_program_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'program_id');
    }

    /**
     * Retrieve the creation date.
     * @return FisdapDateTime The date / time.
     */
    public function get_creation_date() {
        return FisdapDateTime::create_from_sql_string($this->creation_date);
    }

    /**
     * Set the due date.
     * @param FisdapDateTime $date_time The date / time.
     */
    public function set_creation_date(FisdapDateTime $date_time) {
        $this->set_helper('datetime', $date_time, 'creation_date');
    }

    /**
     * Determine if the application is valid today.
     * @return boolean TRUE if today is within the valid dates.
     */
    public function is_valid_now() {
        $today = FisdapDate::today();

        $date = $this->get_start_date();
		if ($date->is_set()) {
			if ($date->compare($today) > 0) return false;
        }

        $date = $this->get_end_date();
        if ($date->is_set()) {
			if ($date->compare($today) < 0) return false;
        }

        return true;
    }

    /**
     * Retrieve the start date.
     * @return FisdapDate The date.
     */
    public function get_start_date() {
        return FisdapDate::create_from_sql_string($this->start_date);
    }

    /**
     * Set the start date.
     * @param FisdapDate $date The date.
     */
    public function set_start_date(FisdapDate $date) {
        $this->set_helper('date', $date, 'start_date');
    }

    /**
     * Retrieve the required by date.
     * @return FisdapDate The date.
     */
    public function get_required_by_date() {
        return FisdapDate::create_from_sql_string($this->required_by_date);
    }

    /**
     * Set the required by date.
     * @param FisdapDate $date The date.
     */
    public function set_required_by_date(FisdapDate $date) {
        $this->set_helper('date', $date, 'required_by_date');
    }

	/**
	 * Retrieve the required ID.
	 * @return int | null A reference to the req_Required table.
	 */
	public function get_required_id() {
        return $this->required_id;
    }

	/**
	 * Set the required ID.
	 * @param int $id A reference to the req_Required table.
	 */
	public function set_required_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'required_id');
    }

    /**
     * Retrieve the end date.
     * @return FisdapDate The date.
     */
    public function get_end_date() {
        return FisdapDate::create_from_sql_string($this->end_date);
    }

    /**
     * Set the end date.
     * @param FisdapDate $date The date.
     */
    public function set_end_date(FisdapDate $date) {
        $this->set_helper('date', $date, 'end_date');
    }

    /**
     * Retrieve the target audience.
     * @return int | null The target audience.
     */
    public function get_target() {
        return $this->target;
    }

    /**
     * Set the target audience.
     * @param int $target The target audience.
     */
    public function set_target($target) {
        Assert::is_int($target);
        Assert::is_true($target >= 0);

        $this->set_helper('int', $target, 'target');
    }

    /**
     * Retrieve the account type mask.
     * @return int | null $mask The account type mask.
     */
    public function get_account_type() {
        return $this->account_type;
    }

    /**
     * Set the account type mask.
     * @param int $mask The account types.
     */
    public function set_account_type($mask) {
        Assert::is_int($mask);
        Assert::is_true($mask >= 0);

        $this->set_helper('int', $mask, 'account_type');
    }

    /**
     * Retrieve the fulfillment ID.
     * @return int | null A reference into the req_Fulfillment table.
     */
    public function get_fulfillment_id() {
        return $this->fulfillment_id;
    }

    /**
     * Set the 
     * @param int $id A reference into the AccountType table.
     */
    public function set_fulfillment_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'fulfillment_id');
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, self::ID_FIELD);
    }

    public function get_summary() {
        return 'ReqApplication[' . $this->get_id() . ']';
    }

    public function get_fieldmap() {
        return array(
            'AccountType' => self::ACCOUNT_TYPE_FIELD,
            'AmbServ_id' => self::AMBULANCE_SERVICE_ID_FIELD,
            'Base_id' => self::BASE_ID_FIELD,
            'CreationDate' => self::CREATION_DATE_FIELD,
            'Deleted' => self::DELETED_FIELD,
            'EndDate'=> self::END_DATE_FIELD,
            'Fulfillment' => self::FULFILLMENT_ID_FIELD,
            'Program_id' => self::PROGRAM_ID_FIELD,
            'Requirement_id' => self::REQ_ID_FIELD,
            'RequiredBy' => self::REQUIRED_BY_DATE_FIELD,
            'Required' => self::REQUIRED_ID_FIELD,
            'StartDate'=> self::START_DATE_FIELD,
            'Target' => self::TARGET_FIELD
        );
    }
}
?>
