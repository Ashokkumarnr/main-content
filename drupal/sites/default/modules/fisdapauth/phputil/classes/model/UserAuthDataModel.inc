<?php

/*----------------------------------------------------------------------*
  |                                                                      |
  |     Copyright (C) 1996-2010.  This is an unpublished work of         |
  |                      Headwaters Software, Inc.                       |
  |                         ALL RIGHTS RESERVED                          |
  |     This program is a trade secret of Headwaters Software, Inc.      |
  |     and it is not to be copied, distributed, reproduced, published,  |
  |     or adapted without prior authorization                           |
  |     of Headwaters Software, Inc.                                     |
  |                                                                      |
*----------------------------------------------------------------------*/

require_once('phputil/classes/model/UserAbstractModel.inc');
require_once('phputil/classes/DateTime.inc');
require_once('phputil/classes/model/AccountTypeModel.inc');
require_once('phputil/handy_utils.inc');

/* Business rules enforced / automated:


 */


class UserAuthDataModel extends UserAbstractModel {
	protected $idx;
	protected $uid;
	protected $gid;
	protected $first_name;
	protected $last_name;
	protected $organization;
	protected $username;
	protected $alias; 
	protected $password;
	protected $home_path;
	protected $instructor;
	protected $has_account; 
	protected $timestamp;
	protected $password_hint;
	protected $md5_password;
	protected $email;
	protected $enabled;

	// password is stored encrypted, password length is real password length before encryption, used for validation
	protected $password_length;

	const DB_TABLE = "UserAuthData";
	const ID_FIELD = "idx";
	const VALIDATION = 'validation';

	public function get_user_type() {
		return ($this->get_instructor()) ? 'instructor' : 'student';
	}

	public function set_user_type($user_type) {
		$ret = true;
		if ($user_type == 'instructor') {
			$this->set_instructor(1);
		} else if ($user_type == 'student') {
			$this->set_instructor(0);
		} else {
			$ret = false;
		}
		return $ret;
	}

	public function __construct($id=null) {
		$ret = self::construct_helper($id, self::DB_TABLE, self::ID_FIELD);
		parent::__construct($id);
		return $ret;
	}

	public function get_fieldmap() {
		return array(
			'uid' => 'uid',
			'gid' => 'gid',
			'firstname' => 'first_name',  
			'lastname' => 'last_name',
			'org' => 'organization',
			'email' => 'username',   
			'alias' => 'alias',
			'epass' => 'password',
			'home' => 'home_path',
			'instructor' => 'instructor', 
			'hasaccount' => 'has_account',
			'tstamp' => 'timestamp',
			'PWHint' => 'password_hint',
			'md5pass' => 'md5_password',
			'emailaddress' => 'email',
			'enabled' => 'enabled'
		);
	}

	protected function load_batch_validation_rules() {
		$this->fields = array(
			'first_name' => array(
				'max_len' => 30,
				'is_required' => true,
				'min_len' => 1
			),
			'last_name' => array(
				'max_len' => 30,
				'is_required' => true,
				'min_len' => 1
			),
			'username' => array(
				'max_len' => 24,
				'is_required' => true,
				'is_valid_fisdap_username' => true
			),
			'email' => array(
				'max_len' => 50,
				'is_required' => true,
				'min_len' => 0
			),
			'password_length' => array(	// 'internal only field used for validation'
				'min_val' => 5
			)
		);
	}

	public function set_all_to_defaults() {

	}



	public static function getBySQL($result) {
		return self::sql_helper($result, "UserAuthDataModel", self::ID_FIELD);
	}

	public function get_summary() {
		return "User Auth Data Model";
	}




	//		FACTORY METHODS

	public static function get_by_username($username) {
		$connection = FISDAPDatabaseConnection::get_instance();

		$query = "SELECT * FROM ".self::DB_TABLE." where `email`='$username'";
		$res = $connection->query($query);

		if (count($res)>0) {
			return new self($res[0]);
		} else {
			return null;
		}
	}




	//		MODEL-SPECIFIC METHODS

	/**
	 *	@param string $plain_text_password
	 *	@return string encrypted password
	 */
	public function encrypt_password($plain_text_password) {
		//create random info for the salt
		$r1 = mt_rand(0, 9);
		$r2 = mt_rand(0, 9);

		//create the salt
		$newsalt = $r1 . $r2;

		//encrypt the new password with the salt 
		$encrypted_password = Crypt($plain_text_password, $newsalt);

		return $encrypted_password;
	}


	//		SETTERS

	public function set_uid($val) {
		return $this->set_helper("int", $val, "uid");
	}

	public function set_gid($val) {
		return $this->set_helper("int", $val, "gid");
	}

	public function set_first_name($val) {
		return $this->set_helper("string", $val, "first_name");
	}

	public function set_last_name($val) {
		return $this->set_helper("string", $val, "last_name");
	}

	public function set_organization($val) {
		return $this->set_helper("string", $val, "organization");
	}

	public function set_username($val) {
		return $this->set_helper("string", $val, "username");
	}

	public function set_alias($val) {
		return $this->set_helper("string", $val, "alias");
	}

	public function set_password($val) {
		$this->password_length = strlen($val);
		$val = $this->encrypt_password($val);
		return $this->set_helper("string", $val, "password");
	}

	public function set_home_path($val) {
		return $this->set_helper("string", $val, "home_path");
	}

	public function set_instructor($val) {
		return $this->set_helper("int", $val, "instructor");
	}

	public function set_has_account($val) {
		return $this->set_helper("int", $val, "has_account");
	}

	public function set_password_hint($val) {
		return $this->set_helper("string", $val, "password_hint");
	}

	public function set_email($val) {
		return $this->set_helper("string", $val, "email");
	}

	public function set_enabled($val) {
		return $this->set_helper("int", $val, "enabled");
	}

	// function needed for password length validation
	private function get_password_length() {
		return $this->password_length;
	}
}
?>
