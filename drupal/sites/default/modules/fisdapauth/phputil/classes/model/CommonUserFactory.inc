<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FisdapCaches.inc');
require_once('phputil/classes/FisdapSimpleCache.inc');
require_once('phputil/classes/common_user.inc');
require_once('phputil/exceptions/FisdapException.inc');

/**
 * A factory for users.
 */
final class CommonUserFactory {
	const ID_PREFIX = 'id:';
	const USER_NAME_PREFIX = 'username:';

	private $cache;

	protected function __construct() {
		$this->cache = new FisdapSimpleCache('CommonUser');
	}

	/**
	 * Retrieve the singleton.
	 * @return The cache.
	 */
	public static function get_instance() {
		static $instance;

		if (is_null($instance)) {
			$instance = new CommonUserFactory();
			FisdapCaches::get_instance()->register($instance->cache);
		}

		return $instance;
	}

	/**
	 * Retrieve the underlying cache.
	 * @return FisdapCache The underlying cache.
	 */
	public function get_cache() {
		return $this->cache;
	}

	/**
	 * Retrieve a user by a variety of sources.
	 * @param mixed $source The model source.
	 * @param boolean $must_exist TRUE if the user must be found.
	 * @return common_user | null The user (null if bad or not found).
	 * @throws FisdapException If the model can't be retrieved if 
	 * $must_exist=true.
	 */
	public function get_by_source($source=null, $must_exist=true) {
		global $REMOTE_USER; 

		Assert::is_boolean($must_exist);
		Assert::is_true(
			Test::is_a($source, 'common_instructor') ||
			Test::is_a($source, 'common_student') ||
			Test::is_a($source, 'common_user') ||
			Test::is_int($source) ||
			Test::is_string($source) ||
			Test::is_null($source));

		$this->cache->clear_hit();
		$user = null;

		if ($source instanceof common_user) {
			$this->add($source);

			if (!$source->is_valid()) {
				if ($must_exist) {
					throw new FisdapException(self::get_msg($source, null));
				}

				return null;
			}

			return $source;
		}

		if ($source instanceof common_student) {
			$source = $source->get_username();
		} elseif ($source instanceof common_instructor) {
			if (!$source->is_valid()) {
				if ($must_exist) {
					throw new FisdapException(self::get_msg($source, $user));
				}

				return null;
			}

			$source = $source->get_username();
		}

		try {
			if (Test::is_int($source)) {
				$key = self::ID_PREFIX . $source;
				if ($this->cache->contains($key)) {
					return $this->cache->get($key);
				}

				$user = common_user::get_by_user_id($source);
			} elseif (is_string($source)) {
				$key = self::USER_NAME_PREFIX . $source;
				if ($this->cache->contains($key)) {
					return $this->cache->get($key);
				}

				$user = common_user::get_by_username($source);
			} else {
				// No source.
				if (!is_null($REMOTE_USER)) {
					return $this->get_by_source($REMOTE_USER, $must_exist);
				}
			}
		}
		catch (FisdapException $e) {
			if ($must_exist) {
				throw new FisdapException(self::get_msg($source, $user), $e);
			}

			$user = null;
		}

		if ($must_exist) {
			if (is_null($user) || !$user->is_valid()) {
				throw new FisdapException(self::get_msg($source, $user));
			}
		}

		if (!is_null($user)) {
			$this->add($user);

			if (!$user->is_valid()) {
				return null;
			}
		}

		return $user;
	}


	/**
	 * Add a user to the cache.
	 * @param common_user $user The user.
	 */
	private function add(common_user $user) {
		if (!$user->is_valid()) {
			return;
		}

		$this->cache->put(self::ID_PREFIX. $user->get_UserAuthData_idx(), $user);
		$this->cache->put(self::USER_NAME_PREFIX. $user->get_username(), $user);
	}

	private static function get_msg($source, $user) {
		$msgs = array();

		if (!Test::is_object($source)) {
			$msgs[] = "source[$source]";
		}

		if (!is_null($user)) {
			if (!$user->is_valid()) {
				$msgs[] = 'invalid user';
			}
		}

		$msg = 'Unable to retrieve user';
		if (count($msgs)) {
			$msg .= '; ' . join(', ', $msgs);
		}

		return $msg;
	}
}
?>
