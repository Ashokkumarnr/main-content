<?php

/* This is a copy of OtherALS, it's not finished at all
 * Keep in mind that there is no table for Arrest. Arrest
 * Data is stored in RunData and BLS tables */

require_once("DataModel.inc");

class Complaint extends DataModel {

	/*********
	 * FIELDS
	 *********/
	protected $complaint;
	protected $assessment_id;
	const dbtable = "PtComplaintData";
	const id_field = "Data_id";
	protected $hook_map = array(3 => 28,
		7 => 29,
		6 => 30,
		4 => 31,
		2 => 32,
		8 => 33,
		1 => 34,
		5 => 35);


	/**************
	 * CONSTRUCTOR
	 **************/

	public function __construct($id=null) {
		parent::__construct();
		$this->set_helper("int", -1, "assessment_id");
		if (!$this->construct_helper($id, self::dbtable, self::id_field)) {
			return false;
		}
	}

	/************
	 * FUNCTIONS
	 ************/


	public function set_complaint($_complaint) {
		$id = $this->id_or_string_helper($_complaint, "get_complaint_names");
		if ($id) {
			return $this->set_helper("int", $id, "complaint");
		}
		return false;
	}

	public function set_assessment_id($assessment_id) {
		return $this->set_helper("int", $assessment_id, "assessment_id");
	}

	public function get_summary() {
		$complaints = self::get_complaint_names();
		return $complaints[$this->complaint];
	}

	public function get_mobile_summary() {
		//STUB
	}

	public function get_hook_ids() {
		if ($this->complaint > 0) {
			return array($this->hook_map[$this->complaint]);
		} else {
			return array();
		}	
	}

	public static function get_complaint_names() {
		return self::data_table_helper("PtComplaintTable", "Complaint_id", "Complaint");
	}

	/*
	 * FUNCTION getBySQL
	 *
	 * Creates Complaint objects based on a MySQL result set so the database doesn't need to be hit more than once.
	 *
	 * @param result A result set containing on or more complete MedData rows. The rows should be complete MedData records, or we have to hit the DB anyway, which completely and totally defeats the purpose. 
	 * @pre The result set CANNOT use alias names.
	 * @return array An array of Meds.
	 * @TODO untested
	 * @TODO can we move this to the parent class and use constants for the class-specific info (ID field name)?
	 */
	public static function getBySQL($result) {
		return parent::sql_helper($result, "complaint", self::id_field);
	}

	public function get_fieldmap() {
		return array("Run_id" => "run_id",
				"Student_id" => "student_id",
				"Complaint_id" => "complaint",
				"Shift_id" => "shift_id",
				"SubjectType_id" => "subject_type",
				"Asses_id" => "assessment_id");
	}

}

?>
