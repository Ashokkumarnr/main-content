<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/model/AbstractModel.inc');

/**
 * This class models the req_Relationship table.
 */
final class ReqRelationshipModel extends AbstractModel {
    const DB_TABLE = 'req_Relationship';
    const ID_COLUMN = 'id';

    protected $alternate_req_id;
    protected $application_id;
    protected $approved;
    protected $deleted;

    const ALTERNATE_REQ_ID_FIELD = 'alternate_req_id';
    const APPLICATION_ID_FIELD = 'application_id';
    const APPROVED_FIELD = 'approved';
    const DELETED_FIELD = 'deleted';

	/**
	 * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
	 */
	public function __construct($data=null) {
        Assert::is_true(is_array($data) || Test::is_null($data) || Test::is_int($data));

        $this->construct_helper($data, self::DB_TABLE, self::ID_COLUMN);
    }

	/**
	 * Retrieve the application ID.
	 * @return int | null A reference to the req_Application table.
	 */
	public function get_application_id() {
        return $this->application_id;
    }

	/**
	 * Set the application ID.
	 * @param int $id A reference to the req_Application table.
	 */
	public function set_application_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'application_id');
    }

	/**
	 * Retrieve the alternate requirement ID.
	 * @return int | null A reference to the req_Reqirement table.
	 */
	public function get_alternate_req_id() {
        return $this->alternate_req_id;
    }

	/**
	 * Set the alternate requirement ID.
	 * @param int $id A reference to the req_Reqirement table.
	 */
	public function set_alternate_req_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'alternate_req_id');
    }

	/**
	 * Determine if the relationship has been approved.
	 * @return boolean | null TRUE if approved.
	 */
	public function is_approved() {
        return ($this->approved != 0);
    }

	/**
	 * Determine if the relationship has been approved.
	 * @return int | null The approval, 0 for not, otherwise approved.
	 */
	public function get_approved() {
        return $this->approved;
    }

	/**
	 * Indicate whether the relationship is approved.
	 * @param boolean $approved TRUE if approved.
	 */
	public function set_approved($approved) {
        Assert::is_boolean($approved);

        $this->set_helper('bool', $approved, 'approved');
    }

	/**
	 * Determine if the relationship has been deleted.
	 * @return boolean | null TRUE if deleted.
	 */
	public function is_deleted() {
        return ($this->deleted != 0);
    }

	/**
	 * Determine if the relationship has been deleted.
	 * @return int | null The deleted, 0 for not, otherwise deleted.
	 */
	public function get_deleted() {
        return $this->deleted;
    }

	/**
	 * Indicate whether the relationship is deleted.
	 * @param boolean $deleted TRUE if deleted.
	 */
	public function set_deleted($deleted) {
        Assert::is_boolean($deleted);

        $this->set_helper('bool', $deleted, 'deleted');
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, self::ID_COLUMN);
    }

    public function get_summary() {
        return 'ReqRelationship[' . $this->get_id() . ']';
    }

    public function get_fieldmap() {
        return array(
            'AlternateRequirement_id' => self::ALTERNATE_REQ_ID_FIELD,
            'Application_id' => self::APPLICATION_ID_FIELD,
            'Approved' => self::APPROVED_FIELD,
            'Deleted' => self::DELETED_FIELD 
        );
    }
}
?>
