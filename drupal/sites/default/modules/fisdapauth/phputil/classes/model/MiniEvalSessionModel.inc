
<?php
 
require_once('AbstractModel.inc');

/**
 * Represents a program in the DB.
 * #######################################################################
 * This class only handles get functions until all fields are implemented.
 * #######################################################################
 */
class MiniEvalSession extends AbstractModel {

// protected mini_eval_session_id;
		protected $evaluator;
		protected $evaluator_type;
		protected $subject;
		protected $subject_type;
		protected $date_completed;
		protected $pass_or_fail;
		protected $crit_criteria_missed;
		protected $mini_eval_id;
		protected $preceptor_site;

		const DB_TABLE_NAME = 'MiniEvalSession';
		const ID_FIELD = 'MiniEvalSession_id';

	public function __construct($data) {
		$this->construct_helper($data, self::DB_TABLE_NAME, self::ID_FIELD);
	}

	public function get_fieldmap() {
		$map = array(
					// 'MiniEvalSession_id' => 'not used'
					'Evaluator' => 'evaluator',
					'EvaluatorType' => 'evaluator_type',
					'Subject' => 'subject',
					'SubjectType' => 'subject_type',
					'DateCompleted' => 'date_completed',
					'PassOrFail' => 'pass_or_fail',
					'CritCriteriaMissed' => 'crit_criteria_missed',
					'MiniEval_id' => 'mini_eval_id',
					'PreceptorSite' => 'preceptor_site'
        );
		return $map;
	}
	
	public function get_summary() {
		return $this->get_string();
	}

	/**
	 * Creates objects based on a MySQL result set so the database doesn't need to be hit more than once.
	 *
	 * @param result A result set containing one or more complete rows. The rows should be complete records, or we have to hit the DB anyway, which completely and totally defeats the purpose.
	 * @pre The result set CANNOT use alias names.
	 * @return array An array of objects.
	 * @todo can we move this to the parent class and use constants for the class-specific info (ID field name)?
	 */
	public static function getBySQL($result) {
		return parent::sql_helper($result, 'MiniEvalSession', self::ID_FIELD);
	}

	/**
	 * Set the the id of the person who is ascting as the evaluator in this particular evaluation.
	 * @param int $evaluator id of the person who is ascting as the evaluator in this particular evaluation
	 */
	public function set_evaluator($evaluator){
		Assert::is_int($evaluator);
	  $this->set_helper("int", $evaluator, "evaluator");
  }

	/**
	 * Set the evaluator type of who is doing the evaluation (IE instructor, preceptor, student)
	 * @param int $evaluator_type The evaluator type of who is doing the evaluation
	 */
	public function set_evaluator_type($evaluator_type){
		Assert::is_int($evaluator_type);
	  $this->set_helper("int", $evaluator_type, "evaluator_type");
  }

	/**
	 * Set the id of the person who is being evaluated
	 * @param int $subject the id of the person who is being evaluated
	 */
	public function set_subject($subject){
		Assert::is_int($subject);
	  $this->set_helper("int", $subject, "subject");
  }

	/**
	 * Set the subject type of who is being evaluated. (IE instructor, preceptor, student)
	 * @param int $subject_type Role type of who is being evaluated
	 */
	public function set_subject_type($subject_type){
		Assert::is_int($subject_type);
	  $this->set_helper("int", $subject_type, "subject_type");
  }

	/**
	 * Set the date time for when the evaluation was completed
	 * @param FisdapDateTime $date_completed When the evaluation was completed
	 */
	public function set_date_completed(FisdapDateTime $date_completed){
		 Assert::is_a($date_completed, 'FisdapDateTime');
	  $this->set_helper("datetime", $date_completed, "date_completed");
  }

	/**
	 * Set whether or not the subject passed of failed the evaluation.
	 * @param bool $pass_or_fail Records the pass or fail status of the evaluation
	 */
	public function set_pass_or_fail($pass_or_fail){
		Assert::is_boolean($pass_or_fail);
	  $this->set_helper("bool", $pass_or_fail, "pass_or_fail");
  }

	/**
	 * Set the critical criteria missed flag
	 * @param bool $crit_criteria_missed Sets Whether or not the subject missed a critical criteria
	 */
	public function set_crit_criteria_missed($crit_criteria_missed){
		Assert::is_boolean($crit_criteria_missed);
	  $this->set_helper("bool", $crit_criteria_missed, "crit_criteria_missed");
  }

	/**
	 * Set the mini evaluation id this evaluation is for (a foreign key to MiniEval)
	 * @param int $mini_eval_id The skill that is being evaluated
	 */
	public function set_mini_eval_id($mini_eval_id){
		Assert::is_int($mini_eval_id);
	  $this->set_helper("int", $mini_eval_id, "mini_eval_id");
  }

	/**
	 * Set the Preceptor Site 
	 * @param int $preceptor_site The skill that is being evaluated
	 */
	public function set_preceptor_site($preceptor_site){
		Assert::is_int($preceptor_site);
	  $this->set_helper("int", $preceptor_site, "preceptor_site");
  }

}
?>


















