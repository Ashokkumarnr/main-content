<?php
/**
 * Defines an enumerated type in the DB.
 * The enumerated type is defined so that it can be used to display information to the users.
 * Note that since the column name in the DB is 'id', and the instance var 'id' 
 * has magic meaning to the Model class, we have to kludge around and save the 
 * 'id' in a different var.
 */
interface EnumeratedModel {
    /**
     * Set the ID.
     * @param int $id The ID.
     */
    public function set_id($id); 

    /**
     * Retrieve the text describing the entry.
     * @return string The text.
     */
    public function get_text(); 

    /**
     * Set the text describing the entry.
     * @param string $text The text.
     */
    public function set_text($text); 

    /**
     * Retrieve the ordering position.
     * Smaller numbers should appear first to the user.
     * @return int The position, at least 1.
     */
    public function get_position(); 

    /**
     * Set the position.
     * Smaller numbers should appear first to the user.
     * @param int $postiion The position, at least 1.
     */
    public function set_position($position); 

    /**
     * Retrieve information to create prompts.
     * @return array A list of objects sorted by ascending position.
     */
    public static function get_prompt_info(); 

    /**
     * Return the special ID's.
     * These are used by the UI and should be defined in the environment.
     * @return array The name as the key and the ID as the value.
     */
    public static function get_special_ids();
}
?>
