<?php
require_once('phputil/classes/Assert.inc');

/**
 * Describes an order by clause.
 * Field names are PHP field names and NOT DB column names.  The mapping occurs 
 * at a later time.
 */
final class OrderBy {
    private $ascending;
    private $field_name;

    /**
     * Constructor.
     * @param string $field_name The field name, i.e. xxx from set_xxx().
     * @param boolean $ascending TRUE if ordering should be in ascending order.
     */
    public function __construct($field_name, $ascending) {
        Assert::is_not_empty_string($field_name);
        Assert::is_boolean($ascending);

        $this->field_name = $field_name;
        $this->ascending = $ascending;
    }

    /**
     * Retrieve the field name.
     * @return string The field name.
     */
    public function get_field_name() {
        return $this->field_name;
    }

    /**
     * Determine if the ordering is in ascending order.
     * @return boolean TRUE if in ascending order.
     */
    public function is_ascending() {
        return $this->ascending;
    }

    /**
     * Determine if the ordering is in descending order.
     * @return boolean TRUE if in descending order.
     */
    public function is_descending() {
        return !$this->ascending;
    }

    public function __toString() {
        $s = 'OrderBy[' . $this->get_field_name() . ',';
        $s .= ($this->is_ascending() ? 'ascending' : 'descending');
        return $s;
    }
}
?>
