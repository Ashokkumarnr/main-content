<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
*----------------------------------------------------------------------*/

require_once("Model.inc");
require_once("PreceptorModel.inc");
//require_once("GenderModel.inc");

class run extends AbstractModel {
	protected $id;
	protected $shift_id;
	protected $student_id;
	protected $precept_id;
	protected $num_in_team;
	protected $team_leader;
	protected $phase;
	protected $priority;
	protected $disposition;
	protected $pulsereturn;
	protected $witness;
	protected $selfeval;
	protected $precepteval;
	protected $pdarun_id;
	protected $age;
	protected $gender;
	protected $ethnicity;
	protected $diagnosis;
	protected $entry_time;
	protected $loc;
	protected $systolicbp;
	protected $diastolicbp;
	protected $syncopal;
	protected $diag2;
	protected $moi;
	protected $months;
	protected $subjecttype_id;

	const dbtable = "RunData";
	const idfield = "Run_id";

	public function __construct($id=null) {
		$this->set_helper('int', -1, 'student_id');
		$this->set_helper('int', -1, 'shift_id');
		return $this->construct_helper($id, self::dbtable, self::idfield);
	}

	public function set_shift_id($_shift) {
		return $this->set_helper("int", $_shift, "shift_id");
	}

	public function set_student_id($_student) {
		return $this->set_helper("int", $_student, "student_id");
	}

	public function set_precept_id($_precept) {
		return $this->set_helper("int", $_precept, "precept_id");
	}

	public function set_team_num($num) {
		return $this->set_helper("int",$num,"num_in_team");
	}

	public function set_team_lead($_lead) {
		return $this->set_helper("bool",$_lead,"team_leader");
	}

	public function set_phase($_phase) {
		if ($_phase > 0 || is_string($_phase)) {
			$_phase = $this->id_or_string_helper($_phase,"get_phase_names");
		}
		return $this->set_helper("int",$_phase,"phase");
	}

	public function set_priority($_priority) {
		if ($_priority > 0 || is_string($_phase)) {
			$_priority = $this->id_or_string_helper($_priority,"get_priority_names");
		}
		return $this->set_helper("int",$_priority,"priority");
	}

	public function set_disposition($_disposition) {
		if ($_disposition > 0 || is_string($_disposition)) {
			$_disposition = $this->id_or_string_helper($_disposition,"get_disposition_names");
		}
		return $this->set_helper("int",$_disposition,"disposition");
	}

	public function set_pulsereturn($_pulse) {
		if ($_pulse > 0 || is_string($_pulse)) {
			$_pulse = $this->id_or_string_helper($_pulse,"get_pulsereturn_names");
		}
		return $this->set_helper("int",$_pulse,"pulsereturn");
	}

	public function set_witness($_witness) {
		if ($_witness > 0 || is_string($_witness)) {
			$_witness = $this->id_or_string_helper($_witness,"get_witness_names");
		}
		return $this->set_helper("int",$_witness,"witness");
	}

	public function set_age($_age) {
		if (is_numeric($_age)) {
			return $this->set_helper("int",$_age,"age");
		} else {
			return false;
		}
	}

	/**
	 * I'm going to use data_table_helper rather than the gender model
	 */
	public function set_gender($_gender) {
		if ($_gender > 0 || is_string($_gender)) {
			$_gender = $this->id_or_string_helper($_gender,"get_gender_names");
		}
		return $this->set_helper("int",$_gender,"gender");
	}

	public function set_ethnicity($_ethnicity) {
		if ($_ethnicity > 0 || is_string($_ethnicity)) {
			$_ethnicity = $this->id_or_string_helper($_ethnicity,"get_ethnicity_names");
		}
		return $this->set_helper("int",$_ethnicity,"ethnicity");
	}

	public function set_diagnosis($_diag) {
		if ($_diag > 0 || is_string($_diag)) {
			$_diag = $this->id_or_string_helper($_diag,"get_diag_names");
		}
		return $this->set_helper("int",$_diag,"diagnosis");
	}

	public function set_loc($_loc) {
		if ($_loc > 0 || is_string($_loc)) {
			$_loc = $this->id_or_string_helper($_loc,"get_loc_names");
		}
		return $this->set_helper("int",$_loc,"loc");
	}

	public function set_systolicbp($_bp) {
		if (is_numeric($_bp)) {
			return $this->set_helper("int",$_bp,"systolicbp");
		}
	}

	public function set_diastolicbp($_bp) {
		if (strtoupper($_bp) == "P") {
			return $this->set_helper("int", -2, "diastolicbp");
		} else if (is_numeric($_bp)) {
			return $this->set_helper("int", $_bp, "diastolicbp");
		}
	}

	public function set_diag2($_diag) {
		if ($_diag > 0 || is_string($_diag)) {
			$_diag = $this->id_or_string_helper($_diag,"get_diag_names");
		}
		return $this->set_helper("int",$_diag,"diag2");
	}

	public function set_moi($_moi) {
		if ($_moi > 0 || is_string($_moi)) {
			$_moi = $this->id_or_string_helper($_moi,"get_diag_names");
		}
		return $this->set_helper("int",$_moi,"moi");
	}

	public function set_months($_months) {
		if ($_months == null) {
			return $this->set_helper("int", 0, "months");
		} else if (is_numeric($_months)) {
			return $this->set_helper("int",$_months,"months");
		} else {
			return false;
		}
	}

	public function get_systolicbp() {
		if ($this->systolicbp < 0) {
			return null;
		} else {
			return $this->systolicbp;
		}
	}

	public function get_diastolicbp() {
		if ($this->diastolicbp == -2) {
			return "P";
		} else if ($this->diastolicbp == -1) {
			return null;
		} else {
			return $this->diastolicbp;
		}
	}



	/**
	 * Was the student Team Leader for this run?
	 *
	 * @param boolean If false, the student gets team lead credit as long as
	 * they are marked as team lead for this run. If true, student only gets credit
	 * according to the NSC guidelines, which state that the student must also
	 * perform the patient interview and exam.
	 *
	 * @return boolean
	 */
	public function get_team_leader($require_assessment=false) {
		$team_lead = ($this->team_leader != 0);
		if ($require_assessment) {
			$team_lead = ($team_lead && $this->get_performed_comprehensive_assessment());
		}
		return $team_lead;
	}

	/**
	 * Did the student perform a comprehensive assessment?
	 *
	 * BTDubs, this doesn't work
	 *
	 * By our definition, this means the student performed both a
	 * patient interview and a patient exam.
	 *
	 * @return boolean
	 */
	public function get_performed_comprehensive_assessment() {
		return true;
		$interview = false;
		$exam = false;
		// Search all skills for this run, looking for an interview and exam
		$blsskills = ModelFactory::getBLSSkillsByRun($this->get_id());
		foreach ($blsskills as $skill) {
			if ($skill->get_skill() == BLSSkill::PATIENT_INTERVIEW_CODE && $skill->get_performed()) {
				$interview = true;
			} else if ($skill->get_skill() == BLSSkill::PATIENT_EXAM_CODE && $skill->get_performed()) {
				$exam = true;
			}
		}
		return ($interview && $exam);
	}

	public function get_interview() {
		$blsskills = ModelFactory::getBLSSkillsByRun($this->get_id());
		foreach ($blsskills as $skill) {
			if ($skill->get_skill() == BLSSkill::PATIENT_INTERVIEW_CODE) {
				return $skill->get_performed_by();
			} 
		}
		return -1;
	}

	public function get_exam() {
		$blsskills = ModelFactory::getBLSSkillsByRun($this->get_id());
		foreach ($blsskills as $skill) {
			if ($skill->get_skill() == BLSSkill::PATIENT_EXAM_CODE) {
				return $skill->get_performed_by();
			} 
		}
		return -1;
	}


	public function get_phase_names() {
		$names =  parent::data_table_helper("PhaseTable","Phase_id","PhaseTitle");
		$names[-1] = 'Unset';
		return $names;
	}
	
	public function get_priority_names() {
		$names =  parent::data_table_helper("PriorityTable","Priority_id","PriorityTitle");
		$names[-1] = 'Unset';
		return $names;
	}

	public function get_disposition_names() {
		$names =  parent::data_table_helper("DispositionTable","Disposition_id","DispositionTitle");
		$names[-1] = 'Unset';
		return $names;
	}

	public function get_pulsereturn_names() {
		$names =  parent::data_table_helper("PulseReturnTable","Pulse_id","Pulse");
		$names[-1] = 'Unset';
		return $names;
	}

	public function get_witness_names() {
		$names =  parent::data_table_helper("WitnessTable","Witness_id","Witness");
		$names[-1] = 'Unset';
		return $names;
	}

	public function get_gender_names() {
		$names =  parent::data_table_helper("GenderTable","Gender_id","GenderTitle");
		$names[-1] = 'Unset';
		return $names;
	}

	public function get_ethnicity_names() {
		$names =  parent::data_table_helper("EthnicityTable","Ethnic_id","EthnicTitle");
		$names[-1] = 'Unset';
		return $names;
	}

	public function get_diag_names() {
		$names =  parent::data_table_helper("DiagnosisTable","Diag_id","DiagTitle");
		$names[-1] = 'Unset';
		return $names;
	}

	public function get_loc_names() {
		$names = parent::data_table_helper("LOCTable","LOC_id","LOCName");
		$names[-1] = 'Unset';

		return $names;
	}

	public function get_summary() {
		$g = $this->get_gender_names();
		$d = $this->get_diag_names();
		return $this->age." yo ".$g[$this->gender]." - ".$d[$this->diagnosis];
	}

	public function get_mobile_summary() {
		$g = self::get_gender_names();
		$d = self::get_diag_names();
		$l = self::get_loc_names();
		$disp = self::get_disposition_names();

		$summary = "<div style='font-weight:bold;'>";
		$summary .= "Team Lead: ";

		if ($this->team_leader) {
			$summary .= "me";
		} else {
			$summary .= "other";
		}

		$summary .= "<br>Patient: ".$this->age." yo ".$g[$this->gender]." - ".$d[$this->diagnosis];
		if ($this->diag2 > 0) {
			$summary .= ", ".$d[$this->diag2];
		}
		$summary .= "</div>";

		$other_info = array();
		if ($this->moi > 0) {
			$other_info[] = $d[$this->moi];
		}
		if ($this->disposition > 0) {
			$other_info[] = $disp[$this->disposition];
		}
		if ($this->loc > 0) {
			$other_info[] = $l[$this->loc];
		}

		if (count($other_info) > 0) {
			$summary .= "<div>".implode(', ', $other_info)."</div>";
		}

		return $summary;	
	}

	public function get_long_mobile_summary() {
		$g = self::get_gender_names();
		$d = self::get_diag_names();

		$summary = "<div style='font-weight:bold;'>";
		$summary .= $this->age." yo ".$g[$this->gender]." - ".$d[$this->diagnosis];
		if ($this->diag2 > 0) {
			$summary .= ", ".$d[$this->diag2];
		}
		$summary .= "</div>";

		$other_info = array();
		if ($this->get_interview() == 'Performed') {
			$other_info[] = 'interview';
		}
		if ($this->get_exam() == 'Performed') {
			$other_info[] = 'exam';
		}
		if ($this->team_leader) {
			$other_info[] = 'team lead';
		}

		$ivs = ModelFactory::getIVsByRun($this->get_id());
		$ekgs = ModelFactory::getECGsByRun($this->get_id());
		$airways = ModelFactory::getAirwaysByRun($this->get_id());

		if (count($ivs) > 0) {
			$iv_summary = count($ivs)." IV";
			if (count($ivs) > 1) {
				$iv_summary .= "s";
			}
			$other_info[] = $iv_summary;
		}

		if (count($airways) > 0) {
			$air_summary = count($airways)." ALS airway";
			if (count($airways) > 1) {
				$air_summary .= "s";
			}
			$other_info[] = $air_summary;
		}

		if (count($ekgs) > 0) {
			$ekg_summary = count($ekgs)." ECG";
			if (count($ekgs) > 1) {
				$ekg_summary .= "s";
			}
			$other_info[] = $ekg_summary;
		}

		if (count($other_info) > 0) {
			$summary .= "<div>Performed: ".implode(', ', $other_info)."</div>";	
		}

		return $summary;
	}

	public static function get_hook_ids() {
		$hook_ids = array(1, 2, 23, 24);
		return $hook_ids;
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "run", self::idfield);
	}

	protected function populate_child_models() {
		$this->child_model_helper('precept_id', 'preceptor', 'Preceptor', true);
		//$this->child_model_helper('gender_id', 'gender', 'Gender', true);
		parent::populate_child_models();
	}

	/**
	 * Returns the fieldmap for run objects.
	 *
	 * Combines the list in its parent model (patient) with the extra fields about run info.
	 */
	public function get_fieldmap() {
		$runfields = array(
			//"Run_id" => "id",
			'Shift_id' => 'shift_id',
			'Student_id' => 'student_id',
			'Precept_id' => 'precept_id',
			'NumInTeam' => 'num_in_team',
			'TeamLeader' => 'team_leader',
			'Phase' => 'phase',
			'Priority' => 'priority',
			'Age' => 'age',
			'Gender' => 'gender',
			'Ethnicity' => 'ethnicity',
			'Disposition' => 'disposition',
			'Diagnosis' => 'diagnosis',
			'EntryTime' => 'entry_time',
			'LOC' => 'loc',
			'PulseReturn' => 'pulsereturn',
			'Witness' => 'witness',
			'SystolicBP' => 'systolicbp',
			'DiastolicBP' => 'diastolicbp',
			'Syncopal' => 'syncopal',
			'SelfEval' => 'selfeval',
			'PreceptEval' => 'precepteval',
			'Diag2' => 'diag2',
			'MOI' => 'moi',
			'Months' => 'months',
			'PDARun_id' => 'pdarun_id',
			'SubjectType_id' => 'subjecttype_id'
			);
		//return array_merge(parent::get_fieldmap(), $runfields);
		return $runfields;
	}
}
