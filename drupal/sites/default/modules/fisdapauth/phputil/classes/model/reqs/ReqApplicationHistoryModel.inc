<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/model/AbstractHistoryModel.inc');

/**
 * This class models the req_ApplicationHistory table in the DB.
 */
final class ReqApplicationHistoryModel extends AbstractHistoryModel {
    protected $application_id;

    const DB_TABLE = 'req_ApplicationHistory';

    const APPLICATION_ID_FIELD = 'application_id';

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    /**
     * Retrieve the application ID.
     * @return int A reference into the req_Application table.
     */
    public function get_application_id() {
        return $this->application_id;
    }

    /**
     * Set the requirement application ID.
     * @param int $id A reference into the req_Application table.
     */
    public function set_application_id($id) {
        Assert::is_int($id);
		$this->set_helper('int', $id, 'application_id');
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'ReqApplicationHistory[' . $this->get_id() . ']';
    }

    public function get_fieldmap() {
        $map = array(
            'RequirementApplication_id' => self::APPLICATION_ID_FIELD
        );

        return array_merge($map, parent::get_fieldmap());
    }

    /**
     * Retrieve all the history entries.
     * @return array A list of objects sorted on entry time ascending. 
     */
    public static function get_all_entries() {
        return parent::do_get_all_entries(__CLASS__);
    }

	/**
	 * Retrive all the history entries by ID.
	 * @param int $id The ID.
     * @return array A list of objects sorted on entry time ascending. 
	 */
	public static function get_all_by_application_id($id) {
		Assert::is_int($id);
		
        return parent::do_get_all_by_id(__CLASS__, self::APPLICATION_ID_FIELD, $id);
	}
}
?>
