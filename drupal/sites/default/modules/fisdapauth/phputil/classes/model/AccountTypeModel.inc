<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/classes/model/EnumeratedModel.inc');
require_once('phputil/classes/model/AbstractModel.inc');

/**
 * This class models the reqs_Status table.
 */
final class AccountTypeModel extends AbstractModel implements EnumeratedModel {
    const DB_TABLE = 'AccountTypeTable';
    const ID_COLUMN = 'AccountType_id';

    protected $alpha_code;
    protected $description;
    protected $label;
    protected $short_description;

    const ALPHA_CODE_FIELD = 'alpha_code';
    const DESCRIPTION_FIELD = 'description';
    const ID_FIELD = 'id';
    const LABEL_FIELD = 'label';
    const SHORT_DESCRIPTION_FIELD = 'short_description';

    // These are for use when using the account type as a bit mask.
    // We need these because the ID field is auto incremented, thus
    // we can't depend on it.
    //
    // Developers: YOU MUST UPDATED get_all_ids() and get_all_by_ids() if you modified this list!
    const ID_EMT_BASIC = 1;
    const ID_EMT_INTERMEDIATE = 2;
    const ID_MEDICAL_ASSIST = 4;
    const ID_PARAMEDIC = 8;
    const ID_PHLEBOTOMY = 16;
    const ID_SURG_TECH = 32;

    private static $ids = array(
        self::ID_EMT_BASIC,
        self::ID_EMT_INTERMEDIATE,
        self::ID_MEDICAL_ASSIST,
        self::ID_PARAMEDIC,
        self::ID_PHLEBOTOMY,
        self::ID_SURG_TECH
    );

    private static $all_items_by_ids;
    private static $caching = true;

    /**
     * Enable/disable caching.
     * @param boolean $enable TRUE to enable caching.
     */
    public static function set_caching($enable) {
        Assert::is_boolean($enable);
        self::$caching = $enable;
        self::$all_items_by_ids = null;
    }

    /**
     * Load all the account types.
     * @return array The AccountTypeModel indexed by ID_.
     */
    public static function get_all_by_ids() {
        if (self::$caching) {
            if (!is_null(self::$all_items_by_ids)) return self::$all_items_by_ids;
        }

        $criteria = new ModelCriteria(new AccountTypeModel());

        $connection = FISDAPDatabaseConnection::get_instance();
        $account_types = $connection->get_by_criteria($criteria);

        $list = array();
        foreach ($account_types as $account_type) {
            if ($account_type->is_emt_basic()) {
                $list[self::ID_EMT_BASIC] = $account_type;
            }
            elseif ($account_type->is_emt_intermediate()) {
                $list[self::ID_EMT_INTERMEDIATE] = $account_type;
            }
            elseif ($account_type->is_medical_assist()) {
                $list[self::ID_MEDICAL_ASSIST] = $account_type;
            }
            elseif ($account_type->is_paramedic()) {
                $list[self::ID_PARAMEDIC] = $account_type;
            }
            elseif ($account_type->is_phlebotomy()) {
                $list[self::ID_PHLEBOTOMY] = $account_type;
            }
            elseif ($account_type->is_surg_tech()) {
                $list[self::ID_SURG_TECH] = $account_type;
            }
        }

        self::$all_items_by_ids = $list;
        return $list;
    }

    /**
     * Load all the account types matching a mask.
     * @return array The AccountTypeModel objects indexed 0-n.
     */
    public static function get_all_by_mask($mask) {
        Assert::is_int($mask);

        $account_types = self::get_all_by_ids();

        $list = array();
        foreach ($account_types as $id=>$account_type) {
            if ($account_type->is_emt_basic()) {
                if (self::include_emt_basic($mask)) {
                    $list[] = $account_type;
                }
            }
            elseif ($account_type->is_emt_intermediate()) {
                if (self::include_emt_intermediate($mask)) {
                    $list[] = $account_type;
                }
            }
            elseif ($account_type->is_medical_assist()) {
                if (self::include_medical_assist($mask)) {
                    $list[] = $account_type;
                }
            }
            elseif ($account_type->is_paramedic()) {
                if (self::include_paramedic($mask)) {
                    $list[] = $account_type;
                }
            }
            elseif ($account_type->is_phlebotomy()) {
                if (self::include_phlebotomy($mask)) {
                    $list[] = $account_type;
                }
            }
            elseif ($account_type->is_surg_tech()) {
                if (self::include_surg_tech($mask)) {
                    $list[] = $account_type;
                }
            }
        }

        return $list;
    }

    /**
     * Retrieve all the known IDs.
     * @return array The known IDs.
     */
    public static function get_all_ids() {
        return self::$ids;
    }

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        $this->construct_helper($data, self::DB_TABLE, self::ID_COLUMN);
    }

    private function is_type($type) {
        return !strcasecmp($type, $this->get_label());
    }

    private static function should_include($mask, $id) {
        Assert::is_int($mask);
        Assert::is_int($id);

        return !!($mask & $id);
    }

    /**
     * Retrieve the ID from an account type string.
     * Examples: emt-b, paramedic
     * @param string | null $account_type The account type, case insensitive.
     * @return int | null The ID or null if not found.
     */
    public static function get_id_from_account_type_string($account_type) {
        Assert::is_true(
            Test::is_null($account_type) || Test::is_string($account_type));

        if (is_null($account_type)) return null;

        $account_type = strtolower(trim($account_type));
        $account_types = self::get_all_by_ids();

        foreach ($account_types as $id=>$type) {
            if ($type->get_label() == $account_type) return $id;
        }

        return null;
    }

    /**
     * Retrieve the mask from the account type strings.
     * Invalid strings are silently ignored.
     * @param array $account_types The account types, case insensitive.
     * @return int The mask.
     */
    public static function get_mask_from_account_type_strings($account_types) {
        Assert::is_array($account_types);

        $mask = 0;
        foreach ($account_types as $account_type) {
            $id = self::get_id_from_account_type_string($account_type);
            if (!is_null($id)) {
                $mask |= $id;
            }
        }

        return $mask;
    }

    /**
     * Determine if this is a EMT-B.
     * @return boolean TRUE if this is a EMT-B.
     */
    public function is_emt_basic() {
        return $this->is_type('emt-b');
    }

    /**
     * Determine if EMT-B should be included.
     * @param int $mask The bit mask to test.
     * @return boolean TRUE if it should be included.
     */
    public static function include_emt_basic($mask) {
        return self::should_include($mask, self::ID_EMT_BASIC);
    }

    /**
     * Determine if this is a EMT-I.
     * @return boolean TRUE if this is a EMT-I.
     */
    public function is_emt_intermediate() {
        return $this->is_type('emt-i');
    }

    /**
     * Determine if EMT-I should be included.
     * @param int $mask The bit mask to test.
     * @return boolean TRUE if it should be included.
     */
    public static function include_emt_intermediate($mask) {
        return self::should_include($mask, self::ID_EMT_INTERMEDIATE);
    }

    /**
     * Determine if this is a paramedic.
     * @return boolean TRUE if this is a paramedic.
     */
    public function is_paramedic() {
        return $this->is_type('paramedic');
    }

    /**
     * Determine if paramedics should be included.
     * @param int $mask The bit mask to test.
     * @return boolean TRUE if it should be included.
     */
    public static function include_paramedic($mask) {
        return self::should_include($mask, self::ID_PARAMEDIC);
    }

    /**
     * Determine if this is a surgical tech.
     * @return boolean TRUE if this is a surgical tech.
     */
    public function is_surg_tech() {
        return $this->is_type('surg-tech');
    }

    /**
     * Determine if surgical tech should be included.
     * @param int $mask The bit mask to test.
     * @return boolean TRUE if it should be included.
     */
    public static function include_surg_tech($mask) {
        return self::should_include($mask, self::ID_SURG_TECH);
    }

    /**
     * Determine if this is phlebotomy.
     * @return boolean TRUE if this is phlebotomy.
     */
    public function is_phlebotomy() {
        return $this->is_type('phlebotomy');
    }

    /**
     * Determine if phlebotomy should be included.
     * @param int $mask The bit mask to test.
     * @return boolean TRUE if it should be included.
     */
    public static function include_phlebotomy($mask) {
        return self::should_include($mask, self::ID_PHLEBOTOMY);
    }

    /**
     * Determine if this is a medical assistant.
     * @return boolean TRUE if this is a medical assistant.
     */
    public function is_medical_assist() {
        return $this->is_type('med-assist');
    }

    /**
     * Determine if medical assistants should be included.
     * @param int $mask The bit mask to test.
     * @return boolean TRUE if it should be included.
     */
    public static function include_medical_assist($mask) {
        return self::should_include($mask, self::ID_MEDICAL_ASSIST);
    }

    /**
     * Retrieve the label.
     * @return string The label.
     */
    public function get_label() {
        return $this->label;
    }

    /**
     * Set the label.
     * @param string $label The label.
     */
    public function set_label($label) {
        Assert::is_not_empty_trimmed_string($label);

        $this->set_helper('string', trim($label), 'label');
    }

    /**
     * Retrieve the alpha code.
     * @return string The alpha code.
     */
    public function get_alpha_code() {
        return $this->alpha_code;
    }

    /**
     * Set the alpha code.
     * @param string $alpha code The alpha code.
     */
    public function set_alpha_code($alpha_code) {
        Assert::is_not_empty_trimmed_string($alpha_code);

        $this->set_helper('string', trim($alpha_code), 'alpha_code');
    }

    /**
     * Retrieve the text describing the entry.
     * @return string The text.
     */
    public function get_text() {
        return $this->get_short_description();
    }

    /**
     * Set the text describing the entry.
     * @param string $text The text.
     */
    public function set_text($text) {
        $this->set_short_description($text);
    }

    /**
     * Retrieve the ordering position.
     * Smaller numbers should appear first to the user.
     * @return int The position, at least 1.
     */
    public function get_position() {
        return $this->get_id();
    }

    /**
     * Set the position.
     * Smaller numbers should appear first to the user.
     * @param int $postiion The position, at least 1.
     */
    public function set_position($position) {
        // We can't change the position (ID).
    }

    /**
     * Our parent allows this, but since the ID field is auto increment we 
     * don't.
     * @param string $id The ID.
     */
    public function set_id($id) {
        // We can't change the position (ID).
    }

    /**
     * Retrieve the description.
     * @return string The description.
     */
    public function get_description() {
        return $this->description;
    }

    /**
     * Set the description.
     * @param string $description The description.
     */
    public function set_description($description) {
        Assert::is_not_empty_trimmed_string($description);

        $this->set_helper('string', trim($description), 'description');
    }

    /**
     * Retrieve the short description.
     * @return string The short description.
     */
    public function get_short_description() {
        return $this->short_description;
    }

    /**
     * Set the short description.
     * @param string $short_description The short description.
     */
    public function set_short_description($short_description) {
        Assert::is_not_empty_trimmed_string($short_description);

        $this->set_helper('string', trim($short_description), 'short_description');
    }

    /**
     * Retrieve information to create prompts.
     * @return array A list of req_RequiredModel sorted by ascending position.
     */
    public static function get_prompt_info() {
        $criteria = new ModelCriteria(new AccountTypeModel());
        $criteria->add_order_by(new OrderBy(self::SHORT_DESCRIPTION_FIELD, true));

        $connection = FISDAPDatabaseConnection::get_instance();
        return $connection->get_by_criteria($criteria);
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, self::ID_COLUMN);
    }

    public function get_summary() {
        return 'AccountType[' . $this->get_id() . ']';
    }

    public static function get_special_ids() {
        // The ID column is auto increment, we better not rely on its value.
        return array();
    }

    public function get_fieldmap() {
        return array(
            'AccountType_desc' => self::DESCRIPTION_FIELD,
            'AccountType_label' => self::LABEL_FIELD,
            'AccountType_shortdesc' => self::SHORT_DESCRIPTION_FIELD,
            'AlphaCode' => self::ALPHA_CODE_FIELD
        );
    }
}
?>
