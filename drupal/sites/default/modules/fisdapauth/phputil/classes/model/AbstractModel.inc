<?php
/*----------------------------------------------------------------------*
  |                                                                      |
  |     Copyright (C) 1996-2006.  This is an unpublished work of         |
  |                      Headwaters Software, Inc.                       |
  |                         ALL RIGHTS RESERVED                          |
  |     This program is a trade secret of Headwaters Software, Inc.      |
  |     and it is not to be copied, distributed, reproduced, published,  |
  |     or adapted without prior authorization                           |
  |     of Headwaters Software, Inc.                                     |
  |                                                                      |
*----------------------------------------------------------------------*/
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/common_utilities.inc');
require_once('phputil/classes/Convert.inc');
require_once('phputil/classes/DateTime.inc');
require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/classes/model/Model.inc');
require_once('phputil/classes/model/ModelCriteria.inc');
require_once('phputil/classes/model/OrderBy.inc');
require_once('phputil/exceptions/FisdapDatabaseException.inc');
require_once('phputil/exceptions/FisdapDatabaseStatementException.inc');
require_once('phputil/exceptions/FisdapRuntimeException.inc');
require_once('phputil/exceptions/FisdapModelException.inc');

/**
 * An abstract class from which all models should inherit.
 *
 * Provides the following methods:
 *  - public reload() reloads the data (by the ID).
 *  - protected remove_helper()
 *  - protected save_helper()
 *  - protected id_or_string_helper($input, $function)
 *  - protected set_helper($type, $data, $field)
 *  - protected construct_helper($id, $table, $idfield)
 *  - protected sql_helper($result, $objname, $id_field)
 *  - protected data_table_helper($table, $idcolumn, $datacolumn, $orderby=null)
 *  - As well as a public method to get_ any field (so name them intelligently) via the magic function __call(). 
 *
 * Classes inheriting from this AbstractModel work best if they interface with one main table.
 *
 * All setters should use set_helper so future modifications are easier.
 *
 * Consider including factory classes in the ModelFactory (model_factory.inc). 
 * The getBySQL method makes them wicked easy to write.
 *
 * Fieldmaps are arrays of the format "db table column name" => "object field name"
 *
 * If you want to use the save_helper, you also need to be using the set_helper. 
 * If you want to use save_helper, construct_helper, or remove_helper, you have to 
 * have an accurate fieldmap.
 *
 * @author eoneil
 * @author iyoung
 */
abstract class AbstractModel implements Model {

	/**
	 * The id of the object we're modeling.
	 * @var int
	 */
	private $id;

	/**
	 * Which model fields have been changed since instantiation/the last save.
	 * @var array
	 */
	protected $changed_fields = array();

	/**
	 * Stores information about the child models.
	 * @var array
	 */
	protected $child_models = array();

	/**
	 * @todo this should be private, but we're using it in MySqlStatementBuilder
	 * as a temporary patch
	 */
	public $db_idfield;
	private $db_table;

	/**
	 * @todo do we still want this?
	 */
	abstract public function get_summary();
	/**
	 * @return array an array mapping columns from the database to property names.
	 */
	abstract public function get_fieldmap();

	/**
	 * Retrieve the ID.
	 * @return int The object ID.
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * Clone the object.
	 *
	 * This will create a new copy of the object with all the same values, but a 
	 * different identity. Only a shallow clone is done.
	 */
	public function __clone() {
		$this->id = null;
		// We want to mark every field as changed so that they all get inserted
		$this->changed_fields = array_values($this->get_fieldmap());
	}

	/**
	 * Determine if the model exists in the DB.
	 * @return boolean TRUE if the ID is set and &gt;=0.
	 * @todo what is this function needed for? currently being used by ReqUtils::verify_exists 
	 * and ReqUtils::verifiy_does_not_exist. Not sure if it's actually USEFUL.
	 */
	public function exists() {
		if (is_null($this->id)) return false;
		return ($this->id >= 0);
	}

	/**
	 * Retrieve the DB table name.
	 * @return string The table name.
	 * @todo what if db_table isn't defined?
	 */
	public function get_table_name() {
		return $this->db_table;
	}

	/**
	 * Retrieve the ID just inserted.
	 * This method gets by the situation where the ID field is NOT auto 
	 * incremented.
	 * @param FISDAPDatabaseConnection $connection The connection used to insert.
	 * @return int The ID.
	 * @todo is this needed? isn't it taken care of by get_id()?
	 */
	protected function get_insert_id($connection) {
		return $connection->get_last_insert_id();
	}

	/** 
	 * Catches all calls to "get_<field>" and returns the value in $this->field,
	 * as long as that field is set.
	 *
	 * If child models exist, will also fall back to looking for a magically parsed
	 * getter method in those (see {@link child_model_helper()});
	 */
	public function __call($function, $params) {
		$method = get_class($this) . "::$function()";

		if (substr($function, 0, 4) == 'get_') {
			// If an appropriate field exists, return it
			$field = substr($function, 4);
			$class = new ReflectionClass(get_class($this));
			if ($class->hasProperty($field)) {
				$property = new ReflectionProperty(get_class($this), $field);
				if ($property->isProtected() || $property->isPublic()) {
					return $this->$field;
				}
			}

			// Otherwise try looking for child model methods
			if ($ret = $this->parse_child_method_call($function, $params)) {
				return $ret;
			}
			// Give up and error out
			throw new FisdapRuntimeException("Trying to get an invalid property: $method");
		} else if (substr($function, 0, 4) == 'set_') {
			$fieldlist = $this->get_settable_fields();
			$field = substr($function, 4);
			if (is_array($fieldlist) && array_key_exists($field, $fieldlist)) {
				$type = $fieldlist[$field];
				return $this->set_helper($type, $params[0], $field);
			} else if (($ret = $this->parse_child_method_call($function, $params)) !== false) {
				// See if this should be sent to a child model method
				return $ret;
			} else {
				throw new FisdapRuntimeException("Not allowed to set property: $method");
			}
		} else {
			// Bad function, complain
			throw new FisdapRuntimeException("Call to invalid function: $method"); 
		}
	}

	/**
	 * Check for methods in child models
	 *
	 * Try parsing a function call for magic conversion to a method call on
	 * a child of this model.
	 * @see child_model_helper()
	 * @see __call()
	 * @return mixed|false the return value of the method if one is found, false otherwise
	 */
	private function parse_child_method_call($function, $params) {
		foreach ($this->child_models as $idfield => $arr) {
			$name = $arr['name'];
			// If this child is read-only, only search for getters
			if ($arr['ro']) {
				$prefix = "(get_)";
			} else {
				// Otherwise search for getters or setters
				$prefix = "(get_|set_)";
			}
			// Try to match our function name to this pattern
			$re = "/^${prefix}${name}_(.+)$/";
			$match = array();
			if (preg_match($re, $function, $match) === 1) {
				$prefix = $match[1];
				$property = $match[2];
				$method = "$prefix$property";
				// If this method in fact exists, return with it, otherwise keep looking
				if (is_callable(array($this->$name, $method))) {
					return call_user_func_array(array($this->$name, $method), $params);
				}
			}
		}
		// We've run out of children, quit in failure
		return false;
	}

	private function get_exception_text($operation) {
		if (is_null($this->id)) {
			$id = '';
		} else {
			$id = $this->id;
		}

		return 'AbstractModel[' . get_class($this) . "($id)]: " . $operation;
	}

	/**
	 * Removes the record represented by $this->id from $this->db_table. 
	 * @throws FisdapDatabaseException if an error occurs.
	 */
	protected function remove_helper() {
		// If the id isn't set, this object isn't in the DB, so ignore it.
		if (is_null($this->id)) return;

		$statement = "DELETE FROM $this->db_table WHERE $this->db_idfield='$this->id' LIMIT 1";
		$connection = FISDAPDatabaseConnection::get_instance();
		if ($connection->purge($statement) == 1) return;

		throw new FisdapDatabaseStatementException(
			$this->get_exception_text('unable to remove'), '', $statement); 
	}

	/**
	 * Saves the current object to $this->db_table in the database. The object must have
	 * a fieldmap correctly defined in get_fieldmap and changes must have been 
	 * saved to $this->changed_fields (done automatically if the set_helper() 
	 * was used).
	 *
	 * @return int The object id.
	 * @throws FisdapDatabaseException if an error occurs.
	 */
	protected function save_helper() {
		// Are we putting a new entry in the database?
		$insert = ($this->get_id() <= 0); 

		// First save any child models
		foreach ($this->child_models as $idfield => $array) {
			// Don't save if access is read-only
			if ($array['ro']) continue;

			$name = $array['name'];
			$this->$name->save();
			// set the id field for our child
			$this->$idfield = $this->$name->get_id();
		}

		// Determine the column names and values.
		$columns = array();
		$field_map = $this->get_fieldmap();
		foreach ($field_map as $db => $obj) {
			if ($obj == 'id') continue;

			if (in_array($obj, $this->changed_fields)) {
				if ($this->$obj instanceof SqlValue) {
					$columns[$db] = $this->$obj->get_as_sql_value(); 
				} else {
					$columns[$db] = $this->$obj;
				}
			}
		}

		$connection = FISDAPDatabaseConnection::get_instance();
		$builder = $connection->get_statement_builder();

		if ($insert) {
		
			$statement = $builder->get_array_insert_statement(
				$this->db_table, $columns); 

			if ($connection->insert($statement) != 1) {
				throw new FisdapDatabaseStatementException(
					$this->get_exception_text('unable to save'), '', $statement); 
			}

			$this->id = $this->get_insert_id($connection);
		} else if (count($this->changed_fields)) {
			$statement = $builder->get_array_update_statement(
				$this->db_table, $columns) .
				" WHERE $this->db_idfield = " . $this->get_id() .  ' LIMIT 1';

			// It's okay to have no affected rows.
			$connection->update($statement);
		}

		$this->changed_fields = array();
		return $this->id;
	}

	/**
	 * Determines whether the $input is a string or an int.
	 *
	 * Used to allow setter
	 * functions to take either an id or the string that the id represents.
	 * Compares the input to the return value of $function to ensure that it is
	 * valid, then returns the id that corresponds to the $input or null if the
	 * value is invalid.
	 *
	 * This function is case-insensitive.
	 */
	protected function id_or_string_helper($input, $function) {
		if (!method_exists($this, $function)) {
			throw new FisdapRuntimeException("$function is not a valid method in " . 
				get_class($this) . '.');
		}
		$things = $this->$function();
		if (is_numeric($input)) {
			if (array_key_exists($input, $things)) {
				return $input;
			}
		} else if (is_string($input)) {
			foreach ($things as $id => $thing) {
				if (strcasecmp($input, $thing) === 0) {
					return $id;
				}
			}
		}
		return null;
	}

	/**
	 * Checks $data according to the rules for $type.
	 *
	 * Saves $data to $this->$field if it passes.
	 * Also does some background work necessary for save_helper.
	 */
	protected function set_helper($type, $data, $field) {
		Assert::is_not_null($type);

		// If the field is already set to $data, return early.
		if ($this->$field === $data) {
			return;
		}

		// Do the type checking.
		// If the type is an array, figure out what we have to do special. 
		if (is_array($type)) {
			switch ($type[0]) {
			case 'object':
				Assert::is_a($type[1], $type[0]);
				break;
			}
		} else {
			// Otherwise, check it against the specified type. 
			switch ($type) {
			case 'float':
				$data = Convert::to_float($data);
				break;

			case 'int':
				$data = Convert::to_int($data);
				break;

			case 'string':
				$data = Convert::to_string($data);
				break;

			case 'bool':
			case 'boolean':
				$data = Convert::to_boolean($data);
				break;

			case 'time':
				$time = Convert::to_a($data, 'FisdapTime');
				$data = $time->get_MySQL_time();
				break;

			case 'date':
				$date_obj = Convert::to_a($data, 'FisdapDate');
				$data = $date_obj->get_MySQL_date();
				break;

			case 'datetime':
				$datetime_obj = Convert::to_a($data, 'FisdapDateTime');
				$data = $datetime_obj->get_MySQL_date_time();
				break;

			default:
				throw new FisdapInvalidArgumentException(
					"Unknown data type[$type]");
			}
		}

		// Store the data and update the array
		$this->$field = $data;
		if (!in_array($field, $this->changed_fields)) {
			$this->changed_fields[] = $field;
		}

		// If this field affects a child model, instantiate that model
		if (array_key_exists($field, $this->child_models)) {
			$array = $this->child_models[$field];
			$classname = $array['class'];
			$name = $array['name'];
			$object = new $classname($this->$field);
			$this->$name = $object;
		}
		return true;

	}

	/**
	 * Helper function for constructing a new model
	 *
	 * @see construct_by_id
	 * @see construct_by_sql
	 * @param int|array either an integer id or an SQL result row array
	 * @param string $table the name of the database table
	 * @param string $idfield the name of the column to serve as an id
	 * @throws ReflectionException
	 */
	protected function construct_helper($param, $table, $idfield) { 
		$this->db_table = $table;
		$this->db_idfield = $idfield;

		if (in_array('id', $this->get_fieldmap()) !== false) {
			throw new FisdapModelException('Field map must not contain a class property named "id"');
		}
		if (array_key_exists($this->db_idfield, $this->get_fieldmap()) !== false) {
			//TODO This is super jankety - we are ignoring Enum models for now because they
			// are doing weird things with ids, to be fixed eventually
			if (!($this instanceof AbstractEnumeratedModel)) {
				throw new FisdapModelException('Field map must not contain the id field');
			}
		}

		if (is_array($param)) {
			// This is a mysql result row, process it as such
			$return = $this->construct_by_sql($param, $table, $idfield);
		} else if ($param !== null) {
			// Hopefully it's an int id
			$return = $this->construct_by_id($param, $table, $idfield);
		} else {
			$return = null;
		}

		// Build any child models
		$this->child_models = array();
		$this->populate_child_models();
		foreach ($this->child_models as $idfield => &$array) {
			$classname = $array['class'];
			$name = $array['name'];
			$object = new $classname($this->$idfield);
			$this->$name = $object;
		}

		return $return;
	}

	/**
	 * Reload the data in this model (by the ID).
	 * @todo what is this function needed for?
	 */
	public final function reload() {
		$this->construct_helper($this->get_id(), $this->db_table, $this->db_idfield);
	}

	/**
	 * Construct an object using an existing id
	 *
	 * Gets data for $id from $table and stores it in this object's fields.
	 *
	 * @param int $id the object id. Must exist in the database
	 */
	private function construct_by_id($id, $table, $idfield) {
		Assert::is_int($id);

		$connection = FISDAPDatabaseConnection::get_instance();
		$query = "SELECT * FROM $table where $idfield=$id";
		$result = $connection->query($query);

		if (count($result) != 1) { 
			throw new FisdapInvalidArgumentException("ID {$idfield}[$id] does not exist");  
		} 

		$this->id = $id;
		$field_map = $this->get_fieldmap(); 
		foreach ($field_map as $db => $obj) { 
			$this->$obj = $result[0][$db]; 
		} 
	}

	/**
	 * Returns objects based on the the values in a mysql result row.
	 *
	 * Ideal for
	 * creating objects from a query because it will not hit the database unless
	 * the $row contains insufficient info.
	 *
	 * @param array $row an SQL result row array
	 */
	private function construct_by_sql($row, $table, $idfield) {
		$this->id = $row[$idfield];
		$fieldmap = $this->get_fieldmap();
		foreach ($fieldmap as $db => $obj) {
			if (array_key_exists($db, $row)) {
				$this->$obj = $row[$db];
			} else {
				// We don't have enough information to create this model without going to the DB
				if (!is_null($row[$idfield])) {
					$this->construct_by_id($row[$idfield], $table, $idfield);
				}

				break;
			}
		}
		return true;
	}

	/**
	 * Populate child models
	 *
	 * This method is called during the construction process of a model. It may
	 * be overridden to allow easy definition of child models (generally it will
	 * be just a series of calls to {@link child_model_helper()}.
	 */
	protected function populate_child_models() {
		return;
	}

	/**
	 * Set up a 'child' model.
	 *
	 * This can be used to automatically handle the processing of models
	 * that the current model wants to control.  Once set within a model definition,
	 * the children will be constructed with or without an id when the parent is
	 * constructed, and saved when the parent is saved. Getter and setter calls
	 * to the parent will be automagically delegated to the
	 * children when appropriate. 
	 *
	 * <b>Stipulation:</b> If you use this method in your model, you should also
	 * use {@link truconstruct_helper()} for instantiation and {@link set_helper()}
	 * to set any of the ids that relate to child models.  But of course you're
	 * using those helpers anyways, right...?
	 *
	 * For example, we can create a shift model and retrieve the name of the
	 * ambulance service:
	 * <code>
	 * $shift = new Shift($id);
	 * $name = $shift->get_ambulance_service_name();
	 * </code>
	 * even though the Shift model does not define a get_ambulance_service_name()
	 * method and does not have an $ambulance_service_name field. This is possible
	 * because the ambulance_service class provides a get_name() method and we
	 * have defined ambulance_service as a child model in ShiftModel.inc:
	 * <code>
	 * public function populate_child_models() {
	 *	$this->child_model_helper('ambulance_service_id', 'ambulance_service', 'AmbulanceService', true);
	 *	$this->child_model_helper('start_base_id', 'base', 'AmbulanceBase', true);
	 * }
	 * </code>
	 *
	 * @param string $idfield the name of the field in this model that stores the id
	 * of the child model.
	 * @param string $name the name of the the child model object.  The instance will
	 * be stored at $this->$name, and calls of the form get_$name_property() or
	 * set_$name_property(...) will be passed to the child model's get_property() and
	 * set_property() methods.
	 * @param string $model_class the name of the child model class to instantiate
	 * @param boolean $readonly if the child model should be available on a read-only basis.
	 * If this is set to true, the setter methods will not be available, and the child
	 * model will not be saved when the parent's save() is called.
	 */
	protected function child_model_helper($idfield, $name, $model_class, $readonly) {
		$this->child_models[$idfield] = array(
			'class' => $model_class, 'name' => $name, 'ro' => $readonly);
	}

	/**
	 * Returns an array of $objects based on the $result.
	 */
	protected static function sql_helper($result, $objname, $id_field) {
		Assert::is_true(is_resource($result) || Test::is_array($result));

		$objects = array();
		if (is_resource($result)) {
			while ($row = mysql_fetch_array($result)) {
				$objects[] = new $objname($row);
			}
		} else if (is_array($result)) {
			foreach ($result as $row) {
				$objects[] = new $objname($row);
			}
		}

		return $objects;
	}

	/**
	 * Returns a key=>value array with $table's values from $idcolumn as keys and values
	 * from $datacolumn as values. Accepts an optional param, $orderby, and orders the 
	 * output by the column of that name.
	 * 
	 * @TODO Confirm that this function can indeed be static
	 */
	protected static function data_table_helper($table, $idcolumn, $datacolumn, $orderby=null) {
		$connection = FISDAPDatabaseConnection::get_instance();
		$query = "SELECT * FROM $table";
		if (!is_null($orderby)) {
			$query .= " ORDER BY $orderby";
		}
		$result = $connection->query($query);
		if (count($result) < 1) {
			// @TODO error handling
			// Something went wrong, so we'll just send back an empty array
			return array();
		}
		$array = array();
		foreach ($result as $row) {
			$array[$row[$idcolumn]] = $row[$datacolumn];
		}

		return $array;
	}

	/**
	 * Returns an array of fields in this model that may be altered by magically
	 * parsed convenience functions.  This is an optional feature - simply override
	 * this method to use it.
	 *
	 * Example:
	 * <code>
	 * public function get_settable_fields() {
	 *	return array('contact_name' => 'string',
	 *		'postal_code' => 'int');
	 * }
	 * </code>
	 * Our model now provides the methods set_contact_name($name) and
	 * set_postal_code($code), and will call {@link set_helper()} with the 
	 * corresponding type check.
	 *
	 * @return array An associative array mapping field names => types
	 */
	public function get_settable_fields() {
		return array();
	}

	/**
	 * Save or update the model.
	 * @return int The model ID.
	 * @throws FisdapDatabaseException if an error occurs.
	 */
	public function save() {
		return $this->save_helper();
	}

	/**
	 * Remove the mode from the database.
	 * @throws FisdapDatabaseException if an error occurs.
	 */
	public function remove() {
		$this->remove_helper();
	}

}
?>
