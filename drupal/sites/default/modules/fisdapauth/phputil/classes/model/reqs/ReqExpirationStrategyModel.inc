<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/model/AbstractEnumeratedModel.inc');

/**
 * This class models the reqs_ExpirationStrategy table.
 */
final class ReqExpirationStrategyModel extends AbstractEnumeratedModel {
    const DB_TABLE = 'req_ExpirationStrategy';

    const ID_EXPIRATION_STRATEGY = 1;
    const ID_EXPIRATION_STRATEGY_NONE = 2;
    const ID_EXPIRATION_STRATEGY_BIND_TO_END_OF_MONTH = 3;
    const ID_EXPIRATION_STRATEGY_BIND_TO_END_OF_YEAR = 4;

    protected $class_name;

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    /**
     * Retrieve the PHP class name.
     * @return string The PHP class name, i.e. ExpirationStrategy.inc.
     */
    public function get_class_name() {
        return $this->class_name;
    }

    /**
     * Set the PHP class name.
     * @param string $class_name The PHP class name, i.e. ExpirationStrategy.inc.
     */
    public function set_class_name($class_name) {
        Assert::is_not_empty_trimmed_string($class_name);

        $this->set_helper('string', trim($class_name), 'class_name');
    }

    /**
     * Retrieve information to create prompts.
     * @return array A list of req_RequiredModel sorted by ascending position.
     */
    public static function get_prompt_info() {
        return self::do_get_prompt_info(__CLASS__);
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'ReqExpirationStrategy[' . $this->get_id() . ']';
    }

    public static function get_special_ids() {
        return array(
            'ID_EXPIRATION_STRATEGY' => self::ID_EXPIRATION_STRATEGY,
            'ID_EXPIRATION_STRATEGY_NONE' => self::ID_EXPIRATION_STRATEGY_NONE,
            'ID_EXPIRATION_STRATEGY_BIND_TO_END_OF_MONTH' => 
                self::ID_EXPIRATION_STRATEGY_BIND_TO_END_OF_MONTH,
            'ID_EXPIRATION_STRATEGY_BIND_TO_END_OF_YEAR' => 
                self::ID_EXPIRATION_STRATEGY_BIND_TO_END_OF_YEAR
        );
    }

    public function get_fieldmap() {
        $map = array(
            'ClassName' => 'class_name'
        );

        return array_merge(parent::get_fieldmap(), $map);
    }
}
?>
