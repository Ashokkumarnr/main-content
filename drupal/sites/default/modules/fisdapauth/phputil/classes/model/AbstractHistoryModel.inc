<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/DateTime.inc');
require_once('phputil/classes/model/AbstractModel.inc');

/**
 * This class models an history entry in the DB.
 */
abstract class AbstractHistoryModel extends AbstractModel {
    protected $entry_time;
    protected $description;
    protected $user_id;

    const ID_COLUMN = 'id';

    const DESCRIPTION_FIELD = 'description';
    const ENTRY_TIME_FIELD = 'entry_time';
    const ID_FIELD = 'id';
    const USER_ID_FIELD = 'user_id';

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     * @param string $table_name The table name.
     */
    public function __construct($data, $table_name) {
        Assert::is_true(is_array($data) || Test::is_null($data) || Test::is_int($data));
        Assert::is_not_empty_string($table_name);

        $this->construct_helper($data, $table_name, self::ID_COLUMN);
    }

    /**
     * Retrieve the date / time of the entry.
     * @return FisdapDateTime The date / time.
     */
    public function get_entry_time() {
        return FisdapDateTime::create_from_sql_string($this->entry_time);
    }

    /**
     * Set the date / time of the entry.
     * @param FisdapDateTime $date_time The date / time.
     */
    public function set_entry_time(FisdapDateTime $date_time) {
        $this->set_helper('datetime', $date_time, 'entry_time');
    }

    /**
     * Retrieve the description.
     * @return string The description.
     */
    public function get_description() {
        return $this->description;
    }

    /**
     * Set the description.
     * @param string $description The description.
     */
    public function set_description($description) {
        Assert::is_not_empty_trimmed_string($description);

        $this->set_helper('string', trim($description), 'description');
    }

    /**
     * Retrieve the user ID.
     * @return int A reference to the UserAuthData table.
     */
    public function get_user_id() {
        return $this->user_id;
    }

    /**
     * Set the user ID.
     * @return int $id A reference to the UserAuthData table.
     */
    public function set_user_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'user_id');
    }

    /**
     * Retrieve all the history entries.
     * @param string $class_name The class name.
     * @return array A list of all the models sorted by ascending entry time.
     */
    protected static function do_get_all_entries($class_name) {
        Assert::is_not_empty_string($class_name);

        $model = new $class_name(null);
        $criteria = new ModelCriteria($model);

        $criteria->add_order_by(new OrderBy(self::ENTRY_TIME_FIELD, true));
        $connection = FISDAPDatabaseConnection::get_instance();
        return $connection->get_by_criteria($criteria);
    }

    /**
     * Retrieve all the history entries by ID.
     * @param string $class_name The class name.
	 * @param string $field_name The field name.
	 * @param int $id The ID to search on.
     * @return array A list of all the models sorted by ascending entry time.
     */
    protected static function do_get_all_by_id($class_name, $field_name, $id) {
        Assert::is_not_empty_string($class_name);
        Assert::is_not_empty_string($field_name);
        Assert::is_int($id);

		$field = "set_$field_name";

        $model = new $class_name(null);
        $criteria = new ModelCriteria($model);
		$criteria->$field($id);

        $criteria->add_order_by(new OrderBy(self::ENTRY_TIME_FIELD, true));
        $connection = FISDAPDatabaseConnection::get_instance();
        return $connection->get_by_criteria($criteria);
    }

    public function get_fieldmap() {
        return array(
            'EntryTime' => self::ENTRY_TIME_FIELD,
            'HistoryDesc' => self::DESCRIPTION_FIELD,
            'UserAuth_id' => self::USER_ID_FIELD
        );
    }
}
?>
