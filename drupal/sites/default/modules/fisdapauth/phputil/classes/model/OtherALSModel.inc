<?php

/* Done? */

require_once("DataModel.inc");

class OtherALS extends DataModel {

	/*********
	 * FIELDS
	 *********/
	protected $other_skill;
	const dbtable = "OtherALSData";
	const id_field = "OtherALS_id";

	/**************
	 * CONSTRUCTOR
	 **************/

	public function __construct($id=null) {
		parent::__construct();
		if (!$this->construct_helper($id, self::dbtable, self::id_field)) {
			return false;
		}
	}

	/************
	 * FUNCTIONS
	 ************/

	public function set_other_skill($skill) {
		$id = $this->id_or_string_helper($skill, "get_other_als_names");
		if ($id) {
			$this->set_helper("int", $id, "other_skill");
			return true;
		}
		return false;
	}

	public function get_summary() {
		$skills = self::get_other_als_names();
		$s = $skills[$this->other_skill];
		return $s;
	}

	public function get_mobile_summary() {
		$skills = self::get_other_als_names();

		$summary = "<div style='font-weight:bold;'>";
		
		$summary .= $skills[$this->other_skill]." ";

		if ($this->get_performed_by() == 0) {
			$summary .= "(Performed)";
		} else if ($this->get_performed_by() == 1) {
			$summary .= "(Observed)";
		}
		$summary .= "</div>";

		return $summary;
	}

	public static function get_other_als_names() {
		return self::data_table_helper("OtherALSSkillsTable", "OtherALSSkills_id", "OtherALSSkillsTitle", "OtherALSSkillsTitle");
	}

	public function get_hook_ids() {
		return array();
	}

	/*
	 * FUNCTION getBySQL
	 *
	 * Creates Med objects based on a MySQL result set so the database doesn't need to be hit more than once.
	 *
	 * @param result A result set containing on or more complete MedData rows. The rows should be complete MedData records, or we have to hit the DB anyway, which completely and totally defeats the purpose. 
	 * @pre The result set CANNOT use alias names.
	 * @return array An array of Meds.
	 * @TODO untested
	 * @TODO can we move this to the parent class and use constants for the class-specific info (ID field name)?
	 */
	public static function getBySQL($result) {
		return parent::sql_helper($result, "otherals", self::id_field);
	}

	public function get_fieldmap() {
		return array("Run_id" => "run_id",
				"Student_id" => "student_id",
				"PerformedBy" => "performed_by",
				"OtherSkill" => "other_skill",
				"Shift_id" => "shift_id",
				"SubjectType_id" => "subject_type",
				"Assessment_id"=>"assessment_id",
			);
	}

}

?>
