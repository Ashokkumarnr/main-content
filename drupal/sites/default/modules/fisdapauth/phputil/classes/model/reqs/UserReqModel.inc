<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/model/AbstractModel.inc');
require_once('phputil/classes/DateTime.inc');

/**
 * This class models the req_UserRequirement table.
 */
final class UserReqModel extends AbstractModel {
    const DB_TABLE = 'req_UserRequirement';
    const ID_COLUMN = 'id';

    protected $application_id;
    protected $completed_date;
    protected $deleted;
    protected $due_date;
    protected $expiration_date;
    protected $req_id;
    protected $shift_id;
    protected $user_id;

    const APPLICATION_ID_FIELD = 'application_id';
    const COMPLETED_DATE_FIELD = 'completed_date';
    const DUE_DATE_FIELD = 'due_date';
    const DELETED_FIELD = 'deleted';
    const EXPIRATION_DATE_FIELD = 'expiration_date';
    const REQ_ID_FIELD = 'req_id';
    const SHIFT_ID_FIELD = 'shift_id';
    const USER_ID_FIELD = 'user_id';

	/**
	 * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
	 */
	public function __construct($data=null) {
        Assert::is_true(is_array($data) || Test::is_null($data) || Test::is_int($data));

        $this->construct_helper($data, self::DB_TABLE, self::ID_COLUMN);
    }

	/**
	 * Retrieve the application ID.
	 * @return int | null A reference to the req_Application table.
	 */
	public function get_application_id() {
        return $this->application_id;
    }

	/**
	 * Set the application ID.
	 * @param int $id A reference to the req_Application table.
	 */
	public function set_application_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'application_id');
    }

	/**
	 * Retrieve the requirement ID.
	 * @return int | null A reference to the req_Reqirement table.
	 */
	public function get_req_id() {
        return $this->req_id;
    }

	/**
	 * Set the requirement ID.
	 * @param int $id A reference to the req_Reqirement table.
	 */
	public function set_req_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'req_id');
    }

    /**
     * Retrieve the user ID.
     * @return int A reference to the UserAuthData table.
     */
    public function get_user_id() {
        return $this->user_id;
    }

    /**
     * Set the user ID.
     * @return int $id A reference to the UserAuthData table.
     */
    public function set_user_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'user_id');
    }

    /**
     * Retrieve the shift ID.
     * @return int | null A reference to the ShiftData table.
     */
    public function get_shift_id() {
        return $this->shift_id;
    }

    /**
     * Set the shift ID.
     * @return int | null $id A reference to the ShiftData table.
     */
    public function set_shift_id($id) {
        Assert::is_true(Test::is_int($id) || Test::is_null($id));

        $this->set_helper('int', $id, 'shift_id');
    }

	/**
	 * Determine if the user requirement has been deleted.
	 * @return boolean | null TRUE if deleted.
	 */
	public function is_deleted() {
        return ($this->deleted != 0);
    }

	/**
	 * Determine if the user requirement has been deleted.
	 * @return int | null The deleted, 0 for not, otherwise deleted.
	 */
	public function get_deleted() {
        return $this->deleted;
    }

	/**
	 * Indicate whether the user requirement is deleted.
	 * @param boolean $deleted TRUE if deleted.
	 */
	public function set_deleted($deleted) {
        Assert::is_boolean($deleted);

        $this->set_helper('bool', $deleted, 'deleted');
    }

    /**
     * Retrieve the due date.
     * @return FisdapDateTime The date / time.
     */
    public function get_due_date() {
        return FisdapDateTime::create_from_sql_string($this->due_date);
    }

    /**
     * Set the due date.
     * @param FisdapDateTime $date_time The date / time.
     */
    public function set_due_date(FisdapDateTime $date_time) {
        $this->set_helper('datetime', $date_time, 'due_date');
    }

    /**
     * Determine if the requirement has expired.
     * If the requirement has NOT been completed, this returns FALSE.
	 * If the expiration date is not set, this returns FALSE.
     * @return boolean TRUE if the requirement has expired.
     */
    public function is_expired() {
        if (!$this->is_completed()) return false;

        // Equivalent of null?
        $exp_date = $this->get_expiration_date();
        if (!$exp_date->is_set()) return false;

        // It is valid until the end of the day.
        return $exp_date->compare(FisdapDate::today()) < 0;
    }

    /**
     * Determine if the requirement is overdue.
     * If the requirement has been completed, this returns FALSE.
     * If the due date is not specified, this returns FALSE.
     * @return boolean TRUE if the requirement has expired.
     */
    public function is_over_due() {
        if ($this->is_completed()) return false;

        // Equivalent of null?
        $due_date = $this->get_due_date();
        if (!$due_date->is_set()) return false;

        return $due_date->compare(FisdapDateTime::now()) <= 0;
    }

    /**
     * Retrieve the expiration date.
     * @return FisdapDate The date.
     */
    public function get_expiration_date() {
        return FisdapDate::create_from_sql_string($this->expiration_date);
    }

    /**
     * Set the expiration date.
     * @param FisdapDate $date The date.
     */
    public function set_expiration_date(FisdapDate $date) {
        $this->set_helper('date', $date, 'expiration_date');
    }

    /**
     * Determine if the requirement has been completed.
     * @return boolean TRUE if the requirement has been completed.
     */
    public function is_completed() {
		return $this->get_completed_date()->is_set();
    }

    /**
     * Retrieve the completed date.
     * @return FisdapDateTime The date / time.
     */
    public function get_completed_date() {
		return FisdapDateTime::create_from_sql_string($this->completed_date);
    }

    /**
     * Set the completed date.
     * @param FisdapDateTime $date_time The date / time.
     */
    public function set_completed_date(FisdapDateTime $date_time) {
        $this->set_helper('datetime', $date_time, 'completed_date');
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, self::ID_COLUMN);
    }

    public function get_summary() {
        return 'UserReq[' . $this->get_id() . ']';
    }

    public function get_fieldmap() {
        return array(
            'Application_id' => self::APPLICATION_ID_FIELD,
            'CompletedDate' => self::COMPLETED_DATE_FIELD,
            'Deleted' => self::DELETED_FIELD,
            'DueDate' => self::DUE_DATE_FIELD,
            'ExpirationDate' => self::EXPIRATION_DATE_FIELD,
            'Requirement_id' => self::REQ_ID_FIELD,
            'Shift_id' => self::SHIFT_ID_FIELD,
            'UserAuth_id' => self::USER_ID_FIELD
        );
    }
}
?>
