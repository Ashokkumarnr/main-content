<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FisdapCache.inc');

/**
 * Manages all caches.
 */
final class FisdapCaches {
	private $caches = array();

	private function __construct() {
	}

	public static function get_instance() {
		static $instance;

		if (is_null($instance)) {
			$instance = new FisdapCaches();
		}

		return $instance;
	}

	/**
	 * Register a cache.
	 * @param FisdapCache $cache The cache to register.
	 */
	public function register(FisdapCache $cache) {
		$this->caches[$cache->get_name()] = $cache;
	}

	/**
	 * Clear the contents of all caches.
	 */
	public function clear() {
		foreach ($this->caches as $cache) {
			$cache->clear();
		}
	}

	/**
	 * Indicate whether the caches are enabled or not.
	 * @param boolean $enabled TRUE if enabled.
	 */
	public function set_enabled($enabled) {
		Assert::is_boolean($enabled);

		foreach ($this->caches as $cache) {
			$cache->set_enabled($enabled);
		}
	}

	public function __toString() {
		$output = array();
		foreach ($this->caches as $cache) {
			$output[] = $cache->__toString();
		}

		return join("\n", $output);
	}
}
?>
