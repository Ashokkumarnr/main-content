<?php
require_once('phputil/classes/mail/AbstractMailTransport.inc');
require_once('phputil/Zend/library/Zend/Exception.php');
require_once('phputil/Zend/library/Zend/Mail.php');
require_once('phputil/Zend/library/Zend/Mail/Transport/Sendmail.php');

/**
 * Handle messages via Zend.
 */
final class ZendTransport extends AbstractMailTransport {
    public function __construct() {
        // If we don't do this, Zend can't find a file it wants
        Zend_Mail::setDefaultTransport(new Zend_Mail_Transport_Sendmail());
    }

    private static function create_zend_mail(MailMessage $message) {
        $mail = new Zend_Mail();

        $mail->setFrom(
            $message->get_from()->get_email(),
            $message->get_from()->get_name());

        $emails = $message->get_reply_to();
        if (count($emails)) {
            $mail->addHeader('Reply-To', self::get_list_as_string($emails));
        }

        $emails = $message->get_bcc();
        foreach ($emails as $email) {
            $mail->addBcc($email->get_email(), $email->get_name());
        }

        $emails = $message->get_cc();
        foreach ($emails as $email) {
            $mail->addCc($email->get_email(), $email->get_name());
        }

        $emails = $message->get_to();
        foreach ($emails as $email) {
            $mail->addTo($email->get_email(), $email->get_name());
        }

        $mail->setSubject($message->get_subject());

        if ($message->get_text_body() != '') {
            $mail->setBodyText($message->get_text_body()); 
        }

        if ($message->get_html_body() != '') {
            $mail->setBodyHtml($message->get_html_body()); 
        }

        return $mail;
    }

    public function do_handle(MailMessage $message) {
        $mail = self::create_zend_mail($message);

        try {
            $mail->send();
            $sent = true;
        }
        catch (Zend_Exception $e) {
            $sent = false;
        }

        return $sent;
    }

    private static function get_list_as_string($list) {
        $parts = array();

        foreach ($list as $address) {
            $parts[] = self::get_email_as_string($address);
        }

        return join(',', $parts);
    }

    private static function get_email_as_string(EmailAddress $address) {
        $name = $address->get_name();

        if (is_null($name)) {
            return $address->get_email();
        }

        $name = addslashes($name);
        if ($name != $address->get_name()) {
            $name = '"' . $name . '"';
        }

        return $name . ' <' . $address->get_email() . '>';
    }
}
?>
