<?php
require_once('phputil/classes/mail/MailMessage.inc');

/**
 * Handle a mail message.
 */
interface MailTransport {
    /**
     * Handle the message.
     * The message has not yet been verified.
     * @param MailMessage $message The message.
     */
    public function handle(MailMessage $message); 
}
?>
