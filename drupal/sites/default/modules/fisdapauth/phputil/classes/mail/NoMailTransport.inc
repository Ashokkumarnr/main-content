<?php
require_once('phputil/classes/mail/AbstractMailTransport.inc');

/**
 * A transport that does nothing.
 */
final class NoMailTransport extends AbstractMailTransport {
    private $handled = array();
    private $errors = array();

    public function do_handle(MailMessage $message) {
        $this->handled[] = $message;
        return true;
    }

    public function do_handle_error(MailMessage $message, $errors) {
        $this->errors[] = array($message, $errors);
    }

    /**
     * Retrieve the handled e-mails.
     * @return array An array of MailMessage.
     */
    public function get_handled() {
        return $this->handled;
    }

    /**
     * Retrieve the errors handled.
     * @return array An array of [MailMessage, error-messages[]].
     */
    public function get_unhandled() {
        return $this->errors;
    }

    public function clear_stats() {
        $this->handled = array();
        $this->errors = array();
        parent::clear_stats();
    }
}
?>
