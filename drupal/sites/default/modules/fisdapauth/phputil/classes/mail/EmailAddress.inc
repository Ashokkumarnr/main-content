<?php
require_once('phputil/classes/Assert.inc');

/**
 * A container for an e-mail address.
 */
final class EmailAddress {
    private $email;
    private $name;

    /**
     * Factory method to create an address.
     * Will NOT throw an exceptions.
     * @param string $email The e-mail address.
     * @param string | null The name associated with the address.
     * @return EmailAddress | null The address or null if it can't be created.
     */
    public static function create($email, $name=null) {
        if (!self::is_valid($email)) return null;

        $email = strtolower(trim($email));

        if (is_string($name)) {
            $name = trim($name);

            if ($name == '') {
                $name = null;
            }
        }
        elseif (!is_null($name)) {
            return null;
        }

        return new EmailAddress($email, $name);
    }

    /**
     * Constructor.
     * @param string $email The e-mail address.
     * @param string | null The name associated with the address.
     */
    public function __construct($email, $name=null) {
        Assert::is_true(self::is_valid($email));
        Assert::is_true(Test::is_string($name) || Test::is_null($name));

        $this->email = strtolower(trim($email));

        if (!is_null($name)) {
            $this->name = trim($name);

            if ($this->name == '') {
                $this->name = null;
            }
        }
    }

    /**
     * Retrieve the e-mail address.
     * @return string The e-mail address.
     */
    public function get_email() {
        return $this->email;
    }

    /**
     * Retrieve the name.
     * @return string | null The name or null if not specified.
     */
    public function get_name() {
        return $this->name;
    }

    /**
     * Determine if an e-mail address is valid.
     * @param mixed $email The address.
     * @return TRUE if the address is valid.
     */
    public static function is_valid($email) {
        if (!is_string($email)) return false;

        $email = trim($email);

        // This code was taken from webtoolkit.info/php-validate-email.html
        // This is NOT a complete validator, it misses some characters and 
        // quoted strings...There is a complete validator on google and other
        // sites but they come with licenses.
        $valid = eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$", 
            $email);
        return $valid;
    }

    public function __toString() {
        $s = get_class($this) . '[' . $this->get_email();

        if (!is_null($this->get_name())) {
            $s .= ',' . $this->get_name();
        }

        $s .= ']';

        return $s;
    }

    /**
     * Compare to another address.
     * @param mixed $address An e-mail address.
     * @return boolean TRUE if they are the same.
     */
    public function equals($address) {
        if (!($address instanceof EmailAddress)) return false;
        if ($this->get_email() != $address->get_email()) return false;

        if (is_null($this->get_name())) {
            return is_null($address->get_name());
        }

        if (is_null($address->get_name())) {
            return is_null($this->get_name());
        }

        return ($this->get_name() == $address->get_name());
    }

    /**
     * Compare to another address ignoring the name.
     * @param mixed $address An e-mail address.
     * @return boolean TRUE if they are the same.
     */
    public function equalsIgnoreName($address) {
        if (!($address instanceof EmailAddress)) return false;
        return ($this->get_email() == $address->get_email()); 
    }

    /**
     * Retrieve the address of the robot.
     * @return EmailAddress The robot address.
     */
    public static function robot() {
        return new EmailAddress('fisdap-robot@fisdap.net', 'FISDAP Robot');
    }
}
?>
