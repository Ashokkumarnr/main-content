<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/Convert.inc');
require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/classes/mail/EmailAddress.inc');
require_once('phputil/classes/model/programData.inc');
require_once('phputil/get_insts.html');

/**
 * A container for a mail message.
 */
final class MailMessage {
    private $bcc = array();     // of EmailAddress
    private $cc = array();      // of EmailAddress
    private $from;
    private $html_body = '';
    private $reply_to = array();
    private $signature; 
    private $subject = '';
    private $text_body = '';
    private $to = array();      // of EmailAddress

    /**
     * Clear the reply-to.
     */
    public function clear_reply_to() {
        $this->reply_to = array();
    }

    /**
     * Remove a reply-to.
     * @param EmailAddress | array $reply_to The recipient(s).
     */
    public function remove_reply_to($reply_to) {
        $this->reply_to = self::remove_from_list($this->reply_to, $reply_to);
    }

    /**
     * Add a reply-to.
     * @param EmailAddress | array $reply_to The recipient(s).
     */
    public function add_reply_to($reply_to) {
        $this->reply_to = self::add_to_list($this->reply_to, $reply_to);
    }

    /**
     * Retrieve the reply-to.
     * @return array The reply-to as EmailAddress.
     */
    public function get_reply_to() {
        return $this->reply_to;
    }

    /**
     * Clear the Bcc.
     */
    public function clear_bcc() {
        $this->bcc = array();
    }

    /**
     * Remove a Bcc.
     * @param EmailAddress | array $bcc The recipient(s).
     */
    public function remove_bcc($bcc) {
        $this->bcc = self::remove_from_list($this->bcc, $bcc);
    }

    /**
     * Add a Bcc.
     * @param EmailAddress | array $bcc The recipient(s).
     */
    public function add_bcc($bcc) {
        $this->bcc = self::add_to_list($this->bcc, $bcc);
    }

    /**
     * Retrieve the Bcc.
     * @return array The Bcc's as EmailAddress.
     */
    public function get_bcc() {
        return $this->bcc;
    }

    /**
     * Clear the Cc.
     */
    public function clear_cc() {
        $this->cc = array();
    }

    /**
     * Remove a Cc.
     * @param EmailAddress | array $cc The recipient(s).
     */
    public function remove_cc($cc) {
        $this->cc = self::remove_from_list($this->cc, $cc);
    }

    /**
     * Add a Cc.
     * @param EmailAddress | array $cc The recipient(s).
     */
    public function add_cc($cc) {
        $this->cc = self::add_to_list($this->cc, $cc);
    }

    /**
     * Retrieve the Cc.
     * @return array The Cc's as EmailAddress.
     */
    public function get_cc() {
        return $this->cc;
    }

    /**
     * Add a recipient.
     * @param EmailAddress | array $to The recipient(s).
     */
    public function add_to($to) {
        $this->to = self::add_to_list($this->to, $to);
    }

    /**
     * Clear the To.
     */
    public function clear_to() {
        $this->to = array();
    }

    /**
     * Remove a To.
     * @param EmailAddress | array $to The recipient(s).
     */
    public function remove_to($to) {
        $this->to = self::remove_from_list($this->to, $to);
    }

    /**
     * Retrieve the recipients.
     * @return array The receipients as EmailAddress.
     */
    public function get_to() {
        return $this->to;
    }

    /**
     * Retrieve the subject.
     * @return string The subject.
     */
    public function get_subject() {
        return $this->subject;
    }

    /**
     * Set the subject.
     * @param string $subject The subject.
     */
    public function set_subject($subject) {
        Assert::is_not_empty_trimmed_string($subject);

        $this->subject = trim($subject);
    }

    /**
     * Retrieve the body of a HTML message.
     * Includes any signature.
     * @return string The body text.
     */
    public function get_html_body() {
        return $this->html_body . $this->get_signature();
    }

    /**
     * Set the body of a HTML message.
     * @param string $body The body text.
     */
    public function set_html_body($body) {
        Assert::is_string($body);

        $this->html_body = $body;
    }

    /**
     * Retrieve the body of a plain text message.
     * Includes any signature.
     * @return string The body text.
     */
    public function get_text_body() {
        return $this->text_body . $this->get_signature();
    }

    /**
     * Set the body of a plain text message.
     * @param string $body The body text.
     */
    public function set_text_body($body) {
        Assert::is_string($body);

        $this->text_body = $body;
    }

    /**
     * Retrieve the sender.
     * @return EmailAddress | null The sender or null is non set.
     */
    public function get_from() {
        return $this->from;
    }

    /**
     * Set the sender.
     * @param EmailAddress $from The sender.
     */
    public function set_from($from) {
        Assert::is_a($from, 'EmailAddress');

        $this->from = $from;
    }

    /**
     * Validate the message.
     * @return array The errors, may be zero length.
     */
    public function validate() {
        $errors = array();

        if (is_null($this->get_from())) {
            $errors[] = 'No from';
        }

        if (!count($this->get_to())) {
            $errors[] = 'No reciipients';
        }

        if (trim($this->get_subject()) == '') {
            $errors[] = 'No subject';
        }

        if ((trim($this->get_html_body()) == '') &&
            (trim($this->get_text_body()) == '')) {
                $errors[] = 'No body';
        }

        return $errors;
    }

    /**
     * Indicate the message came from a robot.
     */
    public function sign_from_robot() {
        $this->signature = self::robot_signature();
    }

    private static function robot_signature() {
        return 'This email was sent by the FISDAP Robot.' .
            '  Please do not reply to this email as the mailbox is unattended.';
    }

    /**
     * Indicate the message came from the scheduler.
     * @param int $student_id The student ID.
     * @param int $program_id The program ID.
     */
    public function sign_from_scheduler($student_id, $program_id) {
        Assert::is_int($student_id);
        Assert::is_int($program_id);

        if ($student_id > 0) {
            $connection = FISDAPDatabaseConnection::get_instance();
            $ids = getInstructors($student_id, $program_id, $connection->get_link_resource());

            $list = array();
            foreach ($ids as $id) {
            {
                $instructor = new common_instructor($id);
                if (!$instructor->is_valid()) continue;

                $data = $instructor->get_instructordata();
                if ($data['EmailEventFlag'] != 1) continue;

                $list[] = $instructor->get_fullname() . ' at ' .
                    $instructor->get_emailaddress();
                sort($list);
		    }

            $this->signature = self::robot_signature() . 
                "  If you have questions, please contact any of the following people:\n\n" .
                join("\n", $list);
            }
        }
        else {
            $program = new ProgramData($program_id);

            $this->signature = 'Please note that this is an automatic' .
                ' email message generated by the FISDAP Scheduler and that you' .
                ' are receiving this message as part of your use of the FISDAP system.' .
                '  If you have any questions regarding this message please contact an' .
                ' instructor for the program "' . $program->get_name() . '."';
        }
    }

    /**
     * Add recipients to a list.
     * @param array $list The current list.
     * @param array | EmailAddress $add The receipients to add.
     * @return array The new list.
     */
    private static function add_to_list($list, $add) {
        Assert::is_true(
            Test::is_a($add, 'EmailAddress') ||
            Test::is_array($add));

        $add = Convert::to_array($add);

        foreach ($add as $email) {
            if (is_null($email)) continue;
            if (!($email instanceof EmailAddress)) continue;

            // Only add the address once.
            $exists = false;
            foreach ($list as $item) {
                if ($item->equalsIgnoreName($email)) {
                    $exists = true;
                    break;
                }
            }

            if ($exists) continue;

            $list[] = $email;
        }

        return $list;
    }

    /**
     * Remove recipients from a list.
     * @param array $list The current list.
     * @param array | EmailAddress $remove The receipients to remove.
     * @return array The new list.
     */
    private static function remove_from_list($list, $remove) {
        Assert::is_true(
            Test::is_a($remove, 'EmailAddress') ||
            Test::is_array($remove));

        $remove = Convert::to_array($remove);

        foreach ($remove as $email) {
            if (is_null($email)) continue;
            if (!($email instanceof EmailAddress)) continue;

            for ($i = 0; $i < count($list); ) {
                if ($list[$i]->equalsIgnoreName($email)) {
                    array_splice($list, $i, 1);
                }
                else {
                    ++$i;
                }
            }
        }

        return $list;
    }

    private function get_signature() {
        if (is_null($this->signature)) return '';
        return "\n\n$this->signature";
    }
}
?>
