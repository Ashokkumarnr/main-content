<?php
require_once('phputil/classes/mail/MailMessage.inc');
require_once('phputil/classes/mail/MailTransport.inc');

/**
 * Handle a mail message.
 * This class provides statistics support.
 */
abstract class AbstractMailTransport implements MailTransport {
    private $error_count = 0;
    private $send_failed_count = 0;
    private $sent_count = 0;
    private $total_count = 0;

    /**
     * Handle the message.
     * The message is valid.
     * @param MailMessage $message The message.
     * @return boolean TRUE if the message was sent.
     */
    public abstract function do_handle(MailMessage $message); 

    /**
     * Handle the message.
     * The message is invalid.
     * @param MailMessage $message The message.
     * @param array $errors The errors.
     */
    public function do_handle_error(MailMessage $message, $errors) {
        $s = join(', ', $errors);
        error_log($s);
    }

    /**
     * Clear the statistics.
     */
    public function clear_stats() {
        $this->error_count = 0;
        $this->send_failed_count = 0;
        $this->sent_count = 0;
        $this->total_count = 0;
    }

    public function __toString() {
        return get_class($this) . 
            '[total=' . $this->total_count . 
            ',sent=' . $this->sent_count .
            ',send_failed=' . $this->send_failed_count .
            ',errors=' . $this->error_count .
            ']';
    }

    /**
     * Handle the message.
     * The message has not yet been verified.
     * @param MailMessage $message The message.
     */
    public final function handle(MailMessage $message) {
        ++$this->total_count;
        $errors = $message->validate();

        if (count($errors)) {
            ++$this->error_count;
            $this->do_handle_error($message, $errors);
        }
        else {
            
            if ($this->do_handle($message, $errors)) {
                ++$this->sent_count;
            }
            else {
                ++$this->send_failed_count;
            }
        }
    }

    /**
     * Retrieve the total number of messages handled.
     * This does include errors.
     * @return int The count.
     */
    public final function get_total_count() {
        return $this->total_count;
    }

    /**
     * Retrieve the number of mail message errors handled.
     * @return int The count.
     */
    public final function get_error_count() {
        return $this->error_count;
    }

    /**
     * Retrieve the number of messages sent.
     * Includes messages that were error free.
     * @return int The count.
     */
    public final function get_sent_count() {
        return $this->sent_count;
    }

    /**
     * Retrieve the number of messages that the transport failed to send.
     * Includes messages that were error free.
     * @return int The count.
     */
    public final function get_send_failed_count() {
        return $this->send_failed_count;
    }
}
?>
