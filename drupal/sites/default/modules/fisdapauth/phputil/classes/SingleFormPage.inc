<?php
/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2007.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/browser_action.inc');
require_once('phputil/classes/common_dispatch.inc');
require_once('phputil/classes/common_form.inc');
require_once('phputil/classes/common_page.inc');
require_once('phputil/classes/MessageArea.inc');
require_once('phputil/classes/FISDAPDatabaseConnection.php');

/**
 * A class that handles a single form with a submit button and an optional
 * cancel button.
 * 
 * As long as you follow the template, there isn't much to do to set up a page.
 *
 * You must provide:
 * - create_form()
 * - process_cancel() or set_cancel_url() if you use a CANCEL button
 * - process_delete() is you use the DELETE button.
 * - process_submit()
 * - set_ok_url() // if you want to move away from the page on success
 *
 * Optional overrides:
 * - display_above_form()
 * - display_below_form()
 *
 * Common usage:
 * $page = new Page();      // your page extends this one
 * $page->set_cancel_url(...);
 * $page->set_ok_url(...);
 * $page->set_site_msg(...);
 * $page->init();
 * $page->dispatch();
 *
 * If you wish to add your own dispatching, use:
 * $dispatcher = $page->get_dispatcher();
 * $displatch->add_student_picker_handlers();   // if using the picker
 * ...
 * $dispatcher->dispatch();     

 * If you wish to make use of a message area:
 * $page->set_message_area(...);
 *
 * If your own callbacks create a form:
 * $page->set_form(...) 
 *
 * If your page uses 1 form at a time but the form contents change based on
 * state, i.e. you have to look at the form name.
 * $page->set_constant_form_name(false);
 *
 * If you are logging anything:
 * $page->set_debug(true);
 * $page->set_log_callback(...);
 *
 */
abstract class SingleFormPage {
    private $cancel_url;
    private $constant_form_name = true;
    private $debug = false;
    private $log_callback = 'devlog';
    private $message_area;
    private $ok_url;
    private $page;
    private $site_msg;

    const FORM_DIV_NAME = 'myform_div';
    const FORM_NAME = 'myform';

    /**
     * Create the form.
     * Attributes are as follows:
     * <ul>
     * <li>first_display: boolean Indicates whether this is the first time 
     * the page has been displayed.
     * <li>student_picker_search_view: optional If present, indicates the picker should 
     * enter search mode.
     * <li>student_picker_selected_view: optional If present, indicates the picker should 
     * enter selected mode.
     * </ul>
     * @param array $attributes The associative attributes.
     * @return common_form The form.
     */
    protected abstract function create_form($attributes);

    /**
     * Process a delete button.
     * If you return TRUE, you should be redirecting yourself.
     * @return boolean TRUE if the operation was successful.
     */
    protected function process_delete() {
        throw new FisdapRuntimeException(
            'Delete is not yet implemented -- override process_delete()' .
            ' or use set_cancel_url()');
    }

    /**
     * Process the cancel button.
     * @return boolean TRUE if the operation was successful.
     */
    protected function process_cancel() {
        if (is_null($this->cancel_url)) {
            $this->message_area->add_internal_error_msg(
                'Cancel is not yet implemented -- override process_cancel()' .
                ' or use set_cancel_url()');
            return false;
        }

        send_browser_action(new RedirectBrowserAction($this->cancel_url));
        return true;
    }

    /**
     * Process the submit button.
     * @return TRUE if the operation was successful.
     */
    protected function process_submit() {
        $this->message_area->add_internal_error_msg(
            'Submit is not yet implemented -- override process_submit()');
        return false;
    }

    /**
     * Provide the actual content above the form.
     */
    protected function display_above_form() {}

    /**
     * Provide the actual content below the form.
     */
    protected function display_below_form() {}

    /**
     * Constructor.
     * @param common_page $page The page to use.
     */
    public function __construct($page) {
        $this->page = $page;
        $this->message_area = new MessageArea();
    }

    /**
     * Set the URL to go to after a cancel operation.
     * @param string $url The URL.
     */
    public final function set_cancel_url($url) {
        $this->cancel_url = $url;
    }

    /**
     * Set the URL to go to after an OK operation.
     * @param string $url The URL.
     */
    public final function set_ok_url($url) {
        $this->ok_url = $url;
    }

    /**
     * Set a message to appear at the top of the page.
     * @param string|null $msg The message.
     */
    public final function set_site_msg($msg) {
        $this->site_msg = $msg;
    }

    /**
     * Indicate whether the debug mode is on.
     * @param boolean $on TRUE if debug mode is on.
     */
    public final function set_debug($on) {
        $this->debug = $on;
    }

    /**
     * Determine whether debug is on.
     * @return boolean TRUE if debug mode is on.
     */
    public final function is_debug() {
        return $this->debug;
    }

    /**
     * Indicate that the form name changes.
     * @param boolean $constant TRUE if the form name is constant.
     */
    public final function set_constant_form_name($constant) {
        $this->constant_form_name = $constant;
    }

    /**
     * Initialize the page.
     */
    public function init() {
    }

    /**
     * Retrieve a dispatcher set up for this form.
     * @return common_dispatch $dispatcher The dispatcher you can modify.
     */
    public final function get_dispatcher() {
        $dispatcher = new common_dispatch();
        $dispatcher->set_default_dispatch_id('default');
        $dispatcher->register('default', 'function', array($this, 'process_default'));

        if ($this->constant_form_name) {
            $dispatcher->register(self::FORM_NAME, 'function', 
                array($this, 'process_form'));
        }

        return $dispatcher;
    }

    /**
     * Add on handlers for the student picker.
     */
    public final function add_student_picker_handlers($dispatcher) {
        $dispatcher->register(DomainPromptStudentsPicker::SEARCH_VIEW_DISPATCH_ID, 
            'function', array($this, 'process_view_search'));
        $dispatcher->register(DomainPromptStudentsPicker::SELECTED_VIEW_DISPATCH_ID, 
            'function', array($this, 'process_view_selected'));
    }

    /**
     * Dispatch based on the user action.
     */
    public final function dispatch() {
        $this->get_dispatcher()->dispatch();
    }

    /**
     * Debug function used to log an associative array.
     * @param array $kvs The keyword/value array.
     */
    public final function log_kvs($kvs) {
        foreach ($kvs as $key=>$value) {
            $this->log("KeyValue $key: $value");
        }
    }

    /**
     * Set the form in use.
     * @param common_form $form The form to use.
     */
    public final function set_form($form) {
        Assert::is_a($form, 'common_form_base');

        if ($this->constant_form_name) {
            $form->set_name(self::FORM_NAME);
        }
    }

    /**
     * Log something.
     * @param string $msg The message to log.
     */
    public final function log($msg) {
        if (!$this->debug) return;
        if (!is_callable($this->log_callback)) return;

        call_user_func($this->log_callback, $msg);
    } 

    /**
     * Set the function for logging.
     * @param callback $callback The logging callback.
     * This function takes a single argument, the string to log.
     */
    public final function set_log_callback($callback) {
        $this->log_callback = $callback;
    }

    /**
     * Log an array one entry per line.
     * @param array $list The array to log.
     */
    public final function log_array($list, $prefix='') {
        foreach ($list as $key=>$value) {
            $msg = "{$prefix}[$key]: " . print_r($value, true);
            $this->log($msg);
        }
    }

    /**
     * Retrieve the message area.
     * @return MessageArea The message area.
     */
    public final function get_message_area() {
        return $this->message_area;
    }

    /**
     * Add a simple message area.
     */
    public final function use_message_area() {
        $message_area = new MessageArea();
        $message_area->set_error_msg_params('errormsgs', 0);
        $message_area->set_status_msg_params('statusmsgs', 0);
        $this->set_message_area($message_area);
    }

    /**
     * Make use of a message area.
     * @param MessageArea|null $message_area The message area.
     */
    public final function set_message_area($message_area) {
        Assert::is_true(
            Test::is_null($message_area) || 
            Test::is_a($message_area, 'MessageArea'));

        if (is_null($message_area)) {
            // Create a stub to make the code cleaner.
            $this->message_area = new MessageArea();
        }
        else {
            $this->message_area = $message_area;
        }
    }

    /**
     * Display the main page.
     */
    protected function display_form($above_or_below) {
        if ($above_or_below == 'above') {
            $this->page->display_header();
            $this->page->display_pagetitle();
            $this->do_display_above_form();
            $this->display_above_form();
        }
        else {
            $this->display_below_form();
            $this->page->display_footer();
        }
    }

    /**
     * Display above the form.
     */
    private function do_display_above_form() {
        echo $this->message_area->get_html();

        if (!is_null($this->site_msg)) {
?>
<div style="text-align: center; font-size: 30px; color: red">
<?php echo $this->site_msg; ?>
</div>
<?php   
        } 
    }

    /**
     * Handle the initial call.
     * @param boolean $clear_msg_area TRUE if the message area should be 
     * cleared.
     */
    public function process_default($clear_msg_area=true) {
        if ($clear_msg_area) {
            $this->message_area->clear_all();
        }

        $attributes = $this->get_attributes();
        $attributes['first_display'] = true;
        $form = $this->create_form($attributes);

        $this->set_form($form);
        $form->process();

        $this->display_form('above');

        $html = $form->get_generated_html_form();
        if (!is_null($html)) {
            echo '<div id="';
            echo self::FORM_DIV_NAME;
            echo '">';
            echo $html;
            echo '</div>';
        }

        $this->display_form('below');
    }

    /**
     * Something has been pressed or changed on the form.
     */
    public function process_form() {
        $this->message_area->clear_all();

        $attributes = $this->get_attributes();
        $form = $this->create_form($attributes);
        $this->set_form($form);

        // Cancelled?
        $button = $form->get_pressed_button();
        $cancelled = !is_null($button) && 
            ($button->get_type() == common_form_button::CANCEL);
        if ($cancelled) {
            if ($this->process_cancel()) return;
        }

        // Submitted?
        $submitted = !is_null($button) &&
            ($button->get_type() == common_form_button::SUBMIT);

        // Deleted?
        $deleted = !is_null($button) &&
            ($button->get_type() == common_form_button::DELETE);

        $validated = $form->process();
        if ($validated && $submitted) {
            if ($this->process_submit()) {
                if (!is_null($this->ok_url)) {
                    send_browser_action(new RedirectBrowserAction($this->ok_url));
                    return;
                }
            }
        }

        if ($validated && $deleted) {
            if ($this->process_delete()) return;
        }

        if (!is_null($button)) {
            // We need to display the form here as in update 'ajax' mode
            // the page is not expected to be returned to.
            if ($form->get_submission_technique() == 'ajax') {
                $form->display(false);
                $html = $form->get_generated_html_form();
                if (!is_null($html)) {
                    send_browser_action(
                        new InnerHtmlBrowserAction(self::FORM_DIV_NAME,$html));
                }
            }
            else {
                $this->display_form('above');

                $html = $form->get_generated_html_form();
                if (is_null($html)) {
                    $form->display(false);
                    $html = $form->get_generated_html_form();
                }

                if (!is_null($html)) {
                    echo '<div id="';
                    echo self::FORM_DIV_NAME;
                    echo '">';
                    echo $html;
                    echo '</div>';
                }

                $this->display_form('below');
            }
        }

        $this->message_area->update_msgs();
    }
    
    /**
     * The user has requested to view the search section.
     */
    public final function process_view_search() {
        $this->message_area->clear_all();

        $attributes = $this->get_attributes();
        $attributes['student_picker_search_view'] = true;
        $form = $this->create_form($attributes);

        $this->set_form($form);
        $form->process();

        $html = $form->get_generated_html_form();
        if (!is_null($html)) {
            send_browser_action(
                new InnerHtmlBrowserAction(self::FORM_DIV_NAME,$html));
        }

        $this->message_area->update_msgs();
    }

    /**
     * The user has requested to view the selected section.
     */
    public final function process_view_selected() {
        $this->message_area->clear_all();

        $attributes = $this->get_attributes();
        $attributes['student_picker_selected_view'] = true;
        $form = $this->create_form($attributes);

        $this->set_form($form);
        $form->process();

        $html = $form->get_generated_html_form();
        if (!is_null($html)) {
            send_browser_action(
                new InnerHtmlBrowserAction(self::FORM_DIV_NAME,$html));
        }

        $this->message_area->update_msgs();
    }

    /**
     * Retrieve the default attributes for creating a form.
     * @return array The name/value pairs.
     */
    protected function get_attributes() {
        return array(
            'first_display' => false
        );
    }

    /**
     * Retrieve a page value.
     * @param string $name The field name.
     * @return string | null The value.
     */
    protected static function get_value($name) {
        $value = null;
        if (isset($_REQUEST[$name])) {
            $value =  $_REQUEST[$name];
        }

        if (is_null($value)) {
            $value = common_utilities::get_scriptvalue($name);
        }

        return $value;
    }

}
?>
