<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
*----------------------------------------------------------------------*/

require_once('phputil/classes/Assert.inc');
require_once('phputil/config.php');
require_once('phputil/handy_utils.inc');
require_once('phputil/session_functions.php');
require_once('phputil/classes/contactus.inc');
require_once('Zend/Log.php');
require_once('Zend/Mail.php');
require_once('Zend/Mail/Transport/Sendmail.php');
require_once('Zend/Log/Filter/Interface.php');
require_once('Zend/Log/Writer/Stream.php');
require_once('Zend/Log/Writer/Firebug.php');
require_once('Zend/Log/Writer/Mail.php');
require_once('Zend/Log/Writer/Null.php');
require_once('Zend/Controller/Request/Http.php');
require_once('Zend/Controller/Response/Http.php');
require_once('Zend/Wildfire/Channel/HttpHeaders.php');

/**
 * @todo get rid of this once we've figured out how to buffer things safely
 */
ob_start();

/**
 * A general-purpose logger utility.
 *
 * Developers can log messages with specified priority levels. In addition, all 
 * uncaught exceptions and PHP errors pass through this utility.
 *
 * Log messages are intelligently routed to a variety of destinations, including 
 * a log file, the browser, Firebug's logging utility, and possibly to email.
 *
 * If you write helpful log messages, you can leave them in your code even while 
 * it goes to production, and they won't show up to users.
 *
 * FisdapLogger implements the mingleton pattern. Most often, you can simply 
 * access the general logger by calling {@link get_logger()} with no parameters. 
 * However, if you want a separate logger instance, you can get one by passing 
 * in a name.
 *
 * Usage:
 * <code>
 * $logger = FisdapLogger::get_logger();
 * $logger->debug('My debug message');
 * $logger->err('Holy crap an error');
 * $logger->exception(new FisdapException('Oh my'));
 * </code>
 *
 * See the class constants for what levels are available.
 *
 * Finally, if you use Firebug (and if you don't, you should), you should 
 * install {@link https://addons.mozilla.org/en-US/firefox/addon/6149 FirePHP} 
 * to be able to access logger output in the Firebug console.
 *
 * <pre>
 * A message format in the config file can make use of any of the following
 * predefined variables (case sensitive).  Variables that are undefined will be 
 * left in the text.
 * </pre>
 * <ul>
 * <li>'additionalInfo'
 * <li>'additionalInfoEol'
 * <li>'classPrefix
 * <li>'filename'
 * <li>'function'
 * <li>'identity'
 * <li>'ipAddress'
 * <li>'lineNbr'
 * <li>'logId'
 * <li>'priorityName'
 * <li>'stackTraceString'
 * <li>'timestamp'
 * <li>'timestampReadable'
 * <li>'userName'
 * </ul>
 * @author Ian Young
 */
class FisdapLogger {

	/**
	 * Something very, very bad has happened.
	 */
	const CRIT = Zend_Log::CRIT;
	/**
	 * An unrecoverable error has occurred.
	 */
	const ERR = Zend_Log::ERR;
	/**
	 * An exception
	 *
	 * Very similar to ERR.
	 */
	const EXCEPTION = Zend_Log::ERR;
	/**
	 * Something recoverable, but outside the expected scope of operation
	 */
	const WARN = Zend_Log::WARN;
	/**
	 * Debugging information
	 */
	const DEBUG = Zend_Log::DEBUG;
	/**
	 * Used to warn developers of the use of deprecated functionality
	 */
	const DEPRECATED = 10;
	/**
	 * Activity that is suspicious or otherwise unusual
	 */
	const SECURITY = 8;

	/**
	 * The server's config options
	 * @var Zend_Config
	 */
	protected $config;
	/**
	 * The logger we'll use (FisdapLogger is basically an adapter)
	 * @var Zend_Log
	 */
	protected $logger;
	/**
	 * An array of Zend_Log_Filter objects that we use to track
	 * writer/filter/formatters that have been added to {@link $logger}.
	 * @var array
	 */
	private $filters = array();
	/**
	 * An array of messages that were deferred.
	 * @var array
	 * @see defer_messages()
	 */
	protected $deferred = array();
	/**
	 * Are we currently deferring log messages?
	 * @var boolean
	 * @see defer_messages()
	 */
	protected $defer = false;
	/**
	 * Are we currently allowing logging at all?
	 * @var boolean
	 * @see disable_all_logging()
	 */
	protected $enabled = true;
	/**
	 * This is not to be used except for testing.
	 * @var an array of Zend_Log_Writers mirroring $filters
	 * @ignore
	 */
	private $writers;

	/**
	 * An array of instantiated loggers
	 * @var array
	 */
	protected static $log_inst = array();
	/**
	 * Sticky tag to remember if logging is disabled for all loggers
	 * @var boolean
	 * @see disable_all_logging_on_all_loggers()
	 */
	protected static $static_enabled = true;

	/**
	 * Get a logger instance
	 *
	 * Gets the logger with the given name, constructing it if necessary.
	 *
	 * @param string $name The name of the logger. Usually not specified.
	 * @return FISDAPLogger
	 */
	public static function get_logger($name = 'general') {

		// Instantiate it if it doesn't already exist
		if (!isset(self::$log_inst[$name])) {
			self::$log_inst[$name] = new FisdapLogger($name);
		}

		// If all logging is disabled, disable this one as well
		if (!self::$static_enabled) {
			self::$log_inst[$name]->disable_all_logging();
		}

		return self::$log_inst[$name];
	}

	private function __construct($name) {
		// Defer messages until we're done constructing
		$this->defer_messages();

		// Initialize settings
		$this->name = $name;
		$this->config = HandyServerUtils::get_config();
		$this->init_logger();
		$this->process_config();

		// Stuff for Firebug logging
		$request = new Zend_Controller_Request_Http();
		$this->response = new Zend_Controller_Response_Http();
		$this->channel = Zend_Wildfire_Channel_HttpHeaders::getInstance();
		$this->channel->setRequest($request);
		$this->channel->setResponse($this->response);

		// If we don't do this, Zend can't find a file it wants
		$sm = new Zend_Mail_Transport_Sendmail();
		Zend_Mail::setDefaultTransport($sm);

		// Now unqueue any messages that were generated during construction
		$this->undefer_messages();
	}

	/**
	 * Initialize the internal Zend_Log object
	 */
	private function init_logger() {
		$this->logger = new Zend_Log();
		// Make it aware of our custom log levels
		$this->logger->addPriority('SECURITY', self::SECURITY);
		$this->logger->addPriority('DEPRECATED', self::DEPRECATED);
		//$this->logger->addPriority('EXCEPTION', self::EXCEPTION);
	}

	/**
	 * Configure our logger according to the settings in the config file.
	 *
	 * Assumes {@link $this->config} has already been set.
	 */
	private function process_config() {

		// Loop through the various types of output
		foreach ($this->config->log->output as $name => $opts) {

			// For starters, disable all levels
			$func = 'disable_' . $name;
			$this->$func();

			if ($opts->enabled) {
				$enable = array();
				$disable = array();
				// If levels are set, get them
				$levels = isset($opts->levels) ? $opts->levels->toArray() : array();
				// Parse the level names into their constant values
				foreach ($levels as $l) {
					if ($l[0] == '-') {
						$disable[] = constant('self::' . substr($l, 1));
					} else {
						$enable[] = constant('self::' . $l);
					}
				}
				// Enable the chosen levels or all if none specified
				$func = 'enable_' . $name;
				call_user_func_array(array($this, $func), $enable);
				// Now disable any levels that were marked with a -
				if (count($disable) > 0) {
					$func = 'disable_' . $name;
					call_user_func_array(array($this, $func), $disable);
				}
			}
		}
	}

	/**
	 * Wipes the existing objects and reloads the writers and filters
	 *
	 * Useful when you change a setting and need to apply it (since Zend_Log 
	 * doesn't provide much in the way of mutation at the moment).
	 */
	protected function reload_config() {
		$this->defer_messages();
		// Wipe out the filters
		$this->filters = array();
		// Rebuild the logger
		$this->init_logger();
		$this->process_config();
		$this->undefer_messages();
	}

	/**
	 * This is not to be used except in unit testing.
	 * @param array
	 * @ignore
	 */
	public function inject_writers($writers) {
		// Save the new writers and reload
		$this->writers = $writers;
		$this->reload_config();
	}

	/**
	 * This is not to be used except in unit testing.
	 * @param Zend_Config
	 * @ignore
	 */
	public function inject_config($config) {
		$this->config = $config;
		$this->reload_config();
	}

	/**
	 * Begin deferring log messages.
	 *
	 * This is useful when the logger is in a 
	 * inconsistent state, for example, when we are constructing. Messages 
	 * logged during this time will be quietly saved until {@link 
	 * undefer_messages()} is called.
	 */
	protected function defer_messages() {
		// Turn on the flag
		$this->defer = true;
	}

	/**
	 * Stop deferring log messages.
	 *
	 * Signals to the logger that we are now in a consistent state and ready 
	 * to output any log messages that have been received during the time we 
	 * have been deferring.
	 */
	protected function undefer_messages() {
		// Turn off the flag
		$this->defer = false;
		// Unqueue any saved messages
		foreach ($this->deferred as $args) {
			call_user_func_array(array($this, 'log'), $args);
		}
		// Reset the message queue
		$this->deferred = array();
	}

	/**
	 * Log a message
	 *
	 * The alternate form is $this->level($message), as in {@link err()} or 
	 * {@link debug()}. This form allows advanced options to be passed in.
	 * 
	 * @param int $level The log level. Must be one of this class' constants.
	 * @param string|FISDAPException $message The message to be logged. May be 
	 * an exception.
	 * @param array $options Logging options. Supported keys are:
	 *  - <var>trace_offset</var>: an int to set the number of levels of the 
	 *    backtrace to ignore. Defaults to 0.
	 *  - <var>log_id</var>: an int to set the log ID to use.
	 * @author Ian Young
	 * @author Brian Peterson
	 */
	public function log($message, $level, $options=array()) {
		// If logging is disabled, drop it like it's hot
		if (!$this->enabled) {
			return;
		}
		// If we are deferring messages, stuff 'em in the array and exit
		if ($this->defer) {
			$this->deferred[] = array($message, $level, $options);
			return;
		}

		// Get an id for this event
		if (isset($options['log_id'])) {
			$id = $options['log_id'];
		}
		else {
			//TODO this is kind of a hack
			// Use a static id generating function
			$id = FisdapExceptionUtils::generate_id();
			// Cut off the first few digits and division it for readability
			$id = substr($id, 3, 3) . '.' . substr($id, 6);
		}
		
		// Give it to the logger
		$this->logger->setEventItem('logId', $id);

		// Set some event attributes using helper functions
		$tracelvl = 0;
		if (isset($options['trace_offset'])) {
			$tracelvl = $options['trace_offset'];
		}
		$this->set_log_event_attributes($message, $tracelvl);
		$this->set_user_attributes($id);

		// Now log the message
		$this->logger->log($message, $level);

		// For Firebug
		$this->channel->flush();
		$this->response->sendHeaders();

		// If handling an exception, show the cause too.
		if ($message instanceof FisdapException) {
			$cause = $message->get_cause();
			if (!is_null($cause)) {
				$options['log_id'] = $id;
				$this->log($cause, $level, $options);
			}
		}
	}

	/**
	 * A convenience function for one-off log statements.
	 *
	 * Gets the default logger instance and sends the message to it.
	 *
	 * This should probably not be used in permanent logging statements. It 
	 * exists merely to cut down on keystrokes for throwaway logging statements.
	 *
	 * @param string $message
	 * @param int $level
	 * @see log()
	 */
	public static function quicklog($message, $level=FisdapLogger::DEBUG) {
		$l = FisdapLogger::get_logger();
		$l->log($message, $level);
	}

	/**
	 * Figure out information about the current log event and pass it to the 
	 * logger for use in displaying log messages.
	 * @param string|FisdapException $message the log message
	 * @param array $options The same options array passed to log()
	 * @todo fix these params
	 */
	private function set_log_event_attributes($message, $tracelvl) {

		$this->logger->setEventItem('timestamp', date('H:i:s Y-m-d'));
		$this->logger->setEventItem('timestampReadable', date(DATE_COOKIE));

		// If an exception is being logged, handle that separately and return early
		if ($message instanceof Exception) {
			return $this->set_exception_event_attributes($message);
		}

		$backtrace = debug_backtrace();

		// Increment the trace level for this private function
		$tracelvl++;

		// We loop here because in certain cases like trigger_error(), the
		// first level of the backtrace won't contain any useful information, and
		// so we need to hop up a level (or maybe several levels?)
		while (isset($backtrace[$tracelvl+1]) && (!isset($backtrace[$tracelvl]['line']) || !isset($backtrace[$tracelvl]['file']))) {
			$tracelvl++;
		}

		// Get the lines of the backtrace we're interested in
		$line = $backtrace[$tracelvl];
		$line2 = isset($backtrace[$tracelvl+1]) ? $backtrace[$tracelvl+1] : array();

		// Pull information out of the trace and give it to the logger for optional use

		$filename = (isset($line['file'])?$line['file']:'');

		// Try to trim lengthy file names.
		$roots = array(
			FISDAP_ROOT,
			'/PHPUnit/',
			'/FISDAPTesting/'
		);
		foreach ($roots as $root) {
			if (($i = strrpos($filename, $root)) !== false) {
				$n = $i + strlen($root);
				if (strlen($filename) > $n) {
					$filename = substr($filename, $n);
				}

				break;
			}
		}

		$this->logger->setEventItem('filename', $filename);

		$this->logger->setEventItem('function', (isset($line2['function'])?$line2['function']:''));

		$this->logger->setEventItem('lineNbr', (isset($line['line'])?$line['line']:''));

		if (isset($line2['class']) && isset($line2['type'])) {
			$this->logger->setEventItem('classPrefix', $line2['class'] . $line2['type']);
		} 
		else {
			$this->logger->setEventItem('classPrefix', '');
		}

		$this->logger->setEventItem('stackTraceString', '');
		$this->logger->setEventItem('additionalInfo', '');
		$this->logger->setEventItem('additionalInfoEol', '');
	}

	/**
	 * Figure out information about an exception and pass it to the 
	 * logger for use in displaying log messages.
	 * @see set_log_event_attributes()
	 */
	private function set_exception_event_attributes($message) {

		$trace = $message->getTrace();
		$line = $trace[0];

		$this->logger->setEventItem('filename', $message->getFile());

		$this->logger->setEventItem('function', (isset($line['function'])?$line['function']:''));

		$this->logger->setEventItem('lineNbr', $message->getLine());

		if (isset($line['class']) && isset($line['type'])) {
			$this->logger->setEventItem('classPrefix', $line['class'] . $line['type']);
		} 
		else {
			$this->logger->setEventItem('classPrefix', '');
		}

		$this->logger->setEventItem('stackTraceString', $message->getTraceAsString());

		if ($message instanceof FisdapException) {
			$additionalInfo = implode("\n", $message->get_additional_info());
		} else {
			$additionalInfo = '';
		}

		if ($additionalInfo == '') {
			$additionalInfoEol = '';
		} else {
			$additionalInfoEol = PHP_EOL . $additionalInfo;
		}

		$this->logger->setEventItem('additionalInfo', $additionalInfo);
		$this->logger->setEventItem('additionalInfoEol', $additionalInfoEol);
	}

	/**
	 * Figure out information about the current user and pass it to the 
	 * logger for use in displaying log messages.
	 * @param int $id The log id number for this log event
	 */
	private function set_user_attributes($id) {
		// Set the user attributes.
		if ( is_masquerade() ) {
			$masquerade_name = get_masquerade_username();
		} else {
			$masquerade_name = '';
		}

		if (isset($_SESSION[SESSION_KEY_PREFIX . 'username'])) {
			$user_name = $_SESSION[SESSION_KEY_PREFIX . 'username'];
		}
		else {
			$user_name = '';
		}

		$this->logger->setEventItem('userName', $user_name);

		$ip_address = (isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:'');
		$this->logger->setEventItem('ipAddress', $ip_address);

		// Set the identity, which is simply some items put together.
		$list = array();
		if ($user_name != '') {
			$list[] = $user_name;
		}

		$list[] = $id;

		if ($ip_address != '') {
			$list[] = $ip_address;
		}

		if ($masquerade_name != '') {
			$list[] = $masquerade_name;
		}

		$this->logger->setEventItem('identity', join(',', $list));
	}

	/**
	 * Simply calls {@link self::log()} with the given level and all the items in an array.
	 *
	 * @param int $level the log level
	 * @param array $message_arr an array of log messages
	 */
	private function log_array($level, $message_arr) {
		Assert::is_array($message_arr);
		Assert::is_int($level);

		// We need to ignore two levels of the backtrace that will be Logger functions
		$opts = array('trace_offset' => 2);

		// Loop through and log each message
		foreach ($message_arr as $m) {
			$this->log($m, $level, $opts);
		}
	}

	/**
	 * Log a message with level CRIT
	 *
	 * @param string|FISDAPException $message,... The message(s) to be logged. May be 
	 * an exception, in which case formatting will be handled automatically.
	 */
	public function crit() {
		$args = func_get_args();
		$this->log_array(self::CRIT, $args);
	}

	/**
	 * Log a message with level ERR
	 *
	 * @param string|FISDAPException $message,... The message(s) to be logged. May be 
	 * an exception, in which case formatting will be handled automatically.
	 */
	public function err() {
		$args = func_get_args();
		$this->log_array(self::ERR, $args);
	}

	/**
	 * Log a message with level EXCEPTION
	 *
	 * @param string|FISDAPException $message,... The message(s) to be logged. May be 
	 * an exception, in which case formatting will be handled automatically.
	 */
	public function exception() {
		$args = func_get_args();
		$this->log_array(self::EXCEPTION, $args);
	}

	/**
	 * Log a message with level WARN
	 *
	 * @param string|FISDAPException $message,... The message(s) to be logged. May be 
	 * an exception, in which case formatting will be handled automatically.
	 */
	public function warn() {
		$args = func_get_args();
		$this->log_array(self::WARN, $args);
	}

	/**
	 * Log a message with level DEBUG
	 *
	 * @param string|FISDAPException $message,... The message(s) to be logged. May be 
	 * an exception, in which case formatting will be handled automatically.
	 */
	public function debug() {
		$args = func_get_args();
		$this->log_array(self::DEBUG, $args);
	}

	/**
	 * Log a message with level DEPRECATED
	 *
	 * @param string|FISDAPException $message,... The message(s) to be logged. May be 
	 * an exception, in which case formatting will be handled automatically.
	 */
	public function deprecated() {
		$args = func_get_args();
		$this->log_array(self::DEPRECATED, $args);
	}

	/**
	 * Log a message with level SECURITY
	 *
	 * @param string|FISDAPException $message,... The message(s) to be logged. May be 
	 * an exception, in which case formatting will be handled automatically.
	 */
	public function security() {
		$args = func_get_args();
		$this->log_array(self::SECURITY, $args);
	}

	/**
	 * Turn off all logging from this logger.
	 *
	 * <b>Do not use in production code!</b>
	 */
	public function disable_all_logging() {
		$this->enabled = false;
	}

	/**
	 * Turn all logging back on.
	 *
	 * Logging is on by default, this is only needed if it has been disabled by 
	 * {@link disable_all_logging()}.
	 */
	public function enable_all_logging() {
		$this->enabled = true;
	}

	/**
	 * Turn off all logging for all loggers.
	 *
	 * <b>Do not use in production code!</b>
	 */
	public static function disable_all_logging_on_all_loggers() {
		foreach (self::$log_inst as $logger) {
			$logger->disable_all_logging();
		}
		self::$static_enabled = false;
	}

	/**
	 * Turn all logging back on.
	 *
	 * Logging is on by default, this is only needed if it has been disabled by 
	 * {@link disable_all_logging_on_all_loggers()}.
	 */
	public static function enable_all_logging_on_all_loggers() {
		foreach (self::$log_inst as $logger) {
			$logger->enable_all_logging();
		}
		self::$static_enabled = true;
	}

	/**
	 * Send the given log levels to Firebug
	 * @param int $levels,... The levels for which to enable Firebug logging. If 
	 * none given, enables all levels.
	 */
	public function enable_firebug() {
		$params = func_get_args();
		$func = 'accept_levels';
		$this->change_status_firebug($func, $params);
	}

	/**
	 * Don't send the given log levels to Firebug
	 * @param int $levels,... The levels for which to disable Firebug logging. If
	 * none given, disables all levels.
	 */
	public function disable_firebug() {
		$params = func_get_args();
		$func = 'reject_levels';
		$this->change_status_firebug($func, $params);
	}

	/**
	 * Does the grunt work of configuring the Firebug log writer
	 */
	private function change_status_firebug($func, $params) {
		// If it doesn't exist, initialize the Firebug writer/filter
		if (!isset($this->filters['firebug'])) {
			$writer = new Zend_Log_Writer_Firebug();

			// Set the styling for various levels
			//$writer->setPriorityStyle(self::EXCEPTION, 'EXCEPTION');
			$writer->setPriorityStyle(self::DEPRECATED, 'WARN');
			$writer->setPriorityStyle(self::SECURITY, 'INFO');

			// For unit testing
			if (isset($this->writers['firebug'])) $writer = $this->writers['firebug'];

			// Give it a filter
			$filter = new FisdapLogger_Filter();
			$writer->addFilter($filter);
			$this->logger->addWriter($writer);
			$this->filters['firebug'] = $filter;
		}
		$filter = $this->filters['firebug'];
		// Pass the enable or disable command to the filter
		call_user_func_array(array($filter, $func), $params);
	}

	/**
	 * Send the given log levels to mail
	 * @param int $levels,... The levels for which to enable mail logging. If 
	 * none given, enables all levels.
	 */
	public function enable_mail() {
		$params = func_get_args();
		$func = 'accept_levels';
		$this->change_status_mail($func, $params);
	}

	/**
	 * Don't send the given log levels to mail
	 * @param int $levels,... The levels for which to disable mail logging. If
	 * none given, disables all levels.
	 */
	public function disable_mail() {
		$params = func_get_args();
		$func = 'reject_levels';
		$this->change_status_mail($func, $params);
	}

	/**
	 * @see change_status_firebug()
	 */
	private function change_status_mail($func, $params) {
		if (!isset($this->filters['mail'])) {
			$mail_to = $this->config->log->output->mail->recipients;
			$mail = new Zend_Mail();
			$mail->setFrom('errors@fisdap.net');
			foreach ($mail_to as $addr) {
				$mail->addTo($addr);
			}
			$writer = new Zend_Log_Writer_Mail($mail);

			$writer->setSubjectPrependText('FISDAP PHP Errors');

			// For unit testing
			if (isset($this->writers['mail'])) $writer = $this->writers['mail'];

			$format = '%timestampReadable%'
				. PHP_EOL . '%identity%'
				. PHP_EOL . '%filename%:%lineNbr% %classPrefix%%function%()'
				. PHP_EOL . ' %message%' . PHP_EOL
				. '%stackTraceString%' .
				'%additionalInfoEol%';
			$formatter = new Zend_Log_Formatter_Simple($format);
			$writer->setFormatter($formatter);
			$filter = new FisdapLogger_Filter();
			$writer->addFilter($filter);
			$this->logger->addWriter($writer);
			$this->filters['mail'] = $filter;
		}
		$filter = $this->filters['mail'];
		call_user_func_array(array($filter, $func), $params);
	}

	/**
	 * Send the given log levels to the logfile
	 * @param int $levels,... The levels for which to enable logfile logging. If 
	 * none given, enables all levels.
	 */
	public function enable_file() {
		$params = func_get_args();
		$func = 'accept_levels';
		$this->change_status_file($func, $params);
	}

	/**
	 * Don't send the given log levels to the logfile
	 * @param int $levels,... The levels for which to disable logfile logging. If
	 * none given, disables all levels.
	 */
	public function disable_file() {
		$params = func_get_args();
		$func = 'reject_levels';
		$this->change_status_file($func, $params);
	}

	/**
	 * @see change_status_firebug()
	 */
	private function change_status_file($func, $params) {
		if (!isset($this->filters['file'])) {
			$file_path = FisdapLogger_Writer_File::get_absolute_path(
				$this->config->log->output->file->path);

			// Let's test to see if the log file is writeable
			$writeable = @fopen($file_path, 'a');
			if ($writeable === false) {
				// Log file isn't writeable, complain and stub out the writer
				$this->warn('Your logfile, "'.$file_path.'", is not writeable. Make sure your permissions are set correctly.');
				$writer = new Zend_Log_Writer_Null();
			} else {
				fclose($writeable);
				$writer = new FisdapLogger_Writer_File($file_path);
			}

			// For unit testing
			if (isset($this->writers['file'])) $writer = $this->writers['file'];

			$filter = new FisdapLogger_Filter();
			$writer->addFilter($filter);
			$this->logger->addWriter($writer);
			$this->filters['file'] = $filter;
		}
		$filter = $this->filters['file'];
		call_user_func_array(array($filter, $func), $params);
	}

	/**
	 * Send the given log levels to the screen
	 * @param int $levels,... The levels for which to enable screen logging. If 
	 * none given, enables all levels.
	 */
	public function enable_screen() {
		$params = func_get_args();
		$func = 'accept_levels';
		$this->change_status_screen($func, $params);
	}

	/**
	 * Don't send the given log levels to the screen
	 * @param int $levels,... The levels for which to disable screen logging. If
	 * none given, disables all levels.
	 */
	public function disable_screen() {
		$params = func_get_args();
		$func = 'reject_levels';
		$this->change_status_screen($func, $params);
	}

	/**
	 * @see change_status_firebug()
	 */
	private function change_status_screen($func, $params) {
		if (!isset($this->filters['screen'])) {
			$writer = new FisdapLogger_Writer_Screen();

			// For unit testing
			if (isset($this->writers['screen'])) $writer = $this->writers['screen'];

			$filter = new FisdapLogger_Filter();
			$writer->addFilter($filter);
			$this->logger->addWriter($writer);
			$this->filters['screen'] = $filter;
		}
		$filter = $this->filters['screen'];
		call_user_func_array(array($filter, $func), $params);
	}

	/**
	 * Send the given log levels to the screen in a pretty format
	 * @param int $levels,... The levels for which to enable prettyscreen logging. If 
	 * none given, enables all levels.
	 */
	public function enable_prettyscreen() {
		$params = func_get_args();
		$func = 'accept_levels';
		$this->change_status_prettyscreen($func, $params);
	}

	/**
	 * Don't send the given log levels to the screen in a pretty format
	 * @param int $levels,... The levels for which to disable prettyscreen logging. If
	 * none given, disables all levels.
	 */
	public function disable_prettyscreen() {
		$params = func_get_args();
		$func = 'reject_levels';
		$this->change_status_prettyscreen($func, $params);
	}

	/**
	 * @see change_status_firebug()
	 */
	private function change_status_prettyscreen($func, $params) {
		if (!isset($this->filters['prettyscreen'])) {
			$writer = new FisdapLogger_Writer_PrettyScreen();

			// For unit testing
			if (isset($this->writers['prettyscreen'])) $writer = $this->writers['prettyscreen'];

			$filter = new FisdapLogger_Filter();
			$writer->addFilter($filter);
			$this->logger->addWriter($writer);
			$this->filters['prettyscreen'] = $filter;
		}
		$filter = $this->filters['prettyscreen'];
		call_user_func_array(array($filter, $func), $params);
	}

}

/**
 * A filter to help us show only the messages we want to show.
 *
 * Can be set to accept or reject all levels or a given set of levels. 
 * Combinations of directives may be given, for example to accept all levels 
 * EXCEPT for x and y, or likewise with rejecting.
 */
class FisdapLogger_Filter implements Zend_Log_Filter_Interface {

	private $accept_all = true;
	private $accepted = array();
	private $rejected = array();

	public function accept($event) {
		$priority = $event['priority'];
		return (($this->accept_all || in_array($priority, $this->accepted))
			&& !(in_array($priority, $this->rejected)));
	}

	/**
	 * Accept the given set of levels
	 * @param int $levels,... The levels to accept. If none are specified, 
	 * accept all levels.
	 */
	public function accept_levels() {
		if (func_num_args() == 0) {
			$this->accept_all_levels();
		} else {
			$new_levels = func_get_args();
			// Add the new levels to the list of accepted levels
			$this->accepted = array_merge($this->accepted, $new_levels);
			// And if they were in the list of rejected levels, remove them
			$this->rejected = array_diff($this->rejected, $new_levels);
		}
	}

	/**
	 * Accept all log levels
	 */
	protected function accept_all_levels() {
		$this->accept_all = true;
		$this->rejected = array();
	}

	/**
	 * Reject the given set of levels
	 * @param int $levels,... The levels to reject. If none are specified, 
	 * reject all levels.
	 */
	public function reject_levels() {
		if (func_num_args() == 0) {
			$this->reject_all_levels();
		} else {
			$new_levels = func_get_args();
			// Add the new levels to the list of rejected levels
			$this->rejected = array_merge($this->rejected, $new_levels);
			// And if they were in the list of accepted levels, remove them
			$this->accepted = array_diff($this->accepted, $new_levels);
		}
	}

	/**
	 * Reject all log levels
	 */
	protected function reject_all_levels() {
		$this->accept_all = false;
		// Clear out the lists of accepted and rejected
		$this->accepted = array();
		$this->rejected = array();
	}

}

/**
 * A log writer for sending messages to the screen.
 *
 * Is smart about AJAX and will automatically use the right kind of output.
 * @todo We still need a way to make sure output always shows up at the top of 
 * the screen.
 */
class FisdapLogger_Writer_Screen extends Zend_Log_Writer_Abstract {

	public function __construct() {
		// If we're logging to the command line, don't output HMTL
		if (HandyServerUtils::is_command_line()) {
			$format = '%priorityName%: %classPrefix%%function%: %message% in %filename% on line %lineNbr% (%logId%)' . PHP_EOL;
		} else {
			$format = '<div class="log_entry">';
			$format .= '<b>%priorityName%</b>: %classPrefix%%function%: %message% in <b>%filename%</b> on line <b>%lineNbr%</b> (%logId%)';
			$format .= '<pre class="stack_trace">%stackTraceString%</pre>';
			$format .= '<div class="additional">%additionalInfo%</div>';
			$format .= '</div>' . PHP_EOL;
		}

		$formatter = new Zend_Log_Formatter_Simple($format);
		$this->setFormatter($formatter);
	}

	/**
	 * @todo use something else to check for ajax mode
	 */
	protected function _write($event) {
		// If we're logging an exception, just pass the embedded message along
		if ($event['message'] instanceof Exception) {
			$event['message'] = $event['message']->getMessage();
		}
		// If it's an array, flatten it
		if (is_array($event['message'])) {
			$event['message'] = '<pre>' . var_export($event['message'], true) . '</pre>';
		}

		// Get the formatted text
		$text = $this->_formatter->format($event);
		// Divine the right way to output it
		if (isset($_POST['ajax'])) {
			$action = new InnerHtmlBrowserAction('fisdap_logger_messages', $text, 'append');
			send_browser_action($action);
		} else {
			echo $text;
		}
	}
	
	static public function factory($config) {}

}

/**
 * A log writer for sending end-user error messages to the screen.
 *
 * Like {@link FisdapLogger_Writer_Screen} but instead of providing error information, 
 * outputs a friendly message saying an error has occurred.
 *
 * <b>NOTE:</b> This is only for dealing with unforseen errors. This message is 
 * no replacement for writing your own error messages that are actually 
 * <i>informative</i> and context-specific.
 */
class FisdapLogger_Writer_PrettyScreen extends Zend_Log_Writer_Abstract {

	public function __construct() {
		if (HandyServerUtils::is_command_line()) {
			// todo Why are we logging to the command line? We shouldn't be.
			$format = '';
		} else {
			$phone = ContactUs::get_office_phone();
			$mail = ContactUs::get_bug_support_email();
			$mailto = 'mailto:' . $mail;
			$mailto .= '?Subject=Error%20Report%20(Event%20%logId%)';
			$format = '<div class="error_message">We\'re sorry. An unexpected problem has occurred. Please call ' . $phone . ' or email <a href="' . $mailto . '">' . $mail . '</a>, and let us know what happened so that we can fix it. <div class="log_id">Event ID: %logId%</div></div>';
		}

		$formatter = new Zend_Log_Formatter_Simple($format);
		$this->setFormatter($formatter);
	}

	protected function _write($event) {
		// Only print output once per call
		static $first = true;
		if ($first) {
			$text = $this->_formatter->format($event);
			if (isset($_POST['ajax'])) {
				$action = new InnerHtmlBrowserAction('fisdap_logger_messages', $text, 'overwrite');
				send_browser_action($action);
			} else {
				echo $text;
			}
			$first = false;
		}
	}
	
	static public function factory($config) {}

}

/**
 * A log writer for writing to a log file.
 */
class FisdapLogger_Writer_File extends Zend_Log_Writer_Stream {

	public function __construct($path) {
		parent::__construct($path);
		// Set up our special formatting rules
		$format = '%classPrefix%%function%(%filename%:%lineNbr%):'
			. ' %message%' . PHP_EOL
			. '%stackTraceString%' .
			'%additionalInfoEol%';
		$formatter = new FisdapLogger_Formatter_Plaintext($format,
			'%priorityName%: (%identity%) ',
			'%priorityName%: (%identity%) %timestamp% ');
		$this->setFormatter($formatter);
	}

	protected function _write($event) {
		// If we're logging an exception, just pass the embedded message along
		if ($event['message'] instanceof Exception) {
			$event['message'] = $event['message']->getMessage();
		}
		// If it's an array, flatten it
		if (is_array($event['message'])) {
			$event['message'] = var_export($event['message'], true);
		}
		parent::_write($event);
	}

	/**
	 * Resolve the path to a log file.
	 * @param string $path The relative or absolute path.
	 * @return string The absolute path.
	 */
	public static function get_absolute_path($path) {
		// Add on the FISDAP directory?
		if ($path == '') return $path;

		if (substr($path, 0, 1) == '/') return $path;
		return FISDAP_ROOT . $path;
	}

}

/**
 * A formatter for creating flexible, greppable plaintext output.
 */
class FisdapLogger_Formatter_Plaintext extends Zend_Log_Formatter_Simple {

	/**
	 * @param string $format The format for messages. See {@link 
	 * parent::__construct()}.
	 * @param string $lineprefix Text to place at the beginning of every newline 
	 * in the message. May contain placeholders like $format.
	 * @param string $firstlineprefix Text to place at the beginning of the 
	 * first line in the message. If this is present, $lineprefix will not 
	 * appear on the first line. May contain placeholders like $format.
	 */
	public function __construct($format, $lineprefix=null, $firstlineprefix=null) {
		if ($firstlineprefix === null) {
			$firstlineprefix = $lineprefix;
		}
		// Create separate formatters for our prefixes
		$this->prefix_formatter = new Zend_Log_Formatter_Simple($lineprefix);
		$this->firstline_prefix_formatter = new Zend_Log_Formatter_Simple($firstlineprefix);
		parent::__construct($format);
	}

	public function format($event) {
		// Get the main message
		$str = parent::format($event);
		// Fill in any prefix placeholders with values
		$prefix = $this->prefix_formatter->format($event);
		$firstline = $this->firstline_prefix_formatter->format($event);
		// Trim the trailing newline
		$str = rtrim($str);
		// Put the prefix at the beginning of each line
		$str = preg_replace('/\n/', "\n" . $prefix, $str);
		// Output everything
		return $firstline . $str . "\n";
	}

}
