<?php
/**
 * Next-generation common prompts.
 *
 * Most of these inherit from Zend prompts. To be used with {@link CommonForm}.
 *
 * @author Ian Young
 * @package CommonInclude
 */

require_once('phputil/classes/common_utilities.inc');
require_once('phputil/classes/CommonFormComponent.inc');
require_once('phputil/classes/CommonPromptGroup.inc');

require_once('Zend/Form/Element/Text.php');
require_once('Zend/Form/Element/Textarea.php');
require_once('Zend/Form/Element/Select.php');
require_once('Zend/Form/Element/Radio.php');
require_once('Zend/Form/Element/Password.php');
require_once('Zend/Form/Element/Hidden.php');
require_once('Zend/Form/Element/MultiCheckbox.php');
require_once('Zend/Form/Decorator/Abstract.php');
require_once('Zend/Form/Decorator/ViewHelper.php');
require_once('Zend/View.php');

/**
 * A prompt component in common_form
 *
 * This is a specialized type of {@link CommonFormComponent} that
 * deals with input as well.
 *
 * @author Ian Young
 */
interface CommonPrompt extends CommonFormComponent {
	/**
	 * Get the prompt's current value
	 *
	 * This will be the submitted value if a submission is in progress, the 
	 * user's last submitted value if session defaulting is on, or else 
	 * the default value set on the page.
	 *
	 * To get submitted values, be sure to call {@link validate()} first.
	 *
	 * @return mixed
	 */
	public function get_promptvalue();
	/**
	 * Validate input to the prompt
	 *
	 * @return boolean true if the prompt value is valid, false otherwise
	 */
	public function is_valid();
	/**
	 * Get any validation errors for this prompt.
	 * @return array an array of strings
	 */
	public function get_errors();
	/**
	 * Delete this prompt's stored value
	 *
	 * Delete the value this prompt has stored in session space.
	 */
	public function delete_value();
	/**
	 * Disable this prompt
	 */
	public function disable();
	/**
	 * Enable this prompt after it's been disabled
	 */
	public function enable();
	/**
	 * Hide this prompt from the user
	 */
	public function hide();
	/**
	 * Show this prompt after it's been hidden
	 */
	public function show();
	/**
	 * Process the settings on this prompt and prepare it for value retrieval or 
	 * display.
	 */
	public function process();

}

/**
 * An abstract base class for things that implement {@link CommonPrompt}
 */
abstract class CommonPrompt_Base implements CommonPrompt {

	/**
	 * A helper for rendering prompts
	 * @var Zend_View
	 */
	protected $zend_view;
	/**
	 * An array mapping names of prompts we are listening to => the callback
	 * to use when that prompt changes
	 * @var array
	 */
	protected $ajax_callbacks;
	/**
	 * Has this prompt been processed yet?
	 */
	protected $processed;
	/**
	 * The form state
	 * @see CommonForm::update_state()
	 * @var array
	 */
	protected $form_state;
	/**
	 * Is this prompt hidden?
	 * @var boolean
	 */
	protected $hidden = false;
	/**
	 * Should we display as having received bad input?
	 * @var boolean
	 */
	protected $bad_input = false;

	public function __construct($name, $value, $text, $subtext=null, $required=false) {

		$this->logger = FisdapLogger::get_logger();

		$this->zend_view = new Zend_View(); //TODO do this later
		$this->zend_element = $this->init_zend_element($name);
		// Set the base values
		$this->set_default_value($value);
		$this->set_text($text);
		$this->set_subtext($subtext);
		$this->set_required($required);

		$this->listento = array();
		$this->listeners = array();

		//$this->zend_element->removeFilter('Zend_Filter_Alnum');
	}

	/**
	 * Create and return the Zend_Form_Element object that this class uses for 
	 * most of the heavy lifting.
	 * @return Zend_Form_Element
	 */
	abstract protected function init_zend_element($name);

	public function is_valid() {
		$value = $this->get_promptvalue();
		return $this->zend_element->isValid($value);
	}

	public function get_errors() {
		// Make sure we run a validity check first to put the messages in place
		$this->is_valid();
		// Now return any error messages generated
		return $this->zend_element->getMessages();
	}

	/**
	 * @todo let's not hit the database every time? maybe?
	 */
	public function get_promptvalue() {
		// Get the value from the session if it's set, default if not
		if (common_utilities::is_scriptvalue($this->get_name())) {
			$val = common_utilities::get_scriptvalue($this->get_name());
			$this->zend_element->setValue($val);
			$this->logger->debug('Session value found (' . $val . '), using it.');
		} else {
			$this->zend_element->setValue($this->default_value);
			$this->logger->debug('Session value not found, using default (' . $this->default_value . ') instead.');
		}
		// Run the value through the Zend object to get any filtering
		return $this->zend_element->getValue();
	}

	/**
	 * Listen to another prompt for changes and respond with a callback
	 *
	 * @param CommonPrompt $prompt the prompt to listen to
	 * @param callback $responder a responder callback. This callback should 
	 * accept two parameters:
	 *  - CommonPrompt <var>$prompt</var>: this prompt
	 *  - CommonPrompt <var>$trigger</var>: the prompt that changed to trigger 
	 *    this callback.
	 */
	public function listen_to($prompt, $responder) {
		$this->ajax_callbacks[$prompt->get_name()] = $responder;
		$prompt->add_listener($this);
	}
	/**
	 * Used internally by CommonPromptBase
	 *
	 * Set another prompt as listening to this prompt. If you are wanting to 
	 * define listener relationships from a page, you should probably use {@link 
	 * listen_to()} instead.
	 */
	public function add_listener($prompt) {
		$this->listeners[] = $prompt;
	}
	public function get_listeners() {
		//return $this->listeners;
		return array($this->get_name() => $this->listeners);
	}

	public function process() {
		// Make sure the Zend object has the correct value
		$this->zend_element->setValue($this->get_promptvalue());
		$this->processed = true;
	}

	public function display() {

		if (!$this->processed) $this->process();

		$this->zend_element->setAttrib('onchange', $this->get_name() . '_js();');
		// Set up the decorators
		$this->zend_element->clearDecorators();
		$this->zend_element->addPrefixPath('CommonDecorator', '', 'decorator');
		$this->zend_element->addDecorator('ViewHelper')
			->addDecorator(array('inputDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'common_prompt_input'))
			->addDecorator('CommonLabel', array('valid' => true))
			//->addDecorator('CommonJavascriptResponder')
			->addDecorator(array('innerOuterDiv' => 'HtmlTag'), array('tag' => 'div'))
			->addDecorator(array('OuterDiv' => 'HtmlTag'), array('tag' => 'div', 'id' => $this->zend_element->getName() . '_div', 'class' => 'common_prompt'));

		// If this prompt is hidden, apply the appropriate styling
		if ($this->hidden) {
			$outer_decorator = $this->zend_element->getDecorator('OuterDiv');
			$old_class = $outer_decorator->getOption('class');
			$outer_decorator->setOption('class', $old_class . ' hidden');
		}

		// If this prompt is marked as invalid, apply the appropriate styling
		if ($this->bad_input) {
			$label_decorator = $this->zend_element->getDecorator('CommonLabel');
			$label_decorator->setOption('valid', false);
		}

		// Now render it with the decorators we gave it
		return $this->zend_element->render($this->zend_view) . $this->get_javascript();
	}

	protected function get_javascript() {
		return '';

		$func = 'function ' . $this->get_name() . '_js() { ';
		if (count($this->get_listeners()) > 0) {
			$func .= 'promptRespondHandler("' . $this->get_name() . '", "' . $this->form_state['name'] . '"); ';
		}
		$func .= '}';

		$script = '<script>' . $func . '</script>';

		return $script;
	}

	public function set_form_state($state) {
		$this->form_state = $state;
	}

	/**
	 * @todo needed?
	 * @todo test
	 */
	public function get_column_width() {
		//STUB
		return 1;
	}

	/**
	 * @todo document
	 * @todo rename?
	 */
	public function respond_formsubmithandler($input=null) {
		if (is_null($input)) {
			$input = $_POST;
		}
		if (isset($input[$this->get_name()])) {
			$this->set_value($input[$this->get_name()]);
		} else {
			$this->delete_value();
		}
	}

	public function get_name() {
		return $this->zend_element->getName();
	}

	public function set_badinput($bool) {
		$this->bad_input = $bool;
	}

	public function disable() {
		$this->zend_element->setAttrib('disable', true);
	}

	public function enable() {
		$this->zend_element->setAttrib('disable', false);
	}

	public function show() {
		$this->hidden = false;
	}

	public function hide() {
		$this->hidden = true;
	}

	/**
	 * Set if this prompt is required
	 * @param boolean
	 */
	public function set_required($required) {
		$this->zend_element->setRequired($required);
	}

	/**
	 * Set the name of the prompt
	 * @param string
	 */
	public function set_name($name) {
		$this->zend_element->setName($name);
	}

	/**
	 * Set the current value of the prompt
	 * @param mixed
	 */
	public function set_value($value) {
		// Just save it to the session
		common_utilities::set_scriptvalue($this->get_name(), $value);
		$this->logger->debug('Setting prompt value to ' . $value);
	}

	/**
	 * Set the default value of the prompt
	 * @param mixed
	 */
	public function set_default_value($value) {
		$this->default_value = $value;
		$this->logger->debug('Setting default prompt value to ' . $value);
	}

	/**
	 * Delete the current value of the prompt
	 *
	 * Generally this will result in the prompt having the default value 
	 * instead.
	 */
	public function delete_value() {
		common_utilities::delete_scriptvalue($this->get_name());
	}

	/**
	 * Set the displayed prompt text
	 * @param string
	 */
	public function set_text($text) {
		// If the text doesn't end in a : or ?, make it so
		if (substr($text, -1) != ':' && substr($text, -1) != '?') {
			$text .= ':';
		}
		$this->zend_element->setLabel($text);
	}

	/**
	 * Set the displayed prompt subtext
	 * @param string
	 */
	public function set_subtext($subtext) {
		$this->zend_element->setDescription($subtext);
	}

	/**
	 * Set a CSS class that the given prompt belongs to
	 * @param string
	 */
	public function set_class($name) {
		$this->zend_element->setAttrib('class', $name);
	}

	/**
	 * Respond to an AJAX event.
	 *
	 * You may override this function if desired, for example if you are writing 
	 * a domain prompt.
	 *
	 * @param CommonPrompt $prompt the prompt that triggered the callback.
	 * @return boolean true if this prompt needs to be updated onscreen
	 */
	public function respond_ajax($prompt) {

		$callback = $this->ajax_callbacks[$prompt->get_name()];

		if ($callback == null) {
			$callback = 'respond_ajax_' . $this->get_name() . '_prompt';
		}

		if (is_callable($callback)) {
			return call_user_func($callback, $this, $prompt);
		}

	}

}

abstract class CommonTextFieldPrompt extends CommonPrompt_Base {
	public function set_size($size) {
		$this->zend_element->setAttrib('size', $size);
	}
	public function set_value($value) {
		try {
			$value = Convert::to_string($value);
			parent::set_value($value);
		} catch (FisdapInvalidArgumentException $e) {
			// Do nothing, the value does not get set
			$this->logger->warn('Tried to set a text field value to a non-string: ' . (string)$value);
		}
	}
	public function set_default_value($value) {
		// If this can't convert to a string, go ahead and leave the exception 
		// uncaught - this is a developer mistake
		$value = Convert::to_string($value);
		parent::set_default_value($value);
	}
}

class CommonTextPrompt extends CommonTextFieldPrompt {
	protected function init_zend_element($name) {
		$el = new Zend_Form_Element_Text($name);
		//$el->removeFilter('Zend_Filter_Alnum');
		return $el;
	}
}

class CommonTextareaPrompt extends CommonTextFieldPrompt {
	protected function init_zend_element($name) {
		$el = new Zend_Form_Element_Textarea($name);
		return $el;
	}
	public function __construct($name, $value, $text, $subtext=null, $required=false) {
		parent::__construct($name, $value, $text, $subtext, $required);
		$this->set_rows(3);
		$this->set_columns(35);
	}

	public function set_rows($rows) {
		$this->zend_element->setAttrib('rows', $rows);
	}

	public function set_columns($cols) {
		$this->zend_element->setAttrib('cols', $cols);
	}

}

/**
 * @todo Should the constructor take a value? It's useless to this prompt, but I 
 * feel like removing it would mess with constructor consistency.
 */
class CommonPasswordPrompt extends CommonTextFieldPrompt {
	protected function init_zend_element($name) {
		$el = new Zend_Form_Element_Password($name);
		return $el;
	}
}

class CommonHiddenPrompt implements CommonPrompt {

	public function __construct($name, $value) {
		$this->zend_view = new Zend_View(); //TODO do this later
		$this->zend_element = new Zend_Form_Element_Hidden($name);
		// Set the base values
		$this->set_value($value);
	}

	public function is_valid() {
		$value = $this->get_promptvalue();
		return $this->zend_element->isValid($value);
	}

	public function delete_value() {
		//STUB
	}

	/**
	 * Set the default value of the prompt
	 * @param mixed
	 */
	public function set_value($value) {
		// Set our default
		$this->zend_element->setValue($value);
		// Now make sure we are set to the current value
		$this->zend_element->setValue($this->get_promptvalue());
	}

	public function get_promptvalue() {
		if (common_utilities::is_scriptvalue($this->get_name())) {
			$val = common_utilities::get_scriptvalue($this->get_name());
			// Run it through the Zend object to get any filtering
			$this->zend_element->setValue($val);
		}
		return $this->zend_element->getValue();
	}

	public function process() {
		// Make sure the Zend object has the correct value
		$this->zend_element->setValue($this->get_promptvalue());
		$this->processed = true;
	}

	public function display() {
		$this->zend_element->setDecorators(array('ViewHelper'));
		return $this->zend_element->render($this->zend_view);
	}

	public function set_form_state($state) {
		//STUB
	}

	public function get_column_width() {
		//STUB
		return 1;
	}

	public function get_name() {
		return $this->zend_element->getName();
	}

	public function set_badinput($bad) {
		//STUB
	}

	public function respond_ajax($prompt) {
		//STUB do nothing
	}

	/**
	 * @todo this, and others, are purloined from CommonPrompt_Base
	 */
	public function respond_formsubmithandler($input=null) {
		if (is_null($input)) {
			$input = $_POST;
		}
		if (isset($input[$this->get_name()])) {
			common_utilities::set_scriptvalue($this->get_name(), $input[$this->get_name()]);
		} else {
			// If nothing got posted, this prompt is empty
			common_utilities::set_scriptvalue($this->get_name(), '');
		}
	}
	/**
	 * @todo don't send this in POST?
	 */
	public function disable() {
		// Do nothing
	}
	public function enable() {
		// Do nothing
	}
	public function get_errors() {
		// Do nothing
		return array();
	}
	/**
	 * For now, we don't offer any listeners
	 */
	public function get_listeners() {
		return array();
	}
	public function hide() {
		// We're always hidden
	}
	public function show() {
		// We're always hidden
	}
}

abstract class CommonMultiOptionPrompt extends CommonPrompt_Base {

}

class CommonRadioPrompt extends CommonMultiOptionPrompt {
	protected function init_zend_element($name) {
		$el = new Zend_Form_Element_Radio($name);
		return $el;
	}

	public function add_prompt_option($value, $text, $subtext=null) {
		if ($text == null) {
			$text = $value;
		}
		$this->zend_element->addMultiOption($value, $text);
	}
}
class CommonSelectPrompt extends CommonRadioPrompt {
	protected function init_zend_element($name) {
		$el = new Zend_Form_Element_Select($name);
		return $el;
	}

	public function add_prompt_option($value, $text, $selectable=true) {
		parent::add_prompt_option($value, $text);
	}
}
class CommonCheckboxPrompt extends CommonMultiOptionPrompt {
	protected function init_zend_element($name) {
		$el = new Zend_Form_Element_MultiCheckbox($name);
		return $el;
	}

	/**
	 * @todo get rid of the nbsps
	 */
	public function add_prompt_option($value, $text, $subtext=null) {
		if ($subtext) {
			$text .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $subtext;
		}
		$this->zend_element->addMultiOption($value, $text);
	}

	public function process() {
		parent::process();
	}
}

/**
 * @todo test
 * @todo document
 */
abstract class CommonCompositePrompt extends CommonPromptGroup implements CommonPrompt {

	public function __construct($name, $value, $text, $subtext=null, $required=false) {
		parent::__construct();
		$this->text = $text;
		$this->subtext = $subtext;
		$this->init_prompts($name, $value, $required);
	}

	protected function map($method) {
		foreach ($this->get_promptlist() as $prompt) {
			$prompt->$method();
		}
	}

	public function delete_value() {
		$this->map('delete_value');
	}

	public function disable() {
		$this->map('disable');
	}

	public function enable() {
		$this->map('enable');
	}

	public function hide() {
		$this->map('hide');
	}

	public function show() {
		$this->map('show');
	}

	public function process() {
		$this->map('process');
	}

	public function set_required($required) {
		foreach ($this->get_promptlist() as $prompt) {
			$prompt->set_required($required);
		}
	}

	public function is_valid() {
		// Return true if all subprompts are valid
		foreach ($this->get_promptlist() as $prompt) {
			if (!$prompt->is_valid()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @todo this is causing errors
	 */
	public function get_listeners() {
		return array();
	}

	public function get_errors() {
		$errors = array();
		foreach ($this->get_promptlist() as $prompt) {
			$errors = array_merge($errors, $prompt->get_errors());
		}
		return $errors;
	}

	public function display() {

		$output = '<div class="common_composite_prompt">' . "\n";

		foreach ($this->get_componentlist() as $object) {

			// Call the display method for this prompt and retrieve the prompt HTML
			$output .= $object->display();

		}

		$output .= '</div>';

		return $output;

	}

	abstract public function set_value($value);
	abstract protected function init_prompts($parentname, $value, $required);
}

class CommonDatePrompt extends CommonCompositePrompt {

	/**
	 * Construct a new date prompt
	 *
	 * @todo what if null?
	 * @param FisdapDate $date The default date value. If null, TODO
	 */
	public function __construct($name, $date, $text, $subtext=null, $required=false) {
		if (is_null($date)) {
			$date = new FisdapDateTime();
		}
		parent::__construct($name, $date, $text, $subtext, $required);
	}

	/**
	 * @todo how do we determine start/end dates?
	 */
	protected function init_prompts($parentname, $date, $required) {

		// Make a month dropdown
		$this->month = new CommonSelectPrompt('month', $date->get_month(), '', '', $required);
		$m = 0;
		while (++$m <= 12) {
			$new_date = new FisdapDate(2000, $m, 1);
			$this->month->add_prompt_option($m, $new_date->get_formatted_date('M'));
		}
		$this->add($this->month);

		// Make a day dropdown
		$this->day = new CommonSelectPrompt('day', $date->get_day(), '', '', $required);
		$d = 0;
		while (++$d <= 31) {
			$this->day->add_prompt_option($d, $d);
		}
		$this->add($this->day);

		// Make a year dropdown
		$this->year = new CommonSelectPrompt('year', $date->get_year(), '', '', $required);
		$y = 1996;
		while (++$y <= 2009) {
			$this->year->add_prompt_option($y, $y);
		}
		$this->add($this->year);

	}

	/**
	 * @todo what to do if it's an invalid date (ticket #1482)
	 */
	public function get_promptvalue() {
		$m = $this->month->get_promptvalue();
		$d = $this->day->get_promptvalue();
		$y = $this->year->get_promptvalue();
		return new FisdapDate($y, $m, $d);
	}

	/**
	 * @param FisdapDate $date
	 */
	public function set_value($date) {
		$this->month->set_value($date->get_months());
		$this->day->set_value($date->get_day());
		$this->year->set_value($date->get_year());
	}

	/**
	 * @todo
	 */
	public function set_badinput($bool) {
	}

	public function is_valid() {
		// First validate prompts individually
		if (parent::is_valid()) {
			// Now make sure the given date is valid too
			// TODO maybe outsource this to get_promptvalue and catch the exception from there?
			$m = $this->month->get_promptvalue();
			$d = $this->day->get_promptvalue();
			$y = $this->year->get_promptvalue();
			try {
				new FisdapDate($y, $m, $d);
			} catch (FisdapInvalidArgumentException $e) {
				return false;
			}
			return true;
		}
		return false;
	}
}

class CommonLegacyPrompt implements CommonPrompt {
	public function __construct($legacyprompt) {
		return $this->myprompt = $legacyprompt;
	}
	public function set_form_state($state) {
		return $this->myprompt->set_form_state($state);
	}
	public function display() {
		return $this->myprompt->display();
		ob_start();
		$this->myprompt->display();
		return ob_get_flush();
	}
	public function get_column_width() {
		return $this->myprompt->get_column_width();
	}
	public function process() {
		$value = $this->get_promptvalue();
	}
	public function respond_formsubmithandler() {
		return $this->myprompt->respond_formsubmithandler();
	}
	public function get_name() {
		return $this->myprompt->get_name();
	}
	public function get_listeners() {
		//STUB
		return array();
	}
	public function set_badinput($badinput) {
		return $this->myprompt->set_badinput($badinput);
	}
	public function get_promptvalue() {
		return $this->myprompt->get_promptvalue();
	}
	public function is_valid() {
		$value = $this->get_promptvalue();
		return $this->myprompt->validate($value);
	}
	public function get_errors() {
		return $this->myprompt->get_errors();
	}
	public function delete_value() {
		return $this->myprompt->delete_session_values();
	}
	public function disable() {
		return $this->myprompt->disable();
	}
	public function enable() {
		return $this->myprompt->enable();
	}
	public function hide() {
		return $this->myprompt->hide();
	}
	public function show() {
		return $this->myprompt->show();
	}
	public function get_listento() {
		return $this->myprompt->get_listento();
	}
	public function get_type() {
		return $this->myprompt->get_type();
	}
	public function respond_ajax($prompt) {
		//STUB
	}
}

class CommonDecorator_CommonLabel extends Zend_Form_Decorator_Abstract {

	/**
	 * @todo I'm uncomfortable depending on getName() here
	 */
	public function render($content) {
		// Get the element we're decorating
		$element = $this->getElement();

		// Hey, this is a non-breaking space
		$suf = ' *';
		// See if this prompt is displaying as valid or not
		if ($this->getOption('valid')) {
			$class = 'mediumboldtext';
		} else {
			$class = 'mediumbolderror';
		}

		// Temporarily add the vanilla Label decorator to get its markup
		$element->addDecorator(array('CommonLabel_Helper' => 'Label'), array('class' => $class, 'requiredSuffix' => $suf));
		$label = $element->renderCommonLabel_Helper();
		$element->removeDecorator('CommonLabel_Helper');

		// Add some needed stuff before and after the label
		$label .= '<img id="' . $element->getName() . '_throbber" class="common_prompt_throbber" src="/~iyoung/dev/FISDAP/images/throbber_small.gif" alt="Working...">';
		$label = '<span class="ie_v_align"></span>' . $label;

		$subtext = $element->getDescription();
		$subtext = '<span class="mediumboldtext">' . $subtext . '</span>';

		// Wrap the whole thing in a couple divs
		$label = '<div class="common_prompt_label"><div class="label_text">' . $label . '</div><div class="label_subtext">' . $subtext . '</div></div>';

		return $label . $content;
	}
}
class CommonDecorator_CommonJavascriptResponder extends Zend_Form_Decorator_Abstract {

	public function render($content) {
		// Get the element we're decorating
		$element = $this->getElement();

		$func = 'function ' . $element->getName() . '_js() {';
		$func .= 'promptHandler("' . $element->getName() . '", "", "", "");';
		$func .= '}';

		$script = '<script>' . $func . '</script>';

		return $content . $script;
	}

}

