<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/

require_once("phputil/classes/sortable_table.inc");
require_once("phputil/classes/common_form.inc");

/**
 * A searchable table
 *
 * Creates a common_table with a search bar at the top to filter the
 * contents of the table.
 *
 * @package CommonInclude
 * @author Ian Young
 * @todo filter the results earlier in the process. This will reduce sorting time.
 */
abstract class searchable_table  extends sortable_table  implements AjaxComponent {

	/**
	 * The term to filter by
	 * @var string
	 */
	private $search_term;
	/**
	 * Which columns should be included in the search
	 *
	 * Useful for excluding things like images or checkbox columns from a search.
	 * @var array|string An array of column indices, or 'all'.
	 */
	private $search_columns;
	/**
	 * The form that controls searching
	 * @var common_form
	 */
	private $search_form;
	/**
	 * Is this table searchable?
	 * @var boolean
	 */
	private $searchable;

	public function __construct() {
		parent::__construct();
		$this->search_columns = 'all';
		//$this->search_columns = array_keys($this->colinfo);
	}

	public function process() {

		if ($this->is_searchable()) {
			// If we're responding to a search event, populate
			if ($_POST['event_type'] == 'search') {
				$this->populate();
			}

			// Build and process the filter form
			$this->searchform = $this->build_searchform();
			$valid = $this->searchform->process();
		}

		$result = parent::process();

		// We'll need this later
		foreach ($this->rowinfo as $rowidx => &$thisrowinfo) {
			$thisrowinfo['name'] = $rowidx;
		}


		if ($this->is_searchable() && $this->is_populated()) {
			// If a filter is set, filter the list by that
			$phrase = common_utilities::get_scriptvalue('table_filter_phrase');
			if ($phrase && $this->is_searchable()) {
				$this->set_search_term($phrase);
				$this->rowinfo = array_filter($this->rowinfo, array($this, 'row_matches'));
			}
		}

		return $result;

	}

	public function display_html(&$common_dest) {
		if ($this->search_columns == 'all') {
			$this->search_columns = array_keys($this->columninfo);
		}
		if ($this->is_searchable() && is_array($this->search_columns)) {
			$form = $this->searchform;
			$output = '<div class="interactive_table_search">';
			$output .= $form->get_generated_html_form();
			$output .= '</div>';
			$common_dest->add($output);
		}
		parent::display_html($common_dest);
	}

	/**
	 * Set searchable status
	 *
	 * Set the list to be searchable or not.  If not, the filter box will not appear.
	 * @param boolean
	 */
	public function set_searchable($bool=true) {
		$this->searchable = $bool;
	}

	/**
	 * Is table searchable?
	 * @return boolean
	 */
	public function is_searchable() {
		return $this->searchable;
	}

	/**
	 * Set the term to filter by
	 *
	 * If this is set, the table will only display rows that contain the tezt in $term.
	 * @param string the text to filter by
	 */
	public function set_search_term($term) {
		$this->search_term = $term;
	}

	/**
	 * Does the search term match this row?
	 *
	 * Determines if the current search term appears in the row.
	 * @param array
	 * @return boolean
	 */
	private function row_matches($rowinfo) {
		$name = $rowinfo['name'];
		$row = $this->tabledata[$name];

		if ($this->search_term) {
			foreach ($row as $cell) {
				if (stripos($cell, $this->search_term) !== false) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Build the search form
	 *
	 * Creates the form used for filtering this table.
	 * @return common_form
	 */
	private function build_searchform() {
		$newform = new common_miniform();
		$newform->set_dispatch_id('interactivetable_event');
		$newform->set_mode('query');
		$newform->set_name($this->get_prefix().'_search_form');
		$newform->set_layout('form_float', 'right');

		$textbox = new common_prompt('table_filter_phrase', '', null, null, 'text', false);
		$newform->add($textbox, 1);

		$event = new common_prompt('event_type', 'search', null, null, 'hidden', false);
		$newform->add($event, 1);
		$tableid = new common_prompt('table_id', $this->get_table_id(), null, null, 'hidden', false);
		$newform->add($tableid, 1);

		return $newform;
	}

	public function respondAJAX() {
		if ($_POST['event_type'] == 'search') {
			echo_ajax('div', $this->get_inner_div_id(), $this->print_table());
			return true;
		} else {
			return parent::respondAJAX();
		}
	}

}
