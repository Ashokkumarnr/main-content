<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/DbStatementBuilder.inc');
require_once('phputil/classes/model/AbstractModel.inc');
require_once('phputil/exceptions/FisdapDatabaseStatementException.inc');

/**
 * This class builds MySQL statements.
 */
final class MySqlStatementBuilder implements DbStatementBuilder 
{
	const SQL_VALUE_METHOD = 'get_as_sql_value';

    public function get_array_insert_statement($table_name, $columns_and_values)
    {
        $columns = array();
        $values = array();

        foreach ($columns_and_values as $column=>$value) {
            $columns[] = $column;
			$values[] = $this->get_clause_value($value);
        }

        $statement = "INSERT INTO $table_name (" .
            join(',', $columns) . ') VALUES (' .
            join(',', $values) . ')';
        return $statement;
    }

    public function get_array_update_statement($table_name, $columns_and_values)
    {
        $clauses = $this->get_clauses($columns_and_values);

        $statement = "UPDATE $table_name"; 
        if (count($clauses)) {
            $statement .= ' SET ' .  join(',', $clauses); 
        }

        return $statement;
    }

    public function get_array_delete_statement($table_name, $columns_and_values)
    {
        $clauses = $this->get_condition_clauses($columns_and_values);

        $statement = "DELETE FROM $table_name"; 
        if (count($clauses)) {
            $statement .= ' WHERE ' .  join(' AND ', $clauses); 
        }

        return $statement;
    }

    public function get_clauses($columns_and_values) {
        $clauses = array();

        foreach ($columns_and_values as $column=>$value) {
            $clauses[] = $this->get_clause($column, $value);
        }

        return $clauses;
    }

    public function get_condition_clauses($columns_and_values) {
        $clauses = array();

        foreach ($columns_and_values as $column=>$value) {
            $clauses[] = $this->get_condition_clause($column, $value);
        }

        return $clauses;
    }

    public function get_clause($column, $value) {
        Assert::is_not_empty_trimmed_string($column);

        return $column . '=' . $this->get_clause_value($value);
    }

    public function get_condition_clause($column, $value) {
        Assert::is_not_empty_trimmed_string($column);
        Assert::is_true(
            Test::is_object($value) ||
            Test::is_array($value) ||
            Test::is_int($value) ||
            Test::is_string($value) ||
            Test::is_boolean($value) ||
            Test::is_null($value));

        if (!is_array($value)) {
            $value = $this->get_scalar_value($value);
		}

        if (is_null($value)) {
            return "($column IS NULL)";
        }

        if (!is_array($value)) {
            return "($column=" . $this->get_clause_value($value). ")";
        }

        $n = count($value);
        if ($n == 0) {
			// This is open to interpretation.
            return "($column=$column)";
        }

        if ($n == 1) {
            return $this->get_condition_clause($column, $value[0]);
        }

        $values = array();
        foreach ($value as $v) {
            $values[] = $this->get_clause_value($v);
        }

        return "($column IN (" . join(',', $values) . '))';
    }

    public function get_not_condition_clause($column, $value) {
        Assert::is_not_empty_trimmed_string($column);
        Assert::is_true(
            Test::is_object($value) ||
            Test::is_array($value) ||
            Test::is_int($value) ||
            Test::is_string($value) ||
            Test::is_boolean($value) ||
            Test::is_null($value));

        if (!is_array($value)) {
            $value = $this->get_scalar_value($value);
		}

        if (is_null($value)) {
            return "($column IS NOT NULL)";
        }

        if (!is_array($value)) {
            return "($column!=" . $this->get_clause_value($value). ")";
        }

        $n = count($value);
        if ($n == 0) {
			// This is open to interpretation.
            return "($column=$column)";
        }

        if ($n == 1) {
            return $this->get_not_condition_clause($column, $value[0]);
        }

        $values = array();
        foreach ($value as $v) {
            $values[] = $this->get_clause_value($v);
        }

        return "($column NOT IN (" . join(',', $values) . '))';
    }

    public function get_clause_value($value) {
        Assert::is_true(
			Test::is_object($value) ||
            Test::is_int($value) ||
            Test::is_string($value) ||
            Test::is_boolean($value) ||
            Test::is_float($value) ||
            Test::is_null($value));

		if (Test::is_object($value)) { 
			$value = $this->get_scalar_value($value);
		}

        if (is_null($value)) return "NULL";
        if (is_bool($value)) return (int)$value;
        if (Test::is_int($value)) return (int) $value;

		return '"' . addslashes($value) . '"';
    }

	private function get_scalar_value($value) {
		if (Test::is_boolean($value)) return (int)$value;
		if (!Test::is_object($value)) return $value;

		Assert::is_true(method_exists($value, self::SQL_VALUE_METHOD));
		return call_user_func(array($value, self::SQL_VALUE_METHOD));
	}

    public function get_column_name(ModelCriteria $criteria, $column_name) {
        $alias = $criteria->get_table_name_alias();
        if ($alias == '') return $column_name;

        return $alias . '.' . $column_name;
    }

    public function get_table_name_clause(ModelCriteria $criteria, $table_name) {
        $alias = $criteria->get_table_name_alias();
        if ($alias == '') return $table_name;

        return $table_name . ' ' . $alias;
    }

    public function get_select_statement_by_criteria(ModelCriteria $criteria) {
        $model = $criteria->get_model();

        // Get the columns to include.
        $field_names = $criteria->get_field_names();

        if (!count($field_names)) {
            $names = '*';
        }
        else {
			$fields = $model->get_fieldmap();

			//TODO this is terrible, do something different
			$fields['id'] = $model->db_idfield;

            $names = array();

            foreach ($field_names as $field_name) {
                $column = array_search($field_name, $fields);
                if ($column === false) {
                    throw new FisdapDatabaseException(
                        "unknown field[$field_name]"); 
                }

                $names[] = $this->get_column_name($criteria, $column);
            }

            $names = join(',', $names);
        }

        $distinct = $criteria->is_distinct() ? ' DISTINCT' : '';

        $statement = "SELECT$distinct $names FROM " . 
            $this->get_table_name_clause($criteria, $model->get_table_name()) .
            $this->get_where_clause_by_criteria($criteria) .
            $this->get_order_by_clause_by_criteria($criteria);

        // Add on the limit.
        $limit = $criteria->get_statement_limit();
        if (!is_null($limit)) {
            $statement .= " LIMIT $limit";
        }

        return $statement;
    }

    public function get_delete_statement_by_criteria(ModelCriteria $criteria) {
        $model = $criteria->get_model();
        $statement = 'DELETE FROM ' . 
            $this->get_table_name_clause($criteria, $model->get_table_name()) .
            $this->get_where_clause_by_criteria($criteria);

        // Add on the limit.
        $limit = $criteria->get_statement_limit();
        if (!is_null($limit)) {
            $statement .= " LIMIT $limit";
        }

        return $statement;
    }

    public function get_update_statement_by_criteria(ModelCriteria $criteria, 
        ModelCriteria $update_criteria) {

        $model = $criteria->get_model();
        $statement = 'UPDATE ' . 
            $this->get_table_name_clause($criteria, $model->get_table_name()); 

        // Add on the field updates.
        $columns_and_values = array();

		$fields = $model->get_fieldmap();

        foreach ($fields as $db=>$field) {
            if (!$update_criteria->was_called("set_$field")) continue;

            $getter = "get_$field";
            $columns_and_values[$this->get_column_name($update_criteria, $db)] = 
                $update_criteria->$getter();
        }

        $clauses = $this->get_clauses($columns_and_values);
        if (count($clauses)) {
            $statement .= ' SET ' . join(',', $clauses);
        }

        // Add on the condition.
        $statement .= $this->get_where_clause_by_criteria($criteria);

        // Add on the limit.
        $limit = $criteria->get_statement_limit();
        if (!is_null($limit)) {
            $statement .= " LIMIT $limit";
        }

        return $statement;
    }

    public function get_order_by_clause_by_criteria(ModelCriteria $criteria) {
		$fields = $criteria->get_model()->get_fieldmap();
        $order_bys = $criteria->get_order_by();
        $clauses = array();

        foreach ($order_bys as $order_by) {
            $field_name  = $order_by->get_field_name();
            $column = array_search($field_name, $fields);
            if ($column === false) {
                throw new FisdapDatabaseException(
                    "order-by on unknown field[$field_name]"); 
            }

            if ($order_by->is_ascending()) {
                $clauses[] = $this->get_column_name($criteria, $column);
            }
            else {
                $clauses[] = $this->get_column_name($criteria, $column) . ' DESC';
            }
        }

        if (count($clauses)) {
            return ' ORDER BY ' . join(',', $clauses);
        }

        return '';
    }

    public function get_where_clause_by_criteria(ModelCriteria $criteria) {
        $condition = $this->get_condition_by_criteria($criteria);
        if ($condition != '') {
            $condition = " WHERE $condition";
        }

        return $condition;
    }

    public function get_condition_by_criteria(ModelCriteria $criteria) {
        $columns_and_values = array();

		$fields = $criteria->get_model()->get_fieldmap();

		//TODO this is terrible, do something different
		$fields[$criteria->get_model()->db_idfield] = 'id';

        foreach ($fields as $db=>$field) {
            if (!$criteria->was_called("set_$field")) continue;

            $getter = "get_$field";
            $columns_and_values[$this->get_column_name($criteria, $db)] = 
                $criteria->$getter();
        }

        $clauses = $this->get_condition_clauses($columns_and_values);

        // The IN value / NOT IN value clauses.
		$data = array(
			array('IN', 'get_condition_clause', $criteria->get_in()), 
			array('NOT IN', 'get_not_condition_clause', $criteria->get_not_in())
		);

        foreach ($data as $d) {
			$data_name = $d[0];
			$func = $d[1];
			$items = $d[2];

            foreach ($items as $field_name=>$values) {
                $n = count($values);
                if (!$n) continue;

                $column = array_search($field_name, $fields);
                if ($column === false) {
                    throw new FisdapDatabaseException(
                        "$data_name clause on unknown field[$field_name]"); 
                }

				$column_name = $this->get_column_name($criteria, $column); 
				$clauses[] = $this->$func($column_name, $values);
            }
        }

        // The IN criteria / NOT IN criteria clauses.
        $data = array('IN' => $criteria->get_in_criteria(), 
            'NOT IN' => $criteria->get_not_in_criteria());
        foreach ($data as $data_name=>$d) {
            foreach ($d as $field_name=>$field_criteria) {
                $column = array_search($field_name, $fields);
                if ($column === false) {
                    throw new FisdapDatabaseException(
                        "$data_name criteria clause on unknown field[$field_name]"); 
                }

                $clauses[] = '(' . $this->get_column_name($criteria, $column) .
                    " $data_name (" . 
                    $this->get_select_statement_by_criteria($field_criteria) . '))';
            }
        }

        // The NOT criteria clause.
        $not_criteria = $criteria->get_not_criteria();
        foreach ($not_criteria as $c) {
            $condition = $this->get_condition_by_criteria($c);
            if ($condition != '') {
                $clauses[] = "NOT ($condition)";
            }
        }

        // Additional AND clauses.
        $clauses = array_merge($clauses,
            $this->add_clause_parens($criteria->get_and_clauses()));

        // The OR criteria clause.
        $or_clauses = array();
        $or_criteria = $criteria->get_or_criteria();
        foreach ($or_criteria as $c) {
            $condition = $this->get_condition_by_criteria($c);
            if ($condition != '') {
                $or_clauses[] = "($condition)";
            }
        }

        // Additional OR clauses.
        $or_clauses = array_merge($or_clauses,
            $this->add_clause_parens($criteria->get_or_clauses()));

        $condition = join(' AND ', $clauses);
        $or_condition = join(' OR ', $or_clauses);
        if ($condition == '') {
            $condition = $or_condition;
        }
        elseif ($or_condition != '') {
            $condition = "($condition) OR $or_condition";
        }

        return $condition;
    }

    public function add_clause_parens($clauses) {
        Assert::is_array($clauses);

        $list = array();
        foreach ($clauses as $clause) {
            $list[] = "($clause)";
        }

        return $list;
    }
}
?>
