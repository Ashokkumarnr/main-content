<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/exceptions/FisdapException.inc');

/**
 * An object cache.
 * Cache keys are always converted to strings.
 * If a cache is disabled:
 * <ul>
 * <li>get() returns NULL.
 * <li>put() is ignored.
 * <li>contains() will return FALSE.
 * </ul>
 */
interface FisdapCache {
	/**
	 * Clear the cache.
	 */
	public function clear(); 

	/**
	 * Indicate that no hit occurred.
	 */
	public function clear_hit();

	/**
	 * Determine if the cache contains a model.
	 * @param int | string $key The key.
	 */
	public function contains($key); 

	/**
	 * Retrieve an item from the cache.
	 * @param int | string $key The key.
	 * @return null | mixed The item, or null if not found.
	 */
	public function get($key); 

	/**
	 * Retrieve the known keys.
	 * @return array The cache keys.
	 */
	public function get_keys(); 

	/**
	 * Retrieve the cache name.
	 * @return string The cache name.
	 */
	public function get_name(); 

	/**
	 * Determine whether the last cache access resulted in a hit.
	 * The access could be a get() or a contains().
	 */
	public function is_hit(); 

	/**
	 * Add/replace an item in the cache.
	 * @param int | string $key The key.
	 * @param mixed $item The item to add.
	 */
	public function put($key, $item); 

	/**
	 * Indicate whether the cache is enabled.
	 * @param boolean $enabled TRUE if enabled.
	 */
	public function set_enabled($enabled); 
}
?>
