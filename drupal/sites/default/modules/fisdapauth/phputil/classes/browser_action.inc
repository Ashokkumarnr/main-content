<?php
/**
 * An action that is performed on the client side.
 */
abstract class BrowserAction {
    const VALUE_DELIMITER = '**!**';

    /**
     * Retrieve the action as a string to be sent to the browser.
     * @return string The action as a string.
     */
    public abstract function encode_for_browser();
}

class RedirectBrowserAction extends BrowserAction {
    private $url;
    private $window;

    /**
     * Constructor.
     * @param string $url The URL to redirect to.
     * @param string $window The javascript window to redirect to the URL.
     */
    public function RedirectBrowserAction($url, $window='window.top') {
        $this->url = $url;
        $this->window = $window;
    }

    /**
     * @see BrowserAction::encode_for_browser()
     */
    public function encode_for_browser() {
        $code = "{$this->window}.location.href='$this->url';";
        $values = array('js', $code);
        return join(parent::VALUE_DELIMITER, $values);
    }
}

/**
 * An action that updates the contents of an element.
 * The inner HTML of the element is set to its new value.
 */
class InnerHtmlBrowserAction extends BrowserAction {
    private $id;
    private $html;
    private $mode;

    /**
     * Constructor.
     * @param string $id The DOM element ID.
     * @param string $html The inner HTML.
     * @param string $mode How the new content will be added to the element. May 
	 * be 'prepend', 'append', or 'overwrite'.
     */
    public function InnerHtmlBrowserAction($id, $html, $mode='overwrite') {
        $this->id = $id;
        $this->html = $html;
        $this->mode = $mode;
    }

    /**
     * @see BrowserAction::encode_for_browser()
     */
    public function encode_for_browser() {
        $values = array('innerHtml', $this->id, $this->html, $this->mode);
        return join(parent::VALUE_DELIMITER, $values);
    }
}

/**
 * An action that runs Javascript.
 */
class JavascriptBrowserAction extends BrowserAction {
    private $script;

    /**
     * Constructor.
     * @param string $script The Javascript code.
     */
    public function JavascriptBrowserAction($script) {
        $this->script= $script;
    }

    /**
     * @see BrowserAction::encode_for_browser()
     */
    public function encode_for_browser() {
        $values = array('js', $this->script);
        return join(parent::VALUE_DELIMITER, $values);
    }
}

/**
 * An action that alerts the user via a message.
 */
class AlertBrowserAction extends BrowserAction {
    private $msg;

    /**
     * Constructor.
     * @param string $msg The message to show the user.
     */
    public function AlertBrowserAction($msg) {
        $this->msg = $msg;
    }

    /**
     * @see BrowserAction::encode_for_browser()
     */
    public function encode_for_browser() {
        $values = array('alert', $this->msg);
        return join(parent::VALUE_DELIMITER, $values);
    }
}

/**
 * An action that sets data on the client side.
 * The data is set in an associative array.
 */
class DataBrowserAction extends BrowserAction {
    private $name;
    private $value;

    /**
     * Constructor.
     * @param string $name The key into the array.
     * @param string $value The array value.
     */
    public function DataBrowserAction($name, $value) {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @see BrowserAction::encode_for_browser()
     */
    public function encode_for_browser() {
        $values = array('data', $this->name, $this->value);
        return join(parent::VALUE_DELIMITER, $values);
    }
}

/**
 * An action that gets confirmation from the user.
 * If the user confirms, then the name passed on the constructor
 * will be set to the string value '1'; otherwise it is '0'.
 */
class ConfirmBrowserAction extends BrowserAction {
    private $msg;
    private $name;

    /**
     * Constructor.
     * @param string $msg The message to show the user.
     * @param string $name The GET/POST parameter name to set.
     */
    public function ConfirmBrowserAction($msg, $name) {
        $this->msg = $msg;
        $this->name = $name;
    }

    /**
     * @see BrowserAction::encode_for_browser()
     */
    public function encode_for_browser() {
        $values = array('confirm', $this->msg, $this->name);
        return join(parent::VALUE_DELIMITER, $values);
    }
}

/**
 * An action that indicates an error occurred.
 */
class ErrorBrowserAction extends BrowserAction {
    private $html;

    /**
     * Constructor.
     * @param string $html The inner HTML.
     */
    public function ErrorBrowserAction($html) {
        $this->html = $html;
    }

    /**
     * @see BrowserAction::encode_for_browser()
     */
    public function encode_for_browser() {
        $values = array('error', $this->html);
        return join(parent::VALUE_DELIMITER, $values);
    }
}

?>
