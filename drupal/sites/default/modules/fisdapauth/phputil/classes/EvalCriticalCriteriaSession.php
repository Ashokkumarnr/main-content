<?php

/**
 * This class models a FISDAP Eval Critical Criteria Session
 */
class FISDAP_EvalCriticalCriteriaSession extends DataEntryDbObj {    

    /** 
     * Create a new Critical Criteria Session with the specified id and the specified db link
     */
    function FISDAP_EvalCriticalCriteriaSession( $id, $connection ) {
        $this->DbObj('Eval_CriticalCriteriaSessions',                           //table name
                     $id,                                                       //id
                     'CritCriteriaSes_id',                                      //id field
                     'Shift',                                                   //parent entity name
                     array(),                                                   //children
                     $connection );                                             //database connection
        $field_map = array(
            'CritCriteriaSes_id' => 'CritCriteriaSes_id', 
            'EvalSession_id' => 'EvalSession_id',     
            'Criterion_id' => 'Criterion_id',       
            'Value' => 'Value'        
            ); 
        $this->set_field_map( $field_map );
    }//constructor FISDAP_EvalCriticalCriteriaSession
}//class FISDAP_EvalCriticalCriteriaSession

?>
