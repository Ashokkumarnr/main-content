<?php

/**
 * This class models a FISDAP Eval Comment Session
 */
class FISDAP_EvalCommentSession extends DataEntryDbObj {    

    /** 
     * Create a new Comment Session with the specified id and the specified db link
     */
    function FISDAP_EvalCommentSession( $id, $connection ) {
        $this->DbObj('Eval_Comment_Session',                                    //table name
                     $id,                                                       //id
                     'CommentSession_id',                                       //id field
                     'Shift',                                                   //parent entity name
                     array(),                                                   //children
                     $connection );                                             //database connection
        $field_map = array(
            'CommentSession_id' => 'CommentSession_id', 
            'Comment_id' => 'Comment_id',        
            'EvalSession_id' => 'EvalSession_id',    
            'EnteredText' => 'EnteredText'
            ); 
        $this->set_field_map( $field_map );
    }//constructor FISDAP_EvalCommentSession
}//class FISDAP_EvalCommentSession

?>
