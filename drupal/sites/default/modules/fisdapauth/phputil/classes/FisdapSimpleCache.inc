<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FisdapCache.inc');
require_once('phputil/exceptions/FisdapException.inc');

/**
 * A simple ID based cache for objects.
 */
final class FisdapSimpleCache implements FisdapCache {
	private $cache = array();
	private $enabled = true;
	private $got_hit = false;
	private $hits = 0;
	private $misses = 0;
	private $name;

	/**
	 * Constructor.
	 * The cache is enabled and empty.
	 * @param string $name The cache name.
	 */
	public function __construct($name) {
		Assert::is_not_empty_trimmed_string($name);
		$this->name = $name;
	}

	public function get_name() {
		return $this->name;
	}

	public function is_hit() {
		return $this->got_hit;
	}

	public function clear_hit() {
		$this->got_hit = false;
	}

	public function set_enabled($enabled) {
		Assert::is_boolean($enabled);
		$this->enabled = $enabled;
	}

	/**
	 * Retrieve the key from the source.
	 * We map the key just in case a user name is all digits.
	 * @param int|string $key The key.
	 */
	private function map_key($key) {
        Assert::is_true(
            Test::is_int($key) ||
            Test::is_string($key)); 

		return (string) $key;
	}

	public function contains($key) {
		if ($this->enabled) {
			$this->got_hit = array_key_exists($this->map_key($key), $this->cache);
		}
		else {
			$this->got_hit = false;
		}

		if ($this->got_hit) {
			++$this->hits;
		}
		else {
			++$this->misses;
		}

		return $this->got_hit;
	}

	public function clear() {
		$this->cache = array();
		$this->hits = 0;
		$this->misses = 0;
	}

    public function __toString() {
		$stats = array(
			'enabled=' . ($this->enabled ? 'y' : 'n'), 
			'size=' . count($this->cache),
			'hits=' . $this->hits,
			'misses=' . $this->misses
		);

		return $this->name . '(' . join(',', $stats) . ')';
    }

	public function put($key, $item) {
		if (!$this->enabled) return;
		$this->cache[$this->map_key($key)] = $item;
	}

	public function get($key) {
		if (!$this->contains($key)) return false;
		return $this->cache[$this->map_key($key)];
	}

	public function get_keys() {
		return array_keys($this->cache);
	}
}
?>
