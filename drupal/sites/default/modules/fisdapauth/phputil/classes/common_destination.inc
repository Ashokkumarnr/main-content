<?php

/**
 * @package CommonInclude
 * @author Warren Jacobson
 * @author Ian Young
 *
 * Destination classes for use with outputting common include objects.
 */

interface common_destination {
    /**
     * Add this output to the destination
     * @param string
     */
     public function add($output);
    /**
     * Display everything in this destination
     * @return mixed
     */
    public function display();
}

/**
 * Defines a chunk of HTML output
 */
class common_html implements common_destination {

	/**
	 * The HTML text markup
	 */
	var $html;

    /**
     * Create a new instance of the common_html class
     */
    function common_html() {

        $this->html = null; // No markup just yet

    }

    /**
     * Add markup to the the common_html class
     */
    function add($output) {

        $this->html .= $output;

    }

    /**
     * Display this chunk of HTML markup to the browser
     */
    function display() {

        echo $this->html;

    }

}

/**
 * Exactly like common_html, except returns a string on display
 */
class common_string extends common_html implements common_destination {

    /**
     * Does not echo any output, just returns it
     * @return string the contents of this destination
     */
    public function display() {
        return $this->html;
    }

}

/**
 * Defines a chunk of text (tab delimited) output
 */
class common_text implements common_destination {

	/**
	 * The tab-delimited text
	 */
	var $text;

    /**
     * Create a new instance of the common_text class
     */
    function common_text() {

        $this->text = null; // No text just yet

    }

    /**
     * Add text to the the common_text class
     */
    function add($output) {

        $this->text .= $output;

    }

    /**
     * Display this chunk of text to the browser
     */
    function display() {

        $filename = 'tsv_report_' . time() . rand(0,10000) . '.tsv';

        $filehandle = fopen('/var/www/html/FISDAP/temp/' . $filename,'w+');
        fwrite($filehandle,$this->text);
        fclose($filehandle);

        echo '<div class="mediumtext" style="text-align: center"><br />Your tab-delimited report is ready to download.<br />';
        echo '<a href="/temp/' . $filename . '">Click Here</a> to download your report.</div>';

    }

}

/**
 * Defines a PDF document
 */
class common_pdf {

	/**
	 * The title of the document
	 */
	var $title;
	/**
	 * The author of the document
	 */
	var $author;
	/**
	 * Either 'portrait' or 'landscape'
	 */
	var $orientation;

	/**
	 * In points
	 */
	var $leftmargin;
	/**
	 * In points
	 */
	var $rightmargin;
	/**
	 * In negative points
	 */
	var $topmargin;
	/**
	 * In negative points
	 */
	var $bottommargin;

	/**
	 * In points
	 */
	var $leftpos;
	/**
	 * In points
	 */
	var $rightpos;
	/**
	 * In points
	 */
	var $toppos;
	/**
	 * In points
	 */
	var $bottompos;

	/**
	 * An array of font sizes measured in inches
	 */
	var $fontsize;
	/**
	 * large,medium,small,tiny
	 */
	var $currentsize;
	/**
	 * The PDFlib font handle of the currently selected font
	 */
	var $font;

	/**
	 * The current cursor position along the X axis measured left to right in points
	 */
	var $xpos;
	/**
	 * The current cursor position along the Y axis measured top to bottom in neg points
	 */
	var $ypos;
	/**
	 * The current page number
	 */
	var $pagenumber;

	/**
	 * Indicates if we're suspending/resuming pages or ending/starting them
	 */
	var $suspendstate;
	/**
	 * Indicates the highest suspended page number
	 */
	var $suspagenumber;

	/**
	 * An array indicating pages that have begun via begin_page
	 */
	var $pagebegun;
	/**
	 * An array indicating pages that have ended via end_page
	 */
	var $pageended;

	/**
	 * An instance of PDFlib's PDF object
	 */
	var $pdf;

    /**
     * Create a new instance of the pdf class
     */
    function common_pdf($title,$author,$orientation) {

        $this->pdf = PDF_new(); // Create an instance of PDFlib's PDF object
        PDF_set_parameter($this->pdf,"license","L700102-010500-732188-XTAG92-HK3G42");
        if (!PDF_begin_document($this->pdf,"","")) $this->displayerror(); // Begin the document
        $this->suspendstate = 0; // Initialize the page suspend state
        PDF_set_info($this->pdf,"Creator",$_SERVER['HTTP_HOST']);
        $this->set_title($title); $this->set_author($author); $this->set_orientation($orientation);
        $this->pagenumber = 0; $this->begin_page(); // Create a new page

        $this->leftpos = $this->leftmargin;
        $this->rightpos = $this->rightmargin;
        $this->toppos = $this->ypos;
        $this->bottompos = $this->bottommargin;

    }

    /**
     * Set the title of the document
     */
    function set_title($title) {

        $this->title = $title;
        PDF_set_info($this->pdf,"Title",$this->title);

    }

    /**
     * Set the author of the document
     */
    function set_author($author) {

        $this->author = $author;
        PDF_set_info($this->pdf,"Author",$this->author);

    }

    /**
     * Set the orientation: 'portrait' or 'landscape'
     */
    function set_orientation($orientation) {

        if ($orientation != 'portrait' and $orientation != 'landscape') {
            $orientation = 'portrait'; // Default to portrait of null or bad orientation sent
        }

        $this->orientation = $orientation;

        if ($this->orientation == "portrait") {
            $this->leftmargin = $this->points(.5); // 1/2in margin on the left
            $this->rightmargin = $this->points(8); // 1/2in margin on the right for letter sized
            $this->topmargin = $this->points(-.5); // 1/2in margin on the top (Y axis is negative)
            $this->bottommargin = $this->points(-10.5); // 1/2in margin on the bottom for letter sized
        }
        else {
            $this->leftmargin = $this->points(.5); // 1/2in margin on the left
            $this->rightmargin = $this->points(10.5); // 1/2in margin on the right for letter size
            $this->topmargin = $this->points(-.5); // 1/2in margin on the top (Y axis is negative)
            $this->bottommargin = $this->points(-8); // 1/2in margin on the bottom for letter sized
        }

    }

    /**
     * End one page and create another
     */
    function next_page() {

        $this->end_page(); // End a page
        $this->begin_page(); // Create a new page

    }

    /**
     * Create a new page
     */
    function begin_page() {

        $this->pagenumber++; // Increment the page number
        $this->pagebegun[$this->pagenumber] = true;
        if ($this->suspendstate > 0 and $this->pagenumber <= $this->suspagenumber) {
            $optlist = "pagenumber=" . $this->pagenumber;
            PDF_resume_page($this->pdf,$optlist);
        }
		else {
			PDF_begin_page_ext($this->pdf,612,792,""); // Letter size, expressed in points
			if ($this->orientation == 'portrait') {
				PDF_translate($this->pdf,1,792); // Coordinate system change from lower left to upper left
			}
			else {
				PDF_rotate($this->pdf,90); // Rotate the coordinate system 90 degrees counterclockwise
			}

			$this->set_font("medium"); // The page needs a font just in case. Likely to be overriden
			$this->set_text_pos($this->leftmargin,$this->topmargin); // Upper left corner, with margins

			// Print debugging gridlines

			$debug_spacing = read_session_field('pdf_debug_spacing');
			if ($debug_spacing == null) $debug_spacing = array();
			if ($debug_spacing[0] + 0 != 0 or $debug_spacing[1] + 0 != 0) {
				$x_max = 612; if ($this->orientation == 'landscape') $x_max = 792;
				$y_max = -792; if ($this->orientation == 'landscape') $y_max = -612;
				$red = 1; $green = 1; $blue = 0; # Yellow
				PDF_setcolor($this->pdf,"stroke","rgb",$red,$green,$blue,null);
				PDF_setlinewidth($this->pdf,.5);
				if ($debug_spacing[0] + 0 != 0) {
					for ($x = 0; $x <= $x_max; $x += $debug_spacing[0]) {
						if (($x / ($debug_spacing[0] * 5)) != floor($x / ($debug_spacing[0] * 5))) {
							PDF_moveto($this->pdf,$x,0);
							PDF_lineto($this->pdf,$x,$y_max);
							PDF_stroke($this->pdf);
						}
					}
				}
				if ($debug_spacing[1] + 0 != 0) {
					for ($y = 0; $y >= $y_max; $y -= $debug_spacing[1]) {
						if (($y / ($debug_spacing[1] * 10)) != floor($y / ($debug_spacing[1] * 10))) {
							PDF_setcolor($this->pdf,"stroke","rgb",$red,$green,$blue,null);
							PDF_moveto($this->pdf,0,$y);
							PDF_lineto($this->pdf,$x_max,$y);
							PDF_stroke($this->pdf);
						}
					}
				}
				$red = 1; $green = 0; $blue = 0; $redline_cnt++; # Red
				PDF_setcolor($this->pdf,"stroke","rgb",$red,$green,$blue,null);
				if ($debug_spacing[0] + 0 != 0) {
					for ($x = 0; $x <= $x_max; $x += $debug_spacing[0]) {
						if (($x / ($debug_spacing[0] * 5)) == floor($x / ($debug_spacing[0] * 5))) {
							PDF_moveto($this->pdf,$x,0);
							PDF_lineto($this->pdf,$x,$y_max);
							PDF_stroke($this->pdf);
						}
					}
				}
				if ($debug_spacing[1] + 0 != 0) {
					$redline_cnt = 0;
					for ($y = 0; $y >= $y_max; $y -= $debug_spacing[1]) {
						if (($y / ($debug_spacing[1] * 10)) == floor($y / ($debug_spacing[1] * 10))) {
							PDF_moveto($this->pdf,0,$y);
							PDF_lineto($this->pdf,$x_max,$y);
							PDF_stroke($this->pdf);
							$font = PDF_load_font($this->pdf,'Helvetica',"winansi","kerning=true");
							PDF_setfont($this->pdf,$font,4);
							$optlist = "alignment=right font=" . $font . " fontsize=4 leading=4";
							$textflow = 0; # Will create a textflow object automatically
							$textflow = PDF_add_textflow($this->pdf,$textflow,$y,$optlist);
							$optlist = " firstlinedist=4";
							$result = PDF_fit_textflow($this->pdf,$textflow,2,($y + 3),15,($y - 1),$optlist);
							PDF_delete_textflow($this->pdf,$textflow);
						}
					}
				}
			}
		}

    }

    /**
     * End a page
     */
    function end_page() {

        $this->pageended[$this->pagenumber] = true;
        if ($this->suspendstate > 0) {
            PDF_suspend_page($this->pdf,'');
            if ($this->pagenumber > $this->suspagenumber) {
                $this->suspagenumber = $this->pagenumber;
            }
        }
        else {
            PDF_end_page_ext($this->pdf,"");
        }

    }

    /**
     * Access a specific suspended page
     */
    function get_page($pagenumber) {

        if ($this->suspendstate == 0) return; // We shouldn't be getting called
        if ($this->pagenumber == $pagenumber) return; // Already on the requested page

        PDF_suspend_page($this->pdf,'');
        if ($this->pagenumber > $this->suspagenumber) {
            $this->suspagenumber = $this->pagenumber;
        }
        $optlist = "pagenumber=" . $pagenumber;
        PDF_resume_page($this->pdf,$optlist);
        $this->pagenumber = $pagenumber;

    }

    /**
     * Move our current position down to the next line
     */
    function next_line() {

        $lineheight = ($this->fontsize[$this->currentsize] * 1.25);
        $this->set_text_pos($this->leftmargin,($this->ypos - $lineheight));

    }

    /**
     * Set the current text position
     */
    function set_text_pos($x,$y) {

        $this->xpos = $x; $this->ypos = $y;
        PDF_set_text_pos($this->pdf,$this->xpos,$this->ypos);

    }

    /**
     * End the PDF document
     */
    function end_pdf() {

        PDF_end_document($this->pdf,null); // End the PDF document

    }

    /**
     * End a PDF and return it's output
     */
    function display() {

        $this->end_page(); // End a page
        $this->end_pdf(); // End the PDF document

        $buffer = PDF_get_buffer($this->pdf); // Get the raw PDF output
        $bufferlength = strlen($buffer);
        header("Pragma: cache");
        header("Content-type: application/pdf");
        header("Content-Length: " . $bufferlength);
        header("Content-Disposition: inline; filename=report.pdf");
        echo $buffer; // Display the PDF output to the browser

    }

    /**
     * Set a font: large,mediumbold,medium,smallbold,small
     */
    function set_font($size) {

        $allowed_sizes = array("large","mediumbold","smallbold","small");
        if (in_array($size,$allowed_sizes) == false) $size = "medium";

        $fontsize['large'] = 12; $fontface['large'] = 'Helvetica-Bold';
        $fontsize['mediumbold'] = 8; $fontface['mediumbold'] = 'Helvetica-Bold';
        $fontsize['medium'] = 8; $fontface['medium'] = 'Helvetica';
        $fontsize['smallbold'] = 6; $fontface['smallbold'] = 'Helvetica-Bold';
        $fontsize['small'] = 6; $fontface['small'] = 'Helvetica';

        $this->fontsize = $fontsize; // So routines can determine line offsets
        $this->currentsize = $size; // So routines can determine line offsets

        $font = PDF_load_font($this->pdf,$fontface[$size],"winansi","kerning=true");
        PDF_setfont($this->pdf,$font,$fontsize[$size]);

        $this->font = $font; // The font handle

    }

    /**
     * Convert inches to points
     */
    function points($inches) {

        $points = ($inches * 72);
        return $points;

    }

    /**
     * Convert <br> tags into newlines for PDF output
     */
    function conv_brs($text) {

        $text = str_replace('<br>',"\n",$text);
        $text = str_replace('<BR>',"\n",$text);

        return $text;

    }

    /**
     * Set a suspend state
     */
    function set_suspendstate() {

        if ($this->suspendstate == 0) $this->suspagenumber = 0;
        $this->suspendstate++; // Increment the suspend state

    }

    /**
     * Release a suspend state
     */
    function release_suspendstate($pagenumber) {

        $this->suspendstate = $this->suspendstate - 1; // Decrement the suspend state
        if ($this->suspendstate < 0) $this->suspendstate = 0; // I'd like to think this won't happen

        if ($this->suspendstate == 0) {
            if ($pagenumber < $this->suspagenumber) {
                PDF_suspend_page($this->pdf,'');
                if ($this->pagenumber > $this->suspagenumber) {
                    $this->suspagenumber = $this->pagenumber;
                }
                for ($pageidx = $pagenumber; $pageidx < $this->suspagenumber; $pageidx++) {
                    $optlist = "pagenumber=" . $pageidx;
                    PDF_resume_page($this->pdf,$optlist);
                    PDF_end_page_ext($this->pdf,"");
                }
                if ($this->pagenumber >= $this->suspagenumber) {
                    $pagenumber = $this->pagenumber;
                }
                else {
                    $pagenumber = $this->suspagenumber;
                }
                $optlist = "pagenumber=" . $pagenumber;
                PDF_resume_page($this->pdf,$optlist);
                $this->pagenumber = $pagenumber;
            }
        }

        if ($this->suspendstate == 0) $this->suspagenumber = 0;

    }

    /**
     * Display an error from PDFLib
     */
    function displayerror() {

        echo "Error: " . PDF_get_errmsg($this->pdf);

    }

}
