<?php 

require_once('phputil/shift_functions.html');


/**
 * This class exists primarily to encapsulate the shared behaviors of
 * the data entry classes.  Currently, we just add the method
 * is_linked_to_complete_shift() and override the to_mysql_db() method.
 */
class DataEntryDbObj extends DbObj {
    /**
     * Return true iff this datum is linked to a complete shift.
     *
     * @param  bool (optional) set to true to limit the check only to
     *         those shifts completed on the Web
     * @return bool true iff this is linked to a complete shift
     */
    function is_linked_to_complete_shift($web_only = false) {
        if ( $this->has_field('Shift_id') ) {
            return get_shift_complete_status( 
                $this->get_field('Shift_id'),
                $this->connection,
                $web_only 
            );
        } else {
            return false;
        }//else
    }//is_linked_to_complete_shift


    /**
     * If the object was created on the pda (indicated by having a
     * non-zero pda_id), and there is an id field for a run or
     * assessment that is 0, rewrite the field to -1 and proceed as
     * before
     */
    function to_mysql_db() {
        // abort if this object is tied to a previously completed shift
        $only_completed_from_web = true;
        if ( $this->is_linked_to_complete_shift($only_completed_from_web) ) {
            log_debug_message(
                $this->my_name.' #'.$this->pda_id.
                ': tried to add to a shift that was already complete'
            );
            return false;
        }//if
    
        // perform the id translation for pda entries
        if ( $this->pda_id ) {
            $is_zero_func_name = create_function(
                '$field',
                'return ($field == \'0\');'
            );

            // Run id
            $this->translate_field( 'Run_id', $is_zero_func_name, -1 );

            // Assessment id
            // @todo: can we someday standardize these id references?
            $this->translate_field( 'Asses_id',      $is_zero_func_name, -1 );
            $this->translate_field( 'Assess_id',     $is_zero_func_name, -1 );
            $this->translate_field( 'Assesment_id',  $is_zero_func_name, -1 );
            $this->translate_field( 'Assessment_id', $is_zero_func_name, -1 );
        }//if

        return parent::to_mysql_db();
    }//to_mysql_db
}//DataEntryDbObj

?>
