<?php
//-------------------------------------------------------------------------->
//--                                                                      -->
//--      Copyright (C) 1996-2007.  This is an unpublished work of        -->
//--                       Headwaters Software, Inc.                      -->
//--                          ALL RIGHTS RESERVED                         -->
//--      This program is a trade secret of Headwaters Software, Inc.     -->
//--      and it is not to be copied, distributed, reproduced, published, -->
//--      or adapted without prior authorization                          -->
//--      of Headwaters Software, Inc.                                    -->
//--                                                                      -->
//-------------------------------------------------------------------------->

require_once('phputil/classes/common_utilities.inc');
require_once('phputil/classes/Assert.inc');

/**
 * Represents a date used in a common form.
 * Assumes months are numbered 1-12, with 0 being ALL months.
 * Assumes that the year -1 is ALL years.
 */
final class common_date
{
    private $day;
    private $month;
    private $year;

    const ALL_MONTHS = 0;
    const ALL_YEARS = -1;

    /**
     * Factory method to create the object from script values.
     * @param string $prefix The value prefix, i.e. startdate_.
     * @return The common date.
     */
    public static function create_from_script($prefix)
    {
        // Sometimes different pieces of the date are not needed,
        // so we allow this.
        $not_set = 'NOT-SET';
        $month = common_utilities::is_scriptvalue($prefix . 'month') ?
            common_utilities::get_scriptvalue($prefix . 'month') : 
            $not_set;

        $day = common_utilities::is_scriptvalue($prefix . 'day') ?
            common_utilities::get_scriptvalue($prefix . 'day') :
            $not_set;

        $year = common_utilities::is_scriptvalue($prefix . 'year') ?
            common_utilities::get_scriptvalue($prefix . 'year') :
            $not_set;

        return new common_date($month, $day, $year);
    }

    /**
     * Factory method to create the object from a formatted string.
     * The year must be 4 digits, but the month an day can be 1 or 2.
     * @param string $date A string the typical form of (Y-m-d).
     * @param string $separator The single character field separator.
     * @return The common date.
     */
    public static function create_from_ymd($date, $separator='-')
    {
        // Validate.
        Assert::is_not_null($date);
        Assert::is_not_null($separator);
        Assert::is_true(strlen($separator) == 1);

        $pieces = split($separator, $date);

        Assert::is_true(count($pieces) == 3);
        Assert::is_true(strlen($pieces[0]) == 4);

        $n = strlen($pieces[1]);
        Assert::is_true(($n == 1) || ($n == 2));

        $n = strlen($pieces[2]);
        Assert::is_true(($n == 1) || ($n == 2));

        return new common_date($pieces[1], $pieces[2], $pieces[0]);
    }

    /**
     * Factory method to create from a Unix timestamp.
     * @param int The timestamp.
     * @return The common date.
     */
    public static function create_from_timestamp($timestamp) {
        Assert::is_int($timestamp);

        $pieces = getdate($timestamp);
        return new common_date($pieces['mon'], $pieces['mday'], $pieces['year']);
    }

    /**
     * Constructor.
     * @param string|int day The day of the month.
     * @param string|int month The month.
     * @param string|int year The year.
     */
    public function common_date($month, $day, $year)
    {
        $this->day = (int) $day;
        $this->month = (int) $month;
        $this->year = (int) $year;
    }

    /**
     * Retrieve the day.
     * @return int The day.
     */
    public function get_day()
    {
        return $this->day;
    }

    /**
     * Retrieve the month.
     * @return int The month.
     */
    public function get_month()
    {
        return $this->month;
    }

    /**
     * Retrieve the year.
     * @return int The year.
     */
    public function get_year()
    {
        return $this->year;
    }

    /**
     * Retrieve the Unix timestamp.
     * @return int The unix time.
     */
    public function get_time($hour=12, $minute=0, $second=0)
    {
        return mktime($hour, $minute, $second,
            $this->get_month(), $this->get_day(), $this->get_year());
    }

    /**
     * Retrieve the SQL date as the YEAR-MONTH-DAY.
     * @return string The unix time.
     */
    public function get_as_sql_date()
    {
        return $this->get_year() . '-' . $this->get_month() . '-' .
            $this->get_day();
    }

    /**
     * Retrieve the date as a linguistic phrase, i.e. Jan 3 2008.
     * @return string The phrase.
     */
    public function get_as_phrase()
    {
        $month_names = common_date::get_month_names();

        $phrase = $month_names[$this->get_month()] . ' ' .
            $this->get_day() . ' ' . $this->get_year();
        return $phrase;
    }

    /**
     * Retrieve the month/year as a linguistic phrase, i.e. Jan 2008 or
     * All, or All 2008.
     * @return string The phrase.
     */
    public function get_month_year_as_phrase()
    {
        $month_names = common_date::get_month_names();

        $month = $this->get_month();
        $year = $this->get_year();

        // Watch out for all months and all years.
        if ($month == self::ALL_MONTHS) 
        {
            if($year == self::ALL_YEARS)
            {
                $phrase = 'All';
            }
            else
            {
                $phrase = (string) $year;
            }
        }
        elseif ($year == self::ALL_YEARS)
        {
            $phrase = $month_names[$month] . ' All Years';
        }
        else 
        {
            $phrase = $month_names[$month] . ' ' . $year;
        }

        return $phrase;
    }

    /**
     * Retrieve the ALL value.
     * @return string The ALL value.
     */
    public static function all()
    {
        return 'All';
    }

    /**
     * Retrieve the month names.
     * @param short_names TRUE if 3 character month names should be used.
     * @return array The month names, 0 for all, 1-12 are the months.
     */
    public static function get_month_names($short_names=true)
    {
        $names = array(common_date::all());

        // Add in the real month names.
        $month_names = array(
            'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'
        );

        $n = count($month_names);
        for($i = 0; $i < $n; ++$i)
        {
            if ($short_names)
            {
                $names[$i+1] = substr($month_names[$i], 0, 3);
            }
            else
            {
                $names[$i+1] = $month_names[$i];
            }
        }

        return $names;
    }

    /**
     * Is this a leap year?
     * @return boolean TRUE if this is a leap year.
     */
    public function is_leap_year() {
        if ($this->year % 4) return false;
        if ($this->year % 100) return true;
        return !($this->year % 400);
    }

    /**
     * How many days are in the month?
     * @return int The number of days in the month.
     */
    public function get_days_in_month() {
        static $days = array(31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

        if ($this->month != 2) return $days[$this->month-1];
        if ($this->is_leap_year()) return 29;
        return 28;
    }

    /**
     * Fix the day so that it is valid for the month.
     */
    public function fix_day() {
        $this->day = max(0, min($this->day, $this->get_days_in_month()));
    }
}
?>
