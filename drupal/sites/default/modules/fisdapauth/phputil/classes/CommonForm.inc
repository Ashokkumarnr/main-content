<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/
 
/**
 * The standard common_form class
 *
 * @package CommonInclude
 * @author Warren Jacobson
 * @author Ian Young
 */

require_once("phputil/classes/CommonFormBase.inc");

class CommonForm extends CommonFormBase {
	/**
	 * An array containing the column layout of this form
	 */
	protected $column_layout;
	/**
	 * The entire HTML rendered form output
	 */
	protected $generated_html;
	/**
	 * The components that have been added to this form
	 * @var CommonPromptBox
	 */
	protected $components;

	public function __construct() {
		parent::__construct();
		$this->components = new CommonPromptBox(array(1,1,1));
	}

	/**
	 * @see CommonPromptBox::set_column_layout()
	 */
	public function set_column_layout($column_layout) {
		$this->components->set_column_layout($column_layout);
	}

	public function get_column_layout() {
		return $this->components->get_column_layout();
	}

	/**
	 * Add a display object (e.g., prompt, section) to the form
	 *
	 * @param common_form_component The component to add
	 * @param int The column index in which to add this
	 */
	public function add($component, $column) {
		if ($component instanceof common_form_prompt_component) {
			$this->legacy_mode = true;
			$component = new CommonLegacyPrompt($component);
		} else if ($component instanceof common_form_component) {
			// meh, do nothing
		} else if (!($component instanceof CommonFormComponent)) {
			//STUB error out
		}

		return $this->components->add($component, $column);
	}

	protected function display() {

		$output = $this->get_form_header_display();
		$output .= $this->components->display();
		$output .= $this->get_form_footer_display();

		return $output;

	}

}

