<?php
/*---------------------------------------------------------------------*
  |                                                                     |
  |      Copyright (C) 1996-2010.  This is an unpublished work of       |
  |                       Headwaters Software, Inc.                     |
  |                          ALL RIGHTS RESERVED                        |
  |      This program is a trade secret of Headwaters Software, Inc.    |
  |      and it is not to be copied, distributed, reproduced, published |
  |      or adapted without prior authorization                         |
  |      of Headwaters Software, Inc.                                   |
  |                                                                     |
* ---------------------------------------------------------------------*/

require_once('phputil/classes/BatchAccountsImport.php');
require_once('phputil/classes/FieldValidatorClasses.php');
require_once('phputil/classes/model/StudentDataModel.inc');
require_once('phputil/classes/model/InstructorDataModel.inc');
require_once('phputil/classes/model/UserAuthDataModel.inc');
require_once('phputil/classes/model/SerialNumbersModel.inc');
?>
