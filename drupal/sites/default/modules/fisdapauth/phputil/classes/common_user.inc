<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/exceptions/FisdapException.inc');
require_once('phputil/exceptions/FisdapRuntimeException.inc');
require_once('phputil/handy_utils.inc');
require_once('phputil/AccountOrdering/PreceptorTransaction.inc');

//todo common_student why do updates have program_id in them also?
//change?

/**
 * Defines a user of the FISDAP system
 */
final class common_user {

	/**
	 * The user name of this user (sometimes referred to as 'email')
	 */
	var $user_name;
	/**
	 * True if the user is valid, false otherwise
	 */
	var $is_valid;
	/**
	 * The program ID associated with this user
	 */
	var $program_id;
	/**
	 * The entire UserAuthData record
	 */
	var $UserAuthData;

	/**
	 * True if the user is a student, false otherwise
	 */
	var $is_student;
	/**
	 * The student ID of this user, assuming the user is a student
	 */
	var $student_id;

	/**
	 * True if the user is an instructor, false otherwise
	 */
	var $is_instructor;
	/**
	 * The instructor ID of this user, assuming the user is an instructor
	 */
	var $instructor_id;

	/**
	 * Factory method to create the user from a user ID.
	 * @param int $user_id The user ID.
	 * @return common_user The user or null if not found.
	 */
	public static function get_by_user_id($user_id) {
		Assert::is_int($user_id);

		$statement = "SELECT * FROM UserAuthData WHERE idx='$user_id'";
		$connection = FISDAPDatabaseConnection::get_instance();
		$results = $connection->query($statement);

		if (count($results) != 1) return null;

		try {
			$user = new common_user($results[0]['email']);

			if (!$user->is_valid()) {
				$user = null;
			}
		}
		catch(FisdapException $e) {
			$user = null;
		}

		return $user;

	}

	/**
	 * Factory method to create the user.
	 * If a string is passed, it is the user name (email).  An int is a user ID.
	 * @param common_student | common_user | common_instructor | int string | null $source The source data.
	 * @return common_user | null The user.
	 */
	public static function get_by_source($source) {
		global $REMOTE_USER; // Assigned via .htaccess

		Assert::is_true(
				Test::is_a($source, 'common_instructor') ||
				Test::is_a($source, 'common_student') ||
				Test::is_a($source, 'common_user') ||
				Test::is_int($source) ||
				Test::is_string($source) ||
				Test::is_null($source));

		if ($source instanceof common_user) {
			return $source;
		}

		try {
			if (Test::is_int($source)) {
				return common_user::get_by_user_id($source);
			}

			if (is_string($source)) {
				return common_user::get_by_username($source);
			}

			if ($source instanceof common_student) {
				return common_user::get_by_username($source->get_username());
			}

			if ($source instanceof common_instructor) {
				return common_user::get_by_username($source->get_username());
			}

			// No source.
			if (is_null($REMOTE_USER)) return null;
			return common_user::get_by_username($REMOTE_USER);
		}
		catch(FisdapException $e) {
			return null;
		}
	}

	/**
	 * Factory method to retrieve a user.
	 * @param string $username The user name.
	 * @param program_id The program ID.
	 * @return common_instructor|null The user if found or null if not.
	 */
	public static function get_by_username($username) {
		Assert::is_string($username);

		try {
			$user = new common_user($username);

			if (!$user->is_valid()) {
				$user = null;
			}
		}
		catch(FisdapException $e) {
			$user = null;
		}

		return $user;
	}

	/**
	 * Is password confirmation required?
	 *
	 * This function is built for ticket #233, and should not be used for 
	 * anything else.
	 *
	 * @return boolean true if the user has chosen to require password
	 * confirmation for important changes.
	 */
	public static function is_password_required() {
		$req = $_SESSION[SESSION_KEY_PREFIX . 'require_password'];
		return $req;
	}

    /**
     * Constructor.
     * @param string|null $user_name The user name.
     * @throws FisdapException
     */
	public function __construct($user_name) {

		// Two things needed from .htaccess: $REMOTE_USER 

		global $REMOTE_USER; // Assigned via .htaccess

		$connection = FISDAPDatabaseConnection::get_instance();
		$dbConnect = $connection->get_link_resource();

		// If no User Name is provided then use the User Name of the logged in user

		if ($user_name == null) $user_name = $REMOTE_USER;

		// If the User Name is null we need to abandon all hope

		if ($user_name == null) {
			// Some pages do not require a valid user.  Since underlying code 
			// expects a common_user object, I have to allow this instance, 
			// which is in an INVALID STATE, to be passed around :(
			$this->is_valid = false;
			return;
			throw new FisdapException('REMOTE_USER is not set');
		}

		// Find this user's UserAuthData record

		$query = 'select * from UserAuthData where email="' . $user_name . '"';
		$resultlist = mysql_query($query,$dbConnect);

		if (mysql_num_rows($resultlist) != 1) {
			$this->is_valid = false;
			return;
			throw new FisdapException("User[$user_name] not found");//todo
		}

		$UserAuthData_row = mysql_fetch_array($resultlist);
		$UserAuthData_instructor = $UserAuthData_row['instructor'];

		// Do we have an instructor or not?

		$program_id = null; $is_instructor = false; $instructor_id = null;
		$is_student = false; $student_id = null;

		if ($UserAuthData_instructor == 1) {

			$is_instructor = true; // Yes, this is an instructor
			$is_student = false; // No, this is not a student

			// Get this instructor's Instructor ID and Program ID

			$query = 'select Instructor_id, ProgramId, Permissions FROM InstructorData WHERE UserName="' . $user_name . '"';

			$resultlist = mysql_query($query,$dbConnect);

			if (mysql_num_rows($resultlist) != 1) {
				$this->is_valid = false;
				return;
				throw new FisdapException("Can not find user[$user_name] as an instructor");
			}

			$InstructorData_row = mysql_fetch_array($resultlist);

			$instructor_id = $InstructorData_row['Instructor_id'];
			$program_id = $InstructorData_row['ProgramId'];

		}
		else {

			// We're dealing with a student

			$is_instructor = false; // No, this is not an instructor
			$is_student = true; // Yes, this is a student

			// Get this student's Student ID and Program ID

			$query = 'select Student_id, Program_id FROM StudentData WHERE UserName="' . $user_name . '"';
			$resultlist = mysql_query($query,$dbConnect);

			if (mysql_num_rows($resultlist) != 1) {
				$this->is_valid = false;
				return;
				throw new FisdapException("Can not find user[$user_name] as an student");
			}

			$StudentData_row = mysql_fetch_array($resultlist);

			$student_id = $StudentData_row['Student_id'];
			$program_id = $StudentData_row['Program_id'];

		}

		// Put away all the info we collected for later use

		$this->user_name = $user_name; // Might be useful if someone is instantiating multiple instances
		$this->is_valid = true; // If not valid we wouldn't have made it this far
		$this->program_id = $program_id; // The Program ID of this User
		$this->UserAuthData = $UserAuthData_row; // The entire UserAuthData record

		$this->is_student = $is_student; // true if the user is a student, false otherwise
		$this->student_id = $student_id; // The student ID of this user, assuming the user is a student

		$this->is_instructor = $is_instructor; // True if the user is an instructor, false otherwise
		$this->instructor_id = $instructor_id; // The instructor ID of this user, assuming the user is an instructor

	}

	/**
	 * Returns this user's user name
	 */

	function get_username() {

		return $this->user_name;

	}

	/**
	 * Returns whether or not this user is valid
	 */

	function is_valid() {

		return $this->is_valid;

	}

	/**
	 * Returns this user's Program ID
	 */

	function get_program_id() {

		return $this->program_id;

	}

	/**
	 * Returns the ID of the UserAuthData record
	 */

	function get_UserAuthData_idx() {

		return $this->UserAuthData['idx'];

	}

	/**
	 * Returns true if user is a student, false otherwise
	 */

	function is_student() {

		return $this->is_student;

	}

	/**
	 * Returns this user's Student ID
	 */

	function get_student_id() {

		return $this->student_id;

	}

	/**
	 * Returns true if user is an instructor, false otherwise
	 */

	function is_instructor() {

		return $this->is_instructor;

	}


	/**
	 * Returns the user's Instructor ID
	 */

	function get_instructor_id() {

		return $this->instructor_id;

	}

	/**
	 * Returns the user's first name
	 */

	function get_firstname() {

		return $this->UserAuthData['firstname'];

	}

	/**
	 * Returns the user's last name
	 */

	function get_lastname() {

		return $this->UserAuthData['lastname'];

	}

	// Get the user's program's profession

	function get_profession() {

		if ($this->program_id == 555 or $this->program_id == 635) {
			$profession = 'surg-tech';
		}
		else {
			$profession = 'ems';
		}

		return $profession;
	}
}

/**
 * Defines a student.
 */
final class common_student {

	/**
	 * The Student ID for this student
	 */
	private $student_id;

	/**
	 * The entire StudentData table entry for this student
	 */
	private $studentdata;
	private $serial_number_data;

	/**
	 * Product as name -> code.
	 */
	private static $PRODUCTS = array(
			'tracking' => 1,
			'scheduler' => 2,
			'pda' => 4,
			'testing' => 8,
			'prep' => 16,
			'classroom' => 32
			);

	/**
	 * A bitwise string of product access
	 */
	private $product_access;

	/**
	 * A bitwise string of product limits
	 */
	private $product_limits;

	/**
	 * Instantiate an instance of the common_student class
	 * @throws FisdapException
	 */
	public function __construct($student_id) {
		// Fetch this student's data.
		$statement = "SELECT * FROM StudentData WHERE Student_id='$student_id'";
		$connection = FISDAPDatabaseConnection::get_instance();
		$results = $connection->query($statement);

		if (count($results) != 1) {
			throw new FisdapException("Student[$student_id] is not a student");
			return; 
		}

		$studentdata = $results[0];

		// Get the student's account configuration, including product access information
		$statement = "SELECT * FROM SerialNumbers WHERE Student_id='$student_id'";
		$results = $connection->query($statement);

		if (count($results) == 0) {
			throw new FisdapException("Student[$student_id] has no serial number information");
		}
		else if (count($results) > 1) {
			throw new FisdapException("Student[$student_id] has too much serial number information");
		}

		$serial_number_data = $results[0];

		$product_access = $serial_number_data['Configuration'];
		$product_limits = $serial_number_data['ConfigLimit'];
		$account_type = $serial_number_data['AccountType'];

		// Turn the PDA stuff into a boolean
		$has_synced_pda = ($serial_number_data['HasSyncedPDA'] == 1); 

		// Put everything away into this object's properties
		$this->student_id = $student_id;
		$this->studentdata = $studentdata; // The entire StudentData row for this student
		$this->product_access = $product_access; // A bitwise string of product access
		$this->product_limits = $product_limits; // A bitwise string of product limits
		$this->account_type = $account_type;
		$this->has_synced_pda = $has_synced_pda;
		$this->serial_number_data = $serial_number_data;
	}

	/**
	 * Retrieve the products.
	 * @return array Name(string) => code(int).
	 */
	public static function get_products() {
		return self::$PRODUCTS;
	}

	/**
	 * Retrieve the student ID.
	 * @return int The student ID.
	 */
	public function get_student_id() {
		return $this->student_id; 
	}

	/**
	 * Retrieve the student information.
	 * @return array An associative array of student info.
	 */
	public function get_studentdata() {
		return $this->studentdata;
	}

	/**
	 * Retrieve the serial number information.
	 * @return array An associative array of serial number info.
	 */
	public function get_serial_number_data() {
		return $this->serial_number_data;
	}

	/**
	 * Retrieve the user name.
	 * @return string The user name.
	 */
	public function get_username() {
		return $this->studentdata['UserName'];
	}

	/**
	 * Retrieve the last name of the student.
	 * @return string The last name.
	 */
	public function get_lastname() {
		return $this->studentdata['LastName'];
	}

	/**
	 * Retrieve the first name of the student.
	 * @return string The first name.
	 */
	public function get_firstname() {
		return $this->studentdata['FirstName'];
	}

	/**
	 * Retrieve the program ID for the student.
	 * @return int The program ID.
	 */
	public function get_program_id() {
		return $this->studentdata['Program_id'];
	}

	/**
	 * Retrieve the users program name.
	 * @return string The program name.
	 */
	public function get_program_name() {
		return $this->studentdata['Program'];
	}

	/**
	 * Retrieve the email address for the student.
	 * @return string The email address.
	 */
	public function get_emailaddress() {
		return $this->studentdata['EmailAddress'];
	}

	/**
	 * Retrieve the account type.
	 * @return string The account type.
	 */
	public function get_account_type() {
		return $this->account_type;
	}

	/**
	 * Determine if the student has a synced PDA.
	 * @return boolean TRUE if the student has a synced PDA.
	 */
	public function has_synced_pda() {
		return $this->has_synced_pda;
	}

	/**
	 * Determine if the student has access to a product.
	 * @param string $product_name The product name.
	 * @return boolean TRUE if the student can access the product.
	 * @throws FisdapInvalidArgumentException
	 */

	public function get_product_access($product_name) {
		Assert::is_true(self::is_product_valid($product_name));

		return !!($this->product_access & self::$PRODUCTS[$product_name]); 
	}

	/**
	 * Determine if the product is limited.
	 * @param string $product_name The product name.
	 * @return boolean TRUE if the product is limited.
	 */

	public function get_product_limit($product_name) {
		Assert::is_true(self::is_product_valid($product_name));

		return !!($this->product_limits & self::$PRODUCTS[$product_name]); 
	}

	/**
	 * Determine if a product name is valid.
	 * @param string $product_name The product name.
	 * @return boolean TRUE if the product name is valid.
	 */
	public static function is_product_valid($product_name) {
		return in_array($product_name, array_keys(self::$PRODUCTS));
	}

	/**
	 * Determine if a product access is valid.
	 * @param string $access The access, on/off.
	 * @return boolean TRUE if valid.
	 */
	public static function is_access_valid($access) {
		if ($access == 'on') return true;
		if ($access == 'off') return true;
		return false;
	}

	/**
	 * Sets access for a requested product to the given setting.
	 * @param string $product_name The product name.
	 * @param string $new_access on/off.
	 * @return boolean TRUE if the access was updated.
	 * @throws FisdapInvalidArgumentException
	 */
	public function set_product_access($product_name, $new_access) {
		Assert::is_true(self::is_product_valid($product_name));
		Assert::is_true(self::is_access_valid($new_access));

		// If the access is not changing there is nothing to do. 
		if ($this->product_access & self::$PRODUCTS[$product_name]) {
			$curr_access = 'on';
		}
		else {
			$curr_access = 'off';
		}

		if ($curr_access == $new_access) return true;

		// Update the setting.
		$curr_setting = $this->product_access;
		$product_code = self::$PRODUCTS[$product_name];

		if ($new_access == 'on') {
			$new_setting = $curr_setting + $product_code;
		}
		else { 
			$new_setting = $curr_setting - $product_code;
		}

		$statement = 'UPDATE SerialNumbers' .
			" SET Configuration = $new_setting" .
			" WHERE Student_id='$this->student_id'" .  
			" AND Program_id='" . $this->get_program_id() . "'" . 
			' LIMIT 1';
		$connection = FISDAPDatabaseConnection::get_instance();
		$dbConnect = $connection->get_link_resource();
		$n = $connection->update($statement);
		if (!$n) return false;

		if($new_setting&16)
		{
			$checksel = "SELECT * from moodle_StudyToolsEnroll WHERE moodlecourse=3 AND username='".$this->get_username()."'";
			$checkres = mysql_query($checksel,$dbConnect);
			$checkrows = mysql_num_rows($checkres);
			if($checkrows==0)
			{
				$prep_insert = "INSERT INTO moodle_StudyToolsEnroll SET moodlecourse=3,username='".$this->get_username()."'";
				$prep_result = mysql_query($prep_insert,$dbConnect);
				if($prep_result == false)
				{
					return false;
				}
			}
		}
		if(!($new_setting&16))
		{
			$checksel = "SELECT * from moodle_StudyToolsEnroll WHERE moodlecourse=3 AND username='".$this->get_username()."'";
			$checkres = mysql_query($checksel,$dbConnect);
			$checkrows = mysql_num_rows($checkres);
			if($checkrows==1)
			{
				$prep_del = "DELETE FROM moodle_StudyToolsEnroll WHERE moodlecourse=3 AND username='".$this->get_username()."' LIMIT 1";
				$prep_result = mysql_query($prep_del,$dbConnect);
				if($prep_result == false)
				{
					return false;
				}
			}
		}


		$this->product_access = $new_setting;
		$this->studentdata['Configuration'] = $new_setting;

		// Testing access updates require the TestingExpDate field to change 
		// also.
		if ($product_name == 'testing') {
			if ($new_access == 'on') {
				$new_exp_date = date('Y-m-d');
			}
			else {
				$new_exp_date = '0000-00-00';
			}

			$this->studentdata['TestingExpDate'] = $new_exp_date;

			$statement = 'UPDATE StudentData' .
				" SET TestingExpDate='$new_exp_date'" .
				" WHERE Student_id='$this->student_id'" .
				" AND Program_id='" . $this->get_program_id() . "'" .
				' LIMIT 1';

			$n = $connection->update($statement);
			if (!$n) return false;
		}

		return true;
	}

	/**
	 * Update the product limit.
	 * @param string $product_name The product name.
	 * @param string $new_limit on/off.
	 * @return boolean TRUE if the limit was updated.
	 * @throws FisdapInvalidArgumentException
	 */
	public function set_product_limit($product_name, $new_limit) {
		Assert::is_true(self::is_product_valid($product_name));
		Assert::is_true(self::is_access_valid($new_limit));

		// If the access is not changing there is nothing to do. 
		if ($this->product_limits & self::$PRODUCTS[$product_name]) {
			$curr_limit = 'on';
		}
		else {
			$curr_limit = 'off';
		}

		if ($curr_limit == $new_limit) return true;

		// Update the limit.
		$curr_setting = $this->product_limits;
		$product_code = self::$PRODUCTS[$product_name];

		if ($new_limit == 'on') {
			$new_setting = $curr_setting + $product_code;
		}
		else {
			$new_setting = $curr_setting - $product_code;
		}

		$statement = 'UPDATE SerialNumbers' .
			" SET ConfigLimit = $new_setting" .
			" WHERE Student_id='$this->student_id'" .
			" AND Program_id='" . $this->get_program_id() . "'" .
			' LIMIT 1';
		$connection = FISDAPDatabaseConnection::get_instance();

		$n = $connection->update($statement);
		if (!$n) return false;

		$this->product_limits = $new_setting; 
		$this->serial_number_data['ConfigLimit'] = $new_setting;

		return true;
	}
}

/**
 * Defines an instructor
 */

class common_instructor {

	/**
	 * The Instructor ID for this instructor
	 */
	private $instructor_id;
	/**
	 * A boolean indicating whether this instructor is valid or not
	 */
	private $is_valid;
	/**
	 * The entire InstructorData table entry for this student
	 */
	private $instructordata;
	/**
	 * A bitwise string of product access
	 */
	private $product_access;

	/**
	 * A bitwise string of product limits
	 */
	private $product_limits;
	
	/**
	 * Serial number associated with instructor if it exists
	 */
	private $serial_number;

	/**
	 * List of programs user has access to
	 */
	private $program_access_list;

	/**
	 * Product as name -> code.
	 */
	private static $PRODUCTS = array(
			'preceptortraining' => 64
			);

	/**
	 * Factory method to retrieve an instructor.
	 * @param string $username The user name.
	 * @param program_id The program ID.
	 * @return common_instructor|null The instructor if found or null if not.
	 */
	public static function get_by_username_and_program_id($username, $program_id) {
		if (is_null($username) || is_null($program_id)) return null;

		$connection = FISDAPDatabaseConnection::get_instance();
		$statement = 'SELECT * FROM InstructorData' .
			" WHERE UserName='$username' AND ProgramId='$program_id'";
		$rows = $connection->query($statement);

		if (count($rows) != 1) return null;

		$instructor = new common_instructor(null);
		$instructor->init($rows[0]);

		return $instructor;
	}

	/**
	 * Instantiate an instance of the common_instructor class
	 * Make sure to test the state of the object with is_valid().
	 * @param int|string|null $instructor_id The ID or null.
	 */

	function common_instructor($instructor_id) {
		$this->is_valid = false; 

		if (is_null($instructor_id)) return;

		$connection = FISDAPDatabaseConnection::get_instance();
		$statement = "SELECT InstructorData.*,SerialNumbers.Number,SerialNumbers.Configuration,SerialNumbers.ConfigLimit,ProgramAccessList FROM InstructorData LEFT JOIN SerialNumbers ON InstructorData.Instructor_id = SerialNumbers.Instructor_id LEFT JOIN CrossProgramAccess ON InstructorData.Instructor_id = CrossProgramAccess.Instructor_id WHERE InstructorData.Instructor_id = '$instructor_id'";
		$rows = $connection->query($statement);
		if (count($rows) != 1) return;

		$this->init($rows[0]);
	}

	/**
	 * Initialize the object from an associate array containing column names as 
	 * keys.
	 * @param array $data The DB row.
	 */
	private function init($data) {
		$this->instructor_id = $data['Instructor_id'];
		$this->is_valid = true; 
		$this->instructordata = $data;
		$this->product_access = $data['Configuration']; // A bitwise string of product access
		$this->product_limits = $data['ConfigLimit']; // A bitwise string of product limits
		$this->serial_number = $data['Number'];
		$this->program_access_list = $data['ProgramAccessList'];
	}

	/**
	 * Get whether or not this instructor is valid
	 */
	function is_valid() {

		return $this->is_valid; // A boolean indicating whether this instructor is valid or not

	}

	/**
	 * Get the Instructor ID for this instructor
	 */

	function get_instructor_id() {

		return $this->instructor_id; // The instructor ID for this instructor

	}

	/**
	 * Get the instructordata record for this instructor
	 */

	function get_instructordata() {

		return $this->instructordata;

	}

	/**
	 * Get the user name for this instructor
	 */

	function get_username() {

		return $this->instructordata['UserName'];

	}

	/**
	 * Get the last name for this instructor
	 */

	function get_lastname() {

		return $this->instructordata['LastName'];

	}

	/**
	 * Get the first name for this instructor
	 */

	function get_firstname() {

		return $this->instructordata['FirstName'];

	}

	/**
	 * Retrieve the full name of the instructor, i.e. "first last".
	 * @return string The full name.
	 */
	function get_fullname() {
		return $this->get_firstname() . ' ' . $this->get_lastname();
	}

	/**
	 * Get the Program ID for this instructor
	 */

	function get_program_id() {

		return $this->instructordata['ProgramId'];

	}

	/**
	 * Get the Email Address for this instructor
	 */

	function get_emailaddress() {

		return $this->instructordata['Email'];

	}

	/**
	 * Retrieve the account type.
	 * @return string The account type.
	 */

	public function get_account_type() {
		return 'instructor';
	}

	/**
	 * Retrieve the user's program access list.
	 * @return string Commas separated list of program IDs
	 */

	public function get_program_access_list() {
		return $this->program_access_list;
	}

	/**
	 * Does this user have access to other programs?
	 * @return boolean
	 */

	public function has_cross_program_access() {
		if ($this->get_program_access_list()) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Returns true or false for a requested instructor permission
	 */

	function get_permission($requested_permission) {

		// Bring in a collection of permission constants expressed as powers of 2.

		require "phputil/inst_perm_vals.html"; // Crazy collection of 'constants' using powers of 2

		// Build an array of permissions. Essentially, re-abstract what should've been abstracted from the get go

		$permissions['canOrderAccounts'] = 'ORDER_ACCOUNTS';
		$permissions['canAdminInstructors'] = 'ADMIN_INSTRUCTORS';
		$permissions['canAdminStudents'] = 'ADMIN_STUDENTS';
		$permissions['canViewShiftSkills'] = 'VIEW_SHIFT_SKILLS_DATA';
		$permissions['canViewAnonReports'] = 'VIEW_ANON_REPORTS';
		$permissions['canViewNamedReports'] = 'VIEW_NAMED_REPORTS';
		$permissions['canEditFieldSkills'] = 'EDIT_FIELD_SKILLS_DATA';
		$permissions['canViewSchedules'] = 'VIEW_SCHEDULES';
		$permissions['canAdminField'] = 'ADMIN_FIELD_SCHEDULES';
		$permissions['canAdminClinic'] = 'ADMIN_CLINIC_SCHEDULES';
		$permissions['canAdminProgram'] = 'ADMIN_PROGRAM';
		$permissions['canEnterEvals'] = 'ENTER_EVALS';
		$permissions['canEditEvals'] = 'EDIT_EVALS';
		$permissions['canEditClinicSkills'] = 'EDIT_CLINICAL_SKILLS_DATA';
		$permissions['canEditLabSkills'] = 'EDIT_LAB_SKILLS_DATA';
		$permissions['canAdminLab'] = 'ADMIN_LAB_SCHEDULES';
		$permissions['canAdminExams'] = 'ADMIN_EXAMS';

		// Determine the requested permission. If the expression using the bitwise & operator makes
		// your head want to explode, you have my deepest sympathy and empathy.

		if (!in_array($requested_permission,array_keys($permissions))) {
			$permission_granted = false; // The requested permission ain't on the list
		}
		else {
			$permission_granted = (($this->instructordata['Permissions'] & $$permissions[$requested_permission]) != 0);
		}

		return $permission_granted;

	}

	/**
	 * Get this instructor's program contact instructor
	 *
	 * Note: This function is typically used to provide an instructor with contact info
	 * 			in the event they were denied permission to a specific application or task.
	 */

	function get_contact() {

		$connection =& FISDAPDatabaseConnection::get_instance();
		$dbConnect = $connection->get_link_resource();

		$contactname = 'an instructor'; // Default contact name if none is found

		// Query the ProgramData table and get the Program Contact

		$query = 'select i.FirstName, i.LastName from InstructorData i, ProgramData p';
		$query .= ' where p.Program_id="' . $this->instructordata['ProgramId'] . '"';
		$query .= ' and p.Program_id=i.ProgramId and p.ProgramContact=i.Instructor_id';

		$resultlist = mysql_query($query,$dbConnect);

		if (mysql_num_rows($resultlist) != 1) {
			return $contactname; // Exit early!
		}

		$InstructorData = mysql_fetch_array($resultlist);

		$contactname = $InstructorData['FirstName'] . ' ' . $InstructorData['LastName'];

		return $contactname;

	}

	public function get_product_access($product_name) {
		Assert::is_true(self::is_product_valid($product_name));

		return !!($this->product_access & self::$PRODUCTS[$product_name]); 
	}

	/**
	 * Determine if the product is limited.
	 * @param string $product_name The product name.
	 * @return boolean TRUE if the product is limited.
	 */

	public function get_product_limit($product_name) {
		Assert::is_true(self::is_product_valid($product_name));

		return !!($this->product_limits & self::$PRODUCTS[$product_name]); 
	}

	/**
	 * Determine if a product name is valid.
	 * @param string $product_name The product name.
	 * @return boolean TRUE if the product name is valid.
	 */
	public static function is_product_valid($product_name) {
		return in_array($product_name, array_keys(self::$PRODUCTS));
	}

	/**
	 * Determine if a product access is valid.
	 * @param string $access The access, on/off.
	 * @return boolean TRUE if valid.
	 */
	public static function is_access_valid($access) {
		if ($access == 'on') return true;
		if ($access == 'off') return true;
		return false;
	}

	/**
	 * Sets access for a requested product to the given setting.
	 * @param string $product_name The product name.
	 * @param string $new_access on/off.
	 * @return boolean TRUE if the access was updated.
	 * @throws FisdapInvalidArgumentException
	 */
	public function set_product_access($product_name, $new_access) {
		Assert::is_true(self::is_product_valid($product_name));
		Assert::is_true(self::is_access_valid($new_access));

		$connection = FISDAPDatabaseConnection::get_instance();
		
		// If the access is not changing there is nothing to do. 
		if ($this->product_access & self::$PRODUCTS[$product_name]) {
			$curr_access = 'on';
		}
		else {
			$curr_access = 'off';
		}

		// If there's no change, we can take off.
		if ($curr_access == $new_access) return true;

		// Update the setting.
		$curr_setting = $this->product_access;
		$product_code = self::$PRODUCTS[$product_name];

		if ($new_access == 'on') {
			$new_setting = $curr_setting + $product_code;
		}
		else { 
			$new_setting = $curr_setting - $product_code;
		}

		if (!$this->serial_number) {
			$returnstring = '';
			GenFISDAPSerialNumber('instructor',$product_code,0,$returnstring);
			$this->serial_number = $returnstring;
			
			$distMethod = 'instructorupgrade';
			$purchaseOrder = '';
			
			insertSNInDatabase($this->serial_number, $distMethod, $this->get_program_id(), $purchaseOrder, 'instructor', 0, $product_code);
			$statement = "UPDATE SerialNumbers SET Instructor_id = ".$this->instructor_id." WHERE Number = '".$this->serial_number."' LIMIT 1";
			$connection->query($statement);

		} else {

			$statement = 'UPDATE SerialNumbers' .
			" SET Configuration = $new_setting" .
			" WHERE Instructor_id='$this->instructor_id'" .  
			" AND Program_id='" . $this->get_program_id() . "'" . 
			' LIMIT 1';

			// If this statement fails, it will throw an exception, so we don't care about return values.
			$connection->update($statement);
		}

		// If they're changing their Preceptor Training access, 
		// we have to update the Moodle table accordingly.
		if ($new_setting&64) {
			if (!PreceptorUtils::enrol_in_moodle($this->get_username())) {
				return false;
			}
		}

		if (!($new_setting&64)) {
			if (!PreceptorUtils::unenrol_in_moodle($this->get_username())) {
				return false;
			}
		}

		$this->product_access = $new_setting;
		$this->instructordata['Configuration'] = $new_setting;

		// Testing access updates require the TestingExpDate field to change 
		// also.
		if ($product_name == 'testing') {
			if ($new_access == 'on') {
				$new_exp_date = date('Y-m-d');
			}
			else {
				$new_exp_date = '0000-00-00';
			}

			$this->instructordata['TestingExpDate'] = $new_exp_date;

			$statement = 'UPDATE StudentData' .
				" SET TestingExpDate='$new_exp_date'" .
				" WHERE Student_id='$this->student_id'" .
				" AND Program_id='" . $this->get_program_id() . "'" .
				' LIMIT 1';

			$n = $connection->update($statement);
			if (!$n) return false;
		}

		return true;
	}

	/**
	 * Update the product limit.
	 * @param string $product_name The product name.
	 * @param string $new_limit on/off.
	 * @return boolean TRUE if the limit was updated.
	 * @throws FisdapInvalidArgumentException
	 */
	public function set_product_limit($product_name, $new_limit) {
		Assert::is_true(self::is_product_valid($product_name));
		Assert::is_true(self::is_access_valid($new_limit));

		// If the access is not changing there is nothing to do. 
		if ($this->product_limits & self::$PRODUCTS[$product_name]) {
			$curr_limit = 'on';
		}
		else {
			$curr_limit = 'off';
		}

		if ($curr_limit == $new_limit) return true;

		// Update the limit.
		$curr_setting = $this->product_limits;
		$product_code = self::$PRODUCTS[$product_name];

		if ($new_limit == 'on') {
			$new_setting = $curr_setting + $product_code;
		}
		else {
			$new_setting = $curr_setting - $product_code;
		}

		$statement = 'UPDATE SerialNumbers' .
			" SET ConfigLimit = $new_setting" .
			" WHERE Student_id='$this->student_id'" .
			" AND Program_id='" . $this->get_program_id() . "'" .
			' LIMIT 1';
		$connection = FISDAPDatabaseConnection::get_instance();

		$n = $connection->update($statement);
		if (!$n) return false;

		$this->product_limits = $new_setting; 
		$this->serial_number_data['ConfigLimit'] = $new_setting;

		return true;
	}
}

?>
