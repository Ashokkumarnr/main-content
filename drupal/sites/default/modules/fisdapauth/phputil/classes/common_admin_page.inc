<?php
require_once('phputil/inst.html');
require_once('phputil/session_functions.php');
require_once('phputil/classes/common_page.inc');
require_once('phputil/classes/common_user.inc');
require_once('phputil/fisdap_staff_lib.php');
require_once("phputil/hasGraduated.html");
require_once('phputil/schedule.html');

/**
 * A web page that is common to the admin functions.
 */
class common_admin_page extends common_page
{
    var $instructor;
    var $user;

    /**
     * Constructor.
     * @param string $title The page title or null.
     * @param string|null $redirect_url The URL to keep redirecting to.
     * @param connection The DB connection.
     */
	public function __construct($title, $redirect_url) 
    {
        $this->user = new common_user(null);

        if($this->user->is_instructor())
        {
            $this->instructor = new common_instructor($this->user->get_instructor_id());
        }

        parent::common_page($title, $redirect_url);
    }

    /**
     * Overridden.
     */
	public function display_header() 
    {
        // Style sheet code.
        $style = <<<EOI
img { border:0px; }
EOI;
        $this->add_stylesheetcode($style);

        // Java script.
        $js = <<<EOI
function sendlink(been_here, action)
{
    document.dataform.action = action;
    document.dataform.beenhere.value = been_here;
    document.dataform.submit();
}
EOI;
        $this->add_javascriptcode($js);

        parent::display_header();

        // Define a simple table layout for the menu and the real content.
?>
<table border="0" width="100%">
  <tr><td id="contentleft" valign="top" style="width: 95px; text-align: left">
<?php 
        $this->display_side_menu(); 
?>
  </td><td id="contentright" valign="top">
<?php
    }

    /**
     * Overridden.
     */
    public function display_footer()
    {
?>
</td></tr></table>
<?php
        parent::display_footer();
    }

    /**
     * Redirect to the InfoFrame.
     * @param url The URL to use.
     * @return The URL going through the scheduler.
     */
    private function get_info_area_url($url)
    {
        return FISDAP_WEB_ROOT . 'index.html?target_pagename=admin/index.html?infoarea=' . $url;
    }

    private function display_side_menu()
    {
        global $REMOTE_USER;
        global $Scheduler; 
        global $shareNum;

        $connection = FISDAPDatabaseConnection::get_instance();
        $dbConnect = $connection->get_link_resource();

        if($this->user->is_student()) {
            $student = new common_student($this->user->get_student_id());
            $has_pda = $student->get_product_access('pda');
            $has_graduated = hasGraduated($REMOTE_USER, $dbConnect);
        }
        else {
            $can_admin_exams = $this->instructor->get_permission('canAdminExams');
            $can_admin_instructors = $this->instructor->get_permission('canAdminInstructors');
            $can_admin_program = $this->instructor->get_permission('canAdminProgram');
            $can_admin_students = $this->instructor->get_permission('canAdminStudents');
            $can_edit_field_skills = $this->instructor->get_permission('canEditFieldSkills');
            $can_edit_clinic_skills = $this->instructor->get_permission('canEditClinicSkills');
            $can_order_accounts = $this->instructor->get_permission('canOrderAccounts');
        }

?>
<table cellpadding="0" cellspacing="0" border="0" align="center">
<?php 
        if (is_fisdap_staff_member()) { 
?>
  <tr><td>
    <a href="<?php echo $this->get_info_area_url('../internal/staff_directory.html');?>">
      <img alt="Directory" src="images/Directory.png" />
    </a>
  </td></tr>
<?php 
        } 
?>
<tr><td>
  <img src="images/studentsm.gif" alt="Student" />
</td></tr>
<tr><td>
  <img src="images/editsm.gif" alt="Edit Student Info" />
</td></tr>
<?php 
        if ($this->user->is_student() || $can_admin_students) {
?>
<tr><td>
  <a href="edit_student.php">
      <img src="images/editstudsm.gif" alt="Edit Student">
  </a>
</td></tr>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('changepass.html'); ?>">
      <img src="images/changepasssm.gif" alt="Change Password">
  </a>
</td></tr>
<?php
        }

 	    if ($this->user->is_instructor() && $can_admin_students) {
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('ClassSection.html'); ?>">
    <img src="images/class.gif" alt="Class Section" />
  </a>
</td></tr>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('ShowStatusSelect.html'); ?>">
    <img src="images/Status.gif" alt="Show Student Status" />
  </a>
</td></tr>
<tr><td>
  <a href="javascript:sendlink(0, 'find_student.php');">
    <img src="images/FindStudent.gif" alt="Find Student" />
  </a>
</td></tr>
<?php
        }
?>
<tr><td>
  <a href="../testing/testSchedule.html" target="_top">
    <img src="images/Testing.png" alt="FISDAP Testing Admin" />
  </a>
</td></tr>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('consent.html'); ?>">
    <img src="images/consentsm.gif" alt="Consent Form" />
  </a>
</td></tr>
<?php
        if ($this->user->is_instructor() || $has_pda) {
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('pdaStart.html'); ?>">
    <img src="images/PDASetup.gif" alt="Setup PDA" />
  </a>
</td></tr>
<?php
        }
    
        if ($this->user->is_student() && !$has_graduated) {
?>
<tr><td>
  <a href="studentAccountUpgrade.php" target="_top">
    <img src="images/Upgrade.png" alt="Acct Upgrade" />
  </a>
</td></tr>
<?php
	    }

        if (($this->user->is_student() && !$has_graduated) ||
            ($this->user->is_instructor() &&
                ($can_edit_field_skills || $can_edit_clinic_skills))) {
?>
<tr><td>
  <img src="images/removesm.gif" alt="Remove Data" />
</td></tr>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('rmshift.html'); ?>">
    <img src="images/rmshiftsm.gif" alt="Shift Delete" />
  </a>
</td></tr>
<?php
	    }
    
        if (($this->user->is_student() && !$has_graduated) ||
            ($this->user->is_instructor() && $can_edit_field_skills)) { 
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('rmrun.html'); ?>">
    <img src="images/rmrunsm.gif" alt="Run Delete" />
  </a>
</td></tr>
<?php
        }		
    
        if (($this->user->is_student() && !$has_graduated) ||
            ($this->user->is_instructor() && $can_edit_clinic_skills)) {
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('rmassesmnt.html'); ?>">
    <img src="images/rmassessmntsm.gif" alt="Assesment Delete" />
  </a>
</td></tr>
<?php
        }
        
        if ($this->user->is_instructor()) {
?>
<tr><td>
  <img src="images/instructsm.gif" alt="Instructor" />
</td></tr>
<?php  
            if ($can_admin_instructors) {
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('addinstruct.html'); ?>">
    <img src="images/addinstsm.gif" alt="Add Instructor" />
  </a>
</td></tr>
<?php
            }
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('editinstruct.html'); ?>">
	<img src="images/editinstsm.gif" alt="Edit Instructor" />
  </a>
</td></tr>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('instchgpass.html'); ?>">
	<img src="images/changepasssm.gif" alt="Change Password" />
  </a>
</td></tr>
<tr><td>
<a href="instSelfUpgrade.php">
    <img src="images/UpgradeMe.png" alt="Acct Upgrade" />
  </a>
</td></tr>
<tr><td>
  <img src="images/progadmin.gif" alt="Site Administration" />
</td></tr>
<?php	
            if( $can_admin_program) {
?>
<tr><td>
  <a href="editProgram.php" target="_top">
    <img src="images/edtprg.gif" alt="Edit Program" />
  </a>
</td></tr>
<?php
            }

            if ($can_order_accounts) {
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('acct_order_choices.html'); ?>">
    <img src="images/ordaccts.gif" alt="Order Student Accounts /">
  </a>
</td></tr>
<?php
            }

            if( $can_admin_program) {
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('sitelist.html?sitetype=clinical'); ?>">
    <img src="images/csitesm.gif" alt="Clinical Sites" />
  </a>
</td></tr>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('sitelist.html?sitetype=field'); ?>">
    <img src="images/fsitesm.gif" alt="Field Sites" />
  </a>
</td></tr>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('sitelist.html?sitetype=lab'); ?>">
    <img src="images/LabSites.gif" alt="Lab Sites" />
  </a>
</td></tr>
<?php
            }

            if($Scheduler == 1) {
    		    $shareNum = 0;
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('startSharing.html'); ?>">
    <img src="../scheduler/images/ShareSites.gif" alt="Share Sites" />
  </a>
</td></tr>
<?php
    	    }
        }
?>
</table>
<?php 
        if ($this->user->is_instructor()) {
?>
<form name="dataform" action="" method="post">
  <input type="hidden" name="beenhere" value="">
</form>
<?php
        }
    }

    /**
     * Display an error page.
     * @param msg The error message.
     */
    public function display_error_page($msg)
    {
        $this->do_display_error_page(
            "An error has occurred: $msg");
    }

    private function do_display_error_page($msg) {
        $this->display_header();
        $this->display_pagetitle();
?>
<p align="center" class="largetext" style="color: red"><?php echo $msg; ?></p>
<?php
        $this->display_footer();
    }

    /**
     * Verify that the user is an administrator.
     * If not, an error will be displayed and the page halted.
     * @param string|null $msg The error message or null for the default.
     */
    public function instructor_only($msg=null) {
        if ($this->user->is_instructor()) return;

        if (is_null($msg)) {
            $msg = <<<EOF
Only instructors are allowed access to this page.
EOF;
        }

        $this->do_display_error_page($msg);
        exit;
    }
}
?>
