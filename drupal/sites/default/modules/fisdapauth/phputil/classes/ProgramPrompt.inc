<?php
require_once('phputil/classes/common_form_base.inc');
require_once('phputil/classes/FISDAPDatabaseConnection.php');

/**
 * Defines a prompt that selects a program.
 * The value of the prompt is the program ID, or -1 for all (if allowed).
 */
class ProgramPrompt extends common_prompt {
    private $allow_all;

    const ALL_PROGRAMS_ID = -1;
    const NAME = 'program';
    const TEXT = 'Program';

    /**
     * Constructor.
     * Options are as follows:
     * - selected (mixed) : The selected program.
     * - show_all (boolean) : TRUE to show an 'All Programs' option.
     * - show_select_one (boolean) : TRUE to show a 'Select One' option.
     * @param string|null $name The prompt name.
     * @param string|null $text The prompt text.
     * @param array|null $options Name/value pairs.
     * @param boolean $required TRUE if the prompt is required.
     */
	public function __construct($name, $text, $options, $required) {
        if (is_null($name)) {
            $name = self::NAME;
        }

        if (is_null($text)) {
            $text = self::TEXT;
        }

        if (is_null($options)) {
            $options = array();
        }

		parent::common_prompt($name, $value, $text, null, 'select', $required);

        // The options.
        $selected = isset($options['selected']) ? $options['selected'] : null;
        $show_all = isset($options['show_all']) && $options['show_all'];
        $show_select_one = isset($options['show_select_one']) && $options['show_select_one'];

        // Add the programs.
        if ($show_select_one) {
            $this->set_promptset('-', '-- Select One --',
                is_null($selected), null, true);
        }

        if ($show_all) {
            $this->set_promptset(self::ALL_PROGRAMS_ID, 'All Programs',
                ($selected == self::ALL_PROGRAMS_ID), null, false);
        }

        $statement = 'SELECT Program_id, ProgramName FROM ProgramData ORDER BY ProgramName';
        $connection = FISDAPDatabaseConnection::get_instance();
        $rows = $connection->query($statement);

        foreach ($rows as $row) {
            $id = $row['Program_id'];
            $this->set_promptset($id, $row['ProgramName'],
                ($selected == $id), null, false);
        }
	}
}
?>
