<?php
require_once('phputil/classes/model/ModelCriteria.inc');

/**
 * An interface that defines ways to build DB statements.
 */
interface DbStatementBuilder
{
    /**
     * Retrieve a delete statement for array data.
     * @param string $table_name The table name.
     * @param array $columns_and_values The column name=>value (if not null, will be quoted).
     * @return string The update statement.
     */
    public function get_array_delete_statement($table_name, $columns_and_values);

    /**
     * Retrieve an insert statement for array data.
     * @param string $table_name The table name.
     * @param array $columns_and_values The column name=>value (if not null, will be quoted).
     * @return string The insert statement.
     */
    public function get_array_insert_statement($table_name, $columns_and_values);

    /**
     * Retrieve an update statement for array data.
     * @param string $table_name The table name.
     * @param array $columns_and_values The column name=>value (if not null, will be quoted).
     * @return string The update statement.
     */
    public function get_array_update_statement($table_name, $columns_and_values);

    /**
     * Retrieve a list of clauses that may be joined by the caller.
     * The values can be anything that get_clause() accepts.
     * @param array $columns_and_values The column name=>value (if not null, will be quoted).
     * @return array The clauses.
     */
    public function get_clauses($columns_and_values); 

    /**
     * Retrieve a value mapped to a statement value.
     * @param string $column The column name.
     * @param mixed $value The value.
     * @return string The clause, i.e. a=null, a in ("ab", "bc"), a=a
     * if an array of no values is passed.
     */
    public function get_clause($column, $value); 

    /**
     * Retrieve a list of conditional clauses that may be joined by the caller.
     * The values can be anything that get_condition_clause() accepts.
     * @param array $columns_and_values The column name=>value (if not null, will be quoted).
     * @return array The clauses as parenthized strings.
     */
    public function get_condition_clauses($columns_and_values); 

    /**
     * Retrieve a value mapped to a conditional statement value.
     * @param string $column The column name.
     * @param string | int | array |null $value The value.
     * @return string The parenthized clause, i.e. (a is null), (a in ("ab", "bc")), (a=a)
     * if an array of no values is passed.
     */
    public function get_condition_clause($column, $value); 

    /**
     * Retrieve a value mapped to the DB.
     * @param mixed $value The value.
     * @return string NULL or "xyz" (addslashed'ed).
     */
    public function get_clause_value($value); 

    /**
     * Retrieve the qualified column name.
     * @param ModelCriteria $criteria The criteria.
     * @param string $column_name The column name in the DB.
     * @return string The column name or "table-name-alias.column-name".
     */
    public function get_column_name(ModelCriteria $criteria, $column_name); 

    /**
     * Retrieve the table name and the alias for use in a statement.
     * @param ModelCriteria $criteria The criteria.
     * @param string $table_name The table name in the DB.
     * @return string The table name or "table-name alias".
     */
    public function get_table_name_clause(ModelCriteria $criteria, $table_name); 

    /**
     * Retrieve the DELETE statement corresponding to a criteria.
     * @param ModelCritiera $critera The search critiera.
     * @return string The statement.
     */
    public function get_delete_statement_by_criteria(ModelCriteria $criteria); 

    /**
     * Retrieve a select statement matching a criteria.
     * @param ModelCritiera $critera The search critiera.
     * @return string The select statement.
     */
    public function get_select_statement_by_criteria(ModelCriteria $criteria); 

    /**
     * Retrieve the UPDATE statement corresponding to a criteria.
     * @param ModelCritiera $criteria The search critiera.
     * @return string The statement.
     */
    public function get_update_statement_by_criteria(ModelCriteria $criteria, 
        ModelCriteria $update_criteria); 

    /**
     * Retrieve the order by clause.
     * @param ModelCriteria $criteria The criteria.
     * @return string The order by clause ( ORDER BY ...) or the empty string if none.
     */
    public function get_order_by_clause_by_criteria(ModelCriteria $criteria); 

    /**
     * Retrieve the condition, the where clause, for the criteria.
     * @param ModelCriteria $criteria The criteria.
     * @return string The where clause ( WHERE ...) or the empty string if none.
     */
    public function get_where_clause_by_criteria(ModelCriteria $criteria); 

    /**
     * Retrieve the condition(s) for the criteria.
     * @param ModelCriteria $criteria The criteria.
     * @return string The condition or the empty string for no condition.
     */
    public function get_condition_by_criteria(ModelCriteria $criteria); 

    /**
     * Add parenthesis around clauses.
     * @param array $clause The clauses.
     * @return array Each clause is surrounded by parenthesis.
     */
    public function add_clause_parens($clauses); 
}
?>
