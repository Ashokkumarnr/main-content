<?php
class ContactUs {
    /**
     * Retrieve the office phone number in a printable format.
     * @return string The office phone.
     */
    public static function get_office_phone() {
        return '651-690-9241';
    }
	/**
	 * Retrieve the email address to be used for reporting errors in the site.
	 * @return string the email address
	 */
	public static function get_bug_support_email() {
		return 'support@fisdap.net';
	}
}
?>
