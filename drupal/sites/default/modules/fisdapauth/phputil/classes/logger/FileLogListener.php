<?php 

/**
 * A logger that writes to a file.
 * This class is NOT optimized for production, but is useful for 
 * development.
 */
class FileLogListener
{
    var $path;

    function FileLogListener($path)
    {
        $this->path = $path;
    }

    function update($message)
    {
        $message .= "\n";
        error_log($message, 3, $this->path);
    }
}
?>
