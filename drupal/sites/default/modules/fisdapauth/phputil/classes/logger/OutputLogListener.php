<?php

/**
 * The simplest log listener possible.  Outputs any 
 * log messages directly to stdout.
 */
class OutputLogListener {
    /** 
     * Print the given message directly to stdout.
     */
    function update($message) {
        echo $message;
    }//update
}//OutputLogListener

?>
