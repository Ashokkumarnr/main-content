<?php


/**
 * This is a Singleton class meant to provide flexible logging to 
 * PHP scripts.  Log messages are broadcast to listeners.
 *
 * @todo  when we move to php5, change the log level constants to static 
 *        fields (so they can be accessed with Logger::WARN, e.g.)
 * @ignore
 */
class Logger {

    /** for errors resulting in the program crashing */
    var $FATAL = 0;

    /** for errors that can't be handled */
    var $ERROR = 1;

    /** for errors we can handle, but that might cause problems */
    var $WARN =  2;

    /** generic (yet useful) system information */
    var $INFO =  3;

    /** information only useful when debugging */
    var $DEBUG = 4;

    /** our current logging level */
    var $level;

    /** references to all listeners */
    var $listeners;

    /**
     * The default constructor.  This should never be used directly.  Use
     * Logger::get_instance instead.
     */
    function Logger() {
        $this->listeners = array();
        $this->leval = $this->WARN;
    }//Logger


    /**
     * Returns the singleton Logger instance, creating it if necessary
     */
    public static function get_instance() {
        static $instances;
        if ( !is_array($instances) ) {
            $instances = array();
            $instances[] = new Logger();
        }

        return $instances[0];
    }

 
    /** 
     * Broadcast the given message if it is at or below the current logging 
     * level.
     */
    function log($message,$level=-1) {
        // by default, use the current logging level
        if ( $level == -1 ) {
            $level = $this->level;
        }//if

        // skip messages at a higher level than this logger
        if ( $level > $this->level ) {
            return;
        }//if

        $num_listeners = count($this->listeners);
        for( $i=0 ; $i < $num_listeners ; $i++ ) {
            $listener =& $this->listeners[$i];
            $listener->update($message);
        }//for
    }//log


    /** 
     * Add the given listener
     */
    function add_listener( &$listener ) {
        $this->listeners[] =& $listener;
    }//add_listener


    /** 
     * Set the logging level
     */
    function set_level($level) {
        $this->level = $level;
    }//set_level


    /** 
     * Get the logging level
     */
    function get_level() {
        return $this->level;
    }//get_level


    /**
     * Remove all listeners
     */
    function clear_listeners() {
        $this->listeners = array();
    }//clear_listeners


    /**
     * Return a count of the listeners. 
     */
    function count_listeners() {
        return ($this->listeners) ? count($this->listeners) : 0;
    }//count_listeners
}//class Logger

?>
