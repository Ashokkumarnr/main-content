<?php
/*---------------------------------------------------------------------*
  |                                                                     |
  |      Copyright (C) 1996-2010.  This is an unpublished work of       |
  |                       Headwaters Software, Inc.                     |
  |                          ALL RIGHTS RESERVED                        |
  |      This program is a trade secret of Headwaters Software, Inc.    |
  |      and it is not to be copied, distributed, reproduced, published |
  |      or adapted without prior authorization                         |
  |      of Headwaters Software, Inc.                                   |
  |                                                                     |
* ---------------------------------------------------------------------*/

require_once('phputil/handy_utils.inc');
require_once('phputil/classes/UserClassesLoader.inc');
require_once('phputil/classes/model/SerialNumbersModel.inc');
require_once('phputil/classes/model/UserAuthDataModel.inc');
require_once('phputil/classes/model/StudentDataModel.inc');
require_once('phputil/classes/model/InstructorDataModel.inc');
require_once('lib/CsvIterator.class.php');


/** Implements importing student accounts from array / spreadsheet applications
*/
class BatchAccountsImport { // StudentAccountsBatchImport {
	protected $not_recognized_fields;
	protected $batch_data;
	
	// Current row's data in this class (across all physical tables)
	protected $row;
	
	// Current rows data models for each table
	protected $sn_row;
	protected $student_row;
	protected $instructor_row;
	protected $user_row;
	protected $user_auth_row;
	
	/* actions to perform 
		run		- do actual run  vs  test run: no changes made
	*/
	protected $user_requested_actions = array();			// ex: array ('run')

	protected $user_action_list = array('run', 'noautousername');

	// array of program_ids allowed during import, if empty - all allowed
	protected $program_ids_allowed = array();

	// CONTROL methods
	public function __construct($batch_data, $options=null) {

		//	verify batch data	
		if (!is_array($batch_data) || empty($batch_data)) {
			throw new FisdapException ("No records in file. Nothing to do. Exiting\n");
		}
		$this->batch_data = $batch_data;

		// todo: load field info setting from ini file for flexibility
		$this->load_field_properties();

		// defaults:
		$this->set_action('run', false);		// actual run?
		$this->set_action('noautousername', false);

		$this->validations = new FieldInputValidatorResults();
		
		// where to save files, results array names (good/bad imports splitting or bunching configuration)
		$this->set_file_names();
	}


	//		USER REQUESTED ACTIONS
	// set / unset actions
	public function set_action($action, $setit) {
		if ($setit) {
			if (!in_array($action, $this->user_requested_actions)) {
				$this->user_requested_actions[]=$action;
			}
		} else if (in_array($action, $this->user_requested_actions)) {
			HandyArrayUtils::remove_value_from_array($this->user_requested_actions, $action);
		}
	}
	
	public function is_user_action_set($action) {
		return (in_array($action, $this->user_requested_actions));
	}


	public function run($options = null) {
		if (!is_null($options)) {
			$this->process_options($options);
		}

		foreach ($this->batch_data as $row_num => $raw_row) {
			// unset objects:
			$this->sn_row = null;
			$this->student_row = null;
			$this->instructor_row = null;
			$this->user_row = null;
			$this->user_auth_row = null;

			$this->row = $this->convert_field_names_set_defaults($raw_row);
			
			$this->process_row_import($row_num, $this->row);
		}
		// ask user to display results:
			// 		fwrite(STDOUT, "Display results? (anything except enter will show results)\n");
			// 		$answer = fgets(STDIN); 
			// 		$a = trim($answer);
			// 		if (!empty($a)) {

		// Show all results:
		$this->validations->show_validation_results_txt();	//$row_num
	}


	/*	row_id is used to keep track of whole 'row' transaction across tables 
		and multiple validations processes
	*/
	protected function process_row_import($row_id, $row) {
		//$sn = new SerialNumbersM$program_ids_allowedodel($row['serial_number']);

		// row description used by validator object's reports: 
		$this->validations->set_row_description($row_id, $row_id." - ".$row['serial_number']);
		
		// load serial number:
		$this->sn_row = $this->load_unactivated_serial_number($row_id, $row['serial_number']);
		if ($this->sn_row) {
			$this->row['user_type'] = $this->sn_row->get_account_type();
			$this->row['program_id'] = $this->sn_row->get_program_id();
		}
		
		// check:  match program id with list of submitted program_ids
		if ($this->sn_row && !empty($this->program_ids_allowed)
			&& !in_array($this->sn_row->get_program_id(), $this->program_ids_allowed)) {
			$error = 'Program ID for serial '.$row['serial_number']." (".$this->sn_row->get_program_id().") doesn't match list of allowed program_ids";
			$this->validations->submit_validation_result($row_id, 'Serial Numbers', 'Program id', $error);
		}
		
		// check: sn must be for student currently
		if ($this->sn_row && $this->sn_row->get_account_type()=='instructor') {
			$this->validations->submit_validation_result($row_id, 'Serial Numbers', 'User Type',
				'SN is for an instrustor. Only batch import of student accounts is currently supported');
		}

		// get unique username to used
		// $this->row['username_requested'] = $this->row['username']; // changed 'username' to 'requested_username'
		$this->row['username'] = $this->check_and_generate_unique_username($row_id, $row['requested_username']);		

		
		// pass values to all the tables
		$this->populate_all_tables($row_id);
		
		// collect validations from the tables
		$this->validate_all_tables($row_id);

		// save record or do test run
		$is_real_run = ($this->is_user_action_set('run') && $this->validations->is_row_valid($row_id));
		$this->process_row_saving($row_id, $is_real_run);
	}




	// IMPORT FUNCTIONS
	// For all import functions:
	// Failures are recorded in $this->validations

	/**
	 *
	 */
	private function load_unactivated_serial_number($row_id, $serial) {
		$sn_error = false;

		if (empty($serial)) {
			$sn_error = "Serial Number was not provided";
		} else {
			$sn = SerialNumbersModel::get_by_serial_number($serial);
			if (!$sn) {
				$sn_error = "Serial Number $serial doesn't exist";
			}
		}

		if ($sn) { 
			// is serial activated check
			$status = $sn->is_activated(); // returns true/false/null

			if ($status) {
				$sn_error = "Serial Number $serial is already activated";
			}
		}

		if ($sn_error) {	// log it
			$this->validations->submit_validation_result($row_id, 'Serial Numbers', 'Serial Number', $sn_error);
		}
		return $sn;
	}
	
	

	/**	Checks for user requested username
	 *	If username is taken it tries to generate unique one unless
	 *		$this->is_user_action_set('noautousername')
	 *	Because of found incosistency in tables about usernames, the usernamecheck
	 *	  uniqueness test is done agains UserAuthData AND StudentData 
		  (skipped Instructor, haven't found inconsistencies)
	 */
	private function check_and_generate_unique_username($row_id, $passed_username) {
		// ensure only alphanumeric values are used
		$username = preg_replace("/[^a-zA-Z0-9]/", "", $passed_username); // \s - blank spaces
		// has the passed username had any non-alphanumeric chars?
		if ($username != $passed_username) {
			if ($this->is_user_action_set('noautousername')) {
				// no need to do more checks, models will fail non-alphanumeric username
				// (also although not 100% correct, I'm not checking for uniqueness of username with illegal characters.
				// non-allowed usernames should be  unique/not-present in database)
				return $passed_username;
			} else {
				$this->validations->submit_validation_result($row_id, 'UserName', 'Alphanumeric Check',
					"Requested UserName: '$passed_username' contained non-alphanumeric characters and/or spaces. It was changed to: '$username'.", true);
			}
		}

		// get valid username usernameprocess_row_saving
		// Make sure username >= 5 charactersx
		if (strlen($username) < 5) {
			$this->validations->submit_validation_result($row_id, 'UserName', 'Length Check',
				"Username '$username' is too short. Needs to be at least 5 characters long");
			return false;
		}
		$i='';
		
		// in auto creation mode we get 100 tries, in no auto creation mode exactly 1 try
		$maxi = ($this->is_user_action_set('noautousername')) ? 1 : 100;
		do {
			//$usernamecheck = $username.$i;
			$usernamecheck = HandyStringUtils::increment_string($username, (int)$i);
			$ua_row = UserAuthDataModel::get_by_username($usernamecheck);
			$sd_row = StudentDataModel::get_by_username($usernamecheck);
			$i++;
		} while (($ua_row || $sd_row) && $i<$maxi);

		// if $ua_row is set at this moment =  we could not create new username
		if ($ua_row || $sd_row) { 
			$msg = ($maxi>1)
				? 'Failed to create unique username automatically starting with '.$username
				: 'Username '.$username.' already exists';
			$this->validations->submit_validation_result($row_id, 'UserName', 'Not Unique', $msg);
		} else {
			// warn / notify only if username was automatically changed:
			if ($i>1) {
				$this->validations->submit_validation_result($row_id, 'UserName', 'UserName Check', 
					"Requested username '$username' is not available. Unique username has been generated: '$usernamecheck'", true);
			}
		}
		
		// return username or null if failed
		return ($ua_row) ? null : $usernamecheck;
	}



	/**
	 *	Populates data from $this->row to models using rules in $this->dbs_save_to
	 *	@param string $row_id
	 */
	protected function populate_all_tables($row_id) {
		// create arrays to pass to row_import of data models
		$values = array();
		foreach ($this->dbs_save_to as $tableto => $fields) {
			foreach ($fields as $key => $tofield) {
				$fromfl = (is_numeric($key)) ? $tofield : $key;
				$values[$tableto][$tofield] = $this->row[$fromfl];
			}
		}
		
		if ($this->sn_row) {
			$isStudent = ($this->sn_row->get_user_type() == 'student');
		} else {
			// set as student only for data validation purposes
			$this->validations->submit_validation_result($row_id, 'Serial Numbers', 'User Type',
				'Since no valid serial, don\'t know if Student or Instructor. Assuming it\'s Student to run validations');
			$isStudent = true;
		}

		// Create student/instructor object
		if ($isStudent) {
			$this->user_row = new StudentDataModel();
					// wrong function call test:	$values['StudentData']['wrong_field'] = 'wrong field test';
			$result = $this->user_row->set_values_by_row_if_valid($values['StudentData']);
					/* test: showing all values that were set now:
					foreach ($values['StudentData'] as $field => $val) {
						$fx = 'get_'.$field;
						$fl = $this->user_row->$fx();
						echo "Got value: '$fl' using function: '$fx'\n";
				}
					//*/
					// 			}
			
		} else if ($this->sn_row) { // instructor_id
			// INSTRUCTOR IMPORT NOT IMPLEMENTED
			$this->validations->submit_validation_result($row_id, 'Serial Numbers', 'User Type',
				'This serial is for instructor account. Batch Import currently does not import these records');
		}
		

		// Create UserAuthData object
		$this->user_auth_row = new UserAuthDataModel();
		$this->user_auth_row->set_values_by_row_if_valid($values['UserAuthData']);


		// Othe fields that need to be populated to database
		if ($this->sn_row) {
			$this->user_auth_row->set_instructor(($isStudent) ? 0 : 1);
		}
			/* test
					foreach ($values['UserAuthData'] as $field => $val) {
						$fx = 'get_'.$field;
						$fl = $this->user_auth_row->$fx();
						echo "Got value: '$fl' using function: '$fx'\n";
					}
			//*/	

		
			// DATA MODELS CREATIONS
			//$student_row; 		$instructor_row;
			//$user_row
			//$user_auth_row;
			// 
	}
		
	/*	tests: all database classes instatiated
	 *	returns true/false if values in table are valid.
	 *	Keep in mind that these validations only validate data in the table, not whole
	 *	row's transaction, which needs to be accessed through validator object
	 */	
	protected function validate_all_tables($row_id) {
		if ($this->sn_row) {
			$isStudent = ($this->sn_row->get_user_type() == 'student');
		}

		if ($this->user_row) {
			// merge validation results created through the model
			$this->validations->import_results($this->user_row->get_validation_results(), $row_id);
		} else {
			$this->validations->submit_validation_result($row_id, 'Import Error', 'Misc', 'Could not create User object');
		}
		
		if ($this->user_auth_row) {
			// $user_auth_valid = $this->user_auth_row->validate_fields();
			$this->validations->import_results($this->user_auth_row->get_validation_results(), $row_id);
		} else {
			$this->validations->submit_validation_result($row_id, 'Import Error', 'Misc', 'Could not create UserAuth object');
		}
	}



	/*	Purpose: 
			Saves data into tables
			Generates report arrays
		when using two distinct result arrays I'm keeping original row_ids for simplicity (and for reference)
		Hope it won't break anything in file save functions. (will find out later)
		
		This function expects:
			loaded models:
				$this->user_auth_row
				$this->user_row
				$this->sn_row
	*/
	protected function process_row_saving($row_id, $is_real_run) {
		//$either_array = ($this->validations->is_row_valid($row_id)) ? 'results_good' : 'results_bad';
		$is_row_valid = $this->validations->is_row_valid($row_id);
		
		if ((!$this->user_auth_row || !$this->user_row || !$this->sn_row) && $is_row_valid) {
			throw new FisdapException("Invalid State: This method needs all models already set if the validation passed");
		}
	
		if ($is_row_valid) {
			$either_array = &$this->results_good; 
		} else {
			$either_array = &$this->results_bad;
		}

		// saving: 
		if ($is_real_run && $is_row_valid) {
			$this->row['student_id'] = $this->user_row->save();
			$this->row['user_auth_idx'] = $this->user_auth_row->save();

			$this->sn_row->set_student_id($this->row['student_id']);
			//	echo "USER SAVE RETURNED: '".$this->row['student_id']."'\n";
			//	echo "USERAUTH SAVE RETURNED: '".$this->row['user_auth_idx']."'\n";
			$this->sn_row->save();
			
		}

		// report: save all DATA fields that came from original file 
		foreach ($this->tsv_file_fields as $tsv_field => $class_field) {
			$either_array[$row_id][$tsv_field] = $this->batch_data[$row_id][$tsv_field];
		}

		// report: save new user name
		$either_array[$row_id]['Requested Username'] = $this->row['requested_username'];  
		
		// report: save status: imported date or validation failed date
		if ($is_row_valid) {
			$either_array[$row_id]['Account Creation Status'] = ($is_real_run)
				? 'Account Created ' : 'Ready for Import';
		} else {
			$either_array[$row_id]['Account Creati.on Status'] = "WARNING: Validation failed";
		}
		$either_array[$row_id]['Validation Messages'] = $this->validations->get_validation_results_txt($row_id);
		
		if (!$this->validations->is_row_valid($row_id)) {

		}
	}



	/*	Uses already loaded $this->sn_row object
	
	 */
	protected function save_row_to_all_tables($row_id, $is_real_run) {
		// not valid row won't save, do report only
		if (!$this->validations->is_row_valid($row_id)) {
		//	echo "Row $row_id is not valid\n";
			return false;
		}
		//echo "Row $row_id is valid!!!\n";

		// Save user row
		//$this->user_row->save();
	}


	/*	results: file names to be used
				 array names - used for configurability purposes: to be able to put results in one result-set 
					or split into good and bad import attempts
	*/
	protected function set_file_names($array_good_results = 'imports_good', $array_bad_results = 'imports_bad') {
	
		// array names and default configuration
		$this->results_good = array();
		$this->results_bad = array();
	
		$results_good = &$this->results_good;
		$results_bad = &$this->results_bad;
	
		$this->filename_suffix['import_file'] = '';
		$this->filename_suffix['import_file_results'] = '';
		
		// result arrays:
		$this->result_array_names['results_good_imports'] = 'imports_good';
		$this->result_array_names['results_failed_imports'] = 'imports_failed';
	}






	// DATABASE and BIZ LOGIC methods
	protected function check_and_load_serial_number($serial_number) {
		$IsValidSerialNumber = SerialNumberUtils::isValidSerialNumber($SerialNumber); //as2:68
		if ($IsValidSerialNumber) {
			
		}
	}

	/*
		Converts names from outside file to data-model field names
		Creates list of not found fields in: $this->mismatched_fields
	*/
	public function convert_field_names_set_defaults($raw_row) {
		$ret = array();
		foreach ($raw_row as $fl => $val) {
			if (isset($this->tsv_file_fields[$fl])) {
				$ret[$this->tsv_file_fields[$fl]] = $val;
			} else {
				if (empty($this->mismatched_fields) || !in_array($fl, $this->mismatched_fields)) {
					$this->mismatched_fields[]=$fl;
				}
			}
		}
		
		if (!empty($this->mismatched_fields)) {
			$warning = true;
			echo "  WARNING: Unknown field names found in file:\n";
			echo "\t".implode(", ", $this->mismatched_fields)."\n";
		}
		
		// check if other fields user is missing in file structure:
		foreach ($this->tsv_file_fields as $file_field => $class_field) {
			if (!isset($raw_row[$file_field])) {
				$missing[] = $file_field;
			}
		}
		if ($missing) {
			$warning = true;
			echo "  WARNING: Your file is missing these columns used in this program:\n";
			echo "\t".implode(", ", $missing)."\n";
		}

			// user input is really sketchy.. so not using it.
			// 		$answer = ask_user ("Do you still want to continue [Y/N] ?");
			// 		if ($answer != strtolower("y")) {
		if ($missing) {
			echo "Please correct these issues.. Exiting.. \n";
			exit;
		}
		
		// defaults set here
		$ret['student_id'] = -15;
		$ret['instructor_id'] = -15;
		
		// quick password check and md5 on it
		/* Validation and encryption moved to model
		if (strlen($ret['password'])>=5) {
			$ret['password'] = md5($ret['password']);
		} else {
			$this->validations->submit_validation_result($row_id, 'Serial Numbers', 'Program id', $error);
		}
		*/
		
		return $ret;
	}




	/*	Field conversion and where what fields get saved
	*/
	protected function load_field_properties() {
		$this->user_type = 'Student';

		$this->import_result = array(
			'errors' => array(),
			'warnings' => array()
		);

		$required_fields = array('Serial No', 'First Name', 'Last Name', 'Requested UserName', 'Password',
			'Class Year', 'Class Month', 'Address', 'City', 'State', 'Zip');
		//$validate_fields = array('SerialNo', 'FirstName', 'LastName', 'UserName', 'Password', 'ClassYear', 'ClassMonth', 'Address', 'City', 'State', 'Zip',
		//	'HomePhone', 'CellPhone', 'WorkPhone', 'Gender', 'BirthDate', 'ContactName');
		/*	Matches names used in tvs file to 
		*/
		/* fields outside to field names */
		$this->tsv_file_fields = array(
			'Serial No' => 'serial_number',
			'First Name' => 'first_name',
			'Last Name' => 'last_name',
			'Requested UserName' => 'requested_username',
			'Password' => 'password',
			'Password Hint' => 'password_hint',
			'Class Year' => 'class_year',
			'Class Month' => 'class_month',
			'Address' => 'address',
			'City' => 'city',
			'State' => 'state',
			'Zip' => 'zip_code',
			'Home Phone' => 'home_phone',
			'Cell Phone' => 'work_phone',
			'Work Phone' => 'work_phone',
			'Gender' => 'gender',
			'BirthDate' => 'birth_date',
			'Emergency Contact Name' => 'contact_name',
			'Emergency Relationship' => 'contact_relation',
			'Emergency Phone' => 'contact_phone',
			'Email' => 'email'
		);

		$this->tsv_file_extra_fields = array(
			// fields that may be there - generated by this program
			'Account Creation Status' => 'account_creation_status',	// Created <date> || Unable to import <date>
			'Username Used' => 'username_used'
		);


		/* these will be copied from respective tables to current ar
		*/
		
		$this->from_db_fields = array();
		/* Fields come from user input, EXCEPT THESE:
			username		if allowed in options may be modified to ensure uniquenes in database
			student_id		generated from StudentData table after inserted
			instructor_id	generated from InstructorData table after inserted
			password		needs to be passed through md5
		
		where which fields get saved: 'database' => 'php_field_name' => 'save_to_db_field_name'
		// if ONE value: it describes both this class' from-field-names and (model's) to-field names
		// if names differ use from => to. ex instead of 'student_id'  you could have 'student-from_id' => 'student-to_id'
		
		 */
		$this->dbs_save_to = array(
			'SerialNumbers' => array(
				'student_id',
				'instructor_id',
				'serial_number'
			),
			'StudentData' => array(
				'first_name',
				'last_name',
				'username',
				'class_year',
				'class_month',
				'address',
				'city',
				'state',
				'zip_code',
				'home_phone',            
				'work_phone',
				'gender',
				'birth_date',
				'contact_name',
				'contact_relation',
				'contact_phone',
				'program_id',
				'email'
			),
			'UserAuthData' => array(
				'first_name',
				'last_name',
				'username',
				'password',
				'password_hint',
				'user_type',
				'email'
			),
			'InstructorData' => array(
				'first_name',
				'last_name',
				'username',
				'work_phone',
				'email'
			)
		);
	}

}

?>
