<?php 

require_once('phputil/inst.html');


/**
 * This class models a FISDAP Evaluation Session
 */
class FISDAP_EvalSession extends DataEntryDbObj {    
    var $linked_pda_data;

    /** 
     * Create a new EvalSession with the specified id and the specified
     * db link
     */
    function FISDAP_EvalSession( $id, $connection ) {
        $this->DbObj(
            'Eval_Session',                      //table name
            $id,                                 //id
            'EvalSession_id',                    //id field
            'Shift',                             //parent entity name
            array(                               //children
                'ItemSession',
                'CommentSession',
                'CriticalCriteriaSession'
            ),
            $connection                          //database connection
        );
        
        //set up the types of data eval links we support
        $this->linked_pda_data = array(
            'Shift_id'=>'ShiftData',
            'Run_id'=>'RunData',
            'Assess_id'=>'AssesmentData',
            'Med_id'=>'MedData',
            'IV_id'=>'IVData',
            'EKG_id'=>'EKGData',
            'Other_id'=>'OtherALSData',
            'BLS_id'=>'BLSSkillsData',
            'PtComp_id'=>'PtComplaintData',
            'Airway_id'=>'ALSAirwayData'
        );

        $field_map = array( 
            'Passed' => 'Passed',
            'Shift_id'=>'Shift_id',
            'Confirmed' => 'Confirmed',
            'Evaluator' => 'Evaluator',
            'Subject' => 'Subject',
            'EvalDef_id' => 'EvalDef_id',
            'EvalHookDef_id' => 'EvalHookDef_id',
            'Signature' => 'Signature'
            );
        $this->set_field_map( $field_map );
    }//constructor FISDAP_EvalSession


    /**
     * Create the data eval links from the Med_id, Run_id, Assess_id,
     * etc fields.  
     *
     * Parse the Evaluator/Subject string and set the appropriate fields
     *
     * Also, we need to map the Day, Month, and Year fields
     * from the pda onto the Date field in the MySQL database.  
     *
     * We also
     * need to create the StartTime and EndTime fields from the
     * StartHour/StartMinute and EndHour/EndMinute fields from the pda.
     *
     *
     */
    function postprocess_pda_submission() {
        global $Program_id;

        $my_name = $this->my_name();
        $field_prefix = $my_name.$this->pda_id;

        //parse the Evaluator string so it is entered correctly
        $EvaluatorValue = $this->pda_postdata[$field_prefix.'Evaluator'];
        $dashIndex = strpos($EvaluatorValue, '_');
        if ($dashIndex) {
            $Evaluator_id = substr($EvaluatorValue,$dashIndex+1);
            $EvaluatorType = substr($EvaluatorValue,0,$dashIndex);
            $this->set_field('Evaluator',$Evaluator_id);
            $this->set_field('EvaluatorType',$EvaluatorType);
        }

        //parse the Subject string so it is entered correctly
        $SubjectValue = $this->pda_postdata[$field_prefix.'Subject'];
        $dashIndex = strpos($SubjectValue, '_');
        if ($dashIndex) {
            $Subject_id = substr($SubjectValue,$dashIndex+1);
            $SubjectType = substr($SubjectValue,0,$dashIndex);
            $this->set_field('Subject',$Subject_id);
            $this->set_field('SubjectType',$SubjectType);
        }
       
        // set the Date field for inserting this session into mysql
        $day = $this->pda_postdata[$field_prefix.'Day'];
        $month = $this->pda_postdata[$field_prefix.'Month'];
        $year = $this->pda_postdata[$field_prefix.'Year'];
        $this->set_field('Date',$year.'-'.$month.'-'.$day);

        // set the StartTime field from the StartHour and StartMinute 
        $start_hour = $this->pda_postdata[$field_prefix.'StartHour'];
        $start_minute = $this->pda_postdata[$field_prefix.'StartMinute'];
        $this->set_field('StartTime',$start_hour.':'.$start_minute.':00');

        // set the EndTime field from the EndHour and EndMinute 
        $end_hour = $this->pda_postdata[$field_prefix.'EndHour'];
        $end_minute = $this->pda_postdata[$field_prefix.'EndMinute'];
        $this->set_field('EndTime',$end_hour.':'.$end_minute.':00');

        // set the Eval Session's program id
        $this->set_field('Program_id',$Program_id);
        
        // get the next pda id from a dummy link object
        $link_obj = new FISDAP_DataEvalLink(0,NULL);
        $link_obj->set_pda_postdata($this->pda_postdata);
        $min_id = $link_obj->get_next_pda_id();

        // @todo: can we stop this loop after we find something, or are
        //        we allowing multiple links (e.g., to a run and a
        //        complaint on that run) ?
        foreach( $this->linked_pda_data as $link_id_field => $link_table ) {
            if ( $datum_id = $this->pda_postdata[
                     $field_prefix.$link_id_field
                 ] 
               ) {
                $this->postprocess_pda_data_link(
                    $link_table,
                    $datum_id,
                    $min_id--
                );
            }//if
        }//foreach

        parent::postprocess_pda_submission();
    }//postprocess_pda_submission

    /**
     * Postprocess the given data link id field from the pda
     */
    function postprocess_pda_data_link( $table_name, $datum_id, $link_id ) {
        //create the link obj for writing to the postdata
        $data_link = new FISDAP_DataEvalLink(0,NULL);
        $data_link->pda_id = $link_id;
        $data_link->set_pda_postdata($this->pda_postdata);

        //set the link properties
        $link_attrs = array(
            'dataTableName' => $table_name,
            'Data_id' => $datum_id,
            'EvalSession_id' => $this->pda_id,
            'EvalHookDef_id' => $this->get_field('EvalHookDef_id'),
            );
        foreach( $link_attrs as $link_field => $link_val ) {
            $data_link->set_field($link_field,$link_val);
        }//foreach

        //finally, write the data eval link to the postdata
        $data_link->to_pda_postdata();
    }//postprocess_pda_data_link
}//class FISDAP_EvalSession

?>
