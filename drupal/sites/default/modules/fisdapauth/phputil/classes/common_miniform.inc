<?php
/**
 * A smaller, more flexible common_form.
 *
 * For use only in select unconventional applications, such as search boxes
 * or login prompts.
 *
 * @package CommonInclude
 * @author Ian Young
 */

require_once('phputil/classes/common_form_base.inc');

class common_miniform extends common_form_base {

	/**
	 * The entire HTML rendered form output
	 */
	protected $generated_html_form;
	/**
	 * Where the submit buttons float
	 * @var string 'left' or 'right'
	 */
	protected $submit_float;
	/**
	 * Where the form as a whole floats
	 * @var string 'left' or 'right'
	 */
	protected $form_float;
	/**
	 * Are prompt labels on the same line as the prompts?
	 * @var string 'above' or 'inline'
	 */
	protected $prompt_labels;
	/**
	 * Controls the layout of the form
	 * @var string 'line' or 'column'
	 */
	protected $form_layout;

	public function __construct() {
		parent::__construct();

		$this->generated_html_form = null; // Initialize the HTML form output to null

		$this->form_layout = 'line';
		$this->prompt_labels = 'inline';
		$this->form_float = 'left';
		$this->submit_float = 'left';
	}

	/**
	 * @todo WhenTF can we call this? only after display()? after process()? when?!?
	 */
	public function get_generated_html_form() {
		return $this->generated_html_form;
	}

	/**
	 * @param int the form width, in columns (as defined in {@link common_form})
	 * @todo maybe scrap this and do it all in CSS?
	 */
	public function set_form_width($width) {
		$this->form_width = $width;
	}

	/**
	 * Set various options related to how the form will display on the page.
	 *
	 * @param string $layout_option One of '{@link prompt_labels}',
	 * '{@link form_float}', '{@link submit_float}', or '{@link form_layout}'
	 * @param string $value the value of the option
	 * @see $form_float
	 * @see $submit_float
	 * @see $form_layout
	 * @see $prompt_labels
	 */
	public function set_layout($layout_option, $value) {
		$accepted = array('form_float' => array('right', 'left'),
			'submit_float' => array('right', 'left'),
			'form_layout' => array('line', 'column'),
			'prompt_labels' => array('above', 'inline'));
		if (array_key_exists($layout_option, $accepted)) {
			if (!in_array($value, $accepted[$layout_option])) {
				common_utilities::displayFunctionError('Layout setting ' . $value . ' not recognized for option ' . $layout_option);
			}
		} else {
			common_utilities::displayFunctionError('Layout option ' . $layout_option . ' not recognized');
		}

		// Set a label width on column forms by default
		if ($layout_option == 'form_layout' && $value == 'column' && $this->label_width == null) {
			$this->set_label_width('medium');
		}

		$this->$layout_option = $value;

	}

	/**
	 * Set the width of prompt labels in this form.
	 *
	 * This setting may be overridden by widths set on individual prompts.
	 * @see common_prompt::set_label_width()
	 */
	public function set_label_width($width) {
		$this->label_width = $width;
	}

	public function display($ajax_mode) {

		$output = null;

		// Build a string of all the classes to specify layout options
		$class_str = '';
		$class_str .= ' miniform_'.$this->form_layout;
		$class_str .= ' labels_'.$this->prompt_labels;
		$class_str .= ' form_float_'.$this->form_float;
		$class_str .= ' submit_float_'.$this->submit_float;

		if (!$ajax_mode) {
			$output .= '<div id="' . $this->name . '_form_div" class="miniform">';
		}
		$output .= '<div class="' . $class_str . '">';

		$output .= $this->get_form_header_display() . "\n";

		// Display all the components
		foreach ($this->displayobjects as $thiscomponentarr) {
			$thiscomponent = $thiscomponentarr['object'];

			// Set the label width, unless it has been overridden
			if ($this->label_width != null && $thiscomponent instanceof common_prompt) {
				if ($thiscomponent->get_label_width() == null) {
					$thiscomponent->set_label_width($this->label_width);
				}
			}

			// Call the display method for this prompt and retrieve the prompt HTML
			$prompthtml = '<div id="' . $thiscomponent->get_name() . '_div" class="common_prompt">';
			$prompthtml .= $thiscomponent->display();
			$prompthtml .= '</div>';

			$output .= $prompthtml . "\n";
		}

		$output .= "\n" . $hiddenqueue;

		$output .= $this->get_form_footer_display();

		$output .= '</div>' . "\n"; // the styling classes div
		if (!$ajax_mode) {
			$output .= '</div>'; // the enclosing div
		}


		// Display the form itself
		if ($ajax_mode == true) {
			echo $output; // Display the form to the browser
		}

		// We're done. Last task is to tuck the generated HTML form away in case the calling app wants it
		if ($ajax_mode == false) $this->generated_html_form = $output;

	}

}

