<?php
/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2007.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/common_form.inc');
require_once('phputil/classes/prompt_utils.inc');

/**
 * Form utilities.
 */
final class FormUtils {
    private $array_prompts = array();
    private $checkbox_prompts = array();
    private $debug = false;
    private $form;
    private $log_callback = 'devlog';
    private $prompt_utils;
    private $text_prompts = array();

    /**
     * Constructor.
     * @param common_form_base $form The form to use.
     */
    public function __construct($form) {
        Assert::is_a($form, 'common_form_base');
        $this->form = $form;
        $this->prompt_utils = new PromptUtils($form, null);
    }

    /**
     * Retrieve the underlying form.
     * @return common_form_base The underlying form.
     */
    public function get_form() {
        return $this->form;
    }

    /**
     * Indicate a prompt is an array prompt.
     * @param common_prompt $prompt The prompt.
     */
    public function add_array_prompt($prompt) {
        $this->array_prompts[] = $prompt->get_name();
    }

    /**
     * Indicate a prompt is a checkbox prompt.
     * @param common_prompt $prompt The prompt.
     */
    public function add_checkbox_prompt($prompt) {
        $this->checkbox_prompts[] = $prompt->get_name();
    }

    /**
     * Indicate a prompt is a text prompt.
     * @param common_prompt $prompt The prompt.
     */
    public function add_text_prompt($prompt) {
        $this->text_prompts[] = $prompt->get_name();
    }

    /**
     * Indicate whether the debug mode is on.
     * @param boolean $on TRUE if debug mode is on.
     */
    public function set_debug($on) {
        $this->debug = $on;
    }

    /**
     * Retrieve the value of a text field by name.
     * @param string $name The field name.
     * @param string $default The default value.
     * @return string The field value.
     */
    public function get_text_value_by_name($name, $default='') {
        // Handle forms where prompts are not shown.
        if (is_null($this->form->get_prompt_by_name($name))) {
            $this->log("Text field[$name] not found, assuming value[$default]");
            return $default;
        }

        return trim($this->prompt_utils->get_text_value_by_name($name, $default));
    }

    /**
     * Determine if a checkbox is checked.
     * @param string $name The field name.
     * @return boolean TRUE if checked.
     */
    public function is_checked_by_name($name) {
        // Handle forms where prompts are not shown.
        if (is_null($this->form->get_prompt_by_name($name))) {
            $this->log("Checkbox field[$name] not found, assuming value[not checked]");
            return false;
        }

        return $this->prompt_utils->is_checked_by_name($name);
    }

    /**
     * Retrieve the value of a array field by name.
     * @param string $name The field name.
     * @param array $default The default value.
     * @return array The field value.
     */
    public function get_array_value_by_name($name, $default=array()) {
        // Handle forms where prompts are not shown.
        if (is_null($this->form->get_prompt_by_name($name))) {
            $this->log("Array field[$name] not found, assuming default value[" .
                join(', ', array_values($default)) . ']');
            return $default;
        }

        return $this->prompt_utils->get_array_value_by_name($name, $default);
    }

    /**
     * Log the values from the form.
     */
    public function log_form_values() {
        // Log the checkbox values.
        foreach ($this->checkbox_prompts as $name) {
            if (is_null($this->form->get_prompt_by_name($name))) continue;

            $value = $this->prompt_utils->is_checked_by_name($name);
            $this->log("Checked[$name]: " .  
                PromptUtils::map_boolean($value, 'Yes', 'No'));
        }

        // Log the text field values.
        foreach ($this->text_prompts as $name) {
            if (is_null($this->form->get_prompt_by_name($name))) continue;

            $value = $this->prompt_utils->get_text_value_by_name($name);
            $this->log("Text-value[$name]: " . $value);
        }

        // Log the array values.
        foreach ($this->array_prompts as $name) {
            if (is_null($this->form->get_prompt_by_name($name))) continue;

            $values = $this->prompt_utils->get_array_value_by_name($name);
            $this->log("Array-value[$name]: " . join(', ', $values));
        }
    } 

    /**
     * Log something.
     * @param string $msg The message to log.
     */
    public function log($msg) {
        if (!$this->debug) return;
        if (!is_callable($this->log_callback)) return;

        call_user_func($this->log_callback, $msg);
    } 

    /**
     * Set the function for logging.
     * @param callback $callback The logging callback.
     * This function takes a single argument, the string to log.
     */
    public function set_log_callback($callback) {
        $this->log_callback = $callback;
    }

    /**
     * Generically validate the form fields.
     * Currently only SQL injection is tested.
     * @param common_form_base $form The form.
     * @return array (is valid, invalid error message)
     */
    public function validate_form_fields() {
        // Ensure no SQL injection.  This is pretty rudamentary.
        $errors = array();
        foreach ($this->text_prompts as $name) {
            $prompt = $this->form->get_prompt_by_name($name);
            if (is_null($prompt)) continue;

            $value = $this->prompt_utils->get_text_value_by_name($name);

            if (strpos($value, '--') !== false) {
                $errors[] = 'The prompt (' . $prompt->text . 
                    ") can not contain '--'";
            }

            if (strpos($value, '\\') !== false) {
                $errors[] = 'The prompt (' . $prompt->text . 
                    ") can not contain '\\'";
            }
        }

        if (count($errors)) return array(false, $errors);
        return array(true, null);
    }
}
?>
