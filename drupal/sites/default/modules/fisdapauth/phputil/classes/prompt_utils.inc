<?php
/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2007.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/

require_once('phputil/classes/common_form.inc');

/**
 * Utilities common to form prompts.
 * If you provide a prompt hierarchy, it will be used to determine if a 
 * prompt is shown; otherwise it is assumed the prompt is shown.
 */
class PromptUtils {
    private $form;
    private $hierarchy;

    /**
     * Constructor.
     * @param common_form_base $form The form containing the prompts.
     * @param PromptHierarchy|null $hierarchy The prompt hierarchy, if any.
     */
    public function PromptUtils($form, $hierarchy) {
        $this->form = $form;
        $this->hierarchy = $hierarchy;
    }

    /**
     * Determine a prompts text value.
     * @param common_prompt $prompt The prompt to check.
     * @param mixed $default The value to return if NOT set.
     * @return mixed The value.
     */
    public function get_text_value($prompt, $default='') {
        if (!$this->is_shown($prompt)) return $default;

        list($isset, $value) = $prompt->get_session_promptvalue();
        if (!$isset) return $default;

        if (is_array($value)) {
            if (!count($value)) return '';
                 $value = $value[0];
        }

        if (is_null($value)) return '';
        return $value;
    }

    /**
     * Determine a prompts text value.
     * @param string $name The prompt name.
     * @param mixed $default The value to return if NOT set.
     * @return mixed The value.
     */
    public function get_text_value_by_name($name, $default='') {
        $prompt = $this->form->get_prompt_by_name($name);
        if (is_null($prompt)) {
            die("get_text_value_by_name($name): prompt not found.");
        }

        return $this->get_text_value($prompt, $default);
    }

    /**
     * Determine if a single checkbox prompt is checked.
     * @param common_prompt $prompt The prompt to check.
     * @return TRUE if checked.
     */
    public function is_checked($prompt) {
        if (!$this->is_shown($prompt)) return false;

        list($isset, $value) = $prompt->get_session_promptvalue();
        if (!$isset) return false;

        if (is_null($value)) return false;
        if (is_array($value)) {
            if (!count($value)) return false;
            $value = $value[0];
        }

        return (!is_null($value) && ($value != ''));
    }

    /**
     * Determine if a single checkbox prompt is checked.
     * @param string $name The prompt name.
     * @return TRUE if checked.
     */
    public function is_checked_by_name($name) {
        $prompt = $this->form->get_prompt_by_name($name);
        if (is_null($prompt)) {
            die("is_checked_by_name($name): prompt not found.");
        }

        return $this->is_checked($prompt);
    }

    private function is_shown($prompt) {
        if (is_null($this->hierarchy)) return true;
        return $this->hierarchy->is_shown($prompt);
    }

    /**
     * Determine a prompts array value.
     * @param common_prompt $prompt The prompt to check.
     * @param mixed $default The value to return if NOT set.
     * @return array The value.
     */
    public function get_array_value($prompt, $default=array()) {
        if (!$this->is_shown($prompt)) return $default;

        list($isset, $value) = $prompt->get_session_promptvalue();
        if (!$isset) return $default;

        if (!is_array($value)) {
            $value = array($value);
        }

        // Strip out any null values.
        if (count($value)) {
            $new_value = array();
            foreach ($value as $item) {
                if (!is_null($item)) {
                    $new_value[] = $item;
                }
            }

            $value = $new_value;
        }

        if (!count($value)) return $default;
        return $value;
    }

    /**
     * Determine a prompts array value.
     * @param string $name The prompt name.
     * @param mixed $default The value to return if NOT set.
     * @return array The value.
     */
    public function get_array_value_by_name($name, $default=array()) {
        $prompt = $this->form->get_prompt_by_name($name);
        if (is_null($prompt)) {
            die("get_array_value_by_name($name): prompt not found.");
        }

        return $this->get_array_value($prompt, $default);
    }

    /**
     * Map a boolean value.
     * @param boolean $value The boolean value.
     * @param mixed $true The true value.
     * @param mixed $false The false value.
     * @return mixed The TRUE of FALSE value.
     */
    public static function map_boolean($value, $true, $false) {
        if ($value) return $true;
        return $false;
    }
}
?>
