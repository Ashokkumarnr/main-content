<?php

/**
 * This class models a FISDAP IV
 */
class FISDAP_IV extends DataEntryDbObj {    
    /** 
     * Create a new IV with the specified id and the specified db link
     */
    function FISDAP_IV( $id, $connection ) {
        $this->DbObj('IVData',                                                  //table name
                     $id,                                                       //id
                     'IV_id',                                                   //id field
                     'Shift',                                                   //parent entity name
                     array('EvalSession'),                                      //children
                     $connection );                                             //database connection
        $field_map = array( 
            'Shift_id' => 'Shift_id',
            'Student_id' => 'Student_id',
            'Run_id' => 'Run_id',
            'PerformedBy' => 'PerformedBy',
            'Site' => 'IVLocation',
            'FluidType' => 'FluidType',
            'NumAttempts' => 'NumAttempts',
            'Success' => 'Success',
            'Gage' => 'IVGage',
            'BloodDraw' => 'BloodDraw',
            'Time' => 'IVTime',
            'IVIO' => 'IVorIO',
            'SubjectType_id' => 'SubjectType_id'
            );
        $this->set_field_map( $field_map );
    }//constructor FISDAP_IV
}//class FISDAP_IV

?>
