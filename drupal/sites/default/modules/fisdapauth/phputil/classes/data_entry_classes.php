<?php
/**
 * This file takes care of the dependencies for all DbObj classes, including 
 * Shifts, Runs, Complaints, IVs, etc.
 */

if ( !defined( 'FISDAP_ROOT' ) ) die('required file dbconnect.html not loaded');

/** provides table_desc, which returns a description of the given MySQL table */
require_once(FISDAP_ROOT.'phputil/table_desc.php'); 

/** to whom do we mail error/status reports? */
define( 'MAIL_TO', 'smartin@fisdap.net' );

// the base class, which contains all database functionality
require_once(FISDAP_ROOT.'phputil/classes/DbObj.php');

// the base class for the core data entry classes.  this currently 
// exists to map the null pda Run_id 0 to the mysql null Run_id -1.
require_once(FISDAP_ROOT.'phputil/classes/DataEntryDbObj.php');

// the 'container' class for all data entry, a DbObj
require_once(FISDAP_ROOT.'phputil/classes/Shift.php');

// the core data entry classes, which descend from DataEntryDbObj
require_once(FISDAP_ROOT.'phputil/classes/Run.php');
require_once(FISDAP_ROOT.'phputil/classes/Assessment.php');
require_once(FISDAP_ROOT.'phputil/classes/OtherALS.php');
require_once(FISDAP_ROOT.'phputil/classes/BLSSkill.php');
require_once(FISDAP_ROOT.'phputil/classes/EKG.php');
require_once(FISDAP_ROOT.'phputil/classes/IV.php');
require_once(FISDAP_ROOT.'phputil/classes/Airway.php');
require_once(FISDAP_ROOT.'phputil/classes/Med.php');
require_once(FISDAP_ROOT.'phputil/classes/Complaint.php');
require_once(FISDAP_ROOT.'phputil/classes/Narrative.php');
require_once(FISDAP_ROOT.'phputil/classes/Preceptor.php');

// the eval classes, DbObjs
require_once(FISDAP_ROOT.'phputil/classes/EvalSession.php');
require_once(FISDAP_ROOT.'phputil/classes/EvalItemSession.php');
require_once(FISDAP_ROOT.'phputil/classes/EvalCommentSession.php');
require_once(FISDAP_ROOT.'phputil/classes/EvalCriticalCriteriaSession.php');

// models links between evals and other data (IVs, EKGs, etc), a DbObj
require_once(FISDAP_ROOT.'phputil/classes/DataEvalLink.php');

?>
