<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/
require_once("phputil/classes/clickable_table.inc");
require_once("phputil/handy_utils.inc");

/**
 * A sortable table.
 *
 * Creates a common_table that can be sorted by clicking on a given column heading.
 *
 * @package CommonInclude
 * @author Ian Young
 */
abstract class sortable_table extends clickable_table implements AjaxComponent {

	/**
	 * What criteria to sort by. Treated like a stack of sorting rules,
	 * with the most recent sorting rule at the end.  If a sorting rule ties,
	 * we consult the next-most-recent rule to resolve the tie.
	 *
	 * A sorting rule is an array with two fields:
	 * - 'col' => the column index to sort
	 * - 'order' => 'asc' or 'desc', depending on which way to order the results.
	 *   Defaults to 'asc'.
	 *
	 * @var array
	 */
	private $sortby;

	/**
	 * Should we make this list sortable? If false, will act like a regular table.
	 * @var boolean
	 */
	private $all_cols_sortable;
	/**
	 * Tracks which columns we are allowed to sort by.
	 *
	 * If a column is not explicitly set and the table is sortable, we assume it 
	 * is sortable.
	 *
	 * @var array an array of booleans
	 */
	private $sortable = array();

	/**
	 * Do we have a stored history of which columns to sort by for this list?
	 * @var boolean
	 */
	private $sort_history_exists;

	public function __construct() {
		parent::__construct();
		// Try to get sorting history
		$this->sort_history_exists = true;
		$this->sortby = common_utilities::get_scriptvalue('sortable_table_sortby');
		// If we failed to get history, start with a blank slate
		if (!$this->sortby) {
			$this->sortby = array();
			$this->sort_history_exists = false;
		}
	}

	/**
	 * Make all columns sortable
	 */ 
	public function make_all_columns_sortable() {
		$this->all_cols_sortable = true;
	}

	/**
	 * Don't allow sorting by this column
	 *
	 * @param string $colid the index of the column to make unsortable.
	 */
	public function make_column_unsortable($colid) {
		$this->sortable[$colid] = false;
	}

	/**
	 * Are we allowed to sort by a given column?
	 * @param string $colid the index of the column
	 * @return boolean
	 */
	public function is_column_sortable($colid) {
		return ($this->all_cols_sortable && (!isset($this->sortable[$colid]) || $this->sortable[$colid]));
	}

	/**
	 * Is this table sortable?
	 * @return boolean
	 */
	public function is_table_sortable() {
		return $this->all_cols_sortable || in_array(true, $this->sortable);
	}

	/**
	 * Set a comparator to use when sorting the given column. This will be used
	 * instead of the default comparison, which effectively asks $a > $b.
	 * @param int $colid the index of the column this comparator should be used on
	 * @param callback $cb 
	 * a {@link http://us.php.net/manual/en/language.pseudo-types.php#language.types.callback PHP callback}.
	 * Should accept two params, and return a negative number if the first is
	 * 'less' than the second, 0 if they are equal, and a positive number if the
	 * first is 'greater' than the second.
	 * @return void
	 */
	public function set_sorting_callback($colid, $cb) {
		$this->comparators[$colid] = $cb;
	}

	/**
	 * Sort the items in this list.  Typically only needs to be called right
	 * before we display the list.
	 */
	private function sort_items() {
		uksort($this->rowinfo, array($this, 'compare_rowinfo'));
	}

	/**
	 * Helper function
	 *
	 * Takes two keys from $this->rowinfo, finds the corresponding data rows,
	 * and sends those to {@link compare()} for real results.  Also does some extra work
	 * if {@link compare()} returns 0, because for some odd reason uksort() is not
	 * {@link http://en.wikipedia.org/wiki/Sorting_algorithm#Stability stable}.
	 */
	private function compare_rowinfo($a, $b) {
		$aval = $this->tabledata[$a];
		$bval = $this->tabledata[$b];
		$result = $this->compare($aval, $bval);
		// If it's a tie, try to retain original placement
		if ($result == 0) {
			// Iterate through rowinfo to determine which key appears first
			foreach ($this->rowinfo as $key => $val) {
				if ($key == $a) {
					$result = -1;
					break;
				}
				if ($key == $b) {
					$result = 1;
					break;
				}
			}
		}
		return $result;
	}

	/**
	 * Compare two rows based on the sorting rules in $this->sortby.
	 * 
	 * Checks if a callback has been set for each column, if not uses basic
	 * > and == comparisons. Searches recursively until it runs out of rules or
	 * gets a non-tied result.
	 *
	 * @param array $a a row of table data
	 * @param array $b a row of table data
	 * @param array $arr an array of sorting rules.  Should only be used on
	 * recursive calls within this function.
	 */
	private function compare($a, $b, $arr=null) {

		// If $arr isn't set, initialize it to our sorting rules
		if ($arr === null) $arr = $this->sortby;

		// Make sure we haven't run out of rules
		if (count($arr) == 0) return 0;

		// This removes the item from the array, perfect for recursing
		$currsort = array_pop($arr);

		$col = $currsort['col'];
		$order = $currsort['order'];
		$acmp = $a[$col];
		$bcmp = $b[$col];

		// Conditionally check column concerning custom comparator callbacks
		// Hee hee.
		if (isset($this->comparators[$col])) {
			$comparison = call_user_func($this->comparators[$col], $acmp, $bcmp);
		} else {
			if ($acmp > $bcmp) $comparison = 1;
			else if ($acmp == $bcmp) $comparison = 0;
			else $comparison = -1;
		}

		// If we are sorting in descending order, reverse our result
		if ($order == 'desc') $comparison *= -1;

		// If tied, try the next sorting rule, otherwise return our result
		if ($comparison == 0) {
			return $this->compare($a, $b, $arr);
		} else {
			return $comparison;
		}
	}

	/**
	 * Set this list to be sorted by the given column.
	 *
	 * Does not actually perform the sort - for that use {@link sort_list()}.
	 *
	 * @param string $col a column id
	 * @param string $order either 'asc' or 'desc'
	 */
	public function sort_by($col) {
		// Check this against the last subject of sorting to see if we should 
		// sort descending or ascending
		$last_col = HandyArrayUtils::array_peek($this->sortby);
		if ($last_col && $col == $last_col['col'] && $last_col['order'] == 'asc') {
			$order = 'desc';
		} else {
			$order = 'asc';
		}
		// Push the entry onto our sorting history
		$newentry = array('col' => $col, 'order' => $order);
		$this->sortby = self::push_sorting_entry($this->sortby, $newentry);
		common_utilities::set_scriptvalue('sortable_table_sortby', $this->sortby);
	}

	/** 
	 * Set default sorting column
	 *
	 * Acts like {@link sort_by()}, but will not override stored sorting history
	 * @param string $col a column id
	 * @param string $order either 'asc' or 'desc'
	 */
	public function default_sort_by($col, $order='asc') {
		if (!$this->sort_history_exists) {
			$this->sort_by($col, $order);
		}
	}

	/**
	 * Pushes a sorting rule $entry onto $array, removing any other rules with the
	 * same column index.  Very similar to {@link array_push_unique()}.
	 */
	private function push_sorting_entry($array, $entry) {
		foreach ($array as $idx => $row) {
			if ($row['col'] == $entry['col']) {
				unset($array[$idx]);
			}
		}
		$array[] = $entry;
		return $array;
	}

	/**
	 * Respond to an AJAX sorting event.
	 *
	 * Currently just re-displays the whole table if needed.
	 */
	public function respondAJAX() {
		// See if the event concerns us
		if ($_POST['event_type'] == 'sort') {
			// Get the sorting parameters
			$sortby = $_POST['colidx'];
			// Sort by the requested column
			$this->sort_by($sortby);
			$this->sort_items();
			// Now spit out the new list
			$source = $this->print_table();
			echo_ajax('div', $this->get_inner_div_id(), $source);
		} else {
			return parent::respondAJAX();
		}
	}

	public function process() {

		// Make headings clickable for any columns that are sortable
		if ($this->all_cols_sortable) {
			$this->make_all_headings_clickable('interactivetable_event', null, array('event_type' => 'sort'));
		}
		foreach ($this->sortable as $colidx => $bool) {
			if ($bool === false) {
				$this->make_heading_unclickable($colidx);
			}
		}

		$result = parent::process();

		if ($this->is_table_sortable()) {

			// If we're responding to a search event, populate
			if ($_POST['event_type'] == 'sort') {
				$this->populate();
			}

			if ($this->is_populated()) {
				// Make sure the list is sorted before we display
				$this->sort_items();
			}
		}

		return $result;
	}

	// Override to provide a visual cue for the currently sorted column
	protected function print_column_heading($thiscolumn, $widthphrase) {

		if ($this->is_column_sortable($thiscolumn['name'])) {
			// See if this is the column we are currently sorting by
			$tmp = HandyArrayUtils::array_peek($this->sortby);
			if ($thiscolumn['name'] == $tmp['col']) {
				$thiscolumn['css'][] = "sort_".$tmp['order'];
			}
		}
		return parent::print_column_heading($thiscolumn, $widthphrase);
	}

}
