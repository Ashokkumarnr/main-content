<?php

/**
 *
 */
class TestItemReviewAssignmentForm extends Form {
    function TestItemReviewAssignmentForm($assignment_id) {
        $this->Form('item_review_assignment.php','POST');

        // the container for all form elements
        $container_element =& new HTMLTag('div',array());
        $this->append_child(&$container_element);

        // the hidden assignment id field
        $assignment_id_input =& new HiddenInput( 'id', ((isset($assignment_id)) ? $assignment_id : '0') );
        $container_element->append_child(&$assignment_id_input);


        // the "pick a user" field


        // the "pick an item" field


        $submit_button_is_singleton = true;
        $submit_button = new HTMLTag(
            'input',
            array(
                'type'=>'submit',
                'value'=>'Submit'
            ),
            $submit_button_is_singleton
        );
        $container_element->append_child(&$submit_button);
    }//TestItemReviewAssignmentForm
}//class TestItemReviewAssignmentForm

?>
