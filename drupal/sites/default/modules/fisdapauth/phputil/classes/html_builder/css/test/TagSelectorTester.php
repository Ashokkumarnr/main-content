<?php

require_once('phputil/classes/html_builder/HTMLTag.php');
require_once('phputil/classes/html_builder/css/TagSelector.php');

/**
 * This class contains unit tests for the TagSelector class
 */
class TagSelectorTester extends UnitTestCase {
    function setUp() {
        $this->div_1 =& new HTMLTag( 'div', array( 'id'=>'tag_3', 'dummy'=>'dummy'));
        $this->div_2 =& new HTMLTag( 'div', array() );

        $this->p_1 =& new HTMLTag( 'p', array('dummy'=>'dummy') );
        $this->p_2 =& new HTMLTag( 'p', array() );
    }//setUp

   
    function tearDown() {
    }//tearDown


    function test_matches() {
        $div_selector =& new TagSelector('div');
        $p_selector =& new TagSelector('p');
        $input_selector =& new TagSelector('input');
        $star_selector =& new TagSelector('*');
        
        // full / empty div tags
        $this->assertTrue($div_selector->matches($this->div_1),'div tag didn\'t match div selector');
        $this->assertTrue($div_selector->matches($this->div_2),'div tag didn\'t match div selector');

        // full / empty p tags
        $this->assertTrue($p_selector->matches($this->p_1),'p tag didn\'t match p selector');
        $this->assertTrue($p_selector->matches($this->p_2),'p tag didn\'t match p selector');

        // full / empty div and p tags, using 'input' selector
        $this->assertFalse($input_selector->matches($this->p_1),'input selector matched non-input tag');
        $this->assertFalse($input_selector->matches($this->p_2),'input selector matched non-input tag');
        $this->assertFalse($input_selector->matches($this->div_1),'input selector matched non-input tag');
        $this->assertFalse($input_selector->matches($this->div_2),'input selector matched non-input tag');

        // * selector should match all tags
        $this->assertTrue($star_selector->matches($this->p_1),'* selector didn\'t match p tag');
        $this->assertTrue($star_selector->matches($this->p_2),'* selector didn\'t match p tag');
        $this->assertTrue($star_selector->matches($this->div_1),'* selector didn\'t match div tag');
        $this->assertTrue($star_selector->matches($this->div_2),'* selector didn\'t match div tag');
    }//test_matches
}//class TagSelectorTester  

?>
