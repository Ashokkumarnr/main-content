<?php

class IDSelector {
    var $id;

    function IDSelector($id) {
        $this->id = $id;
    }//IDSelector

    
    /**
     * Return true iff the given tag matches this selector
     *
     * @param  HTMLTag $tag
     * @return bool
     */
    function matches( $tag ) {
        return ($tag->get_attribute('id') == $this->id); 
    }//matches
}//class IDSelector

?>
