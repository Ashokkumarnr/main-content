<?php

/**
 * This class models an html input element of type 'text,' which
 * allows entry of a single line of text.
 */
class TextInput extends ValueAttrInput {
    /** 
     *
     */
    function TextInput( $name, $value, $size=false ) {
        $is_singleton = true;

        // set the primary attributes
        $attributes = array(
            'name'=>$name,
            'type'=>'text',
            'value'=>$value
        );

        // add the size attribute, if any
        if ( $size ) {
            $attributes['size'] = $size;
        }//if

        // create the input tag with the above attributes
        $this->HTMLTag(
            'input',
            $attributes,
            $is_singleton
        );
    }//TextInput
}//class TextInput

?>
