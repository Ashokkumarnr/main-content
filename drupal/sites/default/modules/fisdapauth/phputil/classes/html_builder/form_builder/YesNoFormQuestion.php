<?php

/**
 * This class models a Yes/No question, primarily for convenience
 * over the more complex FormQuestion class.
 */
class YesNoFormQuestion extends FormQuestion {

    ////////////////////////////////////////////////////////////
    // Private instance variables
    ////////////////////////////////////////////////////////////

    var $YES_VALUE='1';
    var $NO_VALUE='0';


    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     * Create a Yes/No question with the given stem, using
     * inputs with the given name. 
     *
     * @param string $stem the text to use for the question
     * @param string $name the name to use for the inputs
     */
    function YesNoFormQuestion( $stem, $name ) {
        $responses = array(
            new FormField(
                new RadioButton(
                    $name,
                    $this->YES_VALUE
                ),
                'Yes'
            ),
            new FormField(
                new RadioButton(
                    $name,
                    $this->NO_VALUE
                ),
                'No'
            )
        );
        
        $this->FormQuestion(
            $stem,
            $responses
        );
    }//YesNoFormQuestion
}//class YesNoFormQuestion

?>
