<?php

require_once('phputil/classes/html_builder/HTMLTag.php');
require_once('phputil/classes/html_builder/HTMLText.php');

/**
 *
 */
class SelectOption extends HTMLTag {

    //////////////////////////////////////////////////////////// 
    // Constructors
    //////////////////////////////////////////////////////////// 

    /** 
     *
     */ 
    function SelectOption($value, $text, $selected=false) {
        // create the tag element
        $this->HTMLTag(
            'option',
            array(
                'value'=>$value
            )
        );

        // set the 'selected' attribute, if specified
        if ( $selected ) {
            $this->set_attribute('selected', 'selected');
        }//if

        // add the option text
        $this->append_child(new HTMLText($text));
    }//SelectOption
}//class SelectOption

?>
