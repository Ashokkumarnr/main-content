<?php

require_once('phputil/classes/model/TestCreationProject.php');
require_once('AssignmentTestItemReviewForm.php');
require_once('shift/evals/generate_item_review_table.php');

/**
 * This class models the simplified assignment review form only meant to
 * require one review for the validity status of the reviewed test item to be
 * determined.
 */
class SingletonAssignmentTestItemReviewForm 
extends AssignmentTestItemReviewForm {
    function SingletonAssignmentTestItemReviewForm($item_id) {
        $this->item_id = $item_id;
        $review_factory = new TestItemReviewFactory();
        $reviews = $review_factory->create_all_reviews_by_item($item_id);

        $this->connection_obj =& FISDAPDatabaseConnection::get_instance();

        $this->Form('item_review.php','POST');

        // the container for all form elements
        $container_element =& new HTMLTag('div',array());
        $this->append_child($container_element);

        if ( $reviews ) {
            $review_list_container = new HTMLTag(
                'div',
                array(
                    'id'=>'review_list_container'
                )
            );
            $contents_only = true;
            $actions = array('view'); 
            /*$review_list_container->append_child( 
                new HTMLText(
                    '<strong>Reviews:</strong>'.
                    '<table id="review_list">'.
                    generate_item_review_table(
                        $item_id,
                        $reviews,
                        $actions,
                        $contents_only
                    ).
                    '</table>'
                )
			);*/
            $container_element->append_child($review_list_container);
        }//if

        $container_element->append_child( 
            new HTMLText(
                '<script type="text/javascript" '.
                        'src="../../phputil/behaviour.js">'.
                '</script>'
            )
        );

        // the hidden display mode field
        $display_mode_input =& new HiddenInput( 'mode', 'assignment' );
        $container_element->append_child($display_mode_input);

        // the hidden assignment id field
        $assignment_id_input =& new HiddenInput( 'assignment_id', '0' );
        $container_element->append_child($assignment_id_input);

        // the hidden AssignmentReviewType field
        $assignment_type_input =& new HiddenInput( 
            'AssignmentReviewType',
            SINGLETON_ASSIGNMENT_REVIEW 
        );
        $container_element->append_child($assignment_type_input);

        // for individuals, use a hidden input for the reviewer and
        // number_in_group fields
        $reviewer_input =& new HiddenInput(
            'reviewer',
            $_SERVER['PHP_AUTH_USER'] 
        );
        $container_element->append_child($reviewer_input);
        $number_in_group_input =& new HiddenInput( 'number_in_group', 1 );
        $container_element->append_child($number_in_group_input);

        // the hidden item id field
        $item_id_input =& new HiddenInput( 'item_id', $item_id );
        $container_element->append_child($item_id_input);

        // the hidden review id field
        $review_id_input =& new HiddenInput( 
            'id',
            ((isset($review_id)) ?  $review_id : '0') 
        );
        $container_element->append_child($review_id_input);

        // let singleton reviewers edit the items they're reviewing
        $edit_item_link_text = '<div>If necessary, you may <strong>'.
                               '<a '.
                                   'href="addTestItem.html?'.
                                       'edititem_id='.$item_id.'" '.
                                   'target="_new">'.
                                   'edit'.
                               '</a>'.
                               '</strong> this item.</div>';
        $edit_item_link =& new HTMLText($edit_item_link_text);
        $container_element->append_child($edit_item_link);

        $question =& new YesNoFormQuestion(
            '1. Is the item STEM grammatically and structurally correct?',
            'stem_grammar_correct' 
        );
        $question->set_attribute('id','question_1');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '2. Do the DISTRACTORS match the stem grammatically and '
            . 'structurally?',
            'distractors_match_grammar' 
        );
        $question->set_attribute('id','question_2');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '3. Are the DISTRACTORS less correct, yet plausible?',
            'distractors_less_correct' 
        );
        $question->set_attribute('id','question_3');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '4. Are the DISTRACTORS similar in appearance and content, '
            . 'yet distinct?',
            'distractors_similar_yet_distinct' 
        );
        $question->set_attribute('id','question_4');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '5. Is the answer CORRECT in all circumstances?',
            'answer_always_correct' 
        );
        $question->set_attribute('id','question_5');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '6. Is the item free from UNNECESSARY complication?',
            'item_free_from_complication' 
        );
        $question->set_attribute('id','question_6');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '7. Is this item UNBIASED toward all groups and regions?',
            'item_unbiased' 
        );
        $question->set_attribute('id','question_7');
        $container_element->append_child($question);

        $rationale_textarea =& new Textarea('explain_rationale');
        $rationale_textarea->set_attribute('id','explain_rationale');
        $rationale_textarea->set_attribute('cols',60);
        $rationale_textarea->set_attribute('rows',10);
        $question =& new FormQuestion(
            'Please explain your rationale for marking "no" on any of the '
            . 'Yes/No questions above:',
            array(
                new FormField(
                    $rationale_textarea
                )
            )
        );
        $question->set_attribute('id','explain_rationale_question');
        $container_element->append_child($question);


        $proposed_edit_textarea =& new Textarea('proposed_edit');
        $proposed_edit_textarea->set_attribute('id','proposed_edit');
        $proposed_edit_textarea->set_attribute('cols',100);
        $proposed_edit_textarea->set_attribute('rows',10);
        //$allow_whitespace_modification = false;
        //$proposed_edit_textarea->append_child(
        //    new HTMLText(
        //        get_test_item_plaintext(
        //            $item_id,
        //            $this->connection_obj->get_link_resource()
        //        ),
        //        $allow_whitespace_modification
        //    )
        //);
        $question =& new FormQuestion(
            'If necessary, use the following box to edit the item so that '
            . 'it better meets the above criteria.',
            array(
                new FormField(
                    $proposed_edit_textarea
                )
            )
        );
        $question->set_attribute('id','proposed_edit_question');
        $container_element->append_child($question);

        $submit_button_is_singleton = true;
        $submit_button = new HTMLTag(
            'input',
            array(
                'id'=>'submit_button',
                'type'=>'submit',
                'value'=>'Submit'
            ),
            $submit_button_is_singleton
        );
        $container_element->append_child($submit_button);
    }//SingletonAssignmentTestItemReviewForm
}//class SingletonAssignmentTestItemReviewForm

?>
