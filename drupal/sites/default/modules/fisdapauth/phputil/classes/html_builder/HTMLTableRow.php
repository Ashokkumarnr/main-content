<?php

/**
 * This file defines the class HTMLTableRow.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @package default
 * @author Sam Martin <smartin@fisdap.net>
 * @copyright 1996-2007 Headwaters Software, Inc.
 *
 */


require_once('phputil/classes/html_builder/HTMLTag.php');
require_once('phputil/classes/html_builder/HTMLText.php');


/**
 * This class provides a convenient HTML wrapper around PHP arrays.  
 * 
 * Example usage:
 * <code>
 *     // start table tag, etc
 *
 *     while ( $row = mysql_fetch_array($some_mysql_result) )
 *     {
 *         $html_row =& new HTMLTableRow($row);
 *         $html_row->display();
 *     }//while
 *
 *     // end table tag, etc
 * </code>
 */
class HTMLTableRow extends HTMLTag
{
    /** an array representation of the table row data, as a flat array */
    var $row_array;


    /**
     * Create a new HTMLTableRow with the given child data.
     */
    function HTMLTableRow($row_array)
    {
        $this->HTMLTag('tr', array());

        $this->row_array = $row_array;
        $this->build_child_cells();
    }//HTMLTableRow


    /**
     * Populate this row's cells (i.e., "td" tags) from the data array.
     */
    function build_child_cells()
    {
        $num_rows = count($this->row_array);
        for ( $i=0; $i < $num_rows; $i++ )
        { 
            $new_datum =& new HTMLTag('td', array());
            $new_datum->append_child(new HTMLText($this->row_array[$i]));
            $this->append_child($new_datum);
        }//for
    }//build_child_cells
}//class HTMLTableRow

?>
