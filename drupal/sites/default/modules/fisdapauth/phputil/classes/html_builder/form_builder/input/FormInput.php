<?php


/**
 * This class models an input or collection of inputs on an 
 * HTML form.  Examples include radio button groups, select menus,
 * textareas, etc.  Note that the inputs in any given FormInput
 * object must be of the same tag name
 */
class FormInput extends HTMLTag {

    ////////////////////////////////////////////////////////////
    //  Private instance variables
    ////////////////////////////////////////////////////////////

    /** the value to be give the element by default, if any */
    var $default_value;

    /** a list of all inputs in this group */
    var $inputs;

    /** this is the div that contains the group of elements */
    var $collection;

    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     *
     */
    function FormInput($tag_name,$attributes=false) {
        $this->tag_name = $tag_name;
        $this->default_value = false;
        $this->inputs = array();

        $this->container =& new HTMLTag('div',array('class'=>'form_input_container'));

        if ( $attributes ) {
            $is_singleton = ($tag_name == 'input');
            foreach( $attributes as $input ) {
                $this->add_input_element($this->tag_name,$input,$is_singleton);
            }//foreach
        }//if
        $this->append_child($this->container);
    }//FormInput


    ////////////////////////////////////////////////////////////
    // Public Instance Methods
    ////////////////////////////////////////////////////////////

    /**
     * Get a list of the HTMLTags for all the input elements
     * (select menus, input tags, etc) inside this object
     *
     * @return array a list of the form inputs in this object
     */
    function get_inputs() {
        return $this->inputs;
    }//get_inputs


    /**
     * Return the container div for this group
     */
    function &get_container() {
        return $this->container;
    }//get_container


    /**
     * Add an input element to this group
     *
     * @param array  $attributes
     * @param bool   $is_singleton true iff this tag doesn't close 
     */
    function add_input_element( $attributes, $is_singleton ) {
        $input_span =& new HTMLTag('span',array('class'=>'form_input'));

        $input_text = $attributes['text'];
        unset($attributes['text']);

        $input_element =& new HTMLTag(
            $this->tag_name,
            $attributes,
            $is_singleton
        ); 
        $this->inputs[] =& $input_element;

        $input_span->append_child($input_element);

        if ( $input_text ) {
            $input_text_obj =& new HTMLText($input_text);
            $input_span->append_child($input_text_obj);
        }//if

        $container =& $this->get_container();
        $container->append_child($input_span);
    }//add_input_element


    /**
     * Set the default value for this input object (e.g., 
     * the value to select by default in a radio button group)
     *
     * @todo do we still need this?
     * @param string $value the value to assign by default
     */
    function set_default_value( $value ) {
        $this->default_value = $value;
    }//set_default_value
}//class FormInput

?>
