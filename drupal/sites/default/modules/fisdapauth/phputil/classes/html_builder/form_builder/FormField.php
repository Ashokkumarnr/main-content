<?php

/**
 * This class models labelled form input fields. Note that the 
 * contained FormInput object is stored by reference.
 */
class FormField extends HTMLTag {
    
    ////////////////////////////////////////////////////////////
    // Private instance variables 
    ////////////////////////////////////////////////////////////

    /** the FormInput element to display */
    var $input;

    /** the label (if any) to display next to the form input */
    var $label;


    ////////////////////////////////////////////////////////////
    // Constructors 
    ////////////////////////////////////////////////////////////

    /**
     * Create a new field with the given input element and optional
     * label.
     *
     * @param FormInput $input_obj the input element
     * @param mixed     $label     (optional) the label to display
     */
    function FormField( &$input_obj, $label=false ) {
        // set the necessary fields
        $this->set_input($input_obj);
        $this->set_label($label);

        // generate the html to display
        $container = new HTMLTag('span',array('class'=>'form_field'));
        $container->append_child($input_obj);  
        if ( $label ) {
            $label_element = new HTMLText($label);
            $container->append_child($label_element);
        }//if
        $this->append_child($container);
    }//FormField
    

    ////////////////////////////////////////////////////////////
    // Public instance methods
    ////////////////////////////////////////////////////////////

    /**
     *
     */
    function get_label() {
        return $this->label;
    }//get_label


    /**
     *
     */
    function set_label( $label ) {
        $this->label = $label;
    }//set_label


    /**
     *
     */
    function set_input( &$input_obj ) {
        $this->input =& $input_obj;
    }//set_input


    /**
     *
     */
    function &get_input() {
        return $this->input;
    }//get_input
}//class FormField
?>
