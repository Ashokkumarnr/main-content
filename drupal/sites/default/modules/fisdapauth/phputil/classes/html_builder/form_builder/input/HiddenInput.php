<?php

/**
 *
 */
class HiddenInput extends ValueAttrInput {
    function HiddenInput( $name, $value ) {
        $is_singleton = true;
        $this->HTMLTag(
            'input',
            array(
                'type'=>'hidden',
                'name'=>$name,
                'value'=>$value
            ),
            $is_singleton
        );
    }//HiddenInput
}//class HiddenInput

?>
