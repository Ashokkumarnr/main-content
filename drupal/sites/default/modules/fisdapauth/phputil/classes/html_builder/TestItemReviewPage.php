<?php

class TestItemReviewPage extends HTMLChunk {
    var $item_id; 
    var $review_id; 
    var $connection;
    var $review_form;
    var $header;

    function TestItemReviewPage($item_id,&$review_form) {
        $this->item_id = $item_id;
        $this->review_id = $review_id;
        $this->review_form =& $review_form;

        $connection_obj =& FISDAPDatabaseConnection::get_instance();
        $this->connection = $connection_obj->get_link_resource();

        // create the header container 
        $this->header =& new HTMLTag(
            'div',
            array(
                'id'=>'item_review_header_container'
            )
        );
    }//TestItemReviewPage


    function display() {
        // set the doctype and include the html copyright notice
        define('DOCTYPE','HTML_401_TRANSITIONAL');
        include('phputil/copyright.php');
        
        echo '
            <html>
            <head>
                <title>
                    Item Review
                </title>

                <link rel=\'stylesheet\' 
                      type=\'text/css\' 
                      href=\'review_item.css\'>
                <style type=\'text/css\'>
                    @import(\'item_review_list.css\');
                </style>

                <script type=\'text/javascript\'
                        src=\'../../phputil/html_gen_lib.js\'></script>

                <script type=\'text/javascript\' 
                        src=\'../../phputil/behaviour.js\'></script>

                <script type=\'text/javascript\' 
                        src=\'test_item_review_form_behaviors.js\'></script>
            </head>
            <body>
            ';

        // add the header div
        $this->header->display();

        // display the item (with its distractors and feedback for each)
        display_test_item($this->item_id,$this->connection,'0%','0%');
        
		
		if(get_class($this->review_form)=="SingletonAssignmentTestItemReviewForm")
		{
			// display comments
			echo "<div class=\"comments_container\">\n";
			printComments($this->item_id);     
			echo "</div>\n";
		}    
        $this->review_form->display();

        echo '</body></html>';
    }//display


    function &get_header() {
        return $this->header;
    }//get_header


    function &get_review_form() {
        return $this->review_form;
    }//get_review_form
}//class TestItemReviewPage
    
?>
