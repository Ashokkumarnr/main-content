<?php


/**
 * This class encapsulates snippets of text enclosed within HTML tags.
 */
class HTMLText {

    ////////////////////////////////////////////////////////////
    //  Private instance variables
    ////////////////////////////////////////////////////////////

    /** the text to render */
    var $text;

    /** wrap strings longer than this number of characters */
    var $wrap_len = 75;

    /** are we allowed to modify the text's whitespace? */
    var $allow_whitespace_changes;


    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /** 
     * Create an HTMLText object with the given string as the text
     *
     * @param string $text the text to render for this object
     * @param bool   $allow_whitespace_changes true iff we can modify
     *               the text's whitespace
     */
    function HTMLText($text,$allow_whitespace_changes=true) {
        $this->allow_whitespace_changes = $allow_whitespace_changes;
        $this->text = $text;
        $this->tag_name = '';
    }//HTMLText


    ////////////////////////////////////////////////////////////
    // Public Instance Methods
    ////////////////////////////////////////////////////////////

    /** 
     * Render the text to stdout
     */
    function display() {
        echo $this->to_string();
    }//display


    /** 
     * Return the enclosed string, with the necessary indentation,
     * and wrapped to 75 characters.
     *
     * @param int $depth the number of spaces to indent
     */
    function to_string($depth=0) {
        if ( $this->allow_whitespace_changes ) {
            // make whitespace changes to the text, so it looks prettier
            $prefix = '';
            foreach( range(0,$depth) as $i ) {
                $prefix .= ' ';
            }//foreach
            $wrapped_text = $prefix;
            $wrapped_text .= wordwrap($this->text,$this->wrap_len,"\n".$prefix);
            return $wrapped_text."\n";
        } else {
            // return the unmodified text
            return $this->text;
        }//else
    }//to_string


    /**
     *
     */
    function set_text($text)
    {
        $this->text = $text;
    }//set_text
}//class HTMLText

?>
