<?php

require_once('phputil/classes/html_builder/form_builder/input/FormInput.php');

/**
 */
class SelectMenu extends FormInput {

    //////////////////////////////////////////////////////////// 
    // Constructors
    //////////////////////////////////////////////////////////// 

    /** 
     * Create a select menu with the given name, and the given 
     * options, if any.
     *
     * @param string $name    the name of the select tag to generate
     * @param array  $options (optional) option tags to add as children
     */ 
    function SelectMenu( $name, $options=false ) {
        // create the select tag
        $this->HTMLTag(
            'select',
            array(
                'name'=>$name
            )
        );

        // add the given options
        $this->append_children($options);
    }//SelectMenu
    

    //////////////////////////////////////////////////////////// 
    // Public instance methods 
    //////////////////////////////////////////////////////////// 

    /**
     * If this element has an entry in the given input, look for a 
     * child option with the given value.  If such an option exists,
     * set its 'selected' attribute to 'selected.'
     *
     * @param array $inputs hash of name/value pairs (e.g., form postdata)
     */
    function populate( $inputs ) {
        if ( isset($inputs[$this->get_attribute('name')]) ) {
            $options = $this->get_children();
            $num_options = count($options);
            for( $i=0 ; $i < $num_options ; $i++ ) {
                $option =& $options[$i];
                if ( $option->get_attribute('value') == $inputs[$this->get_attribute('name')] ) {
                    $option->set_attribute('selected','selected');
                    break;
                }//if
            }//for
        }//if
    }//populate


    function &get_selected_option() {
        if ( $this->children ) {
            foreach ( $this->children as $child ) {
                if ( $child->get_attribute('selected') ) {
                    return $child;
                }//if
            }//foreach
        }//if

        return false;
    }//get_selected_option
}//class SelectMenu

?>
