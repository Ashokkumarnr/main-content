<?php

/**
 * This class encapsulates a question on an HTML form.
 * A question, in this sense, is a collection of html 
 * elements, some of which are ostensibly input elements.
 */
class FormQuestion extends HTMLTag {

    ////////////////////////////////////////////////////////////
    // Private instance variables
    ////////////////////////////////////////////////////////////

    /** this holds a reference to the div containing error messages */
    var $error_container;

    /** this holds references to all inputs within the question */
    var $inputs;

    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     * Create a new question with the given stem text and the given 
     * inputs (objects of the class FormInput)
     *
     * @todo  chop this into a buncha shorter instance methods
     * @param string $stem  the text to display with the question
     * @param array $fields the FormFields to display
     */
    function FormQuestion( $stem, $fields=false ) {
        // wrap the entire question in a div
        $this->HTMLTag('div',array('class' => 'form_question'));

        // use a spacer row to contain any floated elements
        $this->append_spacer_row();

        // generate the stem div
        $stem_element = new HTMLTag('div',array('class'=>'form_question_stem'));
        $stem_text = new HTMLText($stem);
        $stem_element->append_child($stem_text);
        $this->append_child($stem_element);

        // add the form fields
        $field_container = new HTMLTag('div',array('class'=>'field_container'));
        $num_fields = count($fields);
        for( $i=0 ; $i < $num_fields ; $i++ ) {
            $field =& $fields[$i];
            $this->inputs[] =& $field->get_input();
            $field_container->append_child($field);
        }//for
        $this->append_child($field_container);

        // another spacer, in case the inputs are floated
        $this->append_spacer_row();

        // this is where error messages will be displayed
        $error_container = new HTMLTag('div',array('class'=>'form_question_error_area'));
        $this->error_container =& $error_container;
        $this->append_child($error_container);

        // a final spacer, again to constrain floated elements
        $this->append_spacer_row();
    }//FormQuestion


    ////////////////////////////////////////////////////////////
    // Public Instance Methods
    ////////////////////////////////////////////////////////////

    /** 
     * Append the error's message to the notification div
     *
     * @param ValidationError $error the error to report
     */
    function add_error( $error ) {
        $message = $error->get_message();
        $this->has_errors = true;
        $error_message_container = new HTMLTag(
            'div',
            array('class'=>'form_question_error_message')
        );
        $error_message = new HTMLText($message);
        $error_message_container->append_child($error_message);
        $this->error_container->append_child($error_message_container);
    }//add_error_message


    /**
     * Returns true iff this question contains an input element
     * with the given name.
     *
     * @param string $name the input name to search for
     */
    function contains_named_input( $name ) {
        $num_inputs = count($this->inputs);
        for( $i=0 ; $i < $num_inputs ; $i++ ) {
            $input =& $this->inputs[$i];
            if ( $input->get_attribute('name') == $name ) {
                return true;
            }//if
        }//for
        return false;
    }//contains_named_input
}//class FormQuestion

?>
