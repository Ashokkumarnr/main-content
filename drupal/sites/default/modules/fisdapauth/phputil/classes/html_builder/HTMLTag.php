<?php

require_once('phputil/form_utils.php');

/**
 * This class encapsulates HTML tags, for easy programatic generation of 
 * HTML documents from PHP.
 */
class HTMLTag {

    ////////////////////////////////////////////////////////////
    //  Private instance variables
    ////////////////////////////////////////////////////////////

    /** does this tag lack a closing? (e.g., input, link, or br tags) */
    var $is_singleton;

    /** the tag name (e.g., "html" or "div") */
    var $tag_name;

    /** the tag's attributes, as a hash of key/value pairs  */
    var $attributes;

    /** 
     *  the child elements, also HTMLTag (or, at least temporarily, 
     *  HTMLText objects), of this tag.  
     */
    var $children;

    /** the number of spaces to indent */
    var $INDENT_LEVEL = 4;


    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     * Create a new HTMLTag for the given tag, with the given attributes.
     *
     * @param string $tag_name   the name of the tag to generate
     * @param array $attributes  the attributes of the tag, as a hash of 
     *        key/value pairs
     * @param bool $is_singleton (optional) true iff this tag shouldn't have 
     *        a closing tag
     */
    function HTMLTag( $tag_name, $attributes, $is_singleton=false ) {
        $this->tag_name = $tag_name;
        $this->attributes = $attributes;
        $this->is_singleton = $is_singleton;
        $this->children = array();
    }//HTMLTag


    ////////////////////////////////////////////////////////////
    // Public Instance Methods
    ////////////////////////////////////////////////////////////

    /**
     * Add the given element, by reference, to the end of the internal list 
     * of children
     * 
     * @param mixed $child a reference to a child html element
     */
    function append_child( &$child ) {
        $this->children[] =& $child;
    }//append_child


    /**
     * Append each of the given children to the end of the internal list
     *
     * @param array $children the children to add
     */
    function append_children( $children ) {
        if ( $children ) {
            $num_children = count($children);
            for( $i=0 ; $i < $num_children ; $i++ ) {
                $child =& $children[$i];
                $this->append_child($child);
            }//for
        }//if
    }//append_children


    /**
     * Get the child element at the given index, by reference
     * 
     * @param int $index the index of the child element to return
     * @return mixed the child element at the specified index
     */
    function &get_child_at($index) {
        return $this->children[$index];
    }//get_child_at


    /**
     * Render this tag, and all children, to stdout
     */
    function display() {
        echo $this->to_string();
    }//display


    /**
     * Convert this tag, and all children, to a string suitable for
     * output
     *
     * @param int $depth (optional) the number of spaces to indent the tag
     * @return string a string representation of the html tag, with all 
     *                child elements
     */
    function to_string($depth=0) {
        // the output string
        $html = '';

        // use this prefix to generate pretty, indented output
        $prefix = $this->get_prefix_string($depth);

        // render the tag, if any
        if ( $this->get_tag_name() ) {
            $html .= $prefix.$this->get_opening_tag()."\n";
        }//if

        // render all the child elements
        $html .= $this->get_child_html($depth);

        // render the closing tag, if any 
        if ( !$this->is_singleton && $this->get_tag_name() ) {
            $html .= $prefix.$this->get_closing_tag();
        }//if

        $html .= "\n";
        return $html;
    }//to_string


    /**
     *
     */
    function get_opening_tag() {
        $html = '<'.$this->get_tag_name().' ';
        $html .= get_tag_attributes($this->attributes);
        $html .= '>';
        return $html;
    }//get_opening_tag


    /**
     *
     */
    function get_child_html($depth) {
        $html = '';
        if ( $this->children ) {
            foreach( $this->children as $child ) {
                $html .= $child->to_string($depth+$this->INDENT_LEVEL);
            }//foreach
        }//if
        return $html;
    }//get_child_html


    /**
     *
     */
    function get_closing_tag() {
        return '</'.$this->get_tag_name().'>';
    }//get_closing_tag


    /**
     *
     */
    function get_prefix_string($depth) {
        $prefix = '';
        foreach( range(0,$depth) as $i ) { 
            $prefix .= ' ';
        }//foreach
        return $prefix;
    }//get_prefix_string


    /**
     * Get the given attribute of this tag (e.g., onClick)
     *
     * @param string $attribute an html attribute name
     * @return string the value for the given attribute, if any
     */
    function get_attribute($attribute) {
        if ( isset($this->attributes[$attribute]) ) {
            return $this->attributes[$attribute];
        } else {
            return false;
        }//else
    }//get_attribute


    /**
     * Set the given attribute to the given value
     * 
     * @param string $attribute the attribute to set
     * @param string $value     the value to assign the attribute
     */
    function set_attribute( $attribute, $value ) {
        $this->attributes[$attribute] = $value;
    }//set_attribute


    /** 
     * Convenience function to append a child div of class 
     * 'spacer_row' to this element.
     */
    function append_spacer_row() {
        $spacer_row = new HTMLTag('div',array('class'=>'spacer_row'));
        $this->append_child($spacer_row);
    }//append_spacer_row


    /**
     * Set the tag's name to the given value
     *
     * @param string $new_tag_name the new tag name
     */
    function set_tag_name($new_tag_name) {
        $this->tag_name = $new_tag_name;
    }//set_tag_name


    /**
     * Get this object's tag name
     *
     * @return string the tag name for this object
     */
    function get_tag_name() {
        return $this->tag_name;
    }//get_tag_name

    
    /**
     * Get an array of the children of this tag
     *
     * @return array the children of this tag
     */
    function get_children() {
        return $this->children;
    }//get_children


    /**
     * Returns true iff this tag has children
     *
     * @return bool true iff this tag has children
     */
    function has_children() {
        return ($this->children) ? true : false ;
    }//has_children
    

    /**
     * Fill in any inputs in this tag, or its children, whose names match
     * keys in the given array of postdata, with the matching values.
     *
     * @param array $inputs an array of key/value pairs to use when filling 
     *                      in the inputs
     */
    function populate( $inputs ) {
        $children = $this->get_children();
        if ( $children ) {
            $i = 0;
            $num_children = count($children);
            for( $i=0 ; $i < $num_children ; $i++ ) {
                $child =& $children[$i];
                if ( method_exists( $child, 'populate' ) ) {
                    $child->populate($inputs);
                }//if 
            }//for
        }//if
    }//populate


    /**
     * Return true iff either the current tag, or a child (or grandchild, etc) 
     * thereof has the given name attribute.
     *
     * @param string $name the name to search for
     */
    function contains_named_tag( $name ) {
        if ( $this->get_attribute('name') == $name ) {
            return true;
        }//if
    
        $children = $this->get_children();
        if ( $children ) {
            // recurse on the children
            $i = 0;
            $num_children = count($children);
            for( $i=0 ; $i < $num_children ; $i++ ) {
                $child =& $children[$i];
                if ( method_exists( $child, 'contains_named_tag' ) ) {
                    if ( $child->contains_named_tag($name) ) {
                        return true;
                    }//if
                }//if 
            }//for
        }//if

        return false;
    }//contains_named_tag


    /**
     * for now, we'll represent a selector as an array of arrays, where
     * the toplevel arrays represent multiple individual selectors and
     * the subarrays represent descendant relationships
     */
    function parse_selector($selector) {
        // the regular expression to parse single selectors (those
        // without spaces or commas)
        $single_selector_regex = '/^([\w]+)(([.]|[#])(\w))?$/i';

        // split multiple selectors (comma-delimited)
        $selectors = explode(',',$selector);
    }//parse_selector


    /**
     * 
     */
    function delete_children_by_selector($selector) {
        $children = $this->get_children();
        $num_children = count($children);
        for( $i=0 ; $i < $num_children ; $i++ ) {
            $child =& $children[$i];   

            // make sure the child supports selectors
            if ( method_exists($child,'delete_children_by_selector') ) {
                // prune matching portions of the parse tree
                if ( $selector->matches($child) ) {
                    unset($this->children[$i]);
                    continue;
                } else {
                    $child->delete_children_by_selector($selector);
                }//else
            }//if
        }//for

        // reindex the child array, so indices are continuous
        $this->children = array_values($this->children);
    }//delete_children_by_selector


    /**
     * Returns a list of references to the elements in the 
     * tree starting at the current object that match the 
     * given selector.
     *
     * @param  Selector $selector the selector to use
     * @return array  the elements rooted at this tag matching 
     *                the given selector   
     */
    function get_elements_by_selector($selector) {
        $matching_elements = array();

        // check the current tag
        if ( $selector->matches($this) ) {
            $matching_elements[] =& $this;
        }//if
         
        // check the children
        $children = $this->get_children();
        $num_children = count($children);
        for( $i=0 ; $i < $num_children ; $i++ ) {
            $child =& $children[$i];   
            if ( method_exists($child,'get_elements_by_selector') ) {
                $child_matches = $child->get_elements_by_selector($selector);
                $num_child_matches = count($child_matches);
                for( $j=0 ; $j < $num_child_matches ; $j++ ) {
                    $matching_elements[] =& $child_matches[$j];
                }//for
            }//if
        }//for

        return $matching_elements;
    }//get_elements_by_selector


    /**
     *
     */
    function disable_all_inputs() {
        // get the inputs
        $inputs = $this->get_elements_by_selector(new TagSelector('input'));

        // get the select tags
        $selects = $this->get_elements_by_selector(new TagSelector('select'));

        // get the textareas
        $textareas = $this->get_elements_by_selector(new TagSelector('textarea'));

        // lump all the inputs together and disable all of 'em
        $all_input_elements = array_merge($inputs,$selects,$textareas);
        $num_input_elements = count($all_input_elements);
        for( $i=0 ; $i < $num_input_elements ; $i++ ) {
            $input_element =& $all_input_elements[$i];
            $input_element->set_attribute('disabled','disabled');
        }//for
    }//disable_all_inputs

}//class HTMLTag

?>
