<?php

require_once('phputil/classes/html_builder/HTMLTag.php');
require_once('phputil/classes/html_builder/css/IDSelector.php');

/**
 * This class contains unit tests for the IDSelector class
 */
class IDSelectorTester extends UnitTestCase {
    function setUp() {
        $this->tag_1 =& new HTMLTag(
            'div',
            array(
                'id'=>'tag_1',
                'class'=>'class_1',
                'dummy_attr'=>'dummy_attr'
            )
        );

        $this->tag_2 =& new HTMLTag(
            'div',
            array(
                'id'=>'tag_2',
                'class'=>'class_1',
            )
        );

        $this->tag_3 =& new HTMLTag(
            'div',
            array(
                'class'=>'class_1',
            )
        );
    }//setUp

   
    function tearDown() {
    }//tearDown


    function test_matches() {
        $tag_1_id_selector =& new IDSelector('tag_1');
        $tag_2_id_selector =& new IDSelector('tag_2');
        $this->assertTrue($tag_1_id_selector->matches($this->tag_1),'selector failed to match a known id');
        $this->assertFalse($tag_2_id_selector->matches($this->tag_1),'selector matched a different id');
        $this->assertFalse($tag_1_id_selector->matches($this->tag_3),'selector matched tag with no id attribute');
        $this->assertFalse($tag_2_id_selector->matches($this->tag_3),'selector matched tag with no id attribute');
    }//test_matches
}//class IDSelectorTester  

?>
