<?php

require_once('phputil/ekg_rhythm_constants.php');

/**
 * This class models a FISDAP EKG entry
 */
class FISDAP_EKG extends DataEntryDbObj {    

    /** 
     * Create a new EKG with the specified id and the specified db link
     */
    function FISDAP_EKG( $id, $connection ) {
        $this->DbObj(
            'EKGData',             //table name
            $id,                   //id
            'EKG_id',              //id field
            'Shift',               //parent entity name
            array('EvalSession'),  //children
            $connection            //database connection
        );
        $field_map = array( 
            'Shift_id' => 'Shift_id',
            'Student_id' => 'Student_id',
            'Run_id' => 'Run_id',
            'PerformedBy' => 'PerformedBy',
            'Rhythm' => 'Rhythm',
            'SubjectType_id' => 'SubjectType_id'
        );
        $this->set_field_map( $field_map );
    }//constructor FISDAP_EKG

    /**
     * The Interventions field maps to 3 different database fields:
     * Defibrillation, Pacing, and Sync.  After the rest of the fields
     * are mapped, insert the right values into these fields based on
     * the submitted Interventions field.
     */ 
    function postprocess_pda_submission(){
        if ( intval($this->get_field('Rhythm')) == 0 ) {
            $this->set_field('Rhythm', EKG_NORMAL_SINUS_RHYTHM);
        }//if

        switch( $this->pda_postdata[
                    $this->my_name().$this->pda_id.'Interventions'
                ] ) {
            case( '0' ) ://none
                break;
            case( '1' ) ://defib
                $this->set_field('Defibrillation','1');
                break;
            case( '2' ) ://pacing
                $this->set_field('Pacing','1');
                break;
            case( '3' ) ://sync
                $this->set_field('Sync','1');
                break;
        }//switch

        parent::postprocess_pda_submission();
    }//postprocess_pda_submission
}//class FISDAP_EKG

?>
