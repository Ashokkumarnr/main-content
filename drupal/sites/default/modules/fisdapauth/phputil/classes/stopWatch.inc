<?php
/**
 * A timer, similiar to a stopwatch.
 * Times are accumulated between start/stop intervals.
 * <pre>
 * Common usage:
 *   ...
 *   $sw = new StopWatch('sorting',true);   // start it now also
 *   ...
 *   usort(...);
 *   ...
 *   devlog($sw);
 *
 * Explicit usage:
 *   ...
 *   $sw = new StopWatch('sorting');
 *   foreach ($foo as $bar) {
 *      $sw->start();       // cumulative
 *      ...
 *      $sw->stop();
 *   }
 *   devlog($sw);       // devlog will ensure $sw->__toString() is used
 *
 * </pre>
 */
class StopWatch {
    private $cumulative = 0.0;
    private $name;
    private $running = false;
    private $start_time;

    /**
     * Constructor.
     * @param string $name The timer name.
     * @param boolean $start TRUE if the timer should be started.
     */
	public function __construct($name, $start=false) {
        $this->name = $name;

        if ($start) {
            $this->start();
        }
	}

	/**
	 * Retrieve the elapsed time since the last reset().
     * If the watch was never started 0.0 will be returned.
	 * @return float The elapsed time.
	 */
	public function elapsed() {
        $time = $this->cumulative + $this->get_last_interval_elapsed();
        return $time;
	}

    private function get_last_interval_elapsed() {
        if ($this->is_running()) {
		    $time = microtime(true) - $this->start_time;
            $time = max(0.0, $time);
        }
        else {
            $time = 0.0;
        }

        return $time;
    }

    /**
     * Overridden.
     */
    public function __toString() {
        $seconds = sprintf('%.3f', $this->elapsed());
        return 'Timer ' . $this->name . ': ' . $seconds . ' elapsed seconds.';
    }

    /**
     * Stop the watch.
     * If not running, nothing is done.
     */
    public function stop() {
        $this->cumulative = $this->elapsed();
        $this->running = false;
    }

    /**
     * Reset the watch to no accumulated time.
     * The watch can be running.
     */
	public function reset() {
        $this->cumulative = 0.0;
	}

    /**
     * Start timing.
     * If currently running, nothing happens.
     */
    public function start() {
        if ($this->is_running()) return;

		$this->start_time = microtime(true);
        $this->running = true;
    }

    /**
     * Determine if the timer is running.
     * @return boolean TRUE if the timer is running.
     */
    public function is_running() {
        return $this->running;
    }
}
?>
