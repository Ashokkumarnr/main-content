<?php 

require_once(FISDAP_ROOT.'phputil/sendNarrative.html');

/**
 * This class models a FISDAP Narrative
 * 
 * @todo: this class doesn't belong under DbObj, so we need to refactor
 *        the DbObj into two (or more) classes, where the MySQL db
 *        functionality is separated from the functionality for reading
 *        from pda postdata
 */
class FISDAP_Narrative extends DataEntryDbObj {    

    /** 
     * Create a new Narrative
     */
    function FISDAP_Narrative( $id, $connection ) {
        $this->pda_id_field = 'Narrative_id';
        $this->DbObj(
            '',                   //table name
            $id,                  //id
            'Narrative_id',       //id field
            'Shift',              //parent entity name
            array(),              //children
            $connection           //database connection
        );

        $field_map = array( 
            'Shift_id' => 'Shift_id', 
            'Narrative' => 'Narrative', 
            'Student_id' => 'Student_id',
            'StartDate' => 'StartDate',
            'AmbServ_id' => 'AmbServ_id',
            'Base_id' => 'Base_id' 
        ); 
        $this->set_field_map( $field_map );
    }//constructor FISDAP_Narrative

    /**
     * Send this narrative via email
     *
     * The key assumption here is that the Shift this narrative is
     * associated with has already been inserted into the MySQL
     * database.  Also, we need a valid $connection field so that the
     * SendNarrative function can do its database stuff
     */
    function send() {
        $my_name = 'Narrative';
        $postdata_prefix = $my_name . $this->pda_id;
        $dbConnect = $this->connection;
        $Shift_id = $this->get_field('Shift_id');
        $Student_id = $this->get_field('Student_id');
        $NarrativeText = $this->get_field('Narrative');
        $StartDate = $this->get_field('StartDate');
        $AmbServ_id = $this->get_field('AmbServ_id');
        $Base_id = $this->get_field('Base_id');
        SendNarrative( 
            $dbConnect, 
            $Shift_id, 
            $Student_id,
            $NarrativeText,
            $StartDate,
            $AmbServ_id,
            $Base_id
        );
    }//send

    function my_name() {
        return 'Narrative';
    }//my_name

    /**
     * @warning: BAD OO DESIGN!  overriding a base class method with a
     *           no-op method is EVIL. this won't be necessary after
     *           refactoring this class away from the standard DbObj,
     *           but for now we don't want anyone trying to insert
     *           narratives into MySQL
     */
    function to_mysql_db() {
        return false;
    }//to_mysql_db
}//class FISDAP_Narrative

?>
