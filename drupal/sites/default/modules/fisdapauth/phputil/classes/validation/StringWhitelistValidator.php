<?php

require_once('InputValidator.php');

/**
 * This validator only allows input that is in its
 * whitelist.
 */
class StringWhitelistValidator extends InputValidator {

    //////////////////////////////////////////////////////////// 
    // Private instance variables
    //////////////////////////////////////////////////////////// 

    /** the whitelist of valid strings */
    var $whitelist;


    //////////////////////////////////////////////////////////// 
    // Constructors
    //////////////////////////////////////////////////////////// 

    /**
     * Create a new StringWhitelistValidator with the given field name and
     * the given error message (optionally).
     *
     * @param string $field_name    the name of the input to check
     * @param string $error_message (optional) the error message to use
     */
    function StringWhitelistValidator( $field_name,
                                       $error_message='' ) {
        if ( !$error_message ) {
            $error_message = 'Invalid input received. Please try again.';
        }//if
        $this->InputValidator($field_name,$error_message);
        $this->whitelist = array();
    }//StringWhitelistValidator


    //////////////////////////////////////////////////////////// 
    // Public instance methods 
    //////////////////////////////////////////////////////////// 

    /** 
     * If there's a value for our field in the given input, make
     * sure it exists in the whitelist
     *
     * @param array $input the input to validate
     */
    function validate($input) {
        if ( isset($input[$this->get_field_name()]) && 
             !in_array($input[$this->get_field_name()],
                       $this->get_whitelist()) ) {
            $error = new ValidationError($this->error_message);
            $this->add_error($error);
        }//if
    }//validate

 
    /** 
     * Set the whitelist to the given array of strings
     *
     * @param array $whitelist the whitelist to use
     */
    function set_whitelist($whitelist) {
        $this->whitelist = $whitelist;
    }//set_whitelist


    /**
     * Get the whitelist array.
     *
     * @return array the whitelist
     */
    function get_whitelist() {
        return $this->whitelist;
    }//get_whitelist


    /** 
     * This is the Prototype method for Validators.  This returns a new
     * StringWhitelistValidator with the same field name and error message as 
     * this instance.
     */
    function &php4_compat_clone() {
        return new StringWhitelistValidator(
            $this->get_field_name(),
            $this->get_error_message()
        );
    }//php4_compat_clone
}//class StringWhitelistValidator
?>
