<?php

require_once('ValidationSet.php');

/** 
 * This class is the basis for validation sets that are 
 * associated with key/value pairs of input (e.g., validating
 * $_POST or $_GET)
 */
class InputValidationSet extends ValidationSet {

    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /** 
     *
     */
    function InputValidationSet() {
    }//InputValidationSet


    ////////////////////////////////////////////////////////////
    // Public Instance Methods
    ////////////////////////////////////////////////////////////

    /** 
     * Returns an array containing all errors raised in the children,
     * indexed by input field name.  For generic errors, or those
     * specific to this validation set itself, the key '' is used.
     *
     * @return array all errors raised by the child validators, indexed
     *               by input field name
     */
    function get_errors() {
        $errors = array();
        $errors[''] = $this->errors;
        $children = $this->get_children();
        $num_children = count($children);
        for( $i=0 ; $i < $num_children ; $i++ ) {
            $child =& $children[$i];
            if ( $child->has_errors() ) {
                // create the field's error array if necessary
                if ( !isset($errors[$child->get_field_name()]) ) {
                    $errors[$child->get_field_name()] = array();
                }//if

                // add all the child's errors to the field's array
                $child_errors = $child->get_errors();
                $num_child_errors = count($child_errors);
                for( $j=0 ; $j < $num_child_errors ; $j++ ) {
                    $child_error = $child_errors[$j];
                    $errors[$child->get_field_name()][] = $child_error;
                }//for
            }//if
        }//for
        return $errors;
    }//get_errors


    /** 
     * Convenience function for cases when multiple fields have the
     * same expected format.  Adds a copy of the validator for each
     * of the given input names.
     */
    function add_common_validator($validator,$input_names) {
        foreach( $input_names as $input_name ) {
            // create a fresh copy of the validator prototype for
            // each input field
            $new_validator =& $validator->php4_compat_clone();
            $new_validator->set_field_name($input_name);
            $this->append_child($new_validator);
        }//foreach
    }//add_common_validator
}//class InputValidationSet
?>
