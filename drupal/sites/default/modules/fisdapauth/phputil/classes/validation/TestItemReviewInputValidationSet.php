<?php

require_once('SingletonTestItemReviewValidationSet.php');

class TestItemReviewInputValidationSet extends SingletonTestItemReviewValidationSet {
    function TestItemReviewInputValidationSet() {
        // this'll add the required field validators, as
        // well as the yes/no validators
        parent::SingletonTestItemReviewValidationSet();

        // add the other validators not found in the singleton reviews
        $this->add_cutscore_validator();
        $this->add_k_a_p_validator();
        $this->add_importance_validator();
    }//TestItemReviewInputValidationSet


    function add_importance_validator() {
        $MIN_IMPORTANCE = 0;
        $MAX_IMPORTANCE = 2;
        $this->append_child(
            new IntegerRangeValidator(
                'important_to_memorize',
                $MIN_IMPORTANCE,
                $MAX_IMPORTANCE,
                'Invalid input received.  Please select one of the above options.'
            )
        );
    }//add_importance_validator


    function add_k_a_p_validator() {
        $k_a_p_validator = new StringWhitelistValidator(
            'item_measures_k_a_p',
            'Invalid input received.  Please select one of the above options.'
        );
        $k_a_p_validator->set_whitelist(array('k','a','p'));
        $this->append_child($k_a_p_validator);
    }//add_k_a_p_validator


    function add_cutscore_validator() {
        $MIN_PERCENT = 0;
        $MAX_PERCENT = 100;
        $this->append_child(
            new IntegerRangeValidator(
                'percent_minimally_qualified',
                $MIN_PERCENT,
                $MAX_PERCENT,
                'Please enter a whole number between '.$MIN_PERCENT.' and '.$MAX_PERCENT
            )
        );
    }//add_cutscore_validator


    function get_required_fields() {
        return array(
            'id',
            'item_id',
            'number_in_group',
            'reviewer',
            'stem_grammar_correct',
            'distractors_match_grammar',
            'distractors_less_correct',
            'distractors_similar_yet_distinct',
            'answer_always_correct',
            'item_free_from_complication',
            'item_unbiased',
            'level_of_difficulty_matches_job',
            'item_measures_k_a_p',
            'important_to_memorize',
            'percent_minimally_qualified'
        );
    }//get_required_fields


    function get_yes_no_fields() {
        return array(
            'stem_grammar_correct',
            'distractors_match_grammar',
            'distractors_less_correct',
            'distractors_similar_yet_distinct',
            'answer_always_correct',
            'item_free_from_complication',
            'item_unbiased',
            'level_of_difficulty_matches_job'
        );
    }//get_yes_no_fields
}//class TestItemReviewInputValidationSet
?>
