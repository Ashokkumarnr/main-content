<?php

/**
 * This class encapsulates errors validating a form submission
 */
class ValidationError {

    ////////////////////////////////////////////////////////////
    // Private instance variables
    ////////////////////////////////////////////////////////////

    /** The error message to report  */
    var $message;


    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     * Create a new ValidationError with the given error message
     *
     * @param string $message an error message
     */
    function ValidationError( $message ) {
        $this->set_message($message);
    }//ValidationError


    ////////////////////////////////////////////////////////////
    // Public Instance Methods
    ////////////////////////////////////////////////////////////

    /**
     * Returns the error message
     *
     * @return string the error message
     */
    function get_message() {
        return $this->message;
    }//get_message


    /**
     * Set the error message to the given string
     *
     * @param string $message the new error message to use 
     */
    function set_message( $message ) {
        $this->message = $message;
    }//set_message
}//class ValidationError

?>
