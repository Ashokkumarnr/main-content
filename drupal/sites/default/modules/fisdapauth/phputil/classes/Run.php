<?php 

require_once('phputil/bls_skill_constants.php');
require_once('phputil/bls_skill_modifier_constants.php');
require_once('phputil/med_route_constants.php');
require_once('phputil/med_name_constants.php');


/**
 * This class models a FISDAP Run
 */
class FISDAP_Run extends DataEntryDbObj {    
    /** 
     * Create a new run with the specified id and the specified db link
     */
    function FISDAP_Run( $id, $connection ) {
        $this->DbObj(
            'RunData',         //table name
            $id,               //id
            'Run_id',          //id field
            'Shift',           //parent entity name
            array(             //child entities
                'IV', 'EKG', 'PtComp', 'Med', 'Airway', 'Other',
                'Narrative', 'BLS', 'EvalSession'
            ),
            $connection        //database connection
        );
        $field_map = array( 
            'Shift_id' => 'Shift_id',
            'Student_id' => 'Student_id',
            'TeamNum' => 'NumInTeam',
            'TeamLead' => 'TeamLeader',
            'Phase' => 'Phase',
            'Priority' => 'Priority',
            'Preceptor_id' => 'Precept_id',
            'Years' => 'Age',
            'Months' => 'Months',
            'Gender' => 'Gender',
            'Ethnicity' => 'Ethnicity',
            'Disposition' => 'Disposition',
            'Diag1' => 'Diagnosis',
            'Diag2' => 'Diag2',
            'LOC' => 'LOC',
            'MOI' => 'MOI',
            'SystolicBP' => 'SystolicBP',
            'DiastolicBP' => 'DiastolicBP',
            'Witness' => 'Witness',
            'PulseReturn' => 'PulseReturn',
            'SubjectType_id' => 'SubjectType_id'
        );
        $this->set_field_map( $field_map );
    }//constructor FISDAP_Run

    /**
     * After the Run has been populated from the postdata,  make sure 
     * the Witness and PulseReturn fields are valid for the MySQL db.
     */ 
    function postprocess_pda_submission(){
        $my_name = $this->my_name();

        // correct the Witness field if it's < 1
        if ( !isset( $this->db_result['Witness'] ) || 
             $this->db_result['Witness'] < 1 ) {
            $this->db_result['Witness'] = 1;
        }//if

        // correct the PulseReturn field if it's < 1
        if ( !isset( $this->db_result['PulseReturn'] ) || 
             $this->db_result['PulseReturn'] < 1 ) {
            $this->db_result['PulseReturn'] = 1;
        }//if

        // prepare any complaints associated with this run
        $this->postprocess_pda_complaint_entries();

        // prepare any bls entries associated with this run
        $this->postprocess_pda_bls_entries();

        // this will get rid of all this entity's fields
        parent::postprocess_pda_submission();
    }//postprocess_pda_submission

    /**
     * Fix the bls entries in the postdata so they're 
     * in the mysql-friendly format the BLS class will
     * expect.
     */
    function postprocess_pda_bls_entries( ) {
        $my_name = $this->my_name();

        // @todo: can I make these things static members of the BLS class ?
        //create a dummy BLS skill to get its attributes
        $bls_obj = new FISDAP_BLS_Skill(0,NULL); 
        $bls_obj->set_pda_postdata($this->pda_postdata);
        $bls_entity_name = $bls_obj->my_name();
        $min_id = $bls_obj->get_next_pda_id();
        $bls_skill_modifier = -3;
        $pda_bls_airway_type = $this->pda_postdata[
            $my_name.$this->pda_id.'BLSAirwayType'
        ];

        // prepare for clients that synced before the zero-based index
        // fix (when 0 was the oral airway key)
        if ( intval($pda_bls_airway_type) == 0 ) {
            $pda_bls_airway_type = BLS_MOD_ORAL_PHARYNGEAL_AIRWAY;
        }//if

        $pda_bls_fields = array(
                'Hospital' => 1, 'MDConsult' => 2, 'Interview' => 3, 
                'Exam' => 4, 'Vitals' => 5, 'Suction' => 6, 'LBoard' => 7, 
                'PtMoved' => 8, 'Ventilation' => 9, 'Traction' => 10, 
                'Bandaging' => 11, 'CPR' => 12, 'Joint' => 13, 
                'BLSAirway' => 14, 'CSpine' => 15, 'LBone' => 16
        );

        foreach( $pda_bls_fields as $skill_field => $skill_code ) {
            //create a postdata entry compatible with the BLS class
            // performed_by ==  0 -> none,   1 -> perf,   2 -> obs
            $skill_postdata_key = $my_name.$this->pda_id.$skill_field;
            $performed_by = $this->pda_postdata[$skill_postdata_key];

            if ( $performed_by ) {
                //if they performed or observed the skill, insert an entry
                
                //get rid of the old postdata
                unset($this->pda_postdata[$skill_postdata_key]); 
                
                //insert the postdata fields the BLS class expects to see
                $new_bls_key_base = $bls_entity_name.($min_id--);
                $this->pda_postdata[$new_bls_key_base.'Run_id'] = $this->pda_id;
                $this->pda_postdata[
                    $new_bls_key_base.'Student_id'
                ] = $this->get_field('Student_id');
                $this->pda_postdata[
                    $new_bls_key_base.'Shift_id'
                ] = $this->get_field('Shift_id');
                $this->pda_postdata[$new_bls_key_base.'Assess_id'] = 0;
                $this->pda_postdata[
                    $new_bls_key_base.'SubjectType_id'
                ] = $this->get_field('SubjectType_id');
                $this->pda_postdata[
                    $new_bls_key_base.'SkillCode'
                ] = $skill_code;
                $this->pda_postdata[
                    $new_bls_key_base.'PerformedBy'
                ] = $performed_by;

                //the skill modifier changes for bls airways
                if ( $skill_code == BLS_AIRWAY ) {
                    $this->pda_postdata[
                        $new_bls_key_base.'SkillModifier'
                    ] = $pda_bls_airway_type;
                } else {
                    $this->pda_postdata[
                        $new_bls_key_base.'SkillModifier'
                    ] = $bls_skill_modifier;
                }//else
            }//if
        }//foreach

        // use the Oxygen field to make a Med entry if applicable
        $oxygen_val = $this->pda_postdata[$my_name.$this->pda_id.'Oxygen'];
        if ( $oxygen_val && is_numeric($oxygen_val) && 
             ($oxygen_val==1 || $oxygen_val==2) ) {
            // get the necessary values from a dummy med object
            $med_obj = new FISDAP_Med(0,NULL);
            $med_entity_name = $med_obj->my_name();
            $med_obj->set_pda_postdata($this->pda_postdata);
            $min_id = $med_obj->get_next_pda_id();
            $med_obj->pda_id = $min_id;

            //insert a Med oxygen admin entry into the postdata 
            
            // translate the BLS PerformedBy into the Med PerformedBy
            $med_obj->set_field('PerformedBy',$oxygen_val-1); 
            $med_obj->set_field('Dose','10-15 lpm'); // default Oxygen dose
            $med_obj->set_field('Medication',MED_OXYGEN); 
            $med_obj->set_field('Shift_id',$this->get_field('Shift_id'));
            $med_obj->set_field('Student_id',$this->get_field('Student_id'));
            $med_obj->set_field('Run_id',$this->pda_id);
            $med_obj->set_field('Route',MED_ROUTE_SIMPLE_MASK); //default route for oxygen 
            $med_obj->set_field('SubjectType_id','2'); // live human by default

            //put the med entry in the postdata we're working with
            $med_obj->to_pda_postdata();
            //$med_obj->email_status_report('Med object for Run oxygen entry');
        }//if

    }//postprocess_pda_bls_entries

    /**
     * Fix the complaint entries in the postdata so they're 
     * in the mysql-friendly format the Complaint class will
     * expect.
     */
    function postprocess_pda_complaint_entries( ) {
        $num_complaints = 8;

        // figure out the next pda complaint id
        $complaint_obj = new FISDAP_Complaint(0,null);
        $complaint_obj->set_pda_postdata($this->pda_postdata);
        $complaint_entity_name = $complaint_obj->my_name();
        $min_id = $complaint_obj->get_next_pda_id();

        if ( isset( 
                 $this->pda_postdata[
                     $complaint_entity_name.$this->pda_id.'Shift_id'
                 ] 
             ) 
           ) {
            // if there were complaints attached to this run, rework
            // them into the format the DbObj will expect
            for( $i = 0 ; $i < $num_complaints ; $i++,$min_id-- ) {
                //translate the aggregate submission into individual
                //complaint entries
                $complaint_key = $complaint_entity_name 
                    . $this->pda_id 
                    . 'Complaint' 
                    . $i;
                $new_complaint_key = $complaint_entity_name 
                    . $min_id 
                    . 'Complaint_id';

                if ( !isset($this->pda_postdata[$complaint_key]) ) continue;
                $complaint_val = $this->pda_postdata[$complaint_key];
                unset($this->pda_postdata[$complaint_key]);

                if ( $complaint_val > 0 ) {
                    // only add the new complaint entry if the complaint
                    // was selected
                    $this->pda_postdata[$new_complaint_key] = $complaint_val;

                    // add the Assess id, Shift id, Student_id,
                    // SubjectType id, and Run id fields
                    $this->pda_postdata[
                        $complaint_entity_name.$min_id.'Shift_id'
                    ] = $this->get_field('Shift_id');
                    $this->pda_postdata[
                        $complaint_entity_name.$min_id.'Student_id'
                    ] = $this->get_field('Student_id');
                    $this->pda_postdata[
                        $complaint_entity_name.$min_id.$this->pda_id_field
                    ] = $this->pda_id;
                    $this->pda_postdata[
                        $complaint_entity_name.$min_id.'Assess_id'
                    ] = 0;
                    $this->pda_postdata[
                        $complaint_entity_name.$min_id.'SubjectType_id'
                    ] = 2; // live human by default
                }//if
            }//for

            // get rid of the rest of the original complaint submission 
            $remaining_fields = array(
                'Shift_id','Student_id','Run_id','Assess_id','dataValid',
                'SubjectType_id','SubjectGroupName'
            );
            foreach( $remaining_fields as $field ) {
                unset(
                    $this->pda_postdata[
                        $complaint_entity_name.$this->pda_id.$field
                    ]
                );
            }//foreach
        }//if
    }//postprocess_pda_complaint_entries

    
    /**
     *
     */
    function to_mysql_db() {
        // translate a diastolic bp of 'P' to -2
        if ( $this->get_field('DiastolicBP') == 'P' ) {
            $this->set_field('DiastolicBP',-2);
        }//if
        parent::to_mysql_db();
    }//to_mysql_db
}//class FISDAP_Run

?>
