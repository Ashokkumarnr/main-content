<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FisdapLogger.inc');
require_once('phputil/classes/MySqlStatementBuilder.inc');
require_once('phputil/classes/model/AbstractModel.inc');
require_once('phputil/classes/model/ModelCriteria.inc');
require_once('phputil/exceptions/FisdapDatabaseStatementException.inc');
require_once('phputil/exceptions/FisdapRuntimeException.inc');

/**
 * This is a Singleton class for providing a connection
 * to the FISDAP MySQL database
 */
final class FISDAPDatabaseConnection 
{
    /** the mysql link resource */
    var $mysql_link;

    /** the database to connect to */
    private $database;

    /** the host on which the database server runs */
    var $db_host = "members1.fisdap.net";

    /** the database user for the connection */
    var $db_user = "root";

    /** the password for the database user */
    var $db_pass = "";

    private $observers = array();

    private $statement_builder;

	/**
	 * Stores the sticky database, if set
	 * @see set_sticky_database
	 * @var string|false
	 */
	private static $sticky;

    const FISDAP_DB_NAME = 'FISDAP';
    const FISDAP_TESTING_DB_NAME = 'FISDAPTEST';

    /**
     * Construct a database connection object for the given database.
     * Note that this should not be called directly by client code.  You
     * should instead use FISDAPDatabaseConnection::get_instance() to 
     * get the singleton instance (thus avoiding multiple physical
     * database connections when creating multiple logical connection
     * objects).
     */
    public function FISDAPDatabaseConnection($database) 
    {
        $this->database = $database;

        if (($this->database == self::FISDAP_DB_NAME) || 
            ($this->database == self::FISDAP_TESTING_DB_NAME)) 
        {
            // define FISDAP_ROOT if it hasn't already been defined
            // elsewhere, for compatibility with files that require
            // dbconnect 
            if ( !defined('FISDAP_ROOT') ) 
            {
                define(
                    'FISDAP_ROOT',
                    substr(
                        getcwd(),
                        0,
                        strpos(getcwd(), '/FISDAP') + 7
                    ) . '/'
                );
            }
        }

        $this->statement_builder = new MySqlStatementBuilder();

        // connect to the mysql server and select the proper database
        $this->connect();
        $this->select_database();
    }

    /**
     * Create a statement builder.
     * @return DbStatementBuilder The builder.
     */
    public function get_statement_builder() {
        return $this->statement_builder;
    }

    /**
     * Add an observer.
     * @param DbObserver $observer The observer.
     */
    public function add_observer($observer) {
        $this->observers[] = $observer;
    }

    /**
     * Remove an observer.
     * @param DbObserver $observer The observer to remove.
     */
    public function remove_observer($observer) {
        $new_list = array();

        foreach ($this->observers as $o) {
            if ($o !== $observer) {
                $new_list[] = $observer;
            }
        }

        $this->observers = $new_list;
    }

    /**
     * Attempt to make a database connection
     */
    public function connect() 
    {
        $create_new_link = true;

        $this->mysql_link = @mysql_connect(
            $this->db_host,
            $this->db_user,
            $this->db_pass,
            $create_new_link
        );

        if ( !$this->mysql_link ) {   
            throw new FisdapRuntimeException(
                "Unable to connect to database[$this->database]");
            exit(1);
        }
    }

    private function get_sql_message() {
        $code = mysql_errno($this->mysql_link);
        if (!$code) return '';

        return "Error[$code]: " . mysql_error($this->mysql_link);
    }

    /**
     * Select the current database
     */
    public function select_database() 
    {
        if (!@mysql_select_db($this->database, $this->mysql_link) ) {   
            throw new FisdapRuntimeException(
                "Unable to select database[$this->database]: " .
                $this->get_sql_message());
        }
    }

    /**
     * Get the singleton database connection.
	 *
	 * @param string $database the name of the database to connect to. Be aware 
	 * that this will be overridden if a sticky database is set.
     */
    public static function get_instance($database='FISDAP') 
    {
        static $instances;
        if ( !is_array($instances) ) {
            $instances = array();
        }

		if (self::$sticky) {
			$database = self::$sticky;
		}

        if ( !isset($instances[$database]) ) {
            $instances[$database] = new FISDAPDatabaseConnection($database);
        }

        return $instances[$database];
    }

	/**
	 * Set the given database as "sticky".
	 *
	 * If this has been called, all calls to {@link get_instance} will get an 
	 * instance of the database indicated here, regardless of what is specified 
	 * in the parameter.
	 *
	 * <em>This is for use during testing only.</em>
	 *
	 * @param string|false The name of the database to make sticky. If false, 
	 * will clear any existing sticky database so that normal operation may 
	 * continue.
	 */
	public static function set_sticky_database($database) {
		if ($database === false) {
			self::$sticky = $database;
		} else {
			Assert::is_string($database);
			self::$sticky = $database;
		}
	}

    /**
     * Perform a delete operation.
     * @param string statement The delete statement.
     * @return The number of affected rows.
     * @throws FisdapDatabaseStatementException
     */
    public function purge($statement)
    {
        $this->notify_statement($statement);

        $ok = mysql_query($statement, $this->mysql_link);
        if (!$ok) 
        {
            throw new FisdapDatabaseStatementException(
                'Delete failed', $this->get_sql_message(), $statement);
        }

        // Return the number of affected rows.
        return mysql_affected_rows($this->mysql_link);
    }

    /**
     * Perform an insert operation.
     * @param string statement The insert statement.
     * @return The number of affected rows.
     * @throws FisdapDatabaseStatementException
     */
    public function insert($statement)
    {
        $this->notify_statement($statement);

        $ok = mysql_query($statement, $this->mysql_link);
        if (!$ok) 
        {
            throw new FisdapDatabaseStatementException(
                'Insert failed', $this->get_sql_message(), $statement);
        }

        // Return the number of affected rows.
        return mysql_affected_rows($this->mysql_link);
    }

    /**
     * Perform a lock operation.
     * @param string statement The lock statement.
     * @return TRUE if the lock was successfully acquired.
     * @throws FisdapDatabaseStatementException
     */
    public function lock($statement)
    {
        $this->notify_statement($statement);

        $ok = mysql_query($statement, $this->mysql_link);
        if (!$ok) 
        {
            throw new FisdapDatabaseStatementException(
                'Unable to acquire lock', $this->get_sql_message(),
                $statement);
        }

        return $ok;
    }

    /**
     * Perform an unlock operation.
     * @param string statement The unlock statement.
     * @return TRUE if the unlock was success.
     * @throws FisdapDatabaseStatementException
     */
    public function unlock($statement)
    {
        $this->notify_statement($statement);

        $ok = mysql_query($statement, $this->mysql_link);
        if (!$ok) 
        {
            throw new FisdapDatabaseStatementException(
                'Unable to release lock', $this->get_sql_message(),
                $statement);
        }

        return $ok;
    }

    /**
     * Perform an update operation.
     * NOTE: MySql may indicate a row has NOT been updated
     *   if the data did NOT change.
     * @param string statement The update statement.
     * @return The number of affected rows.
     * @throws FisdapDatabaseStatementException
     */
    public function update($statement)
    {
        $this->notify_statement($statement);

        $ok = mysql_query($statement, $this->mysql_link);
        if (!$ok) 
        {
            throw new FisdapDatabaseStatementException(
                'Update failed', $this->get_sql_message(), $statement);
        }

        // Return the number of affected rows.
        return mysql_affected_rows($this->mysql_link);
    }

    /**
     * Get the result set for the given query.
     * @return array The results as an associate array of rows.
     * @throws FisdapDatabaseStatementException
     */
    public function query($query) 
    {
        $this->notify_statement($query);

        $result = mysql_query($query, $this->mysql_link);
        if ( !$result ) 
        {
            throw new FisdapDatabaseStatementException(
                'Query failed', $this->get_sql_message(), $query);
        }

        // collect the rows in the result set
        $rows = array();
        while( $row = @mysql_fetch_assoc($result) ) 
        {
            $rows[] = $row;
        }

        // in case we want to use this for giant result sets, 
        // go ahead and free the result now
        @mysql_free_result($result);
         
        return $rows;
    }


    /**
     * Returns the id of the most recently inserted record.
     * @return int the last insert id
     */
    public function get_last_insert_id() 
    {
        return mysql_insert_id($this->mysql_link);
    }


    /**
     * Return the mysql link for use with lower-level database handling routines.
     * @return resource The DB resource.
     */
    public function get_link_resource() 
    {
        return $this->mysql_link;
    }


    /**
     * Return the name of the database for this connection.
     * @return string The DB name.
     */
    public function get_database()
    {
        return $this->database;
    }

    /**
     * Notify the observers of a new statement.
     * @param string $statement The statement that executed.
     */
    private function notify_statement($statement) {
        foreach ($this->observers as $observer) {
            $observer->statement($statement);
        }
    }

    /**
     * Retrieve all entries matching a criteria.
     * @param ModelCritiera $critera The search critiera.
     * @return array A list of objects. 
     */
    public function get_by_criteria(ModelCriteria $criteria) {
        $statement = $this->statement_builder->get_select_statement_by_criteria(
            $criteria);
        $rows = $this->query($statement);

        $model = $criteria->get_model();
        $class = get_class($model);

        $result = array();
        foreach ($rows as $row) {
            $result[] = new $class($row);
        }

        return $result;
    }

    /**
     * Retrieve all entries matching a criteria.
     * @param ModelCritiera $critera The search critiera.
     * @return array An associative array of results.
     */
    public function get_array_by_criteria(ModelCriteria $criteria) {
        $statement = $this->statement_builder->get_select_statement_by_criteria(
            $criteria);
        return $this->query($statement);
    }

    /**
     * Delete all entries matching a criteria.
     * @param ModelCritiera $critera The search critiera.
     * @return int The number of deleted rows.
     */
    public function delete_by_criteria(ModelCriteria $criteria) {
        $statement = $this->statement_builder->get_delete_statement_by_criteria(
            $criteria);
        return $this->purge($statement);
    }

    /**
     * Update all entries matching a criteria.
     * @param ModelCritiera $criteria The search critiera.
     * @param ModelCriteria $update_criteria The update criteria, IN, NOT, OR ignored.
     * @return int The number of deleted rows.
     */
    public function update_by_criteria(ModelCriteria $criteria, ModelCriteria $update_criteria) {
        $statement = $this->statement_builder->get_update_statement_by_criteria(
            $criteria, $update_criteria);
        return $this->update($statement);
    }
}

/**
 * An observer that watches the DB.
 */
interface DbObserver {
    /**
     * Update the observer with the statement that executed.
     * @param string $statement The statement.
     */
    public function statement($statement);
}

/**
 * An observer that logs to the log file.
 */
final class LogDbObserver implements DbObserver {
    private $logger;

    public function __construct() {
        $this->logger = FisdapLogger::get_logger();
    }

    public function statement($statement) {
        $s = preg_replace('/[\\r\\n]+/', ' ', $statement);
        $this->logger->debug($s);
    }
}
?>
