<?php

require_once('phputil/classes/DbObj.php');
require_once('phputil/classes/DataEntryDbObj.php');
require_once('phputil/bls_skill_constants.php');


/**
 * This class represents FISDAP Shifts
 */
class FISDAP_Shift extends DataEntryDbObj {
    /** 
     * Create a new shift with the specified id 
     */
    function FISDAP_Shift( $id, $connection ) {
        $this->DbObj(
            'ShiftData',          //table name
            $id,                  //id
            'Shift_id',           //id field
            'Shift',              //parent entity name
            array(                //children
                'Run', 'Assess', 'IV', 'EKG', 'PtComp', 'Med', 'Airway',
                'Other', 'Narrative', 'BLSC', 'EvalSession'
            ),
            $connection           //database connection
        );        

        $field_map = array( 
            'Student_id'    =>   'Student_id', 
            'AmbServ_id'    =>   'AmbServ_id',
            'Base_id'       =>   'StartBase_id',
            'StartDate'     =>   'StartDate',
            'StartTime'     =>   'StartTime',
            'Hours'         =>   'Hours',
            'Completed'     =>   'Completed',
            'CompletedFrom' =>   'CompletedFrom',
            'Type'          =>   'Type'
        );
        $this->set_field_map( $field_map );
    }//constructor FISDAP_Shift

    /**
     * Calculate the EndTime for this shift, based on the start time and
     * number of hours.
     *
     * Note: this method requires that the StartDate and StartTime have
     * already been set
     */
    function set_end_time() {
        $start_time = $this->db_result['StartTime'];
        $start_date = $this->db_result['StartDate'];
        $hours = $this->db_result['Hours'];

        $date_container = array();        
        $date_container = explode("-",$start_date);

        if (strlen($start_time)==3) $start_time = '0'.$start_time;

        $start_hour = substr($start_time,0,2);
        $start_min = substr($start_time,2,2);

        $styear = $date_container[0];
        $stmonth = $date_container[1];
        $stday = $date_container[2];

        $start_timestamp = mktime(
            $start_hour, $start_min, 0, $stmonth, $stday, $styear
        );
        $duration_seconds = $hours * 60 * 60;
        $end_timestamp = $start_timestamp + $duration_seconds;

        $end_time = date("Hi",$end_timestamp);
        $this->db_result['EndTime'] = $end_time;
    }//set_end_time

    /**
     * After the Shift has been populated from the postdata, if the
     * shift already existed, read it from the database.  Otherwise, set
     * the end time based on the start time and number of hours.
     */ 
    function postprocess_pda_submission(){
        // for now, check for a bad StartDate field, and correct
        // it if necessary
        $matches = array();
        if ( preg_match(
                '/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/',
                $this->get_field('StartDate'),
                $matches
             ) ) {
            $month = $matches[1];
            $day = $matches[2];
            $year = $matches[3];
            $this->set_field('StartDate',$year.'-'.$month.'-'.$day);
        }//if

        if ( !$this->web_id ) {
            $this->set_end_time();
        }//if 

        $this->postprocess_pda_bls_entries();

        parent::postprocess_pda_submission();
    }//postprocess_pda_submission

    /**
     * if the shift type is not field, process the bls entries into 
     * mysql-friendly chunks that the BLS class can parse
     */
    function postprocess_pda_bls_entries() {
        //if ( $this->get_field('Type') != 'field' ) {
        if ( false ) {
            // @todo: can I make these things static members of the BLS class ?
            $bls_obj = new FISDAP_BLS_Skill(0, NULL); 
            $bls_obj->set_pda_postdata($this->pda_postdata);
            $bls_entity_name = $bls_obj->my_name().'C';
            $min_id = $bls_obj->get_next_pda_id();
            $bls_skill_modifier = -3;
            $pda_bls_airway_type = $this->pda_postdata[
                $bls_entity_name.$this->pda_id.'BLSAirwayType'
            ];
            $pda_bls_fields = array(
                'HospitalO' => 1, 'MDConsultO' => 2, 'InterviewO' => 3, 
                'ExamO' => 4, 'VitalsO' => 5, 'SuctionO' => 6, 
                'LBoardO' => 7, 'PtMovedO' => 8, 'VentilationO' => 9, 
                'TractionO' => 10, 'BandagingO' => 11, 'CPRO' => 12, 
                'JointO' => 13, 'BLSAirwayOralO' => 14, 
                'BLSAirwayNasoO' => 14, 'CSpineO' => 15, 'LBoneO' => 16,
                'HospitalP' => 1, 'MDConsultP' => 2, 'InterviewP' => 3, 
                'ExamP' => 4, 'VitalsP' => 5, 'SuctionP' => 6, 
                'LBoardP' => 7, 'PtMovedP' => 8, 'VentilationP' => 9, 
                'TractionP' => 10, 'BandagingP' => 11, 'CPRP' => 12, 
                'JointP' => 13, 'BLSAirwayOralP' => 14, 
                'BLSAirwayNasoP' => 14, 'CSpineP' => 15, 'LBoneP' => 16
            );

            // @todo : handle BLS Airway Naso/Oral obs/perf 
            // @warning: the blsc assess_id is reporting -1
            foreach( $pda_bls_fields as $skill_field => $skill_code ) {
                //create a postdata entry compatible with the BLS class

                // @todo: rewrite this to use the OO style of the Med
                // for Oxygen (see Run.php)

                //create the multiples for each skill
                $performed_by = (substr($skill_field,strlen($skill_field)-1) == 'P') ? 1 : 2; 
                $skill_postdata_key =  $bls_entity_name . $this->pda_id . $skill_field; 
                if ( $performed_by == 1  || $performed_by == 2 ) {
                    for ( $i=0 ; $i < $this->pda_postdata[$skill_postdata_key] ; $i++ ) {
                        //1 -> perf, 2 -> obs ... set based on the last
                        //char in the field ('O' or 'P')
                        
                        //if they performed or observed the skill,
                        //insert an entry

                        //insert the postdata fields the BLS class
                        //expects to see
                        
                        //@todo: replace the hard-coded live human
                        //subject when we put subject types on the pda
                        $new_bls_key_base = 'BLS'.($min_id--);
                        $this->pda_postdata[$new_bls_key_base.'Run_id'] = 0;
                        $this->pda_postdata[$new_bls_key_base.'Student_id'] = $this->get_field('Student_id');
                        $this->pda_postdata[$new_bls_key_base.'Shift_id'] = $this->pda_id;
                        $this->pda_postdata[$new_bls_key_base.'Assess_id'] = 0;
                        $this->pda_postdata[$new_bls_key_base.'SubjectType_id'] = 2;//live human by default 
                        $this->pda_postdata[$new_bls_key_base.'SkillCode'] = $skill_code;
                        $this->pda_postdata[$new_bls_key_base.'PerformedBy'] = $performed_by;
                        
                        //the skill modifier changes for bls airways
                        if ( $skill_code == BLS_AIRWAY  ) {
                            if ( substr($skill_field,9,1) == 'N' ) { // Naso, skill mod = 0
                                $this->pda_postdata[$new_bls_key_base.'SkillModifier'] = 0;
                            } else { // Oral, skill mod = 1
                                $this->pda_postdata[$new_bls_key_base.'SkillModifier'] = 1;
                            }//else
                        } else {
                            $this->pda_postdata[$new_bls_key_base.'SkillModifier'] = $bls_skill_modifier;
                        }//else
                    }//for
                }//if
            }//foreach

            // @todo: make sure this code is culled/updated if there's ever some other
            //        thing in the database that starts with "BLSC"
            foreach( $this->pda_postdata as $key => $val ) {
                if ( substr( $key, 0, 4 ) == 'BLSC' &&
                     (is_numeric(substr($key,4,1)) || is_numeric(substr($key,4,2))) ) {
                    unset( $this->pda_postdata[$key] );
                }//if
            }//foreach
        }//if
    }//postprocess_pda_bls_entries

    
    /**
     * Returns true iff the shift is complete (disallowing any further 
     * edits to the data therein).
     *
     * @return bool  true iff the shift is complete
     */
    function is_complete() {
        return $this->get_field('Complete');
    }//is_complete
}//FISDAP_Shift


?>
