<?php 

/**
 * This class models a FISDAP BLS Skill
 *
 */
class FISDAP_BLS_Skill extends DataEntryDbObj {    

    /** 
     * Create a new BLS Skill with the specified id and the specified db link
     */
    function FISDAP_BLS_Skill( $id, $connection ) {
        $this->pda_id_field = 'BLS_id';
        $this->DbObj('BLSSkillsData',                                           //table name
                     $id,                                                       //id
                     'Skill_id',                                                //id field
                     'Shift',                                                   //parent entity name
                     array('EvalSession'),                                      //children
                     $connection );                                             //database connection
        $field_map = array(
            'Shift_id' => 'Shift_id',
            'Run_id' => 'Run_id',
            'Assess_id' => 'Assesment_id',
            'Student_id' => 'Student_id',
            'PerformedBy' => 'PerformedBy',            
            'SkillCode' => 'SkillCode',
            'SkillModifier' => 'SkillModifier',
            'SubjectType_id' => 'SubjectType_id'
            );
        $this->set_field_map( $field_map );
    }//constructor FISDAP_BLS_Skill
    

    /**
     * If there were multiples on the PDA, create multiple
     * submissions. (attaching any evals to the first entry
     * alone)
     *
     */ 
    function postprocess_pda_submission(){
        if ( intval($this->get_field('SkillModifier')) == 0 ) {
            $this->set_field('SkillModifier', BLS_MOD_ORAL_PHARYNGEAL_AIRWAY);
        }//if

        $entity_name = $this->my_name();
        $num_multiples = $this->pda_postdata[$entity_name.$this->pda_id.'Multiple'];
        if ( $num_multiples && $num_multiples > 1 ) {
            // if there are more BLS_Skills to insert, re-write the pda postdata
            // with a decremented "Multiple" field (remembering to set the
            // "Multiple" field on this object back to the original value
            // when we're done)            
            $original_pda_id = $this->pda_id;
            $this->pda_id = $this->get_next_pda_id();
            $this->to_pda_postdata();
            $this->pda_postdata[$entity_name.$this->pda_id.'Multiple'] = $num_multiples-1;
            $this->pda_id = $original_pda_id;
        }//if

        parent::postprocess_pda_submission();
    }//postprocess_pda_submission    
}//class FISDAP_BLS_Skill

?>
