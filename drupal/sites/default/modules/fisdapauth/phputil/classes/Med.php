<?php 

require_once('phputil/med_name_constants.php');
require_once('phputil/med_route_constants.php');

/**
 * This class models a FISDAP Med entry
 * 
 */
class FISDAP_Med extends DataEntryDbObj {    
    /** 
     * Create a new Med with the specified id and the specified db link
     */
    function FISDAP_Med( $id, $connection ) {
        $this->DbObj(
            'MedData',              //table name
            $id,                    //id
            'Med_id',               //id field
            'Shift',                //parent entity name
            array('EvalSession'),   //children
            $connection             //database connection
        );

        $field_map = array( 
            'Shift_id' => 'Shift_id',
            'Student_id' => 'Student_id',
            'Run_id' => 'Run_id',
            'PerformedBy' => 'PerformedBy',
            'MedType' => 'Medication',
            'Route' => 'Route',
            'Dose' => 'Dose',
            'SubjectType_id' => 'SubjectType_id'
        );
        $this->set_field_map( $field_map );
    }//constructor FISDAP_Med

    /**
     * If there were multiples on the PDA, create multiple
     * submissions. (attaching any evals to only one of the 
     * med entries)
     *
     */ 
    function postprocess_pda_submission(){
        // correct 0-based medication key (from old syncs)
        if ( intval($this->get_field('Medication')) == 0 ) {
            $this->set_field('Medication', MED_ADENOSINE);
        }//if

        // correct 0-based med route key (from old syncs)
        if ( intval($this->get_field('Route')) == 0 ) {
            $this->set_field('Route', MED_ROUTE_IV_BOLUS);
        }//if

        $entity_name = $this->my_name();
        $num_multiples = $this->pda_postdata[
            $entity_name.$this->pda_id.'Multiple'
        ];
        if ( $num_multiples > 1 ) {
            // if there are more Meds to insert, re-write the pda
            // postdata with a decremented "Multiple" field (remembering
            // to set the "Multiple" field on this object back to the
            // original value when we're done)

            // we need to give the multiple a new id, so we don't delete
            // it after postprocessing
            $original_pda_id = $this->pda_id;
            $this->pda_id = $this->get_next_pda_id();
            $this->to_pda_postdata();
            $this->pda_postdata[
                $entity_name.$this->pda_id.'Multiple'
            ] = $num_multiples-1;
            $this->pda_id = $original_pda_id;
        }//if

        parent::postprocess_pda_submission();
    }//postprocess_pda_submission
}//class FISDAP_Med
?>
