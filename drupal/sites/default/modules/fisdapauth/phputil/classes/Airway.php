<?php 

require_once('phputil/als_air_type_constants.php');

/**
 * This class models a FISDAP Airway entry
 */
class FISDAP_Airway extends DataEntryDbObj {    

    /** 
     * Create a new Airway with the specified id and the specified db
     * link
     */
    function FISDAP_Airway( $id, $connection ) {
        $this->DbObj(
            'ALSAirwayData',                //table name
            $id,                            //id
            'Airway_id',                    //id field
            'Shift',                        //parent entity name
            array('EvalSession'),           //children
            $connection                     //database connection
        );

        $field_map = array( 
            'Shift_id' => 'Shift_id',
            'Run_id' => 'Run_id',
            'Student_id' => 'Student_id',
            'PerformedBy' => 'PerformedBy',
            'NumAttempts' => 'NumAttempts',
            'Success' => 'Success',
            'Type' => 'Type',
            'ETSize' => 'ETSize',
            'Time' => 'ETTime',
            'SubjectType_id' => 'SubjectType_id'
        );
        $this->set_field_map( $field_map );
    }//constructor FISDAP_Airway


    function postprocess_pda_submission(){
        if ( intval($this->get_field('Type')) == 0 ) {
            $this->set_field('Type', ALS_AIR_ENDOTRACHEAL_TUBE);
        }//if

        parent::postprocess_pda_submission();
    }//postprocess_pda_submission
}//class FISDAP_Airway

?>
