<?php

/**
 * @package CommonInclude
 * @author Warren Jacobson
 */

require_once('phputil/classes/browser_action.inc');
require_once('phputil/classes/common_utilities.inc');
require_once('phputil/classes/FisdapErrorHandler.inc');

// todo this should really be refactored to use static methods.
$common_dispatch_action = array('continue',null);
$common_dispatch_ajax_captured_output = array();
$common_dispatch_ajax_output = array();
$common_dispatch_ajax_state = null; 
$common_dispatch_interrupt = false;

/**
 * Send a browser action back to the web client via ajax.
 * @param BrowserAction $action The browser action.
 */
function send_browser_action($action)  {

	global $common_dispatch_ajax_output; 

	$common_dispatch_ajax_output[] = $action;
}

/**
 * Send output back to the web client via ajax
 * @deprecated 2/13/2009 in favor of send_browser_action() when called from 
 * outside this file.
 * @see send_browser_action()
 * @param string $output_type Either 'div', 'alert', 'js' or 'data'
 * @param string $div_id Either a div id or a JavaScript array index value
 * @param string $output The appropriate output depending upon output type
 */
function echo_ajax($output_type,$div_id,$output) {

	// Create an action for the browser.
	$action = null;
	switch($output_type) {
	case 'alert': {
		$action = new AlertBrowserAction($output);
		break;
	}
case 'confirm': {
	$action = new ConfirmBrowserAction($div_id, $output);
	break;
}
case 'data': {
	$action = new DataBrowserAction($div_id, $output);
	break;
}
case 'div': {
	$action = new InnerHtmlBrowserAction($div_id, $output);
	break;
}
case 'error': {
	$action = new ErrorBrowserAction($output);
	break;
}
case 'js': {
	$action = new JavascriptBrowserAction($output);
	break;
}
default: break;
	}

	if (is_null($action)) {
		common_utilities::displayFunctionError("echo_ajax({$output_type}, {$div_id}, {$output})" . ' is not a valid browser action.');
		return;
	}

	send_browser_action($action);
}

/**
 * Start capturing output to send back to the web client via ajax
 *
 * Output is captured via PHP's 'echo' statement or HTML mode via PHP's output 
 * buffering. Calls to start_ajax() and stop_ajax() may be nested.
 */

function start_ajax() {

	global $common_dispatch_ajax_captured_output;

	// Capture whatever output might be in the buffer already and shut down the buffer

	$output = ob_get_contents(); 
	if ($output != false) ob_end_clean();

	// Append the captured output to the last element in our array

	if ($output != false) {
		$idx = (count($common_dispatch_ajax_captured_output) - 1);
		$common_dispatch_ajax_captured_output[$idx] .= $output;
	}

	// Add a null placeholder to the end of our array
	$common_dispatch_ajax_captured_output[] = null;

	// Turn on output buffering
	ob_start(); // Turn on output buffering
}

/**
 * Stop capturing output to send back to the web client via ajax
 * @param string $output_type Either 'div', 'alert', 'js', or 'data'
 * @param string $div_id Either a div id or a JavaScript array index value
 * null common_dispatch_set_interrupt ()
 * Causes the dispatcher to stop dispatching to any additional registered 
 * routines listening for a specific dispatch_id. 
 */

function stop_ajax($output_type,$div_id) {

	global $common_dispatch_ajax_captured_output;

	// Validate the parameters

	$is_valid = true;
	$valid_output_types = array('div','alert','js','data','error');

	if (!in_array($output_type, $valid_output_types)) {
		common_utilities::displayFunctionError('"' . $output_type . '"' . ' is not a valid output_type');
		$is_valid = false;
	}

	if ($output_type == 'div' or $output_type == 'data') {
		if ($div_id == null or is_string($div_id) == false) {
			common_utilities::displayFunctionError('Invalid div_id');
			$is_valid = false;
		}
	}

	if (($output_type != 'div' and $output_type != 'data') and $div_id != null) {
		common_utilities::displayFunctionError('Expecting div_id to be null for output_type: "' . $output_type . '"');
		$is_valid = false;
	}

	// Capture the output

	$output = ob_get_contents(); 
	if ($output != false) ob_end_clean();

	// Grab whatever additional output that might've already been captured

	$additional_output = array_pop($common_dispatch_ajax_captured_output);
	$output .= $additional_output;

	// Send the otuput back to the web client via ajax

	if ($output != null or ($output == null and ($output_type != 'alert' and $output_type != 'js'))) {
		echo_ajax($output_type, $div_id, $output); // Send the output back to the web client via ajax
	}

	// Turn on output buffering

	ob_start(); // Turn on output buffering

}

/**
 * Set a flag to let the dispatcher know to quit processing listeners
 */

function common_dispatch_set_interrupt() {

	global $common_dispatch_interrupt; 

	$common_dispatch_interrupt = true;
}

/**
 * Set a flag to let the dispatcher know what to do when done dispatching
 * @param string $action Either 'continue', 'stop', or 'redirect'. Defaults to 'continue'.
 * @param string $redirect_url The URL to redirect to if action is 'redirect'. Defaults to 
 * the URL of the current script
 */

function common_dispatch_set_action($action,$redirect_url = null) {

	global $common_dispatch_action;

	// Validate the input parameters

	$is_valid = true;

	if ($action != 'stop' and $action != 'continue' and $action != 'redirect') {
		common_utilities::displayFunctionError('"' . $action . '"' . ' is not a valid action');
		$is_valid = false;
	}

	if ($action == 'redirect' and !(is_string($redirect_url) == true or $redirect_url == null)) {
		common_utilities::displayFunctionError('Must specify a valid redirect_url');
		$is_valid = false;
	}

	if ($action != 'redirect' and $redirect_url != null) {
		common_utilities::displayFunctionError('Unexpected redirect_url');
		$is_valid = false;
	}

	// Set the action and the redirect URL

	if ($is_valid) {
		if ($action == 'redirect' and $redirect_url == null) $redirect_url = $_SERVER["SCRIPT_NAME"];
		$common_dispatch_action = array($action,$redirect_url);
	}

}

/**
 * Add a key-variable pair to be posted with ajax requests.
 *
 * To stop a previously entered key from posting, set its value to null. 
 * 
 * Values explicitly provided in the linkHandler call will override any values added via this function if they both have the same key.
 * 
 * Remember that values set with this function will be on the client's broswer, so they should be validated in the post as much as any you'd validate any other value.
 *
 * @author Eryn O'Neil
 */
function common_dispatch_set_state_variable($key, $value) {

	global $common_dispatch_ajax_state; 

	$common_dispatch_ajax_state[$key] = $value;
}

/**
 * The dispatcher handles Ajax and other I/O requests from the web client
 *
 * Javascript Functions that interact with the dispatcher:
 *
 * null <b>linkHandler</b> ( string dispatch_id, string scriptname, string parameters )
 * - <var>dispatch_id</var> - The dispatch_id to send to the dispatcher
 * - <var>scriptname</var> - The name of the script to send the dispatch request.
 *   Defaults to the script that generated the page
 * - <var>parameters</var> - An arbitrary list of parameters to be sent to the server
 *   via the $_POST array. In JSON format. (e.g., 'Name: Warren, Age: 45')
 *
 * null <b>dispatchHandler</b> ( string dispatch_id, string scriptname, string 
 * parameters, string options, function respond_before_function, function 
 * respond_after_function )
 * - <var>dispatch_id</var> - The dispatch_id to send to the dispatcher
 * - <var>scriptname</var> - The URL of the server side script to send the dispatch request. 
 *   Defaults to the script that generated the displayed page
 * - <var>parameters</var> - An arbitrary list of parameters to be sent to the server (via 
 *   the $_POST array) in JSON format. (e.g., 'Name: Warren, Age: 45')
 * - <var>options</var> - Reserved for future use. null
 * - <var>respond_before_function - The JavaScript function that dispatchHandler should 
 *   execute before populating the page with any received DIV contents, JavaScript 
 *   snippets, etc. (Note that dispatchHandler will populate dataArray prior to 
 *   invoking this function.) null if not needed
 * - <var>respond_after_function</var> - The JavaScript function that dispatchHandler should 
 *   execute after to populating the page with any received DIV contents, 
 *   JavaScript snippets, etc. null if not needed
 *
 */

class common_dispatch {

	protected $listeners = array(); // An array of registered listeners

	/**
	 * Set the default dispatch ID
	 *
	 * @param string dispatch_id The dispatch id to use if none is found
	 * (e.g., when script is initially launched)
	 */
	public function set_default_dispatch_id($dispatch_id) {

		// Validate the dispatch_id

		if (!is_string($dispatch_id)) {
			common_utilities::displayFunctionError('"dispatch_id" must be a string');
			$is_valid = false;
		}

		// Tuck the dispatch_id into session space where it can be found later

		$_SESSION[SESSION_KEY_PREFIX . 'dispatch_id'] = $dispatch_id;

	}

	/**
	 * Accept a registration from a listening routine
	 * @param string $dispatch_id The dispatch id being listened for
	 * @param string $routine_type Either 'function' or 'code'
	 * @param string $routine If routine type is 'function', a
	 * {@link http://us3.php.net/callback#language.types.callback callback}. If 
	 * routine type is 'code', a string of code, such as
	 * <code>"echo 'Hello, world!';"</code>
	 * @param int $sequence Controls the order of execution of multiple registered
	 * listeners to a single dispatch id
	 * @param string $output_type Either 'div', 'alert', 'js', or 'data'
	 * @param string $div_id Either a div id for output type of 'div', or an 
	 * JS array index value for output type of 'data'
	 */

	public function register($dispatch_id,$routine_type,$routine,$sequence = null,$output_type = null,$div_id = null) {

		// Make sure input paramaters are valid

		$is_valid = true; // Boolean to determine if input paramaters are valid

		// Validate the routine type

		// These two are now functionally equivalent, only allow method in case
		// there's old code to break
		if ($routine_type == 'method') $routine_type = 'function';

		$valid_routine_types = array('function','code');
		if (!in_array($routine_type, $valid_routine_types)) {
			common_utilities::displayFunctionError('Invalid routine_type');
			$is_valid = false;
		}

		// Validate the dispatch_id

		if (!is_string($dispatch_id)) {
			common_utilities::displayFunctionError('dispatch_id must be a string');
			$is_valid = false;
		}

		// Validate the function

		if ($routine_type == 'function') {
			if (!is_callable($routine)) {
				common_utilities::displayFunctionError('Routine ' . var_dump($routine) . 'is not callable');
				$is_valid = false;
			}
		}

		// Validate the code

		if ($routine_type == 'code') {
			if (!is_string($routine)) {
				common_utilities::displayFunctionError('"routine" must be a string for routine_type of "code"');
				$is_valid = false;
			}
		}

		// Validate the sequence

		if (!is_null($sequence)) {
			if (!is_string($sequence)) settype($sequence, 'string');
			if (!ctype_digit($sequence)) {
				common_utilities::displayFunctionError('"sequence" must be a positive integer or null');
				$is_valid = false;
			} else {
				settype($sequence, "integer");
			}
		}

		// Validate the output type and div_id

		if ($output_type != null or $div_id != null) {
			$valid_output_types = array('div', 'alert', 'js', 'data');
			if (!in_array($output_type, $valid_output_types)) {
				common_utilities::displayFunctionError('"' . $output_type . '"' . ' is not a valid output_type');
				$is_valid = false;
			}
			if ($output_type == 'div' or $output_type == 'data') {
				if ($div_id == null or is_string($div_id) == false) {
					common_utilities::displayFunctionError('Invalid div_id');
					$is_valid = false;
				}
			}
			if (($output_type != 'div' and $output_type != 'data') and $div_id != null) {
				common_utilities::displayFunctionError('Expecting div_id to be null for output_type: "' . $output_type . '"');
				$is_valid = false;
			}
		} else {
			$output_type = 'alert'; // Will cause unexpected standard output to be routed to an alert box
		}

		// Tuck this registered listener away into the listener array

		if ($is_valid) {

			$listener['dispatch_id'] = $dispatch_id;
			$listener['routine_type'] = $routine_type;
			$listener['routine'] = $routine;
			$listener['sequence'] = $sequence;
			$listener['output_type'] = $output_type;
			$listener['div_id'] = $div_id;

			$this->listeners[] = $listener;
		}
	}

	/**
	 * Dispatch an I/O request to a listening routine
	 */

	public function dispatch() {

		global $common_dispatch_ajax_output;
		global $common_dispatch_ajax_captured_output;
		global $common_dispatch_interrupt;
		global $common_dispatch_action;
		global $common_dispatch_ajax_state;

		// Initialize the global common_dispatch variables

		$common_dispatch_ajax_output = array(); 
		$common_dispatch_ajax_captured_output = array();
		$common_dispatch_interrupt = false; 
		$common_dispatch_action = array('continue',null);
		$common_dispatch_ajax_state = null;

		// Find a dispatch ID reference in the GET / POST arrays or session space

		// We'll check GET first, POST second, SESSION last
		if (isset($_GET['dispatch_id'])) {
			$dispatch_id = $_GET['dispatch_id'];
		} else if (isset($_POST['dispatch_id'])) {
			$dispatch_id = $_POST['dispatch_id'];
		} else if (isset($_SESSION[SESSION_KEY_PREFIX . 'dispatch_id'])) {
			$dispatch_id = $_SESSION[SESSION_KEY_PREFIX . 'dispatch_id'];
		} else {
			// If there isn't a dispatch_id then this must be a standard page display
			// request. Just leave. LEAVE!
			return true;
		}

		// Determine if this is an ajax request coming from dispatchHandler
		$ajax = (isset($_POST['ajax']) && $_POST['ajax'] == 'dispatchHandler');

		// Build an array of listeners for this dispatch ID sorted by sequence

		$dispatch_to = array(); // An array containing key references and sequences from the listeners array

		foreach ($this->listeners as $key=>$listener) {
			if ($dispatch_id == $listener['dispatch_id']) {
				$dispatch_to[$key] = $listener['sequence'];
			}
		}

		asort($dispatch_to, SORT_NUMERIC); // Sort by the sequence number

		// If this is an Ajax event then establish an 'all ajax responses' array

		if ($ajax == true) {
			$all_ajax_responses = array(); // An array containing the stuff to send back to the web client
		}

		// Iterate through the dispatch_to array

		foreach ($dispatch_to as $key=>$sequence) {

			// Only dispatch if we've not already been interrupted

			if (!$common_dispatch_interrupt) {

				// If this is an Ajax event then turn fire up an ajax session

				if ($ajax == true) {
					start_ajax(); // Start capturing output to send back to the web client via ajax
				}

				// The manner in which we perform the dispatch depends greatly upon the type of routine we're invoking

				// todo better exception handling is imperative.
				$routine_type = $this->listeners[$key]['routine_type'];
				$caught_exception = false;

				try {
					if ($routine_type == 'function') {
						$function_name = $this->listeners[$key]['routine'];
						call_user_func($function_name); // Dispatch to a function
					} else {
						$code = $this->listeners[$key]['routine'];
						$response = eval($code); // Dispatch to a code fragment
						if ($response === false) {
							common_utilities::displayFunctionError('Code did not parse');
						}
					}
				} catch (Exception $e) {
					if ($ajax) {
						$logger = FisdapLogger::get_logger();
						$logger->exception($e);
					} else {
						throw $e;
					}
				}

				// If this is an Ajax event then turn off output buffering and capture the output

				if ($ajax == true) {
					$output_type = $this->listeners[$key]['output_type']; 
					$div_id = $this->listeners[$key]['div_id'];
					stop_ajax($output_type, $div_id); 
				}
			}

		}

		// If a header redirect was requested, we want to do that in advance of sending back ajax output

		if ($common_dispatch_action[0] == 'redirect') {
			header("Location: " . $common_dispatch_action[1]); // Redirect 
		}

		// If this is an Ajax event then package up the Ajax responses and send them back to the client

		if ($ajax == true) {

			// Include the state values for javascript
			if (is_array($common_dispatch_ajax_state)) {
				foreach ($common_dispatch_ajax_state as $key => $value) {
					$js_code = "setAjaxState(\"$key\", \"$value\")";
					send_browser_action(new JavascriptBrowserAction($js_code));
				}
			}

			// Create and send the Ajax output.
			$ajax_output_string = array();
			foreach ($common_dispatch_ajax_output as $action) {
				$ajax_output_string[] = $action->encode_for_browser();
			}

			$ajax_output_string[] = 'complete**!**1';
			echo join('**!!**', $ajax_output_string); 
		} else {
			// If this wasn't an Ajax event, append any state variables to the end of the file
			if (is_array($common_dispatch_ajax_state)) {
				echo "<script type='text/javascript'>\n";
				foreach ($common_dispatch_ajax_state as $key => $value) {
					echo "setAjaxState(\"$key\", \"$value\");\n";
				}
				echo "</script>\n";
			}
		}

		// 'stop' is our safe word

		if ($common_dispatch_action[0] == 'stop') {
			exit; // Halt the execution of the script
		}

	} // End of common_dispatch's dispatch() method

	/**
	 * Create an anchor element (HTML) with a link handler invocation.
	 * @param string $text The HTML text of the anchor.
	 * @param string $dispatch_id The dispatch ID.
	 * @param string|null $script_name The script name or null to use the current script.
	 * @param array|string|null $options A string, an array of key/values, or null (will be 
	 * run through htmlspecialchars).
	 * @return string The anchor element HTML.
	 */
	public static function get_anchor_with_link_handler($dispatch_id, $text, $script_name=null, $options=null) {
		$js = self::get_link_handler_js($dispatch_id, $script_name, $options);

		return '<a href="javascript:;" onclick="' . $js . ';">' . $text . '</a>';
	}

	/**
	 * Create a link handler method call in JavaScript.
	 * @param string $dispatch_id The dispatch ID.
	 * @param string|null $script_name The script name or null to use the current script.
	 * @param array|string|null $options A string, an array of key/values, or null (will be 
	 * run through htmlspecialchars).
	 * @return string The unquoted text "linkHandler(...)".
	 */
	public static function get_link_handler_js($dispatch_id, $script_name=null, $options=null) {
		$dispatch_id_text = "'{$dispatch_id}'";

		if (is_null($script_name)) {
			$script_name_text = 'null';
		} else {
			$script_name_text = "'{$script_name}'";
		}

		if (is_null($options)) {
			$options_text = 'null';
		} else if (is_array($options)) {
			$list = array();
			foreach ($options as $key=>$value) {
				if (is_null($value) || (trim($value) == '')) {
					$list[] = $key;
				} else {
					$value = htmlspecialchars($value, ENT_QUOTES);
					$list[] = "{$key}: '{$value}'";
				}
			}

			$options_text = "{" . join(', ', $list) . "}";
		} else {
			$options_text = "'" . htmlspecialchars($options, ENT_QUOTES) . "'";
		}

		return "linkHandler({$dispatch_id_text},{$script_name_text}" .
			",{$options_text})";
	}
}
?>
