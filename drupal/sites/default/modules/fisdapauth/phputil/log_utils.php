<?php

require_once('phputil/classes/logger/Logger.php');
require_once('phputil/classes/logger/SyslogOutputLogListener.php');

/**
 * 
 */
function &get_default_logger() {
    $logger =& Logger::get_instance();
    $logger->set_level($logger->DEBUG);
    $logger->add_listener(new SyslogOutputLogListener());
    $logger->log(
        'pda config process started by user "'.$_SERVER['PHP_AUTH_USER'].'"',
        $logger->DEBUG
    );

    return $logger;
}//get_default_logger

?>
