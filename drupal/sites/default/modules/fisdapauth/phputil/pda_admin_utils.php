<?php

/** provides constants for the pda server group IDs */
require_once('phputil/avantgo_utils.php');

/** provides common-user */
require_once("phputil/classes/common_user.inc");

/**
 * Return true iff the student with the given id is allowed to access
 * FISDAP Handheld
 */
function get_student_pda_access($student_username) {
	
	$user = new common_user($student_username);

	if( $user->is_valid() && $user->is_student() ) {
		$student = new common_student($user->get_student_id());
        return array(
            'has_synced' => ($student->has_synced_pda()),
            'has_access' => ($student->get_product_access('pda'))
        );
    } 
	else {
        return false;
    }//else
}//get_student_pda_access


/**
 * Connect to the SOAP administration interface
 */
function &get_pda_soap_connection() {
    $soap_admin =& new AvantgoSoapAdmin();
    if ( !$soap_admin 
         || !$soap_admin->is_connected()
         || $soap_admin->get_error() ) {
        die(
            '<h2>Error connecting to FISDAP Handheld</h2>'
            . '<p style="width:60%">'
                . 'There was a problem connecting to the FISDAP Handheld '
                . 'server.  Please try again in a few minutes.  If this '
                . 'problem persists, please contact your FISDAP Support '
                . 'person for assistance.  We apologize for the '
                . 'inconvenience.'
            . '</p>'
        );
    }//if

    return $soap_admin;
}//get_pda_soap_connection


/**
 * Returns the user's pda user id (creating one if desired)
 */
function get_or_create_pda_account(&$soap_admin, 
                                    $user, 
                                    $password, 
                                    $create_if_absent=true) {
    $avantgo_user_id = $soap_admin->get_user_id($user->get_username());
    if ( !$avantgo_user_id && $create_if_absent ) {
        $avantgo_user_id = $soap_admin->create_user( 
            $user->get_username(),  
            $user->get_first_name(), 
            $user->get_last_name(),
            $password,
            '-- this user was created automatically --'
        );
            
        if ( !$avantgo_user_id ) {
            die(
                'We could not create a FISDAP Handheld account for you.  '
                . 'If this problem persists, please contact your FISDAP '
                . 'support person for assistance.'
            );
        }//if
    }//if

    return $avantgo_user_id;
}//get_or_create_pda_account


/**
 *
 */
function set_user_device_group(&$soap_admin, $avantgo_user_id, $device_os) {
    if ( $device_os == 'palm' ) {
        $soap_admin->add_user_to_palm_group($avantgo_user_id);
        if ( $soap_admin->get_error() ) {
            die(
                'Error adding user to the Palm OS group.  If this problem '
                . 'persists, please contact your FISDAP support person.  '
                . 'We apologize for the inconvenience.'
            );
        }//if
    } else if ( $device_os == 'pocketpc' ) {
        $soap_admin->add_user_to_pocketpc_group($avantgo_user_id);
        if ( $soap_admin->get_error() ) {
            die(
                'Error adding user to the PocketPC group.  If this problem '
                . 'persists, please contact your FISDAP support person.  '
                . 'We apologize for the inconvenience.'
            );
        }//if
    }//else if
}//set_user_device_group


/**
 * Update the given user's FISDAP Handheld password to the given string,
 * which is assumed to match his/her FISDAP password.
 */
function synchronize_user_pda_password(&$soap_admin, 
                                        $avantgo_user_id,
                                        $new_password) {
    return $soap_admin->synchronize_user_password(
        $avantgo_user_id,
        $new_password
    );
}//synchronize_user_pda_password


/**
 * Returns the user's device group ID, depending on whether the user is
 * in the palm or pocketpc group on the pda server.  If there was a SOAP
 * error, the user isn't in any groups, or there was some other failure,
 * we return false.
 */
function get_user_device_group(&$soap_admin, $avantgo_user_id) {
    $user_groups = $soap_admin->get_user_groups($avantgo_user_id);
    if ( is_array($user_groups) && count($user_groups) ) {
        foreach ( $user_groups as $group_id ) {
            if ( $group_id == AVANTGO_POCKETPC_GROUP_ID
                 || $group_id == AVANTGO_PALM_OS_GROUP_ID
               ) {
                return $group_id;
            }//if
        }//foreach
    }//if

    return false;
}//get_user_device_group


/**
 *
 */
function set_user_primary_groups(&$soap_admin, $avantgo_user_id) {
    if ( !$soap_admin->add_user_to_primary_group($avantgo_user_id) ) {
        die(
            'We could not add the new account to the FISDAP Handheld '
            . 'data entry group.  If this problem persists, please '
            . 'contact your FISDAP support person for assistance.'
        );
    }//if
}//set_user_primary_groups



/**
 * Returns true iff an instructor in the given program can create a pda
 * account for the student with the given username and password,
 * otherwise returns an array containing the key 'error,' which contains
 * a string with the error message that occurred during validation.
 */
function validate_instructor_pda_config($student_username, 
                                        $student_password,
                                        $instructor_program_id) {
    if ( $student_username ) {
        if ( check_user_password($student_username, $student_password) ) {
            // valid password! make sure they're a student
            $student_info = get_user_info($student_username);
            $is_instructor = $student_info['instructor'];

            if ( !$is_instructor ) {
                // student! check the program
                $student_program = $student_info['Program_id'];
                if ( $student_program == $instructor_program_id ) {
                    // same program... now check the user's pda access
                    if ( $student_info['PDAAccess'] ) {
                        // pda access! the user can be configured
                        return true;
                    } else {
                        // no pda access! 
                        return array(
                            'error'   => 'This student has not upgraded to '
                                          . 'FISDAP Handheld'
                        );
                    }//else
                } else {
                    // wrong program!
                    return array(
                        'error'   => 'This student is not in your program'
                    );
                }//else
            } else {
                // wrong account type!
                return array(
                    'error'   => 'This user is an instructor'
                );
            }//else
        } else {
            // invalid password!
            return array(
                'error'   => 'You submitted the wrong username and/or '
                              . 'password for this student'
            );
        }//else
    } else {
        // no username was submitted
        return array(
            'error'   => 'The student username cannot be empty.'
        );
    }//else
}//validate_instructor_pda_config


/**
 * Return information on the status of a given student's PDA account
 */
function check_student_pda_status(&$soap_admin, $username) {
    $status = array();

    // check the info stored in FISDAP
    $user_info = get_user_info($username);
    if ( !$user_info || !isset($user_info['Student_id']) ) {
        return false;
    }//if

    $student_id = $user_info['Student_id'];
    $pda_access = get_student_pda_access($username);
    if ( !$pda_access ) {
        return false;
    }//if

    $status['has_synced'] = $pda_access['has_synced'];
    $status['has_access'] = $pda_access['has_access'];

    // check the PDA server
    $student_obj =& FISDAPUser::find_by_username(
        strtolower($username)
    );
    if ( !$student_obj ) {
        return false;
    }//if
    $pda_account_id = get_or_create_pda_account(
        $soap_admin,
        $student_obj,
        '',   // empty password, as we're not creating an account
        false // don't create a new account if none is found
    );

    $status['account_id'] = $pda_account_id;
    if ( $pda_account_id ) {
        $status['device_group'] = get_user_device_group(
            $soap_admin,
            $pda_account_id
        );
    } else {
        $status['device_group'] = 0;
    }//else

    return $status;
}//check_student_pda_status


/**
 * Generates a link to configure the M-Business Client to connect to
 * FISDAP
 * 
 * @param string $path_to_root the relative path from the pwd to the
 *        FISDAP root (defaults to '../')
 */
function render_mbiz_config_link($path_to_root='../', 
                                 $for_student=true,
                                 $username='',
                                 $password='') {
    if ( !$for_student ) {
        $mal_file_suffix = '?'
            . 'username=' . urlencode(htmlentities($username))
            . '&password=' . urlencode(htmlentities($password));
    } else {
        $mal_file_suffix = '';
    }//else

    $pda_config_url =  $path_to_root 
        . 'dl_scripts/pda_config.mal.php' 
        . $mal_file_suffix;
    ?>
        <p> 
            M-Business Anywhere must be configured to connect to FISDAP
            Handheld.  Depending on the speed of your Internet
            connection, as well as the speed of your computer, it may
            take a few moments to start the configuration process.  Once
            the configuration window appears, follow the on-screen
            instructions to configure M-Business Anywhere.  
        </p> 

        <p>
            If you are asked whether you want to open or save the
            configuration file, you should choose to open it.
        </p>
        
        <p> 
            <a href='<?php echo $pda_config_url; ?>'>Configure 
            M-Business Anywhere</a> to connect to FISDAP Handheld.
        </p> 
    <?php
}//render_mbiz_config_link


/**
 * Generates a set of download links for the pda software
 */
function render_pda_download_options($for_student=true, $show_device=false) { 
    // these are the download link URLs for the client software 
    $pocketpc_client_url = get_mbiz_pocketpc_download_link();
    $palm_os_client_url  = get_mbiz_palm_download_link();
    ?>
    <p> 
        FISDAP Handheld relies upon third-party software called 
        M-Business Anywhere.  This software provides a way to 
        download Web content to 
        <?php echo ($for_student) ? 'your' : 'the student\'s'; ?> 
        PDA for offline use.  Each time 
        <?php echo ($for_student) ? 'your' : 'the student\'s'; ?>
        PDA is synced, the most recent changes will be saved to 
        <?php echo ($for_student) ? 'your' : 'the student\'s'; ?>
        FISDAP account.  
    </p> 
    
    <p> 
        We recommend that you save the installation program to your hard
        drive, and that you make sure you have downloaded the entire
        file by checking the file details against those listed below.
        When you have successfully downloaded the file, run it (usually
        by double-clicking the file's icon in Windows Explorer) to
        install M-Business Anywhere on your PC.  
    </p> 

    <p> 
        <a id='pocketpc_download_software_link' 
           href='<?php echo $pocketpc_client_url;?>'
           <?php
               if ( $show_device && $show_device != 'pocketpc' ) {
                   echo 'style="display:none;"';
               }//if
           ?>>Download</a> 
        <a id='palm_os_download_software_link' 
           href='<?php echo $palm_os_client_url;?>'
           <?php
               if ( $show_device && $show_device != 'palm' ) {
                   echo 'style="display:none;"';
               }//if
           ?>>Download</a> 
        and install M-Business Anywhere on your PC.  
    </p> 

    <span id='software_download_details'> 
        File details: 
        <span id='pocketpc_software_download_details' 
              <?php
                  if ( $show_device && $show_device != 'pocketpc' ) {
                      echo 'style="display:none;"';
                  }//if
              ?>> 
            <?php echo get_mbiz_pocketpc_download_details(); ?>
        </span> 
        <span id='palm_os_software_download_details'
              <?php
                  if ( $show_device && $show_device != 'palm' ) {
                      echo 'style="display:none;"';
                  }//if
              ?>> 
            <?php echo get_mbiz_palm_download_details(); ?>
        </span> 
    </span> 

    <?php
}//render_pda_download_options

?>
