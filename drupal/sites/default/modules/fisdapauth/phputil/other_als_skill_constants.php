<?php

/**
 * This file contains constants for the IDs of the "Other ALS Skills"
 */

define('OTHER_CHEST_DECOMPRESSION'       , '25');
define('OTHER_CRICOTHYROTOMY'            ,  '1');
define('OTHER_TWELVE_LEAD_EKG'           ,  '2');
define('OTHER_NG_TUBE_OG_TUBE'           ,  '3');
define('OTHER_FIELD_AMPUTATION'          ,  '4');
define('OTHER_FOLEY_CATHETER'            ,  '5');
define('OTHER_SUTURE'                    ,  '6');
define('OTHER_PERICARDIOCENTESIS'        ,  '7');
define('OTHER_CHEST_TUBE'                ,  '8');
define('OTHER_MAST_APPLIED'              ,  '9');
define('OTHER_MAST_INFLATED'             , '10');
define('OTHER_PULSE_OXIMETRY'            , '11');
define('OTHER_AUTOMATIC_VENTILATOR'      , '12');
define('OTHER_BLOOD_GLUCOSE_LEVEL_CHECK' , '13');
define('OTHER_CENTRAL_IV_LINE'           , '14');
define('OTHER_CAPNOGRAPHY_CAPNOMETRY'    , '15');
define('OTHER_VALSALVAS_MANEUVER'        , '16');
define('OTHER_CAROTID_SINUS_MASSAGE'     , '17');
define('OTHER_BLOOD_DRAW'                , '18');
define('OTHER_AUTOPSY'                   , '19');
define('OTHER_IV_REMOVAL'                , '20');
define('OTHER_OPTHALMOSCOPE'             , '21');
define('OTHER_OTOSCOPE'                  , '22');
define('OTHER_ENDO_STERILE_SUCTIONING'   , '23');
define('OTHER_CPAP'                      , '24');

?>
