<?php

/**
* settings_functions.php - functions to get and set session variables
*/

/**
* FUNCTION set_user_setting - Set the given setting to the given value for the given user
*
* @param $settingName the name of the setting to set
* @param $settingValue the value to set the setting to
* @param $userid the user auth id of the user to set the setting for
*/

function set_user_setting($settingName,$settingValue,$userid) {

	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();
	$checkSel = "select SettingValue from UserSettings WHERE UserAuth_id=$userid AND SettingName='$settingName'";
	error_log($checkSel);
	$checkRes = mysql_query($checkSel,$dbConnect);
	$checCount = mysql_num_rows($checkRes);
	if ($checCount > 0) {
		$checVal = mysql_result($checkRes,0,"SettingValue"); $checkVal = unserialize($checVal);
		$query = "UPDATE UserSettings";
	}
	else {
		$query = "INSERT INTO UserSettings";
	}
	$settingValue = serialize($settingValue);
	if ($checCount > 0) {
		$query = $query . " SET SettingValue='" . addslashes($settingValue) . "' WHERE UserAuth_id=$userid AND SettingName='$settingName' LIMIT 1";
	}
	else {
		$query = $query . " SET SettingValue='" . addslashes($settingValue) . "',SettingName='$settingName', UserAuth_id=$userid";
	}
	if ($checCount > 0) {
		if ($checVal != $settingValue) {
			return mysql_query($query,$dbConnect); // Exit early!
		}
		else {
			return true; // Exit early!
		}
	}
	else {
		return mysql_query($query,$dbConnect); // Exit early!
	}

}

/**
* FUNCTION get_user_setting - Get a particular setting for a particular user
*
* @param $userid the idx from the UserAuthData table for the user 
* @param $settingName the name of the setting
*/

function get_user_setting($userid,$settingName) {

	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();
	$select = "SELECT * FROM UserSettings WHERE UserAuth_id=$userid AND SettingName='$settingName'";
	error_log($select);
	$result = mysql_query($select,$dbConnect);
	$count = mysql_num_rows($result);
	if ($count==1) {
		$serialized_return_result = mysql_result($result,0,"SettingValue");
		$return_result = unserialize($serialized_return_result);
		if (serialize($return_result)!==$serialized_return_result) {
			$return_result = $serialized_return_result;
		}
		return $return_result;
	}
	else {
		return false;
	}

}

/**
* FUNCTION is_user_setting - Determine whether a particular user setting exists or not
*
* @param $userid the idx from the UserAuthData table for the user 
* @param $settingName the name of the setting
*/

function is_user_setting($userid,$settingName) {

	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();
	$select = "SELECT * FROM UserSettings WHERE UserAuth_id=$userid AND SettingName='$settingName'";
	error_log($select);
	$result = mysql_query($select,$dbConnect);
	$count = mysql_num_rows($result);
	if ($count == 1) {
		$isset = true;
	}
	else {
		$isset = false;
	}

	return $isset;

}

/**
* FUNCTION delete_user_setting - Delete a user setting
*
* @param $userid the idx from the UserAuthData table for the user 
* @param $settingName the name of the setting
*/

function delete_user_setting($userid,$settingName) {

	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();
	$delete_statement = "DELETE FROM UserSettings WHERE UserAuth_id=$userid AND SettingName='$settingName' limit 1";
	error_log($delete_statement);
	$result = mysql_query($delete_statement,$dbConnect);

	return;

}

/**
* FUNCTION get_all_user_settings - Get all of the user settings that have been set for a given user.
*
* @param $userid the idx from the UserAuthData table for the user
*/

function get_all_user_settings($userid) {

	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();
	
	$ret_array = array();
	
	$select = "SELECT * FROM UserSettings WHERE UserAuth_id=$userid";
	$result = mysql_query($select,$dbConnect);
	$count = mysql_num_rows($result);
	for ($i=0; $i < $count; $i++) {
		$tmpname = mysql_result($result,$i,"SettingName");
		$tmpvalue = mysql_result($result,$i,"SettingValue");
		$ret_array[$i] = array($tmpname,$tmpvalue);
	}

	return $ret_array;

}

?>
