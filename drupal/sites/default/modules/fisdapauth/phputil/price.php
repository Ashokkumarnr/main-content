<?php
//------------------------------------------------------------------------
//                                                                      --
//      Copyright (C) 1996-2006.  This is an unpublished work of        --
//                       Headwaters Software, Inc.                      --
//                          ALL RIGHTS RESERVED                         --
//      This program is a trade secret of Headwaters Software, Inc.     --
//      and it is not to be copied, distributed, reproduced, published, --
//      or adapted without prior authorization                          --
//      of Headwaters Software, Inc.                                    --
//                                                                      --
//------------------------------------------------------------------------

require_once("classes/common_user.inc");
require_once("dbconnect.html");

//Define our prices here and here alone
define('BLS_TRACKING', 15);
define('ALS_TRACKING', 55);
define('BLS_SCHEDULER', 15);
define('ALS_SCHEDULER', 40);
define('BLS_TESTING', 20);
define('ALS_TESTING', 20);
define('BLS_REVIEW', 30);
define('ALS_REVIEW', 30);
define('BLS_MOODLE', 0);
define('ALS_MOODLE', 0);
define('ALS_PDA', 30);
define('PRECEPTOR_TRAINING', 25); 
define('BUNDLE_DISCOUNT', 15); //used when als tracking and scheduler are both ordered

//
//  Get the discount percentage.  This is the percentage that 
//  gets taken off the regular price.
//

function GetPercentOff( $Program_id, $AccountType, $ProductCode )
{	global $dbConnect;

	$PercentOff = 0.0;
	$CurMonth = Date('m');
	$CurYear = Date('Y');
	$CurDay = Date('d');

	$CurDate = $CurYear . "-" . $CurMonth . "-" . $CurDay;

	$query = "SELECT max(PercentOff) ".
		"AS PercentOff ".
		"FROM PriceDiscount ".
		"WHERE Program_id=$Program_id ".
		"AND (Type='$AccountType' OR Type='All') ".
		"AND (Configuration & $ProductCode) ".
		"AND StartDate <='$CurDate' ".
		"AND EndDate >= '$CurDate'";

	$result = mysql_query( $query, $dbConnect);

	if( $result > 0)
	{	$count = mysql_num_rows($result);
		if( $count > 0)
		{
			$PercentOff = mysql_result($result, 0, "PercentOff");
			if( $PercentOff < 0.0 || $PercentOff > 100.0 || $PercentOff == NULL)
			{	$PercentOff = 0.0;
			}
		}
	}	
	return  $PercentOff;
}



//
// 	This Function will translate an account type to the price and 
//	Display name for that account type.
//


function GetPriceAndDisplayName( $Program_id, $AccountType, $Configuration, 
		&$Price, &$DisplayAccountType)
{
	$Price = 0.0;
	$DisplayAccountType = "FISDAP";
	$TrackingAccess = 0;
	$SchedulerAccess = 0;
	$PDAAccess = 0;
	$TestingAccess = 0;
	$PrepAccess = 0;
	$PreceptorAccess = 0;

	if ($Configuration & 1) {
		$TrackingAccess = 1;
		$TrackingMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 1)/100);
	}
	if ($Configuration & 2) {
		$SchedulerAccess = 1;
		$SchedulerMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 2)/100);
	}
	if ($Configuration & 4) {
		$PDAAccess = 1;
		$PDAMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 4)/100);
	}
	if ($Configuration & 8) {
		$TestingAccess = 1;
		$TestingMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 8)/100);
	}
	if ($Configuration & 16) {
		$PrepAccess = 1;
		$PrepMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 16)/100);
	}
	if ($Configuration & 64) {
		$PreceptorAccess = 1;
		$PreceptorMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 64)/100);
	}

	switch ($AccountType)
	{
		case 'paramedic':
			$DisplayAccountType = "FISDAP ALS (Paramedic)";
		case 'emt-i':
			$Price = 0;
			if( $AccountType == 'emt-i')
			{	$DisplayAccountType = "FISDAP ALS (EMT-I)";
			}
			if( $TrackingAccess == 1 )
			{	$Price += ALS_TRACKING * $TrackingMultiplier;
			}
			if ( $TrackingAccess == 1 && $SchedulerAccess == 1)
			{	$Price += (ALS_SCHEDULER - BUNDLE_DISCOUNT) * $SchedulerMultiplier;
			}
			if ( $TrackingAccess == 0 && $SchedulerAccess == 1)
			{	$Price += ALS_SCHEDULER * $SchedulerMultiplier;
			}
			if( $PDAAccess == 1)
			{	$Price += ALS_PDA * $PDAMultiplier;
			}
			if( $TestingAccess == 1) {
				$Price += ALS_TESTING * $TestingMultiplier;
			}
			if( $PrepAccess == 1) {
				$Price += ALS_REVIEW * $PrepMultiplier;
			}
			break;

		case 'emt-b':
			$Price = 0;
			if( $TrackingAccess == 1)
			{   $Price += BLS_TRACKING * $TrackingMultiplier;
			}
			if( $SchedulerAccess == 1)
			{	$Price += BLS_SCHEDULER * $SchedulerMultiplier;		
			}
			if( $TestingAccess == 1)
			{	$Price += BLS_TESTING * $TestingMultiplier;		
			}
			if( $PrepAccess == 1) {
				$Price += BLS_REVIEW * $PrepMultiplier;
			}
			break;
		case 'instructor':
			$Price = 0;
			if ($AccountType == 'instructor') 
			{	$DisplayAccountType = 'FISDAP Instructor';
			}
			if ($PreceptorAccess == 1)
			{	$Price += PRECEPTOR_TRAINING * $PreceptorMultiplier;
			}
			break;
	}
}

/**
 * FUNCTION get_access_by_configuration - Returns true or false for access to a requested product
 */

function get_access_by_configuration($Configuration, $requested_product) {

	/**
	 * Set up the product array linking products to their configuration code
	 */
	$product_array['tracking'] = 1;
	$product_array['scheduler'] = 2;
	$product_array['pda'] = 4;
	$product_array['testing'] = 8;
	$product_array['prep'] = 16;
	$product_array['classroom'] = 32;
	$product_array['preceptortraining'] = 64;

	/**
	 * Check to make sure the given argument is valid.
	 */
	if (!in_array($requested_product,array_keys($product_array))) {
		$access_granted = false;  // The requested product isn't on the list
	}

	/**
	 * Determine if the user has access to the requested product.
	 */
	else {
		if ($Configuration & $product_array[$requested_product]) {
			$access_granted = true;
		}
		else {
			$access_granted = false;
		}
	}

	return $access_granted;
} // End of the 'get_access_by_configuration' function

/**
 * FUNCTION get_product_description - Returns a string describing the product configuration
 */
function get_product_description($Configuration, $format='string') {
	$description = array();

	if (get_access_by_configuration($Configuration, 'tracking')) {
		$description[1] = 'Tracking';
	}
	if (get_access_by_configuration($Configuration, 'pda')) {
		$description[4] = 'PDA';
	}
	if (get_access_by_configuration($Configuration, 'scheduler')) {
		$description[2] = 'Scheduler';
	}
	if (get_access_by_configuration($Configuration, 'testing')) {
		$description[8] = 'Testing';
	}
	if (get_access_by_configuration($Configuration, 'prep')) {
		$description[16] = 'Study Tools';
	}
//	if (get_access_by_configuration($Configuration, 'classroom')) {
//		$description[32] = 'Classroom';
//	}

	if (get_access_by_configuration($Configuration, 'preceptortraining')) {
		$description[64] = 'Clinical Educator Training';
	}

	if ( $format == 'array' ) {
		return $description;
	}

	else if ( $format != 'string' ) {
		return false;
	}

	$count = count($description);
	if ($count>1) {
		end($description);
		$description[key($description)] = 'and '.current($description);
	}

	if ($count==2) {
		return implode(' ', $description);
	}
	else {
		return implode(', ', $description);
	}

} // End of the 'get_product_description' function

/**
 * FUNCTION account_type_display - Returns a suitable string for displaying account type
 */
function account_type_display($AccountType) {
	if ($AccountType == 'paramedic') {
		return 'Paramedic';
	}
	if ($AccountType == 'emt-i') {
		return 'EMT-I';
	}
	if ($AccountType == 'emt-b') {
		return 'EMT-B';
	}
	if ($AccountType == 'All') {
		return 'All';
	}
	if ($AccountType == 'instructor') {
		return 'Instructor';
	}
}


//
// 	This Function will translate a config code to a price, taking into account
//  what products the user already owns.
//
function GetPriceAndDisplayNameByUser($studentObj, $Configuration, &$Price, &$DisplayAccountType)
{
	$Price = 0.0;
	$DisplayAccountType = "FISDAP";
	$AccountType = $studentObj->get_account_type();
	$Program_id = $studentObj->get_program_id();
	if ($AccountType == 'instructor') {
		$PreceptorAccess = $studentObj->get_product_access('preceptortraining');
	}
	else {
		$TrackingAccess = $studentObj->get_product_access("tracking");
		$SchedulerAccess = $studentObj->get_product_access("scheduler");
		$PDAAccess = $studentObj->get_product_access("pda");
		$TestingAccess = $studentObj->get_product_access("testing");
		$PrepAccess = $studentObj->get_product_access("prep");
		//	$classroomAccess = $userObj->get_product_access("classroom");
	}

	if ($Configuration & 1 && !$TrackingAccess) {
		$TrackingAccess = 10;
		$TrackingMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 1)/100);
	}
	if ($Configuration & 2 && !$SchedulerAccess) {
		$SchedulerAccess = 10;
		$SchedulerMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 2)/100);
	}
	if ($Configuration & 4 && !$PDAAccess) {
		$PDAAccess = 10;
		$PDAMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 4)/100);
	}
	if ($Configuration & 8 && !$TestingAccess) {
		$TestingAccess = 10;
		$TestingMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 8)/100);
	}
	if ($Configuration & 16 && !$PrepAccess) {
		$PrepAccess = 10;
		$PrepMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 16)/100);
	}
	if ($Configuration & 64 && !$PreceptorAccess) {
		$PreceptorAccess = 10;
		$PreceptorMultiplier = 1 - (GetPercentOff( $Program_id, $AccountType, 64)/100);
	}

	switch ($AccountType)
	{
		case 'paramedic':
			$DisplayAccountType = "FISDAP ALS (Paramedic)";
		case 'emt-i':
			$Price = 0;
			if( $AccountType == 'emt-i') {	
				$DisplayAccountType = "FISDAP ALS (EMT-I)";
			}
			if( $TrackingAccess === 10 ) {	
				$Price += ALS_TRACKING * $TrackingMultiplier;
			}
			if ($TrackingAccess === 10 && $SchedulerAccess === true) {
				$Price = $Price - GetSchedulerBundleDiscount($Program_id, $AccountType);
			}
			else if ( ($TrackingAccess === 10 && $SchedulerAccess === 10) ||
				 ($TrackingAccess === true && $SchedulerAccess === 10)) {	
				$Price += (ALS_SCHEDULER - BUNDLE_DISCOUNT) * $SchedulerMultiplier;
			}
			if ( !$TrackingAccess && $SchedulerAccess === 10) {	
				$Price += ALS_SCHEDULER * $SchedulerMultiplier;
			}
			if( $PDAAccess === 10) {	
				$Price += ALS_PDA * $PDAMultiplier;
			}
			if( $TestingAccess === 10) {
				$Price += ALS_TESTING * $TestingMultiplier;
			}
			if( $PrepAccess === 10) {
				$Price += ALS_REVIEW * $PrepMultiplier;
			}
			break;

		case 'emt-b':
			$Price = 0;
			if( $TrackingAccess == 10)
			{   $Price += BLS_TRACKING * $TrackingMultiplier;
			}
			if( $SchedulerAccess == 10)
			{	$Price += BLS_SCHEDULER * $SchedulerMultiplier;		
			}
			if( $TestingAccess == 10)
			{	$Price += BLS_TESTING * $TestingMultiplier;		
			}
			if( $PrepAccess == 10) {
				$Price += BLS_REVIEW * $PrepMultiplier;
			}
			break;
		case 'instructor':
			$DisplayAccountType = "FISDAP Instructor";
			$Price = 0;
			if( $PreceptorAccess == 10) {
				$Price += PRECEPTOR_TRAINING * $PreceptorMultiplier;
			}
			break;
	}
}

function show_as_currency($number) {
	$numberStr = strval($number);
	if (!strpos($numberStr, '.')) {
		$currencyStr = $numberStr.".00";
	}
	else {
		$decimalStr = substr($numberStr, strpos($numberStr, '.'));
		while (strlen($decimalStr)<3) {
			$decimalStr .= "0";
			$numberStr .= "0";
		}
		$currencyStr = $numberStr;
	}
	return "$".$currencyStr;
}

function GetSchedulerBundleDiscount($program_id, $acct_type) {
	//Discount = (Regular Bundle Discount) * (Scheduler Multiplier)
	//Scheduler Multiplier = 1 - GetPercentOff/100

	if ($acct_type == "emt-b") {
		return 0;
	}
	else {
		return BUNDLE_DISCOUNT * (1 - (GetPercentOff($program_id, $acct_type, 2)/100));
	}
}
?>
