<?php

require_once('dbconnect.html');

/**
 * Return an assoc array of attribute-value pairs from the ProgramData
 * table, given a program id and an array of column names.
 *
 * @param int the id of the program whose info should be returned
 * @param array an array of attributes (column names) to return 
 * @return array an array af attribute-value pairs from ProgramData
 */
function get_program_info($program_id,$attrs) {
    global $dbConnect;

    //make sure $program_id is numeric
    if ( !is_numeric($program_id) ) {
        return false;
    }//if

    //make sure $attrs entries only contains letters and underscores
    $attrs_regex = "/^[a-z_]+/i";
    foreach ( $attrs as $column ) {
        if ( !preg_match($attrs_regex,$column) ) {
            return false;
        }//if
    }//foreach
 
    $query_columns = implode(',',$attrs);

    $query = 'SELECT '.$query_columns.' FROM ProgramData '.
             'WHERE Program_id="'.$program_id.'"';
    $result = mysql_query($query,$dbConnect);
    if ( !$result || mysql_num_rows($result) != 1 ) return false;

    return mysql_fetch_assoc($result);
}//get_program_info
?>
