<?php

/**
 * Return the path (from the server root) to the contents of the FISDAP
 * directory (containing shift,phputil, . . .).
 *
 * @return string the path to the contents of the FISDAP directory
 */
function get_FISDAP_root() {
    $path = getcwd();
    return substr($path,0,strpos($path,'/FISDAP')+7) . '/' ;
}//get_FISDAP_root

?>
