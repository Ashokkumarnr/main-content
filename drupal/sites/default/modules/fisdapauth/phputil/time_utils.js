/**
 * Return the current time in 24-hr format (as a string)
 */
function get_current_24_hr_time() {
    var DateObj = new Date();
    var Hours = DateObj.getHours();
    var Minutes = DateObj.getMinutes();
    var TimeString;
    if ( Hours < 10)
        TimeString = "0" + Hours.toString();
    else
        TimeString = Hours.toString();

    if ( Minutes < 10)
        TimeString = TimeString + "0" + Minutes.toString();
    else
        TimeString = TimeString + Minutes.toString();

    return TimeString;
}//get_current_24_hr_time
