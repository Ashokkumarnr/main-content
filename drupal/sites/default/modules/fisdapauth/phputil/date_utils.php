<?php

// handy constants for the months
define('JANUARY',    1);
define('FEBRUARY',   2);
define('MARCH',      3);
define('APRIL',      4);
define('MAY',        5);
define('JUNE',       6);
define('JULY',       7);
define('AUGUST',     8);
define('SEPTEMBER',  9);
define('OCTOBER',   10);
define('NOVEMBER',  11);
define('DECEMBER',  12);


/**
 *
 */
function month_short_names() {
    $months = array( 
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    );

    return $months;
}//month_short_names

?>
