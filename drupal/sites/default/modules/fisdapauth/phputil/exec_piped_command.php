<?php

  /**
   * This function replicates php5's microtime behavior, returning
   * a float.
   * 
   * @return float the number of seconds since the Unix epoch, with
   *               milliseconds
   */
 function microtime_float()
 {
    list($usec, $sec) = explode(" ", microtime());
       return ((float)$usec + (float)$sec);
 }//microtime_float

  
  /**
   * This function writes the given output to a temporary file, then runs the
   * shell command '$cat [TEMPFILE] | [COMMAND]', returning its output.
   *
   * Until we upgrade php to a version >= 4.3.0, we need this workaround
   * to do without the proc_open and proc_close functions for setting up
   * bidirectional pipes.
   * 
   * @param string the command to execute (including the full path unless the
   *               command is in the PATH)
   * @param string the output to pipe to the given command's stdin
   * @return string the output from executing the given command
   */
  function exec_piped_command_workaround($command, $output) {
      //write the output to a temp text file
      $textfile = "/var/www/html/FISDAP/temp/eg_eval_results_".md5($REMOTE_USER.microtime_float()).".txt";

      //make sure we can write the file
      if ( is_writable('/var/www/html/FISDAP/temp') ) {
          $file_handle = fopen($textfile,'w+');

          //make sure we can open the file
          if ( $file_handle ) {
              //make sure writing to the file succeeded
              if ( !fwrite($file_handle,$output) ) {
                  die('exec_piped_command_workaround: error writing to file');
              }//if
              
              //close the file handle
              fclose($file_handle);
          } else {
              die('exec_piped_command_workaround: error opening file');
          }//else
      } else {
          die('exec_piped_command_workaround: file is not writable');
      }//else

      //cat the temp text file and pipe that output to the 
      //given prog
      return `cat $textfile | $command`;
  }//exec_piped_command_workaround

?>
