<?php

/**
 * This file defines constants for the IDs of the various Med routes
 */

define('MED_ROUTE_IV_BOLUS'      , '17'); 
define('MED_ROUTE_IM'            ,  '1'); 
define('MED_ROUTE_SQ'            ,  '2'); 
define('MED_ROUTE_SUB_LINGUAL'   ,  '3'); 
define('MED_ROUTE_NEB'           ,  '4'); 
define('MED_ROUTE_ORAL'          ,  '5'); 
define('MED_ROUTE_SIMPLE_MASK'   ,  '6'); 
define('MED_ROUTE_NRB_MASK'      ,  '7'); 
define('MED_ROUTE_NASAL_CANNULA' ,  '8'); 
define('MED_ROUTE_PPV'           ,  '9'); 
define('MED_ROUTE_VENTURI'       , '10');  
define('MED_ROUTE_ET'            , '11');  
define('MED_ROUTE_IO'            , '12');  
define('MED_ROUTE_IV_DRIP'       , '13');  
define('MED_ROUTE_TRANSDERMAL'   , '14');  
define('MED_ROUTE_BVM'           , '15');  
define('MED_ROUTE_OTHER'         , '16');  

?>
