<?php

/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2005.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/

//
//  The grace period is here to deal with PDA syncs where the shift might be 
//  updated as complete before the data has been stored.
//
//  If the shift has been audited we ignore the grace period.
//
//  The Grace period is measured in seconds
//
//
// Note: Complete Values
//	0 Not Complete
//	1 Complete
//	2 Absent
//	3 Absent w/ Permission
//


function GetShiftCompleted( $ShiftId, $dbConnect, $GracePeriod = 0)
{
	$Complete = 1;   
	$Audited = 1;
	$UnixTimeStamp = 0;

	$query = "SELECT Completed, Audited, UNIX_TIMESTAMP( FirstCompleted) as UnixTimeStamp, CompletedFrom FROM ShiftData WHERE Shift_id=$ShiftId";
	$result = mysql_query( $query, $dbConnect);
        $count = mysql_num_rows($result);
	if( $count == 0 && $ShiftId < -1) 
	{	
	    // This case means the shift was created on the PDA and may not be installed yet.
		return 0;
	}
	else if( $count > 0)
       	{	$Complete = mysql_result( $result, 0, "Completed");
		$Audited = mysql_result( $result, 0, "Audited");
		$UnixTimeStamp = mysql_result( $result, 0, "UnixTimeStamp");
		$CompletedFrom = mysql_result( $result, 0, "CompletedFrom");
       	}

	if( $Audited > 0 || $Complete == 0 || $CompletedFrom != 'pda' || $GracePeriod <= 0)
	{	return $Complete;
	}
	else
	{
	  	$CurTimeStamp = time();
		if( abs($CurTimeStamp - $UnixTimeStamp) < $GracePeriod)
		{	return 0;
		}
		else
		{	return $Complete;
		}
	}
}

//
// We set the standard PDA grace period to the following.
// The thinking here is that the only way someone can submit 
// data with this grace peroid is via a PDA sync.
// But once they sync, they no longer see the shift to add 
// more data, so it is OK if this is a longer period of time.
//

$PDAGracePeriod = 3600; 

?>
