<?php
/*
 * iofunctions.inc
 *
 * Functions dealing with retrieving or outputting data
 */


/* Returns an associative array of options passed through GET and/or POST */
function get_register_options() {
	if ($_GET && $_POST) {
		$opt_array = array_merge($_GET, $_POST);
	} else if ($_GET) {
		$opt_array = $_GET;
	} else {
		$opt_array = $_POST;
	}
	return $opt_array;
}

/* 
 * Fetches the value of a specified option from the given array.  If no option is present,
 * uses the provided default instead.
 *
 * @param string $opt_name the name of the option to be retrieved.
 * @param value $default_val the default value to be set if the option is not found in the array.
						May be null.
 * @param array $opt_array the array to search. If null, fetch_opt() will try GET and then POST.
 *
 * @return value the value of the requested option, or null.
 */
function fetch_opt($opt_name, $default_val, $opt_array=null) {
	if ($opt_array == null) {
		$opt_array = get_register_options();
	}

	$retval = $opt_array[$opt_name];
	if (!isset($retval)) {
		$retval = $default_val;
	}
	return $retval;
}

//TODO document
function set_opt($opt_name, $default_val) {
	if (func_num_args() > 2) {
		$opt_array = func_get_arg(2);
	} else {
		$opt_array = get_register_options();
	}

	if (!isset($opt_array[$opt_name])) {
		$opt_array[$opt_name] = $default_val;
	}
	return $opt_array;
}
