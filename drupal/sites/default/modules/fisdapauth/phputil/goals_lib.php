<?php

/**
 * Convenience functions for accessing program goals and
 * computing a student's progress.
 *
 * Note: This file assumes that FISDAP_ROOT/phputil/dbconnect.html
 * has been included by the current script.
 */


/**
 * Print out the goal report in the form "progress / goal (percent_complete)"
 *
 * @param int the progress toward a goal
 * @param int the goal
 */
function print_goal_stat($progress,$goal) {
    echo $progress
        . '/'
        . (($goal)?$goal:'n/a')
        . ' ('
        . floor(($progress/$goal)*100)
        . '%)';
}//print_goal_stat


/**
 * Get the goals for the given program.  If the program doesn't
 * have an entry, use the table's default values.
 *
 * @param  int    a program id
 * @param  string the type of account these goals are 
 *                for (emt-b, emt-i, or paramedic)
 * @return array  the relevant column from the database, as returned by 
 *                mysql_fetch_assoc
 */
function get_program_goals($Program_id,$type='paramedic') {
    global $dbConnect; 

    //check for program-specific goals
    if ( !is_numeric($Program_id) || 
         ($type != 'paramedic' && 
          $type != 'emt-i' && 
          $type != 'emt-b') ) return false;
    $query = "SELECT * FROM JRCPrefsData ".
             "WHERE Program_id='$Program_id' ".
             "AND Type='$type'";
    $result = mysql_query($query,$dbConnect);
    if ( !$result ) return $result;

    return (mysql_num_rows($result)) ? mysql_fetch_assoc($result) : 
                                       get_default_goals();
}//get_program_goals

/**
 * Get the default goals. 
 *
 * @return array the relevant column from the database, as returned by 
 *               mysql_fetch_assoc
 */
function get_default_goals() {
    global $dbConnect;

    $query = "DESC JRCPrefsData";
    $result = mysql_query($query, $dbConnect);
    if ( !$result ) return false;

    $defaults = array();
    while( $row = mysql_fetch_assoc($result) ) {
        $defaults[$row['Field']] = $row['Default'];
    }//while
    return $defaults;
}//get_default_goals


/**
 * Get the given student's progress toward his/her goals.  This returns an
 * associative array whose keys are goal names and whose values are 
 * associatives arrays.  The value arrays have the keys 'observed,' 'performed,'
 * and 'goal,' whose values contain the raw integer data for the given goal.
 *
 * @param int the student id to consider
 * @param string the type of goals to fetch (must be one of: nsc, paramedic, 
 *               emt-i, or emt-b)
 */
function get_student_goal_progress( $student_id, $goal_type ) {
    // set the order of the comma-separated goal values returned by the C binary
    $GOAL_NAME_POS = 0;
    $OBS_NUM_POS   = 1;
    $PERF_NUM_POS  = 2;
    $GOAL_NUM_POS  = 3;
    $ignore_array = array(
	"Team Lead ASAP",
	"Team Lead Emergency",
	"Team Lead Transfer",
	"Team Lead ALS",
	"Team Lead Unconscious",
	"Team Lead Pediatrics");

    // get the output from Mike's C binary
    $output = `/var/www/cgi-bin/CalcGoals $student_id $goal_type`;
    $lines = explode( "\n", $output );

    //find the first line that contains raw data (skipping the totals at
    //the top)
    //these lines should be the only ones which contain the character '='
    for( $i=0 ; !(strpos($lines[$i],'=') === false)  ; $i++ ) {}//for
    $first_goal_index = $i;

    //construct the progress array  by parsing the C binary's output.
    $progress = array();
    $num_lines = count( $lines );
    for( $i=$first_goal_index ; $i < $num_lines ; $i++ ) {
        $line = $lines[$i];
        if ( strpos($line,',') === false ) {
            // skip lines that don't have commas 
            continue;
        }//if

        // extract the data from the current line
        $line_array = explode( ',', $line );
        $goal_name = $line_array[$GOAL_NAME_POS];
        $perf_num  = $line_array[$PERF_NUM_POS];
        $obs_num   = $line_array[$OBS_NUM_POS];
        $goal_num  = $line_array[$GOAL_NUM_POS];
	if(!(in_array($goal_name,$ignore_array)))
	{
            // put the extracted data in the progress array
            $progress[$goal_name] = array();
            $progress[$goal_name]['goal']      = $goal_num;
            $progress[$goal_name]['observed']  = $obs_num;
            $progress[$goal_name]['performed'] = $perf_num;
	}
    }//for

    return $progress;
}//get_student_goal_progress


/**
 * Get the number of successful IVs for the given student.
 *
 * @param  int the ID of a student 
 * @return int the number of successful IVs the student had
 */
function get_iv_success($student_id) {
    global $dbConnect;

    if ( !is_numeric($student_id) ) return false;

    $query = "SELECT COUNT(*) FROM IVData ".
             "WHERE Student_id='$student_id' AND ".
             "Success='1'";
    $result = mysql_query($query, $dbConnect);
    if ( !$result || !(mysql_num_rows($result) == 1) ) return false;

    $row = mysql_fetch_assoc($result);
    return $row['COUNT(*)'];
}//get_iv_success

/**
 * Get the number of successes for the given column in the given table for 
 * the given student.
 *
 * @todo figure out how to validate $success_val
 *
 * @param int    the ID of a student
 * @param string the column to count
 * @param string the table to look in (must have a column named 'Student_id')
 * @param string the value to look for
 */
function get_matching_count($student_id,$column,$table,$success_val) {
    global $dbConnect;

    if ( !is_numeric($student_id) ||
         !preg_match('/[a-zA-Z0-9_]+/',$column.$table) ) return false;

    $query = "SELECT COUNT(*) FROM $table ".
             "WHERE Student_id='$student_id' AND ".
             "$column='$success_val'";
    $result = mysql_query($query, $dbConnect);
    if ( !$result || !(mysql_num_rows($result) == 1) ) return false;

    $row = mysql_fetch_assoc($result);
    return $row['COUNT(*)'];
}//get_matching_count

/**
 * Get the number of entries the given student has in the given table.
 *
 * @param   int    a student ID  
 * @param   string a table with a column named 'Student_id' 
 * @return  int    the number of entries 
 */
function get_raw_count( $student_id, $table ) {
    global $dbConnect;

    if ( !is_numeric($student_id) ||
         !preg_match('/[a-zA-Z0-9_]+/',$table) ) return false;

    $query = "SELECT COUNT(*) FROM $table ".
             "WHERE Student_id='$student_id'";
    $result = mysql_query($query, $dbConnect);
    if ( !$result || !(mysql_num_rows($result) == 1) ) return false;

    $row = mysql_fetch_assoc($result);
    return $row['COUNT(*)'];
}//get_raw_count

?>
