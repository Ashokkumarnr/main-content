<?php

function FindNRScores ($MoodleDBName, $MoodleHeader, $MoodleQuizID, $UnixStartTime, $UnixEndTime) {
// 
//  This function will find information about all the students who have taken a given test.
// Then it will print it out

// set basic test types (per TestTypes table)
	if ($MoodleQuizID == 21 || $MoodleQuizID == 22 || $MoodleQuizID == 14) { $testTypes = "3,4,15,16,17,18,19,20"; }
// set paramedic test types (per TestTypes table)
	else { $testTypes = "1,2,9,10,11,12,13,14"; }
	
	
// Connect to Moodle DB
	$dbConnect = mysql_connect("localhost","root", '', true);
	if ( !$dbConnect )
	{
		echo mysql_errno()
		. ": Cannot connect to database: "
		. mysql_error()
		. "<BR>";
		exit;
	}//if
	
	if ( !mysql_select_db( $MoodleDBName, $dbConnect) )
	{    
		echo mysql_errno()
		. ": Cannot select database: "
		. mysql_error()
		. "<br>";
			exit;
	}//if
	
// Get Moodle UserNames and dates the test was taken
	
	$querystring = "SELECT u.username as 'UserName', FROM_UNIXTIME(a.timefinish,'%c/%e/%Y') as 'time' ".
			       "FROM " . $MoodleHeader . "quiz_attempts a, " . $MoodleHeader . "user u ".
			       "WHERE a.quiz= $MoodleQuizID ".
			       "AND a.userid=u.id ".
			       "AND a.timefinish>=$UnixStartTime ".
				   "AND a.timefinish <=$UnixEndTime";
	
//echo $querystring."<br>";
//exit;
	
	$mysql_result = mysql_query($querystring, $dbConnect);
	$numrows = mysql_num_rows($mysql_result);
	if(  $numrows == 0) {
		echo "No users found in Quiz<br>";
		return;
	}
	
	$dateArray = array();
	for( $i=0; $i<$numrows; $i++) {	
		$TmpStr = mysql_result( $mysql_result, $i, "UserName");
		$Date = mysql_result( $mysql_result, $i, "time");
		if( substr($TmpStr,0,4)=='ngw_') {
			$UserName = substr($TmpStr,4); 
		}
		else {
			$UserName = $TmpStr; 
		}
		
		$dateArray[strtolower($UserName)] = $Date;
	}

	
// Switch to the FISDAP Database
	if ( !mysql_select_db( 'FISDAP', $dbConnect) )
	{     	echo mysql_errno()
			. ": Cannot select database: "
			. mysql_error()
			. "<br>";
			exit;
	}//if
	
// Find the users in the FISDAP db
	$unameString = '"'.implode('", "',array_keys($dateArray)).'"';
	$select = "SELECT S.*, P.ProgramName ".
			  "FROM StudentData S, ProgramData P ".
			  "WHERE S.UserName IN ($unameString) ".
			  "AND S.Program_id = P.Program_id ".
			  "ORDER BY P.ProgramName, S.LastName, S.FirstName";
//echo $uelect;
	$result = mysql_query($select,$dbConnect);
	$numStudents = mysql_num_rows($result);
	if ($numStudents == 0) {
		echo "No students with matching FISDAP accounts.<br>";
		return;
	}

echo "<p class='mediumtext'>$numStudents students</p>";

// start the table
	if ($numStudents>0) {
		echo "<table class='common_table mediumtext'><tr>\n";
		echo "<th align=left>Program</th>\n";
		echo "<th align=left>Student</th>\n";
		echo "<th>Date</th>\n";
		echo "<th>NR Scores?</th>\n";
		echo "</tr>\n";
	}
	
// print out each students' information
	for ($j=0;$j<$numStudents;$j++) {
		$tmpProg = mysql_result( $result, $j, "P.ProgramName");	
		$tmpFName = mysql_result( $result, $j, "S.FirstName");	
		$tmpLName = mysql_result( $result, $j, "S.LastName");
		
		$tmpUsername = strtolower(mysql_result( $result, $j, "S.UserName"));	
		$tmpDate = $dateArray[$tmpUsername];
		
		$tmpID = mysql_result( $result, $j, "S.Student_id");	
		$scoreFlag = "";
		$scoreSelect = "SELECT * FROM TestScores ".
				       "WHERE Student_id = $tmpID ".
				       "AND TestType_id IN ($testTypes) ".
				       "AND (TestScore>-1 OR PassOrFail>-1)";
//echo "$scoreSelect<br>";
		$scoreResult = mysql_query($scoreSelect,$dbConnect);
		if (mysql_num_rows($scoreResult) > 0) { $scoreFlag = "X"; }
		
		if  ($j & 1 == 1) {
			echo "<tr class='odd_row'>\n";
		}
		else {
			echo "<tr>\n";
		}
		echo "<td>$tmpProg</td>\n";
		echo "<td>$tmpLName, $tmpFName</td>\n";
		echo "<td align=center>$tmpDate</td>\n";
		echo "<td align=center>$scoreFlag</td>\n";
		echo "</tr>\n";
	}	

	if ($numStudents>0) {
        echo '</table>';
    }
}
?>
