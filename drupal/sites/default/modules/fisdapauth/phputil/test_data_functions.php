<?php
/**
 * Utilities for handling test schedules, data, etc.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @package default
 * @author Kate Hanson
 * @copyright 1996-2007 Headwaters Software, Inc.
 *
 */

require_once "phputil/classes/common_user.inc";
require_once "phputil/classes/common_date.inc";
require_once "phputil/tbp_moodle_functions.php";
require_once "phputil/classes/model/ScheduledTestModel.inc";

/**
 * We'll be needing the database
 */

$connection = FISDAPDatabaseConnection::get_instance();
$dbConnect = $connection->get_link_resource();

/**
 * FUNCTION print_test_schedule_table - given a program id, print out a nice table
 * with every test event scheduled for that program.  If the given program id is 0 or less,
 * print out all scheduled test events for the entire system
 */

function print_test_schedule_table($Program_id, $startdate, $enddate, $Test_id) {

	global $connection;

	$select = "SELECT * FROM ScheduledTests ";
	$select .= "WHERE StartDate>='$startdate' ";
	$select .= "AND StartDate<='$enddate' ";
	if ($Program_id>0) {
		$select .= "AND Active=1 ";
		$select .= "AND Program_id=$Program_id ";
	}
	if ($Test_id>0) {
		$select .= "AND Test_id=$Test_id ";
	}
	$select .= "ORDER BY StartDate DESC";
	$result = $connection->query($select);

	// If no tests, exit early!!
	if (count($result) < 1) {
		echo "<div class='mediumboldtext'>Your program has not scheduled any tests.</div>\n";
		return;
	}

	/**
	 * If we found some tests, print a pretty table
	 */
	echo "<table class='test_schedule_tbl sortable' id='testSchTable'>\n";
	echo "<tr class='mediumtext'>";
	echo "<th>Date</th>\n";
	echo "<th width=150px>Test</th>\n";
	if ($Program_id==-1) {
		echo "<th>Program</th>\n";
		echo "<th>Active</th>\n";
	}
	echo "<th>Contact</th>\n";
	echo "<th># Students</th>\n";
	echo "<th width=200px>Notes</th>\n";
	if ($Program_id > 0) {
		echo "<th>Details</th>\n";
		echo "<th>Edit</th>\n";
		echo "<th>Delete</th>\n";
		echo "<th>Scores</th>\n";
	}
	echo "<th>Published</th>\n";
	echo "</tr>";

	foreach ($result AS $testEvent) {
		$Test_id = $testEvent['Test_id'];
		$TestName = fetch_test_name($Test_id);
		if ($Program_id==-1) {
			$ProgramName = fetch_program_name($testEvent['Program_id']);
		}

		$Date = $testEvent['StartDate'];
		$Date_year = substr($Date, 0, 4);
		$Date_month = substr($Date, 5, 2);
		$Date_day = substr($Date, 8, 2);
		$Date_mktime = mktime(0, 0, 0, $Date_month, $Date_day, $Date_year);
		$Publish = $testEvent['PublishScores'];

		$testNotes = $testEvent['TestNotes'];
		if ($testNotes == null) {
			$testNotes = '&nbsp;&nbsp;';
		}

		$rowBG = '#FFFFFF';
		if ($Date_mktime>time()) {
			$rowBG = '#DDDDDD';
		}

		echo "<tr style='background-color:$rowBG'>\n";
		echo "<td class='smalltext'>".$Date."</td>";
		echo "<td class='mediumtext'>".$TestName."</td>";
		if ($Program_id==-1) {
			echo "<td class='mediumtext'>".$ProgramName."</td>\n";
			echo "<td class='mediumtext'>".$testEvent['Active']."</td>\n";
		}
		echo "<td class='mediumtext'>".$testEvent['ContactName']."</td>";
		echo "<td class='mediumtext' style='text-align:center'>".count(unserialize($testEvent['ScheduledStudents']))."</td>";
		echo "<td class='mediumtext'>".$testNotes."</td>";

		if ($Program_id > 0) {
			echo "<td><div class='detailsButton mediumtext'>\n";
			echo "<a href='../testing/testDetails.html?test_id=".$testEvent['ScheduledTest_id']."'>\n";
			echo "Details</a></div></td>\n";

			echo "<td><div class='editButton mediumtext'>\n";
			echo "<a href='../testing/testScheduler.html?formid=maintform&scheduledtest_id=".$testEvent['ScheduledTest_id']."'>\n";
			echo "Edit</a></div></td>\n";

			echo "<td><div class='deleteButton mediumtext'>\n";
			echo "<a href='javascript:deleteEvent(".$testEvent['ScheduledTest_id'].", \"".
				$Date."\", \"".
				$TestName."\")'>\n";
			echo "Delete</a></div></td>\n";

			echo "<td style='text-align:center'>\n";
			if ($Date_mktime<mktime()) {
				echo "<div class='scoresButton mediumtext'>\n";
				echo "<a href='testScores.html?test_id=".$testEvent['ScheduledTest_id']."'>\n";
				echo "Scores</a></div>\n";
			} else {
				echo "----";
			}
			echo "</td>\n";
		}

		echo "<td style='text-align:center'>\n";
		if ($Publish==1) {
			echo "<img src='../images/check_icon_sm.png' alt='Yes'>";
		} else {
			echo "&nbsp;&nbsp;";
		}
		echo "</td>\n";
		echo "</tr>\n";
	}
	echo "</table>\n";
	echo "<form name='deleteform' action='$PHP_SELF' method=POST>\n";
	echo "<input type='hidden' name='do_delete' value=0>\n";
	echo "<input type='hidden' name='delete_test_id' value=-1>\n";

}//function

/**
 * FUNCTION fetch_test_name - given the test id (the moodle quiz id), get the test name
 */

function fetch_test_name($Test_id) {
	$connection = FISDAPDatabaseConnection::get_instance();

	$select = "SELECT TestName FROM MoodleTestData WHERE MoodleQuiz_id=$Test_id";
	$result = $connection->query($select);
	$count = count($result);

	if ($count!=1) {
		return false;
	} else {
		return $result[0]['TestName'];
	}
}

/**
 * FUNCTION fetch_bp_name - given the blueprint id, get the blueprint name
 */

function fetch_bp_name($tbp_id) {
	global $connection;

	$select = "SELECT Name ".
		"FROM TestBluePrints ".
		"WHERE tbp_id=$tbp_id";
	$result = $connection->query($select);
	$count = count($result);

	if ($count!=1) {
		return false;
	} else {
		return $result[0]['Name'];
	}
}

/**
 * FUNCTION fetch_full_bp_name - given the blueprint id, get the blueprint name, including project
 */

function fetch_full_bp_name($tbp_id) {
	global $connection;

	$select = "SELECT p.ProjectName, b.Name ".
		"FROM ProjectTable p, TestBluePrints b ".
		"WHERE b.tbp_id=$tbp_id ".
		"AND p.Project_id=b.Project_id";
	$result = $connection->query($select);
	$count = count($result);

	if ($count!=1) {
		return false;
	} else {
		return $result[0]['ProjectName'].": ".$result[0]['Name'];
	}
}

/**
 * FUNCTION fetch_test_blueprint_id - given the test id (the moodle quiz id), get our blueprint id
 */

function fetch_test_blueprint_id($Test_id) {
	$connection = FISDAPDatabaseConnection::get_instance();

	$select = "SELECT Blueprint_id FROM MoodleTestData WHERE MoodleQuiz_id=$Test_id";
	$result = $connection->query($select);
	$count = count($result);

	if ($count!=1) {
		return false;
	} else {
		return $result[0]['Blueprint_id'];
	}
}

/**
 * FUNCTION fetch_test_id - given the blueprint id, get the associated test ids (the moodle quiz ids)
 */

function fetch_associated_tests($tbp_id, $MoodleQuiz_id=-1) {
	global $connection;

	// if no specific test is requested, return associated tests
	if ($MoodleQuiz_id<1) {
		$where_clause = "WHERE T.Blueprint_id=$tbp_id ";
	} else {
		$where_clause = "WHERE T.MoodleQuiz_id=$MoodleQuiz_id ";
	}

	$select = "SELECT * ".
		"FROM MoodleTestData T, TestStatusTable S ".
		$where_clause. 
		"AND T.Active = S.StatusType_id";
	$result = $connection->query($select);
	$count = count($result);

	if ($count<1) {
		return false;
	} else {
		foreach ($result as $row) {
			$tmp_id = $row['MoodleQuiz_id'];
			$keyed_result[$tmp_id] = $row;
		}
		return $keyed_result;
	}
}

/**
 * FUNCTION fetch_test_code - given the test id (the moodle quiz id), get code necessary for the results functions
 */

function fetch_test_code($Test_id) {
	$TestName = fetch_test_name($Test_id);
	$Blueprint_id = fetch_test_blueprint_id($Test_id);
	if ($TestName != false && $Blueprint_id != false) {
		$test_code = $Blueprint_id.":".$Test_id.":".$TestName;
		return $test_code;
	} else {
		return false;
	}
}

/**
 * FUNCTION fetch_scheduled_test_active_status - given a test id, return whether the test is active
 * 0 = the test has been "deleted"
 * 1 = the test is active
 */

function fetch_scheduled_test_active_status($ScheduledTest_id) {
	global $connection;

	$select = "SELECT Active FROM ScheduledTests ".
		"WHERE ScheduledTest_id=$ScheduledTest_id";
	$result = $connection->query($select);
	$count = count($result);

	if ($count!=1) {
		return false;
	} else {
		return $result[0]['Active'];
	}
}


/**
 * FUNCTION fetch_scheduled_test_program - given a test id, return the test's program
 */

function fetch_scheduled_test_program($ScheduledTest_id) {
	global $connection;

	$select = "SELECT Program_id FROM ScheduledTests ".
		"WHERE ScheduledTest_id=$ScheduledTest_id";
	$result = $connection->query($select);
	$count = count($result);

	if ($count!=1) {
		return false;
	} else {
		return $result[0]['Program_id'];
	}
}


/**
 * FUNCTION get_latest_test_date - given a program id, return the most recent test start date
 */

function get_latest_test_date($Program_id) {
	global $connection;

	$select = "SELECT max(StartDate) ".
		"AS 'StartDate' ".
		"FROM ScheduledTests ".
		"WHERE Program_id=$Program_id";
	$result = $connection->query($select);
	$count = count($result);

	//echo $count;
	if ($count!=1) {
		return $select;
	} else {
		return $result[0]['StartDate'];
	}
}

/**
 * FUNCTION fetch_start_date - given a test id, return the test's start date
 */

function fetch_start_date($ScheduledTest_id) {
	global $connection;

	$select = "SELECT StartDate FROM ScheduledTests ".
		"WHERE ScheduledTest_id=$ScheduledTest_id";
	$result = $connection->query($select);
	$count = count($result);

	//echo $select;
	if ($count!=1) {
		return date('Y-m-d');
	} else {
		return $result[0]['StartDate'];
	}
}


/**
 * FUNCTION delete_test_event - given a test id, delete the scheduled test
 */

function delete_test_event($ScheduledTest_id) {
	global $connection;

	$update = "UPDATE ScheduledTests ".
		"SET Active=0 ".
		"WHERE ScheduledTest_id=$ScheduledTest_id ".
		"LIMIT 1";
	$result = $connection->query($update);
}

/**
 * FUNCTION send_new_test_email - given a test id, send an email about the 
 * creation of that test event
 */

function send_test_email($ScheduledTest_id, $userInstID, $email_type) {
	global $connection;

	$thisInst = new common_instructor($userInstID);
	$userEmail = $thisInst->get_emailaddress();
	$userFullName = $thisInst->get_firstname()." ".$thisInst->get_lastname();

	$select = "SELECT * FROM ScheduledTests ".
		"WHERE ScheduledTest_id=$ScheduledTest_id ".
		"LIMIT 1";
	$test_info = $connection->query($select);

	/**
	 * send the email to the user doing the scheduling and the primary contact
	 */
	$to = $userEmail;
	if ($userEmail != $test_info[0]['ContactEmail']) {
		$to .= ', '.$test_info[0]['ContactEmail'];
	}

	/**
	 * the subject line should be appropriate to the type of email sent
	 */
	$subject = "FISDAP Test";
	if ($email_type == "new") {
		$subject = "New FISDAP Test Scheduled";
	}
	if ($email_type == "edit") {
		$subject = "Scheduled FISDAP Test has been edited";
	}
	if ($email_type == "delete") {
		$subject = "Scheduled FISDAP Test has been deleted";
	}

	/**
	 * Get info for the body of the email
	 */
	$ContactName = $test_info[0]['ContactName'];
	$numStudents = count(unserialize($test_info[0]['ScheduledStudents']));
	$Test_id = $test_info[0]['Test_id'];
	$TestName = fetch_test_name($Test_id);
	$StartDate = $test_info[0]['StartDate'];
	$EndDate = $test_info[0]['EndDate'];
	if ($StartDate == $EndDate || $EndDate == "0000-00-00") {
		$datePhrase = "on $StartDate";
	} else {
		$datePhrase = "from $StartDate through $EndDate";
	}
	$dateArray = get_date_array($StartDate, $EndDate);
	$detailsLink = 'http://'.$_SERVER['SERVER_NAME'].get_url_for('testing/testDetails.html?test_id='.$ScheduledTest_id);

	/**
	 * Create the body of the email
	 */

	if ($email_type == "new") {
		$body = "$userFullName has scheduled $numStudents of your students ".
			"to take the $TestName $datePhrase.\n\n".
			"You will need a proctor password to 'unlock' the exam before your students ".
			"can begin.  The proctor password is changed daily at midnight PST.  Please ".
			"make a note of the following password(s):\n\n";

		foreach ($dateArray as $date) {
			$password = get_test_password_for_date($date, $Test_id);
			$body .= "$date - proctor password: $password\n";
		}

		$body .= "\n\nAll passwords are case-sensitive so please be sure to type them ".
			"exactly as they appear.  For the sake of test security, we ask that ".
			"you do NOT give the proctor password to your students. Instead, you ".
			"will need to enter it for each student.\n\n";
		if ($test_info[0]['PublishScores'] == 1) {
			$body .= "The scores for this test have been published. ".
				"After completing the test, these students will be able to ".
				"access their own scores from the Admin section of their ".
				"FISDAP accounts.\n\n";
		} else {
			$body .= "The scores for this test have NOT been published. ".
				"Students will NOT have access to their scores.\n\n";
		}
		$body .= "You may view more detailed information, edit your testing schedule, ".
			"and retrieve student results by clicking on the link below. ".
			"There you will also find a set ".
			"of instructions that can be printed and distributed to your students.\n\n".
			"$detailsLink";
	}

	if ($email_type == "edit") {
		$body = "The $TestName scheduled to be taken by $numStudents of your students ".
			"$datePhrase has been edited by $userFullName. ".
			"To view your revised test information, ".
			"click on the link below. ".
			"Be sure to check your proctor passwords as they may have changed.\n\n".
			"$detailsLink";
	}

	if ($email_type == "delete") {
		$body = "The $TestName scheduled to be taken by $numStudents of your students ".
			"$datePhrase has been removed from the schedule by $userFullName.\n\n".
			"Any student scores associated with this test ".
			"have NOT been deleted.  You can retrieve student ".
			"scores by clicking on the 'Testing Admin' button ".
			"in the 'Admin' section of FISDAP.\n";
	}


	$from = "From: fisdap-robot@fisdap.net";
	mail($to, $subject, $body, $from);

}

/** 
 * FUNCTION - get_date_array - given a startdate and an enddate, returns an array of
 * all the dates in the range (including start and end)
 */

function get_date_array($StartDate, $EndDate) {
	$dateArray[] = $StartDate;
	$whileDate = strtotime($StartDate);
	while ($whileDate<strtotime($EndDate)) {
		$whileDate = strtotime("+1 day", $whileDate);
		$newDate = date('Y-m-d', $whileDate);
		$dateArray[] = $newDate;
	}
	return $dateArray;
}

/**
 * FUNCTION get_all_moodle_databases - returns an array of all the moodle databases and their
 * prefixes
 */

function get_all_moodle_databases() {
	global $connection;

	$select = "SELECT distinct MoodleDatabase, MoodlePrefix FROM MoodleTestData";
	$result = $connection->query($select);
	//echo $select;
	$count = count($result);

	if ($count < 1) {
		//echo $count;
		return false;
	} else {
		return $result;
	}
}

/**
 * Retrieve the name of the Moodle DB.
 * @param int $test_id The test ID.
 * @return array|null ['MoodleDatabase', 'MoodlePrefix'] if found or null if not.
 */
function get_moodle_database($test_id) {
	$connection = FISDAPDatabaseConnection::get_instance();

	$statement = 'SELECT MoodleDatabase, MoodlePrefix FROM MoodleTestData' .
		" WHERE MoodleQuiz_id=$test_id";
	$result = $connection->query($statement);

	if (!$result) {
		return null;
	}
	if (count($result) == 1) {
		return $result[0];
	}
	return null;
}

/**
 * Retrieve REAL Moodle Quiz ID because some quizes in pt_moodle have modifiers or something
 * @param int $test_id The test ID
 * @return int modified test ID
 */
function get_modified_test_id($test_id) {
	$connection = FISDAPDatabaseConnection::get_instance();

	$statement = "SELECT (MoodleQuiz_id - MoodleIDMod) as new_id  FROM MoodleTestData WHERE MoodleQuiz_id=$test_id";
	$result = $connection->query($statement);

	if (!$result) {
		return null;
	} else if (count($result) == 1) {
		return $result[0]['new_id'];
	} else {
		return null;
	}
}

/**
 * FUNCTION scheduled_test_is_published - determines whether the student can see their score for a sceduled test
 */

function scheduled_test_is_published($test_id) {
	global $connection;

	$select = "SELECT PublishScores ".
		"FROM ScheduledTests ".
		"WHERE ScheduledTest_id=$test_id";
	$result = $connection->query($select);
	//echo $select;
	//var_export($result);
	if (count($result) != 1) { 
		return false; 
	}
	if ($result[0]['PublishScores'] == 1) {
		return true;
	}
	return false;
}

/**
 * FUNCTION student_attempt_is_published - determines whether the student can see their score for a given test
 */

function student_attempt_is_published($test_id, $student_id, $date) {
	global $connection;

	/**
	 * See if the exam as a whole is published
	 */
	$select = "SELECT PublishScores FROM MoodleTestData WHERE MoodleQuiz_id=$test_id";
	$result = $connection->query($select);
	//echo $select;
	$count = count($result);

	if ($count!=1) {
		//echo $count;
		return false;
	} else {
		if ($result[0]['PublishScores'] == 1) { 
			return true; 
		}
	}

	/**
	 * If not, see if there is a scheduled exam for this attempt, and if so, if it's published
	 */
	$select = 'SELECT PublishScores '.
		'FROM ScheduledTests '.
		'WHERE (("'.$date.'" = StartDate AND EndDate = "0000-00-00") '.
		'OR ("'.$date.'" >= StartDate AND "'.$date.'" <= EndDate)) '.
		'AND ScheduledStudents LIKE \'%"'.$student_id.'"%\' '.
		'AND Active = 1 '.
		'AND Test_id = '.$test_id;
	$result = $connection->query($select);
	//echo $select;
	//var_export($result);
	if (count($result) == 0) { 
		return false; 
	}
	foreach ($result as $scheduledTest) {
		if ($scheduledTest['PublishScores'] == 0) {
			return false;
		}
	}
	return true;
}

/**
 * FUNCTION getTestParentAr - given a test, returns the appropriate parent trees
 * for the NSC and NR Task Analysis
 */

function getTestParentAr($test_id) {
	if ($test_id != 21 && $test_id != 22) { // Paramedic level objectives
		$parentAr = array('12', '4192.4344');
	} else { // Basic level objectives
		$parentAr = array('5023', '4192.4193');
	}
	return $parentAr;
}

/**
 * FUNCTION print_item_details_per_bp - prints out the topics associated with given items
 * for a particular blueprint
 */

function print_item_details_per_bp($test_id, $sectionAr, $items) {
	global $connection;
	global $dbConnect;

	/**
	 * Figure out which blueprint we care about based on the test,
	 * and put the items in a usable list
	 */ 
	$blueprint_id = fetch_test_blueprint_id($test_id);
	$itemList = implode(", ", $items);

	echo "<div class='page_content'>\n";

	/**
	 * if we're looking at all sections, get the category info for the given blueprint
	 */ 
	if (count($sectionAr)>1) {
		// get the category info for the whole exam
		$cat_all_array = get_category_info($blueprint_id, null, null); 

		// get the category info for the missed items only
		$cat_missed_array = get_category_info($blueprint_id, $itemList, null);

		// Print out the category information
		echo "<div class='section largetext'>Overall</div>";
		echo "<div class='category_info'>\n";
		echo "<span class='mediumboldtext'>Learning Category Analysis</span> ";
		echo common_utilities::get_bubblelink('scoreDetails', 'ItemType', null);
		echo "<ul>\n";
		foreach ($cat_all_array as $category=>$total) {
			$missed = $cat_missed_array[$category];
			$percent = round((($total-$missed)/$total)*100, 2);
			echo "<li>$category: $percent% correct</li>";
		}
		echo "</ul>\n";
		echo "</div>\n";
	} // end cat section if looking at all results


	// Introductory Blurb
	echo "<div class='intro'>\n";
	echo "Based on your performance, FISDAP recommends you study the topics ".
		"listed below.<br>";
	echo "</div>\n";

	// Go through each section requested and print out the details
	foreach ($sectionAr as $section) {
		$sectionName = getSectionName($section, $dbConnect);
		echo "<div class='section largetext'><a name='$sectionName'></a>$sectionName</div>";

		// print out the topics for the missed items
		echo "<ul>\n";
		$topic_array = get_topics_for_items_by_id($itemList, $blueprint_id, $section);
		foreach ($topic_array as $topic) {
			echo "<li>".$topic['Topic']."</li>\n";
		}
		echo "</ul>\n";

		// get category info for the section
		$cat_all_array = get_category_info($blueprint_id, null, $section); 

		// get the category info for the missed section items only
		$cat_missed_array = get_category_info($blueprint_id, $itemList, $section); 

		// print out the category information
		echo "<div class='category_info'>\n";
		echo "<span class='mediumboldtext'>Learning Category Analysis</span> ";
		echo common_utilities::get_bubblelink('scoreDetails', 'ItemType', null);
		echo "<ul>\n";
		foreach ($cat_all_array as $category=>$total) {
			$missed = $cat_missed_array[$category];
			$percent = round((($total-$missed)/$total)*100, 2);
			echo "<li>$category: $percent% correct</li>";
		}
		echo "</ul>\n";
		echo "</div>\n";

	} // end section loop

	echo "</div>\n"; // content div

}

function get_category_info($blueprint_id, $items, $section_id) {
	global $connection;

	// get info for selected items, or all items in the exam/section if no items are specified
	if ($items != null) {
		$item_clause = "AND item.Item_id IN ($items) ";
	} else {
		$item_clause = '';
	}

	// get info for selected section, or for the whole exam if no section is specified
	if ($section_id != null) {
		$section_table = ", TestBPTopics topic ";
		$section_clause = "AND item.tbpTopic_id=topic.tbpTopic_id ".
			"AND topic.tbpSect_id=$section_id ";
	} else {
		$section_table = "";
		$section_clause = "";
	}

	$cat_select = "SELECT cat.Name as Category, count(*) as Total ".
		"FROM TestBPColumns cat, TestBPItems item ".$section_table.
		"WHERE item.tbp_id=$blueprint_id ".
		"AND item.tbpColumn_id=cat.tbpColumn_id ".
		$item_clause.
		$section_clause.
		"group by cat.Name ".
		"order by cat.ColumnNumber";
	$results = $connection->query($cat_select);
	$cat_array = array();
	foreach ($results as $row) {
		$cat_array[$row['Category']] = $row['Total'];
	}
	return $cat_array;	
}	

function get_topics_for_items_by_id($items, $blueprint_id, $section_id) {
	global $connection;

	if (!$items) {
		return array();
	}

	$topic_select = "SELECT topic.Name as Topic, count(*) as Count ".
		"FROM TestBPTopics topic, TestBPItems item ".
		"WHERE item.Item_id IN ($items) ".
		"AND item.tbp_id=$blueprint_id ".
		"AND item.tbpTopic_id=topic.tbpTopic_id ".
		"AND topic.tbpSect_id=$section_id ".
		"group by topic.Name ".
		"order by topic.Name";
	$topic_array = $connection->query($topic_select);
	return $topic_array;
}

// THIS FUNCTION IS A STUB; IT IS NOT FUNCTIONAL
/**
 * FUNCTION print_item_details_per_asset - prints out the unique assets linked with given
 * array of items
 */

function print_item_details_per_asset($items) {

	/**
	 * Get asset ids based on item ids
	 */
	foreach ($items as $item_id) {
		$asset_ids[] = getTestItemAsset_id($item_id);
	}

	/**
	 * Get unique asociations based on asset ids
	 */
	$association_ids = array();
	foreach ($asset_ids as $asset) {
		$tmp_association_ids = getAllAssetsInParentAr($asset, 1, $parentAr);
		foreach ($tmp_association_ids as $tmp_assoc) {
			if (!in_array($tmp_assoc, $association_ids)) {
				$association_ids[] = $tmp_assoc;
			}
		}
	}
	sort($association_ids);

	/**
	 * Print out each of the associations
	 */
	foreach ($association_ids as $association) {
		$tmpAsset = getAsset($association, $dbConnect);
		//echo $tmpAsset["Tree_id"]."<br>";
		echo $tmpAsset["AssetName"];
		echo "<br>";
	}
} // function

/**
 * Retrieve all the students in a given program who took a test during a scheduled test time but 
 * WEREN'T scheduled to take the test.
 * @param int $scheduled_test_id The id of the scheduled test.
 * @return array An associative array of student data: student_id=>'LastName, FirstName'
 */
function get_unscheduled_student_array($scheduled_test_id) {
	// get necessary info 
	$scheduled_test = new ScheduledTest($scheduled_test_id);
	$moodle_quiz_id = $scheduled_test->get_test_id();
	$program_id = $scheduled_test->get_program_id();
	$scheduled_students = $scheduled_test->get_scheduled_students();
	$start_date = common_date::create_from_ymd($scheduled_test->get_start_date(), '-');
	$end_date = $scheduled_test->get_end_date();
	if ($end_date == '0000-00-00') {
		$end_date = $start_date;
	} else {
		$end_date = common_date::create_from_ymd($end_date, '-');
	}
	$start_date_timestamp = $start_date->get_time(0, 0, 0);
	$end_date_timestamp = $end_date->get_time(23, 59, 59);
	$tested_students = get_user_array($moodle_quiz_id, $start_date_timestamp, $end_date_timestamp);
	// find out which students tested and add them to the 'unscheduled' list, if appropriate
	$unscheduled_students = array();
	foreach ($tested_students as $tmp_username => $date_taken) {
		$tmp_student = common_user::get_by_username($tmp_username); 
		$tmp_student_id = $tmp_student->get_student_id();
		$tmp_program_id = $tmp_student->get_program_id();
		if (!in_array($tmp_student_id, $scheduled_students) && $tmp_program_id == $program_id) {
			$unscheduled_students[$tmp_student_id] = $tmp_student->get_lastname().", ".$tmp_student->get_firstname(); 
		}
	}   
	return $unscheduled_students;
}

/**
 * Retrieve all the users that took a test.
 * @param int $MoodleQuizID The quiz ID.
 * @param string $UnixStartTime The starting time.
 * @param string $UnixEndTime The ending time.
 * @return array The student data.
 */
function get_user_array($MoodleQuizID, $UnixStartTime, $UnixEndTime) {
	$users = array();

	$db_array = get_moodle_database($MoodleQuizID);
	if (is_null($db_array)) {
		return $users;
	}

	$db_name = $db_array['MoodleDatabase'];
	if (is_null($db_name) || (trim($db_name) == '')) {
		return $users;
	}

	$MoodleHeader = $db_array['MoodlePrefix'];

	$connection = FISDAPDatabaseConnection::get_instance($db_name);

	// make sure we're using the quiz ID used by Moodle, not the modulated one from the FISDAP db
	$real_moodle_quiz_id = get_real_moodle_quiz_id($MoodleQuizID);
	if (is_null($real_moodle_quiz_id)) {
		return $users;
	}

	// Get Moodle UserNames and dates the test was taken
	$statement = "SELECT u.username as 'UserName', FROM_UNIXTIME(a.timefinish, '%c/%e/%Y') as 'time' ".
		"FROM " . $MoodleHeader . "_quiz_attempts a, " . $MoodleHeader . "_user u ".
		"WHERE a.quiz=$real_moodle_quiz_id ".
		"AND a.userid=u.id ".
		"AND a.timefinish>=$UnixStartTime ".
		"AND a.timefinish <=$UnixEndTime";
	$results = $connection->query($statement);
	if (!count($results)) {
		return $users;
	}

	foreach ($results as $result) {
		$users[strtolower($result['UserName'])] = $result['time'];
	}

	return $users;
}

function strip_ngw_prefix($userarray) {
	$stripped_array = array();
	foreach ($userarray as $TmpStr=>$Date) {
		if (substr($TmpStr, 0, 4)=='ngw_') {
			$UserName = substr($TmpStr, 4);
		} else {
			$UserName = $TmpStr;
		}
		$stripped_array[strtolower($UserName)] = $Date;
	}
	return $stripped_array;
}

?>
