<?php

require_once('exec_piped_command.php');

/**
 * Returns a string containing an image tag for the given Eureka graph
 * data
 */
function eureka_graph_tag($results_array, $desc_1, $desc_2) {
    // encode the results array
    $GZ_ENCODING_LEVEL = 9;
    $encoded_results = urlencode(
        base64_encode(
            gzcompress(
                implode(" ",$results_array),
                $GZ_ENCODING_LEVEL
            )
        )
    );
    
    // put the url together
    $graph_url = htmlentities(
        'eureka_graph.php'
        . '?results=' . $encoded_results
        . '&desc1=' . urlencode($desc_1)
        . '&desc2=' . urlencode($desc_2)
    );

    // construct the image tag
    return '<img src="' . $graph_url . '" alt="Eureka graph" />';
}//eureka_graph_tag


/**
 * Generate a Eureka graph for the given student results array (see 
 * {@link gen_student_results_array gen_student_results_array()}), and
 * return the URL to that image.
 *
 * @param array a result array from gen_student_results_array()
 * @param string the 1st description string
 * @param string the 2nd description string
 * @return string the binary PNG data for the Eureka graph
 */
function gen_eureka_graph($results_array, $desc1, $desc2, $filename="",$show_comp=1) {
    if($filename=="")
    {
	$filename="temp_generic_img".md5(time()).".png";
    }
    $num_elements = count($results_array);

    //format the result array for input to the eurekagraph program
    $output = '/var/www/html/FISDAP/temp/' . $filename . "\n"
            . $desc1 . "\n"
            . $desc2 . "\n"
	    . $show_comp . "\n"
            . $num_elements . "\n"
            . implode(" ", $results_array) . "\n";

    // the path to the generic eureka graph generator
    $graph_generator = '/var/www/cgi-bin/genericgraph';
    if ( file_exists($graph_generator) ) {
        $generator_output = exec_piped_command_workaround(
            $graph_generator,
            $output
        );
    } else {
        die('Could not find graph generator');
    }//else
    return $generator_output;
}//gen_eureka_graph

?>
