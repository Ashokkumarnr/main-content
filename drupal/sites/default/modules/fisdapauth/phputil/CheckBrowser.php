<?php
/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2006.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/

//THIS FILE REQUIRES THAT THE FILE THAT INCLUDES IT ALSO INCLUDES REPORTERROR.HTML
	$browserversion=0;
	$notSupported = 0;
	$brname = "";
//echo "alert('" . $_SERVER['HTTP_USER_AGENT'] . "');\n";
	if(strpos($_SERVER['HTTP_USER_AGENT'],"MSIE"))
    {	$tmpArr = explode("MSIE",$_SERVER['HTTP_USER_AGENT']);
        $browserversion=floatval($tmpArr[1]);
//echo "alert('version is: $browserversion');\n";
        if($browserversion<5.5)
        {	$notSupported = 1;
        	$brname = "Internet Explorer";
        }
    }
    elseif (strpos($_SERVER['HTTP_USER_AGENT'],"Netscape6"))
    {
    	$notSupported = 1;
        $brname = "Netscape";
        $browserversion = 6;
    }
    elseif(!(strpos($_SERVER['HTTP_USER_AGENT'],"Mozilla")===false))
    {	//echo "alert('found it');\n";
    	$tmpArr = explode('Mozilla',$_SERVER['HTTP_USER_AGENT']);
    	$tmpArr2 = explode('/',$tmpArr[1]);
        $browserversion=floatval($tmpArr2[1]);
//echo "alert('version is: $browserversion');\n";
        if($browserversion<5)
        {	$notSupported = 1;
        	$brname = "Netscape/Mozilla";
        }
    }
    else{}
    if($notSupported == 1)
    {  	//echo "alert('not supported');\n";
        echo "</script>\n";
        echo "<style>";
    	echo "<!--";
    	echo "BODY {font-family: Arial;}";
    	echo "-->";
    	echo "</style>";
        echo "</head>\n";
        echo "<body bgcolor='#ffffff'>\n";
        echo "<center>\n";
        echo "<table width='80%'><tr><td>\n";
        echo "Our system has detected that you are using $brname version $browserversion.  The FISDAP system has been tested with:  ";
        echo "<ul>\n";
        echo "<li>Firefox 1.0 or higher ( <a href='http://www.mozilla.com' target='_top'>http://www.mozilla.com</a> ) <font size=-2>For Mac and Windows</font>\n";
        echo "<li>Internet Explorer 5.5 or higher ( <a href='http://www.microsoft.com/windows/ie/' target='_top'>http://www.microsoft.com/windows/ie/</a> ) <font size=-2>Windows Only</font>\n";
        echo "<li>Netscape Navigator 7 or higher. ( <a href='http://browser.netscape.com' target='_top'>http://browser.netscape.com</a> ) <font size=-2>For Mac and Windows</font>\n";
        echo "</ul><br>\n";
        echo "You can download a supported web browser by following one of the links on the list above.<br>";
        echo "If you have any questions please contact support@fisdap.net.";
        echo "</td></tr></table>\n";
        echo "</center>\n";
        echo "</body>\n";
        echo "</html>\n";
        EmailErrorReport("CheckBrowser.php", 1, $REMOTE_USER, "STRING", "A user using an out of date browser has tried to access the Data Entry Section");
        exit;
    }
//echo "alert('version is: $browserversion');\n";
?>