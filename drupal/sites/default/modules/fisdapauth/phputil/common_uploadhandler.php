<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--**********************************************************************-->
<!--                                                                      -->
<!--      Copyright (C) 1996-2008.  This is an unpublished work of        -->
<!--                       Headwaters Software, Inc.                      -->
<!--                          ALL RIGHTS RESERVED                         -->
<!--      This program is a trade secret of Headwaters Software, Inc.     -->
<!--      and it is not to be copied, distributed, reproduced, published, -->
<!--      or adapted without prior authorization                          -->
<!--      of Headwaters Software, Inc.                                    -->
<!--                                                                      -->
<!--**********************************************************************-->

<html>
<head>
<title>Uploading...</title>

</head>
<body>
<?php
require_once("phputil/classes/common_utilities.inc"); // Common Utilities
require_once("phputil/session_data_functions.php"); // Session variable function library

// Get upload-related options passed to us by the form
$script_prefix = $_POST['formprefix'];
$promptnames = $_POST['uploadprompt_promptnames'];

foreach ($promptnames as $name) {
	check_file_upload($name, $script_prefix);
}

/* Checks the file uploaded by the prompt named $promptname.  Validates the file size
 * and file type if requested, and reports any problems with the upload or validation. */
function check_file_upload($promptname, $script_prefix) {

	$value_name = $script_prefix . $promptname;

	// Get the stored settings for this file
	$settings = get_session_value($value_name.'_settings');

	$max_size = $settings['max_size'];
	$dest_dir = $settings['destination'];
	$allowed_types = $settings['allowed_types'];
	$limit_filetype = (count($allowed_types) > 0);

	// Get the array of information about this file
	$tmpdir_fileobj = $_FILES[$promptname];

	$errcode = $tmpdir_fileobj['error'];

	$finfo = new finfo(FILEINFO_MIME, '/usr/share/file/magic');
	$filetype = $finfo->file($tmpdir_fileobj['tmp_name']);
	$filetype = strtok($filetype, ';');

	// See if it uploaded successfully
	if (!is_array($settings)) {
		$failed_message = 'No settings found - has your session expired?';
	} else if ($tmpdir_fileobj['size'] > $max_size
		|| $errcode == UPLOAD_ERR_INI_SIZE
		|| $errcode == UPLOAD_ERR_FORM_SIZE) {
		$failed_message = "File '".$tmpdir_fileobj['name']."' is larger than the allowed size";
	} else if ($errcode == UPLOAD_ERR_PARTIAL) {
		$failed_message = "Your file failed to transfer fully. Please try again";
	} else if ($errcode == UPLOAD_ERR_NO_FILE) {
		$failed_message = "No file was uploaded";
	} else if ($errcode === UPLOAD_ERR_OK && $limit_filetype
		&& !in_array($filetype, $allowed_types)) {
		$failed_message = "Files of type '$filetype' are not allowed here";
	} else if ($errcode !== UPLOAD_ERR_OK) {
		$failed_message = "An error occurred in the upload mechanism. If this occurs repeatedly, please contact FISDAP for support. Error code: $errcode";
	} else {

		// Try to move the file to a permanent location
		$desired_filename = basename($tmpdir_fileobj['name']);

		/*
		// Find a filename that doesn't already exist
		$dest_filename = $dest_dir . $desired_filename;
		for ($i=1; file_exists($dest_filename); $i++) {
			$pinfo = pathinfo($desired_filename);
			//print_r($pinfo);
			$extension = $pinfo['extension'];
			$filename = basename($desired_filename, ".$extension");
			$dest_filename = $dest_dir . "$filename($i)".($extension?".$extension":"");
		}
		 */
		// Get a randomly generated filename
		$dest_filename = tempnam($dest_dir, 'upload_');

		// Now move it
		if (move_uploaded_file($tmpdir_fileobj['tmp_name'], $dest_filename)) {
			$failed_message = '';
		} else {
			$failed_message = "An error occurred in the upload mechanism. If this occurs repeatedly, please contact FISDAP for support. Error: File move";
		}
	}

	// If something went wrong, save the error message and quit
	if ($failed_message != '') {
		set_session_value($value_name,'');
		set_session_value($value_name.'_error', array('const'=>$errcode, 'msg'=>$failed_message));
	} else {
		set_session_value($value_name,$dest_filename);
		set_session_value($value_name.'_error',null);
	}
}
$value_name = $script_prefix . $promptnames[0];
	$settings = get_session_value($value_name.'_settings');

// Run the script to finish the submission process
?>
<script language="javascript" type="text/javascript">
window.top.window.uploadHandler();</script>
</body></html>
