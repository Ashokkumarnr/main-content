<?php

require_once("phputil/classes/common_utilities.inc");
require_once("phputil/test_item_validity_constants.php");
require_once("phputil/fisdap_staff_lib.php");

function validity_text_arr($internal) {
	if ($internal) {
		$inv = 'Failed';
	} else {
		$inv = 'Invalid';
	}
	return array(
		FAILED_ITEM_VALIDITY => $inv,
		NEEDS_REVISION_ITEM_VALIDITY => 'Needs Revision',
		VALIDATED_ITEM_VALIDITY => 'Valid',
		UNDETERMINED_ITEM_VALIDITY => 'Undetermined'
		);
}

function get_validity_constants() {
	return array(VALIDATED_ITEM_VALIDITY, NEEDS_REVISION_ITEM_VALIDITY,
	FAILED_ITEM_VALIDITY, UNDETERMINED_ITEM_VALIDITY);
}

// Returns a textual description of the given validity constant
// $internal denotes text output for internal vs. external viewing
function validity_constant_to_text($const, $internal=false) {
	$validityText = validity_text_arr($internal);
	return $validityText[$const];
}

// Returns HTML to represent item validity, based on the constant it is passed
function validity_constant_to_html($val, $internal=false) {
	$validityText = validity_text_arr($internal);
	switch ($val) {
		case FAILED_ITEM_VALIDITY:
			$ret = '<img src="../images/icon_x.gif"'
					.' title="'.$validityText[$val].'" alt="'.$validityText[$val].' icon">';
			break;
		case VALIDATED_ITEM_VALIDITY:
			$ret = "<img src='../images/icon_check.gif'"
					.' title="'.$validityText[$val].'" alt="'.$validityText[$val].' icon">';
			break;
		case NEEDS_REVISION_ITEM_VALIDITY:
		case UNDETERMINED_ITEM_VALIDITY:
		default:
			$ret = "<img src='../images/icon_hourglass.gif'"
					.' title="'.$validityText[$val].'" alt="'.$validityText[$val].' icon">';
			break;
	} //switch
	return $ret;
}

/* This class is confused and doesn't know how to act.  It should be rewritten to either
 * behave statically or not, and to be either a model or controller, not both. */
class CertificationLevels {
	//$connection; // DB

	/* Constructor */
	function CertificationLevels($connection) {
		$this->connection = $connection;
	}

	function get_levels($connection=null) {
		if (!$connection) {
			$connection = $this->connection;
		}
		//TODO hardcoded stuff
		$select = "SELECT AssetDef_id,Tree_id,AssetName FROM Asset_def"
			." WHERE Tree_id LIKE '6550.6551.%'";
		$result = $connection->query($select);
		$newarr = array();
		foreach ($result as $obj) {
			$newarr[$obj['Tree_id']] = $obj['AssetName'];
		}
		return $newarr;
	}

	function treeid_to_cert_const($treeid) {
		$levels = CertificationLevels::get_levels();
		$name = $levels[$treeid];
		switch (strtoupper($name)) {
		case 'EMT-B':
			return TestObjectivesList::EMT_B();
		case 'EMT-I':
			return TestObjectivesList::EMT_I();
		case 'PARAMEDIC':
			return TestObjectivesList::EMT_PARAMEDIC();
		}
	}

	//WARNING: Untested
	function cert_const_to_treeid($const) {
		$name='';
		switch ($const) {
		case TestObjectivesList::EMT_B():
			$name =  'EMT-B';
			break;
		case TestObjectivesList::EMT_I():
			$name =  'EMT-I';
			break;
		case TestObjectivesList::EMT_PARAMEDIC():
			$name =  'PARAMEDIC';
			break;
		}
		if ($name != '') {
			$levels = CertificationLevels::get_levels();
			$treeid = array_search($name, $levels);
		}

		return $treeid;
	}
}

/**
 * A class to handle the mapping from curriculum category names to specific
 * asset id numbers.  Also can differentiate by the type of certification a
 * category is in the context of (i.e. NSC, NREMS, etc)
 *
 * @todo change all the constants here to real constants (thanks, PHP 5!)
 */
class TestObjectivesList {

	var $ids; // Holds arrays of TestId objects, indexed by module names (e.g. 'Airway')

	/* Pseudo-Constants */
	function NSC_PARAMEDIC() { return 1; }
	function NSC_BASIC() { return 2; }
	function NREMT_2004() { return 3; }
	function ALL_CONTEXTS() { return 99; }

	function EMT_B() { return 4; }
	function EMT_I() { return 5; }
	function EMT_PARAMEDIC() { return 6; }

	/* Constructor */
	function TestObjectivesList($ids = array()) {
		$this->ids = $ids;
	}

	/* Returns an array of all the context constants in the class */
	function get_all_contexts() {
		return array($this->NSC_PARAMEDIC(), $this->NSC_BASIC(), $this->NREMT_2004(), $this->ALL_CONTEXTS());
	}

	/* Returns an array of all the certification constants in the class */
	function get_all_certifications() {
		return array($this->EMT_B(), $this->EMT_I(), $this->EMT_PARAMEDIC());
	}

	/* Add associations for elements in $iditems to $name. */
	function add($name, $iditems) {
		if (!is_array($iditems)) {
			$iditems = array($iditems);
		}
		$this->ids = array_merge_recursive($this->ids, array($name => $iditems));
	}

	/* Filter known mappings for module $name by $criteria, which may be a certification
		or context constant, or an array of several of those.
	   Returns an array of TestIds. */
	function filter_by($name, $criteria) {
		if (!is_array($criteria)) $criteria = array($criteria);
		$retarr = array();
		$thisarray = $this->get_array($name);
		foreach ($thisarray as $curr_tid) {
			$valid = false;
			foreach ($criteria as $thisarg) {

				if ((in_array($thisarg, $this->get_all_certifications())
					  && $curr_tid->has_certification($thisarg))
					|| (in_array($thisarg, $this->get_all_contexts())
					  && $curr_tid->in_context($thisarg))) {
					$valid = true;
					break;
				}
			}
			if ($valid) {
				//$retarr[] = $curr_tid->get_tree_id();
				$retarr[] = $curr_tid;
			}
		}
		return $retarr;
	}

	/* Get the array of TestIds associated with $name */
	function get_array($name) {
		return $this->ids[$name];
	}

	/* Returns an array of tree_ids related to the given $name for the given $criteria */
	function get_related_tree_ids($name, $criteria) {
		$newarr = $this->filter_by($name, $criteria);
		//return $newarr;
		return array_map(create_function('$item', 'return $item->get_tree_id();'), $newarr);
	}

	/* Returns an array of category-to-treeids associations in this object */
	function get_all_associations() {
		$newarr = array();
		foreach ($this->ids as $name => $idlist) {
			$newarr[$name] = array_map(create_function('$item', 'return $item->get_tree_id;'), $idlist);
		}
		return $newarr;
	}

	/* Returns an array of all category names found in this object */
	function get_category_names() {
		return array_keys($this->get_all_associations());
	}

}

/* An object that tracks a given item in the DB, along with the certification level and
	part of the curriculum that it maps to. */
class TestId {

	var $curriculum;
	var $certifications;
	var $tree_id;

	function TestId($id=null, $curric=null, $certs=null) {
		$this->curriculum = $curric;
		$this->tree_id = $id;
		if (is_array($_certs)) {
			$this->certifications = $certs;
		} else {
			$this->certifications = array($certs);
		}
	}

	/* STATIC: Create an array of TestIds with given associations from an array $arr of tree_ids */
	function from_array($arr, $curric, $cert) {
		$cb = create_function('$id', "return new TestId(\$id,$curric,$cert);");
		return array_map($cb, $arr);
	}

	/* Does this item match the given curriculum context? */
	function in_context($context) {
		return ($context == $this->curriculum || $context == TestObjectivesList::ALL_CONTEXTS());
	}

	/* Is this marked appropriate for the given certification level? */
	function has_certification($cert) {
		return (in_array($cert, $this->certifications));
	}

	function get_tree_id() {
		return $this->tree_id;
	}

}

// Create an objectives list and populate it with all of our mappings.
function create_objectives_list($mode = 'search') {

	$list = new TestObjectivesList();

	// for readability
	$const_NSC_PARAMEDIC = TestObjectivesList::NSC_PARAMEDIC();
	$const_NSC_BASIC = TestObjectivesList::NSC_BASIC();
	$const_NREMT_2004 = TestObjectivesList::NREMT_2004();

	$const_EMT_B = TestObjectivesList::EMT_B();
	$const_EMT_I = TestObjectivesList::EMT_I();
	$const_EMT_PARAMEDIC = TestObjectivesList::EMT_PARAMEDIC();


	$list->add('Airway', array_merge(
		TestId::from_array(array('12.13', '12.1703.1233'), $const_NSC_PARAMEDIC, $const_EMT_PARAMEDIC),
		TestId::from_array(array('5023.5025'), $const_NSC_BASIC, $const_EMT_B),
		array(
			new TestId('4192.4193.4194', $const_NREMT_2004, $const_EMT_B),
			new TestId('4192.4267.4268', $const_NREMT_2004, $const_EMT_I),
			new TestId('4192.4344.4345', $const_NREMT_2004, $const_EMT_PARAMEDIC))
		));

	$list->add('Trauma', array_merge(
		TestId::from_array(array('12.644'), $const_NSC_PARAMEDIC, $const_EMT_PARAMEDIC),
		TestId::from_array(array('5023.5028'), $const_NSC_BASIC, $const_EMT_B),
		array(
			new TestId('4192.4193.4207', $const_NREMT_2004, $const_EMT_B),
			new TestId('4192.4267.4281', $const_NREMT_2004, $const_EMT_I),
			new TestId('4192.4344.4358', $const_NREMT_2004, $const_EMT_PARAMEDIC))
		));

	$list->add('Medical', array_merge(
		TestId::from_array(array('12.1703'), $const_NSC_PARAMEDIC, $const_EMT_PARAMEDIC),
			
		TestId::from_array(array('5023.5027'), $const_NSC_BASIC, $const_EMT_B),
		array(
			new TestId('4192.4193.4224', $const_NREMT_2004, $const_EMT_B),
			new TestId('4192.4267.4299', $const_NREMT_2004, $const_EMT_I),
			new TestId('4192.4344.4375', $const_NREMT_2004, $const_EMT_PARAMEDIC))
		));

	$list->add('OB, Gyn, & Peds', array_merge(
		TestId::from_array(array('12.1473.1474', '12.1473.1581', '12.1703.1937'), $const_NSC_PARAMEDIC, $const_EMT_PARAMEDIC),
			
		TestId::from_array(array('5023.5029', '5023.5027.5055'), $const_NSC_BASIC, $const_EMT_B),
		array(
			new TestId('4192.4193.4251', $const_NREMT_2004, $const_EMT_B),
			new TestId('4192.4267.4328', $const_NREMT_2004, $const_EMT_I),
			new TestId('4192.4344.4403', $const_NREMT_2004, $const_EMT_PARAMEDIC))
		));

			$list->add('Cardiology', array_merge(
		TestId::from_array(array('12.1703.1260'), $const_NSC_PARAMEDIC, $const_EMT_PARAMEDIC),
			
		TestId::from_array(array('5023.5027.5049'), $const_NSC_BASIC, $const_EMT_B),
		array(
			new TestId('4192.4193.4199', $const_NREMT_2004, $const_EMT_B),
			new TestId('4192.4267.4273', $const_NREMT_2004, $const_EMT_I),
			new TestId('4192.4344.4350', $const_NREMT_2004, $const_EMT_PARAMEDIC))
		));

	$list->add('Operations', array_merge(
		TestId::from_array(array('12.1722'), $const_NSC_PARAMEDIC, $const_EMT_PARAMEDIC),
			
		TestId::from_array(array('5023.5030'), $const_NSC_BASIC, $const_EMT_B),
		array(
			new TestId('4192.4193.4877', $const_NREMT_2004, $const_EMT_B),
			new TestId('4192.4267.4917', $const_NREMT_2004, $const_EMT_I),
			new TestId('4192.4344.4837', $const_NREMT_2004, $const_EMT_PARAMEDIC))
		));

	// Add stuff that we only search and do not associate
	if ($mode != 'associate') {

		$list->add('Airway', TestId::from_array(array('5023.5027.5048', '5023.5031'), $const_NSC_BASIC, $const_EMT_B));

		$list->add('Trauma', TestId::from_array(array('5023.5026.5041'), $const_NSC_BASIC, $const_EMT_B));

		$list->add('Medical', TestId::from_array(array('5023.5024.5035'), $const_NSC_BASIC, $const_EMT_B));

		$list->add('Medical', TestId::from_array(array('12.14.316', '12.14.282', '12.14.317', '12.4640'), $const_NSC_PARAMEDIC, $const_EMT_PARAMEDIC));

		$list->add('Operations', array_merge(
			TestId::from_array(array('12.14.125', '12.14.172', '12.14.219', '12.14.234', '12.14.270', '12.14.361', '12.393', '12.1720'), $const_NSC_PARAMEDIC, $const_EMT_PARAMEDIC),
			TestId::from_array(array('5023.5026', '5023.5024.5032', '5023.5024.5033', '5023.5024.5034', '5023.5024.5034', '5023.5024.5037', '5023.5026'), $const_NSC_BASIC, $const_EMT_B)
			));
	}

	return $list;
}

function create_certification_selection($connection, $text=null, $name=null, $required=null, $type=null, $subtext=null, $nulloption=false, $nulloptionvalue=null) {
	common_utilities::apply_defaults(
		array(&$text, &$name, &$required, &$type, &$subtext),
		array('Certification:', 'cert_level', false, 'checkbox', ''));

	$prompt = new common_prompt($name,null,$text,$subtext,$type,$required);

	$cert_arr = CertificationLevels::get_levels($connection);
	foreach ($cert_arr as $id => $name) {
		$prompt->set_promptset($id, $name, false, null, false);
	}
	return $prompt;
}

// Creates a dialog to choose curriculum objectives
// Takes as an argument an object of type TestObjectivesList
function create_module_selection($connection, $module_listobject, $text=null, $name=null, $required=null, $type=null, $subtext=null, $nulloption=false, $nulloptionvalue=null) {
	common_utilities::apply_defaults(
		array(&$text, &$name, &$required, &$type, &$subtext),
		array('Module:', 'module_list', false, 'checklist', ''));

	$module_prompt = new common_prompt($name,null,$text,$subtext,$type,$required);

	if ($nulloption) {
		$module_prompt->set_promptset($nulloptionvalue, '-- Select One --', true, null, $required | ($type == 'checklist'));
	}

	// Get the names of all categories
	foreach ($module_listobject->get_category_names() as $name) {
		$module_prompt->set_promptset($name, $name, false, null, false);
	}
	//$module_prompt->set_option('height','medium');
	return $module_prompt;
}

// Creates and returns the common_prompt to select an author with a dropdown
function create_author_selection($connection, $text=null, $name=null, $required=null, $type=null, $subtext=null, $nulloption=false, $nulloptionvalue=null) {
	common_utilities::apply_defaults(
		array(&$text, &$name, &$required, &$type, &$subtext),
		array('Author:', 'author', true, 'select', ''));

	$auth_prompt = new common_prompt($name,null,$text,$subtext,$type,$required);

	if ($nulloption) {
		$auth_prompt->set_promptset($nulloptionvalue, '-- Select One --', true, null, $required & $nulloption);
	}

	// Get distinct authors who have contributed something
	$select = $select." SELECT DISTINCT(U.idx), U.firstname, U.lastname";
	$select = $select." FROM UserAuthData U, AuthorData, Asset_def";
	$select = $select." WHERE Asset_def.DataType_id=".ITEM_ASSET_TYPE;
	$select = $select." AND Asset_def.AssetDef_id=AuthorData.AssetDef_id
						AND AuthorData.UserAuth_id=U.idx";
	$select = $select." ORDER BY U.lastname";

	$result = $connection->query($select);

	// Add all authors to list with UserAuth_id as value
	$i = -1;
	while ($result[++$i]) {
		$auth_prompt->set_promptset($result[$i]['idx'], $result[$i]['lastname'].", ".$result[$i]['firstname'], null, null, null);
	}

	return $auth_prompt;
}

function create_item_type_selection($connection, $text=null, $name=null, $required=null, $type=null, $subtext=null, $nulloption=false, $nulloptionvalue=null) {
	$args =	array(&$text, &$name, &$required, &$type, &$subtext);
	common_utilities::apply_defaults($args, array('Item Type', 'itemtype', true, 'select', ''));

	$prompt = new common_prompt($name,$value,$text,$subtext,$type,$required);

	if ($nulloption) {
		$prompt->set_promptset($nulloptionvalue, '-- Select One --', true, null, $required & $nulloption);
	}
	$select="SELECT TestItemType_id,TestItemType from TestItemTypes ORDER BY
	TestItemType_id";
	$result = $connection->query($select);
	foreach ($result as $obj) {
		$type_id = $obj['TestItemType_id'];
		$name = $obj['TestItemType'];
		$prompt->set_promptset($type_id, ucwords($name), null, null, null);
	}
	return $prompt;
}

function create_active_project_selection($connection, $text=null, $name=null, $required=null, $type=null, $subtext=null, $nulloption=false, $nulloptionvalue=null) {
    if(is_fisdap_staff_member() || is_user_in(null,array('mbowen2','cdick1'))) {
		$activetest = ' > 0';
	} else {
		$activetest = ' = 1';
	}

	return create_project_selection($connection, $text, $name, $required, $type, $subtext, $nulloption, $nulloptionvalue, $activetest);
}
function create_all_project_selection($connection, $text=null, $name=null, $required=null, $type=null, $subtext=null, $nulloption=false, $nulloptionvalue=null) {
	$activetest = null;
	return create_project_selection($connection, $text, $name, $required, $type, $subtext, $nulloption, $nulloptionvalue, $activetest);
}

function create_project_selection($connection, $text=null, $name=null, $required=null, $type=null, $subtext=null, $nulloption=false, $nulloptionvalue=null, $activetest) {
	$args =	array(&$text, &$name, &$required, &$type, &$subtext);
	common_utilities::apply_defaults($args, array('Project:', 'active_project', true, 'select', ''));

	$prompt = new common_prompt($name,$value,$text,$subtext,$type,$required);

	if ($nulloption) {
		$prompt->set_promptset($nulloptionvalue, '-- Select One --', true, null, $required & $nulloption);
	}

	if ($activetest)
		$active = 'WHERE Active '.$activetest;

    $select = "SELECT Project_id,ProjectName,Active FROM ProjectTable $active ORDER BY ProjectName";
	$result = $connection->query($select);

	// If we didn't find any projects, exit early
	if (count($result) == 0) {
		$prompt->set_promptset(null, '-- None Found --', false, null, true);
		return $prompt;
	}

	$inactives = array();
	foreach ($result as $obj) {
		$name = $obj['ProjectName'];
		$projid = $obj['Project_id'];
		$active = $obj['Active'];
		if ($active < 1) {
			$inactives[] = $obj;
			continue;
		}
		$prompt->set_promptset($projid, $name, null, null, null);
	}
	foreach ($inactives as $obj) {
		$name = $obj['ProjectName'];
		$projid = $obj['Project_id'];
		$active = $obj['Active'];
		$name = "~$name";
		$prompt->set_promptset($projid, $name, null, null, null);
	}

	return $prompt;
}

function create_blueprint_selection($connection, $text=null, $name=null, $required=null, $type=null, $subtext=null, $nulloption=false, $nulloptionvalue=null, $no_bp_option, $no_bp_value=null, $project_id=null) {
	$args =	array(&$text, &$name, &$required, &$type, &$subtext);
	common_utilities::apply_defaults($args, array('Blueprint:', 'blueprint', true, 'select', ''));

	$prompt = new common_prompt($name,$value,$text,$subtext,$type,$required);

	if ($nulloption) {
		$prompt->set_promptset($nulloptionvalue, '-- Select One --', true, null, $required & $nulloption);
	}
	if ($no_bp_option) {
		$prompt->set_promptset($no_bp_value, 'Not yet in a blueprint', true, null, false);
	}

	//TODO
	if ($project_id) {
		$where = "WHERE Project_id = $project_id";
	}

	$select = "SELECT Name,tbp_id FROM TestBluePrints $where ORDER BY Name";
	$result = $connection->query($select);

	foreach ($result as $obj) {
		$name = $obj['Name'];
		$projid = $obj['tbp_id'];
		$prompt->set_promptset($projid, $name, null, null, null);
	}
	return $prompt;
}

function create_available_reviewer_selection($connection, $text=null, $name=null, $required=null, $type=null, $subtext=null, $nulloption=false, $nulloptionvalue=null) {
	$args =	array(&$text, &$name, &$required, &$type, &$subtext);
	common_utilities::apply_defaults($args, array('Reviewer:', 'avail_reviewers', true, 'select', ''));

	$prompt = new common_prompt($name,$value,$text,$subtext,$type,$required);

	if ($nulloption) {
		$prompt->set_promptset($nulloptionvalue, '-- Select One --', true, null, $required & $nulloption);
	}

	$select = "SELECT DISTINCT reviewer FROM ItemReviews WHERE mode!='consensus' ORDER BY reviewer";
	$result = $connection->query($select);
	foreach ($result as $obj) {
		$name = strtolower($obj['reviewer']);
		$prompt->set_promptset($name, $name, null, null, null);
	}
	return $prompt;
}

