<?php
/**
 * This file contains utility functions for dealing
 * with preceptors in FISDAP.  
 */

//////////////////////////////////////////////////
// Useful Constants 
//////////////////////////////////////////////////
define('PDA_PRECEPTOR_SITE_LIMIT', 100);


/**
 * Link the given preceptor to the given program, using the given active
 * status (0 => non-active, 1 => active).
 *
 * @warning: this function does not sanitize its input 
 */
function link_preceptor_to_program($preceptor_id, 
                                   $program_id, 
                                   $is_active, 
                                   $db_connect) {
    // check for an existing link
    $query = 'SELECT * FROM ProgramPreceptorData '.
             'WHERE Preceptor_id="'.$preceptor_id.'" '.
             'AND Program_id="'.$program_id.'"';
    $result = mysql_query( $query, $db_connect );
    if ( !$result ) {
        die(
            'link_preceptor_to_program: failed to check for existing links '.
            'for the given preceptor and program'
        );
    }//if
    
    $query = 'INSERT INTO ProgramPreceptorData SET '.
             'Preceptor_id="'.$preceptor_id.'", '.
             'Program_id="'.$program_id.'", '.
             'Active="'.$is_active.'"';
    $result = mysql_query( $query, $db_connect );
    if ( !$result ) {
        die('link_preceptor_to_program: failed to insert '.
            'program-preceptor associations');
    }//if

    return true;
}//link_preceptor_to_program

/**
 * Get an array representation of the preceptor from the MySQL database. 
 * This associative array contains the key/value pairs from PreceptorData
 * in addition to the key "Programs", which is associated with an array 
 * of arrays containing the result set from the relevant query to 
 * ProgramPreceptorData.
 *
 * @warning: this function does not sanitize its input
 */
function get_preceptor_by_name($first_name, 
                               $last_name, 
                               $amb_serv_id, 
                               $db_connect) {
    // first, get the info from PreceptorData
    $query = 'SELECT * FROM PreceptorData '.
             'WHERE FirstName="'.$first_name.'" '.
             'AND LastName="'.$last_name.'" '.
             'AND AmbServ_id="'.$amb_serv_id.'"';
    $result = mysql_query($query,$db_connect);
    if ( !$result ) {
        // on a mysql error, stop the script with an error message
        die(
            'get_preceptor_by_name: query to get basic preceptor '.
            'information failed'
        );
    } else if ( !mysql_num_rows($result) ) {
        // if there weren't any preceptors by the given name, return
        // false
        return false;
    }//if
 
    $preceptor_array = mysql_fetch_assoc($result);
    
    // now, we need the rows from ProgramPreceptorData
    $preceptor_array['Programs'] = array();
    $query = 'SELECT Preceptor_id, '
        .           'Program_id, '
        .           'Active '
        .    'FROM ProgramPreceptorData '
        .    'WHERE Preceptor_id="'.$preceptor_array['Preceptor_id'].'"';
    $result = mysql_query($query,$db_connect);
    if ( !$result ) {
        die(
            'get_preceptor_by_name: query to get program-preceptor '.
            'associations failed'
        );
    }//if

    // add the ProgramPreceptorData to the result array
    while( $row = mysql_fetch_assoc($result) ) {
        $preceptor_array['Programs'][] = $row;
    }//while

    return $preceptor_array;
}//get_preceptor_by_name


/**
 * Fetch all distinct preceptors for the given student at the given
 * sites.
 */
function fetch_student_preceptors_for_sites($student_id, $sites) {
    $query = 'SELECT preceptors.Preceptor_id, '
        .           'preceptors.FirstName, '
        .           'preceptors.LastName, '
        .           'preceptors.AmbServ_id '
        .    'FROM PreceptorData preceptors, '
        .         'ProgramPreceptorData links, '
        .         'StudentData students '
        .    'WHERE preceptors.AmbServ_id IN (' . implode(', ', $sites) . ') '
        .    'AND   students.Student_id="' . $student_id . '" '
        .    'AND   links.Preceptor_id=preceptors.Preceptor_id '
        .    'AND   links.Active="1" '
        .    'AND   links.Program_id=students.Program_id '
        .    'GROUP BY preceptors.FirstName, '
        .             'preceptors.LastName, '
        .             'preceptors.AmbServ_id '
        .    'ORDER BY preceptors.LastName, preceptors.FirstName';
    
    $connection_obj =& FISDAPDatabaseConnection::get_instance();
    $result = $connection_obj->query($query);
    if ( $result && is_array($result) ) {
        return $result;
    }//if

    return array();
}//fetch_student_preceptors_for_sites


/**
 * Uses an alternate method to fetch preceptors, for use with sites with
 * too many preceptors to view at once via the PDA interface.
 */
function fetch_student_preceptors_for_sites_alternate($student_id, 
                                                      $sites) {
    $query = 'SELECT preceptors.Preceptor_id, '
        .           'preceptors.AmbServ_id, '
        .           'preceptors.LastName, '
        .           'preceptors.FirstName, '
        .           'preceptors.AmbServ_id '
        .    'FROM PreceptorData preceptors, '
        .         'ProgramPreceptorData links, '
        //.         'StudentData students1, '
        //.         'StudentData students2, '
        .         'RunData runs, '
        .         'ShiftData shifts '
        .    'WHERE preceptors.AmbServ_id IN (' . implode(', ', $sites) . ') '
        .    'AND   shifts.AmbServ_id IN (' . implode(', ', $sites) . ') '
        .    'AND   shifts.StartDate>DATE_SUB('
        .               'CURDATE(),  '
        .               'INTERVAL 2 YEAR'
        .          ') '
        .    'AND   shifts.Shift_id=runs.Shift_id '
        //.    'AND   students1.Student_id="' . $student_id . '" '
        //.    'AND   students1.Program_id=students2.Program_id '
        //.    'AND   runs.Student_id=students2.Student_id '
        .    'AND   runs.Student_id="' . $student_id . '" '
        .    'AND   runs.Precept_id=preceptors.Preceptor_id '
        .    'AND   links.Preceptor_id=preceptors.Preceptor_id '
        .    'AND   links.Active="1" '
        .    'GROUP BY preceptors.FirstName, '
        .             'preceptors.LastName, '
        .             'preceptors.AmbServ_id '
        .    'ORDER BY preceptors.LastName, preceptors.FirstName';
    
    $connection_obj =& FISDAPDatabaseConnection::get_instance();
    $result = $connection_obj->query($query);
    if ( $result && is_array($result) ) {
        return $result;
    }//if

    return array();
}//fetch_student_preceptors_for_sites_alternate


/**
 * Returns an array of preceptor ids, firstnames, lastnames, full names,
 * and ambserv_ids for each unique preceptor name->ambserv_id combo tied
 * to the given student's program.
 *
 * @param int $student_id the numeric id of the student in question
 * @return array the preceptor information for the given student
 */
function fetch_student_preceptors($student_id) {
    $connection_obj =& FISDAPDatabaseConnection::get_instance();

    $preceptor_query = 'SELECT P.AmbServ_id as site, '.
                              'COUNT('.
                                  'DISTINCT '.
                                  'P.FirstName, '.
                                  'P.LastName, '.
                                  'P.AmbServ_id'.
                              ') as preceptors '.
                       'FROM PreceptorData P, '.
                            'StudentData St, '.
                            'ProgramPreceptorData T '. 
                       'WHERE St.Student_id="'.$student_id.'" AND '.
                             'St.Program_id=T.Program_id AND '.
                             'T.Preceptor_id=P.Preceptor_id AND '.
                             'T.Active="1" '.
                       'GROUP BY P.AmbServ_id ';

    $result = $connection_obj->query($preceptor_query);
    if ( !$result || !is_array($result) ) {
        return false;
    }//if

    $normal_sites   = array();
    $overfull_sites = array();
    foreach ( $result as $row ) {
        if ( $row['preceptors'] > PDA_PRECEPTOR_SITE_LIMIT ) {
            $overfull_sites[] = $row['site'];
        } else {
            $normal_sites[] = $row['site'];
        }//else
    }//foreach

    if ( !count($overfull_sites) ) {
        return fetch_student_preceptors_for_sites($student_id, $normal_sites);
    } else {
        return array_merge(
            fetch_student_preceptors_for_sites($student_id, $normal_sites),
            fetch_student_preceptors_for_sites_alternate(
                $student_id,
                $overfull_sites
            )
        );
    }//else


    //$preceptor_query = 'SELECT P.Preceptor_id, '.
    //                          'P.FirstName, '.
    //                          'P.LastName, '.
    //                          'P.AmbServ_id '.
    //                   'FROM PreceptorData P, '.
    //                        'StudentData St, '.
    //                        'ProgramPreceptorData T '. 
    //                   'WHERE St.Student_id="'.$student_id.'" AND '.
    //                         'T.Preceptor_id=P.Preceptor_id AND '.
    //                         'St.Program_id=T.Program_id AND '.
    //                         'T.Active="1"'.
    //                   'GROUP BY P.FirstName, P.LastName, P.AmbServ_id '.
    //                   'ORDER BY P.LastName, P.FirstName';

    //$preceptor_query = 'SELECT preceptors.Preceptor_id, '
    //    .                     'preceptors.FirstName, '
    //    .                     'preceptors.LastName, '
    //    .                     'preceptors.AmbServ_id '
    //    .              'FROM PreceptorData preceptors, '
    //    .                   'RunData runs, '
    //    .                   'StudentData students1, '
    //    .                   'StudentData students2, '
    //    .                   'ShiftData shifts '
    //    .              'WHERE students1.Student_id="' . $student_id . '" '
    //    .              'AND students1.Program_id=students2.Program_id '
    //    .              'AND runs.Student_id=students2.Student_id '
    //    .              'AND runs.Precept_id=preceptors.Preceptor_id '
    //    .              'AND preceptors.Active="1" '
    //    .              'AND runs.Shift_id=shifts.Shift_id '
    //    .              'AND shifts.StartDate>DATE_SUB('
    //    .                      'CURDATE(), '
    //    .                      'INTERVAL 2 YEAR'
    //    .                  ') ' 
    //    .              'GROUP BY preceptors.FirstName, '
    //    .                       'preceptors.LastName, '
    //    .                       'preceptors.AmbServ_id '
    //    .              'ORDER BY preceptors.LastName, preceptors.FirstName';

    return $connection_obj->query($preceptor_query);
}//fetch_student_preceptors


/**
 * Dumps XML data for all preceptors for the given student to stdout.
 * Note that this function will only output Preceptor elements for
 * inclusion in an XML file (see the schema file pda/Preceptor.xsd), but
 * will not generate a full xml file.
 */
function dump_pda_preceptor_xml($student_id) {
    $preceptor_rows = fetch_student_preceptors($student_id);
    if ( is_array($preceptor_rows) && count($preceptor_rows) ) {
        foreach( $preceptor_rows as $preceptor_row ) {
            echo '    <Preceptor ';
            foreach( $preceptor_row as $key=>$value ) {
                $value = utf8_encode(
                    htmlspecialchars($value, ENT_COMPAT, 'UTF-8')
                );
                echo $key.'="'.$value.'" ';
            }//foreach
            
            // add the FullName field for easier on-device sorting
            $full_name = utf8_encode(
                htmlspecialchars(
                    $preceptor_row['LastName'] 
                    . ' ' 
                    . $preceptor_row['FirstName'],
                    ENT_COMPAT, 
                    'UTF-8'
                )
            );
            echo 'FullName="' . $full_name . '" ';
            echo '/>'."\n";
        }//foreach
    }//if
}//dump_pda_preceptor_xml

?>
