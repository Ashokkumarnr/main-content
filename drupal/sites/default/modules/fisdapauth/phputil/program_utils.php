<?php

/**
 * This file contains utilities related to fetching/manipulating program
 * information in FISDAP.
 *
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *  
 *                                                                           *
 *                                                                           *
 * @author Sam Martin <smartin@fisdap.net>                                   *
 * @copyright 1996-2007 Headwaters Software, Inc.                            *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 */

  
/**
 * Returns true iff the given program is allowed to order accounts via invoice, false otherwise.
 */
function program_canBuyAccounts($program_id) {
    $connection =& FISDAPDatabaseConnection::get_instance();
    $query = "SELECT CanBuyAccounts "
        .    "FROM ProgramData "
        .    "WHERE Program_id=$program_id";
	$result = $connection->query($query);
	if ( !$result || !is_array($result) || !count($result) ) {
        return false;
    }//if

    return $result[0]['CanBuyAccounts'];
}//program_canBuyAccounts

/**
 * Returns true iff the given program uses narratives, false otherwise.
 */
function program_uses_narrative($program_id) {
    $connection =& FISDAPDatabaseConnection::get_instance();
    $query = "SELECT UseNarrative "
        .    "FROM ProgramData "
        .    "WHERE Program_id=$program_id";
    $result = $connection->query($query);
    if ( !$result || !is_array($result) || !count($result) ) {
        return false;
    }//if

    return ($result['UseNarrative'] == 1);
}//program_uses_narrative

/**
 * Given a program id, returns the program name
 */

function fetch_program_name($program_id) {
    $connection =& FISDAPDatabaseConnection::get_instance();

	$select = "SELECT ProgramName FROM ProgramData ".
			  "WHERE Program_id=$program_id";
	$result = $connection->query($select);
	
	if (count($result) != 1) {
		return false;
	}
	else {
		return $result[0]['ProgramName'];
	}
}

?>
