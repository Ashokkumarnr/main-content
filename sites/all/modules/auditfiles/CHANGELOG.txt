30 October 2007
---------------
Module updated for Drupal 6.
  - reports separated out in to separate include files
  - admin page separated out in to separate include file
  - files not on server table made sortable
  - reports now appear directly under Administer > Logs as grouping has been
    removed from the new Drupal 6 menu system.

27 October 2007
---------------
#183798 - makes exclusions user configurable - exclusions can be set for files,
paths or extensions

26 October 2007
---------------
#183730 - add an "edit" link for files not on the server.
#183725 - provide count of files found in each category when report is viewed.
#186139 - make links to files not in the database clickable

11 April 2007
-------------
More helpful if the Files not on server report lists sorted by filename, not
nid. Easier to use the report if you want to compare with an FTP listing etc

10 April 2007
-------------
First version for Drupal 5

