<?php
/**
 * @file
 * Provides admin settings page to adjust HubSpot API key, debugging settings,
 * and JavaScript embedding.
 */

function hubspot_admin_settings() {
  $form = array();

  $hubspot_access_token = $_GET['access_token'];
  if (!empty($hubspot_access_token)) variable_set('hubspot_access_token', $hubspot_access_token);
  
  $hubspot_refresh_token = $_GET['refresh_token'];
  if (!empty($hubspot_refresh_token)) variable_set('hubspot_refresh_token', $hubspot_refresh_token);
  
  $hubspot_expires_in = $_GET['expires_in'];
  if (!empty($hubspot_expires_in)) variable_set('hubspot_expires_in', $hubspot_expires_in);
  
  $hubspot_error = $_GET['error'];
  
  if ($hubspot_error == "access_denied") {
    drupal_set_message(t('You denied the request for authentication with Hubspot. Please click the button again and choose the AUTHORIZE option.'),'error',FALSE);
  }

  $form['settings'] = array(
    '#title' => t('HubSpot Settings'),
    '#type' => 'fieldset',
  );

  $form['settings']['hubspot_portalid'] = array(
    '#title' => t('HubSpot Portal ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hubspot_portalid', ''),
    '#description' => t('Enter the Hubspot Portal ID for this site.  It can be found by <a href="https://login.hubspot.com/login" target="_blank">logging into HubSpot</a> going to the Dashboard and examining the url. Example: "https://app.hubspot.com/dashboard-plus/12345/dash/".  The number after "dashboard-plus" is your Portal ID.'),
  );
  
  if (variable_get('hubspot_portalid', '')) {
    $form['settings']['hubspot_authentication'] = array(
      '#value' => t('Connect Hubspot Account'),
      '#type' => 'submit',
      '#validate' => array(),
      '#submit' => array('hubspot_oauth_submit'),
    );
    
    if (variable_get('hubspot_refresh_token','')) {
      $form['settings']['hubspot_authentication']['#suffix'] = t('Your Hubspot account is connected.');
      $form['settings']['hubspot_authentication']['#value'] = t('Disconnect Hubspot Account');
      $form['settings']['hubspot_authentication']['#submit'] = array('hubspot_oauth_disconnect');
    }
  }
  
  $form['settings']['hubspot_log_code'] = array(
    '#title' => t('HubSpot Traffic Logging Code'),
    '#type' => 'textarea',
    '#default_value' => variable_get('hubspot_log_code', ''),
    '#description' => t('To enable HubSpot traffic logging on your site, paste the External Site Traffic Logging code here.'),
  );
  
  $form['debug'] = array(
    '#title' => t('HubSpot Debugging Settings'),
    '#type' => 'fieldset',
  );
  
  $form['debug']['hubspot_debug_on'] = array(
    '#title' => t('Debugging enabled'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('hubspot_debug_on', 0),
    '#description' => t('If debugging is enabled, HubSpot errors will be emailed to the address below. Otherwise, they will be logged to the regular Drupal error log.'),
  );
  
  $form['debug']['hubspot_debug_email'] = array(
    '#title' => t('Debugging email'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hubspot_debug_email', variable_get('site_mail', '')),
    '#description' => t('Email error reports to this address if debugging is enabled.'),
  );
  
  return system_settings_form($form);
}

/**
 * Validation for hubspot_admin_settings form. Ensure that the Portal ID
 * has been entered and the debug email address provided is valid if debugging
 * is enabled.
 */
function hubspot_admin_settings_validate($form, &$form_state) {
  if (empty($form_state['values']['hubspot_portalid'])) {
    form_set_error('hubspot_portalid', t('You must provide a Hubspot Portal ID.'));
  }
  if ($form_state['values']['hubspot_debug_on'] &&
      !valid_email_address($form_state['values']['hubspot_debug_email'])) {
    form_set_error('hubspot_debug_email', t('You must provide a valid email address.'));
  }

}

function hubspot_oauth_submit($form, &$form_state) {
  global $base_root;
  $data = 'client_id=' . HUBSPOT_CLIENT_ID .
    '&portalId=' . $form_state['values']['hubspot_portalid'] .
    '&redirect_uri=' . $base_root . '/admin/settings/hubspot' .
    '&scope=' . 'leads-rw+offline';
    
  $form_state['redirect'][] = 'https://app.hubspot.com/auth/authenticate?'. $data;
  
}

function hubspot_oauth_disconnect($form, &$form_state) {
    variable_del('hubspot_access_token');
    variable_del('hubspot_refresh_token');
    variable_del('hubspot_expires_in');
  
}