<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
  <head>
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body style="margin: 10px;">
          <?php include('google_analytics.php'); ?>
  
          <?php if ($header): ?>
          <div id="header-blocks" class="region region-header">
            <?php print $header; ?>
          </div> <!-- /#header-blocks -->
          <?php endif; ?>
    
          <?php if ($breadcrumb || $title || $tabs || $help || $messages): ?>
          <div id="content-header">
            <?php print $messages; ?>
            <?php if ($tabs): ?>
            <div class="tabs"><?php print $tabs; ?></div>
            <?php endif; ?>
            <?php print $help; ?>
          </div> <!-- /#content-header -->
          <?php endif; ?>
    
    
          <div id="content-area">
            <?php print $content; ?>
          </div>
          
          <?php if ($footer): ?>
            <div id="footer"><div id="footer-inner" class="region region-footer">
              <?php print $footer; ?>
            </div></div> <!-- /#footer-inner, /#footer -->
          <?php endif; ?>
  
    <?php print $closure; ?>
  
  </body>
</html>
