<?php
// $Id: theme-settings.php,v 1.1 2010/10/26 23:22:22 aross Exp $ 

// Include the definition of phptemplate_settings().
include_once './' . drupal_get_path('theme', 'fusion_core') . '/theme-settings.php';


/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function fisdap2_help_settings($saved_settings) {

  /*
   * Create the form using Forms API: http://api.drupal.org/api/6
   */
  $form = array();

  $form['fisdap2_help_colors'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fisdap custom styles'),
    '#description' => t('Select the base colors for your site.  For the full range of site customization options,
      install the <a href="http://drupal.org/project/skinr">Skinr module</a> and set colors for blocks, nodes, and comments on the block and
      content type configuration pages.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );


  $form['fisdap2_help_colors']['fisdap2_help_corners'] = array(
    '#type'          => 'select',
    '#title'         => t('Rounded corners'),
    '#options'       => array(
      'default-corners' => t('None'),
      'round-corners-3' => t('3px radius corners'),
      'round-corners-7' => t('7px radius corners'),
      'round-corners-11' => t('11px radius corners'),
    ),
    '#description' => t('This will add rounded corners to blocks and some other
      elements.  Corners are CSS3-based in compliant browsers. To also display
      round corners in IE with CSS3 PIE <a href="http://css3pie.com/">(more info)
      </a>, follow instructions in README.txt'),
    '#default_value' => $saved_settings['fisdap2_help_corners'],
  );

  // Add the base theme's settings.
  $form += phptemplate_settings($saved_settings);

  // Remove some of the base theme's settings.
  // unset($form['themedev']['zen_layout']); // We don't need to select the base stylesheet.

  // Return the form
  return $form;
}
