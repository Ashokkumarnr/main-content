<?php

jquery_ui_add('ui.button');
jquery_ui_add('ui.accordion');

/**
 * Maintenance page preprocessing
 */
function fisdap2_help_preprocess_maintenance_page(&$vars) {
  fisdap2_help_preprocess_page($vars);
}

/**
 * Page preprocessing
 */
function fisdap2_help_preprocess_page(&$vars) {
  // are we on the support homepage?
  $vars['is_help_homepage'] = FALSE;
  if ($vars['node']->nid) {
 	if (drupal_get_path_alias('node/' . $vars['node']->nid) == 'support') {
		$vars['is_help_homepage'] = TRUE;
	}
  }

  // Set custom breadcrumb for help section
  // no breadcrumb if we're already on the main supprot page, though
  if ($vars['is_help_homepage']) {
  	$help_breadcrumbs = array();
  } else {
        // check if we have a student/instructor context set
        $options = array();
        if (isset($_GET['r'])) {
		$options['query'] = 'r=' . $_GET['r'];
        } else if (isset($_SESSION['fisdap_role'])) {
		$options['query'] = 'r=' . $_SESSION['fisdap_role'];
        }
  	$help_breadcrumbs = array(l('Support', 'support', $options));
  }
  if ($vars['node']->nid) {
	// check for a taxonomy term to put in the breadcrumb
	$support_tags_vocab_id = 11;
	foreach($vars['node']->taxonomy as $term) {
	  	if ($term->vid == $support_tags_vocab_id) {
			//$help_breadcrumbs[] = l($term->name, 'taxonomy/term/' . $term->tid);
			// no longer want linked-up terms
			$help_breadcrumbs[] = $term->name;
	  	}
	}
	$help_breadcrumbs[] = $vars['node']-> title;
  }
  $vars['help_breadcrumbs'] = '<span id="nav-bar-myfisdap">' . l('<< Back to MyFisdap', 'http://members.fisdap.net') . '</span> ' . implode(' &gt; ', $help_breadcrumbs);
  
  // Set grid info & row widths
  $grid_name = substr(theme_get_setting('theme_grid'), 0, 7);
  $grid_type = substr(theme_get_setting('theme_grid'), 7);
  $grid_width = (int)substr($grid_name, 4, 2);
  $vars['grid_width'] = $grid_name . $grid_width;
  
  // custom sidebar widths per page/path
  // JKM: I don't think we need this stuff in the support theme
  $sidebar_first_width = $sidebar_last_width = 0;
  /*  if (preg_match('/whats_new|open_airways/', request_uri())) {
	$sidebar_first_width = 3;
	$sidebar_last_width = 0;
  } else {
	$sidebar_first_width = ($vars['sidebar_first']) ? theme_get_setting('sidebar_first_width') : 0;
	$sidebar_last_width = ($vars['sidebar_last']) ? theme_get_setting('sidebar_last_width') : 0;
	} */
  
  $vars['sidebar_first_width'] = $grid_name . $sidebar_first_width;
  $vars['main_group_width'] = $grid_name . ($grid_width - $sidebar_first_width);
  // For nested elements in a fluid grid calculate % widths & add inline
  if ($grid_type == 'fluid') {
	$sidebar_last_width = round(($sidebar_last_width/($grid_width - $sidebar_first_width)) * 100, 2);
	$vars['content_group_width'] = '" style="width:' . (100 - $sidebar_last_width) . '%';
	$vars['sidebar_last_width'] = '" style="width:' . $sidebar_last_width . '%';
  }
  else {
	$vars['content_group_width'] = $grid_name . ($grid_width - ($sidebar_first_width + $sidebar_last_width));
	$vars['sidebar_last_width'] = $grid_name . $sidebar_last_width;
  }
  
  // Add body classes for custom design options
  $body_classes = explode(' ', $vars['body_classes']);
  $body_classes[] = theme_get_setting('fisdap2_help_corners');
  
   // Add body classes per page/path
  if (preg_match('/whats_new/', request_uri())) {
	$body_classes[] = 'whats-new';
  }
  
  $body_classes = array_filter($body_classes);   
  $vars['body_classes'] = implode(' ', $body_classes);
  
  // allow for page templates per content type
  if (isset($vars['node'])) {
	// If the node type is "blog" the template suggestion will be "page-blog.tpl.php".
	$vars['template_files'][] = 'page-'. str_replace('_', '-', $vars['node']->type);
  }
  
  // Replace page title as Drupal core does, but strip tags from site slogan.
  // Site name and slogan do not need to be sanitized because the permission
  // 'administer site configuration' is required to set and should be given to
  // trusted users only.
  if (drupal_get_title()) {
	// Site name first, then page title
    $head_title = array(variable_get('site_name', 'Drupal'), strip_tags(drupal_get_title()));
  }
  else {
    $head_title = array(variable_get('site_name', 'Drupal'));
    if (variable_get('site_slogan', '')) {
      $head_title[] = strip_tags(variable_get('site_slogan', ''));
    }
  }
  $vars['head_title'] = implode(' | ', $head_title);
}
