$(function() {
	//$( ".header-menu .inner ul.menu li a" ).button();
	
	// enable various jquery ui button styles (dependent on custom themes)
	$(".blue-button a").button();
	$(".dark-gray-button a").button();
	$(".gray-button a").button();
	$(".green-button a").button();
	$(".orange-button a").button();
	$(".yellow-button a").button();
	
	// use jquery ui button for search box submit
	$( ".inline_ajax_search_submit" ).button();
	
	// overwrite 'Home' in primary navigation menu with Fisdap logo
	//$("#primary-menu-inner ul.sf-menu > li.first:first-child").html('<a href="/" title="Home"><img src="/sites/all/themes/fisdap2/images/Fisdap_logo_new_small.png" style="height:35px;"/></a>');
	
	/* temporarily remove search block title
	  (normally this would be done in the block settings)
	*/
	$( "#block-inline_ajax_search-0 h2" ).html('');

});


// enable jquery ui accordion
$(function() {
	$( ".accordion-public" ).accordion();
});


// enable floating .stickyblock
$(function() {
	$(".stickyblock").stickyfloat({ duration: 250, lockBottom: false, startOffset: 210 });

});

$(function() {
	$( "input:submit, a, button", ".jq_button" ).button();
});


// override default Superfish behavior of Fusion core theme
$(function() {
  $("ul.sf-menu").superfish({
    hoverClass: 'sfHover',
    delay: 500,
    animation: {height:'show'},
	speed: 'fast',
    autoArrows: false,
    dropShadows: true,
    disableHI: true
  });
});


// enable mediaelement player
$(function() {
	$('video,audio').mediaelementplayer({
		pluginPath: '/sites/all/libraries/mediaelement/build/'
		});
});


// prevent return/enter key press on /support page (which otherwise goes to an unfiltered/confusing lucene node search page
$(function() {
  $(document).keypress(function(e) {
    if (e.keyCode == 13 && $(e.target).is("#node-940 #inline-ajax-search-form input")) {
      e.preventDefault();
    }
  });
});
