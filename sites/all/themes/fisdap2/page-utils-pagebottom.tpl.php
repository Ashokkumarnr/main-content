		</div><!-- /main-group -->
	  </div><!-- /main -->
	</div><!-- /main-wrapper -->
	
    <?php if ($footer): ?>
	<div id="footer-wrapper">
	  <div id="footer" class="footer row <?php print $grid_width; ?>">
		<?php print $footer; ?>
	  </div><!-- /footer -->
	</div><!-- /footer-wrapper -->
    <?php endif; ?>	
	
  </div><!-- /page -->
  <?php print $closure; ?>
</body>
