<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $setting_styles; ?>
  <!--[if IE 8]>
  <?php print $ie8_styles; ?>
  <![endif]-->
  <!--[if IE 7]>
  <?php print $ie7_styles; ?>
  <![endif]-->
  <!--[if lte IE 6]>
  <?php print $ie6_styles; ?>
  <![endif]-->
  <?php print $local_styles; ?>
  <?php print $scripts; ?>
</head>

<body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>">
   <div id="page" class="page" style="margin-top: 20px;">
	<div id="main-wrapper" class="main-wrapper full-width">
	  <div id="main" class="main row clearfix <?php print $grid_width; ?>">
		<div id="main-group" class="main-group row nested <?php print $main_group_width; ?>">
		  <div id="content-group" class="content-group row nested <?php print $content_group_width; ?>">
			<?php //print theme('grid_block', $breadcrumb, 'breadcrumbs'); ?>
			<!-- help and messages were here -->
			<a name="main-content-area" id="main-content-area"></a>
			<?php print theme('grid_block', $tabs, 'content-tabs'); ?>

			<div id="content-inner" class="content-inner block">
			  <div id="content-inner-inner" class="content-inner-inner inner">
			    <div class="left" style="width:550px; margin:15px 0px 0px 15px">
				<h1>Well this is awkward.</h1>
				<h3>You've caught us in the middle of an update.</h3>	

				<?php if ($content): ?>
				<div id="content-content" class="content-content" style="padding: 20px 0px;">
				  <?php print $content; ?>
				  <?php print $feed_icons; ?>
				</div><!-- /content-content -->
				<?php endif; ?>

				<div>
				  <img src="<?php print base_path() . path_to_theme(); ?>/images/Fisdap_logo_new_small.png" alt="<?php print t('Home'); ?>" class="left" style="padding:0px 8px 0px 0px">
				  <span class="header_4">Questions?
				    <br>Call 651.690.9241 or email 
				    <a href="mailto:support@fisdap.net">support@fisdap.net</a>
				  </span>
				</div>
			      </div>
			      
			      <img alt="" src="/sites/default/files/images/Bathing%20Robot.png" style="width:300px; margin:6px 0px 0px 25px; float: left;">
			  
			    </div><!-- /content-inner-inner -->
			</div><!-- /content-inner -->
		  </div><!-- /content-group -->

		</div><!-- /main-group -->
	  </div><!-- /main -->
	</div><!-- /main-wrapper -->

	<!-- postscript row: width = grid_width -->
    <?php print theme('grid_row', $postscript, 'postscript', 'full-width', $grid_width); ?>

	<!-- <div id="ff-break" style="width:980px; height: 10px; margin: 5px auto 0; background: repeat-x url('<?php print base_path() . path_to_theme(); ?>/images/footergrad.png');"></div> -->

	<?php if ($footer): ?>
	<div id="footer-wrapper">
	  <div id="footer" class="footer row <?php print $grid_width; ?>">
		<?php print $footer; ?>
	  </div><!-- /footer -->
	</div><!-- /footer-wrapper -->
    <?php endif; ?>
	
  </div><!-- /page -->
  <?php print $closure; ?>
</body>
</html>
