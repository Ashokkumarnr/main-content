<?php
/**
 * @file
 * Module admin page callbacks.
 */

//////////////////////////////////////////////////////////////////////////////
// fisdapauth settings

/**
 * Implements the settings page.
 *
 * @return
 *   The form structure.
 */
function fisdapauth_admin_settings() {
  $options_login_process = array(
    FISDAPAUTH_AUTH_MIXED => t('Mixed mode. FISDAP authentication is performed only if Drupal authentication fails'),
    FISDAPAUTH_AUTH_EXCLUSIVED => t('FISDAP database only')
  );
  $options_login_conflict = array(
    FISDAPAUTH_CONFLICT_LOG => t('Disallow login and log the conflict'),
    FISDAPAUTH_CONFLICT_RESOLVE => t('Associate local account with the FISDAP database entry')
  );
  $options_single_sign_out = array(
    FISDAPAUTH_SSOUT_ENABLE => t('Also sign out users from FISDAP during Drupal logout hook'),
    FISDAPAUTH_SSOUT_DISABLE => t('Only sign out users from Drupal')
  );

  $form['system-options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authentication mode'),
    '#description' => t('<strong>NOTE:</strong> These settings have no effect on Drupal user with uid 1. The admin account never uses FISDAP.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['system-options']['fisdapauth_login_process'] = array(
    '#type' => 'radios',
    '#title' => t('Choose authentication mode'),
    '#description' => t('Pick the mode based on the types of user accounts and other configuration decisions. If <i>FISDAP database only</i> option is activated some UI modications will be applied.'),
    '#default_value' => FISDAPAUTH_LOGIN_PROCESS,
    '#options' => $options_login_process,
    '#required' => TRUE,
  );
  $form['system-options']['fisdapauth_login_conflict'] = array(
    '#type' => 'radios',
    '#title' => t('Choose user conflict resolve procedure'),
    '#description' => t('Pick what should be done if the local Drupal account already exists with the same login name.'),
    '#default_value' => FISDAPAUTH_LOGIN_CONFLICT,
    '#options' => $options_login_conflict,
    '#required' => TRUE,
  );
  $form['system-options']['fisdapauth_single_sign_out'] = array(
    '#type' => 'radios',
    '#title' => t('Choose single sign-out behavior'),
    '#description' => t('Choose whether users who log out of Drupal should also be logged out of FISDAP.'),
    '#default_value' => FISDAPAUTH_SINGLE_SIGN_OUT,
    '#options' => $options_single_sign_out,
    '#required' => TRUE,
  );

  $form['security-options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Security Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['security-options']['fisdapauth_forget_passwords'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do not store users\' passwords during sessions'),
    '#default_value' => FISDAPAUTH_FORGET_PASSWORDS,
    '#description' => t('<p>***NOT IMPLEMENTED--LEAVE THIS BOX CHECKED***: If you use the <strong>fisdapdata</strong> module and want to allow users to modify their FISDAP attributes, you have two options:</p><ul><li>Setup a special fisdap manager user that has (limited) permissions to edit the requisite FISDAP records - using this method means Drupal\'s built in password reset will work;</li> <li>or allow this module to store the user\'s FISDAP password, in clear text, during the session;</li></ul><p>Physically, these passwords are stored in the Drupal\'s session table in clear text. This is not ideal and is not the recomended configuration.</p><p>Unless you need to use the latter configuration, leave this checked.</p>'),
  );
  $form['security-options']['fisdapauth_sync_passwords'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync FISDAP password with the Drupal password'),
    '#default_value' => FISDAPAUTH_SYNC_PASSWORDS,
    '#description' => t('***NOT IMPLEMENTED--LEAVE THIS BOX UNCHECKED***: If checked, then FISDAP and Drupal passwords will be syncronized. This might be useful if some other modules need to authenticate against the user password hash stored in Drupal and works only in Mixed mode. It might introduce security issues in the Mixed mode since after deletion of the FISDAP account user still be able to login to Drupal with his password. If unsure, leave this unchecked.'),
  );

  $form['fisdap-ui'] = array(
    '#type' => 'fieldset',
    '#title' => t('FISDAP UI Options'),
    '#description' => t('<p>Alters FISDAP users\' interface only, though admin accounts can still access email and password fields of FISDAP users regardless of selections. Does not effect non-FISDAP authenticated accounts. </p>'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['fisdap-ui']['fisdapauth_disable_pass_change'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove password change fields from user edit form'),
    '#default_value' => FISDAPAUTH_DISABLE_PASS_CHANGE,
    '#description' => t('<strong>NOTE:</strong> Request new password feature will be disabled for all users even for the user with uid 1.'),
  );
  $options_email_field = array(
    FISDAPAUTH_EMAIL_FIELD_NO => t('Do nothing'),
    FISDAPAUTH_EMAIL_FIELD_REMOVE => t('Remove email field from form'),
    FISDAPAUTH_EMAIL_FIELD_DISABLE => t('Disable email field on form'),
  );
  $form['fisdap-ui']['fisdapauth_alter_email_field'] = array(
    '#type' => 'radios',
    '#title' => t('Alter email field on user edit form'),
    '#description' => t('Remove or disable email field from user edit form for FISDAP authenticated users.'),
    '#default_value' => FISDAPAUTH_ALTER_EMAIL_FIELD,
    '#options' => $options_email_field,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['reset'] = array(
    '#type'  => 'submit',
    '#value' => t('Reset to defaults'),
  );
  return $form;
}


/**
 * Submit hook for the settings form.
 */
function fisdapauth_admin_settings_submit($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  $values = $form_state['values'];
  switch ($op) {
    case t('Save configuration'):
      variable_set('fisdapauth_login_process', $values['fisdapauth_login_process']);
      variable_set('fisdapauth_login_conflict', $values['fisdapauth_login_conflict']);
      variable_set('fisdapauth_forget_passwords', $values['fisdapauth_forget_passwords']);
      variable_set('fisdapauth_sync_passwords', $values['fisdapauth_sync_passwords']);
      variable_set('fisdapauth_disable_pass_change', $values['fisdapauth_disable_pass_change']);
      variable_set('fisdapauth_alter_email_field', $values['fisdapauth_alter_email_field']);
      variable_set('fisdapauth_single_sign_out', $values['fisdapauth_single_sign_out']);

      drupal_set_message(t('The configuration options have been saved.'));
      break;
    case t('Reset to defaults'):
      variable_del('fisdapauth_login_process');
      variable_del('fisdapauth_login_conflict');
      variable_del('fisdapauth_forget_passwords');
      variable_del('fisdapauth_sync_passwords');
      variable_del('fisdapauth_disable_pass_change');
      variable_del('fisdapauth_alter_email_field');
      variable_del('fisdapauth_single_sign_out');

      drupal_set_message(t('The configuration options have been reset to their default values.'));
      break;
  }

  // Rebuild the menu router.
  menu_rebuild();
}


/**
 * Implements the FISDAP admin  page.
 *
 * @return
 *   The themed HTML page.
 */
function fisdapauth_admin_menu_block_page() {
  return theme('admin_block_content', system_admin_menu_block(menu_get_item()));
}


?>