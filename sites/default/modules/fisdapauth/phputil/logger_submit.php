<?php

/*
* logger_submit.php - Log some client-side browser info
*/

$lognames = array('is_ie','is_fx','is_safari','is_otherbrowser','is_ie5_5','is_ie6','is_ie7','is_ie8','is_ie_other','is_winvista','is_winxp','is_winother','is_mac','is_otherplatform','screenDimension','timestamp','username');

$logarray[] = date("m-d-y H:i");
$logarray[] = $_SERVER["REMOTE_ADDR"]; // IP Address of remote machine
$logarray[] = $_POST['username'];
$logarray[] = round(microtime_float() - $_POST['timestamp'],4);
$logarray[] = $_POST['screenDimension'];

foreach ($lognames as $logname) {
	if ($logname != "screenDimension" and $logname != 'timestamp' and $logname != 'username') {
		$value = 0; if ($_POST[$logname] == 'true') $value = 1;
		$logarray[] = $value;
	}
}

$logline = implode(",",$logarray) . "\n";
error_log($logline,3,'/var/www/snifferlogs/browserlog');

#
# Put the IE Flag and Screen Resolution away into session space
#

$is_ie = false; if ($_POST['is_ie'] == 'true') $is_ie = true;
$_SESSION[SESSION_KEY_PREFIX . 'is_ie'] = $is_ie;

$unnamedScreenDimension = explode('X',$_POST['screenDimension']);
$screenDimension = array('width'=>$unnamedScreenDimension[0],'height'=>$unnamedScreenDimension[1]);
$_SESSION[SESSION_KEY_PREFIX . 'screenDimension'] = $screenDimension;

return "All Done";

/*
* FUNCTION microtime_float - Get the time in microseconds
*/

function microtime_float() {

	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);

}

?>
