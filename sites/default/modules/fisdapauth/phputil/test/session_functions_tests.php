<?php

require_once('phputil/session_functions.php');

/**
 *
 */
class SessionFunctionsTester extends UnitTestCase {
    function setUp() {
    }//setUp

    function test_get_initial_target() {
        // some valid ways to call shift/index.html, a known whitelist entry
        $path_1 = 'shift/index.html';
        $test_url_1 = '/production/session_auth/FISDAP/' . $path_1;
        $this->assertEqual($path_1, get_initial_target($test_url_1));

        $path_2 = 'shift/index.html#test';
        $test_url_2 = '/~smartin/FISDAP/' . $path_2;
        $this->assertEqual($path_2, get_initial_target($test_url_2));

        $path_3 = 'shift/index.html?foo=foo';
        $test_url_3 = '/' . $path_3;
        $this->assertEqual($path_3, get_initial_target($test_url_3));

        $path_4 = 'shift/index.html?foo=foo&bar=bar';
        $test_url_4 = '/' . $path_4;
        $this->assertEqual($path_4, get_initial_target($test_url_4));

        $path_5 = 'shift/index.html?foo=foo&bar=bar#stuff';
        $test_url_5 = '/' . $path_5;
        $this->assertEqual($path_5, get_initial_target($test_url_5));

        // try a couple bogus urls
        $bogus_url_1 = '/shift/evals/delete_stuff.php';
        $bogus_url_2 = '/../../invalid_path/foo.html?foo=foo#test';

        $this->assertFalse(get_initial_target($bogus_url_1));
        $this->assertFalse(get_initial_target($bogus_url_2));
    }//test_get_initial_target
}//class SessionFunctionsTester

?>
