<?php

require_once('phputil/quick_books_utils.php');

/**
 *
 */
class QuickbooksTester extends UnitTestCase {
    function setUp() {
    }//setUp

    function testQuickbooksUtils() 
    {
//        $FileString;
//        $GeneratedString;
//        $TestFileString;
//        $KnownFileString;
//        $FilePntr;

        $TestFileName = FISDAP_ROOT .'test/KnownFiles/QuickbooksTestFile.iif';
        $KnownFileName = FISDAP_ROOT . 'test/KnownFiles/quickbooks_import.iif';
        
        // Generate the test file
        $FilePntr = QB_OpenFile( $TestFileName);
        QB_WriteHeaders( $FilePntr);
        
     // First Transaction
        QB_WriteTransaction( 
            $FilePntr, 
            '1/11/2007', 								// Date
            QB_CREDIT_CARD_ACCOUNT,		// Account
            '409-Denver Health & Hospital',	// Customer
            80.0, 										// Ammount
            'VXJE0DE57865-Dorsey Allison',	// Verisign Transaction ID
            'Visa');										// Payment Type
            
        QB_WriteDistribution( 
            $FilePntr, 
            '1/11/2007', 								// Date        
            QB_ITEM_TRACKING_ALS,			// Item Name
            55.00,										// Price
            1,												// Quanity
            QB_SALES_TRACKING_ALS );        // Sales account to book split
            
        QB_WriteDistribution( 
            $FilePntr, 
            '1/11/2007', 								// Date        
            QB_ITEM_SCHEDULER_ALS,			// Item Name
            25.00,										// Price
            1,												// Quanity
            QB_SALES_SCHEDULER_ALS);       // Sales account to book split       

        QB_EndTransaction( $FilePntr );
        
     // Second Transaction
        QB_WriteTransaction( 
            $FilePntr, 
            '1/11/2007', 								// Date
            QB_CREDIT_CARD_ACCOUNT,		// Account
            '410-Inver Hills Comm College',	// Customer
            80.00, 										// Ammount
            'VXJE0DE57865-Dorsey Allison',	// Verisign Transaction ID
            'Visa');										// Payment Type
            
        QB_WriteDistribution( 
            $FilePntr, 
            '1/11/2007', 								// Date        
            QB_ITEM_TRACKING_ALS,			// Item Name
            55.00,										// Price
            1,												// Quanity
            QB_SALES_TRACKING_ALS );        // Sales account to book split
            
        QB_WriteDistribution( 
            $FilePntr, 
            '1/11/2007', 								// Date        
            QB_ITEM_SCHEDULER_ALS,			// Item Name
            25.00,										// Price
            1,												// Quanity
            QB_SALES_SCHEDULER_ALS);       // Sales account to book split       

        QB_EndTransaction( $FilePntr );        
            
      // Close the file  
        QB_CloseFile( $FilePntr ); 
                
    // Get contence of test file
        $GeneratedFileString = file_get_contents($TestFileName );
        
    // Get contents of Known file
        $KnownFileString = file_get_contents($KnownFileName );
        
    // Compare strings
        $this->assertEqual( $KnownFileString, $GeneratedFileString);
 
    }//test_str_ends_with
}//class StrEndsWithTester

?>
