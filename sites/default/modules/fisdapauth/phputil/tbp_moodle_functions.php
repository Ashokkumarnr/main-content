<?php
//!-------------------------------------------------------------------------->
//!--                                                                      -->
//!--      Copyright (C) 1996-2006.  This is an unpublished work of        -->
//!--                       Headwaters Software, Inc.                      -->
//!--                          ALL RIGHTS RESERVED                         -->
//!--      This program is a trade secret of Headwaters Software, Inc.     -->
//!--      and it is not to be copied, distributed, reproduced, published, -->
//!--      or adapted without prior authorization                          -->
//!--      of Headwaters Software, Inc.                                    -->
//!--                                                                      -->
//!-------------------------------------------------------------------------->

/**
 * tbp_moodle_functions.php - functions for working with blueprints and moodle tests
 */

require_once('phputil/classes/common_user.inc');
require_once('phputil/classes/common_utilities.inc');
require_once('phputil/classes/common_report.inc');
require_once('phputil/session_data_functions.php');
require_once('phputil/test_data_functions.php');

/*
 * procedure: getMoodleAttempt
 * parameters: $quiz: the id of the quiz in moodle to get the attempt
 * 			   $user: the moodle id of the user in question
 *			   $db: a mysql link resource
 *			   $unix_start_date: the unix timestamp for the start date
 *			   $unix_end_date: the unix timestamp for the end date, zero if open-ended
 * produces: 
 *   a) id of the attempt of the quiz by the given user
 *   - or -
 *   b) false on falure
 */
function getMoodleAttempt($quiz, $user, $db, $unix_start_date, $unix_end_date, $pre='dpmdl') {
	if ($unix_end_date > 0) {
		$endclause = "AND timefinish < $unix_end_date ";
	} else {
		$endclause = "";
	}
	$select = "SELECT id ".
		"FROM ".$pre."_quiz_attempts ".
		"WHERE userid=$user ".
		"AND quiz=$quiz ".
		"AND timefinish > $unix_start_date ".
		$endclause.
		"ORDER BY id DESC";
	//echo "select: $select<br>";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count>0) {
		return mysql_result($result, 0, "id");
	} else {
		return false;
	}
}

/*
 * procedure: getAttemptUser
 * parameters: $attempt_id: the id of the moodle quiz attempt
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) the moodle id of the user who made the attempt
 *   - or -
 *   b) false on falure
 */
function getAttemptUser($attempt_id, $db, $pre='dpmdl') {
	$select = "select userid from ".$pre."_quiz_attempts where id=$attempt_id";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count==1) {
		return mysql_result($result, 0, "userid");
	} else {
		return false;
	}
}

/*
 * procedure: getAttemptTime
 * parameters: $attempt_id: the id of the moodle quiz attempt
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) the unix time stamp of when the attempt was begun
 *   - or -
 *   b) false on falure
 */
function getAttemptTime($attempt_id, $db, $pre='dpmdl') {
	$select = "select timestart from ".$pre."_quiz_attempts where id=$attempt_id";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count==1) {
		return mysql_result($result, 0, "timestart");
	} else {
		return false;
	}
}

/*
 * procedure: getMoodleItemGrade
 * parameters: $attempt: id of the attempt of the quiz that the item in question is tied to
 * 			   $item: the moodle item in question
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) grade for the given item for the given attempt
 *   - or -
 *   b) false on falure
 */
function getMoodleItemGrade($attempt, $item, $db, $pre='dpmdl') {
	if ($pre=='dpmdl') {
		$select = "SELECT grade FROM ".$pre."_quiz_states WHERE attempt=$attempt AND question=$item AND event=6";
	} else {
		$select = "SELECT grade FROM ".$pre."_question_states WHERE attempt=$attempt AND question=$item AND event IN (0, 3, 6, 9)";
	}
	//echo "select is: $select<br>\n";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count==1) {
		$grade = mysql_result($result, 0, "grade");
		//echo "Item Grade is $grade<br>\n";
		return $grade;	
	} else {
		//echo "returning false<br>";
		return false;
	}
}

function getMoodleItemGrade_2($attempt, $item, $db, $pre='dpmdl') {
	$select = "SELECT grade FROM ".$pre."_question_states WHERE attempt=$attempt AND question=$item AND event=6";
	//echo "select is: $select<br>\n";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count==1) {
		return mysql_result($result, 0, "grade");
	} else {
		return false;
	}
}


/*
 * procedure: getMoodlePointsArray
 * parameters: $attempt: the id of the student's quiz attempt
 *			   $quiz: the id of the moodle quiz in question
 * 			   $ItemIDs: an associative array of the moodle and fisdap ids of the questions in question
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) an associative array of the ids, granted points and total points possible for the 
 * 		given questions in the given quiz
 *   - or -
 *   b) false on failure
 */
function getMoodlePointsArray($attempt, $quiz, $questionArray, $db, $pre='dpmdl') {
	$MoodleIDs = array_keys($questionArray);
	//var_export($questionArray);
	$MoodleIDStr = implode(",", $MoodleIDs);

	/** 
	 * get the possible grade
	 */
	$select = "SELECT question, grade ".
		"FROM ".$pre."_quiz_question_instances ".
		"WHERE quiz=$quiz ".
		"AND question IN ($MoodleIDStr)";
	//echo "select is: $select<br>\n";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count==count($MoodleIDs)) {
		for ($i=0;$i<$count;$i++) {
			$question = mysql_result($result, $i, "question");
			$possible = mysql_result($result, $i, "grade");
			$questionArray[$question]['possible'] = $possible;
			$questionArray[$question]['grade'] = 0;
		}
	} else {
		return false;
	}

	/**
	 * get the student's grade
	 */
	if ($pre=='dpmdl') {
		$select = "SELECT question, grade ".
			"FROM ".$pre."_quiz_states ".
			"WHERE attempt=$attempt ".
			"AND question IN ($MoodleIDStr) ".
			"AND event=6";
	} else {
		$select = "SELECT question, grade ".
			"FROM ".$pre."_question_states ".
			"WHERE attempt=$attempt ".
			"AND question IN ($MoodleIDStr) ".
			"AND event IN (0, 3, 6, 9) ".
			"ORDER BY timestamp";
	}
	//echo "select is: $select<br>\n";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count > 0) {
		for ($j=0;$j<$count;$j++) {
			$question = mysql_result($result, $j, "question");
			$grade = mysql_result($result, $j, "grade");
			$questionArray[$question]['grade'] = $grade;
		}
	} else {
		return false;
	}

	return $questionArray;
}

/*
 * procedure: getMoodlePossiblePoints
 * parameters: $quiz: the id of the moodle quiz in question
 * 			   $question: the id of the question in question
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) the total points possible for the given question in the given quiz
 *   - or -
 *   b) false on failure
 */
function getMoodlePossiblePoints($quiz, $question, $db, $pre='dpmdl') {
	$select = "SELECT grade FROM ".$pre."_quiz_question_instances WHERE quiz=$quiz AND question=$question";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count==1) {
		$grade = mysql_result($result, 0, "grade");
		//echo "grade is: $grade<br>\n";
		return $grade;
	} else {
		//echo "returning false quiz: $quiz and queston $question<br>\n";
		return false;
	}
}

/*
 * procedure: getBPSectionItems
 * parameters: $section: the blueprint section to be considered
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) an array of the Items that are in the given section
 *   - or -
 *   b) false on falure
 */
function getBPSectionItems($section, $db) {
	$retArray = array();
	$select = "SELECT I.Item_id FROM TestBPItems I, TestBPTopics T WHERE T.tbpSect_id=$section AND T.tbpTopic_id=I.tbpTopic_id";
	//	echo "select is: $select<br>\n";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	//echo "count is: $count<br>\n";
	if ($count > 0) {
		for ($i=0;$i<$count;$i++) {
			array_push($retArray, mysql_result($result, $i, "Item_id"));
		}
		return $retArray;
	} else {
		return false;
	}
}

/*
 * procedure: getItemMoodleIDs
 * parameters: $items: an array of the item ids in the asset tracking system
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) an array of the moodle ids associated with the given item
 *   - or -
 *   b) false on failure
 */
function getItemMoodleIDs($items, $quiz, $db, $pre='dpmdl', $database='moodle') {
	$itemStr = implode(",", $items);
	$select = "SELECT Moodle_id ".
		"FROM ItemMoodleMap ".
		"WHERE Item_id IN ($itemStr) ".
		"AND MoodleQuiz_id=$quiz ".
		"AND MoodleDBName='$database' ".
		"AND MoodleDBPrefix='$pre' ".
		"ORDER BY Item_id";
	//echo "select is $select<br>\n";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	//	echo "count $count items ". print_r($items);
	if ($count==count($items)) {
		for ($i=0;$i<$count;$i++) {
			$moodleIDs[] = mysql_result($result, $i, "Moodle_id");
		}
		//var_export($moodleIDs);
		return $moodleIDs;
	} else {
		return false;
	}
}

/*
 * procedure: getItemMoodleID
 * parameters: $item: the item id in the asset tracking system
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) the moodle id associated with the given item
 *   - or -
 *   b) false on falure
 */
function getItemMoodleID($item, $quiz, $db, $pre='dpmdl', $database='moodle') {
	$select = "SELECT Moodle_id FROM ItemMoodleMap WHERE Item_id=$item AND MoodleQuiz_id=$quiz AND MoodleDBName='$database' AND MoodleDBPrefix='$pre'";
	//echo "select is $select<br>\n";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count==1) {
		return mysql_result($result, 0, "Moodle_id");
	} else {
		return false;
	}
}

/*
 * procedure: getBPSections
 * parameters: $blueprint: the blueprint id of the blueprint in question
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) an array of section ids of the sections in the givem blueprint
 *   - or -
 *   b) false on falure
 */
function getBPSections($blueprint, $db) {
	$retArray = array();
	$select = "SELECT tbpSect_id FROM TestBPSections WHERE tbp_id=$blueprint";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count > 0) {
		for ($i=0;$i<$count;$i++) {
			array_push($retArray, mysql_result($result, $i, "tbpSect_id"));
		}
		return $retArray;
	} else {
		return false;
	}
}

/*
 * procedure: getSectionName
 * parameters: $sectionid: the ID of the blueprint section in question
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) the name of the section with the id given
 *   - or -
 *   b) false on falure
 */
function getSectionName($sectionid, $db) {
	$select = "SELECT Name FROM TestBPSections WHERE tbpSect_id=$sectionid";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count==1) {
		$name = mysql_result($result, 0, "Name");
		//	echo "Name is $name<br>\n";
		return $name;
	} else {
		return false;
	}
}

/*
 * procedure: show_score_details
 * parameters: $test_id: the Moodle quiz ID of the test in question
 *             $db: a mysql link resource
 *
 * produces: 
 *   a) true if there is an associated blueprint formatted to show score details
 *   - or -
 *   b) false if not
 */
function show_score_details($test_id, $db) {
	$select = "SELECT showDetails FROM MoodleTestData WHERE MoodleQuiz_id=$test_id";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count==1) {
		$showDetails = mysql_result($result, 0, "showDetails");
		if ($showDetails==1) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

/*
 * procedure: getMoodleUserID
 * parameters: $username: the moodle user name of the user in question
 *			   $db: a mysql link resource
 *
 * produces: 
 *   a) the moodle id of the user with the given user name
 *   - or -
 *   b) false on falure
 */
function getMoodleUserID($username, $db, $pre='dpmdl') {
	$select = "SELECT id from ".$pre."_user WHERE username='$username'";
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	if ($count==1) {
		return mysql_result($result, 0, "id");
	} else {
		return false;
	}
}

function get_real_moodle_quiz_id($exam_id) {
	$connection = &FISDAPDatabaseConnection::get_instance(); // Connect to MySQL Database
	$select = "SELECT MoodleIDMod from MoodleTestData where MoodleQuiz_id=$exam_id";
	$result = $connection->query($select);
	$mod_val = $result[0]["MoodleIDMod"];
	$retval = $exam_id - $mod_val;
	return $retval;
}

function get_result_array($username, $exam, $unix_start_date, $unix_end_date, $attemptid = -15) {
	$connection = FISDAPDatabaseConnection::get_instance(); // Connect to MySQL Database
	$dbConnect = $connection->get_link_resource(); // Represents the connection itself

	/**
	 * Figure out which exam we're working with
	 */
	$examAr = explode(":", $exam);
	$blueprint_id=$examAr[0];
	$exam_id=$examAr[1];
	$exam_name=$examAr[2];

	$exam_info = get_moodle_database($exam_id, $exam_name);

	$database_name=$exam_info["MoodleDatabase"];
	$database_prefix=$exam_info["MoodlePrefix"];

	$real_quiz_id = get_real_moodle_quiz_id($exam_id);

	/**
	 * Get the list of sections for this exam, and each section's name, associated ItemIds and 
	 * associated Moodle Item Ids
	 */
	$sectionList = getBPSections($blueprint_id, $dbConnect);
	$sectionInfo = array();
	$num_sections = count($sectionList);
	for ($a=0;$a<$num_sections;$a++) {
		$sectionInfo[$a]['ID'] = $sectionList[$a];
		$sectionInfo[$a]['Name'] = getSectionName($sectionList[$a], $dbConnect);
		$tmpFISDAPItemArray = getBPSectionItems($sectionList[$a], $dbConnect);
		//print_r($tmpFISDAPItemArray);
		sort($tmpFISDAPItemArray);
		$sectionInfo[$a]['FISDAPItemArray'] = $tmpFISDAPItemArray;
		$sectionInfo[$a]['MoodleItemArray'] = getItemMoodleIDs($sectionInfo[$a]['FISDAPItemArray'], $real_quiz_id, $dbConnect, $database_prefix, $database_name);
		$num_items = count($sectionInfo[$a]['FISDAPItemArray']);	
		for ($i=0;$i<$num_items;$i++) {
			if ($sectionInfo[$a]['MoodleItemArray'][$i]) {
				$assocMoodleID = $sectionInfo[$a]['MoodleItemArray'][$i];
				$sectionInfo[$a]['ItemArray'][$assocMoodleID]['FISDAPItemID'] = $sectionInfo[$a]['FISDAPItemArray'][$i];
			} else {
				//echo "error 1<br>";
				return false;
			}
		}
	}
	//var_export($sectionInfo);

	$results = array();

	/**
	 * SWITCH TO MOODLE DATABASE
	 * then get the student's moodle account info
	 */

	if (!mysql_select_db($database_name, $dbConnect)) {
		echo mysql_errno().": Cannot select database: ".mysql_error()."<BR>";
		//	echo "error 2<br>";
		return false;
	}

	$select = "SELECT id, firstname, lastname, email ".
		"FROM ".$database_prefix."_user ".
		"WHERE username='" . addslashes($username) . "' " .
		"ORDER BY lastname";
	$result = mysql_query($select, $dbConnect);
	$count = mysql_num_rows($result);
	if ($count==1) {
		$tmp_id =  mysql_result($result, 0, "id");
		$firstname = mysql_result($result, 0, "firstname");
		$lastname = mysql_result($result, 0, "lastname");
		$email = mysql_result($result, 0, "email");

		$totalScore = 0;
		$totalScorePossible = 0;
		$pointsreceived = 0;

		$results['id'] = $tmp_id;
		$results['lastname'] = $lastname;
		$results['firstname'] = $firstname;
		$results['email'] = $email;
	} else {
		mysql_select_db('FISDAP', $dbConnect);
		//		echo "error 3</br>";
		return false;
	}

	/**
	 * Get the student's most recent attempt at the quiz (or the requested attempt), 
	 * and report when the attempt happened
	 * 
	 */
	if ($attemptid > 0) { 
		$tmpAttempt = $attemptid;
		// make sure the attempt user is the same as the requested user
		$attemptUser = getAttemptUser($tmpAttempt, $dbConnect, $database_prefix);
		if ($tmp_id != $attemptUser) {
			mysql_select_db('FISDAP', $dbConnect);
			//			echo "error 4";
			return false;
		}
	} else { 
		$tmpAttempt = getMoodleAttempt($real_quiz_id, $tmp_id, $dbConnect, $unix_start_date, $unix_end_date, $database_prefix); 
	}
	if ($tmpAttempt) {
		$AttemptTime = getAttemptTime($tmpAttempt, $dbConnect, $database_prefix);
		$results['attempt_id'] = $tmpAttempt;
		$results['time'] = $AttemptTime;

		/**
		 * loop through the sections, getting the grades for that section
		 */
		$num_sections = count($sectionInfo);
		for ($a=0;$a<$num_sections;$a++) {
			$sectScore=0;
			$sectPossible=0;
			$sectItemArray = $sectionInfo[$a]['ItemArray'];
			$tmpSectionName = $sectionInfo[$a]['Name'];
			$itemPoints = getMoodlePointsArray($tmpAttempt, $real_quiz_id, $sectItemArray, $dbConnect, $database_prefix);
			if (!$itemPoints) {
				mysql_select_db('FISDAP', $dbConnect);
				//				echo "error 5";
				return false;
			}
			/**
			 * Loop through each item in the section, getting the grade
			 */
			foreach ($itemPoints as $MoodleId => $MoodleItem) {
				$itemPossible = $MoodleItem['possible'];
				$itemScore = $MoodleItem['grade'];
				$sectPossible += $itemPossible;
				$sectScore += $itemScore;

				/**
				 * If the student got the answer wrong, add it to the incorrect array for the
				 * section and overall
				 * NOTE: we may want to change how this is done if we ever assign per-item cut scores
				 */
				if ($itemScore < $itemPossible) {
					$results[$tmpSectionName.'_incorrect'][] = $MoodleItem['FISDAPItemID'];
					$results['total_incorrect'][] = $MoodleItem['FISDAPItemID'];
				}
			}

			/**
			 * store the section score, and add section scores to overall score
			 */
			$results[$tmpSectionName] = array("score"=>$sectScore, "possible"=>$sectPossible);
			$totalScore += $sectScore;
			$totalScorePossible += $sectPossible;

		} // section loop
	} else {
		mysql_select_db('FISDAP', $dbConnect);
		//		echo "error 5.5<br>";
		return false;
	}

	/**
	 * Format and report the overall score
	 */
	$results['totalscore'] = $totalScore.' / '.$totalScorePossible;
	$results['totalpercentage'] = 100*(round(($totalScore / $totalScorePossible), 2));

	/**
	 * SWITCH BACK TO FISDAP DATABASE
	 */
	if (!mysql_select_db('FISDAP', $dbConnect)) {
		echo mysql_errno().": Cannot select database: ".mysql_error()."<BR>";
		//		echo "error 6<br>";
		return false;
	}
	return $results;
}

function print_score_table($test, $UnixStartDate, $UnixEndDate, $studentArray, $scheduled_test_id=null) {
	$connection = FISDAPDatabaseConnection::get_instance(); // Connect to MySQL Database 
	$dbConnect = $connection->get_link_resource(); // Represents the connection itself

	$StartDate = date('n/j/Y', $UnixStartDate);
	$EndDate = date('n/j/Y', $UnixEndDate);

	/**
	 * Figure out which test we're working with
	 */
	$testAr = explode(":", $test);
	$blueprint_id=$testAr[0];
	$test_id=$testAr[1];
	$test_name=$testAr[2];
	$showDetails = show_score_details($test_id, $dbConnect);
	//	echo "test: $test<br>";
	//	echo "blueprint: $blueprint_id<br>";
	//	echo "name: $test_name<br>";

	$real_quiz_id = get_real_moodle_quiz_id($test_id);

	/**
	 * Get the list of sections (section ids) for this test
	 */
	$sectionList = getBPSections($blueprint_id, $dbConnect);

	/**
	 * Print out table header
	 */
	echo "<div class='tableHeader'>\n";
	echo "<div class='tableTitle largetext'>Results for $test_name, $StartDate-$EndDate</div>\n";
	echo "<div class='statsLink mediumtext'>\n";
	if (file_exists('stats/' . $test_name . '_stats.pdf')) {
		echo "<a href='stats/".$test_name."_stats.pdf' target='_BLANK'>test stats</a>\n";
		echo common_utilities::get_bubblelink('legacy', 'test_stats', null);
	}
	echo "</div>\n"; // statsLink

	/**
	 * Create hidden printForm for posting data to printableResults
	 */
	echo "<div class='printLinks'>\n";
	echo "<form name='printForm' method='post' action='printableResults.html'>\n";
	echo "<input type='hidden' name='destination'>\n";
	echo "<input type='hidden' name='test' value='$test'>\n";
	echo "<input type='hidden' name='UnixStartDate' value='$UnixStartDate'>\n";
	echo "<input type='hidden' name='UnixEndDate' value='$UnixEndDate'>\n";
	echo "<input type='hidden' name='studentArray' value='".serialize($studentArray)."'>\n";
	echo "<a href='Javascript:document.printForm.destination.value=\"pdf\";document.printForm.submit();' ";
	echo "title='Printer Friendly (PDF)'>\n";
	echo "<img src='../images/pdf_icon.png'></a>\n";
	echo "<a href='Javascript:document.printForm.destination.value=\"text\";document.printForm.submit();' ";
	echo "title='Spreadsheet Friendly (Tab Delimited Text)'>\n";
	echo "<img src='../images/text_icon.png'></a>\n";
	if ($showDetails) { // if apprpropriate, create the print learning prescription(s) link
		echo "<a href='Javascript:document.printForm.destination.value=\"prescription\";document.printForm.submit();' ";
		echo "title='All Learning Prescriptions (PDF)'>\n";
		echo "<img src='../images/icon_rx_pdf.png'></a>\n";
	}
	echo "</form>\n";
	echo "</div>\n"; // printLinks
	echo "</div>\n"; // tableHeader

	echo "<div class='clear'></div>\n";

	//	echo "Click on a student's name to email his or her results. ";
	//	if ($showDetails) {
	//		echo "Click on a student's score to view his or her learning prescription.";
	//	}
	echo "<table id='results' class='sortable mediumtext'>";
	echo "<tr>\n";
	echo "<th>Email<br>Score</th>\n";
	echo "<th>Last Name</th>\n";
	echo "<th>First Name</th>\n";
	/**
	 * Print column headings for each section of the selected test
	 */
	if ($sectionList) {
		$num_sections = count($sectionList);
		for ($a=0;$a<$num_sections;$a++) {
			$tmpSectName=getSectionName($sectionList[$a], $dbConnect);
			if ($tmpSectName) {
				echo "<th>$tmpSectName</th>\n";
				$tmpSect_id = $sectionList[$a];
				$sectionList[$a] = array();
				$sectionList[$a]['Section_id'] = $tmpSect_id;
				$sectionList[$a]['Name'] = $tmpSectName;
				$sectionList[$a]['Average'] = 0; // reset the score tabulation to calc averages later
				$sectionList[$a]['Possible'] = 0; // reset the score tabulation to calc averages later
			} else {
				echo "Unable to find name for section " . $sectionList[$a] . "<br>\n";
				exit;
			}
		}
	} else {
		echo "No sections found for blueprint $tbp_id<br>\n";
		exit;
	}
	echo "<th>Total</th>\n";
	if ($showDetails) { // if apprpropriate, provide details column
		echo "<th>Learning<br>Pres.</th>\n";
	}
	echo "</tr>\n";
	$numCols = count($sectionList) + 4;
	if ($showDetails) { // if apprpropriate, stretch the hr
		$numCols++;
	}

	/**
	 * get the results for each student, keeping track
	 * of the students whose results came back false (meaning they
	 * didn't take that test within the time frame
	 */
	$nullStudents = array();
	$count = 0;
	$Total_Average = 0; // reset the score tabulation to calc averages later
	$Total_Possible = 0; // reset the score tabulation to calc averages later

	if ($showDetails) { // if apprpropriate, create the group learning prescription array
		$group_incorrect_items = array();
		$group_id = md5($test_id.mktime());
		$group_incorrect_items['group_id'] = $group_id;
		$group_incorrect_items['test_id'] = $test_id;
		$group_incorrect_items['date'] = "$StartDate - $EndDate";
	}

	// STUDENT LOOP
	$num_students = count($studentArray);
	for ($a=0;$a<$num_students;$a++) {
		$username = get_student_username_by_id($studentArray[$a]);
		$results = get_result_array($username, $test, $UnixStartDate, $UnixEndDate);	
		if ($results) {
			$tmpStudent = new common_student($studentArray[$a]);
			$results['firstname'] = $tmpStudent->get_firstname();
			$results['lastname'] = $tmpStudent->get_lastname();
			$results['email'] = $tmpStudent->get_emailaddress();
			$testDate = $results['time'];
			//var_export($results);
			// only do the work if the student's results are within the date range
			if ( $testDate>$UnixStartDate && $testDate<$UnixEndDate ) {
				$date = date("F jS, Y", $testDate);
				$email = $results['email'];
				$attempt_id = $results['attempt_id'];
				$firstname = stripslashes($results['firstname']);
				$lastname = stripslashes($results['lastname']);
				/** 
				 * create the email message containing the scores
				 */
				$subject = "Your $test_name scores";
				$message  = "Dear $firstname,<br><br>";
				$message .=	"Here are your scores for the $test_name, ";
				$message .= "which you took on $date: <br><br>";
				if ($sectionList) {
					for ($i=0;$i<$num_sections;$i++) {
						$tmpSectName=$sectionList[$i]['Name'];
						if ($tmpSectName) {
							$sectScore = $results[$tmpSectName]['score'];
							$sectPossible = $results[$tmpSectName]['possible'];
							$tmppercentage = 100*(round(($sectScore / $sectPossible), 2));
							$score_str = $results[$tmpSectName]['score']." / ".$results[$tmpSectName]['possible'].
								"<br>".$tmppercentage."%";
							$message .= "$tmpSectName: $score_str <br><br>";
						}
					}
				}
				$message .= "Total Score: ".$results['totalscore']."<br>";
				$message .= $results['totalpercentage']."% <br><br>";

				//&#39 = apostrophe
				//str_replace  ( mixed $search  , mixed $replace  , mixed $subject  ) {
				$message = str_replace("'", "&#39", $message);

				$message = addslashes($message);


				/**
				 * print out the student's results
				 */
				echo "<tr class='resultRow" . ($count%2) . "'>";
				echo '<td style="text-align:center">';
				echo '<a href="javascript:;" onclick=\'post_to_sendScores("'.$email.'", "'.$subject.'", "'.$message.'")\' ';
				//echo "<a href=\"javascript:;\" onclick="post_to_sendScores(\''.$email.'\', \''.$subject.'\', \''.$message.'\')' ";
				echo 'title="Email Score">';
				echo '<img style="border-style:none" src="../images/Email.png"></a></td>';
				echo '<td>'.$lastname.'</td>';
				echo '<td>'.$firstname.'</td>';
				// Print the results for each section
				if ($sectionList) {
					for ($i=0;$i<$num_sections;$i++) {
						$tmpSectName=$sectionList[$i]['Name'];	
						if ($tmpSectName) {
							// format the score
							$sectScore = $results[$tmpSectName]['score'];
							$sectPossible = $results[$tmpSectName]['possible'];
							$tmppercentage = 100*(round(($sectScore / $sectPossible), 2));
							$score_str = $results[$tmpSectName]['score']." / ".$results[$tmpSectName]['possible'].
								"<br>".$tmppercentage."%";

							echo "<td>".$score_str."</td>\n";

							if ($showDetails) { 
								//add missed items to the group prescription
								if (count($results[$tmpSectName.'_incorrect'])>0) {
									foreach ($results[$tmpSectName.'_incorrect'] as $incorrect_item) {
										$group_incorrect_items[$tmpSectName][$incorrect_item][] = $studentArray[$a];
									}
								}
							}

							// add the student's section score to the average for that section
							$sectionList[$i]['Average'] = $sectionList[$i]['Average'] + $sectScore;
							$sectionList[$i]['Possible'] = $sectPossible;
						} else {
							echo "<td>N/A</td>\n";
						}
					}
				}
				// Print the student's overall results
				echo "<td>\n";
				echo $results['totalscore']."<br>\n";
				echo $results['totalpercentage']."%\n";
				//				}
				echo "</td>\n";

				if ($showDetails) { // if appropriate, provide learning prescription link
					echo '<td style="text-align:center">';
					echo "<a href='scoreDetails.html?student=$studentArray[$a]&attempt_id=$attempt_id&test_id=$test_id' ";
					echo 'title="View Learning Prescription" ';
					echo "target='_blank'>\n";
					echo '<img style="border-style:none" src="../images/icon_rx.png"></a></td>';
				}
				echo "</tr>";

				// add the student's overall score to the average
				$Total_Array = explode(" / ", $results['totalscore']);
				$Total_Average = $Total_Average + $Total_Array[0];
				$Total_Possible = $Total_Array[1];

				$count++;
			} else {		
				$nullStudents[] = $username;
			}
		} else {
			$nullStudents[] = $username;
		}
	} // end student loop
	if ($count == 0) {
		echo "<tr class='warning'><td align='center' colspan=$numCols>\n";
		echo "No results found for the given criteria.\n";
		echo "</td></tr>";
	}

	if ($count > 0) {

		/**
		 * Print out the averages row for this group of students
		 */
		$cols = count($sectionList)+4;
		if ($showDetails) { // if apprpropriate, stretch the hr
			$cols++;
		}
		echo "<tr><td colspan=$cols style='padding:0px;margin:0px;'><hr></td></tr>\n";
		echo "<tr><td colspan=3>Group Average:</td>\n";
		foreach ($sectionList as $tmp_section) {
			$tmpSectName=$tmp_section['Name'];	
			$tmp_avg_score = round(($tmp_section['Average']/$count), 0);
			$tmp_poss_score = $tmp_section['Possible'];
			echo "<td>\n";
			echo $tmp_avg_score." / ".$tmp_poss_score."<br>";
			echo 100*(round(($tmp_avg_score / $tmp_poss_score), 2))."%";
			echo "</td>\n";
		}
		$total_avg_score = round(($Total_Average/$count), 0);
		echo "<td>\n";
		echo $total_avg_score." / ".$Total_Possible."<br>";
		echo 100*(round($total_avg_score / $Total_Possible, 2))."%";
		echo "</td>\n";
		if ($showDetails) { // if apprpropriate, provide details link
			$group_incorrect_items['student_count'] = $count;
			echo '<td style="text-align:center">';
			echo "<a href='groupDetails.html?group_id=$group_id' ";
			echo 'title="View Group Learning Prescription" ';
			echo "target='_blank'>\n";
			echo '<img style="border-style:none" src="../images/icon_group.png"></a></td>';
		}
		echo "</tr>\n";
	}


	echo '</table>';
	mysql_select_db('FISDAP', $dbConnect);
	set_session_value('class_learning_prescription_data', $group_incorrect_items);

	/**
	 * Print the hidden div containing the list of students who fit the criteria
	 * but didn't take the given test during the given date range (if any) 
	 */
	$num_unscheduled = 0;
	if ($scheduled_test_id != NULL) {
		$unscheduled_students = get_unscheduled_student_array($scheduled_test_id);
		$num_unscheduled = count($unscheduled_students);
	}
	if ( count($nullStudents) > 0 || $num_unscheduled > 0) {
		echo "<div class='container'>\n";
		$numNullStudents = count($nullStudents);
		$num_missing = $num_unscheduled + $numNullStudents;
		echo "<button onclick=\"toggleDiv('StudentList', 'toggleButton', $num_missing);\" ";
		echo "id='toggleButton'>\n";
		echo "Missing someone?</button>\n";
		echo common_utilities::get_bubblelink('legacy', 'student_scores', null);
		echo "<div id='StudentList' class='mediumtext' style='display:none;'>\n";

		// first, the unscheduled students, if any
		if ($num_unscheduled > 0) {
			if ($num_unscheduled == 1) {
				$explanation = "We found scores for a student who wasn't scheduled to take this exam. ";
				$link_text = "add the student";
			} else {
				$explanation = "We found scores for students who weren't scheduled to take this exam. ";
				$link_text = "add them";
			}
			$scheduled_test = new ScheduledTest($scheduled_test_id);
			$window_open_string = "'add_students_to_scheduled_test.html?scheduled_test_id=$scheduled_test_id','Help','height=600,width=750,left=250,top=50'";
			echo "<div class='sectionContent'>$explanation You can ";
			echo "<a href='#' ";
			echo "onclick=\"myWindow = window.open(".$window_open_string.");myWindow.focus(); return false;\">$link_text</a>";
			echo " to the schedule now.</div>";
		}	

		// then loop through the students who didn't take it
		if (count($nullStudents) > 0) {
			if (count($nullStudents) == 1) {
				$explanation = "It looks like this student was scheduled to take the exam but didn’t. Click on the student’s name to email him/her.";
			} else {
				$explanation = "It looks like these students were scheduled to take the exam but didn’t. Click on a student’s name to email him/her.";
			}
			echo "<div class='sectionContent'>$explanation<br>";
			foreach ($nullStudents as $nullStudent) {
				$tmp_student = new common_user($nullStudent);
				$select = "SELECT FirstName, LastName, EmailAddress ".
					"FROM StudentData ".
					"WHERE UserName='$nullStudent'";
				$result = mysql_query($select, $dbConnect);
				if (!$result || mysql_num_rows($result)!=1 ) {
					echo "Unknown student";
				} else {
					$firstname = mysql_result($result, 0, 'FirstName');
					$lastname = mysql_result($result, 0, 'LastName');
					$email = mysql_result($result, 0, 'EmailAddress');
					echo "<a href='mailto:$email'>$lastname, $firstname</a>\n";
					echo "<br>";
				}
			}
			echo "</div>\n";
		}
		echo "</div>\n";
		echo "</div>\n";
		echo "<br>\n";
	}

} // function print_score_table


function get_score_report($test, $UnixStartDate, $UnixEndDate, $studentArray) {
	$connection = &FISDAPDatabaseConnection::get_instance(); // Connect to MySQL Database
	$dbConnect = $connection->get_link_resource(); // Represents the connection itself

	$StartDate = date('n/j/Y', $UnixStartDate);
	$EndDate = date('n/j/Y', $UnixEndDate);

	/**
	 * Figure out which test we're working with
	 */
	$testAr = explode(":", $test);
	$blueprint_id=$testAr[0];
	$test_name=$testAr[2];

	/**
	 * Get the list of sections (section ids) for this test
	 */
	$sectionList = getBPSections($blueprint_id, $dbConnect);
	$num_sections = count($sectionList);

	/**
	 * Build the report
	 */
	$page_title = "FISDAP Test Results";

	/**
	 * Start to build the results table
	 */
	$width = null; // Let the table find it's own width
	$justification = 'center'; // The table is centered on the page
	$display_border = false; // we don't like borders
	$display_headings = true; // Yes, please display some column headings
	$alternate_shading = true; // Yes, please alternate the shading of each row

	$results_table = new common_table($width, $justification, $display_border, $display_headings, $alternate_shading);

	/**
	 * Create the Last Name column
	 */
	$colidx = 0; // Each column has to have it's own unique index
	$text = 'Last Name'; // The displayed column text
	$width = null; // Let the column find it's own width
	$justification = 'left'; // left is the default
	$textsize = null; // medium is the default

	$results_table->set_column($colidx, $text, $width, $justification, $textsize);

	/**
	 * Create the First Name column
	 */
	$colidx = 1; // Each column has to have it's own unique index
	$text = 'First Name'; // The displayed column text
	$width = null; // Let the column find it's own width
	$justification = 'left'; // left is the default
	$textsize = null; // medium is the default

	$results_table->set_column($colidx, $text, $width, $justification, $textsize);

	/**
	 * Create 2 columns for each section of the selected test
	 */
	if ($sectionList) {
		for ($a=0;$a<$num_sections;$a++) {
			$colspan = 2;
			$tmpSectName=getSectionName($sectionList[$a], $dbConnect);
			if ($tmpSectName) {
				$text = $tmpSectName; // The displayed column text
				$justification = 'center';
				$textsize = null; // medium is the default

				// while we're looping through the sections, set them up to calculate averages
				$tmpSect_id = $sectionList[$a];
				$sectionList[$a] = array();
				$sectionList[$a]['Section_id'] = $tmpSect_id;
				$sectionList[$a]['Name'] = $tmpSectName;
				$sectionList[$a]['Average'] = 0; 
				$sectionList[$a]['Possible'] = 0; 
			} else {
				$text = 'Unknown'; // The displayed column text
			}
			$results_table->set_supercolumn('super', ($a*2)+2, $colspan, $text, $justification, $textsize);
			$results_table->set_column(($a*2)+2, 'score', null, $justification, $textsize);
			$results_table->set_column(($a*2)+3, '%', null, $justification, $textsize);
		}
	}

	/**
	 * Create the Total column
	 */
	$Total_Average = 0; 
	$Total_Possible = 0; 

	$cols = count($sectionList)*2+2; // number of columns so far 
	$colspan = 2;
	$text = 'Total'; // The displayed column text
	$justification = 'center'; // left is the default
	$textsize = null; // medium is the default

	$results_table->set_supercolumn('super', $cols, $colspan, $text, $justification, $textsize);
	$results_table->set_column($cols, 'score', null, $justification, $textsize);
	$results_table->set_column($cols+1, '%', null, $justification, $textsize);

	/**
	 * get the results for each student
	 */
	$count = 0;
	$num_total_students = count($studentArray);
	for ($a=0;$a<$num_total_students;$a++) {
		$username = get_student_username_by_id($studentArray[$a]);
		//var_export($username);
		$results = get_result_array($username, $test, $UnixStartDate, $UnixEndDate);	
		//var_export($results);
		if ($results) {
			$tmpStudent = new common_student($studentArray[$a]);
			$results['firstname'] = $tmpStudent->get_firstname();
			$results['lastname'] = $tmpStudent->get_lastname();
			$results['email'] = $tmpStudent->get_emailaddress();
			$testDate = $results['time'];
			if ( $testDate>$UnixStartDate && $testDate<$UnixEndDate ) {
				$date = date("F jS, Y", $testDate);
				$email = $results['email'];
				$firstname = stripslashes($results['firstname']);
				$lastname = stripslashes($results['lastname']);

				/**
				 * populate the table with the student's results
				 */
				$tmp_results_array = array($lastname, $firstname);
				if ($sectionList) {
					for ($i=0;$i<$num_sections;$i++) {
						$tmpSectName=$sectionList[$i]['Name'];	
						if ($tmpSectName) {
							$tmp_score = $results[$tmpSectName]['score'];
							$tmp_possible = $results[$tmpSectName]['possible'];
							$tmp_results_array[] = " ".$tmp_score." / ".$tmp_possible;
							$tmp_results_array[] = 100*(round(($tmp_score / $tmp_possible), 2));

						} else {
							$tmp_results_array[] = "N/A";
						}

						// add the student's section score to the average for that section
						$sectionList[$i]['Average'] = $sectionList[$i]['Average'] + $tmp_score;
						$sectionList[$i]['Possible'] = $tmp_possible;

					}
				}
				$tmp_results_array[] = $results['totalscore'];
				$tmp_results_array[] = $results['totalpercentage'];

				// add the student's overall score to the average
				$Total_Array = explode(" / ", $results['totalscore']);
				$Total_Average = $Total_Average + $Total_Array[0];
				$Total_Possible = $Total_Array[1];

				// add the student's row to the table
				$results_table->add_row($count, $tmp_results_array);
				$count++;
			}
		}
	} // end student loop
	if ($count == 0) {
		$results_table->add_row(0, array("No results found for the given criteria."));
	}

	if ($count > 0) {

		/**
		 * Add the averages row for this group of students
		 */
		$tmp_results_array = array('Average:',' ');
		foreach ($sectionList as $tmp_section) {
			$tmp_sect_score = round(($tmp_section['Average']/$count), 0);
			$tmp_sect_possible = $tmp_section['Possible'];
			$tmp_results_array[] = " ".$tmp_sect_score." / ".$tmp_sect_possible;
			$tmp_results_array[] = 100*(round(($tmp_sect_score / $tmp_sect_possible), 2));
		}
		$total_avg_score = round(($Total_Average/$count), 0);
		$tmp_results_array[] = $total_avg_score." / ".$Total_Possible;
		$tmp_results_array[] = 100*(round(($total_avg_score / $Total_Possible), 2));

		$results_table->add_row($count, $tmp_results_array);
	}

	mysql_select_db('FISDAP', $dbConnect);

	/**
	 * Create a report object
	 */

	$subtitle = "Results for $test_name, $StartDate-$EndDate";
	$createdby = null; // common_report will figure out the current user
	$options = array('orientation'=>'landscape'); // An array of report options

	$report = new common_report($page_title, $subtitle, $createdby, $options);

	/**
	 * Add the table to the report
	 */

	$report->add($results_table); 

	return $report;
} // function

function get_prescription_report($test, $UnixStartDate, $UnixEndDate, $studentArray) {
	$connection = &FISDAPDatabaseConnection::get_instance(); // Connect to MySQL Database
	$dbConnect = $connection->get_link_resource(); // Represents the connection itself

	// Figure out which test we're working with
	$testAr = explode(":", $test);
	$blueprint_id=$testAr[0];
	$test_name=$testAr[2];

	// Create a report object
	$page_title = "FISDAP Learning Prescriptions for $test_name";
	$subtitle = "";
	$createdby = null; // common_report will figure out the current user
	$options = array('orientation'=>'portrait'); 
	$report = new common_report($page_title, $subtitle, $createdby, $options);

	// Get the list of sections (section ids) for this test
	$sectionList = getBPSections($blueprint_id, $dbConnect);
	$num_sections = count($sectionList) + 1;
	$column_width = 100/$num_sections."%";

	// get the prescription for each student
	$num_students = count($studentArray);
	for ($a=0;$a<$num_students;$a++) {
		$username = get_student_username_by_id($studentArray[$a]);
		$results = get_result_array($username, $test, $UnixStartDate, $UnixEndDate);	
		if ($results) {
			$tmpStudent = new common_student($studentArray[$a]);
			$testDate = $results['time'];
			if ( $testDate>$UnixStartDate && $testDate<$UnixEndDate ) {
				// build the student's individual page heading
				$date = date("M j, Y", $testDate);
				$results['firstname'] = $tmpStudent->get_firstname();
				$results['lastname'] = $tmpStudent->get_lastname();
				$firstname = stripslashes($results['firstname']);
				$lastname = stripslashes($results['lastname']);
				$prescription_title = "Learning Prescription for $firstname $lastname<br>$test_name, $date<br>";
				$textblock = new common_textblock($prescription_title, 'large', 'left');
				$report->add($textblock);

				// build the results table
				$width = array("html"=>"80%","pdf"=>"80%"); // Let the table find it's own width
				$justification = 'center'; // The table is centered on the page
				$display_border = true; 
				$display_headings = true; 
				$alternate_shading = true; 
				$results_table = new common_table($width, $justification, $display_border, $display_headings, $alternate_shading);

				$row_data = array();
				foreach ($sectionList as $section_id) {
					$section_name=getSectionName($section_id, $dbConnect);
					if ($section_name) {
						$column_id = $section_id;
						$text = $section_name; // The displayed column text
						$width = array("html"=>$column_width, "pdf"=>$column_width);
						$justification = 'center';
						$textsize = null; // medium is the default

					} 
					$results_table->set_column($column_id, $text, $width, $justification, $textsize);

					$score = $results[$section_name]['score'];
					$possible = $results[$section_name]['possible'];
					$percentage = round(($score/$possible), 2)*100;
					$row_data[$column_id] = $score." / ".$possible."<br>".$percentage."%";
				}

				$text = 'Total'; // The displayed column text
				$width = array("html"=>$column_width, "pdf"=>$column_width);
				$justification = 'center'; // left is the default
				$textsize = null; // medium is the default
				$results_table->set_column('Total', $text, $width, $justification, $textsize);
				$row_data['Total'] = $results['totalscore']."<br>".$results['totalpercentage']."%";

				$results_table->add_row(null, $row_data);	
				$report->add($results_table);

				// add the overall category breakdown
				$textblock = new common_textblock('Overall', 'mediumbold', 'left');
				$report->add($textblock);

				$overall_category_breakdown = 'Learning Category Analysis<br>';
				$overall_items_incorrect = implode(", ", $results["total_incorrect"]);
				$overall_category_info = get_category_info($blueprint_id, null, null);
				$overall_missed_category_info = get_category_info($blueprint_id, $overall_items_incorrect, null);
				foreach ($overall_category_info as $category=>$total) {
					$missed = $overall_missed_category_info[$category];
					$percent = round((($total-$missed)/$total)*100, 2);
					$overall_category_breakdown .=  "$category: $percent% correct<br>";
				}
				$textblock = new common_textblock($overall_category_breakdown, 'medium', 'left');
				$report->add($textblock);

				// loop through each section, building the prescription and the category analysis
				foreach ($sectionList as $section_id) {
					$section_name = getSectionName($section_id, $dbConnect);
					$textblock = new common_textblock($section_name, 'mediumbold', 'left');
					$report->add($textblock);

					$items_incorrect = implode(", ", $results[$section_name."_incorrect"]);
					$topics_incorrect = get_topics_for_items_by_id($items_incorrect, $blueprint_id, $section_id);
					$topic_text = "";
					foreach ($topics_incorrect as $topic) {
						$topic_text .= $topic['Topic']."<br>";
					}
					$textblock = new common_textblock($topic_text, 'medium', 'left');
					$report->add($textblock);

					$section_category_breakdown = 'Learning Category Analysis<br>';
					$section_category_info = get_category_info($blueprint_id, null, $section_id);
					$section_missed_category_info = get_category_info($blueprint_id, $items_incorrect, $section_id);
					foreach ($section_category_info as $category=>$total) {
						$missed = $section_missed_category_info[$category];
						$percent = round((($total-$missed)/$total)*100, 2);
						$section_category_breakdown .=  "$category: $percent% correct<br>";
					}
					$textblock = new common_textblock($section_category_breakdown, 'medium', 'left');
					$report->add($textblock);

				}
				$report->add(new common_pagebreak());
			} // if attempt fails within the date range
		} // if there is an attempt
	} // student loop

	return $report;
} 

function get_all_attempts($username, $database_name, $database_prefix) {
	$connection = &FISDAPDatabaseConnection::get_instance(); // Connect to MySQL Database
	$dbConnect = $connection->get_link_resource(); // Represents the connection itself

	$list_sel = "SELECT MoodleQuiz_id, MoodleIDMod FROM MoodleTestData where MoodleDatabase='$database_name' AND MoodlePrefix='$database_prefix'";
	$list_res = $connection->query($list_sel);

	$instr='';
	$real_exam_list = array();
	$empty_array = array();

	foreach ($list_res as $exam_list) {
		$real_exam_list[]=$exam_list["MoodleQuiz_id"]-$exam_list["MoodleIDMod"];
	}   
	if (count($real_exam_list)==1) {
		$instr = " AND quiz=".$real_exam_list[0]." ";
	} else {   
		$instr = " AND quiz IN (".implode(',', $real_exam_list).") ";
	}

	/**
	 * SWITCH TO MOODLE DATABASE
	 * then get the student's moodle account info
	 */
	if (!mysql_select_db($database_name, $dbConnect)) {
		echo mysql_errno().": Cannot select database: ".mysql_error()."<BR>";
		return false;
	}
	$select = "SELECT id ".
		"FROM ".$database_prefix."_user ".
		"WHERE username='".$username."'";
	$result = mysql_query($select, $dbConnect);
	$count = mysql_num_rows($result);
	if ($count==1) {
		$tmp_id =  mysql_result($result, 0, "id");
	} else {
		mysql_select_db('FISDAP', $dbConnect);
		//echo $select."<br>";
		return $empty_array;
	}

	$select = "SELECT id, quiz, DATE(FROM_UNIXTIME(timestart)) as date ".
		"FROM ".$database_prefix."_quiz_attempts ".
		"WHERE userid=$tmp_id ".
		"AND timefinish>0 ".
		$instr.
		"ORDER BY timefinish";
	//echo "select: $select<br>";
	$result = $connection->query($select);
	$count = count($result);
	if ($count>0) {
		mysql_select_db('FISDAP', $dbConnect);
		return $result;
	} else {
		mysql_select_db('FISDAP', $dbConnect);
		//echo "no attempts<br>";
		return $empty_array;
	}
}

function get_bp_item_details($tbp_id) {
	$connection = &FISDAPDatabaseConnection::get_instance(); // Connect to MySQL Database

	$select = "SELECT I.Item_id as Item_id, A.AssetDef_id as Asset_id, D.Stem as Stem, S.Name as Section, T.Name as Topic, C.Name as Col ".
		"FROM TestBPItems I, Asset_def A, Item_def D, TestBPSections S, TestBPTopics T, TestBPColumns C ".
		"WHERE I.tbp_id=$tbp_id ".
		"AND I.Item_id = A.Data_id ".
		"AND A.DataType_id = 17 ".    // asset type of test item
		"AND I.Item_id = D.Item_id ".
		"AND I.tbpTopic_id = T.tbpTopic_id ".
		"AND T.tbpSect_id = S.tbpSect_id ".
		"AND I.tbpColumn_id = C.tbpColumn_id ".
		"ORDER BY S.Name, T.tbpTopic_id, C.ColumnNumber, I.Item_id";
	//echo $select;
	$result = $connection->query($select);
	if (!$result || count($result)<1) {
		return false;
	}

	$item_id_keyed = array();
	foreach ($result as $row_array) {
		$item_id = $row_array['Item_id'];
		$item_id_keyed[$item_id] = $row_array;
	}
	return $item_id_keyed;
}

function get_moodle_user_array($first, $last, $db, $pre='dpmdl') {
	$select = "SELECT id, firstname, lastname, username, email ".
		"FROM ".$pre."_user ".
		"WHERE firstname LIKE '" . addslashes($first) . "%' " .
		"AND lastname LIKE '" . addslashes($last) . "%' " .
		"ORDER BY lastname";
	//echo $select;
	$result = mysql_query($select, $db);
	$count = mysql_num_rows($result);
	$array = array();
	for ($i=0;$i<$count;$i++) {
		$user_info = mysql_fetch_assoc($result);
		$user_id = $user_info['id'];
		$array[$user_id] = $user_info;
	}

	return $array;
}

function delete_quiz_attempt($attemptid, $db, $pre='dpmdl') {
	$debug = false;

	//get attempt info 
	$attempt_select = 	"SELECT uniqueid, quiz, userid ".
		"FROM ".$pre."_quiz_attempts ".
		"WHERE id = '$attemptid'";
	if ($debug) {
		echo $attempt_select."<br>";
	}
	$attempt_info = mysql_query($attempt_select, $db);
	$count = mysql_num_rows($attempt_info);
	if ($count!=1) {
		echo "Could not find quiz attempt<br>";
		return false;
	} else {
		$uniqueid = mysql_result($attempt_info, 0, 'uniqueid');
		$quizid = mysql_result($attempt_info, 0, 'quiz');
		$userid = mysql_result($attempt_info, 0, 'userid');
	}

	//get information about the quiz 
	$quiz_select = "SELECT grademethod, grade, sumgrades, decimalpoints ".
		"from ".$pre."_quiz ".
		"where id=$quizid";
	if ($debug) {
		echo $quiz_select."<br>";
	}
	$quiz_result = mysql_query($quiz_select, $db);
	$quiz_method = mysql_result($quiz_result, 0, 'grademethod');
	$quiz_grade = mysql_result($quiz_result, 0, 'grade');
	$quiz_sumgrades = mysql_result($quiz_result, 0, 'sumgrades');
	$quiz_decimalpoints = mysql_result($quiz_result, 0, 'decimalpoints');

	//get ids of the question states
	$select_states = "SELECT * FROM ".$pre."_question_states ".
		"WHERE attempt = '$uniqueid'";
	if ($debug) {
		echo $select_states."<br>";
	}
	$state_info = mysql_query($select_states, $db);
	$num_states = mysql_num_rows($state_info);
	for ($i=0;$i<$num_states;$i++) {
		$state_ids[] = mysql_result($state_info, $i, 'id');
	}
	$state_id_str = implode(', ', $state_ids);

	//delete the attempt
	$delete_attempt = 	"DELETE FROM ".$pre."_quiz_attempts ".
		"WHERE id = '$attemptid' ".
		"LIMIT 1";
	if ($debug) {
		echo $delete_attempt."<br>";
	}
	if (!mysql_query($delete_attempt, $db)) {
		echo "Could not delete quiz attempt<br>";
		return false;
	}
	$rows_deleted = mysql_affected_rows($db);
	if ($rows_deleted!=1) {
		echo "Wrong number of rows affected for delete quiz attempt: $rows_deleted<br>";
		return false;
	}

	//delete question states records from question_rqp_states
	$delete_rqp_states = "DELETE FROM ".$pre."_question_rqp_states ".
		"WHERE stateid IN (".$state_id_str.")";
	if ($debug) {
		echo $delete_rqp_states."<br>";
	}
	if (!mysql_query($delete_rqp_states, $db)) {
		echo "Could not delete rqp states<br>";
		return false;
	}

	//delete the rest of the question data
	$delete_states = "DELETE FROM ".$pre."_question_states WHERE attempt = '$uniqueid' LIMIT $num_states";
	if ($debug) {
		echo $delete_states."<br>";
	}
	if (!mysql_query($delete_states, $db)) {
		echo "Could not delete quiz states<br>";
		return false;
	}
	$rows_deleted = mysql_affected_rows($db);
	if ($rows_deleted!=$num_states) {
		echo "Wrong number of rows affected for delete state: $rows_deleted<br>";
		return false;
	}

	$delete_sessions = "DELETE FROM ".$pre."_question_sessions WHERE attemptid = '$uniqueid' LIMIT $quiz_sumgrades";
	if ($debug) {
		echo $delete_sessions."<br>";
	}
	if (!mysql_query($delete_sessions, $db)) {
		echo "Could not delete question sessions<br>";
		return false;
	}
	$rows_deleted = mysql_affected_rows($db);
	if ($rows_deleted!=$quiz_sumgrades) {
		echo "Wrong number of rows affected for delete sessions: $rows_deleted<br>";
		return false;
	}

	$delete_attempts = "DELETE FROM ".$pre."_question_attempts WHERE id = '$uniqueid' LIMIT 1";
	if ($debug) {
		echo $delete_attempts."<br>";
	}
	if (!mysql_query($delete_attempts, $db)) {
		echo "Could not delete question attempts<br>";
		return false;
	}
	$rows_deleted = mysql_affected_rows($db);
	if ($rows_deleted!=1) {
		echo "Wrong number of rows affected for delete attempts: $rows_deleted<br>";
		return false;
	}


	//see if there is at least one other attempt
	$attempts_select = "SELECT * ".
		"FROM ".$pre."_quiz_attempts ".
		"WHERE userid = $userid ".
		"AND quiz = $quizid ".
		"AND timefinish > 0";	
	if ($debug) {
		echo $attempts_select."<br>";
	}
	$other_attempts = mysql_query($attempts_select, $db);
	$num_attempts = mysql_num_rows($other_attempts);

	//if there is no other attempt, delete the grade record entirely
	if ($num_attempts<1) {
		$delete_grade = "DELETE FROM ".$pre."_quiz_grades ".
			"WHERE userid = $userid AND quiz = $quizid ".
			"LIMIT 1";
		if ($debug) {
			echo $delete_grade."<br>";
		}
		if (!mysql_query($delete_grade, $db)) {
			echo "Could not delete grade record<br>";
			return false;
		}
		$rows_deleted = mysql_affected_rows($db);
		if (!$rows_deleted<1) {
			echo "Wrong number of rows affected for delete grade record: $rows_deleted<br>";
			return false;
		}
	} else {
		for ($i=0;$i<$num_attempts;$i++) {
			$grades[] = mysql_result($other_attempts, $i, 'sumgrades');
		}
		if ($debug) {
			var_export($grades);
		}

		//calculate the correct grade
		switch ($quiz_method) {
		case 3: //first attempt
			$chosen_grade = $grades[0];
			break;

		case 4: //last attempt
			$last_index = count($grades) - 1;
			$chosen_grade = $grades[$last_index];
			break;

		case 2: //average
			$chosen_grade = (float)array_sum($grades)/count($grades); 
			break;

		case 1: //highest
			rsort($grades);
			$chosen_grade = $grades[0];
			break;
		}
		$chosen_grade = round($chosen_grade*$quiz_grade/$quiz_sumgrades, $quiz_decimalpoints);

		//update existing record with grade and timestamp OR create new record with best grade and timestamp
		$quiz_grade_select = "SELECT * ".
			"FROM ".$pre."_quiz_grades ".
			"WHERE userid = $userid AND quiz = $quizid";
		if ($debug) {
			echo $quiz_grade_select."<br>";
		}
		$quiz_grade_result = mysql_query($quiz_grade_select, $db);
		$record_exists = mysql_num_rows($quiz_grade_result);
		if (!$record_exists) {
			$insert_grade = "INSERT INTO ".$pre."_quiz_grades ".
				"SET grade = $chosen_grade, ".
				"timemodified = ".time().", ".
				"userid = $userid, ".
				"quiz = $quizid";
			if ($debug) {
				echo $insert_grade."<br>";
			}
			if (!mysql_query($insert_grade, $db)) {
				echo "Could not insert new grade record<br>";
				return false;
			}
			$rows_inserted = mysql_affected_rows($db);
			if ($rows_inserted!=1) {
				echo "Wrong number of rows affected for insert grade record: $rows_inserted<br>";
				return false;
			}
		} else {
			$update_grade = "UPDATE ".$pre."_quiz_grades ".
				"SET grade = $chosen_grade, ".
				"timemodified = ".time()." ".
				"WHERE userid = $userid AND quiz = $quizid ".
				"LIMIT 1";
			if ($debug) {
				echo $update_grade."<br>";
			}
			if (!mysql_query($update_grade, $db)) {
				echo "Could not update new grade record<br>";
				return false;
			}
			$rows_updated = mysql_affected_rows($db);
			if ($rows_updated!=1) {
				echo "Wrong number of rows affected for update grade record: $rows_updated<br>";
				return false;
			}
		}
	} // deal w/ quiz_grades record if there were other attempts
}
?>
