<?php

// CONSTANTS
define(REVIEW, 1);
define(PODCAST, 2);

// check to see if an instructor is a reviewer
function is_reviewer($username) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$select = "SELECT Reviewer FROM InstructorData WHERE UserName='$username'";
	$result = mysql_query($select, $dbConnect);
	return mysql_result($result, 0, "Reviewer");

}

// set or unset whether an instructor is a reviewer
function toggleInstReviewerFlag($Instructor_id, $newReviewerValue) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$query = "UPDATE InstructorData ".
		"SET Reviewer=$newReviewerValue ".
		"WHERE Instructor_id=$Instructor_id ".
		"LIMIT 1";
	return mysql_query($query, $dbConnect);
}

// add a new session
function add_session($date, $time, $slots, $duration, $url, $phonenumber, $code, $sessionNotes, $topic, $type) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$ins = "INSERT INTO ScheduledSessions ".
		"SET Date='$date', ".
		"StartTime='$time', ".
		"TotalSlots=$slots, ".
		"Duration='$duration', ".
		"URL='$url', ".
		"PhoneNumber='$phonenumber', ".
		"PhoneCode='$code', ".
		"Notes='".addslashes($sessionNotes)."', ".
		"Topic='".addslashes($topic)."', ".
		"Type=$type";

	//echo $ins."\n";
	return mysql_query($ins, $dbConnect);
}

// edit an existing session
function update_session($date, $time, $slots, $duration, $url, $phonenumber, $code, $sessionNotes, $id, $topic) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$update = "UPDATE ScheduledSessions ".
		"SET Date='$date', ".
		"StartTime='$time', ".
		"TotalSlots=$slots, ".
		"Duration='$duration', ".
		"URL='$url', ".
		"PhoneNumber='$phonenumber', ".
		"PhoneCode='$code', ".
		"Notes='".addslashes($sessionNotes)."', ".
		"Topic='".addslashes($topic)."' ".
		"WHERE ScheduledSession_id=$id LIMIT 1";

	return mysql_query($update, $dbConnect);
}

// delete an existing session
function del_session($id, $reason='') {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$get_participants = "SELECT Instructor_id ".
		"FROM ScheduledSessionSignups ".
		"WHERE ScheduledSession_id=".$id.
		" AND CantCome=0";
	$participantsResult = mysql_query($get_participants);

	if (!$participantsResult) {
		die('Unable to get participants; session NOT deleted'); 
	}

	// create the array of ids, starting with Louise's
	$RecipientArray = array();
	for ($i=0;$i<mysql_num_rows($participantsResult);$i++) {
		$RecipientArray[] = mysql_result($participantsResult, $i, 'Instructor_id');     
	}
	send_session_email($id, $RecipientArray, 'cancelled', $reason);

	$del_signups = "DELETE FROM ScheduledSessionSignups WHERE ScheduledSession_id=$id";
	if (mysql_query($del_signups, $dbConnect)==true) {
		$del = "DELETE FROM ScheduledSessions WHERE ScheduledSession_id=$id LIMIT 1";
		return mysql_query($del, $dbConnect);
	} else {
		return false;
	}
}

// find out if the given session has any open slots
function sess_has_slots($id) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$sel = "SELECT TotalSlots ".
		"FROM ScheduledSessions ".
		"WHERE ScheduledSession_id=$id";
	$res = mysql_query($sel, $dbConnect);
	$slots = mysql_result($res, 0, "TotalSlots");

	// count people who are scheduled OR attended
	$sel = "SELECT count(*) ".
		"FROM ScheduledSessionSignups ".
		"WHERE ScheduledSession_id=$id ".
		"AND CantCome=0 ".
		"AND Attended!=0";
	$res = mysql_query($sel, $dbConnect);
	$count = mysql_result($res, 0, "count(*)");

	if ($count < $slots) {
		return true;
	} else {
		return false;
	}
}

// get the number for filled slots for an existing session
function get_num_filled_slots($id) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$sel = "SELECT count(*) ".
		"FROM ScheduledSessionSignups ".
		"WHERE ScheduledSession_id='$id' ".
		"AND CantCome=0 ".
		"AND Attended!=0";
	$res = mysql_query($sel, $dbConnect);
	$count = mysql_result($res, 0, "count(*)");
	if ($count) {
		return $count;
	} else {
		return false;
	}
}

// sign up an instructor for an existing session
function sess_sign_up($inst_id, $sess_id, $sendMail=1) {
	// double-check to make sure there are still slots 
	if (!sess_has_slots($sess_id)) {
		//echo "FILLED";
		return "filled";
	}

	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$RecipientArray = array($inst_id);

	if (sess_is_signed_up($inst_id, $sess_id)) {
		$update = "UPDATE ScheduledSessionSignups ".
			"SET CantCome=0, Attended=-1 ".
			"WHERE Instructor_id=$inst_id ".
			"AND ScheduledSession_id=$sess_id";
		$updateResult = mysql_query($update, $dbConnect);
		if ($updateResult) {
			if ($sendMail==1) {
				send_session_email($sess_id, $RecipientArray, 'signup');
			}
		}
		//echo "UPDATED";
		return $updateResult;
	} else {
		$insert = "INSERT INTO ScheduledSessionSignups ".
			"SET Instructor_id=$inst_id, ScheduledSession_id=$sess_id";
		$insertResult = mysql_query($insert, $dbConnect);
		if ($insertResult) {
			if ($sendMail==1) {
				send_session_email($sess_id, $RecipientArray, 'signup');
			}
		}
		//echo "ADDED";
		return $insertResult;
	}
}

// drop an instructor from a session for which s/he was signed up
function sess_un_sign_up($inst_id, $sess_id, $reason='') {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$RecipientArray = array($inst_id);

	if (sess_is_signed_up($inst_id, $sess_id)) {
		$update = "UPDATE ScheduledSessionSignups ".
			"SET CantCome=1 ".
			"WHERE Instructor_id=$inst_id ".
			"AND ScheduledSession_id=$sess_id";
		$updateResult =  mysql_query($update, $dbConnect);
		if ($updateResult) {
			send_session_email($sess_id, $RecipientArray, 'drop', $reason);
		}
		return $updateResult;
	}
}

// set whether or not an instructor attended a session for which s/he was signed up
function sess_update_attended($attended, $inst_id, $sess_id) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$update = "UPDATE ScheduledSessionSignups SET Attended=$attended WHERE Instructor_id=$inst_id AND ScheduledSession_id=$sess_id";
	return mysql_query($update, $dbConnect);
}

// update the signup entry for a given instructor 
function sess_update_signup($sess_id, $inst_id, $attended, $cantcome, $notes) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$update = "UPDATE ScheduledSessionSignups ".
		"SET Attended=$attended, ".
		"CantCome=$cantcome, ".
		"Notes='".addslashes($notes)."' ".
		"WHERE Instructor_id=$inst_id ".
		"AND ScheduledSession_id=$sess_id ".
		"LIMIT 1";
	//echo $update."<br>";
	return mysql_query($update, $dbConnect);
}

// check if the given istructor is signed up for a given session
function sess_is_signed_up($inst_id, $sess_id, $cantcome=-1) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$sel = "SELECT count(*) FROM ScheduledSessionSignups WHERE Instructor_id=$inst_id AND ScheduledSession_id=$sess_id";
	if ($cantcome != -1) {
		$sel .= " AND CantCome=$cantcome";
	}
	$res = mysql_query($sel, $dbConnect);
	$count = mysql_result($res, 0, "count(*)");
	if ($count > 0) {
		return true;
	} else {
		return false;
	}
}


function get_inst_name($id) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$sel = "SELECT FirstName, LastName FROM InstructorData where Instructor_id=$id";
	$name_array = $connection->query($sel);
	return $name_array[0]["FirstName"] . " " . $name_array[0]["LastName"];
}

// show all the available review session for a given instructor on his/her myFisdap page
function display_sessions($username, $types) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$today = Date("Y-m-d");

	//get the instructor ID
	$select_inst = "SELECT Instructor_id FROM InstructorData WHERE UserName='$username'";
	//echo "inst sel: $select_inst<br>\n";
	$select_inst_res = mysql_query($select_inst, $dbConnect);
	$select_inst_count = mysql_num_rows($select_inst_res);

	// if there is more than one instructor with that username
	if ($select_inst_count!=1) {
		die('error retrieving user data');
	}

	$inst_id = mysql_result($select_inst_res, 0, "Instructor_id");

	// get all the review sessions from today onward
	$sess_select = "SELECT * ".
		"FROM ScheduledSessions ".
		"WHERE Date >= '$today' ".
		"AND Type IN ($types) ".
		"ORDER BY Date,StartTime";

	//echo "rev sel: $sess_select<br>\n";
	$sess_result = mysql_query($sess_select, $dbConnect);
	$sess_count = mysql_num_rows($sess_result);

	$available = 0;

	// for each of the review sessions
	for ($i=0;$i<$sess_count;$i++) {
		$sess_id = mysql_result($sess_result, $i, "ScheduledSession_id");
		$sess_date = mysql_result($sess_result, $i, "Date");
		$sess_topic = mysql_result($sess_result, $i, "Topic");
		$sess_time = mysql_result($sess_result, $i, "StartTime");
		$sess_duration = mysql_result($sess_result, $i, "Duration");
		$sess_total_slots = mysql_result($sess_result, $i, "TotalSlots");
		$sess_timezone = mysql_result($sess_result, $i, "timezone");

		// if the session has open slots OR this user is signed up
		//if(sess_has_slots($sess_id) || sess_is_signed_up($inst_id, $sess_id, 0))
		//{
		//count the available shift
		$available++;

		//figure out how many red doors to show
		$red_doors = get_num_filled_slots($sess_id);
		$green_doors = $sess_total_slots-$red_doors;


		if (sess_is_signed_up($inst_id, $sess_id, 0)) {
			echo "<tr bgcolor='#EEEEEE'>\n";
			echo "<td><button onclick='javascript:SignUpCB($sess_id,0)'>Drop</button></td>\n";
		} else if (sess_has_slots($sess_id)) {
			echo "<tr>";
			echo "<td><button onclick='javascript:SignUpCB($sess_id,1)'>Pick</button></td>\n";
		} else {
			echo "<tr>";
			echo "<td></td>\n";
		}

		// print out the doors
		echo "<td>";
		for ($r=0;$r<$red_doors;$r++) {
			echo "<img border=0 src='../scheduler/images/closed.gif'>";
		}
		for ($g=0;$g<$green_doors;$g++) {
			echo "<img border=0 src='../scheduler/images/open.gif'>";
		}
		echo "</td>";

		echo "<td>$sess_date</td>\n";
		echo "<td>$sess_topic</td>\n";
		echo "<td>$sess_time $sess_timezone Time</td>\n";
		echo "<td>$sess_duration</td>\n";
		echo "</tr>\n";
		//}

	}

	//  if there were no available reviews
	if ($available == 0) {
		echo "<tr>";
		echo "<td colspan=5 align=center>";
		echo "No review sessions are available.";
		echo "</td></tr>\n";
	}
}

// returns a string signifying the instructor's region and state (ie; "Midwest, MN")
function get_inst_region($Instructor_id) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$StateQuery = "SELECT PD.ProgramState ".
		"FROM InstructorData ID, ProgramData PD ".
		"WHERE PD.Program_id = ID.ProgramId ".
		"AND ID.Instructor_id = $Instructor_id";

	$StateResult = mysql_query($StateQuery, $dbConnect);

	if ($StateResult && mysql_num_rows($StateResult) == 1) {   
		$State = mysql_result($StateResult, 0, "PD.ProgramState");
		$region = get_state_region($State);
		$region_state = $region.", ".$State;
	} else {
		$region_state = "Unknown";
	}

	return $region_state;
} // get_inst_region

// returns a sting representation of the given state's region
function get_state_region($State) {
	$Northwest = array('WA', 'OR', 'ID', 'MT', 'WY'); // 5
	$Southwest = array('CA', 'NV', 'UT', 'AZ');       // 4
	$Midwest = array('ND', 'SD', 'MN', 'NE', 'IA', 'MO', 'WI', 'IL', 'IN', 'MI', 'KY'); // 11
	$Southcentral = array('CO', 'NM', 'TX', 'OK', 'KS', 'AR', 'LA', 'MS'); // 8
	$Northeast = array('OH', 'WV', 'VA', 'DC', 'MD', 'DE', 'NJ', 'PA', 'NY', 'CT', 'RI', 'MA', 'VT', 'NH', 'ME'); // 15
	$Southeast = array('AL', 'TN', 'NC', 'SC', 'GA', 'FL'); // 6
	$Pacific = array('AK', 'HI'); // 2

	$Regions = array("Northwest" => $Northwest, 
		"Southwest" => $Southwest, 
		"Midwest" => $Midwest, 
		"Southcentral" => $Southcentral, 
		"Northeast" => $Northeast, 
		"Southeast" => $Southeast, 
		"Pacific" => $Pacific);

	$Region = "Other";

	foreach ($Regions as $RegionName => $RegionArray) {
		if (in_array($State, $RegionArray)) {
			$Region = $RegionName;
		}
	}

	return $Region;
}

// get a list of email addresses for the instructors signed up for a given session
function get_email_list($Session_id) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$emailQuery = "SELECT ID.Email ".
		"FROM InstructorData ID, ScheduledSessionSignups RS ".
		"WHERE RS.ScheduledSession_id=".$Session_id." ".
		"AND ID.Instructor_id = RS.Instructor_id ".
		"AND RS.CantCome = 0";
	$emailResult = mysql_query($emailQuery, $dbConnect);
	if (!$emailResult) {
		die("Addresses could not be found; no email generated.");
	}

	$count = mysql_num_rows($emailResult);
	// put the email addresses into a list
	$listString = "";
	for ($i;$i<$count;$i++) {
		$listString .= mysql_result($emailResult, $i, "ID.Email");
		$listString .= ",";
	}
	$listString = substr($listString, 0, strlen($listString)-1);

	return $listString;
}

// automatically sends out an email about the given review session
// RecipientArray should be an array of INSTRUCTOR IDs
function send_session_email($ScheduledSession_id, $RecipientArray, $EmailType, $reason='') {

	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();


	// CREATE THE LIST OF EMAILS ($to)
	//convert the array to a string
	$RecipientString = implode(",", $RecipientArray);

	//get email addresses
	$addressQuery = "SELECT Email ".
		"FROM InstructorData ".
		"WHERE Instructor_id ".
		"IN (".$RecipientString.")";

	$addressResult = mysql_query($addressQuery, $dbConnect);
	if (!$addressResult) {
		die("Addresses could not be found; no email generated.");
	}

	$count = mysql_num_rows($addressResult);

	// put the email addresses into a list
	$to = "";
	for ($i;$i<$count;$i++) {
		$to .= mysql_result($addressResult, $i, "Email");
		$to .= ",";
	}

	$to .= "support@fisdap.net,";
	$to = substr($to, 0, strlen($to)-1);
	$from = "From: fisdap-robot@fisdap.net";


	//GET INFORMATION ABOUT THE SESSION
	$sessionQuery = "SELECT * ".
		"FROM ScheduledSessions s, ScheduledSessionTypes t ".
		"WHERE s.ScheduledSession_id = ".$ScheduledSession_id." ".
		"AND s.Type = t.type_id";

	$sessionResult = mysql_query($sessionQuery, $dbConnect);
	if (!$sessionResult || mysql_num_rows($sessionResult)!=1) {
		die("Review could not be found; no email generated.");
	}

	$ReviewDate = mysql_result($sessionResult, 0, "Date");
	$StartTime = mysql_result($sessionResult, 0, "StartTime");
	$SessionType = mysql_result($sessionResult, 0, "type_name");
	$Duration = mysql_result($sessionResult, 0, "Duration");
	$URL = mysql_result($sessionResult, 0, "URL");
	$PhoneNumber = mysql_result($sessionResult, 0, "PhoneNumber");
	$PhoneCode = mysql_result($sessionResult, 0, "PhoneCode");
	$timezone = mysql_result($sessionResult, 0, "timezone");


	// CREATE THE SUBJECT AND MESSAGE BASED ON $EmailType
	switch ($EmailType) {
	case ('signup'):
		$subject = "FISDAP $SessionType session scheduled!";
		$message = "You are scheduled for a FISDAP $SessionType session!\n\n".
			"Session Information:\n".
			"Date: ".$ReviewDate."\n".
			"Time: ".$StartTime." ".$timezone." Time\n".
			"Duration: ".$Duration."\n\n";
		if ($SessionType=='Review') {
			$message .= "During this time, we use an online software called ".
				"Breeze to view, discuss, and edit original test-items.  ".
				"You will need to have Internet access and a telephone.  ".
				"Someone will contact you 24-48 hours before the meeting ".
				"with more details. Please be aware that review sessions ".
				"may be canceled if there are fewer than three participants.  ";
		} else {
			$message .= "Someone will contact you shortly with more details.  ";
		}
		$message .= "If you need to cancel your participation, please do so ".
			"greater than 48 hours in advance.  ".
			"If you have any questions, please contact Rachael ".
			"(rbaird@fisdap.net).\n\n".
			"Thank you for participating!";
		break;

	case ('drop'):
		$subject = "FISDAP $SessionType session dropped";
		$message = "You are no longer scheduled for the following ".
			"FISDAP $SessionType session:\n\n".
			"Date: ".$ReviewDate."\n".
			"Time: ".$StartTime." ".$timezone." Time\n\n";
		if ($reason!='') {
			$message .= "Reason: ".$reason."\n\n";
		}
		$message .= "If you believe you have received this email ".
			"in error, please contact Rachael ".
			"(rbaird@fisdap.net).";
		break;

	case ('cancelled'):
		$subject = "FISDAP $SessionType session cancelled";
		$message = "The following FISDAP $SessionType session has been cancelled:\n\n".
			"Date: ".$ReviewDate."\n".
			"Time: ".$StartTime." ".$timezone." Time\n";
		if ($reason!='') {
			$message .= "Reason for cancellation: ".$reason."\n\n";
		}
		$message .= "If you have any questions, ".
			"please contact Rachael ".
			"(rbaird@fisdap.net).\n\n".
			"Thank you for participating!";
		break;

	case ('auto'):
		$subject = "FISDAP $SessionType session reminder";
		$message = "You are scheduled for a FISDAP $SessionType session tomorrow!\n\n".
			"Session Information:\n".
			"Date: ".$ReviewDate."\n".
			"Time: ".$StartTime." ".$timezone." Time\n".
			"This session will last ".$Duration.".\n\n";
		break;

	default:
		$to = "support@fisdap.net,khanson@fisdap.net";
		$subject = "Scheduled session email glitch";

		$message = "There has been an error in the scheduled session ".
			"auto-email system.\n\n".
			"Session Information:\n".
			"Date: ".$ReviewDate."\n".
			"Time: ".$StartTime."\n";
		break;
	}

	//echo "sending email....$to, $subject, $message, $from";
	mail($to, $subject, $message, $from);

}//send_session_email

// returns an associative array of all the instructors who are signed up to be reviewers
function fetch_all_reviewers() {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$revQuery = "SELECT Instructor_id, FirstName, LastName ".
		"FROM InstructorData ".
		"WHERE Reviewer=1 ".
		"ORDER BY Instructor_id";
	$revResult = mysql_query($revQuery, $dbConnect);
	if (!$revResult) {
		die("Couldn't fetch Instructor info from the database.");
	}

	$reviewerArray = array();
	while ($reviewer = mysql_fetch_assoc($revResult)) {
		$reviewerArray[] = array( 'Instructor_id' => $reviewer['Instructor_id'],
			'FirstName' => $reviewer['FirstName'],
			'LastName' => $reviewer['LastName']);
	}//while

	return $reviewerArray;
}//fetch_all_reviewers

