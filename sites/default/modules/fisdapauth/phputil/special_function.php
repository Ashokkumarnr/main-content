<?php

function get_events($Program_id,$EndDate,$StartDate,$today,$ClockDay,$Instructor,$pickfield,$pickclinic,$picklab,$setAmbServ_id,$setBase_id,$setPreceptor,$tcode,$seclist,$whatShifts,$Student_id,$Class_Year,$ClassMonth,$ViewAll,$REMOTE_USER,$dbConnect)
{
	$ALLTYPES = 7;
	/************************************
	 *Here we select the unlimited shifts*
	 ************************************/	
	$FirstSelect = buildFirst($Program_id,$EndDate,$StartDate,$today,$ClockDay,$Instructor,$pickfield,$pickclinic,$picklab,$setAmbServ_id,$setBase_id,$setPreceptor,0,$whatShifts,$Student_id);
	$SharedFirst = buildFirst($Program_id,$EndDate,$StartDate,$today,$ClockDay,$Instructor,$pickfield,$pickclinic,$picklab,$setAmbServ_id,$setBase_id,$setPreceptor,1,$whatShifts,$Student_id);

	/******************************************
	 *Here we need to select the limited shifts*
	 ******************************************/	

	if(!(isset($tcode)))
	{
		$tcode = $ALLTYPES;
	}
	if(!(isset($seclist)))
	{
		$seclist = "";
	}

	//echo "Seclist is: $seclist<br>\n";
	//echo "tcode is: $tcode<br>\n";
	$SecondSelect = buildSecond($Instructor,$Program_id,$tcode,$seclist,$EndDate,$StartDate,$setAmbServ_id,$setBase_id,$pickfield,$pickclinic,$picklab,$setPreceptor,$today,$ClockDay,$ALLTYPES,0,"a",$whatShifts,$Student_id);
	$SharedSecond = buildSecond($Instructor,$Program_id,$tcode,$seclist,$EndDate,$StartDate,$setAmbServ_id,$setBase_id,$pickfield,$pickclinic,$picklab,$setPreceptor,$today,$ClockDay,$ALLTYPES,1,"a",$whatShifts,$Student_id);
	$SharedSecondb = buildSecond($Instructor,$Program_id,$tcode,$seclist,$EndDate,$StartDate,$setAmbServ_id,$setBase_id,$pickfield,$pickclinic,$picklab,$setPreceptor,$today,$ClockDay,$ALLTYPES,1,"b",$whatShifts,$Student_id);

	//echo "<br>Shared Second is: $SharedSecond<br><br>\n";

	if(($Instructor != 1) && ($seclist != "" && $seclist != -1))
	{
		$ThirdSelect = buildThird($Program_id,$seclist,$EndDate,$StartDate,$pickfield,$pickclinic,$picklab,$setAmbServ_id,$setBase_id,$setPreceptor,$Instructor,$today,$ClockDay,0,"a",$whatShifts,$Student_id);
		$SharedThird = buildThird($Program_id,$seclist,$EndDate,$StartDate,$pickfield,$pickclinic,$picklab,$setAmbServ_id,$setBase_id,$setPreceptor,$Instructor,$today,$ClockDay,1,"a",$whatShifts,$Student_id);
		$SharedThirdb = buildThird($Program_id,$seclist,$EndDate,$StartDate,$pickfield,$pickclinic,$picklab,$setAmbServ_id,$setBase_id,$setPreceptor,$Instructor,$today,$ClockDay,1,"b",$whatShifts,$Student_id);
	}

	//Here we select all the shifts that we want to see and add them to the above list if they are not there.
	/*
	 *	We will have to re visit this and see if it is nessicary when we do all the shifts at once.
	 *	I am betting it will be to get the non scheduler shifts.
	 */
	if($whatShifts != 1)
	{
		$select = "SELECT DISTINCT S.Event_id, S.StartDate, S.Shift_id, S.StartTime FROM StudentData ST, ShiftData S, AmbulanceServices A, AmbServ_Bases AB";
		if($Instructor==1)
		{		
			if($tcode != $ALLTYPES)
			{
				$select = $select . ", SerialNumbers SN";
			}
			if($seclist != "" && $seclist != -1)
			{
				$select = $select . ", SectStudents SC";
			}
		}		
		if($setPreceptor > 0)
		{
			$select = $select . ", EventData E, EventPreceptorData EPD";
			//$select = $select . ", RunData R";
		}

		$select = $select . " WHERE ST.Program_id=$Program_id";

		if($Instructor == 1)
		{		
			if($tcode != $ALLTYPES)
			{
				if($tcode == 1)
				{
					//$TC = "emt-b";
					$select = $select . " AND SN.Student_id = ST.Student_id AND (SN.AccountType='emt-b')";
				}
				else
				{
					if($tcode == 2)
					{
						//$TC = "emt-i";
						$select = $select . " AND SN.Student_id = ST.Student_id AND (SN.AccountType='emt-i')";
					}
					if($tcode == 4)
					{
						//$TC = "paramedic";
						$select = $select . " AND SN.Student_id = ST.Student_id AND (SN.AccountType='paramedic')";
					}
					if($tcode == 6)
					{
						//$TC = "SS";
						$select = $select . " AND SN.Student_id = ST.Student_id AND (SN.AccountType='paramedic')";
					}
					//$select = $select . " AND SN.Student_id = ST.Student_id AND SN.AccountType='$TC'";
				}
			}
			if($seclist != "" && $seclist != -1)
			{
				$first=0;
				$tmpstr='';
				$tok = strtok($seclist,",");
				while($tok)
				{
					if($first == 0)
					{
						$tmpstr = "SC.Section_id = $tok";
					}
					else
					{
						$tmpstr = $tmpstr . " OR SC.Section_id = $tok";
					}
					$tok=strtok(",");
				}
				// AND CS.Program_id=$Program_id AND E.Event_id=CS.Event_id AND ($tmpstr)
				$select = $select . " AND ST.Student_id = SC.Student_id AND ($tmpstr)";
			}
			if($setAmbServ_id == -10 && $pickfield == 1)
			{
				$select = $select . " AND S.Type='field'";
			}
			//If 'clinical' is specified, select only clinical shifts.
			if($setAmbServ_id == -15 && $pickclinic == 1)
			{
				$select = $select . " AND S.Type='clinical'";
			}
			if($setAmbServ_id == -20 && $picklab == 1)
			{
				$select = $select . " AND S.Type='lab'";
			}
		}          	

		//if($Instructor != 1)

		if($ViewAll != 1)
		{
			$select = $select . " AND ST.UserName = \"$REMOTE_USER\"";
		}
		$select = $select . " AND S.AmbServ_id = A.AmbServ_id AND S.StartBase_id = AB.Base_id";
		$select = $select . " AND S.StartDate < '$EndDate' AND S.StartDate >= '$StartDate'";
		$select = $select . " AND ST.Student_id = S.Student_id";
		if($setPreceptor > 0)
		{
			$select = $select . " AND E.Event_id=S.Event_id AND E.Event_id=EPD.Event_id AND EPD.Preceptor_id=$setPreceptor";
			//$select = $select . " AND S.Type='field' AND R.Precept_id=$setPreceptor AND S.Shift_id=R.Shift_id";
		}
		if(isset($Class_Year) && $Class_Year > 0)
		{
			$select = $select . " AND ST.Class_Year=$Class_Year";
		}
		if(isset($ClassMonth) && $ClassMonth > 0)
		{
			$select = $select . " AND ST.ClassMonth=$ClassMonth";
		}
		if(isset($Student_id) && $Student_id > 0)
		{
			$select = $select . " AND S.Student_id = $Student_id";
		}
		if($setAmbServ_id == -2)
		{
			$select = $select . " AND S.Type='field'";
		}
		//If 'clinical' is specified, select only clinical shifts.
		if($setAmbServ_id == -3)
		{
			$select = $select . " AND S.Type='clinical'";
		}
		//If location is specified, select events and shifts at that location.
		if(isset($setAmbServ_id) && $setAmbServ_id > 0)
		{
			$select = $select . " AND S.AmbServ_id = $setAmbServ_id";
		}
		if(isset($setBase_id) && $setBase_id != 0 && $setBase_id!="" && $setBase_id!=-1)
		{
			$select = $select . " AND S.StartBase_id IN ($setBase_id)";
		}

		$select = $select . " ORDER BY S.StartDate, ABS(S.StartTime), A.AmbServ_id, AB.Base_id LIMIT 1002";
	}

if($Instructor==1) {
	//echo "seclist is $seclist<br>\n";
	//enables shared schedulder shifts to show up when all shifts is chosen.
	if($whatShifts == 2 && ($seclist != "" && $seclist != -1)) {
		$skipFirst = 1;
	}//if
	}//if
	else {
		if($whatShifts == 2) {
			$skipFirst = 1;
		}//if
	}//else
	$FullArray = array();
	$SecondArray = array();
	$ThirdArray = array();
	$SharedFirstArray = array();
	$SecondSharedArray = array();
	$ThirdSharedArray = array();

	$SecondSharedArrayb = array();
	$ThirdSharedArrayb = array();
	$AddArray = array();
	//echo "FirstSelect is: $FirstSelect<br><br>\n";
	//echo "SecondSelect is: $SecondSelect<br><br>\n";
	//echo "ThirdSelect is: $ThirdSelect<br><br>\n";
	//echo "select is: $select<br><br>\n";
	//echo "Shared First is: $SharedFirst<br>\n";
	//echo "Shared Second is: $SharedSecond<br>\n";
	//echo "Shared Third is: $SharedThird<br>\n";
	//exit;

	if($skipFirst!= 1)
	{

		$result = mysql_query($FirstSelect, $dbConnect);
		$count = mysql_num_rows($result);
		//echo "Count is: $count\n";
		//echo "first<br>\n";
		if($count == 0 && $whatShifts == 1)
		{
	//		echo "<tr><td colspan='100%' align='center'>There are no shifts available at this time.</td></tr>";
		}
		$numEventsSelected = $count;

		for($i=0; $i<$count; $i++)
		{
			$Event_id = mysql_result($result, $i, "E.Event_id");
			//echo "$Event_id<br>";
			$SlotsLeft = calcOpenSlots($Event_id, $dbConnect);
			//echo "before trade<br>\n";
			$TradeSlotSelect = "SELECT Shift_id FROM ShiftData WHERE Event_id=$Event_id AND Trade=1 AND TradeStatus=3";
			$TradeSlotResult = mysql_query($TradeSlotSelect, $dbConnect);
			$TradeSlotCount = mysql_num_rows($TradeSlotResult);
			//echo "after trade<br>\n";
			$SlotsLeft = $SlotsLeft + $TradeSlotCount;

			if($Instructor == 1)
			{
				$OtherStuSel = "SELECT count(*) FROM ShiftData S, StudentData ST WHERE S.Event_id=$Event_id AND ST.Student_id=S.Student_id AND ST.Program_id != $Program_id";
				if(isset($Class_Year) && $Class_Year > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.Class_Year=$Class_Year";
				}
				if(isset($ClassMonth) && $ClassMonth > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.ClassMonth=$ClassMonth";
				}
				if(isset($Student_id) && $Student_id > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.Student_id = $Student_id";
				}
				$OtherStuRes = mysql_query($OtherStuSel,$dbConnect);
				$OtherStuCount = mysql_result($OtherStuRes,0,"count(*)");
			}
			//echo "Other stu count is $OtherStuCount<br>\n";	
			if(($SlotsLeft > 0 && $whatShifts != 2) || ($Instructor==1 && $OtherStuCount>0))
			{

				$Vari = new EventStuff;
				$SomeStartDate = mysql_result($result, $i, "E.StartDate");
				$Vari->set_date($SomeStartDate);
				//$Vari->set_id(mysql_result($result, $i, "E.Event_id"));
				$Vari->set_id($Event_id);
				$SomeStartTime = mysql_result($result, $i, "E.StartTime");
				//echo "StartTime is: $SomeStartTime<br>\n";
				$Vari->setSTime((int)$SomeStartTime);
				array_push($FullArray, $Vari);
			}
		}

		$sharedresult = mysql_query($SharedFirst, $dbConnect);
		$sharedcount = mysql_num_rows($sharedresult);
		//echo "Count is: $sharedcount\n";/
		//echo "first shared<br>\n";
		if($sharedcount == 0 && $whatShifts == 1)
		{
//			echo "<tr><td colspan='100%' align='center'>There are no shifts available at this time.</td></tr>";
		}
		$numSharedEventsSelected = $count;

		$secArray = explode(",",$seclist);
		for($i=0; $i<$sharedcount; $i++)
		{
			$isAvailable = 1;
			$typeAvailable = 1;
			$csAvailable = 1;
			$Event_id = mysql_result($sharedresult, $i, "E.Event_id");
			//echo "$Event_id<br>";
			$SlotsLeft = calcOpenSlots($Event_id, $dbConnect);
			//echo "before trade<br>\n";
			$TradeSlotSelect = "SELECT Shift_id FROM ShiftData WHERE Event_id=$Event_id AND Trade=1 AND TradeStatus=3";
			$TradeSlotResult = mysql_query($TradeSlotSelect, $dbConnect);
			$TradeSlotCount = mysql_num_rows($TradeSlotResult);
			//echo "after trade<br>\n";
			$SlotsLeft = $SlotsLeft + $TradeSlotCount;

			if($Instructor == 1)
			{
				$OtherStuSel = "SELECT count(*) FROM ShiftData S, StudentData ST WHERE S.Event_id=$Event_id AND ST.Student_id=S.Student_id AND ST.Program_id != $Program_id";
				if(isset($Class_Year) && $Class_Year > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.Class_Year=$Class_Year";
				}
				if(isset($ClassMonth) && $ClassMonth > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.ClassMonth=$ClassMonth";
				}
				if(isset($Student_id) && $Student_id > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.Student_id = $Student_id";
				}
				$OtherStuRes = mysql_query($OtherStuSel,$dbConnect);
				$OtherStuCount = mysql_result($OtherStuRes,0,"count(*)");
			}

			$sharedTypeSelect = "SELECT * FROM EventTypeAccess WHERE Event_id=$Event_id AND Program_id=$Program_id";
			$sharedTypeResult = mysql_query($sharedTypeSelect,$dbConnect);
			$sharedTypeCount = mysql_num_rows($sharedTypeResult);
			if($sharedTypeCount == 1)
			{
				$sharedType = mysql_result($sharedTypeResult,0,"AccessCode");
				if(!($tcode&$sharedType))
				{
					$typeAvailable = 0;
				}
			}
			$tmpCSArray = Array();
			$sharedCSSelect = "SELECT * FROM EventCSAccess WHERE Event_id=$Event_id AND Program_id=$Program_id";
			$sharedCSResult = mysql_query($sharedCSSelect,$dbConnect);
			$sharedCSCount = mysql_num_rows($sharedCSResult);
			for($cscounter=0;$cscounter<$sharedCSCount;$cscounter++)
			{
				$tmpCS = mysql_result($sharedCSResult,$cscounter,"ClassSection_id");
				array_push($tmpCSArray,$tmpCS);
			}
			$intersection = array_intersect($secArray,$tmpCSArray);
			if(sizeof($intersection) == 0)
			{
				$csAvailable = 0;
			}
			if($csAvailable == 0 && $typeAvailable == 0)
			{
				$isAvailable = 0;
			}


			if(($SlotsLeft > 0 || ($Instructor==1 && $OtherStuCount>0)) && $isAvailable!=0)
			{
				$Vari = new EventStuff;
				$SomeStartDate = mysql_result($sharedresult, $i, "E.StartDate");
				$Vari->set_date($SomeStartDate);
				//$Vari->set_id(mysql_result($sharedresult, $i, "E.Event_id"));
				$Vari->set_id($Event_id);
				$SomeStartTime = mysql_result($sharedresult, $i, "E.StartTime");
				//echo "StartTime is: $SomeStartTime<br>\n";
				$Vari->setSTime((int)$SomeStartTime);
				array_push($SharedFirstArray, $Vari);
			}
		}

		//echo "before second<br>\n";
		$secondresult = mysql_query($SecondSelect, $dbConnect);
		$secondcount = mysql_num_rows($secondresult);
		//echo "second<br>\n";
		for($j=0; $j<$secondcount; $j++)
		{
			$Event_id = mysql_result($secondresult, $j, "E.Event_id");
			//echo "$Event_id<br>";
			$SlotsLeft = calcOpenSlots($Event_id, $dbConnect);
			//$SlotsLeft = 1;

			$TradeSlotSelect = "SELECT Shift_id FROM ShiftData WHERE Event_id=$Event_id AND Trade=1 AND TradeStatus=3";
			$TradeSlotResult = mysql_query($TradeSlotSelect, $dbConnect);
			$TradeSlotCount = mysql_num_rows($TradeSlotResult);

			$SlotsLeft = $SlotsLeft + $TradeSlotCount;

			if($Instructor == 1)
			{
				$OtherStuSel = "SELECT count(*) FROM ShiftData S, StudentData ST WHERE S.Event_id=$Event_id AND ST.Student_id=S.Student_id AND ST.Program_id != $Program_id";
				if(isset($Class_Year) && $Class_Year > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.Class_Year=$Class_Year";
				}
				if(isset($ClassMonth) && $ClassMonth > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.ClassMonth=$ClassMonth";
				}
				if(isset($Student_id) && $Student_id > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.Student_id = $Student_id";
				}
				$OtherStuRes = mysql_query($OtherStuSel,$dbConnect);
				$OtherStuCount = mysql_result($OtherStuRes,0,"count(*)");
			}

			if(($SlotsLeft > 0 && $whatShifts != 2) || ($Instructor==1 && $OtherStuCount>0))
			{
				$Vari = new EventStuff;
				$SomeStartDate = mysql_result($secondresult, $j, "E.StartDate");
				$Vari->set_date($SomeStartDate);
				//$Vari->set_id(mysql_result($secondresult, $j, "E.Event_id"));
				$Vari->set_id($Event_id);
				$SomeStartTime = mysql_result($secondresult, $j, "E.StartTime");
				//echo "StartTime is: $SomeStartTime<br>\n";
				$Vari->setSTime((int)$SomeStartTime);
				array_push($SecondArray, $Vari);
			}
		}

		$secondsharedresult = mysql_query($SharedSecond, $dbConnect);
		$secondsharedcount = mysql_num_rows($secondsharedresult);
		//echo "second<br>\n";
		for($j=0; $j<$secondsharedcount; $j++)
		{
			$Event_id = mysql_result($secondsharedresult, $j, "E.Event_id");
			//echo "$Event_id<br>";
			$SlotsLeft = calcOpenSlots($Event_id, $dbConnect);
			//$SlotsLeft = 1;

			$TradeSlotSelect = "SELECT Shift_id FROM ShiftData WHERE Event_id=$Event_id AND Trade=1 AND TradeStatus=3";
			$TradeSlotResult = mysql_query($TradeSlotSelect, $dbConnect);
			$TradeSlotCount = mysql_num_rows($TradeSlotResult);

			$SlotsLeft = $SlotsLeft + $TradeSlotCount;

			if($Instructor == 1)
			{
				$OtherStuSel = "SELECT count(*) FROM ShiftData S, StudentData ST WHERE S.Event_id=$Event_id AND ST.Student_id=S.Student_id AND ST.Program_id != $Program_id";
				if(isset($Class_Year) && $Class_Year > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.Class_Year=$Class_Year";
				}
				if(isset($ClassMonth) && $ClassMonth > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.ClassMonth=$ClassMonth";
				}
				if(isset($Student_id) && $Student_id > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.Student_id = $Student_id";
				}
				$OtherStuRes = mysql_query($OtherStuSel,$dbConnect);
				$OtherStuCount = mysql_result($OtherStuRes,0,"count(*)");
			}

			if(($SlotsLeft > 0 && $whatShifts != 2) || ($Instructor==1 && $OtherStuCount>0))
			{
				$Vari = new EventStuff;
				$SomeStartDate = mysql_result($secondsharedresult, $j, "E.StartDate");
				$Vari->set_date($SomeStartDate);
				//$Vari->set_id(mysql_result($secondsharedresult, $j, "E.Event_id"));
				$Vari->set_id($Event_id);
				$SomeStartTime = mysql_result($secondsharedresult, $j, "E.StartTime");
				//echo "StartTime is: $SomeStartTime<br>\n";
				$Vari->setSTime((int)$SomeStartTime);
				array_push($SecondSharedArray, $Vari);
			}
		}

		$secondsharedresultb = mysql_query($SharedSecondb, $dbConnect);
		$secondsharedcountb = mysql_num_rows($secondsharedresultb);
		//echo "second<br>\n";
		for($j=0; $j<$secondsharedcountb; $j++)
		{
			$Event_id = mysql_result($secondsharedresultb, $j, "E.Event_id");
			//echo "$Event_id<br>";
			$SlotsLeft = calcOpenSlots($Event_id, $dbConnect);
			//$SlotsLeft = 1;

			$TradeSlotSelect = "SELECT Shift_id FROM ShiftData WHERE Event_id=$Event_id AND Trade=1 AND TradeStatus=3";
			$TradeSlotResult = mysql_query($TradeSlotSelect, $dbConnect);
			$TradeSlotCount = mysql_num_rows($TradeSlotResult);

			$SlotsLeft = $SlotsLeft + $TradeSlotCount;

			if($Instructor == 1)
			{
				$OtherStuSel = "SELECT count(*) FROM ShiftData S, StudentData ST WHERE S.Event_id=$Event_id AND ST.Student_id=S.Student_id AND ST.Program_id != $Program_id";
				if(isset($Class_Year) && $Class_Year > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.Class_Year=$Class_Year";
				}
				if(isset($ClassMonth) && $ClassMonth > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.ClassMonth=$ClassMonth";
				}
				if(isset($Student_id) && $Student_id > 0)
				{
					$OtherStuSel = $OtherStuSel . " AND ST.Student_id = $Student_id";
				}
				$OtherStuRes = mysql_query($OtherStuSel,$dbConnect);
				$OtherStuCount = mysql_result($OtherStuRes,0,"count(*)");
			}

			if(($SlotsLeft > 0 && $whatShifts != 2) || ($Instructor==1 && $OtherStuCount>0))
			{
				$Vari = new EventStuff;
				$SomeStartDate = mysql_result($secondsharedresultb, $j, "E.StartDate");
				$Vari->set_date($SomeStartDate);
				//$Vari->set_id(mysql_result($secondsharedresultb, $j, "E.Event_id"));
				$Vari->set_id($Event_id);
				$SomeStartTime = mysql_result($secondsharedresultb, $j, "E.StartTime");
				//echo "StartTime is: $SomeStartTime<br>\n";
				$Vari->setSTime((int)$SomeStartTime);
				array_push($SecondSharedArrayb, $Vari);
			}
		}

		if(($Instructor != 1) && ($seclist != "" && $seclist != -1))
		{
			//echo "before third<br>\n";
			$thirdresult = mysql_query($ThirdSelect, $dbConnect);
			$thirdcount = mysql_num_rows($thirdresult);
			//echo "third count is : $thirdcount<br>\n";
			for($j=0; $j<$thirdcount; $j++)
			{
				$Event_id = mysql_result($thirdresult, $j, "E.Event_id");
				//echo "$Event_id<br>";
				$SlotsLeft = calcOpenSlots($Event_id, $dbConnect);
				//$SlotsLeft = 1;

				$TradeSlotSelect = "SELECT Shift_id FROM ShiftData WHERE Event_id=$Event_id AND Trade=1 AND TradeStatus=3";
				$TradeSlotResult = mysql_query($TradeSlotSelect, $dbConnect);
				$TradeSlotCount = mysql_num_rows($TradeSlotResult);

				$SlotsLeft = $SlotsLeft + $TradeSlotCount;

				if($SlotsLeft > 0)
				{
					$Vari = new EventStuff;
					$SomeStartDate = mysql_result($thirdresult, $j, "E.StartDate");
					$Vari->set_date($SomeStartDate);
					//$Vari->set_id(mysql_result($thirdresult, $j, "E.Event_id"));
					$Vari->set_id($Event_id);
					$SomeStartTime = mysql_result($thirdresult, $j, "E.StartTime");
					//echo "StartTime is: $SomeStartTime<br>\n";
					$Vari->setSTime((int)$SomeStartTime);
					array_push($ThirdArray, $Vari);
				}
			}

			//echo "before third shared<br>\n";
			$thirdsharedresult = mysql_query($SharedThird, $dbConnect);
			$thirdsharedcount = mysql_num_rows($thirdsharedresult);
			//    echo "third<br>\n";
			for($j=0; $j<$thirdsharedcount; $j++)
			{
				$Event_id = mysql_result($thirdsharedresult, $j, "E.Event_id");
				//    echo "$Event_id<br>";
				$SlotsLeft = calcOpenSlots($Event_id, $dbConnect);
				//$SlotsLeft = 1;

				$TradeSlotSelect = "SELECT Shift_id FROM ShiftData WHERE Event_id=$Event_id AND Trade=1 AND TradeStatus=3";
				$TradeSlotResult = mysql_query($TradeSlotSelect, $dbConnect);
				$TradeSlotCount = mysql_num_rows($TradeSlotResult);

				$SlotsLeft = $SlotsLeft + $TradeSlotCount;

				if($SlotsLeft > 0)
				{
					$Vari = new EventStuff;
					$SomeStartDate = mysql_result($thirdsharedresult, $j, "E.StartDate");
					$Vari->set_date($SomeStartDate);
					//$Vari->set_id(mysql_result($thirdsharedresult, $j, "E.Event_id"));
					$Vari->set_id($Event_id);
					$SomeStartTime = mysql_result($thirdsharedresult, $j, "E.StartTime");
					//echo "StartTime is: $SomeStartTime<br>\n";
					$Vari->setSTime((int)$SomeStartTime);
					array_push($ThirdSharedArray, $Vari);
				}
			}

			$thirdsharedresultb = mysql_query($SharedThirdb, $dbConnect);
			$thirdsharedcountb = mysql_num_rows($thirdsharedresultb);
			//    echo "third<br>\n";
			for($j=0; $j<$thirdsharedcountb; $j++)
			{
				$Event_id = mysql_result($thirdsharedresultb, $j, "E.Event_id");
				//    echo "$Event_id<br>";
				$SlotsLeft = calcOpenSlots($Event_id, $dbConnect);
				//$SlotsLeft = 1;

				$TradeSlotSelect = "SELECT Shift_id FROM ShiftData WHERE Event_id=$Event_id AND Trade=1 AND TradeStatus=3";
				$TradeSlotResult = mysql_query($TradeSlotSelect, $dbConnect);
				$TradeSlotCount = mysql_num_rows($TradeSlotResult);

				$SlotsLeft = $SlotsLeft + $TradeSlotCount;

				if($SlotsLeft > 0)
				{
					$Vari = new EventStuff;
					$SomeStartDate = mysql_result($thirdsharedresultb, $j, "E.StartDate");
					$Vari->set_date($SomeStartDate);
					//$Vari->set_id(mysql_result($thirdsharedresultb, $j, "E.Event_id"));
					$Vari->set_id($Event_id);
					$SomeStartTime = mysql_result($thirdsharedresultb, $j, "E.StartTime");
					//echo "StartTime is: $SomeStartTime<br>\n";
					$Vari->setSTime((int)$SomeStartTime);
					array_push($ThirdSharedArrayb, $Vari);
				}
			}
		}
	}

	if($whatShifts != 1)
	{
	//echo "<br>select: $select<br>\n";
		$OtherResult = mysql_query($select, $dbConnect);
		$OtherCount = mysql_num_rows($OtherResult);

		$numShiftsSelected = $OtherCount;
		for($i=0; $i<$OtherCount; $i++)
		{
			$vari = new EventStuff;
			$vari->set_date(mysql_result($OtherResult, $i, "S.StartDate"));
			$vari->set_id(mysql_result($OtherResult, $i, "S.Event_id"));
			$vari->setShift_id(mysql_result($OtherResult, $i, "S.Shift_id"));
			$SomeStartTime = mysql_result($OtherResult, $i, "S.StartTime");
			$vari->setSTime((int)$SomeStartTime);

			//echo "Shift_ id is: $vari->ShiftId<br>\n";
			array_push($AddArray, $vari);
		}

	}

	if($numEventsSelected < 1001)
	{
		if($numShiftsSelected < 1001)
		{

			if($Instructor != 1)
			{
				$TempFinalArray1 = MySort($SecondArray,$ThirdArray);
				$TempFinalArray2b = MySort($TempFinalArray1,$ThirdSharedArray);
				$TempFinalArray2 = MySort($TempFinalArray2b,$ThirdSharedArrayb);
				$TempFinalArray3 = MySort($FullArray,$TempFinalArray2);
				//We need to add the third shared array here.  
			}
			else
			{
				$TempFinalArray3 = MySort($FullArray, $SecondArray);
			}
			//$TempFinalArray4 = array_unique($TempFinalArray3);
			//$FinalArray = array_values($TempFinalArray4);
			//$FinalArray = array_unique($TempFinalArray3);
			$TempFinalArray4 = MySort($SharedFirstArray,$TempFinalArray3);
			$TempFinalArray4b = MySort($TempFinalArray4,$SecondSharedArrayb);
			$TempFinalArray5 = MySort($SecondSharedArray,$TempFinalArray4b);
			$FinalArray = MySort($TempFinalArray5, $AddArray);
		}
	}
	return $FinalArray;
}
		?>
