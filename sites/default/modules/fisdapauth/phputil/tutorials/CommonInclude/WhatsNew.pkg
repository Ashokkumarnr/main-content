<refentry id="{@id}">
	<info>
		<author><firstname>Ian</firstname> <surname>Young</surname></author>
	</info>
	<refnamediv>
		<refname>What's New in CommonForm</refname>
		<refpurpose>Differences you can expect to see when migrating from common_form (the legacy package) to CommonForm (the new package)</refpurpose>
	</refnamediv>
	<refsynopsisdiv>
		TODO
	</refsynopsisdiv>
	{@toc}
	<refsect1 id="{@id modes}">
		<title>Modes and display</title>
		<para>
			Modes are no more. Modes have been replaced with {@link CommonFormBase::$submission_technique submission_technique}, {@link CommonFormBase::$default_promptvalue_mode default_promptvalue_mode}, and display behavior controlled by the developer.
		</para>
		<para>
			<code>process()</code> will no longer display the form automatically, ever. This means the script must check to see if the form is valid and redisplay it if necessary. This can either be done manually, or with the assistance of {@link CommonForm::redisplay_form()}.
		</para>
		<para>
			Delete mode behavior can be reproduced by simply adding a delete button to a form, like so:
			TODO
		</para>
		<para>
			TODO no more set_div_listener
		</para>
		<para>
			<title>Examples</title>
			<programlisting role="php">
				<![CDATA[
				//TODO
				]]>
			</programlisting>
		</para>
	</refsect1>
	<refsect1 id="{@id prompts}">
		<title>Old Prompts</title>
		<para>
			Old prompts (that is, common_prompt objects) may be given to new CommonForm. This is only to ease the pain of transitioning. If you are thoroughly (re)writing a page, you should be creating CommonPrompt objects instead.
		</para>
		<para>
			When CommonForm receives a common_prompt object, it goes into "legacy mode." This mode tries to replicate the quirky behavior of old common_form so that pages will work like they used to. Still, any page you update <emphasis>must</emphasis> be tested. Pages that used "delete mode" {@link http://trac.fisdapoffice.int/trac/fisdap/ticket/1141 very likely will not work}.
		</para>
	</refsect1>
	<refsect1 id="{@id buttons}">
		<title>Buttons</title>
		TODO no more set_submitvalue()
	</refsect1>
	<refsect1 id="{@id session}">
		<title>Session Values</title>
		<para>
			You should not retrieve the submitted values of CommonPrompts from session space. You should get them directly from {@link CommonPrompt::get_promptvalue()} (generally this means keeping a copy of your form lying around and using TODO).
		</para>
		<para>
			If a form is in legacy mode, it will still store prompt values in session space under the prompt names. Don't write new code that relies on this, though.
		</para>
	</refsect1>
	<refsect1 id="{@id methods}">
		<title>New and Changed Methods</title>
		<itemizedlist>
			<listitem>{@link CommonFormBase::get_html()} is replacing get_generated_html_form(). The old method will remain in existence for legacy support.</listitem>
			<listitem>common_prompt's set_promptset() does not exist on new CommonPrompt objects. Instead, each of the prompt objects that accepts multiple options has its own add_prompt_option() method, with context-appropriate parameters.</listitem>
		</itemizedlist>
	</refsect1>
	<refsect1 id="{@id domainprompts}">
		<title>Special Prompts</title>
		Date and time prompts now take FisdapDate and FisdapTime objects as values.

		The domain prompts are entirely new. They should have reasonably similar functionality. Take a look at {@link DomainPrompts.inc the documentation} if you're planning on using them - it should be much more complete now.
	</refsect1>
	<refsect1 id="{@id misc}">
		<title>Miscellaneous</title>
		<refsect2 id="{@id formnames}">
			<title>Names are required</title>
			Forms are now required to have a name set with {@link CommonFormBase::set_name()}. This name must be unique within the page.
		</refsect2>
	</refsect1>
</refentry>
