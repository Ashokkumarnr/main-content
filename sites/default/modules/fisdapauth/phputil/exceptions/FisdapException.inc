<?php
require_once('phputil/exceptions/FisdapBaseException.inc');

/**
 * A generic Fisdap exception
 */
class FisdapException extends FisdapBaseException {
}
?>
