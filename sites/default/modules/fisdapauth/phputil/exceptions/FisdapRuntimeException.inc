<?php
require_once('phputil/exceptions/FisdapBaseException.inc');

/**
 * A runtime exception.
 *
 * Runtime exceptons should <b>never</b> be caught.
 */
class FisdapRuntimeException extends FisdapBaseException {
}
?>
