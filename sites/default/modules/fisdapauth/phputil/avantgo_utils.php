<?php 

/**
 * Utility functions to help with identifying and 
 * dealing with AvantGo clients versus ordinary
 * online Web traffic.
 */

/**
 * Return true if the client is using AvantGo to view the 
 * current page, or false otherwise.
 *
 * @todo come up with a better regex for finding avantgo user agents
 *
 * @return bool true if the client is AvantGo, false otherwise
 */
function is_avantgo_client() {
    $user_agent = ($_SERVER['HTTP_USER_AGENT']) ? 
        strtolower($_SERVER['HTTP_USER_AGENT']) : '';
    if ( !$user_agent ) die('Couldn\'t get HTTP User-Agent header.');
    $avantgo_pattern = '/avantgo/';
    return preg_match($avantgo_pattern,$user_agent);
}//is_avantgo_client


/**
 * Returns a full URL for the Palm OS M-Business Anywhere installer
 */
function get_mbiz_palm_download_link() {
    $pda_hostname = get_pda_hostname();
    $palm_os_client_url = 'http://' 
                        . $pda_hostname 
                        . ':8091/admin/install/6_5_216/desktop/MBEntPalmEN_6_5_216.exe';
//	$palm_os_client_url = 'https://www.fisdap.net/~mjohnson/MBEntPalmEN_6_2_146.exe';
    return $palm_os_client_url;
}//get_mbiz_palm_download_link


/**
 * Returns a string containing the filename and size information for the 
 * Palm OS M-Biz installer
 */
function get_mbiz_palm_download_details() {
    return 'MBEntPalmEN_6_5_216.exe 4.8 MB (5,000,305 bytes)';
}//get_mbiz_palm_download_details


/**
 * Returns a string containing the filename and size information for the 
 * Palm OS M-Biz installer
 */
function get_mbiz_pocketpc_download_details() {
    return 'MBEntPPCEN_6_5_216.exe 5.1 MB (5,347,891 bytes)';
}//get_mbiz_pocketpc_download_details


/**
 * Returns a full URL for the PocketPC M-Business Anywhere installer
 */
function get_mbiz_pocketpc_download_link() {
    $pda_hostname = get_pda_hostname();
    $pocketpc_client_url = 'http://' 
                         . $pda_hostname 
                         . ':8091/admin/install/6_5_216/desktop/MBEntPPCEN_6_5_216.exe'; 
    return $pocketpc_client_url;
}//get_mbiz_pocketpc_download_link


/**
 * Return the pda hostname corresponding to the current server's 
 * hostname.
 *
 * @return string the pda hostname corresponding to $_SERVER['HTTP_HOST']
 */
function get_pda_hostname() {
    switch ( strtolower($_SERVER['HTTP_HOST']) ) {

        case "dev4.fisdapoffice.int":
            return "pda.fisdapoffice.int";

        case "dev5.fisdapoffice.int":
            return "pda.fisdapoffice.int"; 

        case "new.fisdap.net":
            return "pda.fisdap.net"; 

        case "www.fisdapoffice.com":
            return "pda.fisdapoffice.int";

        case "localdev":
            return "localdev";

        default:
            return 'pda.fisdap.net';
    }//switch
}//get_pda_hostname


///////////////////////////////////////////////////////////////////////////
// Avantgo Server Group Constants
//
// (note: these MUST be defined after the definition of the function
// get_pda_hostname above)
///////////////////////////////////////////////////////////////////////////

// unfortunately, the development and production servers have different
// group IDs, so we have to split the logic here
if ( get_pda_hostname() != 'pda.fisdap.net' ) {
    // use my special group on the development server, as well as the
    // virtualized test server
    define('AVANTGO_EMS_TRACKING_GROUP_ID','7');
    define('AVANTGO_OLD_PRIMARY_XML_GROUP_ID','10');
    define('AVANTGO_OLD_PRIMARY_GROUP_ID','1');
    define('AVANTGO_POCKETPC_GROUP_ID','2');
    define('AVANTGO_PALM_OS_GROUP_ID','3');
} else {
    // set the tracking group on the production server
    define('AVANTGO_EMS_TRACKING_GROUP_ID','6');
    define('AVANTGO_OLD_PRIMARY_XML_GROUP_ID','5');
    define('AVANTGO_OLD_PRIMARY_GROUP_ID','1');
    define('AVANTGO_POCKETPC_GROUP_ID','3');
    define('AVANTGO_PALM_OS_GROUP_ID','2');
}//else

?>
