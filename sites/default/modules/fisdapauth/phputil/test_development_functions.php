<?php

function getActiveStatus($UserAuth_id)
{
    $db =& FISDAPDatabaseConnection::get_instance();
    
    $sel = "SELECT ID.ActiveReviewer ".
           "FROM InstructorData ID, UserAuthData UD ".
           "WHERE UD.idx=$UserAuth_id ".
           "AND UD.email=ID.UserName";
    $status_array = $db->query($sel);
    $status = $status_array[0]["ActiveReviewer"];

    return $status;
}

function setActiveStatus($UserAuth_id,$newStatus) {
    
    $db =& FISDAPDatabaseConnection::get_instance();
    
    $sel = "SELECT email ".
           "FROM UserAuthData ".
           "WHERE idx=$UserAuth_id";
    $username_array = $db->query($sel);
    $username = $username_array[0]["email"];

    $update = "UPDATE InstructorData ".
              "SET ActiveReviewer='$newStatus' ".
              "WHERE UserName='$username' ".
              "limit 1";
    $updateResult = $db->query($update);
}

function getReviewerNotes($UserAuth_id)
{
    $db =& FISDAPDatabaseConnection::get_instance();
    
    $sel = "SELECT ID.ReviewerNotes ".
           "FROM InstructorData ID, UserAuthData UD ".
           "WHERE UD.idx=$UserAuth_id ".
           "AND UD.email=ID.UserName";
    $notes_array = $db->query($sel);
    $notes = $notes_array[0]["ReviewerNotes"];

    return $notes;
}


function setReviewerNotes($UserAuth_id, $NewNotes) {
    
    $db =& FISDAPDatabaseConnection::get_instance();
    
    $sel = "SELECT email ".
           "FROM UserAuthData ".
           "WHERE idx=$UserAuth_id";
    $username_array = $db->query($sel);
    $username = $username_array[0]["email"];

    $update = "UPDATE InstructorData ".
              "SET ReviewerNotes='$NewNotes' ".
              "WHERE UserName='$username' ".
              "limit 1";
    $updateResult = $db->query($update);
}

?>
