<?php
/**
 * @package UnitTesting
 * @author Ian Young
 */

/**
 * Convenience methods to be used while testing.
 */
class Tester {

	static protected $stopwatch;

	/**
	 * Object construction helper
	 *
	 * Build an object with a variable number of parameters
	 * @param string $objname The name of the object
	 * @param array $params An array of any number of parameters to be
	 * passed to the object's constructor.
	 */
	static public function object_construct($objname, $params) {

		if (!is_array($params)) {
			$params = array($params);
		}
		// Crazy PHP class - this lets us build an object with variable # params
		$ref = new ReflectionClass($objname);
		$obj = $ref->newInstanceArgs($params);
		return $obj;
	}

	static public function start_timer() {
		self::$stopwatch = new TesterStopwatch();
	}

	static public function poll_timer() {
		if (self::$stopwatch instanceof TesterStopwatch) {
			return self::$stopwatch->elapsed();
		} else {
			return null;
		}
	}

	static public function stop_timer() {
		$time = Tester::poll_timer();
		self::$stopwatch = null;
		return $time;
	}

}

/**
 * Acts like a stopwatch.
 *
 * You can start it, get a split, or reset it.  Uses microsecond granularity.
 */
class TesterStopwatch {

	/**
	 * @var float
	 */
	protected $start_microtime;

	public function __construct() {
		$this->reset();
	}

	/**
	 * Returns the time elapsed since the last reset.
	 *
	 * @param boolean $float If true, returns the value in float form (i.e.
	 * provides microsecond information. If false, returns an int.
	 * @return float|int The number of seconds elapsed
	 */
	public function elapsed($float=true) {
		$curr = microtime(true);
		$time = $curr - $this->start_microtime;
		if (!$float) {
			$time = (int)round($time);
		}
		return $time;
	}

	public function reset() {
		$this->start_microtime = microtime(true);
	}

}
