<?php 
  // define constants for the bls skill types 

  define('HOSPITAL_NOTIFY','1');
  define('MD_CONSULT','2');
  define('PATIENT_INTERVIEW','3');
  define('PATIENT_EXAM','4');
  define('VITAL_SIGNS','5');
  define('SUCTION','6');
  define('L_BOARD_IMMOBILIZE','7');
  define('PATIENT_MOVED_LIFTED','8');
  define('VENTILATION','9');
  define('TRACTION_SPLINT','10');
  define('BANDAGING','11');
  define('CHEST_COMPRESSIONS','12');
  define('JOINT_IMMOBILIZATION','13');
  define('BLS_AIRWAY','14');
  define('C_SPINE_IMMOBILIZATION','15');
  define('LONG_BONE_IMMOBILIZATION','16');

  // map bls skill codes to their respective constants
  // (the reverse of the above), for convenience
  $bls_skills_constants = array(
      HOSPITAL_NOTIFY => 'HOSPITAL_NOTIFY',
      MD_CONSULT => 'MD_CONSULT',
      PATIENT_INTERVIEW => 'PATIENT_INTERVIEW',
      PATIENT_EXAM => 'PATIENT_EXAM',
      VITAL_SIGNS => 'VITAL_SIGNS',
      SUCTION => 'SUCTION',
      L_BOARD_IMMOBILIZE => 'L_BOARD_IMMOBILIZE',
      PATIENT_MOVED_LIFTED => 'PATIENT_MOVED_LIFTED',
      VENTILATION => 'VENTILATION',
      TRACTION_SPLINT => 'TRACTION_SPLINT',
      BANDAGING => 'BANDAGING',
      CHEST_COMPRESSIONS => 'CHEST_COMPRESSIONS',
      JOINT_IMMOBILIZATION => 'JOINT_IMMOBILIZATION',
      BLS_AIRWAY => 'BLS_AIRWAY',
      C_SPINE_IMMOBILIZATION => 'C_SPINE_IMMOBILIZATION',
      LONG_BONE_IMMOBILIZATION => 'LONG_BONE_IMMOBILIZATION'
  );
?>
