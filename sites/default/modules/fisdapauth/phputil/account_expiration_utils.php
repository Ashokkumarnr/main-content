<?php
require_once('phputil/classes/common_user.inc');

$connection = FISDAPDatabaseConnection::get_instance();

function print_grad_message($username = null, $scheduler = false, $script = false) {
	global $connection;

	$user = new common_user($username);
	$student_id = $user->get_student_id();

	$year = date('Y');
	$month = date('m');

	if ($student_id) {

		$query = "SELECT * FROM StudentData WHERE Student_id = $student_id";
		$student = $connection->query($query);

		if (count($student) == 1) {
			$student = $student[0];
			$class_year = $student['Class_Year'];
			$class_month = $student['ClassMonth'];
			$good_data = $student['GoodDataFlag'];
		}

		if (!isset($class_year) || !isset($class_month) || $good_data != 0 || $year > $class_year || ($year == $class_year && $month > $class_month)) {

			if ($script) {
				echo "</script>";
			}

			echo "</HEAD><BODY bgcolor='#FFFFFF'><center><br><br>\n";
			echo "<h2 style='font-family:\"Arial\",sans-serif;'>Your account is no longer active.</h2><br>\n";
			echo "<div style='text-align:justify; width:40%;font-family:\"Arial\",sans-serif;'>";

			if ($good_data == 0) {
				echo "<p>It looks like you graduated in <b>$class_month/$class_year</b>. ";

				if ($scheduler == true) {
					echo "You may continue to view your data, but you may not access the Scheduler. ";
				} else {
					echo "You may continue to view your patient care reports, but you will no longer be able to change or add to them. ";
				}
				echo "If you have not yet graduated, please ask your instructor to reset your graduation date.</p>";
			} else {
				if ($good_data == 3) {
					$grad_desc = 'failed to graduate';
				} else if ($good_data == 4) {
					$grad_desc = 'left the program';
				} else {
					$grad_desc = 'graduated';
				}
				echo "<p>It looks like your instructor indicated that you $grad_desc. ";
				if ($scheduler == true) {	
					echo "You may continue to view your data, but you may not access the Scheduler. ";
				} else {
					echo "You may continue to view your patient care reports, but you will no longer be able to change them. ";
				}
				echo "If you need help reactivating your account, please contact your instructor directly.</p>";
			}
			echo "</div>";
			echo "<br><br>\n";
			echo "</center></body></html>";
			exit;

		}
	}
}








?>
