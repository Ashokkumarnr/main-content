<?php

/**
 * This file defines the function get_research_students(), which returns
 * an array of student IDs for each student with viable research data.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *  
 *                                                                           *
 *                                                                           *
 * @author Sam Martin <smartin@fisdap.net>                                   *
 * @copyright 1996-2007 Headwaters Software, Inc.                            *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 */


/**
 * Return a WHERE clause to select the current research students.  Note
 * that the query *must* select from both StudentData and SerialNumbers.
 *
 * @todo make this function automagically generate the appropriate date
 *       range
 */
function get_research_student_where_clause()
{
    return 'StudentData.Student_id = SerialNumbers.Student_id '
        .  'AND '
        .  'StudentData.GoodDataFlag = "1" '
        .  'AND '
        .  'StudentData.Class_Year > "2000" '
        .  'AND '
        .  'StudentData.Class_Year < "2007" '
        .  'AND '
        .  'StudentData.ResearchConsent = "yes" '
        .  'AND '
        .  'StudentData.Program_id != "3" '
        .  'AND '
        .  'SerialNumbers.AccountType = "paramedic" '
		.  'AND '
		.  '(SerialNumbers.Configuration & 1)';
}//get_research_student_where_clause


/**
 *
 */
function get_research_programs() {
    $connection =& FISDAPDatabaseConnection::get_instance();
    $query = 'SELECT '
        .    'StudentData.Program_id '
        .    ' '
        .    'FROM '
        .    'StudentData, '
        .    'SerialNumbers '
        .    ' '
        .    'WHERE '
        .    get_research_student_where_clause()
        .    ' '
        .    'GROUP BY '
        .    'StudentData.Program_id';

    $result = $connection->query($query);
    if ( !$result || !is_array($result) ) {
        return false;
    }//if

    $programs = array();
    foreach ( $result as $row ) {
        $programs[] = $row['Program_id'];
    }//foreach

    return $programs;
}//get_research_programs


/**
 *
 */
function get_research_students() {
    $connection =& FISDAPDatabaseConnection::get_instance();
    $query = 'SELECT '
        .    'StudentData.Student_id '
        .    ' '
        .    'FROM '
        .    'StudentData, '
        .    'SerialNumbers '
        .    ' '
        .    'WHERE '
        .    get_research_student_where_clause();

    $result = $connection->query($query);
    if ( !$result || !is_array($result) ) {
        return false;
    }//if

    $students = array();
    foreach ( $result as $row ) {
        $students[] = $row['Student_id'];
    }//foreach

    return $students;
}//get_research_students

?>
