<?php

/**
 * This file defines constants for the IDs of the various Med names
 */

define('MED_ADENOSINE'            , '55'); 
define('MED_ALBUTEROL'            ,  '1'); 
define('MED_ATROPINE'             ,  '2'); 
define('MED_TERBUTALINE'          ,  '3'); 
define('MED_BRETYLIUM'            ,  '4'); 
define('MED_CALCIUM'              ,  '5'); 
define('MED_DEXTROSE_50W'         ,  '6'); 
define('MED_GLUTOSE'              ,  '7'); 
define('MED_BENADRYL'             ,  '8'); 
define('MED_DOPAMINE'             ,  '9'); 
define('MED_EPINEPHRINE_1_1000'   , '10');
define('MED_EPINEPHRINE_1_10000'  , '11');
define('MED_GLUCAGON'             , '12');
define('MED_LASIX'                , '13');
define('MED_LIDOCAINE'            , '14');
define('MED_MORPHINE_SULFATE'     , '15');
define('MED_NARCAN'               , '16');
define('MED_NITROGLYCERIN'        , '17');
define('MED_NITROUS_OXIDE'        , '18');
define('MED_SODIUM_BICARBONATE'   , '19');
define('MED_SYRUP_OF_IPECAC'      , '20');
define('MED_VALIUM'               , '21');
define('MED_VERAPAMIL'            , '22');
define('MED_VERSED'               , '23');
define('MED_PROPARACAINE'         , '24');
define('MED_OXYGEN'               , '25');
define('MED_TORADOL'              , '26');
define('MED_DILANTIN'             , '27');
define('MED_PROCARDIA'            , '28');
define('MED_STREPTO_TPA'          , '29');
define('MED_ASPIRIN'              , '30');
define('MED_ACTIVATED_CHARCOAL'   , '31');
define('MED_TETRACAINE'           , '32');
define('MED_PARALYTICS'           , '33');
define('MED_ATROVENT'             , '34');
define('MED_DEMEROL'              , '35');
define('MED_HEPARIN'              , '36');
define('MED_LABETALOL'            , '37');
define('MED_MAGNESIUM'            , '38');
define('MED_PITOCIN'              , '39');
define('MED_ROMAZICON'            , '40');
define('MED_SOLU_MEDROL'          , '41');
define('MED_THIAMINE'             , '42');
define('MED_OTHER'                , '43');
define('MED_INAPSINE'             , '44');
define('MED_AMIODERONE'           , '45');
define('MED_VASOPRESSIN'          , '46');
define('MED_HALDOL'               , '47');
define('MED_NUBAIN'               , '48');
define('MED_PHENERGAN'            , '49');
define('MED_ANTIBIOTICS'          , '50');
define('MED_TETANUS'              , '51');
define('MED_DILTIAZEM'            , '52');
define('MED_XOPENEX'              , '53');
define('MED_FENTANYL'             , '54');

?>
