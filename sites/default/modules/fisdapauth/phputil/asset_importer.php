<?php
require_once('phputil/start_fisdap_session.php');
require_once('phputil/classes/model/NemsesObjective.inc');
require_once('phputil/handy_utils.inc');
error_reporting(E_ALL);
// Don't buffer output, we're a shell script
ob_end_flush();

/**
 * Print ascii text to stdout to show progress?
 */
$show_progress = false;

if ($argc < 2) {
	if (strpos($argv[0], basename(__FILE__)) !== false) {
		echo 'Usage: php '.$argv[0].' filename.tsv'."\n";
	}
} else {
	$show_progress = true;
	main($argv);
}

function main($argv) {
	global $show_progress;
	global $stack;
	$stack = array();

	$filename = $argv[1];
	$fhandle = fopen($filename, "r");

	if ($fhandle === false) {
		echo "Failed! Couldn't open file.\n";
		exit;
	}

	$line_count = 0;
	while (!feof($fhandle)) {
		$line_count++;
		$line = fgets($fhandle);
		try {
			parse_line($line);
		} catch (FisdapException $e) {
			throw new FisdapException("Line $line_count: " . $e->getMessage(), 0, $e);
		}
	}

	if ($show_progress) {
		echo "Success!\n";
		echo "Top level Objective_id=" . $stack[0][0]->get_id() . "\n";
	}
}

function parse_line($line) {
	global $show_progress;
	global $stack;

	// Skip empty lines
	if (trim($line) == '') {
		return;
	}
	// Build and run a regular expression against the line
	// Expect tabs, then a description
	$re_desc = '^(\t*)([^\t]+)';
	// Now numbers (or blank spots) separated by tabs
	$re_ratings = '(\t*)\t(\d?)\t(\d?)\t(\d?)\t(\d?)\t(\d?)\t(\d?)\t(\d?)\t(\d?)$';
	$re = "/$re_desc$re_ratings/";
	$matches = array();
	if (preg_match($re, $line, $matches) == 0) {
		echo 'Unrecognized line: '.$line;
		return;
	}

	// Now pull out the values we want to know about
	$depth = strlen($matches[1]);
	$name = $matches[2];
	// Sanity check: see if the tabs before and after the name add up to 5
	Assert::is_true(($depth + strlen($matches[3]) == 5));
	$depths = array(
		$matches[4],
		$matches[6],
		$matches[8],
		$matches[10],
	);
	$breadths = array(
		$matches[5],
		$matches[7],
		$matches[9],
		$matches[11],
	);

	static $curr_depth = -1;

	// See if we've gone down or up levels of indentation
	if ($depth > $curr_depth) {
		// Make sure the indentation has only increased one level
		if ($depth != $curr_depth + 1)
			throw new FisdapException("Indentation jumped by more than one");
		// Now pop a new level onto the stack
		$stack[$depth] = array();
		$curr_depth = $depth;
	} else if ($depth < $curr_depth) {
		// Pop levels off the stack until we get back up to where we wanted to be
		while ($depth < $curr_depth) {
			unset($stack[$curr_depth]);
			$curr_depth--;
		}
	}

	// If we're not at the top level, find the parent
	$parent = null;
	if ($curr_depth > 0) {
		$parent = HandyArrayUtils::array_peek($stack[$curr_depth - 1]);
	}

	// Now build the objective and push it onto the stack
	$objective = build_objective($name, $parent, $breadths, $depths);
	$objective->save();
	$stack[$curr_depth][] = $objective;

	if ($show_progress) echo '.';

}

function build_objective($name, $parent, $breadths, $depths) {

	$objective = new NemsesObjective();

	$objective->set_name($name);
	$objective->set_description($name);

	// If it has a parent, set that
	if ($parent) {
		$objective->set_parent($parent);
	}

	// Set the breadth/depth stuff
	$rates = array(
		0 => 'emr',
		1 => 'emt',
		2 => 'aemt',
		3 => 'paramedic',
	);
	foreach ($rates as $i => $level) {
		$b = $breadths[$i];
		$d = $depths[$i];
		if ($b === "0" || $i === "0") {
			// Sanity check: both should equal zero
			Assert::is_true($b == $d);
			$objective->set_applies_to($level, false);
		} else {
			$objective->set_applies_to($level, true);
			$objective->set_breadth($level, $b);
			$objective->set_depth($level, $d);
		}
	}

	return $objective;
}
