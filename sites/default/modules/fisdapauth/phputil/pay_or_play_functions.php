<?php
/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2008.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/

$connection =& FISDAPDatabaseConnection::get_instance();
$dbConnect = $connection->get_link_resource();

require_once('instlist.html'); // gives us fetch_instructors function
require_once('fisdap_staff_lib.php'); // gives us is_fisdap_staff_member function
require_once('classes/common_user.inc'); // gives us is_fisdap_staff_member function

/*
 * CONSTANTS
 */
define('ITEM_CONTRIBUTION', 2);
define('ITEM_VALIDATED', 4);
define('INDIVIDUAL_REVIEW', 3);
define('CONSENSUS_REVIEW', 15);

define('TEN_OFF', 50);
define('TWENTYFIVE_OFF', 100);
define('FIFTY_OFF', 300);
define('SEVENTYFIVE_OFF', 600);
define('HUNDRED_OFF', 1000);

define('OLD_PROJECTS', "-1,1,4,5,7,8,9");

/*
 * This function will return an array of points a program has accumulated,
 * broken down by instructor, program bonus and spent, and total.
 */
function fetch_program_points_array($Program_id) {
	$points_array = array();
	$inst_points_array = array();
	$allFlag = true; // include deleted instructors, too
	$instructors = fetch_instructors($Program_id,$allFlag);
	
	foreach ($instructors as $inst) {
		$tmp_name = $inst['FirstName']." ".$inst['LastName'];
		$tmp_uname = $inst['UserName'];
		$tmp_unameAr = array($tmp_uname);
		$tmp_id = $inst['Instructor_id'];
		$tmp_idAr = array($tmp_id);

/**
 * only count the points if the instructor is not one of us
 */
		if (!is_fisdap_staff_member($tmp_uname)) {
			$inst_points_array[$tmp_id]['fullname'] = $tmp_name;
			$inst_points_array[$tmp_id]['username'] = $tmp_uname;
			
			$item_count = fetch_donated_items($tmp_unameAr);
			$item_points = $item_count * ITEM_CONTRIBUTION;
			$inst_points_array[$tmp_id]['item_count'] = $item_count;
			$inst_points_array[$tmp_id]['item_points'] = $item_points;
//echo "$tmp_name: $item_points item points ($item_count)<br>";

			$valid_count = fetch_validated_items($tmp_unameAr);
			$valid_points = $valid_count * ITEM_VALIDATED;
			$inst_points_array[$tmp_id]['valid_count'] = $valid_count;
			$inst_points_array[$tmp_id]['valid_points'] = $valid_points;
//echo "$tmp_name: $valid_points validated points ($valid_count)<br>";

			$individual_review_count = fetch_individual_reviews($tmp_unameAr);
			$individual_review_points = $individual_review_count * INDIVIDUAL_REVIEW;
			$inst_points_array[$tmp_id]['individual_review_count'] = $individual_review_count;
			$inst_points_array[$tmp_id]['individual_review_points'] = $individual_review_points;
//echo "$tmp_name: $individual_review_points ind rev points ($individual_review_count)<br>";
		
			$consensus_count = fetch_consensus_reviews($tmp_idAr);
			$consensus_points = $consensus_count * CONSENSUS_REVIEW;
			$inst_points_array[$tmp_id]['consensus_count'] = $consensus_count;
			$inst_points_array[$tmp_id]['consensus_points'] = $consensus_points;
//echo "$tmp_name: $consensus_points consensus points ($consensus_count)<br>";
			
			$tmp_points = $item_points + 
						  $valid_points + 
						  $individual_review_points + 
						  $consensus_points;
			$inst_points_array[$tmp_id]['inst_points'] = $tmp_points;
//echo "$tmp_name: $tmp_points total points<br><br>";
		
			$total_points = $total_points + $tmp_points;
		}
	} // instructor loop

	$points_array['instructors'] = $inst_points_array;
	$points_array['total_inst_points'] = $total_points;
	$points_array['prog_bonus_points'] = fetch_bonus_points($Program_id, 'program');
	$points_array['spent_points'] = fetch_spent_points($Program_id);
	$points_array['points_balance'] = $points_array['total_inst_points'] + 
									  $points_array['prog_bonus_points'] + 
									  $points_array['spent_points'];
	return $points_array;
}

/*
 * This function will return the current points balance for a program
 */
function fetch_program_points_balance($Program_id) {
	
	$allFlag = true; // include deleted instructors, too
	$instructors = fetch_instructors($Program_id,$allFlag);
	$inst_ids = array();
	$inst_unames = array();
	
	// build the id and username arrays, making sure no FISDAP staff members
	// are added
	foreach ($instructors as $inst) {
		$tmp_id = $inst['Instructor_id'];
		$tmp_uname = $inst['UserName'];

		if (!is_fisdap_staff_member($tmp_uname)) {
			$inst_ids[] = $tmp_id;
			$inst_unames[] = $tmp_uname;
		}
	}
	
	$item_count = fetch_donated_items($inst_unames);
	$item_points = $item_count * ITEM_CONTRIBUTION;

	$valid_count = fetch_validated_items($inst_unames);
	$valid_points = $valid_count * ITEM_VALIDATED;

	$individual_review_count = fetch_individual_reviews($inst_unames);
	$individual_review_points = $individual_review_count * INDIVIDUAL_REVIEW;

	$consensus_count = fetch_consensus_reviews($inst_ids);
	$consensus_points = $consensus_count * CONSENSUS_REVIEW;
			
	$total_inst_points = $item_points +
						 $valid_points +
						 $individual_review_points +
						 $consensus_points;
	$prog_bonus_points = fetch_bonus_points($Program_id, 'program');
	$prog_spent_points = fetch_spent_points($Program_id);
	
	$points_balance = $total_inst_points +
					  $prog_bonus_points +
					  $prog_spent_points;

	return $points_balance;
}
 
/*
 * This function will return the quantity of items donated by the given user 
 */
function fetch_donated_items($unameArray) {
	global $connection;

	$unameString = "'".implode("','",$unameArray)."'";
	$select = "SELECT count(*) as count ".
			  "FROM UserAuthData UAD, AuthorData AD, ".
			  "Asset_def A, Item_def I ".
			  "WHERE UAD.email IN (".$unameString.") ".
			  "AND UAD.idx=AD.UserAuth_id ".
			  "AND AD.AssetDef_id=A.AssetDef_id ".
			  "AND A.PointEligibility & 1 ".
			  "AND A.Data_id=I.Item_id ".
			  "AND I.Project_id NOT IN (".OLD_PROJECTS.")";
//echo "<br>$select<br>";
	$result = $connection->query($select);
//var_export($result);
	if (!$result) {
		return 0;
	}
	return $result[0]['count'];
}

/*
 * This function will return the quantity of validated items donated by the given user 
 */
function fetch_validated_items($unameArray) {
	global $connection;

	$unameString = "'".implode("','",$unameArray)."'";
	$select = "SELECT count(*) as count ".
			  "FROM UserAuthData UAD, AuthorData AD, ".
			  "Asset_def A, Item_def I ".
			  "WHERE UAD.email IN (".$unameString.") ".
			  "AND UAD.idx=AD.UserAuth_id ".
			  "AND AD.AssetDef_id=A.AssetDef_id ".
			  "AND A.PointEligibility & 2 ".
			  "AND A.Data_id=I.Item_id ".
			  "AND I.ValidStatus=3 ".
			  "AND I.Project_id NOT IN (".OLD_PROJECTS.")";
//echo "<br>$select<br>";
	$result = $connection->query($select);
//var_export($result);
	if (!$result) {
		return 0;
	}
	return $result[0]['count'];

}

/*
 * This function will return the quantity of individual reviews completed by the given user 
 */
function fetch_individual_reviews($unameArray) {
	global $connection;

	$unameString = "'".implode("','",$unameArray)."'";
	$select = "SELECT count(*) as count ".
			  "FROM UserAuthData UAD, ReviewAssignmentData RAD, ".
			  "Asset_def A, Item_def I ".
			  "WHERE UAD.email IN (".$unameString.") ".
			  "AND UAD.idx=RAD.UserAuth_id ".
			  "AND RAD.DateReviewReceived!='0000-00-00' ".
			  "AND RAD.Active=1 ".
			  "AND RAD.AssetDef_id=A.AssetDef_id ".
			  "AND A.Data_id=I.Item_id ".
			  "AND I.Project_id NOT IN (".OLD_PROJECTS.")";
//echo "<br>$select<br>";
	$result = $connection->query($select);
//var_export($result);
	if (!$result) {
		return 0;
	}
	return $result[0]['count'];

}

/*
 * This function will return the quantity of concensus reviews attended by the given user 
 */
function fetch_consensus_reviews($inst_idArray) {
	global $connection;
	
	$inst_idString = "'".implode("','",$inst_idArray)."'";
	$select = "SELECT count(*) as count ".
			  "FROM ScheduledSessionSignups signup, ScheduledSessions session ".
			  "WHERE signup.Instructor_id IN (".$inst_idString.") ".
			  "AND signup.Attended=1 ".
			  "AND signup.ScheduledSession_id = session.ScheduledSession_id ".
			  "AND session.Type=1";
//echo "<br>$select<br>";
	$result = $connection->query($select);
//var_export($result);
	if (!$result) {
		return 0;
	}
	return $result[0]['count'];

}

/*
 * This function will return the total bonus points accrued by program 
 */
function fetch_bonus_points($Program_id) {
	global $connection;
	
	$today = date('Y-m-d');
	$select = "SELECT sum(Amount) as points ".
			  "FROM POPTransactions ".
			  "WHERE program_id=$Program_id ".
			  "AND amount>0 ".
			  "AND transaction_date<='$today'";
//echo "<br>$select<br>";
	$result = $connection->query($select);
//var_export($result);
	if (!$result || $result[0]['points']===NULL) {
		return 0;
	}
	return $result[0]['points'];

}

/*
 * This function will return the total points spent by a program to date 
 */
function fetch_spent_points($Program_id) {
	global $connection;

	$today = date('Y-m-d');
	$select = "SELECT sum(Amount) as points ".
			  "FROM POPTransactions ".
			  "WHERE program_id=$Program_id ".
			  "AND amount<0 ".
			  "AND transaction_date<='$today'";
//echo "<br>$select<br>";
	$result = $connection->query($select);
//var_export($result);
	if (!$result || $result[0]['points']===NULL) {
		return 0;
	}
	return $result[0]['points'];

}

/*
 * This function will return an array of transactions for the given
 * program that meet the other given requirements.
 */
function fetch_transaction_array($program_id,$mode,$startdate,$enddate) {
	global $connection;
	
	switch ($mode) {
		case 'bonus':
			$and_clause = 'AND amount>0 ';
			break;
		case 'spent':
			$and_clause = 'AND amount<0 ';
			break;
	}

	$select = "SELECT * FROM POPTransactions ".
			  "WHERE program_id=$program_id ".
			  "AND transaction_date>='$startdate' ".
			  "AND transaction_date<='$enddate' ".
			  $and_clause.
			  "ORDER BY transaction_date";
//echo $select;
	$result = $connection->query($select);
//var_export($result);
	return $result;
}

/**
 * This function will return an array containing information
 * about a program's discounts
 *
 * If $active is true, the function will only return discounts that are currently active
 *		if false, it will return ALL discounts, including expired ones
 * If $rewards is true, the function will only return discounts that are part of FISDAP Rewards
 * 		if false, it will return ALL discounts
 *
 */
function fetch_discounts($Program_id,$active,$rewards) {
	global $connection;

	$select = "SELECT Type,StartDate,EndDate,PercentOff,Configuration ".
			  "FROM PriceDiscount ".
			  "WHERE Program_id=$Program_id ".
			  "AND PercentOff>0 ";
	if($active) {
		$today = Date('Y-m-d');
		$select .= "AND StartDate <='$today' ".
				   "AND EndDate >= '$today' ";
	}
	if($rewards) {
		$select .= "AND Rewards = 1 ";
	}
	$select .= "ORDER BY EndDate";
//echo "<br>$select<br>";
	$result = $connection->query($select);
	if (!$result || $result===NULL) {
		return array();
	}
	return $result;
}

/*
 * Given a points balance, this function returns the largest discount
 * it is possible to buy
 */
function get_possible_discount($points_balance) {
	if ($points_balance>=TEN_OFF && $points_balance<TWENTYFIVE_OFF) {
		return 10;
	}
	if ($points_balance>=TWENTYFIVE_OFF && $points_balance<FIFTY_OFF) {
		return 25;
	}
	if ($points_balance>=FIFTY_OFF && $points_balance<SEVENTYFIVE_OFF) {
		return 50;
	}
	if ($points_balance>=SEVENTYFIVE_OFF && $points_balance<HUNDRED_OFF) {
		return 75;
	}
	if ($points_balance>=HUNDRED_OFF) {
		return 100;
	}
	else {
		return 0;
	}
}
 
?>
