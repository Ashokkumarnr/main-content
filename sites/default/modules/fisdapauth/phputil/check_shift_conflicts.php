<?php

function check_shift_conflicts($student, $startdate, $starttime, $duration) {
	global $dbConnect;
	
	$overlap=0;
	
	//seconds in a day
	$day_seconds = 60 * 60 * 24;
	
	$startStamp = get_shift_start_stamp($startdate,$starttime);
//	echo "startstamp is: $startStamp<br>\n";
	
	//convert the duration (hours) to seconds
//	echo "duration is $duration<br>\n";
	$durationSeconds = $duration * 60 * 60;
//	echo "duration seconds is $durationSeconds<br>\n";
	
	$endStamp = $startStamp + $durationSeconds;
	
	$prevStamp = $startStamp - $day_seconds;
	$nextStamp = $startStamp + $day_seconds;
	$notfound=1;
	$counter=0;
	$tmptime = $startStamp;
	while(($notfound==1) && ($counter<4)) {
		$t_stdate = date("Y-m-d",$tmptime);
		$tmpsel = "SELECT * FROM ShiftData WHERE Student_id=$student AND StartDate='$t_stdate' ORDER BY StartTime DESC";
//		echo "tmpsel is: $tmpsel<br>\n";
		$tmpres = mysql_query($tmpsel,$dbConnect);
		$tmpcount = mysql_num_rows($tmpres);
		if($tmpcount>0) {
			$tmp_st_stamp = get_shift_start_stamp(mysql_result($tmpres,0,"StartDate"),mysql_result($tmpres,0,"StartTime"));
			$tmp_dur = mysql_result($tmpres,0,"Hours");
			$tmp_dur_sec = $tmp_dur * 60 * 60;
			$tmp_end_stamp = $tmp_st_stamp + $tmp_dur_sec;
		//	echo "endstamp $tmp_end_stamp<br>\n";
		//	echo "start stamp $startStamp<br>\n";
			if($tmp_end_stamp > $startStamp) {
				if(!($tmp_st_stamp>$endStamp)) {
	//				echo "marking overlap 1<br>\n";
					$notfound=0;
					$overlap=1;
				}//if
			}//if
			else {
				$tmptime = $tmptime - $day_seconds;
			}//else
		}//if
		else {
			$tmptime = $tmptime - $day_seconds;
		}//else
		$counter++;
	}//while
	
	$notfound=1;
	$counter=0;
	$tmptime = $startStamp;
	while(($notfound==1) && ($counter<4)) {
		$t_stdate = date("Y-m-d",$tmptime);
		$tmpsel = "SELECT * FROM ShiftData WHERE Student_id=$student AND StartDate='$t_stdate' ORDER BY StartTime";
//		echo "tmpsel 2 is: $tmpsel<br>\n";
		$tmpres = mysql_query($tmpsel,$dbConnect);
		$tmpcount = mysql_num_rows($tmpres);
		if($tmpcount>0) {
			$tmp_st_stamp = get_shift_start_stamp(mysql_result($tmpres,0,"StartDate"),mysql_result($tmpres,0,"StartTime"));
			$tmp_dur = mysql_result($tmpres,0,"Hours");
			$tmp_dur_sec = $tmp_dur * 60 * 60;
			$tmp_end_stamp = $tmp_st_stamp + $tmp_dur_sec;
		//	echo "endstamp 2 $endStamp<br>\n";
		//	echo "start stamp 2 $tmp_st_stamp<br>\n";
			if($endStamp > $tmp_st_stamp) {
				if(!($startStamp>$tmp_end_stamp)) {
//					echo "marking overlap 2<br>\n";
					$notfound=0;
					$overlap=1;
				}//if
			}//if
			else {
				$tmptime = $tmptime + $day_seconds;
			}//else
		}//if
		else {
			$tmptime = $tmptime + $day_seconds;
		}//else
		$counter++;
	}//while
	return $overlap;
}//check_shift_conflicts

function get_shift_start_stamp($startdate,$starttime) {
	$dateArr = explode("-",$startdate);
	$startString = strval($starttime);
	
	if(strlen($startString)==3) {
		$st_hour = substr($startString,0,1);
		$st_min = substr($startString,1,2);
	}//if
	else {
		$st_hour = substr($startString,0,2);
		$st_min = substr($startString,2,2);
	}//else
//	echo "st hour $st_hour<br>\n";
//	echo "st min $st_min<br>\n";
	
//	echo "month " . $dateArr[1] . "<br>\n";
//	echo "day " . $dateArr[2] . "<br>\n";
//	echo "year " . $dateArr[0] . "<br>\n";
	return mktime($st_hour,$st_min,0,$dateArr[1],$dateArr[2],intval($dateArr[0]));	
}//get_shift_start_stamp

?>