<?php

require_once("Transaction.inc");
require_once('PreceptorTransaction.inc');
require_once("WorkshopTransaction.inc");
require_once("../fisdap_staff_lib.php");
require_once("../session_functions.php");
require_once("utils.php");

require_once("../require_valid_session.php");

require_staff_member();


/*
 * This page is an interface for the account ordering error logging system.
 * 
 * This page will allow someone to look at the errors (or just the information)
 * left behind after a Transaction object does its thing, without making that
 * someone dive into the code or the server. 
 */

//require valid session and staff and yadda 


echo "<html><head><title>Account Order Error Logs " . $_POST["fisdap_id"] . "</title></head><body>\n";
echo "<h2>Account Order Error Logs</h2>\n";
echo "Enter a FISDAP transaction ID into the box below to look up information about a previous account order transaction.\n";
echo "<form name='account_info' id='account_info' method='POST' action='$PHPSELF' >\n";
/*	$id_value = "Enter FISDAP ID";
	if (isset($_POST["fisdap_id"])) {$id_value = $_POST["fisdap_id"];} */
echo "<input type='text' id='fisdap_id' name='fisdap_id' value='$id_value'>\n";
echo "<input type='submit' id='submit' name='submit' value='Submit'>\n";
echo "<input type='hidden' id='submitted' name='submitted' value='true'>\n";

if ($_POST["submitted"] == true) {
	$id = trim($_POST["fisdap_id"]);

	if ($id == null || $id == "Enter FISDAP ID") {
		echo "<p class='alert'>Invalid FISDAP ID.</p>\n";
		die();
	}
	$file = get_file_name($id);
	echo $file;
	$exists = file_exists($file);
	if ($exists == false) {
		echo "<p class='alert'>I can find no file associated with that ID.</p>\n";
	}
	else {
		$dataArray = read_acct_data_to_array($id);

		for ($i = 0; $i < 100000; $i++) {
			$error = $dataArray["error" . $i];
			if (is_null($error)) {
				if ($i == 0) {
					echo "<h4>No errors! Transaction was a rousing success.</h4>\n";
				}
				break;					
			}
			else {
				echo "<h4>Error $i: $error</h4>\n";
			}
		}

		//			echo "<pre>"; print_r($dataArray); echo "</pre>";
		echo "<table border=1>\n";
		echo "<th>Key</th>\n";
		echo "<th align=left>Value</th>\n";
		foreach ($dataArray as $key=>$value) {
			if ($key != null) {
				echo "<tr><td valign=top>$key</td><td align=left><pre>";
				print_r($value);
				echo "</pre></td></tr>\n";
			}
		}
		echo "</table>";

		/*
		   echo "Test stuff<br>";
		   echo read_acct_data($id, "started");
		   echo read_acct_data($id, "core-type");
		 */
	}
}
?>
