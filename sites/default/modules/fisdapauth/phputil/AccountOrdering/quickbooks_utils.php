<?php
/**
 * These functions are used to produce files containing transaction data
 * that can be improted into Quickbooks via an .iif import file.   This file 
 * format should be replaced with the newer XML format.
 *
 *  The order they are used is as follows:
 *    QB_OpenFile             - Open the output file
 *    QB_WriteHeaders      - Creates the file and writes the header.
 *    QB_WriteTransaction - Writes the transaction line.
 *    QB_WriteDistribution - Writes a distribution for the transaction.
 *    QB_EndTransaction    - Ends the transaction.
 *    QB_CloseFile            - Writes the footer and closes the file.
 *    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @package default
 * @author Mike Johnson
 * @copyright 1996-2007 Headwaters Software, Inc.
 *
 */


/** the acount for receiving funds from Credit Card transactions */
define("QB_CREDIT_CARD_ACCOUNT",	"Credit Card Receivables");

/** Items that can be purchased */
define("QB_ITEM_TRACKING_ALS",	"FA-FISDAP ALS");
define("QB_ITEM_TRACKING_BLS",	"FB-FISDAP BLS");
define("QB_ITEM_SCHEDULER_ALS",	"SA-Scheduler ALS");
define("QB_ITEM_SCHEDULER_BLS",	"SB-Scheduler BLS");
define("QB_ITEM_PDA", "PA-PDA Access");
define("QB_ITEM_TESTING_BASIC", "TB-Testing-Basic");
define("QB_ITEM_TESTING_PARAMEDIC", "TP-Testing-Paramedic");
define('QB_ITEM_STUDYTOOLS_ALS', 'STA-Study Tools Access ALS');
define('QB_ITEM_STUDYTOOLS_BLS', 'STB-Study Tools Access BLS');
define('QB_ITEM_EVENT_REGISTRATION', 'ER-Event Registration');
define('QB_ITEM_PRECEPTOR_TRAINING', 'CET-Clinical Educator Training'); //STUB get string from mike


/** Sales accounts for booking splits */
define("QB_SALES_TRACKING_ALS",	"FISDAP Sales:Skills Tracking Sales:EMT ALS Access");
define("QB_SALES_TRACKING_BLS", "FISDAP Sales:Skills Tracking Sales:EMT BLS Access");
define("QB_SALES_SCHEDULER_ALS", "FISDAP Sales:Scheduler Sales:ALS Add-on");
define("QB_SALES_SCHEDULER_BLS", "FISDAP Sales:Scheduler Sales:BLS Add-on");
define("QB_SALES_PDA", "FISDAP Sales:PDA Sales:PDA Access");
define("QB_SALES_TESTING_BASIC", "FISDAP Sales:Testing:Basic");
define("QB_SALES_TESTING_PARAMEDIC", "FISDAP Sales:Testing:Paramedic");
define('QB_SALES_STUDYTOOLS_ALS', 'FISDAP Sales:Study Tools Sales:ALS Study Tools Access');
define('QB_SALES_STUDYTOOLS_BLS', 'FISDAP Sales:Study Tools Sales:BLS Study Tools Access');
define('QB_SALES_EVENT_REGISTRATION', 'Special Projects:Event Registration');
define('QB_SALES_PRECEPTOR_TRAINING', 'FISDAP Sales:Training:Clinical Educator Training'); 

/**
 * Open the file to wirte the transaction.
 *
 * @param string FileName The name of the file to be opened.
 */
function QB_OpenFile($FileName) {
	if (($FilePntr = fopen($FileName, "w")) == FALSE) {   
	   	return FALSE;
	}
	return $FilePntr;
}

/**
 * Write the transaction Headers to the open file.
 *
 * @param mixed FilePnter Points to open file ready for writing. 
 */
function  QB_WriteHeaders($FilePntr) {

	/*  Write the transaction Heading  */
	$TmpStr1 =  "!TRNS	TRNSID	TRNSTYPE	DATE	ACCNT	";
	$TmpStr2 =  "NAME	CLASS	AMOUNT	DOCNUM	MEMO	";
	$TmpStr3 =  "CLEAR	TOPRINT	NAMEISTAXABLE	ADDR1	";
	$TmpStr4 =  "PAYMETH	ADDR3	ADDR4	ADDR5	DUEDATE	";
	$TmpStr5 =  "TERMS\n";

	/* Write the Distribution Heading   */
	$TmpStr6 =  "!SPL	SPLID	TRNSTYPE	DATE	ACCNT	";
	$TmpStr7 =  "NAME	CLASS	AMOUNT	DOCNUM	MEMO	";
	$TmpStr8 =  "CLEAR	QNTY	PRICE	INVITEM	PAYMETH	";
	$TmpStr9 =  "TAXABLE	VALADJ	REIMBEXP	SERVICEDATE	";
	$TmpStr10 =  "OTHER2\n";

	/* End the Transaction Heading */
	$TmpStr11 =  "!ENDTRNS\n";

	$TmpStr = $TmpStr1 . $TmpStr2. $TmpStr3. $TmpStr4 . $TmpStr5 . $TmpStr6 
		.$TmpStr7 . $TmpStr8 . $TmpStr9 . $TmpStr10 . $TmpStr11;

	if ((fwrite($FilePntr, $TmpStr)) == FALSE) {
		return FALSE;
	}

	return TRUE;
}

/**
 * Writes the lead line of the transaction.
 *
 * @param mixed FilePnter Points to open file ready for writing.
 * @param mixed Date Transaction date format: 1/11/2007
 * @param mixed Account The account where funds will be going in QB.
 * @param mixed Customer The customer number and Name. IE. '401-Inver Hills'.
 * @param mixed Ammount The total ammount of the transaction.
 * @param mixed VTransId Verisign transaction id.
 * @param mixed PaymentType Visa, Mastercard... may be empty string.
 *
 */
function QB_WriteTransaction($FilePntr, $Date, $Account, $Customer, $Ammount, $VTransId, $PaymentType) {
	$TmpStr1 = sprintf("TRNS		CASH SALE	%s	%s	", $Date, $Account);
	$TmpStr2 = sprintf("%s		%0.2f		%s	N	N	", $Customer, $Ammount, $VTransId);
	$TmpStr3 = sprintf("N		%s				%s\n", $PaymentType, $Date);

	$TmpStr = $TmpStr1 . $TmpStr2 . $TmpStr3;

	if (fwrite($FilePntr, $TmpStr) == FALSE) {	
		return FALSE;
	}

	return TRUE;
}

/**
 * Writes one distribution line of the transaction.  There may be multiple
 * distribution lines for each transaction.  These tell what made up the 
 * transaction.  For example if a student purchased an account that 
 * included both tracking and scheduler, they would each have their own
 * distribution line.
 *
 * @param FilePnter Points to open file ready for writing.
 * @param Date Transaction date format: 1/11/2007
 * @param ItemName The name of the item in QB, ie. FA-FISDAP ALS     
 * @param Price The price or each of the item.
 * @param Quanity The number of items purchased.
 * @param SalesAccount The QB account where the sale will be booked.
 *
 */
function QB_WriteDistribution($FilePntr, $Date, $ItemName, $Price, $Quanity, $SalesAccount) {
	$Amount = $Price * $Quanity;

	$TmpStr1 = sprintf("SPL		CASH SALE	%s	%s			", $Date, $SalesAccount);
	$TmpStr2 = sprintf("-%0.2f			N	-%d	%0.2f	%s", $Amount, $Quanity, $Price, $ItemName);
	$TmpStr3 = sprintf("		N	N	NOTHING	0/0/0\n");

	$TmpStr = $TmpStr1 . $TmpStr2 . $TmpStr3;

	if (fwrite($FilePntr, $TmpStr) == FALSE) {	
		return FALSE;
	}

	return TRUE;
}

/**
 * Write the transaction End statment.
 *
 * @param mixed FilePnter Points to open file ready for writing. 
 * 
 */
function QB_EndTransaction($FilePntr) {
	if (fwrite($FilePntr, "ENDTRNS\n") == FALSE) {	
		return FALSE;
	}

	return TRUE;
}




/**
 * Close the file.
 *
 * @param mixed FilePnter Points to open file ready for writing. 
 * 
 */   
function QB_CloseFile($FilePntr) {
	$RValue = fclose($FilePntr);

	return $RValue;
}

/**
 * Returns the unique, static Quickbooks identifier for a given program. 
 * If the program does not have one, it creates and stores it before returning.
 */
function get_quickbooks_id($program_id) {

	require_once('phputil/classes/Assert.inc');
	Assert::is_int($program_id);

	$connection = FISDAPDatabaseConnection::get_instance();

	$query = 'SELECT QuickbooksID, ProgramName, CustumerId from ProgramData WHERE ' .
		"Program_id = $program_id";
	$result = $connection->query($query);

	if (!is_null($result[0]['QuickbooksID']) && $result[0]['QuickbooksID'] != '') {

		return $result[0]['QuickbooksID'];

	} else {

		// Generate and store a new quickbooks ID.

		// An ID is the first 40 characters of "<customer ID> - <program name>" (with
		// trailing white space removed).
		$customer_id = $result[0]['CustumerId'];
		$programname = $result[0]['ProgramName'];
		$new_qb_id = trim(substr("$customer_id-$programname", 0, 40));

		$update = "UPDATE ProgramData SET QuickbooksID = '$new_qb_id' WHERE " .
			"Program_id = $program_id LIMIT 1";
		$connection->update($update);
		return $new_qb_id;

	}
}

?>
