<?php

require_once('phputil/date_utils.php');
require_once('phputil/classyear.html');
require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/classes/model/FISDAPUser.php');
require_once('phputil/classes/html_builder/HTMLTag.php');
require_once('phputil/classes/html_builder/HTMLText.php');
require_once('phputil/classes/html_builder/form_builder/input/SelectMenu.php');
require_once(
    'phputil/classes/html_builder/form_builder/input/'
    . 'SelectOption.php'
);


// where should we put these constants?
define('ALL_CLASS_MONTHS_VALUE', '0');
define('ALL_CLASS_YEARS_VALUE', '0');
define('ALL_STUDENTS_VALUE', '0');
define('ALL_SECTIONS_VALUE', '0');


/**
 *
 */
class StudentSearchWidget extends HTMLTag {

    ////////////////////////////////////////////////// 
    // private instance fields
    // (with the default widget parameters)
    ////////////////////////////////////////////////// 

    var $tag_name_override = false;
    var $form_name_base = 'IShiftForm';
    var $field_name_prefix = '';
    var $action = false;
    var $method = 'POST';
    var $show_sections = true; 
    var $show_class_year = true;
    var $show_class_month = true;
    var $show_student_selector = true; 
    var $on_submit = false;
    var $on_param_change = false;
    var $on_student_change = false;
    var $on_continue = false;
    var $allow_all_class_months = true;
    var $allow_all_class_years = true;
    var $allow_all_students = false;
    var $increment_class_year_in_july = true;

    var $class_month_menu;
    var $class_year_menu;
    var $section_year_menu;
    var $section_menu;
    var $student_menu;

    var $remote_user;
    var $submission;
    var $description_func;
    var $value_field;
    var $extra_fields;


    ////////////////////////////////////////////////// 
    // the constructor
    ////////////////////////////////////////////////// 

    /**
     * Create a search widget with the given parameters
     */
    function StudentSearchWidget($params=false) {
        $this->HTMLTag('form', false);

        // get the remote user
        $this->remote_user =& FISDAPUser::find_by_username(
            $_SERVER['PHP_AUTH_USER']
        );
        if ( !$this->remote_user ) {
            die('Error creating student search.');
        }//if

        // read the parameter hash
        $this->read_params($params);

        $this->submission = ($_GET) ? $_GET : $_POST;

        // set the form name / id
        $this->configure_tag_attributes();

        // add the widget components (class section menu, etc)
        $this->add_components();

        // add the onchange scripts to the components, if required
        $this->add_onchange_scripts();

        // add any additional hidden input fields to the form
        $this->add_extra_fields();

        // try to fill the form from POST/GET data
        $this->fill();
    }//StudentSearchWidget


    ////////////////////////////////////////////////// 
    // private helper methods for building the widget
    ////////////////////////////////////////////////// 


    function configure_tag_attributes() {
        if ( $this->tag_name_override === false ) {
            $this->set_attribute(
                'id',
                $this->field_name_prefix . $this->form_name_base
            );

            $this->set_attribute(
                'name',
                $this->field_name_prefix . $this->form_name_base
            );
        } else {
            $this->set_tag_name($this->tag_name_override);
        }//else
    }//configure_tag_attributes


    function read_params($params) {
        if ( $params ) {
            foreach ( $params as $param=>$value ) {
                $this->$param = $value;
            }//foreach

            // set the action and method in the html tree if found
            if ( $this->action && $this->method ) {
                $this->set_attribute('action',$this->action);
                $this->set_attribute('method',$this->method);
            }//if
        }//if
    }//read_params

    function add_components() {
        $this->add_class_month_selector();
        $this->add_class_year_selector();
        $this->add_section_year_selector();
        $this->add_section_selector();
        $this->add_student_selector();
    }//add_components


    function add_script(&$element, $script, $event='onchange') {
        if($element) {
        $element->set_attribute($event, $script);
    }//if
    }//add_onchange_script


    function add_onchange_scripts() {
        if ( $this->on_param_change ) {
            $this->add_script($this->class_year_menu, $this->on_param_change);
        $this->add_script($this->class_month_menu, $this->on_param_change);
            $this->add_script($this->section_year_menu, $this->on_param_change);
            $this->add_script($this->section_menu, $this->on_param_change);
        }//if

        if ( $this->on_student_change ) {
            $this->add_script($this->student_menu, $this->on_student_change);
        }//if
    }//add_onchange_scripts


    function add_class_year_selector() {
        if ( $this->show_class_year ) {
            // add the container
            $container =& new HTMLTag(
                'div',
                array('class' => 'student_class_year_selector')
            );
            $this->append_child($container);

            // add the form label
            $label =& new HTMLTag(
                'label',
                array(
                    'for' => $this->field_name_prefix . 'student_class_year'
                )
            );
            $label_text =& new HTMLText('Class Year');
            $label->append_child($label_text);
            $container->append_child($label);
           
            // add the menu
            $year_options = $this->generate_year_options(
                array(
                    'allow_all_option' => true
                )
            );
            $this->class_year_menu =& new SelectMenu(
                $this->field_name_prefix . 'student_class_year',
                $year_options
            );
            $container->append_child($this->class_year_menu);
        }//if
    }//add_class_year_selector


    function generate_year_options($params) {
        $options = array();
        $current_year = $this->get_default_display_year();

        // show 8 previous years
        $initial_year = $current_year - 8;

        // show 12 total years
        $final_year   = $initial_year + 12;
        
        // spit out an option for each year, selecting the appropriate entry
        $i = 0;
        for ( $i = $initial_year ; $i < $final_year ; $i++ ) {
            $options[] = new SelectOption($i, $i);
        }//for

        // add an option to select all class years if desired
        if ( $params['allow_all_option'] ) {
            array_unshift(
                $options,
                new SelectOption(
                    ALL_CLASS_YEARS_VALUE, 
                    'All'
                )
            );
        }//if
    
        return $options;
    }//generate_year_options


    function add_class_month_selector() {
        if ( $this->show_class_month ) {
            // add the container
            $container =& new HTMLTag(
                'div',
                array('class' => 'student_class_month_selector')
            );
            $this->append_child($container);

            // add the form label
            $label =& new HTMLTag(
                'label',
                array(
                    'for' => $this->field_name_prefix . 'student_class_month'
                )
            );
            $label_text =& new HTMLText('Class Month');
            $label->append_child($label_text);
            $container->append_child($label);
           
            // add the menu
            $month_options = $this->generate_month_options();
            $this->class_month_menu =& new SelectMenu(
                $this->field_name_prefix . 'student_class_month',
                $month_options
            );
            $container->append_child($this->class_month_menu);
        }//if
    }//add_class_month_selector

    function generate_month_options() {
        $options = array();
        $months = month_short_names();

        $i = 0;
        $num_months = count($months);
        for ( $i=0 ; $i < $num_months ; $i++ ) {
            $options[] = new SelectOption($i+1, $months[$i]);
        }//for

        // add the 'All' option if necessary
        if ( $this->allow_all_class_months ) {
            array_unshift(
                $options,
                new SelectOption(
                    ALL_CLASS_MONTHS_VALUE, 
                    'All'
                )
            );
        }//if

        return $options;
    }//generate_month_options

    function add_section_year_selector() {
        if ( $this->show_sections ) {
            // add the container
            $container =& new HTMLTag(
                'div',
                array('class' => 'student_section_year_selector')
            );
            $this->append_child($container);

            // add the form label
            $label =& new HTMLTag(
                'label',
                array(
                    'for' => $this->field_name_prefix . 'student_section_year'
                )
            );
            $label_text =& new HTMLText('Section Year');
            $label->append_child($label_text);
            $container->append_child($label);
           
            // add the menu
            $year_options = $this->generate_year_options(
                array(
                    'allow_all_option' => false
                )
            );
            $this->section_year_menu =& new SelectMenu(
                $this->field_name_prefix . 'student_section_year',
                $year_options
            );
            $container->append_child($this->section_year_menu);
        }//if
    }//add_section_year_selector


    function add_section_selector() {
        if ( $this->show_sections ) {
            // add the container
            $container =& new HTMLTag(
                'div',
                array('class' => 'student_section_selector')
            );
            $this->append_child($container);

            // add the form label
            $label =& new HTMLTag(
                'label',
                array('for' => $this->field_name_prefix . 'student_section')
            );
            $label_text =& new HTMLText('Class Section');
            $label->append_child($label_text);
            $container->append_child($label);
           
            // add the menu
            $this->section_menu =& new SelectMenu(
                $this->field_name_prefix . 'student_section',
                array() 
            );
            $container->append_child($this->section_menu);
        }//if
    }//add_section_selector


    function add_student_selector() {
        if ( $this->show_student_selector ) {
            // add the container
            $container =& new HTMLTag(
                'div',
                array('class' => 'student_selector')
            );
            $this->append_child($container);

            // add the form label
            $label =& new HTMLTag(
                'label',
                array('for' => $this->field_name_prefix . 'student_id')
            );
            $label_text =& new HTMLText('Available Students');
            $label->append_child($label_text);
            $container->append_child($label);

            // add the menu
            $this->student_menu =& new SelectMenu(
                $this->field_name_prefix . 'student_id',
                false
            );

            $container->append_child($this->student_menu);
        }//if
    }//add_student_selector


    function fill() {
        // populate from the incoming request, if possible
        $params = ($_GET) ? $_GET : $_POST;

        if ( $params && count($params) ) {
            $this->populate($params);
        } else {
            $this->populate_defaults();
        }//else

        // sync the class sections to the selected section year
        $this->sync_class_sections(
            $params[$this->field_name_prefix . 'student_section_year']
        );

        // select the same class section as was submitted (if possible)
        $this->populate(
            array(
                $this->field_name_prefix . 'student_section' => $params[
                    $this->field_name_prefix . 'student_section'
                ]
            )
        );

        // if we are showing the student selector, sync the students
        // menu to the other selected fields
        if ( $this->show_student_selector ) {
            $this->sync_students($params);
        }//if

        // select the same student as was submitted (if possible)
        $this->populate(
            array(
                $this->field_name_prefix . 'student_id' => $params[
                    $this->field_name_prefix . 'student_id'
                ]
            )
        );
    }//fill


    function sync_class_sections($section_year) {
        $db =& FISDAPDatabaseConnection::get_instance();
        $connection = $db->get_link_resource();

        $class_sections = get_class_sections(
            array(
                'section_year' => $section_year,
                'program_id'   => $this->remote_user->get_program_id(),
                'connection'   => $connection
            )
        );
        
        if ( $class_sections ) {
            // add an 'All' entry
            $this->section_menu->append_child(
                new SelectOption(
                    ALL_SECTIONS_VALUE,
                    'All'
                )
            );

            foreach ( $class_sections as $section ) {
                $section_id = $section['Sect_id'];
                $section_name = $section['Name'];

                $this->section_menu->append_child(
                    new SelectOption(
                        $section_id,
                        htmlentities($section_name)
                    )
                );
            }//foreach
        } else {
            $this->section_menu->append_child(
                new SelectOption(
                    '0',
                    'None found for the selected year'
                )
            );
        }//else
    }//sync_class_sections


    function get_menu_value(&$menu) {
        if ( !$menu ) {
            return false;
        }//get_menu_value

        $selected = $menu->get_selected_option();
        if ( $selected ) {
            return $selected->get_attribute('value');
        } else {
            $children = $menu->get_children();
            if ( $children ) {
                $first_child = $children[0];
                return $first_child->get_attribute('value');
            }//if
        }//else

        return false;
    }//get_menu_value


    function get_class_year() {
        return $this->get_menu_value($this->class_year_menu);
    }//get_class_year


    function get_class_month() {
        return $this->get_menu_value($this->class_month_menu);
    }//get_class_month


    function get_section_year() {
        return $this->get_menu_value($this->section_year_menu);
    }//get_section_year


    function get_section_id() {
        return $this->get_menu_value($this->section_menu);
    }//get_section_id


    function get_student_id() {
        return $this->get_menu_value($this->student_menu);
    }//get_student_id


    function generate_student_query($params) {
        $query = 'SELECT students.* '
               . 'FROM StudentData students';

        // check the form's current parameters
        $class_year = $this->get_class_year();
        $class_month = $this->get_class_month();
        $section_id = $this->get_section_id();

        $extra_conditions = array();
        $extra_tables = array();

        if ( ($section_id = intval($section_id)) != ALL_SECTIONS_VALUE ) {
            $extra_tables[] = 'SectStudents sections';
            $extra_conditions[] = 'AND students.Student_id='
                                . 'sections.Student_id '
                                . 'AND sections.Section_id="' 
                                . $section_id . '" ';
        }//if

        if ( ($class_month = intval($class_month)) != ALL_CLASS_MONTHS_VALUE ) {
            $extra_conditions[] = 'AND students.ClassMonth="' 
                                . $class_month . '"';
        }//if

        if( ($class_year = intval($class_year)) != ALL_CLASS_YEARS_VALUE ) {
            $extra_conditions[] = 'AND students.Class_Year="' 
                                . $class_year . '"';
        }//if

        // add any extra tables
        if ( $extra_tables ) {
            $query .= ', ' . implode(', ', $extra_tables);
        }//if

        $query .= ' WHERE students.Program_id="'
                . $this->remote_user->get_program_id()
                . '"';

        // add any extra conditions
        if ( $extra_conditions ) {
            $query .= ' ' . implode(' ', $extra_conditions);
        }//if

        $query .= ' ORDER BY students.LastName, students.FirstName ';

        return $query;
    }//generate_student_query


    function sync_students($params) {
        $db =& FISDAPDatabaseConnection::get_instance();

        $query = $this->generate_student_query($params);

        $result = $db->query($query);
        if ( !$result ) {
            $this->student_menu->append_child(
                new SelectOption(
                    '0',
                    'None found for the selected options'
                )
            );
            return;
        }//if

        $this->student_menu->append_child(
            new SelectOption(
                ALL_STUDENTS_VALUE - 1,
                'Select One'
            )
        );

        if ( $this->allow_all_students ) {
            $this->student_menu->append_child(
                new SelectOption(
                    ALL_STUDENTS_VALUE,
                    'All'
                )
            );
        }//if

        foreach ( $result as $row ) {
            $this->student_menu->append_child(
                new SelectOption(
                    $this->get_student_menu_option_value($row),
                    $this->get_student_menu_description($row)
                )
            );
        }//foreach
    }//sync_students


    function get_student_menu_option_value($row) {
        if ( $this->value_field ) {
            return $row[$this->value_field];
        } else {
            return $row['Student_id'];
        }//else
    }//get_student_menu_option_value


    function get_student_menu_description($row) {
        $description_func = $this->description_func;
        if ( $description_func ) {
            return $description_func($row);
        } else {
            return $row['LastName'] . ', ' . $row['FirstName'] 
                . ' (' . $row['Student_id'] . ')';
        }//else
    }//get_student_menu_description


    function get_default_display_year() {
        $current_year = date('Y');
        if ( $this->increment_class_year_in_july
             && intval(date('m')) >= JULY ) {
            $current_year++;
        }//if

        return $current_year;
    }//get_default_display_year


    function populate_defaults() {
        // increment the class year in july if desired
        $current_year = $this->get_default_display_year();

        $fake_postdata = array(
            $this->field_name_prefix . 'student_class_year'   => $current_year,
            $this->field_name_prefix . 'student_section_year' => $current_year
        );

        $this->populate($fake_postdata);
    }//populate_defaults


    function add_extra_fields() {
        if ( $this->extra_fields ) {
            foreach ( $this->extra_fields as $name => $value ) {
                $tag =& new HTMLTag(
                    'input',
                    array(
                        'type'  => 'hidden',
                        'name'  => htmlentities($name),
                        'value' => htmlentities($value)
                    )
                );
                $this->append_child($tag);
            }//foreach
        }//if
    }//add_extra_fields
}//class StudentSearchWidget


?>
