<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/
 
// Let's turn on strict error reporting
error_reporting(E_ALL);

/**
 * @todo figure out what we actually need from this list
 */
require_once('phputil/session_data_functions.php');
require_once('phputil/classes/CommonPrompt.inc');
require_once('phputil/classes/common_form_base.inc');
require_once('phputil/classes/CommonPromptGroup.inc');
require_once('phputil/classes/common_utilities.inc');
require_once('phputil/classes/listFactory.inc');
require_once('phputil/Structures_Graph/Graph.php');
require_once('phputil/Structures_Graph/Graph/Manipulator/AcyclicTest.php');
require_once('phputil/Structures_Graph/Graph/Manipulator/TopologicalSorter.php');

/**
 * The base functionality for common_form.
 *
 * Handles everything except displaying the form.
 *
 * @package CommonInclude
 * @author Warren Jacobson
 * @author Ian Young
 */
abstract class CommonFormBase {

	/**
	 * The state of this form.
	 * @see update_state()
	 * @var array
	 */
	protected $state;
	/**
	 * The name of this form
	 */
	protected $name;
	/**
	 * The url to submit the form input to
	 */
	protected $action;
	/**
	 * The name of the calling routine's validate function
	 */
	protected $validate_function;
	/**
	 * Either 'session' or 'none'
	 *
	 * Defaults to 'session'
	 */
	protected $default_promptvalue_mode;
	/**
	 * Either 'form' or 'ajax'
	 *
	 * Defaults to 'form'
	 */
	protected $submission_technique;
	/**
	 * The buttons this form includes.
	 *
	 * Currently, this is indexed by the type constant for convenience
	 *
	 * @var array an array of {@link CommonFormButton}s
	 */
	protected $form_buttons;
	/**
	 * The button that was pressed, or null
	 * @var CommonFormButton
	 */
	protected $pressed_button;
	/**
	 * An array containing div id's and prompt names
	 */
	protected $div_listeners;
	/**
	 * The Dispatch ID to use for this form
	 */
	protected $dispatch_id;
	/**
	 * The components that have been added to this form
	 * @var CommonPromptGroup
	 */
	protected $components;
	/**
	 * True if the form passes validation, false otherwise
	 */
	protected $validated;
	/**
	 * An array of prompt and form validation error strings
	 */
	protected $validation_msgs;
	/**
	 * Does this form contain at least one upload prompt?
	 * @var boolean
	 */
	protected $contains_upload = false;
	/**
	 * Should we give a warning message about deleting things?
	 * @var boolean
	 */
	protected $delete_warning_message = false;
	/**
	 * The dependency graph for prompt listeners
	 * @var Structures_Graph
	 */
	protected $listeners;
	/**
	 * Flag to indicate if we're in legacy mode.
	 *
	 * If so, we do a couple extra 
	 * things for the sake of backwards compatibility.
	 *
	 * @var boolean
	 */
	protected $legacy_mode;

	/**
	 * Create a new instance of the common_form class
	 */
	public function __construct() {

		$this->state = array();
		$this->components = new CommonPromptGroup();
		$this->set_action(null);
		$this->set_button(new CommonFormButton(CommonFormButton::SUBMIT, 'Submit'));
		$this->set_default_promptvalue_mode('session');
		$this->set_submission_technique('form');

		$this->displayobjects = array();
		$this->div_listeners = array();
		$this->validation_msgs = array();

		$this->listeners = new Structures_Graph(true);

		$this->logger = FisdapLogger::get_logger();

	}

	/**
	 * @todo come back to this function
	 * Update the form state
	 *
	 * Update the state of the form, and push those updates to any
	 * existing children.
	 * @param array an array containing any or all of the following:
	 *  - 'name' => the name of the form
	 *  - 'default_promptvalue_mode' =>
	 *  - 'submission_technique' =>
	 *  @todo finish this documentation
	 */
	private function update_state($state) {

		// Merge the new state with whatever exists
		$this->state = array_merge($this->state, $state);

		// Update the state info for any prompts we already know about
		$this->components->set_form_state($this->state);

	}

	/**
	 * Set the name of the form
	 * @param string $name The name of the form. Must be unique
	 * amongst forms on a page.
	 */
	public function set_name($name) {
		$this->name = $name;
		$this->update_state(array('name' => $name));
	}

	/**
	 * Set the url to submit the form input to
	 * @param string $url The URL to submit the form input to.
	 * Defaults to $_SERVER['SCRIPT_NAME']
	 */
	public function set_action($action=null) {

		if ($action == null) {
			$action = $_SERVER["SCRIPT_NAME"];
		}
		$this->action = $action;

	}

	/**
	 * Add a button to the form.
	 *
	 * Forms may only have one button of a given type, so calling this will
	 * overwrite any existing button of the same type.
	 *
	 * @param CommonFormButton $button
	 */
	public function set_button($button) {
		Assert::is_a($button, 'CommonFormButton');
		$index = $this->get_button_index($button->get_type());
		$this->form_buttons[$index] = $button;

		// Order the buttons appropriately
		uasort($this->form_buttons, array('CommonFormButton', 'compare'));

	}

	/**
	 * Remove a button from the form
	 * @param int The button type to be removed.
	 */
	public function remove_button($buttontype) {
		$index = $this->get_button_index($buttontype);
		unset($this->form_buttons[$index]);
	}

	/**
	 * Get the index a button should have in the button array.
	 */
	private function get_button_index($buttontype) {
		return $buttontype;
	}

	/**
	 * Does this form have a certain kind of button?
	 *
	 * @param int A constant from CommonFormButton
	 * @return CommonFormButton A button object if this form has a
	 * button of that type, or false if it doesn't exist.
	 */
	public function get_button($buttontype) {
		$index = $this->get_button_index($buttontype);
		if (array_key_exists($index, $this->form_buttons)) {
			return $this->form_buttons[$index];
		} else {
			return null;
		}
	}

	/**
	 * Show a button on the form.
	 * @see hide_button()
	 * @param int A constant from CommonFormButton
	 */
	public function show_button($buttontype) {
		$b = $this->get_button($buttontype);
		if ($b instanceof CommonFormButton) {
			$b->show();
		}
	}

	/**
	 * Hide a button on the form.
	 *
	 * The button will still be available for processing, but will not be displayed.
	 *
	 * @param int A constant from CommonFormButton
	 */
	public function hide_button($buttontype) {
		$b = $this->get_button($buttontype);
		if ($b instanceof CommonFormButton) {
			$b->hide();
		}
	}

	/**
	 * Hide all buttons on the form
	 */
	public function hide_all_buttons() {
		foreach ($this->form_buttons as $b) {
			$b->hide();
		}
	}

	/**
	 * @todo actually remove this?
	 * Set the text on the face of the submit button
	 * @param string  $submitvalue The text that appears on the form's 'submit' button.
	 * Defaults to 'Submit'. Recommended values include 'Display Report', 'Save' and
	 * 'Refresh' for report, update, and query modes, respectively
	 * @deprecated Use {@link set_button()}
	 */
	public function set_submitvalue($submitvalue) {

		if ($submitvalue == null) $submitvalue = "Submit";

		$this->set_button(new CommonFormButton(CommonFormButton::SUBMIT, $submitvalue));

	}

	/**
	 * Set the calling routine's validate function name upon submit
	 * @param callback $function_name The name of the callback function to invoke
	 * when validating the form. Optional. Permits custom form validation logic.
	 */
	public function set_validate_function($validate_function) {
		$this->validate_function = $validate_function;
	}

	/**
	 * @todo revisit this
	 * Set the calling routine's validate function name upon delete
	 * @param callback $function_name The name of the callback function to invoke
	 * when the user clicks 'Delete' from either 'delete' or 'update' mode.
	 * Optional. Permits custom form validation logic.
	 */
	public function set_validate_delete_function($validate_delete_function) {
		$this->validate_delete_function = $validate_delete_function;
	}

	/**
	 * Set how the form provides prompt defaults
	 * @param string $default_promptvalue_mode Controls how prompts do their
	 * defaulting. Either 'session' or 'none'
	 */
	public function set_default_promptvalue_mode($default_promptvalue_mode) {

		Assert::is_string($default_promptvalue_mode);
		Assert::is_in_array($default_promptvalue_mode, array('session', 'none'));

		$this->default_promptvalue_mode = $default_promptvalue_mode;
		$this->update_state(array('default_promptvalue_mode' => $default_promptvalue_mode));

	}

	/**
	 * Set how the form is submitted to the server
	 * @param string $submission_technique Either 'form' or 'ajax'
	 */
	public function set_submission_technique($submission_technique) {

		Assert::is_string($submission_technique);
		Assert::is_in_array($submission_technique, array('form', 'ajax'));

		$this->submission_technique = $submission_technique;
		$this->update_state(array('submission_technique' => $submission_technique));

	}

	/**
	 * @todo keep this? change it?
	 * Set what div ID's are listening to changes to this form
	 * @param mixed $div_listeners A string or an array of strings indicating
	 * which DIVs to populate via callback functions upon ajax submission of a form.
	 * For example, a value of 'lowerlist' causes the function
	 * 'respond_ajax_lowerlist_div' to be invoked upon submission of the query form 
	 */
	public function set_div_listeners($div_listeners) {

		if (!is_array($div_listeners)) {
			$div_listeners = array($div_listeners);
		}

		$this->div_listeners = $div_listeners;

	}

	/**
	 * Set the Dispatch ID to use with this form
	 * @param string $dispatch_id The Dispatch ID to use for all form I/O.
	 * Defaults to the form's name.
	 */
	public function set_dispatch_id($dispatch_id=null) {

		Assert::is_true(Test::is_not_empty_string($dispatch_id) || Test::is_null($dispatch_id));

		$this->dispatch_id = $dispatch_id;

	}

	/**
	 * Add a display object (e.g., prompt, section) to the form
	 *
	 * @param CommonFormComponent The component to add
	 */
	public function add($component) {

		return $this->components->add($component);

	}

	/**
	 * Display or validate this form
	 * @todo fix submitbuttontype and submissionattempt's use of session
	 */
	public function process() {

		if (!isset($this->name)) {
			//STUB
			throw new Exception('You must set a name on CommonForm!');
		}

		foreach ($this->get_promptlist() as $prompt) {
			$prompt->process();
		}

		// Is this an Ajax request for this form? If so, handle it
		$ajax_for_me = isset($_POST['ajax_common_form']) && ($_POST['formid'] == $this->name); 
		if ($ajax_for_me) {
			$ajax_type = $_POST['ajax_common_form'];
			switch($ajax_type) {

			case 'promptHandler':
				// If this happens, a prompt has changed and we should respond to that. 
				$this->respond_prompthandler();
				return;

			case 'ajaxSubmitHandler':
				// Tuck the form input away into session space and carry on processing
				$this->respond_formsubmithandler();
				break;

			case 'formSubmitHandler':
				// If this happens, it is the first half of a 'report'-style form 
				// submission. So simply pack the received data into session space.
				$this->respond_formsubmithandler();
				return;

			default:
				//TODO STUB
				common_utilities::displayFunctionError('An error!');

			}
		}

		// Assume the form has not been validated
		$this->validated = false;

		// Clear out old button pressed values
		common_utilities::set_scriptvalue("submitbuttontype", null);
		if ($this->legacy_mode) {
			common_utilities::set_scriptvalue("submitform", null);
		}
		// Get the button that was pressed, if any
		$pressed = $this->get_pressed_button();

		// If the following condition is true then the user hit a button
		if ($pressed instanceof CommonFormButton) {

			// Save the new button type where we can retrieve it
			common_utilities::set_scriptvalue("submitbuttontype",$pressed->get_type());
			if ($this->legacy_mode) {
				// And save the value for old scripts that are depending on that
				common_utilities::set_scriptvalue("submitform",$pressed->get_value());
			}

			// Validate the form according to which buttons were pushed
			$this->validated = false;
			if ($pressed->get_type() == CommonFormButton::SUBMIT) {
				$this->validate();

			} else if ($pressed->get_type() == CommonFormButton::DELETE) {
				$this->validate_delete();

			} else if ($pressed->get_type() == CommonFormButton::UNCONFIRMED_DELETE) {
				// Legacy scripts aren't expecting a value here
				common_utilities::set_scriptvalue("submitform", null);
				// Confirm the deletion
				$this->hide_all_buttons();
				$this->show_button(CommonFormButton::DELETE);
				$this->show_button(CommonFormButton::DELETE_CANCEL);
				$this->delete_warning_message = true;
				$this->disable_all_prompts();
				$this->validated = false;

			} else if ($pressed->get_type() == CommonFormButton::RESET) {
				$this->delete_session_values();
				$this->validated = false;

			} else if ($pressed->get_type() == CommonFormButton::DELETE_CANCEL) {
				$this->validated = false;

			} else if ($pressed->get_type() == CommonFormButton::CANCEL) {
				$this->validated = false;
			}

			// Increment the submission attempt counter
			if (true || $this->mode != 'query') {
				$submissionattempt = common_utilities::get_scriptvalue('submissionattempt');
				$submissionattempt++; // Increment Submission Attempt
				common_utilities::set_scriptvalue("submissionattempt",$submissionattempt);
			}

			if ($this->validated && $this->legacy_mode) {
				$this->process_valid_form();
			}

		} else {
			$this->process_fresh_form();
		}

		// If this method returns true, then the form does not need to be acted upon

		return $this->validated;

	}

	/**
	 * Is this form valid?
	 *
	 * Valid means that the form has been processed successfully and the input 
	 * is safe to use.
	 *
	 * <b>Call process() before calling this method.</b>
	 *
	 * @todo magically call process()? throw an exception if process() hasn't been called?
	 * @return boolean
	 */
	public function is_valid() {
		return $this->validated;
	}

	/**
	 * Figure out if a button on this form has been pressed
	 *
	 * @return CommonFormButton|null the button object representing the button that 
	 * was pressed, or null if nothing was pressed
	 */
	public function get_pressed_button() {

		if ($this->pressed_button == null) {
			// Look for a button value in data we've just received
			$button_type = $this->get_button_type();

			$pressed = $this->get_button($button_type);

			if ($button_type === null || $pressed === null) return null;

			$this->pressed_button = $pressed;
		}

		return $this->pressed_button;
	}

	/**
	 * Disable all prompts on this form.
	 */
	private function disable_all_prompts() {
		foreach ($this->get_promptlist() as $prompt) {
			$prompt->disable();
		}
	}

	/**
	 * We are just loading the page and want a shiny new clean form
	 * @todo fix submissionattempt here too
	 */
	private function process_fresh_form() {

		// Never display an unsubmitted form on an ajax callback
		$is_ajax = isset($_POST['ajax_common_form']);
		if ($is_ajax) return;

		// If we're not saving prompt choices then delete the form's session values
		if ($this->default_promptvalue_mode == 'none') {
			$this->delete_session_values(); // Delete the form's session values
		}

		// Set the submission attempt
		if (common_utilities::get_scriptvalue('submissionattempt') == null) {
			common_utilities::set_scriptvalue("submissionattempt",0);
		}

		$this->process_all_ajax_responders();

	}

	/**
	 * We processed the form and it was valid. Do some legacy stuff.
	 */
	private function process_valid_form() {
		if ($this->submission_technique == 'ajax' && $this->legacy_mode) {
			// Loop through each listener and call it's respond_ajax() function
			foreach ($this->div_listeners as $listener) {
				$function_name = 'respond_ajax_' . $listener . '_div';
				if (function_exists($function_name)) {
					start_ajax();
					$respond = $function_name();
					stop_ajax('div',$listener);
				}
			}
		}
	}

	/**
	 * Redisplay the form.
	 *
	 * Divines whether we are in AJAX mode or not, and sends the form output to 
	 * the appropriate location.
	 */
	public function redisplay_form() {

		/**
		 * @todo what is this doing here?
		 */
		$this->process_all_ajax_responders();

		$is_ajax = isset($_POST['ajax_common_form']);

		if ($is_ajax) {
			send_browser_action(new InnerHtmlBrowserAction($this->name . '_form_div', $this->display()));
		} else {
			echo $this->get_generated_html_form();
		}

	}

	/**
	 * Retrieve a prompt by name.
	 * @param string $name The prompt name.
	 * @return common_form_component|null The component or null if not found.
	 * @todo duplicates get_prompt()
	 */
	public function get_prompt_by_name($name) {
		$prompts = $this->get_promptlist();

		foreach ($prompts as $prompt) {
			if ($prompt->get_name() == $name) return $prompt;
		}

		return null;
	}

	/**
	 * Build the dependency graph for prompt listeners
	 */
	protected function build_listeners() {

		if ($this->legacy_mode) {
			$this->build_legacy_listeners();
		}

		foreach ($this->components->get_listeners() as $name => $array) {
			$prompt = $this->get_prompt_by_name($name);
			$prompt_node = $this->add_to_listener_graph($prompt);
			foreach ($array as $listener) {
				if ($listener == $prompt) continue;
				$listener_node = $this->add_to_listener_graph($listener);
				$prompt_node->connectTo($listener_node);
			}
		}

	}

	/**
	 * Reconstruct listener dependencies out of the legacy-style specification by 
	 * prompt name.
	 */
	private function build_legacy_listeners() {
		foreach ($this->get_promptlist() as $prompt) {
			if ($prompt instanceof CommonLegacyPrompt) {
				foreach ($prompt->get_listento() as $name) {
					$listento = $this->get_prompt($name);
					$prompt->listen_to($listento);
				}
			}
		}
	}

	/**
	 * Add a prompt to the dependency graph, or simply find the node if it 
	 * already exists.
	 *
	 * @param CommonPrompt the prompt to add
	 * @return Structures_Graph_Node the node on the dependency graph that 
	 * contains the prompt
	 */
	protected function add_to_listener_graph($prompt) {
		foreach ($this->listeners->getNodes() as $node) {
			// Comparing by name, for lack of a better idea
			if ($node->getData()->get_name() == $prompt->get_name()) {
				return $node;
			}
		}
		// We didn't find it, add a new node
		$newnode = new Structures_Graph_Node();
		$newnode->setData($prompt);
		$this->listeners->addNode($newnode);
		return $newnode;
	}

	/**
	 * Sort through the dependency tree, calling any relevant prompt listeners.
	 *
	 * Be aware that prompts listening to themselves are not included here. They 
	 * should be invoked beforehand.
	 *
	 * @param CommonPrompt $prompt the prompt that changed and triggered this 
	 * response. If null, we will activate all prompt responders.
	 * @return array an array of the CommonPrompt objects that have changed and 
	 * need to be updated client-side.
	 */
	protected function call_listeners($prompt=null) {

		// Build the listener dependency graph
		$this->build_listeners();

		// Get the listener dependency graph
		$graph = $this->listeners;
		
		// If there are no listener relationships, exit early
		if (count($graph->getNodes()) == 0) return array();

		// It has to be acyclic for this to work
		if (!Structures_Graph_Manipulator_AcyclicTest::isAcyclic($graph)) {
			//STUB
			throw new Exception('This form\'s prompts have circular dependencies, aborting.');
		}

		// Sort it topologically - this will put all the listeners in cascading 
		// order, so if we start at the top, we'll always be calling a prompt's 
		// response before calling those of anything listening to it
		$node_sort = Structures_Graph_Manipulator_TopologicalSorter::sort($graph);
		// This is silly, but the array is actually not key-sorted when it comes back
		ksort($node_sort);

		if (is_null($prompt)) {
			// Trigger the responder chains for everything at the top level
			$changed_nodes = $node_sort[0];
			// Pull out the first row so we don't interate through it
			array_shift($node_sort);
		} else {
			// Get the node that changed and start responses with it
			$prompt_node = $this->add_to_listener_graph($prompt);
			$changed_nodes = array($prompt_node);
		}

		// An array of CommonFormPrompts that have changed
		$updated = array();

		// Iterate through the levels and nodes, checking each for parents that 
		// have changed
		foreach ($node_sort as $i => $level) {
			foreach ($level as $node) {
				// If array_unique was better, we wouldn't need this
				$seen = false;
				// Do this as a for loop because changing the contents of the 
				// array within a foreach is undefined and probably a bad idea
				$length = count($changed_nodes);
				for ($i=0; $i<$length; $i++) {
					if ($changed_nodes[$i]->connectsTo($node)) {
						$update = $node->getData()->respond_ajax($changed_nodes[$i]->getData());
						if ($update && !$seen) {
							$changed_nodes[] = $node;
							$updated[] = $node->getData();
						}
						$seen = true;
					}
				}
			}
		}
		//return array_unique($updated, SORT_REGULAR);
		return $updated;
	}

	/**
	 * Validate the form upon submit
	 */
	protected function validate() {

		$this->validated = true; // We'll assume everything passes validation

		// Loop through each prompt
		$promptlist = $this->get_promptlist();
		foreach ($promptlist as $thisprompt) {

			// Validate this prompt
			$prompt_validated = $thisprompt->is_valid();
			// If it's not valid, form fails and we display some messages
			if (!$prompt_validated) {
				$this->validated = false;
				$thisprompt->set_badinput(true);
				$this->validation_msgs = array_merge($this->validation_msgs, $thisprompt->get_errors());
			}

		}

		// Do custom form validation if it's present
		if ($this->validated and is_callable($this->validate_function)) {
			$validate_function = $this->validate_function;
			$form_validated = $validate_function($this);
			if (!$form_validated) {
				$this->validated = false;
			}
		}

	}

	/**
	 * Validate the form upon delete
	 */
	protected function validate_delete() {

		$this->validated = true; // We'll assume everything passes validation

		if (is_callable($this->validate_delete_function)) {
			$validate_function = $this->validate_delete_function;
			$form_validated = $validate_function($this);
			if (!$form_validated) {
				$this->validated = false;
			}
		}

	}

	/**
	 * @todo
	 */
	public function get_submission_technique() {
	}

	/**
	 * Get a prompt in this form  by name.
	 * @param string the name of the prompt
	 * @return CommonPrompt|false
	 */
	public function get_prompt($name) {
		foreach ($this->get_promptlist() as $prompt) {
			if ($prompt->get_name() == $name) {
				return $prompt;
			}
		}
		return false;
	}

	/**
	 * Respond to a prompt onchange event
	 * @todo Do something better than echo_ajax on an error
	 * @todo delegate all the display work to CommonPrompt
	 */
	protected function respond_prompthandler() {
		$ajax_baseid = $_POST['id'];

		$baseprompt = $this->get_prompt($ajax_baseid);

		// Did we actually find a prompt object? If not we're outta here
		if ($baseprompt === false) {
			echo_ajax('alert',null,'Ajax Error: Prompt not found'); 
			return;
		}

		// Update our session value with the new input
		$baseprompt->respond_formsubmithandler($_POST);

		// If we're listening to ourselves, call that
		if (in_array($baseprompt, $baseprompt->get_listeners())) {
			if ($baseprompt->respond_ajax($baseprompt)) {
				send_browser_action(new InnerHtmlBrowserAction($baseprompt->get_name() . '_div', $baseprompt->display()));
			}
		}
		// Now trigger any other responders
		$arr = $this->call_listeners($baseprompt);

		foreach ($arr as $prompt) {
			send_browser_action(new InnerHtmlBrowserAction($prompt->get_name() . '_div', $prompt->display()));
		}

	}

	/**
	 * Tuck form input away into session space
	 */
	protected function respond_formsubmithandler() {

		// Loop through each prompt
		foreach ($this->get_promptlist() as $thisprompt) {
			$thisprompt->respond_formsubmithandler(); // Tuck a prompt's form input away into session space
		}

	}

	/**
	 * Delete a form's session values
	 */
	protected function delete_session_values() {

		// Loop through each enclosed display object and delete its values
		foreach ($this->get_promptlist() as $this_prompt) {
			$this_prompt->delete_value();
		}

	}

	/**
	 * Call the ajax response methods/functions for all listeners
	 *
	 * Prompts listening to themselves will process before any others.
	 */
	protected function process_all_ajax_responders() {

		foreach ($this->get_promptlist() as $prompt) {
			if (in_array($prompt, $prompt->get_listeners())) {
				$prompt->respond_ajax($prompt);
			}
		}

		$this->call_listeners();

	}

	/**
	 * Get the dispatch id for this form.
	 *
	 * If none has been set, defaults to the form's name.
	 *
	 * @return string
	 */
	protected function get_dispatch_id() {
		if ($this->dispatch_id === null) {
			$this->dispatch_id = $this->get_name();
		}
		return $this->dispatch_id;
	}

	/**
	 * Get the name of the form
	 * @return string
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * Get a list of all prompts on this form
	 */
	public function get_promptlist() {
		return $this->components->get_promptlist();
	}

	/**
	 * Get a list of components on this form.
	 *
	 * @see CommonPromptGroup::get_componentlist()
	 */
	protected function get_componentlist() {
		return $this->components->get_componentlist();
	}

	/**
	 * Has the browser's Back button been hit?
	 * 
	 * <b>This only returns accurate results when called <i>before</i>
	 * {@link process()}!</b>
	 *
	 * @return boolean
	 */
	private function is_back_button_pressed() {
		// Use the submissionattempt hidden prompt and session value to determine
		// whether or not the back button on the browser has been pressed.
		if ($this->submission_technique == 'form') {
			$server_submissionattempt = common_utilities::get_scriptvalue("submissionattempt");
			if ($server_submissionattempt == null) $server_submissionattempt = 0;
			$backbuttonnotpressed = (isset($_GET["submissionattempt"]) && $server_submissionattempt + 1 == $_GET["submissionattempt"]);
		}
		else {
			$backbuttonnotpressed = true; // It's all done w/ ajax, has to be true
		}

		// A little something to make things work with Safari. Mac users deserve love too.
		if ($this->submission_technique == 'form') {
			$navigator_user_agent = ( isset( $_SERVER['HTTP_USER_AGENT'] ) ) ? strtolower( $_SERVER['HTTP_USER_AGENT'] ) : '';
			if ($navigator_user_agent == 'safari') $backbuttonnotpressed = true;
		}

		return !$backbuttonnotpressed;
	}

	/**
	 * Get the type of the button that was hit. Only returns a value if the 
	 * button was for this form.
	 *
	 * @return string
	 */
	private function get_button_type() {

		// If the back button has be hit, then return null
		if ($this->is_back_button_pressed()) {
			return null;
		}

		$inputs = null;
		// Look in the appropriate place depending on how we're submitting
		if ($this->submission_technique == 'form') {
			if (isset($_GET['formid']) && $_GET['formid'] == $this->get_name()) {
				$inputs = $_GET;
			}
		} else {
			if (isset($_POST['formid']) && $_POST['formid'] == $this->get_name()) {
				$inputs = $_POST;
			}
		}

		// Look for something in the submitted input that matches one of our 
		// button names
		if (is_array($inputs)) {
			foreach ($this->form_buttons as $button) {
				if (isset($inputs[$button->get_name()])) {
					return $button->get_type();
				}
			}
		}

		return null;
	}

	/**
	 * Helper function for child classes. Get the top of the form.
	 *
	 * This includes the form tag, validation errors, and anything
	 * else that goes at the top of the form.
	 *
	 * @return string the HTML
	 */
	protected function get_form_header_display() {

		// Stuff to set up submit on enter (unless we're in a delete form)
		$hasDeleteButton = (($this->get_button(CommonFormButton::DELETE) != null) ||
			($this->get_button(CommonFormButton::UNCONFIRMED_DELETE) != null));
		if($hasDeleteButton)
		{
			$submitButtonId = 'null';
		}
		else
		{
			$submitButton = $this->get_button(CommonFormButton::SUBMIT);
			$submitButtonId = "'" . $submitButton->get_id($this->name) . "'";
		}

		// Make the form tag and surrounding element
		$formtag = '<div class="common_form_top"><form action="' . $this->action . '"';
		$formtag .= ' id="' . $this->name . '"';
		$formtag .= ' enctype="multipart/form-data"';
		$formtag .= ' method="GET"';
		$formtag .= ' onKeyPress="return handleKeyPress(';
		$formtag .= "{$submitButtonId}, event);\"";
		$formtag .= '>';

		// Get validation errors
		$validation = $this->get_validation_display();

		return $formtag . $validation;
	}

	/**
	 * Get the validation messages for this form
	 * @return string
	 */
	protected function get_validation_display() {
		$output = '<div class="validation_errors">' . "\n";
		$errors = array_map(create_function('$x', 'return \'<span class="mediumbolderror">\' . $x . \'</span>\';'), $this->validation_msgs);
		$output .= implode('<br>', $errors);
		$output .= "\n</div>\n";
		return $output;
	}

	/**
	 * Get the form footer
	 *
	 * This includes hidden inputs, the buttons, and anything else we need.
	 * @return string
	 */
	protected function get_form_footer_display() {
		$output = '';
		// Get the usual hidden inputs
		$output .= $this->get_hidden_inputs_display();
		// Close the main form tag
		$output .= '</form></div>' . "\n";
		// Get the bottom form with buttons 'n stuff
		$output .= $this->get_lower_form_display();

		// If this form contains at least one upload prompt, add a hidden
		// iframe to handle the file upload
		if ($this->contains_upload) {
			$output .= '<iframe style="display: none;" id="upload_iframe" name="upload_iframe"></iframe>';
		}

		return $output;
	}

	/**
	 * Get the standard set of hidden inputs that we include on forms
	 * @return string
	 */
	protected function get_hidden_inputs_display() {
		$output = '';
		$output .= '<input type="hidden" value="' . $this->get_name() . '" name="formid">';
		$output .= '<input type="hidden" value="' . $this->get_dispatch_id() . '" name="dispatch_id">';

		// If uploading, give the submit handler a hint about where to send the files
		if ($this->contains_upload) {
			$output .= '<input type="hidden" name="formprefix" value="'.common_utilities::get_scriptprefix().'" />';
			$output .= '<input type="hidden" name="uploadprompt_handler" value="';
			$output .= FISDAP_WEB_ROOT . 'phputil/common_uploadhandler.php" />';
		}

		return $output;
	}

	/**
	 * Get the lower form, that contains the buttons and is submitted with GET
	 * @return string
	 */
	protected function get_lower_form_display() {

		$formtag = '<div class="common_form_footer"><form action="' . $this->action . '"';
		$formtag .= ' id="' . $this->name . 'Buttons"';
		$formtag .= ' method="GET">';

		$output = $formtag . "\n";

		$output .= $this->get_form_buttons_display();

		$output .= '</form></div>' . "\n";

		return $output;
	}

	/**
	 * Returns a string of HTML for the buttons on this form. This does not 
	 * include the second form that wraps these buttons, just the button div 
	 * itself.
	 *
	 * <i>Doesn't print anything to stdout</i>
	 *
	 * @return string
	 */
	protected function get_form_buttons_display() {

		$submissionattempt = (common_utilities::get_scriptvalue("submissionattempt") + 1);
		$output = '';

		// Build the onclick event for the Submit button
		if ($this->submission_technique == "ajax") {
			$onclick_submitevent = ' onclick="ajaxSubmitHandler(' . "'" . $this->name . "'" . ', this); return false;"';
		} else {
			$onclick_submitevent = ' onclick="formSubmitHandler(this.id,' . "'" . $this->name . "'" . '); return false;"';
		}

		$output .= '<input type="hidden" value="' . $this->name . '" name="formid">';
		$output .= '<input type="hidden" value="' . $this->get_dispatch_id() . '" name="dispatch_id">';
		$output .= '<input type="hidden" value="' . $submissionattempt . '" name="submissionattempt">' . "\n";

		$output .= '<div class="common_form_buttons" id="' . $this->name . '_footer">';

		if ($this->delete_warning_message == true) {
			$output .= '<span class="mediumboldtext">Are you sure you want to delete this item?</span>';
		}

		foreach ($this->form_buttons as $button) {
			// If it's a hidden button, skip it
			if ($button->is_hidden()) continue;

			$buttonmarkup = '<input type="submit" class="button"';
			// See get_button_type() for an explanation
			$buttonmarkup .= ' name="' . $button->get_name() . '"';
			// Make the id unique to this form
			$buttonmarkup .= ' id="' . $button->get_id($this->name) . '"';
			// The text to be displayed on the button
			$buttonmarkup .= ' value="' . $button->get_value() . '"';
			// Sending an AJAX POST on delete cancel will blow away our session
			// information, since all prompts are disabled
			if ($button->get_type() != CommonFormButton::DELETE_CANCEL) {
				$buttonmarkup .= $onclick_submitevent;
			}
			$buttonmarkup .= ' />';

			$output .= $buttonmarkup;
		}
		// Make the throbber and submit button
		$output .= '<img id="' . $this->name . '_throbber" class="common_form_throbber" src="' . FISDAP_WEB_ROOT . 'images/throbber_small.gif" alt="">';
		$output .= '</div>';

		return $output;
	}

	/**
	 * @deprecated Use get_html() instead
	 * @ignore
	 */
	public function get_generated_html_form() {
		$this->logger->deprecated('Use get_html() instead.');
		return $this->get_html();
	}

	/**
	 * Get the HTML output for the form
	 *
	 * @return string the HTML output
	 */
	public function get_html() {

		if (!isset($this->generated_html)) {
			$form = $this->display();

			$tag = '<div id="' . $this->name . '_form_div" class="common_form_containing_div">';
			$endtag = '</div>';

			$this->generated_html = $tag . $form . $endtag;
		}

		return $this->generated_html;
	}

	/**
	 * Create HTML code for the form.
	 *
	 * Does not include the outer div with the form id - if you want that, you 
	 * should use {@link get_html()}.
	 *
	 * @return string the HTML
	 * @todo public?
	 */
	abstract protected function display();

}

/**
 * Defines a button on a common_form
 *
 * A button may be one of several types defined by the class constants.
 * Buttons may be added to forms with {@link CommonFormBase::set_button()}.
 * Forms may only have one button of a particular type at a time.
 */
class CommonFormButton {

	// Having these constants in the order of appearance (from left to right) 
	// makes sorting for display super easy

	// These constants must also be reconciled with the logic in 
	// get_button_index()

	/**
	 * Don't use this value
	 */
	const DUMMY = 1;
	/**
	 * A cancel button
	 *
	 * The script should catch this button and return the user to the screen 
	 * they were on before.
	 */
	const CANCEL = 20;
	/**
	 * A special type of cancel button, used to cancel out of a delete confirmation.
	 *
	 * No action needed by the script.
	 */
	const DELETE_CANCEL = 21;
	/**
	 * A delete button
	 *
	 * The script should catch this button and delete the item this form 
	 * represented.  This button should be used in delete mode or indirectly 
	 * through the UNCONFIRMED_DELETE button.
	 */
	const DELETE = 30;
	/**
	 * A special type of delete button that will lead to a deletion confirmation screen
	 *
	 * No action needed by the script.
	 */
	const UNCONFIRMED_DELETE = 31;
	/**
	 * A reset button
	 *
	 * Resets the form values to the defaults set by the developer. No action needed 
	 * by the script.
	 */
	const RESET = 40;
	/**
	 * A submit button
	 *
	 * Submits the form and all that jazz.
	 */
	const SUBMIT = 100;

	/**
	 * What type of button this is
	 * @var int One of the class constants
	 */
	private $type;
	/**
	 * The value (i.e. display text) of this button
	 * @var string
	 */
	private $value;
	/**
	 * Is this button hidden?
	 * @var boolean
	 */
	private $hidden;

	/**
	 * @todo validate $type
	 * @param int $type One of the class constants
	 * @param string $value The value (i.e. display text) of the button
	 */
	public function __construct($type, $value=null) {

		$this->set_type($type);

		if ($value == null) {
			$value = $this->get_default_value();
		}

		$this->set_value($value);

		$this->hidden = false;
	}

	public function hide() {
		$this->hidden = true;
	}

	public function show() {
		$this->hidden = false;
	}

	public function is_hidden() {
		return $this->hidden;
	}

	public function get_value() {
		return $this->value;
	}

	public function set_value($value) {
		$this->value = $value;
	}

	/**
	 * @return int One of the class constants
	 */
	public function get_type() {
		return $this->type;
	}

	/**
	 * @param int $type One of the class constants
	 */
	public function set_type($type) {
		Assert::is_in_array($type, array(self::SUBMIT, self::CANCEL, self::RESET, self::DELETE, self::UNCONFIRMED_DELETE, self::DELETE_CANCEL));
		$this->type = $type;
	}

	/**
	 * Get the id for this element.
	 * @todo why are we using default val? Why is this different from get_name?
	 * @todo test this
	 *
	 * @return string the id
	 */
	public function get_id($name) {
		$idstr = $name;
		$idstr .= '_' . strtolower($this->get_default_value());
		return $idstr;
	}

	/**
	 * Get the default value for this button
	 * @return string
	 */
	private function get_default_value() {
		switch($this->type) {
		case self::SUBMIT:
			$value = 'Submit';
			break;
		case self::CANCEL:
		case self::DELETE_CANCEL:
			$value = 'Cancel';
			break;
		case self::DELETE:
		case self::UNCONFIRMED_DELETE:
			$value = 'Delete';
			break;
		case self::RESET:
			$value = 'Clear Form';
			break;
		}
		return $value;
	}

	/**
	 * Get the button element's name attribute.
	 *
	 * Button names must be unique to that type of button. Here's why: 
	 *
	 * We need to get the button type as a value separate from the actual 
	 * text of the button. The obvious way to do this would be to use the 
	 * <button> element. I say 'would' because IE bungs up those elements so 
	 * badly it's not feasible to use them. So we have to pass the constant 
	 * as part of the input name instead. Awesome.
	 * @todo test this
	 */
	public function get_name() {
		return 'submit_button_' . $this->get_type();
	}


	/**
	 * Comparator for button order - greater means further right
	 * @return int
	 */
	public static function compare($button1, $button2) {
		return $button1->get_type() - $button2->get_type();
	}
}

?>
