<?php

/*
 * schedulerutilities.inc - Scheduler Utilities
 */

/*
 * CLASS scheduler_utilties - Static Utility Methods
 */

class scheduler_utilities {

	function student_meets_req_certs($student,$event) {

		$site_sel = "SELECT AmbServ_id FROM EventData where Event_id=$event";
		$connection = FISDAPDatabaseConnection::get_instance();
		$site_res = $connection->query($site_sel);
		$site = $site_res[0]['AmbServ_id'];

		$prog_sel = "SELECT Program_id FROM StudentData where Student_id=$student";
		$prog_res = $connection->query($prog_sel);
		$program_id = $prog_res[0]["Program_id"];

		$site_array = scheduler_utilities::get_tmp_comp_sites($program_id);

		if(in_array($site,$site_array))
		{	
			$required_comp = scheduler_utilities::get_required_tmp_comp($program_id);
			$completed_comps = array();

			$completed_comp_sel = "SELECT comp_id FROM tmpStudentCompTable WHERE Student_id=$student";
			$completed_comp_res = $connection->query($completed_comp_sel);

			if(sizeof($completed_comp_res)==0)
			{
				return false;
			}

			for($b=0;$b<sizeof($completed_comp_res);$b++)
			{
				$completed_comps[]=$completed_comp_res[$b]["comp_id"];
			}

			$missing = 0;

			for($a=0;$a<sizeof($required_comp);$a++)
			{
				if(!(in_array($required_comp[$a],$completed_comps)))
				{
					$missing = 1;
				}
			}

			if($missing == 1) {
				return false;
			}
			else {
				return true;
			}
		}
		else
		{
			return true;
		}
	}

	function get_comp_group($program_id)
	{
		$group1 = array(595,850,477,876,313,832,829,430,72);
		$group2 = array(452);
		$group3 = array(3,259);
		if(in_array($program_id, $group1))
		{
			return 1;
		}
		else if(in_array($program_id, $group2))
		{
			return 2;
		}
		else if(in_array($program_id, $group3))
		{
			return 3;
		}
		else {
			return 0;
		}
	}

	/*
	 * Given a $program_id, returns an array containing all programs for which they can edit competencies.
	 */

	function programs_can_admin($program_id) {
		if ($program_id == 595) return array(595,850,477,876,313,832,829,430,72);
		if ($program_id == 452) return array(452);
		if ($program_id == 259) return array(3,259);
		
		if (scheduler_utilities::get_comp_group($program_id)) { return array($program_id); }
		else return array();
	}

	function get_tmp_comp_sites($program_id)
	{
		//$retarray = array(3181,3180);
		$group = scheduler_utilities::get_comp_group($program_id);
		$retarray = array();
		if($group == 1)
		{
			$retarray = array(616,4358,5429);
		}
		else if($group==2)
		{
			$retarray = array();
		}
		else if($group==3)
		{
			$retarray = array(3180);
		}
		else
		{
			$retarray = array();
		}
		return $retarray;
	}

	function program_admin_comp($program,$stuprogram)
	{
		$found = 0;
		/*		$connection = FISDAPDatabaseConnection::get_instance();
				$site_array = scheduler_utilities::get_tmp_comp_sites($program);

				$found = 0;

				$adminsel = "SELECT Site_id FROM ProgramSiteAssoc WHERE Program_id=$program AND Main=1";
				$adminres = $connection->query($adminsel);

				for($j=0;$j<sizeof($adminres);$j++)
				{
				if(in_array($adminres[$j]["Site_id"],$site_array))
				{
				$found = 1;
				}
				}
		 */
		$group = scheduler_utilities::get_comp_group($program);
		$stugroup = scheduler_utilities::get_comp_group($stuprogram);
		if($group!=$stugroup)
		{
			return false;
		}
		else
		{
			if($group == 1)
			{
				if($program==595)
				{
					$found=1;
				}
			}
			else if ($group==2)
			{
				if($program == 452)
				{
					$found=1;
				}	
			}
			else if($group==3)
			{
				if($program==259)
				{
					$found=1;
				}
			}
			else{$found=0;}
			if($found==1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}	
	}

	function program_use_tmp_comp($program)
	{
		/*
		   $connection = FISDAPDatabaseConnection::get_instance();
		   $site_array = scheduler_utilities::get_tmp_comp_sites($program);

		   $found = 0;

		   $prog_site_sel = "SELECT AmbServ_id FROM ProgramSiteData where Program_id=$program";
		   $prog_site_res = $connection->query($prog_site_sel);

		   for($i=0;$i<sizeof($prog_site_res);$i++)
		   {
		   $tmpsite = $prog_site_res[$i]["AmbServ_id"];
		   if(in_array($tmpsite,$site_array))
		   {
		   $found = 1;
		   }
		   }
		 */
		//$programs = array(595,850,477,876,313,832,829,430);
		//	if(in_array($program,$programs))
		//		{
		//			$found=1;
		//		}
		$group = scheduler_utilities::get_comp_group($program);

		if($group > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function get_required_tmp_comp($program_id)
	{
		$group = scheduler_utilities::get_comp_group($program_id);
		if($group==1)
		{
			$req_array = array(18,19,36,37);
			//$req_array = array(1,2,3,4,5,6,7,9,10,12,13,14,15,17,18,19);
			//$req_array = array(10,11,12);
		}
		else if($group==2)
		{
			$req_array = array();
		}
		else if($group==3)
		{
			$req_array = array(22);
		}
		else
		{
			$req_array = array();
		}
		return $req_array;
	}

	/*
	 * Returns a resultset with Student id, First name, Last name, Competency id, and Date completed for 
	 * each student, one row per competency.
	 * Uses a left join so that even if a student has no competencies, they will be in the result set if 
	 * their id is passed into the function.
	 */

	function get_competencies_by_ids($idArray,$orderbylastname=false) {
		$connection =& FISDAPDatabaseConnection::get_instance();

		$student_query = "SELECT StudentData.Student_id as StudentID, StudentData.FirstName as First, StudentData.LastName as Last, tmpStudentCompTable.comp_id as CompID, tmpStudentCompTable.date_completed as Date " .
			"FROM StudentData " .
			"LEFT JOIN tmpStudentCompTable ON tmpStudentCompTable.Student_id = StudentData.Student_id " .
			"WHERE StudentData.Student_id IN (" . implode(",", $idArray) . ") " ;
		if ($orderbylastname) {
			$student_query .= 'ORDER BY Last, First, CompID';
		}
		else {
			$student_query .= 'ORDER BY StudentID, CompID';
		}
		return $connection->query($student_query);
	}

	/*
	 * Returns an array with the keys 'href', 'title', 'desc', and 'id', ordered by the 'title'.
	 */

	function get_requirement_titles($program) {
		//todo this needs to get the right group first
		$group = scheduler_utilities::get_comp_group($program);

		$connection = &FISDAPDatabaseConnection::get_instance(); // Connect to MySQL Database   
		$reqlist = array();

		$select = "SELECT * FROM tmpCompTable WHERE comp_group = $group ORDER BY title";
		$result = $connection->query($select);

		$reqlist = array();
		/*	for($count=0; $count < sizeof($result); $count++)
			{
			$reqlist[$count]['href'] = $result[$count]['url'];
			$reqlist[$count]['title'] = $result[$count]['title'];
			$reqlist[$count]['desc'] = $result[$count]['description'];
			$reqlist[$count]['id'] = $result[$count]['comp_id'];
			}
		 */
		foreach ($result as $row) {
			$idx = $row['comp_id'];
			$reqlist[$idx]['href'] = $row['url'];
			$reqlist[$idx]['title'] = $row['title'];
			$reqlist[$idx]['desc'] = $row['description'];
		}

		return $reqlist;
	}

}
