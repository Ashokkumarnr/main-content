<?php
/**
 * A counter.
 * <pre>
 * Common usage:
 *   ...
 *   $counter = new Counter('sorting'); 
 *   $counter->increment();
 *   ...
 *   usort(...);
 *   ...
 *   devlog($sw);
 * </pre>
 */
class Counter {
    private $name;
    private $value;

    /**
     * Constructor.
     * @param string $name The counter name.
     * @param int $value The initial value.
     */
	public function __construct($name, $value=0) {
        $this->name = $name;
        $this->value = $value;
	}

	/**
	 * Retrieve the value.
	 * @return int The counter value.
	 */
	public function get_value() {
        return $this->value;
	}

    /**
     * Overridden.
     */
    public function __toString() {
        return 'Counter ' . $this->name . ': ' . $this->get_value();
    }

    /**
     * Reset the counter to zero.
     */
	public function reset() {
        $this->value = 0;
	}

    /**
     * Set the counter value.
     * @param int $value The new value.
     */
	public function set_value($value) {
        $this->value = $value;
	}

    /**
     * Increment the value.
     * @param int $n The amount to add.
     */
    public function increment($n=1) {
        $this->value += $n;
    }

    /**
     * Decrement the value.
     * @param int $n The amount to subtract.
     */
    public function decrement($n=1) {
        $this->value -= $n;
    }
}
?>
