<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/mail/NoMailTransport.inc');
require_once('phputil/classes/mail/SendmailTransport.inc');
require_once('phputil/classes/mail/ZendTransport.inc');

/**
 * A factory to create transports.
 */
final class MailTransportFactory {
    const DEFAULT_TRANSPORT_NAME = 'ZendTransport';

    /**
     * Create a transport.
     * @param string | null $class_name The transport class name.
     * @return MailTransport The transport.
     */
    public static function create($class_name=null) {
        Assert::is_true(
            Test::is_string($class_name) ||
            Test::is_null($class_name));

        if (is_null($class_name)) {
            $class_name = self::DEFAULT_TRANSPORT_NAME;
        }

        // Include any logic here to set up the transport parameters.
        $transport = new $class_name();
        return $transport;
    }
}
?>
