<?php
require_once('phputil/classes/mail/MailMessage.inc');
require_once('phputil/classes/mail/MailTransportFactory.inc');

/**
 * Handle mailing messages.
 */
final class Mailer {
    private static $transport;

    /**
     * Send the message.
     * @param MailMessage $message The message.
     */
    public static function send(MailMessage $message) {
        self::get_transport()->handle($message);
    }

    /**
     * Retrieve the transport.
     * If you did not set one, you still get one back.
     */
    public static function get_transport() {
        if (is_null(self::$transport)) {
            self::$transport = MailTransportFactory::create();
        }

        return self::$transport;
    }

    /**
     * Set a mail transport to handler the mail.
     * @param MailTransport $transport The handler transport or null for the 
     * default transport.
     */
    public static function set_transport($transport) {
        Assert::is_true(
            Test::is_null($transport) || 
            Test::is_a($transport, 'MailTransport'));

        self::$transport = $transport;
    }
}
?>
