<?php
require_once('phputil/classes/mail/AbstractMailTransport.inc');

/**
 * Handle messages via sendmail.
 */
final class SendmailTransport extends AbstractMailTransport {
    public function do_handle(MailMessage $message) {
        $headers = array();
        $headers[] = 'From: ' . $message->get_from()->get_email();

        $emails = $message->get_reply_to();
        if (count($emails)) {
            $headers[] = 'Reply-To: ' . self::get_list_as_string($emails);
        }

        $emails = $message->get_cc();
        if (count($emails)) {
            $headers[] = 'Cc: ' . self::get_list_as_string($emails);
        }

        $emails = $message->get_bcc();
        if (count($emails)) {
            $headers[] = 'Bcc: ' . self::get_list_as_string($emails);
        }

        return Mail(self::get_list_as_string($message->get_to()),
            $message->get_subject(), 
            $message->get_text_body(),
            join("\r\n", $headers));
    }

    private static function get_list_as_string($list) {
        $parts = array();

        foreach ($list as $address) {
            $parts[] = self::get_email_as_string($address);
        }

        return join(',', $parts);
    }

    private static function get_email_as_string(EmailAddress $address) {
        $name = $address->get_name();

        if (is_null($name)) {
            return $address->get_email();
        }

        $name = addslashes($name);
        if ($name != $address->get_name()) {
            $name = '"' . $name . '"';
        }

        return $name . ' <' . $address->get_email() . '>';
    }
}
?>
