<?php

/**
 * common_page.inc - Defines a Standard FISDAP Page
 *
 * Includes:
 *
 *  - common_page
 *
 * @package CommonInclude
 * @author Warren Jacobson
 */

require_once("phputil/classes/common_user.inc");
require_once("phputil/classes/common_utilities.inc");

/**
 * Defines a Standard FISDAP Page
 */

class common_page {

	var $title; // The title of the page
	var $username; // The User Name of the authenticated user
	var $maskedusername; // The Masked User Name of the authenticated user
	var $redirect_url; // The URL to redirect the user to via JavaScript
	var $javascriptcode; // JavaScript code to be added to the <head> section
	var $javascriptsrc; // An array of Javascript source URL's
	var $stylesheetcode; // CSS code to be added to the <head> section
	var $stylesheetsrc; // An array of CSS source URL's
	var $ajax_enabled; // true if enabled, false otherwise

	const BOTTOM_OF_PAGE_ANCHOR_NAME = 'cp_bottom_of_page';
	const TOP_OF_PAGE_ANCHOR_NAME = 'cp_top_of_page';

	/**
	 * Create a new instance of the page class
	 */

	function common_page($title,$redirect_url) {

		// Default the title to FISDAP if none provided

		if ($title == null or isset($title) == false) {
			$title = "ERROR! PAGE TITLE HAS NOT BEEN ASSIGNED!";
		}

		// Get the username

		$username = null;
		if (isset($_SESSION[SESSION_KEY_PREFIX . 'username']) ) {
			$username = htmlentities($_SESSION[SESSION_KEY_PREFIX . 'username']);
		}
		$this->username = $username;

		// Get the masquerade username. Functions can be found in session_functions.php

		$maskedusername = null;
		if (is_masquerade()) {
			$maskedusername = htmlentities(get_masquerade_username());
		}
		$this->maskedusername = $maskedusername;

		// Define remaining properties

		$fwr = FISDAP_WEB_ROOT; // Absolute path to web root so that this code is portable

		$this->javascriptcode = null; // No additional JavaScript code in the <head> section yet
		$this->javascriptsrc = array(); // No JavaScript source yet
		$this->stylesheetcode = null; // No additional CSS code in the <head> section yet
		$this->stylesheetsrc = array(); // No additional CSS files yet
		$this->stylesheetsrc_ie = array(); // No additional CSS files yet
		$this->ajax_enabled = false; // Ajax is not enabled by default

		$this->set_title($title); // Set the title
		$this->set_redirect_url($redirect_url); // Set the redirect URL
		$this->add_javascriptcode('var default_scriptname = "' . common_utilities::get_scriptname() . '";');
		$this->add_javascriptsrc($fwr . 'phputil/prototype.js');
		$this->add_javascriptsrc($fwr . 'phputil/scriptaculous.js');
		$this->add_javascriptsrc($fwr . 'phputil/effects.js');
		$this->add_javascriptsrc($fwr . 'phputil/common_page.js'); // JavaScript used by common_page

		// Include common stylesheets
		$this->add_stylesheetsrc($fwr . 'common_form.css');
		$this->add_stylesheetsrc($fwr . 'common_form_ie.css', array('conditional' => 'lte IE 7'));

	}

	/**
	 * Set a page's title
	 */

	function set_title($title) {

		$this->title = $title;

	}

	/**
	 * Set the Redirect URL
	 */

	function set_redirect_url($redirect_url) {

		$this->redirect_url = $redirect_url;

	}

	/**
	 * Add JavaScript to the <head> section
	 */

	function add_javascriptcode($javascriptcode) {

		$this->javascriptcode .= $javascriptcode;

	}

	/**
	 * Add a link to an external Javascript file
	 */

	function add_javascriptsrc($javascriptsrc) {

		if (!in_array($javascriptsrc, $this->javascriptsrc)) {
			$this->javascriptsrc[] = $javascriptsrc;
		}

	}

	/**
	 * Add inline CSS code to the <head> section
	 */

	function add_stylesheetcode($stylesheetcode) {

		$this->stylesheetcode .= $stylesheetcode;

	}

	/**
	 * Add a link to an external CSS file
	 *
	 * @param string $stylesheetsrc the location of the stylesheet to add
	 * @param array $opts an array of key/value option pairs.  Currently the
	 * only supported pair is 'conditional' => 'clause', where 'clause' is
	 * the test case for a conditional comment (for ex. 'lte IE7').
	 */

	function add_stylesheetsrc($stylesheetsrc, $opts=array()) {

		// The usual stylesheet array
		$sheet_arr = &$this->stylesheetsrc;

		// If this is conditionally commented, put it in the conditional array
		foreach ($opts as $key => $val) {
			switch ($key) {
			case 'conditional':
				$sheet_arr = &$this->stylesheetsrc_ie[$val];
				break;
			default:
				common_utilities::displayFunctionError("Stylesheet option $key => $val not recognized.");
				break;
			}
		}

		if (!is_array($sheet_arr) || !in_array($stylesheetsrc, $sheet_arr)) {
			$sheet_arr[] = $stylesheetsrc;
		}

	}

	/**
	 * Include the JavaScript that common_form needs to do it's thing
	 * @deprecated Everything we need is included by default now
	 */

	function enable_common_form() {
		// Nothing here any more
	}

	/**
	 * Display the page's header
	 */

	public function display_header() {

		$fwr = FISDAP_WEB_ROOT; // Absolute path to web root so that this code is portable

		// Begin buffering all output

		ob_start(); // We're buffering all output

		// Output the header stuff
		$this->display_header_info();

		// Prepare the onload redirect stuff for the <body> tag

		$redirect_string = null;
		if ($this->redirect_url != null) {
			$redirect_string = 'redirect_with_delay(' . "'" . $this->redirect_url . "'" . ');';
		}

		// Display the <body> tag and the navigation up top

		echo "<body \n";
		echo "onLoad=\"$redirect_string MM_preloadImages('".
			$fwr."images/home_f2.gif','".
			$fwr."images/dataentry_f2.gif','".
			$fwr."images/schedblack_f2.gif','".
			$fwr."images/reports_f2.gif','".
			$fwr."images/admin_f2.gif','".
			$fwr."images/help_f2.gif','".
			$fwr."images/smlogo.gif');\">\n";
		echo "<a name='".self::TOP_OF_PAGE_ANCHOR_NAME."'></a>\n";

		$this->display_body();

		echo '<div id="fisdap_logger_messages"></div>';

		// All done generating the header. What to do with the output?

		$output = ob_get_contents(); // Send the output to common_html
		ob_end_clean(); // Delete the contents of the output buffer
		echo $output;

	}

	/**
	 * Display the page's header WITHOUT the FISDAP navbar
	 */

	public function display_header_sans_navbar() {

		$fwr = FISDAP_WEB_ROOT; // Absolute path to web root so that this code is portable

		// Begin buffering all output

		ob_start(); // We're buffering all output

		// Output the header stuff
		$this->display_header_info();

		// Display the <body> tag 
		echo "<body>\n";
		echo '<div id="fisdap_logger_messages"></div>';

		// All done generating the header. What to do with the output?

		$output = ob_get_contents(); // Send the output to common_html
		ob_end_clean(); // Delete the contents of the output buffer
		echo $output;

	}


	/**
	 * Display the page's header stuff (the copyright stuff, all the js and css includes, etc) 
	 * This is just an internal method that provides the duplicate code needed for both display_header and
	 * display_header_sans_navbar
	 */

	private function display_header_info() {

		$fwr = FISDAP_WEB_ROOT; // Absolute path to web root so that this code is portable

		// Begin buffering all output

		ob_start(); // We're buffering all output


		// Display the doctype and the copyright notice

		define('DOCTYPE', 'HTML_401_TRANSITIONAL');
		require_once('phputil/copyright.php');

		// Display the <html> tag and the <head> section of the HTML document

?>
	<html>
	<head>
	<title><?php echo "FISDAP " . $this->title; ?></title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<?php

		// Link to the standard common_page.css stylesheet (kept at web root)


?>
<link href="<?php echo $fwr; ?>common.css" rel="stylesheet" type="text/css">
<!--<link href="<?php echo $fwr; ?>common_page.css" rel="stylesheet" type="text/css">-->
<?php

		// Check if we're on production or development, give ridiculous banner if on dev
		if (HandyServerUtils::get_environment() != 'production') {
			echo "<link href='".$fwr."common_dev.css' rel='stylesheet' type='text/css'>";
		}

		// Add external CSS files
		foreach ($this->stylesheetsrc as $this_stylesheetsrc) {
			echo '<link href="' . $this_stylesheetsrc . '" rel="stylesheet" type="text/css">' . "\n";
		}

		// Add external CSS files that are conditionally commented
		foreach ($this->stylesheetsrc_ie as $cond => $arr) {
			foreach ($arr as $this_stylesheetsrc) {
				echo "<!--[if $cond]>";
				echo '<link href="' . $this_stylesheetsrc . '" rel="stylesheet" type="text/css">';
				echo "<![endif]-->\n";
			}
		}


		// Add inline CSS code to the <head> section
		/*
		 * A "workaround" to make IE6 display :hover css rules
		 */
		echo '<!--[if lte IE 6]><style type="text/css">';
		echo 'body { behavior: url("'.$fwr.'phputil/ie6hover.htc"); }';
		echo '</style><![endif]-->' . "\n";
?>
<style type="text/css">
<?php
		echo $this->stylesheetcode . "\n";
?>
</style>
<?php

		// Add external JavasScript files
		foreach ($this->javascriptsrc as $this_javascriptsrc) {
			echo '<script type="text/javascript" src="' . $this_javascriptsrc . '"></script>' . "\n";
		}

		// Add inline JavaScript code to the <head> section
?>
		<script type='text/javascript'>
<?php
		echo $this->javascriptcode . "\n";
?>
		</script>
</head>
<?php
		$output = ob_get_contents(); // Send the output to common_html
		ob_end_clean(); // Delete the contents of the output buffer
		echo $output;
	}

	/**
	 * Display just the body portion of the standard page header
	 */

	function display_body() {

		$fwr = FISDAP_WEB_ROOT; // Absolute path to web root so that this code is portable

?>
<div id="header">
<img src="<?php echo $fwr; ?>images/smlogo.gif" name="logo" id="LogoImg" alt="FISDAP. Be a part of the solution." width="141" height="50">
<div id='main_links'>
<?php
		echo "<a href=\"".$fwr."index.html?target_pagename=home.html\" ".
			"onMouseOut=\"MM_swapImgRestore();\" ".
			"onClick=\"return checkLink();\" ".
			".onMouseOver=\"MM_swapImage('home', '', '".$fwr."images/home_f2.gif', 1);\">".
			"<img name=\"home\" class=\"short_menu_image\" src=\"".$fwr."images/home.gif\" alt=\"Home\" width=\"56\" height=\"10\"></a>\n";
		echo "<a href=\"".$fwr."index.html?target_pagename=shift/index.html\" ".
			"onMouseOut=\"MM_swapImgRestore();\" ".
			"onClick=\"return checkLink();\" ".
			"onMouseOver=\"MM_swapImage('dataentry', '', '".$fwr."images/dataentry_f2.gif', 1);\">".
			"<img name=\"dataentry\" class=\"tall_menu_image\" src=\"".$fwr."images/dataentry.gif\" alt=\"Data Entry\" width=\"97\" height=\"13\"></a>\n";
		echo "<a href=\"".$fwr."index.html?target_pagename=scheduler/schedulercont.html\" ".
			"onMouseOut=\"MM_swapImgRestore();\" ".
			"onClick=\"return checkLink();\" ".
			"onMouseOver=\"MM_swapImage('schedblack', '', '".$fwr."images/schedblack_f2.gif', 1);\">".
			"<img name=\"schedblack\" class=\"short_menu_image\" src=\"".$fwr."images/schedblack.gif\" alt=\"Scheduler\" width=\"99\" height=\"10\"> </a>\n";
		echo "<a href=\"".$fwr."reports/index.html\" ".
			"onMouseOut=\"MM_swapImgRestore();\" ".
			"onClick=\"return (checkLink());\" ".
			"onMouseOver=\"MM_swapImage('reports', '', '".$fwr."images/reports_f2.gif', 1);\">".
			"<img name=\"reports\" class=\"tall_menu_image\" src=\"".$fwr."images/reports.gif\" alt=\"Reports\" width=\"82\" height=\"13\"></a>\n";
		echo "<a href=\"".$fwr."index.html?target_pagename=admin/index.html\" ".
			"onMouseOut=\"MM_swapImgRestore();\" ".
			"onClick=\"return checkLink();\" ".
			"onMouseOver=\"MM_swapImage('admin', '', '".$fwr."images/admin_f2.gif', 1);\">".
			"<img name=\"admin\" class=\"short_menu_image\" src=\"".$fwr."images/admin.gif\" alt=\"Admin\" width=\"66\" height=\"10\"></a>\n";
		echo "<a href=\"".$fwr."index.html?target_pagename=content/help\" ".
			"onMouseOut=\"MM_swapImgRestore();\" ".
			"onClick=\"return checkLink();\" ".
			"onMouseOver=\"MM_swapImage('help', '', '".$fwr."images/help_f2.gif', 1);\">".
			"<img name=\"help\" class=\"tall_menu_image\" src=\"".$fwr."images/help.gif\" alt=\"Help\" width=\"61\" height=\"13\"></a>\n";
		echo "<div class=\"spacer_row\"></div>\n";
		echo "</div>\n";

		// Display login information

		if ($this->username != null) {
			$username = htmlentities($this->username);

?>
<div id='login_info'>You're logged in as "<?php echo $username; ?>"
<?php

			if ($this->maskedusername != null) {
				$maskedusername = htmlentities($this->maskedusername);
				echo "[masked as \"$maskedusername\"]&nbsp;(<a href=\"".$fwr."internal/user_switcher/unmask.php\" style=\"color:#ffffff;\" ".
					"onClick=\"return checkLink();\">Unmask</a>)\n";
			}
?>
&nbsp;(<a href="<?php echo $fwr; ?>auth/logout.php" style="color:#ffffff;" onClick="return checkLink();">Log out</a>)
</div>
<?php

		}

		// Display today's date and the link to My Fisdap

?>
<div id='sidebar'>
<?php echo Date("F jS, Y"); ?>
<div class='mini_links'>
<span style="font-size:12px;">
<a href="<?php echo $fwr; ?>index.html?target_pagename=my_fisdap/my_fisdap.php" style="color:#ffffff;" onClick="return checkLink();">My FISDAP</a>
</span>
</div>
</div>
</div>
<div id="content">
<?php
	}

	/**
	 * Display the page's footer
	 */

	function display_footer() {

		ob_start(); // We're buffering all output

		// Display the footer

?>
<a name="<?php echo self::BOTTOM_OF_PAGE_ANCHOR_NAME;?>"></a></div>
</body>
</html>
<?php

		// All done generating the footer. What to do with the output?

		$output = ob_get_contents(); // Send the output to common_html
		ob_end_clean(); // Delete the contents of the output buffer
		echo $output;

	}

	/**
	 * Add a link to the top of the page.
	 */
	public final function add_top_of_page_link() {
		echo '<div style="text-align: center; margin-top: 1em; margin-bottom: 1em">';
		echo '<a class="mediumtext" href="#';
		echo self::TOP_OF_PAGE_ANCHOR_NAME;
		echo '">Top of page</a></div>';
	}

	/**
	 * Add a link to the bottom of the page.
	 */
	public final function add_bottom_of_page_link() {
		echo '<div style="text-align: center; margin-top: 1em; margin-bottom: 1em">';
		echo '<a href="#';
		echo self::BOTTOM_OF_PAGE_ANCHOR_NAME;
		echo '">Bottom of page</a></div>';
	}

	/**
	 * Display the page's page title
	 */

	function display_pagetitle() {

		echo "<div class='largetext' align='center'>" . $this->title . "</div>";

	}

}

?>
