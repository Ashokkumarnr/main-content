<?php

/**
 * mini_eval_utils.inc - Utilities for getting info about mini evals
 */

/**
 * CLASS mini_eval_utils - Static Utility Methods
 */

class MiniEvalUtils {

	/**
	 * get_goal_for_program_minieval returns the goal given the program and minieval IDs
	 *
	 * Most useful tags:
	 * @todo refactor to use models
	 * @copyright 1999-2010 Headwaters Software, Inc.
	 * @param int $program the program id in question
	 * @param int $minieval_id the minieval id in question
	 * @return int the goal associated with the unique combination of program id and minieval id
	 */
	public static function get_goal_for_program_minieval($program, $minieval_id)  {

		$program_minieval_goal = "";
		
		$connection = FISDAPDatabaseConnection::get_instance();
		$query = "select * from Program_MiniEval_Data WHERE Program_id=$program and MiniEval_id=$minieval_id limit 1";

		$result = $connection->query($query);
		foreach ($result as $program_minieval_data_row) {
			$program_minieval_goal = $program_minieval_data_row["Goal"];
		}

		return $program_minieval_goal;
	}

	/**
	 * get_program_list_by_cert returns the list of mini evals for a given program and certification level.
	 *
	 * Most useful tags:
	 * @todo refactor to use models
	 * @copyright 1999-2010 Headwaters Software, Inc.
	 * @param int $program the program id in question
	 * @param string $certlvl the certification level in question
	 * @return array an array of MiniEval_ids
	 */
	public static function get_program_list_by_cert($program, $certlvl='all') {

		$return_array = array();

		$connection = FISDAPDatabaseConnection::get_instance();
		$query = "SELECT MiniEval_id FROM Program_MiniEval_Data WHERE Program_id=$program";
		if ($certlvl != 'all') {
			$query .= " AND CertLevel='$certlvl'";
		}
		$result = $connection->query($query);

		foreach ($result as $mini_eval_row) {
			$return_array[]=$mini_eval_row["MiniEval_id"];
		}

		return $return_array;
	}

	/**
	 * get_program_list_by_desc_by_cert returns the list of mini evals for a given program and certification level sorted by minieval description.
	 *
	 * Most useful tags:
	 * @todo refactor to use models
	 * @copyright 1999-2010 Headwaters Software, Inc.
	 * @param int $program the program id in question
	 * @param string $certlvl the certification level in question
	 * @return array an array of MiniEval_ids
	 */
	public static function get_program_list_by_desc_by_cert($program, $certlvl='all') {

		$return_array = array();

		$connection = FISDAPDatabaseConnection::get_instance();
		$query = "SELECT PMED.MiniEval_id FROM Program_MiniEval_Data PMED,MiniEval ME WHERE PMED.Program_id=$program and PMED.MiniEval_id = ME.MiniEval_id ";

		if ($certlvl != 'all') {
			$query .= " AND PMED.CertLevel='$certlvl'";
		}

		$query .= ' ORDER BY ME.MiniEval_desc';

		$result = $connection->query($query);

		foreach ($result as $mini_eval_row) {
			$return_array[]=$mini_eval_row["MiniEval_id"];
		}

		return $return_array;
	}


	/**
	 * get_mini_eval_desc returns the list of mini evals for a given program and certification level.
	 *
	 * Most useful tags:
	 * @todo refactor to use models
	 * @copyright 1999-2010 Headwaters Software, Inc.
	 * @param int $mini_eval_id the id of the MiniEval in question
	 * @return string the description of the MiniEval from the MiniEval table
	 */
	public static function get_mini_eval_desc($mini_eval_id) {

		$connection = FISDAPDatabaseConnection::get_instance();
		$query = "SELECT MiniEval_desc from MiniEval WHERE MiniEval_id=$mini_eval_id";
		$result = $connection->query($query);

		return $result[0]["MiniEval_desc"];
	}
}


?>
