<?php
require_once('phputil/classes/common_utilities.inc');

class CommonPromptGroup implements CommonFormComponent {

	/**
	 * @todo needed? delete?
	 */
	protected $state;
	/**
	 * The name of this prompt box
	 */
	protected $name;
	/**
	 * The array of objects to be displayed
	 */
	protected $displayobjects;

	public function __construct() {

		$this->set_name('mypromptbox' . common_utilities::get_unique_suffix());
		$this->displayobjects = array(); // Initialize this sucker

	}

	public function get_name() {
		return $this->name;
	}

	public function set_badinput($bool) {
		// simply a stub
	}

	public function set_name($name) {
		$this->name = $name;
	}

	public function set_form_state($state) {
		$this->state = $state;
		// Update the state info for any prompts we already know about
		foreach ($this->get_componentlist() as $component) {
			$component->set_form_state($this->state);
		}
	}

	/**
	 * Add a display object (e.g., prompt, section) to the form
	 *
	 * @param common_form_component The component to add
	 */
	public function add($component) {
		if ($component instanceof CommonFormComponent) {
			$component->set_form_state($this->state);
			$this->displayobjects[] = $component;
		} else {
			//TODO error out
		}
	}

	public function respond_formsubmithandler() {
		foreach ($this->get_componentlist() as $component) {
			$component->respond_formsubmithandler();
		}
	}

	/**
	 * Get a list of all prompts on this form
	 *
	 * This includes prompts in subgroups (such as promptboxes)
	 * @todo a unit test to make sure we check for CommonPrompt before CommonPromptGroup
	 */
	public function get_promptlist() {

		$promptlist = array();

		foreach ($this->get_componentlist() as $component) {

			// We check if it's a prompt before we check if it's a group
			// because composite prompts will want to be treated as prompts
			if ($component instanceof CommonPrompt) {
				$promptlist[] = $component;
			} else if ($component instanceof CommonPromptGroup) {
				$this_promptlist = $component->get_promptlist();
				$promptlist = array_merge($promptlist,$this_promptlist);
			}

		}

		return $promptlist;

	}

	/**
	 * Get a list of components in this group.
	 *
	 * This includes both prompts and other members of the group, such as
	 * other prompt groups or section titles.
	 */
	public function get_componentlist() {

		return $this->displayobjects;

	}

	public function respond_ajax($prompt) {
		//STUB
	}

	public function get_listeners() {
		// Aggregate all the listeners from our children
		$listeners = array();
		foreach ($this->get_componentlist() as $obj) {
			$listeners = array_merge_recursive($listeners, $obj->get_listeners());
		}
		return $listeners;
	}

	public function display() {

		$output = '<div class="common_promptgroup">' . "\n";

		foreach ($this->get_componentlist() as $object) {

			// Call the display method for this prompt and retrieve the prompt HTML
			$output .= $object->display();

		}

		$output .= '</div>';

		return $output;

	}

	/**
	 * @todo trash?
	 */
	public function get_column_width() {
	}

}

class CommonPromptBox extends CommonPromptGroup {
	/**
	 * An array containing the column layout of this prompt box
	 */
	protected $column_layout;
	/**
	 * The width of this prompt box
	 */
	protected $column_width;
	/**
	 * The height of this box, or null.
	 * @var int the height in pixels
	 */
	private $height;

	/**
	 * @param array $column_layout Establishes the width of prompt box columns from
	 * left to right. e.g., array(1,2) creates a 3 column wide prompt box with a 1
	 * column wide left column and a 2 column wide right column. array(1,1) creates
	 * a 2 column prompt box with two 1 column width columns
	 */
	public function __construct($column_layout) {
		parent::__construct();
		$this->set_column_layout($column_layout);
	}

	/**
	 * Set the column layout:
	 *
	 * Establishes the width of form columns from left to right.
	 * e.g., array(1,2) creates a 3 column wide form with a 1 column wide left
	 * column and a 2 column wide right column. array(1,1,1) creates a 3 column wide
	 * form with three 1 column width columns. Useful in conjunction with the
	 * common_promptbox class
	 *
	 * @param array $column_layout Either (1), (2), (3), (1,1), (2,1), (1,2), or (1,1,1)
	 */
	public function set_column_layout($column_layout) {

		if (!is_array($column_layout)) $column_layout = array($column_layout);

		$total_column_width = 0;
		foreach ($column_layout as $this_column_width) {
			if ($this_column_width != 1 and $this_column_width != 2 and $this_column_width != 3) {
				common_utilities::displayFunctionError('Invalid layout');
			}
			$total_column_width += $this_column_width;
		}
		if ($total_column_width < 1 or $total_column_width > 3) {
			common_utilities::displayFunctionError('Invalid layout');
		}

		$column_width = array_sum($column_layout);

		$this->column_layout = $column_layout;
		$this->column_width = $column_width;

	}

	public function add($component,$column=1) {

		$valid_column = true;
		if ($column != "1" and $column != "2" and $column != "3") $valid_column = false;
		if ($column > count($this->column_layout)) $valid_column = false;
		if (!$valid_column) {
			common_utilities::displayFunctionError('Invalid column number');
		}

		if ($component->get_column_width() != $this->column_layout[$column - 1]) {
			//common_utilities::displayFunctionError('Invalid column width');
		}

		$this->set_component_column($component, $column);
		return parent::add($component);

	}

	protected function set_component_column($component, $colnum) {
		$this->columninfo[$component->get_name()] = $colnum;
	}

	protected function get_component_column($component) {
		return $this->columninfo[$component->get_name()];
	}

	public function get_column_width() {
		return $this->column_width;
	}

	/**
	 * Set the height of the box.
	 * @param string|null $height The height in pixels. If null, no height will 
	 * be applied.
	 */
	public function set_height($height) {
		$this->height = $height;
	}

	public function display() {

		$output = null;
		$columnqueues = array();

		// Setup $tablestart and $tablefinish for use in the queues
		$column_css = '';
		if (!is_null($this->height)) {
			$column_css = ' style="overflow: scroll; height: ' . $this->height . 'px"';
		}
		$tablestart = '<table' . $column_css . '>';
		$tablefinish = '</table>';

		// Initialize the queues
		$column_count = count($this->column_layout);
		for ($colidx = 0; $colidx < $column_count; $colidx++) {
			$columnqueues[$colidx] = $tablestart;
		}

		// Build the encompassing <table> structure
		$this_width = common_form_utilities::get_column_width($this->column_width - 1);
		$inline_css = "width: " . $this_width . "px;";
		$tabletag = '<table style="' . $inline_css . '">';

		$output .= $tabletag . "\n";

		// Loop through each enclosed display object
		foreach ($this->get_componentlist() as $object) {

			// Retrieve the prompt HTML
			$prompthtml = $object->display();

			$hidden = ($object instanceof CommonHiddenPrompt ||
				// This next line is just for legacy purposes
				$object instanceof CommonLegacyPrompt && $object->get_type() == 'hidden');

			if (!$hidden) {
				// Build a nice table row for the prompt
				$beforeprompthtml = '<tr><td valign="top" align="left">';
				$afterprompthtml = '</td></tr>' . "\n";
				$prompthtml = $beforeprompthtml . $prompthtml . $afterprompthtml;
			}

			// What column are we trying to put a prompt in?
			$column = $this->get_component_column($object);

			// Place the output of the display object into the appropriate queue
			$columnqueues[$column - 1] .= $prompthtml;

		}

		// Make a spacer element
		$fwr = FISDAP_WEB_ROOT;
		$spacer = '<img src="' . $fwr . 'images/pixel.gif" width="9" height="1" alt="">';
		if ($columnqueues == array()) $spacer = null;

		// Now assemble the queues into code

		$output .= '<tr>';

		foreach ($columnqueues as $colidx => $q) {
			// Add a spacer to the code
			if ($colidx >= 1) {
				$output .= '<td width="9" valign="top" align="left">' . $spacer . '</td>';
			}
			// Close the tags for this column
			$q .= $tablefinish;
			// Build a table cell to put this column in
			$this_width = common_form_utilities::get_column_width($this->column_layout[$colidx] - 1);
			$output .= '<td width="' . $this_width . '">';
			$output .= $q;
		}
		$output .= '</tr></table>' . "\n";

		return $output;

	}
}

