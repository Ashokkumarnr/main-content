<?php
require_once('phputil/classes/counter.inc');

/**
 * This class manages counters.
 * <pre>
 * DEVELOPERS - ONLY BASIC FUNCTIONALITY HAS BEEN TESTED HERE !
 *
 * It provides an easy way to hold onto them in a static way so that they
 * can be used, for instance, each time a set of code is entered, i.e. a
 * method inside a class or object, a loop inside a class method, etc.
 * 
 * Typical usage:
 *
 *    function foo() {
 *      // Do NOT reuse old counter 
 *      $counter = CounterMgr::create('foo');     // sets to 0
 *      ...
 *      $counter->increment();
 *    }
 *
 *    function foo() {
 *      // Reuse old counter for acummulation
 *      $counter = CounterMgr::retrieve('foo');
 *      for (...) {
 *        $counter->increment();
 *        ...
 *        $counter->decrement();
 *      }
 *    }
 *
 *    function foo() {
 *      CounterMgr::increment('foo');
 *    }
 * </pre>
 */
class CounterMgr
{
    private static $counters = array(); // name=>integer value

    /**
     * Constructor is hidden.
     */
	private function __construct() {
    }

    /**
     * Always create a brand new counter.
     * @param string $name The counter name.
     * @param int $value The initial value.
     * @return Counter The counter.
     */
    private static function create($name, $value=0) {
        if (isset(self::$counters[$name])) {
            unset(self::$counters[$name]);
        }

        $counter = new Counter($name, $value);
        self::$counters[$name] = $counter;

        return $counter;
    }

    /**
     * Retrieve an existing counter if possible, otherwise create one.
     * @param string $name The counter name.
     * @return Counter The counter.
     */
    public static function retrieve($name) {
        if (isset(self::$counters[$name])) {
            $counter = self::$counters[$name];
        }
        else {
            $counter = self::create($name);
        }

        return $counter;
    }

    /**
     * Increment an existing counter if possible, otherwise create one first.
     * @param string $name The counter name.
     * @param int $n The amount to add.
     */
    public static function increment($name, $n=1) {
        $counter = self::retrieve($name);
        $counter->increment($n);
    }

    /**
     * Retrieve the counter output.
     * @return array Each element is a counter __toString() value.
     */
    public static function get_output() {
        $output = array();

        // Sort based on name.
        $names = array_keys(self::$counters);
        sort($names, SORT_STRING);

        foreach ($names as $name) {
            $output[] = self::$counters[$name]->__toString();
        }

        return $output;
    }
}
?>
