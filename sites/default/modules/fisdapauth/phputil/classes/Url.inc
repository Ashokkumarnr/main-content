<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/

class Url {
    private $host = '';
    private $fragment = '';
    private $path = '';
    private $params = array();

	/**
	 * @param string $url the URL
	 */
	public function __construct($url='') {
		$components = parse_url($url);
		if ($components === false) {
			return;
        } 

        // Set some values
        if (isset($components['host'])) {
            $this->set_host($components['host']);
        }

        if (isset($components['path'])) {
            $this->set_path($components['path']);
        }

        if (isset($components['fragment'])) {
            $this->set_fragment($components['fragment']);
        }

        // Set the parameters
        if (isset($components['query'])) {
            $q_arr = array();
            parse_str($components['query'], $q_arr);
            $this->params = $q_arr;
        }
	}

	/**
	 * Get the host name
	 *
	 * @return string the host name
	 */
	public function get_host() {
		return $this->host;
	}

	/**
	 * Get the path
	 *
	 * @return string the path
	 */
	public function get_path() {

		$path = $this->path;

		// If we're working from the root, make sure it's the right root
		if ($this->get_host() == null && substr($path, 0, 1) == '/' && strpos($path, FISDAP_WEB_ROOT) !== 0) {
			$path = FISDAP_WEB_ROOT . $path;
		}

		return self::normalize_path($path);
	}

	/**
	 * Get the fragment
	 *
	 * @return string the fragment
	 */
	public function get_fragment() {
		return $this->fragment;
	}

	/**
	 * Get a parameter
	 *
	 * @param mixed $key the param name
	 * @return mixed the param value
	 */
	public function get_param($key) {
		return $this->params[$key];
	}

	/**
	 * Get an associative array of the parameters
	 *
	 * @return array
	 */
	public function get_params() {
		return $this->params;
	}

	/**
	 * Get this URL as a string
	 *
	 * @return string the URL
	 */
	public function get_url_string() {
		$url = '';
		if ($this->get_host()) {
			$url .= 'http://' . $this->get_host();
		}

		$url .= $this->get_path();

		if (count($this->get_params()) > 0) {
			$url .= '?' . http_build_query($this->get_params());
		}

		if ($this->get_fragment()) {
			$url .= '#' . $this->get_fragment();
		}

		return $url;
	}

	/**
	 * Get the path for this file locally (i.e. on the hard drive, not requested 
	 * through the web server).
	 * @return string
	 */
	public function get_local_path() {
		if ($this->get_host() != null && preg_match('/fisdap(office)?\.(int|net)/i', $this->get_host()) != 1) {
			//STUB throw some kinda exception, we can't get a local path on a 
			//remote link
			return;
		}

		$path = $this->path;
		if (strpos($path, FISDAP_WEB_ROOT) === 0) {
			$path = substr($path, strlen(FISDAP_WEB_ROOT));
		}

		$path = FISDAP_ROOT . '/' . $path;
		$path = self::normalize_path($path);
		return $path;
	}

	/**
	 * Set the host name.
	 *
	 * @param string $host the host name, for example, 'dev5.fisdap.int'
	 */
	public function set_host($host) {
		$this->host = $host;
	}

	/**
	 * Set the path.
	 *
	 * @param string $path the path, for example, '/shift/evals/foo.htm'
	 */
	public function set_path($path) {
		$this->path = $path;
	}

	/**
	 * Set the fragment.
	 *
	 * The fragment is the part after the '#' - it can link to an element within 
	 * the page.
	 *
	 * @param string $fragment the fragment, for example, 'section3'
	 * @todo handle # on param
	 */
	public function set_fragment($fragment) {
		$this->fragment = $fragment;
	}

	/**
	 * Set a GET param
	 *
	 * @param string $key the param name, for example, 'shift_id'
	 * @param mixed value the value of the param, for example, 1998076
	 */
	public function set_param($key, $value) {
		$this->params[$key] = $value;
	}

	/**
	 * Normalize the given path
	 *
	 * Strip extraneous slashes. Resolve '.' and '..'
	 *
	 * @param string $path
	 * @return string the normalized path
	 */
	public static function normalize_path($path) {
		$path = preg_replace('_^\./_', '', $path);
		while (preg_match('_//|/\./|/\.$|/\.\./|/\.\.$_', $path) != 0) {
			$path = preg_replace('_/\./|/\.$_', '/', $path);
			$path = preg_replace('_(/?)\w+(/\.\./|/\.\.$)_', '\1', $path);
			$path = preg_replace('_/+_', '/', $path);
		}
		return $path;
	}
}
