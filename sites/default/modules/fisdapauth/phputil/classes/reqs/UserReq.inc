<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/common_user.inc');
require_once('phputil/classes/FisdapLogger.inc');
require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/classes/model/AccountTypeModel.inc');
require_once('phputil/classes/model/reqs/UserReqModel.inc');
require_once('phputil/classes/model/reqs/UserReqHistoryModel.inc');
require_once('phputil/classes/reqs/ReqAllStats.inc');
require_once('phputil/classes/reqs/ReqUtils.inc');
require_once('phputil/classes/reqs/UserReqModels.inc');
require_once('phputil/handy_utils.inc');

/**
 * The business logic for a user requirement.
 * No security is performed here.  If you call the method you get to use it.
 * <pre>
 * Since shifts come and go, and to prevent pending operations in the DB,
 * the philosophy is to never have a shift entry flagged as completed; 
 * otherwise, when the shift goes away, or has been run, we would need to clear 
 * out the ID.
 *
 * Notes (assume a single user and a single application):
 * - There are 0 or more completed user req(s) that are not expired.  They
 *   have no shift ID.  This case may occur when a user completes a course once, 
 *   then the req is close to being expired, so they complete it again.
 * - There are 0 or more completed user req(s) that are expired.  They
 *   have no shift ID.  These are kept for historical reasons.
 * - There are 0 or 1 incomplete user req(s) with no shift ID.
 * - There are 0 or more incomplete user req(s) with a shift ID.
 * - After the application is applied, if the underlying requirement is NOT 
 *   shift related, there is either completed unexpired user req(s) or 1
 *   incompleted user req.
 * - After signing up for a shift, the user has exactly 1 user req for the 
 *   shift.
 * - Signing up for multiple shifts that all require the same application will
 *   cause the user to have 1 user req for each shift.  This is done so
 *   bookkeeping is easier as shifts come and go.
 * - After a shift has started, the user req for the shift will go away.
 * </pre>
 */
final class UserReq {
    private $logger;
    private $requirement;
    private $user;

    const TYPE_NAME = 'UserRequirement';

	/**
	 * Constructor.
	 * @param UserReqModel $requirement The underlying requirement.
	 * @param common_user $user The user making the changes.
	 */
	public function __construct(UserReqModel $requirement, common_user $user) {
        $this->requirement = $requirement;
        $this->user = $user;
        $this->logger = FisdapLogger::get_logger();
    }

    private function get_log_prefix() {
		return 'UserReq[' . $this->requirement->get_id() . 
			']: user ID[' . $this->requirement->get_user_id() . ']';
    }

	/**
	 * Create the requirement in the DB.
	 * @param string | null $history_text The text to use for the history entries.
     * @return ReqAllStats Statistics.
	 * @throws FisdapDatabaseException if a problem occurs.
	 */
	public function create($history_text=null) {
        ReqUtils::verify_does_not_exist($this->requirement, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Created');

        $this->requirement->save();

        $stats = new ReqAllStats();
        $stats->get_user_req_stats()->increment_created_count(1);
        $stats->merge($this->add_history($history_text));

        $this->logger->debug($this->get_log_prefix() . ': created');
        return $stats;
    }
    
	/**
	 * Delete the requirement from the DB.
     * If the requirement is incomplete, it is deleted; otherwise is is 
     * marked as deleted in the DB.
	 * @param string | null $history_text The text to use for the history entries.
     * @return ReqAllStats Statistics.
	 * @throws FisdapDatabaseException if a problem occurs.
	 */
	public function delete($history_text=null) {
        ReqUtils::verify_exists($this->requirement, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Deleted');

        $this->logger->debug($this->get_log_prefix() . ": delete started");

        $stats = new ReqAllStats();

        if ($this->requirement->is_completed()) {
            $this->requirement->set_deleted(true);
            $this->requirement->set_shift_id(-1);
            $this->requirement->save();

            $stats->get_user_req_stats()->increment_marked_as_deleted_count(1);
			$stats->merge($this->add_history($history_text));
        }
        else {
            // Remove the history entries.
            $criteria = new ModelCriteria(new UserReqHistoryModel());
            $criteria->set_user_req_id($this->requirement->get_id());

            $connection = FISDAPDatabaseConnection::get_instance();
            $n = $connection->delete_by_criteria($criteria);
            $stats->get_user_req_stats()->increment_history_deleted_count(1);

            // Remove the requirement.
            $this->requirement->remove();
            $stats->get_user_req_stats()->increment_deleted_count(1);

        }

        $this->logger->debug($this->get_log_prefix() . ": delete complete");
        return $stats;
    }

	/**
	 * Update the requirement in the DB.
	 * @param string | null $history_text The text to use for the history entries.
     * @return ReqAllStats Statistics.
	 * @throws FisdapDatabaseException if a problem occurs.
	*/
    public function update($history_text=null) {
        $this->logger->debug($this->get_log_prefix() . ": update started");

        ReqUtils::verify_exists($this->requirement, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Modified');

        $this->requirement->save();

        $stats = new ReqAllStats();
        $stats->get_user_req_stats()->increment_modified_count(1);
		$stats->merge($this->add_history($history_text));

        $this->logger->debug($this->get_log_prefix() . ": update complete");
        return $stats;
    }

    private function add_history($description) {
		return self::add_history_entries($this->user, 
			array($this->requirement->get_id()), $description);
    }

	/**
	 * Remove user reqs that are incomplete and complete.
	 * Remember the complete reqs are flag as deleted.
	 * @param common_user $user The user.
	 * @param int $application_id The application ID to use.
	 * @param string $history_text The text to use for the history entries.
	 * @retun ReqAllStats The statistics.
	 */
	public static function delete_by_application_id($application_id, common_user $user, 
		$history_text) {

		Assert::is_int($application_id);
		Assert::is_string($history_text);

		$stats = self::delete_incomplete_user_reqs_by_application_id($application_id);
		$stats->merge(
			self::delete_complete_user_reqs_by_application_id(
				$application_id, $user, $history_text));

		return $stats;
	}

	/**
	 * Remove user reqs that are complete (they are flagged).
	 * @param int $application_id The application ID to use.
	 * @param common_user $user The user.
	 * @param string $history_text The text to use for the history entries.
	 * @return ReqAllStats The statistics.
	 */
	private static function delete_complete_user_reqs_by_application_id(
		$application_id, common_user $user, $history_text) {

		$logger = FisdapLogger::get_logger();
        $logger->debug(
            "Delete application[$application_id] complete user reqs started");

        // Find the user requirements that reference the application.
        $not_criteria = new ModelCriteria(new UserReqModel());
        $not_criteria->set_completed_date(FisdapDateTime::not_set());

        $criteria = new ModelCriteria(new UserReqModel());
        $criteria->add_field_name('id');
		$criteria->add_not_criteria($not_criteria);
        $criteria->set_application_id($application_id);
        $criteria->set_deleted(false);

        // We need to hang on to the IDs being updated so we can add history 
        // entries.
        $connection = FISDAPDatabaseConnection::get_instance();
        $ids = HandyArrayUtils::flatten_values(
            $connection->get_array_by_criteria($criteria)); 

        $update_criteria = new ModelCriteria(new UserReqModel());
        $update_criteria->set_deleted(true);
        $n = $connection->update_by_criteria($criteria, $update_criteria);

		$stats = new ReqAllStats();
        $stats->get_user_req_stats()->increment_marked_as_deleted_count($n);

        // Add the history entries.
        $stats->merge(self::add_history_entries($user, $ids, $history_text));
        $logger->debug(
            "Delete application[$application_id] complete user reqs ($n) complete");

		return $stats;
	}

	/**
	 * Remove user reqs that are incomplete.
	 * @param int $application_id The application ID to use.
	 * @return ReqAllStats The statistics.
	 */
    private static function delete_incomplete_user_reqs_by_application_id($application_id) {
		$logger = FisdapLogger::get_logger();
        $logger->debug(
            "Delete application[$application_id] incomplete user reqs started");

        // Remove the history entries.
        $criteria = new ModelCriteria(new UserReqModel());
        $criteria->add_field_name('id');
        $criteria->set_application_id($application_id);
        $criteria->set_completed_date(FisdapDateTime::not_set());
        $criteria->set_deleted(false);
        
        $history_criteria = new ModelCriteria(new UserReqHistoryModel());
        $history_criteria->set_in_criteria(UserReqHistoryModel::USER_REQ_ID_FIELD, $criteria);

        $connection = FISDAPDatabaseConnection::get_instance();
        $n = $connection->delete_by_criteria($history_criteria);

		$stats = new ReqAllStats();
        $stats->get_user_req_stats()->increment_history_deleted_count($n);

        // Remove the requirements.
        $n = $connection->delete_by_criteria($criteria);
        $stats->get_user_req_stats()->increment_deleted_count($n);

        $logger->debug(
			"Delete application[$application_id] incomplete user reqs ($n) complete");

		return $stats;
    }

	/**
	 * Permanently delete requirements from the DB.
     * The requirement and its history entries will no longer be in the DB.
	 * @param array $ids The user req IDs to delete.
     * @return ReqAllStats Statistics.
	 * @throws FisdapDatabaseException if a problem occurs.
	 */
	public static function delete_user_reqs($ids) {
		Assert::is_array($ids);

        $stats = new ReqAllStats();
		if (!count($ids)) return $stats;

		$ids_string = join(',', $ids);

		$logger = FisdapLogger::get_logger();
		$logger->debug("Delete user req IDs($ids_string) started");

        // Remove the history entries.
        $criteria = new ModelCriteria(new UserReqHistoryModel());
        $criteria->set_in(UserReqHistoryModel::USER_REQ_ID_FIELD, $ids);

        $connection = FISDAPDatabaseConnection::get_instance();
        $n = $connection->delete_by_criteria($criteria);
        $stats->get_user_req_stats()->increment_history_deleted_count($n);

        // Remove the requirements.
        $criteria = new ModelCriteria(new UserReqModel());
        $criteria->set_in('id', $ids);
        $n = $connection->delete_by_criteria($criteria);
        $stats->get_user_req_stats()->increment_deleted_count($n);

        $logger->debug("Delete user req IDs($ids_string) complete");
        return $stats;
    }

	/**
	 * Add history entries.
	 * @param common_user $user The user.
	 * @param array $ids The user req IDs.
	 * @param string $description The history description.
	 * @return ReqAllStats The statistics.
	 */
    private static function add_history_entries(common_user $user, $ids, $description) {
        $description = trim($description);
		$stats = new ReqAllStats();

        foreach ($ids as $id) {
            $history = new UserReqHistoryModel();
            $history->set_description($description);
            $history->set_entry_time(FisdapDateTime::now());
            $history->set_user_id($user->get_UserAuthData_idx());
            $history->set_user_req_id($id);
            $history->save();

			$stats->get_user_req_stats()->increment_history_created_count(1);
        }

		return $stats;
    }

	/**
	 * Retrieve all the user reqs.
	 * @param int $user_id The user ID to retrieve for.
	 * @return UserReqModel The user reqs.
	 */
	public static function get_user_reqs_by_user_id($user_id) {
		Assert::is_int($user_id);

		$criteria = new ModelCriteria(new UserReqModel());
		$criteria->set_user_id($user_id);

		$connection = FISDAPDatabaseConnection::get_instance();
		return new UserReqModels($user_id, $connection->get_by_criteria($criteria));
	}
}
?>
