<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/reqs/ReqStats.inc');

/**
 * A container for all the statistics surrounding requirement operations.
 */
final class ReqAllStats {
    private $application;
    private $relationship;
    private $req;
    private $user_req;

    /**
     * Constructor.
     */
    public function __construct() {
        $this->application = new ReqStats('application');
        $this->relationship = new ReqStats('relationship');
        $this->req = new ReqStats('requirement');
        $this->user_req = new ReqStats('user requirement');
    }

    /**
     * Retrieve the application stats.
     * @return ReqStats The application stats.
     */
    public function get_application_stats() {
        return $this->application;
    }

    /**
     * Retrieve the relationship stats.
     * @return ReqStats The relationship stats.
     */
    public function get_relationship_stats() {
        return $this->relationship;
    }

    /**
     * Retrieve the requirement stats.
     * @return ReqStats The requirement stats.
     */
    public function get_req_stats() {
        return $this->req;
    }

    /**
     * Retrieve the user requirement stats.
     * @return ReqStats The user requirement stats.
     */
    public function get_user_req_stats() {
        return $this->user_req;
    }

    /**
     * Merge these stats with another.
     * @param ReqAllStats $stats The stats to merge.
     */
    public function merge(ReqAllStats $stats) {
        $this->application->merge($stats->application);
        $this->relationship->merge($stats->relationship);
        $this->req->merge($stats->req);
        $this->user_req->merge($stats->user_req);
    }

    /**
     * Retrieve messages describing the counts.
     * @param boolean $include_zero_counts TRUE if zero counts should be 
     * included.
     */
    public function get_messages($include_zero_counts) {
        $msgs = array();
        $msgs = array_merge($msgs, 
            $this->get_application_stats()->get_messages($include_zero_counts));
        $msgs = array_merge($msgs, 
            $this->get_relationship_stats()->get_messages($include_zero_counts));
        $msgs = array_merge($msgs, 
            $this->get_req_stats()->get_messages($include_zero_counts));
        $msgs = array_merge($msgs, 
            $this->get_user_req_stats()->get_messages($include_zero_counts));

        return $msgs;
    }

    public function __toString() {
        $data = array(
            $this->get_application_stats()->__toString(),
            $this->get_relationship_stats()->__toString(),
            $this->get_req_stats()->__toString(),
            $this->get_user_req_stats()->__toString()
        );

        return 'ReqAllStats[' . join(',', $data) . ']';
    }
}
?>
