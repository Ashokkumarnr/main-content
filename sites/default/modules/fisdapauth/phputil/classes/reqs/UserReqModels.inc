<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FisdapLogger.inc');
require_once('phputil/classes/model/reqs/UserReqModel.inc');

/**
 * A container for user req models.
 */
class UserReqModels {
	private $completed_reqs = array();
	private $deleted_reqs = array();
	private $expired_reqs = array();
	private $logger;
	private $shift_related_reqs = array();
	private $shift_unrelated_reqs = array();
	private $user_id;

	/**
	 * Constructor.
	 * @param int $user_id The user ID these reqs are for.
	 * @param array $reqs The UserReqModel's.
	 */
	public function __construct($user_id, $user_reqs) {
		Assert::is_int($user_id);
		Assert::is_array($user_reqs);

		$this->user_id = $user_id;
		$this->logger = FisdapLogger::get_logger();

		$n = count($user_reqs);
		for ($i = 0; $i < $n; ++$i) {
			$r = $user_reqs[$i];
			Assert::is_a($r, 'UserReqModel');

			if ($r->is_completed()) {
			   	if ($r->is_expired()) {
					$this->expired_reqs[] = $r;
				}
				else {
					$this->completed_reqs[] = $r;
				}
			}
			elseif ($r->is_deleted()) {
				// This case should never occur, though I don't want to assert 
				// it.
				$this->logger->warn(
					"User ID[$user_id]: User req[" . $r->get_id() . '] was found deleted');
				$this->deleted_reqs[] = $r;
			}
			elseif ($r->get_shift_id() < 0) {
				$this->shift_unrelated_reqs[] = $r;
			}
			else {
				$this->shift_related_reqs[] = $r;
			}
		}
	}

	/**
	 * Retrieve the completed (and NOT expired) reqs.
	 * @return array The completed reqs.
	 */
	public function get_completed_reqs() {
		return $this->completed_reqs;
	}

	/**
	 * Retrieve the deleted (incomplete) reqs.
	 * @return array The deleted reqs.
	 */
	public function get_deleted_reqs() {
		return $this->deleted_reqs;
	}

	/**
	 * Retrieve the completed (and expired) reqs.
	 * @return array The completed reqs.
	 */
	public function get_expired_reqs() {
		return $this->expired_reqs;
	}

	/**
	 * Retrieve the shift related reqs.
	 * @return array The shift related reqs.
	 */
	public function get_shift_related_reqs() {
		return $this->shift_related_reqs;
	}

	/**
	 * Set the shift related reqs.
	 * @param array $list The shift related reqs.
	 */
	public function set_shift_related_reqs($list) {
		Assert::is_array($list);
		$this->shift_related_reqs = $list;
	}

	/**
	 * Retrieve the shift unrelated reqs.
	 * @return array The shift unrelated reqs.
	 */
	public function get_shift_unrelated_reqs() {
		return $this->shift_unrelated_reqs;
	}

	/**
	 * Set the shift unrelated reqs.
	 * @param array $list The shift unrelated reqs.
	 */
	public function set_shift_unrelated_reqs($list) {
		Assert::is_array($list);
		$this->shift_unrelated_reqs = $list;
	}

	public function __toString() {
		return 'UserReqModels: ' . $this->get_state_msg();
	}

	/**
	 * Retrieve a message suitable for displaying the state.
	 * @return string The message.
	 */
	public function get_state_msg() {
		$msgs = array();

		$n = count($this->completed_reqs);
		if ($n) {
			$msgs[] = "completed($n)";
		}

		$n = count($this->expired_reqs);
		if ($n) {
			$msgs[] = "expired($n)";
		}

		$n = count($this->deleted_reqs);
		if ($n) {
			$msgs[] = "deleted($n)";
		}

		$n = count($this->shift_related_reqs);
		if ($n) {
			$msgs[] = "shiftRelated($n)";
		}

		$n = count($this->shift_unrelated_reqs);
		if ($n) {
			$msgs[] = "shiftUnrelated($n)";
		}

		if (!count($msgs)) {
			$msgs[] = 'none';
		}

		return 'user ID[' . $this->get_user_id() . ']: ' . join(' ', $msgs);
	}

	/**
	 * Retrieve the user ID these reqs are for.
	 * @return int The user ID.
	 */
	public function get_user_id() {
		return $this->user_id;
	}

	/**
	 * Retrieve the shift entries that have expired, i.e. beyond the due date.
	 * These entries can be deleted at some point.  This does not modify the list.
	 * @return array The removed entries.
	 */
	public function get_expired_shift_related_reqs() {
		$n = count($this->shift_related_reqs);
		$list = array();
		$now = FisdapDateTime::now();

		for ($i = 0; $i < $n; ++$i) {
			$user_req = $this->shift_related_reqs[$i];

			if ($user_req->get_due_date()->compare($now) < 0) {
				$list[] = $user_req;
			}
		}

		return $list;
	}

	/**
	 * Cleanup the entries, usually for display purposes.
	 * <ul>
	 * <li>Remove any duplicate entries.
	 * <li>Remove any shift entries that are no longer needed (due date passed).
	 * </ul>
	 * @return UserReqModels The removed entries.
	 */
	public function cleanup() {
		// For completed reqs, keep only the most recent one.
		$removed = new UserReqModels($this->user_id, array());
		$now = FisdapDateTime::now();

		$n = count($this->completed_reqs);
		if ($n > 1) {
			$keep = $this->completed_reqs[0];
			for ($i = 1; $i < $n; ++$i) {
				$user_req = $this->completed_reqs[$i];

				$status = $keep->get_completed_date()->compare($user_req->get_completed_date());
				if ($status < 0) {
					$removed->completed_reqs[] = $keep;
					$keep = $user_req;
				}
				else {
					$removed->completed_reqs[] = $user_req;
				}
			}

			$this->completed_reqs = array($keep);
		}

		// For expired reqs, keep only the most recently expired one.
		$n = count($this->expired_reqs);
		if ($n > 1) {
			$keep = $this->expired_reqs[0];
			for ($i = 1; $i < $n; ++$i) {
				$user_req = $this->expired_reqs[$i];

				$status = $keep->get_expiration_date()->compare($user_req->get_expiration_date());
				if ($status < 0) {
					$removed->expired_reqs[] = $keep;
					$keep = $user_req;
				}
				else {
					$removed->expired_reqs[] = $user_req;
				}
			}

			$this->expired_reqs = array($keep);
		}

		// We keep the deleted reqs.

		// We only keep the most recently created shift unrelated entry.
		// There should normally only be 1.  If there is more than 1, we
		// will log a warning, and keep the first one due.  Note that we should
		// look at the history entry to determine which one was created last,
		// but since the system is in an inconsistent state, we won't rely on
		// there being a history entry.
		$n = count($this->shift_unrelated_reqs);
		if ($n > 1) {
			$keep = $this->shift_unrelated_reqs[0];

			$this->logger->warn(
				"User ID[$this->user_id]: found $n shift unrelated reqs" .
				', expected 0 or 1');
			for ($i = 1; $i < $n; ++$i) {
				$user_req = $this->shift_unrelated_reqs[$i];

				$keep_set = $keep->get_due_date()->is_set();
				$ur_set = $user_req->get_due_date()->is_set();

				if ($keep_set) {
					if (!$ur_set) {
						$status = 1;
					}
				}
				else if ($ur_set) {
					$status = -1;
				}
				else {
					// It doesn't matter which one we keep.
					$status = 1;
				}

				$status = $keep->get_due_date()->compare($user_req->get_due_date());
				if ($status < 0) {
					$removed->shift_unrelated_reqs[] = $keep;
					$keep = $user_req;
				}
				else {
					$removed->shift_unrelated_reqs[] = $user_req;
				}
			}

			$this->shift_unrelated_reqs = array($keep);
		}

		// For the shift related reqs, we keep one per shift.  
		// There is normally only 1.  If there is more than 1, we
		// will log an error if the due dates don't match and keep the first one 
		// due.
		// Also, if the due date has passed, we remove it.
		$n = count($this->shift_related_reqs);
		if ($n > 1) {
			$duplicate_shift_ids = array();
			$keep_by_shift_id = array();

			for ($i = 0; $i < $n; ++$i) {
				$user_req = $this->shift_related_reqs[$i];
				$shift_id = $user_req->get_shift_id();

				if ($user_req->get_due_date()->compare($now) < 0) {
					$removed->shift_related_reqs[] = $user_req;
				}
				elseif (isset($keep_by_shift_id[$shift_id])) {
					$keep = $keep_by_shift_id[$shift_id];
					$status = $keep->get_due_date()->compare($user_req->get_due_date());

					if ($status > 0) {
						$duplicate = $keep;
						$keep_by_shift_id[$shift_id] = $user_req;
					}
					else {
						$duplicate = $user_req;
					}

					$removed->shift_related_reqs[] = $duplicate;
					$duplicate_shift_ids[] = $shift_id;
				}
				else {
					$keep_by_shift_id[$shift_id] = $user_req;
				}
			}

			// Update what we kept.
			$this->shift_related_reqs = array();
			foreach ($keep_by_shift_id as $shift_id=>$user_req) {
				$this->shift_related_reqs[] = $user_req;
			}

			// Log the duplicates.
			if (count($duplicate_shift_ids)) {
				$duplicate_shift_ids = array_unique($duplicate_shift_ids);
				sort($duplicate_shift_ids, SORT_NUMERIC);
					
				$this->logger->warn(
					"User ID[$this->user_id]: found duplicate shift related reqs for" .
					' shift IDs: ' . join(',', $duplicate_shift_ids));
			}
		}

		return $removed;
	}

	/**
	 * Retrieve the user reqs as a single list.
	 * Note that this is a LIVE list, be carefull with it.
	 * @return array The UserReqModel objects.
	 */
	public function get_all_reqs() {
		return array_merge(
			$this->completed_reqs,
			$this->deleted_reqs,
			$this->expired_reqs,
			$this->shift_related_reqs,
			$this->shift_unrelated_reqs
		);
	}
}
?>
