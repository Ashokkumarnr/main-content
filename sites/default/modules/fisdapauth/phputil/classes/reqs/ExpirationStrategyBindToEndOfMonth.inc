<?php
require_once('phputil/classes/common_date.inc');
require_once('phputil/classes/reqs/ExpirationStrategy.inc');

/**
 * An expiration date strategy that binds to the end of the month.
 */
final class ExpirationStrategyBindToEndOfMonth extends ExpirationStrategy {
    public function get_expiration_date($timestamp) {
        $date = parent::get_expiration_date($timestamp);
		if (!$date->is_set()) return $date;

        // Bind it.
		return new FisdapDateTime(
			FisdapDate::create_from_ymd(
				$date->get_year(),
				$date->get_month(),
				$date->get_days_in_month()),
			$date->get_time());
    }
}
?>
