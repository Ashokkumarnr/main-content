<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/common_user.inc');
require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/classes/FisdapLogger.inc');
require_once('phputil/classes/reqs/ReqAllStats.inc');
require_once('phputil/classes/reqs/ReqApplication.inc');
require_once('phputil/classes/reqs/UserReq.inc');
require_once('phputil/classes/model/AbstractModel.inc');
require_once('phputil/classes/model/ModelCriteria.inc');
require_once('phputil/classes/model/reqs/ReqRequiredModel.inc');
require_once('phputil/classes/model/reqs/UserReqModel.inc');
require_once('phputil/classes/model/reqs/UserReqHistoryModel.inc');
require_once('phputil/classes/model/ShiftModel.inc');
require_once('phputil/classes/model/SiteProxyTypeModel.inc');
require_once('phputil/exceptions/FisdapDatabaseException.inc');
require_once('phputil/handy_utils.inc');

/**
 * Utilties common to requirements.
 */
final class ReqUtils {
    /**
     * Verify that an object exists.
     * @param AbstractModel $model The model to test.
     * @param string $type The type, i.e. 'requirement, proxy'.
     * @throws FisdapDatabaseException if the verification fails.
     */
    public static function verify_exists(AbstractModel $model, $type) {
        Assert::is_not_empty_trimmed_string($type);

        if ($model->exists()) return;

        throw new FisdapDatabaseException("$type must exist");
    }

    /**
     * Verify that an object does not exist.
     * @param AbstractModel $model The model to test.
     * @param string $type The type, i.e. 'requirement, proxy'.
     * @throws FisdapDatabaseException if the verification fails.
     */
    public static function verify_does_not_exist(AbstractModel $model, $type) {
        Assert::is_not_empty_trimmed_string($type);

        if (!$model->exists()) return;

        throw new FisdapDatabaseException("$type must not exist");
    }

    /**
     * Retrieve a description.
     * @param string | null $description The description to retrieve from.
     * @param string $default_description The default in case $description is 
     * missing.
     */
    public static function get_description($description, $default_description) {
        Assert::is_true(
            Test::is_string($description) || Test::is_null($description));
        Assert::is_string($default_description);

        if (!Test::is_not_empty_trimmed_string($description)) {
            $description = $default_description;
        }

        return trim($description);
    }

    /**
     * Retrieve the SQL for matching a program to a proxy group.
     * @param string The column to test.
     * @param int | string $ambulance_service_id The ID of the service.
     * @return string The SQL clause.
     */
    public static function get_proxy_group_match_clause($column, $ambulance_service_id) {
        $proxy_type = SiteProxyTypeModel::ID_REQUIREMENT;

        $clause = <<<EOI
($column IN
  (
    SELECT PGAS.OwnerProgram_id FROM AmbulanceServices AS PGAS
    WHERE (PGAS.OwnerProgram_id >= 0) AND (PGAS.AmbServ_id=$ambulance_service_id)

    UNION

    SELECT PGSP.Program_id FROM SiteProxies AS PGSP
    WHERE (PGSP.AmbServ_id=$ambulance_service_id) AND (PGSP.ProxyType=$proxy_type)
  )
)
EOI;

        return $clause;
    }

	/**
	 * A shift has changed.
     * @param Shift $shift The shift that is changing.
	 * @param common_user $admin The user administering the change.
     * @return ReqAllStats Statistics.
	 */
	public static function update_shift(Shift $shift, common_user $admin) {
		$shift_id = $shift->get_id();

		$logger = FisdapLogger::get_logger();
        $logger->debug("Shift ID[$shift_id]: update started");

		// Find the user reqs pertaining to the shift, and update them.
		// If we didn't need to create history entries we could do this in a 
		// single UPDATE statement.
		$criteria = new ModelCriteria(new UserReqModel());
		$criteria->set_deleted(false);
		$criteria->set_shift_id($shift_id);
        $connection = FISDAPDatabaseConnection::get_instance();
		$user_reqs = $connection->get_by_criteria($criteria);

		// Modify them.
        $stats = new ReqAllStats();

		foreach ($user_reqs as $user_req) {
            $user_req->set_due_date(
                new FisdapDateTime($shift->get_start_date(), $shift->get_start_time()));

            $ur = new UserReq($user_req, $admin);
			$stats->merge($ur->update('Shift time changed'));
        }

		$n = count($user_reqs);
        $logger->debug("Shift ID[$shift_id]: update of [$n] user reqs complete");
        return $stats;
	}

    /**
     * The user has added a shift.
	 * @param common_user $user The user.
     * @param Shift $shift The shift the user wants to add.
	 * @param common_user $admin The user administering the change.
     * @return ReqAllStats Statistics.
     */
    public static function add_shift(common_user $user, Shift $shift, common_user $admin) {
		$shift_id = $shift->get_id();
		$user_id = $user->get_UserAuthData_idx();

		$logger = FisdapLogger::get_logger();
        $logger->debug("User ID[$user_id]: add shift ID[$shift_id] started");

        // Find the applications.
        $ids = array(
            ReqRequiredModel::ID_REQUIRED_BEFORE_SHIFT
        );

        $applications = ReqApplication::find_applications_effecting_user($user, $ids, $shift);

        // Add the new user req if one does not already exist.
        $connection = FISDAPDatabaseConnection::get_instance();
        $stats = new ReqAllStats();

        foreach ($applications as $application) {
			// If a user req exists it won't be added.
			$criteria = new ModelCriteria(new UserReqModel());
            $criteria->set_application_id($application->get_id());
			$criteria->set_deleted(false);
			$criteria->set_shift_id($shift_id);
			$criteria->set_user_id($user_id);
			$user_req_ids = HandyArrayUtils::flatten_values(
				$connection->get_array_by_criteria($criteria));
			if (count($user_req_ids)) continue;

			// Add it.
            $req = new UserReqModel();
            $req->set_application_id($application->get_id());
            $req->set_completed_date(FisdapDateTime::not_set());
			$req->set_deleted(false);
            $req->set_due_date(
                new FisdapDateTime($shift->get_start_date(), $shift->get_start_time()));
            $req->set_expiration_date(FisdapDate::not_set());
            $req->set_req_id($application->get_req_id());
            $req->set_shift_id($shift_id);
            $req->set_user_id($user->get_UserAuthData_idx());

            $user_req = new UserReq($req, $admin);
			$stats->merge($user_req->create());
        }

        $logger->debug("User ID[$user_id]: add shift ID[$shift_id] complete");
        return $stats;
    }

    /**
     * The user has been removed from a shift.
	 * @param common_user $user The user.
     * @param int $shift_id The ID of the shift the user is being removed from.
     * @return ReqAllStats Statistics.
     */
    public static function delete_shift(common_user $user, $shift_id) {
        Assert::is_int($shift_id);

		$user_id = $user->get_UserAuthData_idx();
		$logger = FisdapLogger::get_logger();
        $logger->debug("User ID[$user_id]: delete shift ID[$shift_id] started");

        // We remove all entries for this shift.
        $criteria = new ModelCriteria(new UserReqModel());
        $criteria->add_field_name('id');
        $criteria->set_shift_id($shift_id);
        $criteria->set_user_id($user->get_UserAuthData_idx());

        // Delete the history entries.
        $history_criteria = new ModelCriteria(new UserReqHistoryModel());
        $history_criteria->set_in_criteria('id', $criteria);

        $connection = FISDAPDatabaseConnection::get_instance();
        $n = $connection->delete_by_criteria($history_criteria);

        $stats = new ReqAllStats();
        $stats->get_user_req_stats()->increment_history_deleted_count($n);

        // Delete the shift entries.
        $n = $connection->delete_by_criteria($criteria);
        $stats->get_user_req_stats()->increment_deleted_count($n);

        $logger->debug("User ID[$user_id]: delete shift ID[$shift_id] complete");
        return $stats;
    }

    /**
     * Verify that the user can sign up for a shift.
	 * The applications returned will be filtered so that alternates have 
	 * been considered.
	 * @param common_user $user The user.
     * @param Shift $shift The shift the user wants to sign up for.
     * @return array The applications that must be completed (and are not) before shift signup.
     */
    public static function attempt_shift_signup(common_user $user, Shift $shift) {
        // Find the applications.
		$shift_id = $shift->get_id();
		$user_id = $user->get_UserAuthData_idx();

		$logger = FisdapLogger::get_logger();
        $logger->debug("User ID[$user_id]: attempt shift ID[$shift_id] signup started");

        $ids = array(
            ReqRequiredModel::ID_REQUIRED_BEFORE_SHIFT_SIGNUP
        );

        $applications = ReqApplication::find_applications_effecting_user($user, $ids, $shift);
		$application_ids = ReqUtils::get_attributes_from_list($applications, 'get_id');

		$completed_applications = ReqApplication::get_completed_applications(
			$user->get_UserAuthData_idx(), 
			array(ReqApplication::OPTION_APPLICATION_IDS => $application_ids)); 
		$completed_ids = ReqUtils::get_attributes_from_list($completed_applications, 'get_id');
		
		$incomplete_ids = array_diff($application_ids, $completed_ids);

		// Now filter the application list.
		$results = array();
		foreach ($applications as $application) {
			if (in_array($application->get_id(), $incomplete_ids)) {
				$results[] = $application;
			}
		}

		$logger->debug("User ID[$user_id]: attempt shift ID[$shift_id] signup," .
			' incomplete applications[' . count($incomplete_ids) . '] complete');
		return $results;
    }

    /**
     * Verify that the user can run a shift.
	 * The applications returned will be filtered so that alternates have 
	 * been considered.  They are the applications based on the shift ID.
	 * @param common_user $user The user.
     * @param Shift $shift The shift the user wants to run.
     * @return array The applications that must be completed before running the shift.
     */
    public static function attempt_shift_run(common_user $user, Shift $shift) {
		$user_id = $user->get_UserAuthData_idx();
        $shift_id = $shift->get_id();

		$logger = FisdapLogger::get_logger();
        $logger->debug("User ID[$user_id]: attempt shift ID[$shift_id] run started");

        // Find the applications required for the shift.
        $today = FisdapDate::today()->get_MySQL_date();
        $user_id = $user->get_UserAuthData_idx();

		$criteria = new ModelCriteria(new UserReqModel());
		$criteria->add_field_name(UserReqModel::APPLICATION_ID_FIELD);
		$criteria->set_deleted(false);
		$criteria->set_distinct(true);
		$criteria->set_shift_id($shift_id);
		$criteria->set_user_id($user->get_UserAuthData_idx());

        $connection = FISDAPDatabaseConnection::get_instance();
		$application_ids = HandyArrayUtils::flatten_values(
			$connection->get_array_by_criteria($criteria));

		$completed_applications = ReqApplication::get_completed_applications(
			$user->get_UserAuthData_idx(), 
			array(ReqApplication::OPTION_APPLICATION_IDS => $application_ids)); 
		$completed_ids = ReqUtils::get_attributes_from_list($completed_applications, 'get_id');

		$ids = array_diff($application_ids, $completed_ids);

		// Load the applications.
		$applications = array();
		foreach ($ids as $id) {
			$applications[] = new ReqApplicationModel($id);
		}

        $logger->debug("User ID[$user_id]: attempt shift ID[$shift_id] run," .
			' incomplete applications[' . count($applications) . '] complete');
		return $applications;
    }

	/**
	 * Retrieve a list of attributes from a list of items.
	 * @param array $list The items.
	 * @param string $method The method used to extract the attribute.
	 * @return array The attributes.
	 */
	public static function get_attributes_from_list($list, $method) {
		Assert::is_array($list);
		Assert::is_not_empty_trimmed_string($method);

		$attributes = array();
		foreach ($list as $item) {
			Assert::is_true(method_exists($item, $method));
			Assert::is_true(is_callable(array($item, $method)));
			$attributes[] = $item->$method();
		}

		return $attributes;
	}

	/**
	 * Remove any shift reqs whose due date has expired.
	 * @return ReqAllStats The statistics.
	 */
	public static function delete_expired_shift_reqs() {
		$logger = FisdapLogger::get_logger();
        $logger->debug("Delete expired shift reqs started");

		// We remove all entries whose shift already started as they 
		// are no longer required.
		$now = FisdapDateTime::now();
		$now->change_time(1, 0);		// allow some leeway

        $criteria = new ModelCriteria(new UserReqModel());
		$criteria->add_and_clause('DueDate < "' . $now->get_MySql_date_time() . '"');
		$criteria->add_field_name('id');

        // Delete the history entries.
        $history_criteria = new ModelCriteria(new UserReqHistoryModel());
        $history_criteria->set_in_criteria(UserReqHistoryModel::USER_REQ_ID_FIELD, $criteria);

        $connection = FISDAPDatabaseConnection::get_instance();
        $n = $connection->delete_by_criteria($history_criteria);

        $stats = new ReqAllStats();
        $stats->get_user_req_stats()->increment_history_deleted_count($n);

        // Delete the shift entries.
        $n = $connection->delete_by_criteria($criteria);
        $stats->get_user_req_stats()->increment_deleted_count($n);

        $logger->debug("Delete expired shift reqs [$n] complete");
        return $stats;
	}
}
?>
