<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/
require_once("phputil/classes/customizable_table.inc");
require_once("phputil/classes/Url.inc");

/**
 * A clickable list
 *
 * Creates a common_table where rows or cells may be designated clickable,
 * with callbacks controlling the response to clicks.
 *
 * @package CommonInclude
 * @author Ian Young
 */
abstract class clickable_table extends customizable_table implements AjaxComponent {

	/**
	 * Stores which rows or cells are clickable
	 *
	 * A 2D array indexed by row, then column, of boolean values.
	 * The values at column '' control clickability of the entire row.
	 */
	private $clickable;
	/**
	 * All rows clickable
	 *
	 * A global flag to control what is clickable. May be overridden by
	 * specific entries in {@link $clickable}.
	 * @var boolean
	 */
	private $all_rows_clickable;
	/**
	 * All cells clickable
	 *
	 * A global flag to control what is clickable. May be overridden by
	 * specific entries in {@link $clickable}.
	 * @var boolean
	 */
	private $all_cells_clickable;
	/**
	 * All heading cells clickable
	 *
	 * A global flag to control what is clickable. May be overridden by
	 * specific entries in {@link $clickable}.
	 * @var boolean
	 */
	private $all_headings_clickable;

	/**
	 * Make a row unclickable.
	 *
	 * @param string $rowidx the index of the row to make unclickable. A null
	 * index indicates the header row. 
	 */
	public function make_row_unclickable($rowidx) {
		$this->clickable[$rowidx][''] = false;
	}

	/**
	 * Make a single cell unclickable.
	 *
	 * @param string $rowidx the index of the row the cell is in
	 * @param string $colidx the index of the column the cell is in
	 */
	public function make_cell_unclickable($rowidx, $colidx) {
		$this->clickable[$rowidx][$colidx] = false;
	}

	/**
	 * Make a heading cell unclickable.
	 *
	 * @param string $colidx the index of the row the heading is in
	 */
	public function make_heading_unclickable($colidx) {
		$this->make_cell_unclickable("", $colidx);
	}

	/**
	 * Make all rows clickable.
	 *
	 * Note: does not make headings clickable. These must be enabled separately.
	 *
	 * See {@link make_all_cells_clickable()} for an example.
	 *
	 * @param string $dispatch_id The dispatch id to send to the dispatcher. If 
	 * null, clicks will cause a regular page GET rather than an AJAX event.
	 * @param string $destination The page to handle this click. Defaults to the 
	 * current page.
	 * @param array $columns_sent An array of column ids. The contents of these 
	 * columns for the clicked row will be sent as parameters. This array may 
	 * also include 'rowidx' to send the row id.
	 * @param array $otherdata an array of key/value pairs to be sent as 
	 * parameters.
	 */
	public function make_all_rows_clickable($dispatch_id='interactivetable_event', $destination=null, $columns_sent=array('rowidx'), $otherdata=array()) {
		$this->all_rows_clickable = true;

		$this->click_row_dispatch = $dispatch_id;
		$this->click_row_dest = $destination;
		$this->click_row_columns = $columns_sent;
		$this->click_row_otherdata = $otherdata;
	}

	/**
	 * Make all cells clickable.
	 *
	 * Note: does not make headings clickable. These must be enabled separately.
	 *
	 * Example:
	 * <code>
	 * 	// Set up our columns
	 * 	$my_table->set_column('foo', 'Foo', null, 'left', 'medium');
	 * 	$my_table->set_column('bar', 'Bar', null, 'left', 'medium');
	 * 	$my_table->set_column('baz', 'Baz', null, 'left', 'medium');
	 * 	$my_table->set_column('id', '', null, 'left', 'medium');
	 * 	// Don't show id column onscreen
	 * 	$my_table->set_column_invisible('id');
	 * 
	 * 	// ...
	 * 
	 * 	$mytable->make_all_cells_clickable(null, 'process_mytable.html', array('colidx', 'cell_contents', 'baz', 'id'), array('some_number' => 2590));
	 * 
	 * 	// ...
	 * 
	 * 	$mytable->add_row(null, array('foo' => 1, 'bar' => 'Some val', 'baz' => 44, 'id' => 101));
	 * 	$mytable->add_row(null, array('foo' => 0, 'bar' => 'Not much', 'baz' => 20, 'id' => 102));
	 * </code>
	 * In the above example, if a user clicks on the cell in the second row
	 * under the Foo column, we will send a GET request to mytable.html 
	 * with the following parameters:
	 *  - colidx: 'foo'
	 *  - cell_contents: 0
	 *  - baz: 20
	 *  - id: 102
	 *  - some_number: 2590
	 *
	 * @param string $dispatch_id The dispatch id to send to the dispatcher. If 
	 * null, clicks will cause a regular page GET rather than an AJAX event.
	 * @param string $destination The page to handle this click. Defaults to the 
	 * current page.
	 * @param array $columns_sent An array of column ids. The contents of these 
	 * columns for the clicked row will be sent as parameters. This array may 
	 * also include the following magic terms, which will send data specific to 
	 * the row or cell:
	 *  - 'rowidx' to send the row id.
	 *  - 'colidx' to send the column id.
	 *  - 'cell_contents' to send the contents of the cell.
	 * @param array $otherdata an array of key/value pairs to be sent as 
	 * parameters.
	 */
	public function make_all_cells_clickable($dispatch_id='interactivetable_event', $destination=null, $columns_sent=array('rowidx', 'colidx', 'cell_contents'), $otherdata=array()) {
		$this->all_cells_clickable = true;

		$this->click_cell_dispatch = $dispatch_id;
		$this->click_cell_dest = $destination;
		$this->click_cell_columns = $columns_sent;
		$this->click_cell_otherdata = $otherdata;
	}

	/**
	 * Make all heading cells clickable.
	 *
	 * See {@link make_all_cells_clickable()} for an example.
	 *
	 * @param string $dispatch_id The dispatch id to send to the dispatcher. If 
	 * null, clicks will cause a regular page GET rather than an AJAX event.
	 * @param string $destination The page to handle this click. Defaults to the 
	 * current page.
	 * @param array $otherdata an array of key/value pairs to be sent as 
	 * parameters. The column index will also be sent with the key 'colidx'.
	 */
	public function make_all_headings_clickable($dispatch_id='interactivetable_event', $destination=null, $otherdata=array()) {
		$this->all_headings_clickable = true;

		$this->click_header_dispatch = $dispatch_id;
		$this->click_header_dest = $destination;
		$this->click_header_columns = array('colidx');
		$this->click_header_otherdata = $otherdata;
	}

	/**
	 * Is the given row or cell clickable?
	 *
	 * @param string $rowidx the index of the row. A null
	 * index indicates the header row. 
	 * @param string $colidx the index of the column the cell is in. If null,
	 * this query concerns the entire row.
	 * @return boolean
	 */
	protected function is_clickable($rowidx, $colidx=null) {

		$is_heading = ($rowidx === null);
		$is_row = ($colidx === null);

		//TODO right now, this counts on having something at index '' in the 
		// array, which works but is kinda freaky.
		if (isset($this->clickable[$rowidx][$colidx])) {
			$clickable = $this->clickable[$rowidx][$colidx];
		} else if ($is_heading) {
			$clickable = $this->all_headings_clickable;
		} else if ($is_row) {
			$clickable = $this->all_rows_clickable;
		} else {
			$clickable = $this->all_cells_clickable;
		}

		return $clickable;
	}

	/**
	 * Get the onClick property for a row or cell.
	 *
	 * @param string $rowidx the index of the row to make clickable. A null
	 * index indicates the header row. 
	 * @param string $colidx the index of the column the cell is in. If null,
	 * this is an action for the entire row.
	 * @return string the onclick property, including onClick="...", or the
	 * empty string if this row or cell is not clickable.
	 */
	protected function get_onclick($rowidx, $colidx=null) {
		if ($this->is_clickable($rowidx, $colidx)) {
			$str = 'onClick="' . $this->get_onclick_value($rowidx, $colidx) . ';"';
		}
		return $str;
	}

	/**
	 * Get value of the onClick property for a row or cell.
	 *
	 * Creates a string that is a Javascript function.
	 *
	 * Note that this method does not check to see if we <i>should</i> be handing
	 * out an onclick, according to what has been set as clickable.  It simply
	 * returns what the onclick command should be if this element is clickable.
	 *
	 * @param string $rowidx the index of the row to make clickable. A null
	 * index indicates the header row. 
	 * @param string $colidx the index of the column the cell is in. If null,
	 * this is an action for the entire row.
	 * @return string the onclick value, not including 'onClick="'.
	 */
	protected function get_onclick_value($rowidx, $colidx=null) {

		// Retrieve the right settings
		$is_heading = ($rowidx === null);
		$is_row = ($colidx === null);
		if ($is_heading) {
			$click_dispatch = $this->click_header_dispatch;
			$click_dest = $this->click_header_dest;
			$click_columns = $this->click_header_columns;
			$click_otherdata = $this->click_header_otherdata;
		} else if ($is_row) {
			$click_dispatch = $this->click_row_dispatch;
			$click_dest = $this->click_row_dest;
			$click_columns = $this->click_row_columns;
			$click_otherdata = $this->click_row_otherdata;
		} else {
			$click_dispatch = $this->click_cell_dispatch;
			$click_dest = $this->click_cell_dest;
			$click_columns = $this->click_cell_columns;
			$click_otherdata = $this->click_cell_otherdata;
		}

		$params = $click_otherdata;
		$params['table_id'] = $this->get_table_id();
		foreach ($click_columns as $thiscolidx) {
			if ($thiscolidx == 'rowidx' || $thiscolidx == 'colidx') {
				// Catch magic terms
				$params[$thiscolidx] = $$thiscolidx;
			} else if ($thiscolidx == 'cell_contents') {
				// Get the contents of the clicked cell
				$params[$thiscolidx] = $this->tabledata[$rowidx][$colidx];
			} else {
				// Otherwise get the contents of the requested column for this row
				$params[$thiscolidx] = $this->tabledata[$rowidx][$thiscolidx];
			}
		}

		if ($click_dispatch) {
			return common_dispatch::get_link_handler_js($click_dispatch, $click_dest, $params);
		} else {

			$location = new Url();
			$location->set_path($click_dest);
			foreach ($params as $k => $v) {
				$location->set_param($k, $v);
			}
			$str = "window.location = '" . $location->get_url_string() . "'; return false;";
		}
		return $str;
	}

	/*
	 * Overridden methods from common_table_plus
	 */

	protected function print_column_heading($thiscolumn, $widthphrase=null) {
		if ($this->is_clickable(null, $thiscolumn['name'])) {
			$thiscolumn['other_properties'] .= $this->get_onclick(null, $thiscolumn['name']);
			$thiscolumn['css'][] = "clickable";
		}
		return parent::print_column_heading($thiscolumn, $widthphrase);
	}

	protected function print_row($thisrowinfo, $height_phrase) {
		if ($this->is_clickable($thisrowinfo['name'])) {
			$thisrowinfo['other_properties'] .= $this->get_onclick($thisrowinfo['name']);
			$thisrowinfo['css'][] = "clickable";
		}
		$output = parent::print_row($thisrowinfo, $height_phrase);
		// Because IE6 is where CSS goes to die
		//$output = '<a class="clickwrap">' . $output . '</a>';
		return $output;
	}

	protected function print_cell($thisrow, $thiscolumn, $width_phrase, $height_phrase) {
		if ($this->is_clickable($thisrow['name'], $thiscolumn['name'])) {
			$thiscolumn['other_properties'] .= $this->get_onclick($thisrow['name'], $thiscolumn['name']);
			$thiscolumn['css'][] = "clickable";
		}
		return parent::print_cell($thisrow, $thiscolumn, $width_phrase, $height_phrase);
	}
}
