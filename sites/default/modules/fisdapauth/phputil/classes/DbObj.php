<?php

require_once('phputil/table_desc.php');

/**
 * This class is intended to provide a simple mapping between PHP, PDA,
 * HTML, javascript, XML, and MySQL representations of FISDAP data.  The
 * goal is to have one standard interface to all the various FISDAP data
 * structures and storage schemes. 
 * 
 * This is currently fairly PDA-centric, and most of the functionality
 * is aimed at ease of processing PDA data submissions (e.g.,
 * from_pda_postdata()).
 */
class DbObj {
    var $fields;
    var $web_id_field;
    var $pda_id_field;
    var $parent_entity;
    var $children;
    var $db_name;
    var $pda2web_field_map;
    var $db_result;
    var $web_id;
    var $pda_id;
    var $connection;
    var $pda_postdata;
    var $entity_ref_fields;
    var $entity_pattern;
    var $child_objects;

    /**
     * 
     */
    function DbObj( $db_name, $id, $id_field, $parent_entity, $children, 
                    $connection, $child_entities=false, 
                    $entity_ref_fields=false ) {
        $this->web_id = $id;
        $this->pda_id = 0;
        $this->web_id_field = $id_field;
        $this->parent_entity = $parent_entity;
        $this->children = $children;
        $this->db_name = $db_name;
        $this->connection = $connection;
        if ( !$this->pda_id_field ) $this->pda_id_field = $id_field;
        $this->entity_ref_fields = $entity_ref_fields || array(); 

        $this->db_result = array();
        $this->pda2web_field_map = array();
        $this->child_objects = array();
        
        $my_name = $this->my_name();
        $this->entity_pattern = '/^'.$my_name.'([-]?[0-9]+)/i';

        //get the fields from the database
        $dbConnect = $connection; //table_desc expects the global $dbConnect
        $db_desc = table_desc( $db_name );
        if ( $db_desc ) {
            foreach( $db_desc as $row ) {
                //skip the id field
                if ( $row['Field'] != $this->web_id_field ) {
                    $this->fields[] = $row['Field'];
                }//if
            }//foreach 
        }//if

        if ( $this->web_id ) {
            $this->from_mysql_db( );
        }//if
    }//DbObj

    /**
     * If we have the given field, and it passes the given test,
     * change its value to the given value.
     */
    function translate_field( $field, $test_func_name, $value ) {
        if ( !$test_func_name ) return false;

        if ( isset($this->db_result[$field]) && 
             $test_func_name($this->get_field($field)) ) {
            $this->set_field($field,$value);
        }//if

        return true;
    }//translate_field

    /**
     * This method allows parent entities to update the references to
     * them in their children.  For example, after a Shift is inserted
     * into the MySQL database, it can use this method to update its
     * children's "Shift_id" fields.
     */
    function set_entity_id_field( $entity_name, $entity_id ) {
        $id_field = $this->entity_ref_fields[$entity_name]; 
        if ( !$id_field ) return false;
        $this->set_field($id_field,$entity_id);
        return true;
    }//set_entity_id_field

    /**
     * Return the db_result field as a string
     */
    function to_string() {
        return var_export($this->db_result,true);
    }//to_string

    /**
     * Return the next pda id (the smallest current id minus
     * one, as they're all negative) for the given object 
     * in its pda_postdata field
     */
    function get_next_pda_id() {
        //get the regex for finding these entities
        $my_name = $this->my_name();
        $regex = $this->entity_pattern;

        //loop over the entries and find the one with the smallest
        //id. then, decrement that to get our new pda id
        $min_id = 0;
        $matches = array();
        foreach( $this->pda_postdata as $key => $val ) {
            if ( preg_match($regex,$key,$matches) ) {
                $found_id = $matches[1];
                if ( $found_id < $min_id ) {
                    $min_id = $found_id;
                }//if
            }//if
        }//foreach

        return --$min_id; //return one less than the current min
    }//get_next_pda_id

    /**
     * This method contains the logic for converting a pda form
     * submission element into a field expected by mysql.
     *
     * By default, pda fields that either 
     *   1) don't exist in the map
     *   - or -
     *   2) have web counterparts that evaluate to false 
     * will be skipped.
     *
     * This method is designed to be overridden by subclasses
     * to allow special processing of some fields that do not 
     * easily map 1:1 to mysql fields.  An example is the EKG 
     * pda field Intervention, which maps to 3 different fields
     * in the mysql database, depending on the value submitted.
     */
    function process_pda_field( $pda_field, $pda_value ) {
        $web_field = $this->pda2web_field_map[$pda_field];
        if ( $web_field && $web_field != $this->web_id_field ) {
            $this->db_result[$web_field] = $pda_value;
        }//if
    }//process_pda_field

    /**
     * Get the given field from the submitted postdata
     *
     */
    function get_pda_field( $field ) {
        if ( $this->pda_postdata ) {
            return $this->pda_postdata[$this->my_name().$this->pda_id.$field];
        } else {
            return false;
        }//else
    }//get_pda_field

    /**
     *
     */
    function get_field( $field ) {
        return $this->db_result[$field];
    }//get_field

    /**
     *
     */
    function set_field( $field, $value ) {
        $this->db_result[$field] = $value;
    }//set_field

    /**
     * Use the given assoc array to map pda fields to 
     * mysql fields.
     */
    function set_field_map( $map ) {
        $this->pda2web_field_map = $map;
    }//set_field_map


    /**
     *
     */
    function delete() {
        if ( $this->web_id ) {
            $query = 'DELETE FROM ' . $this->db_name . ' '
                .    'WHERE ' . $this->web_id_field 
                .               '="' . $this->web_id . '" '
                .    'LIMIT 1';
            $result = mysql_query($query, $this->connection);
            if ( !$result ) {
                return false;
            }//if

            return true;
        }//if
    }//delete


    /**
     *
     */
    function from_mysql_db( ) {
        $query = 'SELECT * FROM '.$this->db_name.' '.
                 'WHERE '.$this->web_id_field.'="'.$this->web_id.'"';
        //echo "\nquery: ".$query."\n";
        $result = mysql_query( $query, $this->connection );
        if ( !$result || mysql_num_rows($result)!=1 ) {
            return false;
        }//if

        $this->db_result = mysql_fetch_assoc( $result );
        $this->web_id = $this->db_result[$this->web_id_field];

        // get rid of the id field in the db_result, since we don't 
        // want to insert/update it
        unset($this->db_result[$this->web_id_field]);
        return true;
    }//from_mysql_db

    /**
     * 
     */
    function my_name( ) {
        return substr($this->pda_id_field,0,strlen($this->pda_id_field)-3);
    }//my_name

    /**
     * Set the internal postdata array to the given input
     *
     * note: the internal array will be a reference to the actual
     *       external array, so don't give this function 
     *       anything you don't want destroyed (I'd recommend 
     *       using a copy of the original $_POST array with 
     *       this method)
     */
    function set_pda_postdata( &$postdata ) {
        $this->pda_postdata =& $postdata;
    }//set_pda_postdata

    /**
     * @warning: this function is destructive.  Each entry in the 
     *           given array of postdata that matches one of this 
     *           object's fields will be removed.
     *
     * @todo: make sure we have a standard way of designating fields
     *        as sanitized or not (e.g., after reading from postdata)
     */
    function from_pda_postdata( &$postdata ) {
        $this->pda_postdata =& $postdata;
        if ( !$this->pda2web_field_map ) return false;

        $my_name = $this->my_name();
        $my_name_len = strlen($my_name);
        $found_id = 0;

        // The following regular expression will capture the numeric string 
        // immediately following the name of the entity.  This captured 
        // string is stored in $matches[1] ($matches[0] is the entire matching
        // string, including the entity name), and contains the ID of the 
        // entity.
        $matches = array();
        foreach( $this->pda_postdata as $key => $val ) {
            if (preg_match($this->entity_pattern,$key,$matches) ) {
                // the ID is the first capture from the above regular expression
                if ($matches[1]) $found_id = $matches[1];
                $this->pda_id = $found_id;
                if ( $this->pda_id > 0 ) {
                    $this->web_id = $this->pda_id;
                }//if
                break;
            }//if
        }//foreach

        // abort now if the search failed
        if ( !$found_id ) return false;

        // wipe out any old values in db_result
        $this->db_result = array();

        // loop over the fields in the map, getting 
        // the posted values and setting this object's
        // fields according to the Web field names
        foreach( $this->pda2web_field_map as $pda_field => $web_field ) {
            $post_key = $my_name . $found_id . $pda_field;
            $posted_val = $this->pda_postdata[$post_key];
            $this->process_pda_field($pda_field,$posted_val);
            //unset($this->pda_postdata[$post_key]);//remove the fields when we're done with 'em
        }//foreach

        //call the postproccess method to do any additional work on getting
        //the values from the pda set up correctly
        $this->postprocess_pda_submission();
        //$this->email_status_report();
        return true;
    }//from_pda_postdata

    /**
     * This method is called automatically after reading from pda postdata, 
     * and is intended to allow automatic behind-the-scenes tinkering with 
     * the result of reading from postdata (creating extra child objects, 
     * populating extra form fields, validating the object's new fields, etc)
     *
     * By default, this is where we clean out all postdata that is associated
     * with this entity, so if this method is overridden, the child class
     * should either duplicate this functionality or call this method.
     */
    function postprocess_pda_submission() {
        // get rid of any extra fields the pda submitted, so we don't
        // leave any traces of this entity in the postdata.
        // @todo: is there a more efficient way to get rid of these?
        foreach( $this->pda_postdata as $key => $val ) {
            if ( preg_match( '/^'.$this->my_name().$this->pda_id.'[a-zA-Z]+[a-zA-Z_0-9]+$/', $key ) ) {
                unset($this->pda_postdata[$key]);
            }//if
        }//foreach
    }//postprocess_pda_submission

    /**
     * Write the object to the internal POSTDATA array, mimicking 
     * a PDA form submission 
     */
    function to_pda_postdata() {
        $my_name = $this->my_name();
        $my_name_len = strlen($my_name);
        $found_id = 0;

        if( $this->parent_entity ) {
            // this entity gets submitted with its parent.

            $matches = array();
            foreach( $this->pda_postdata as $key => $val ) {
                if ( preg_match($this->entity_pattern, $key, $matches) 
                     && $matches[1]==$this->pda_id 
                   ) {
                    mail(
                        MAIL_TO,
                        'DbObj: to_pda_postdata: entity exists already',
                        'The entity to be inserted already existed in '
                        . 'the given array'
                    );
                    $this->email_status_report(
                        'The '.$my_name.' already exists in the pda postdata'
                    );
                    return false;
                }//if
            }//foreach
            
            // loop over the fields in the map, setting the right
            // postdata fields from the existing Web values stored in
            // $this->db_result
            foreach( $this->pda2web_field_map as $pda_field => $web_field ) {
                //@todo: make sure this logic makes sense
                //skip fields that aren't set for this object
                if ( isset( $this->db_result[$web_field] ) ) {
                    $this->pda_postdata[
                        $my_name . $this->pda_id . $pda_field
                    ] = $this->db_result[$web_field];
                }//if
            }//foreach
        } else {
            // this entity is the toplevel form submission.
             
            // make sure the entity doesn't already exist in 
            // the postdata
            if ( isset($this->pda_postdata[$this->pda_id_field]) ) {
                // it's probably sufficient to check for the ID field only,
                // since it must exist for a submission to be valid
                mail(
                    MAIL_TO,
                    'DbObj: to_pda_postdata: entity exists already',
                    'The entity to be inserted already existed in the '
                    . 'given array'
                );
                return false;
            }//if

            // set the pda field names to the top level of the
            // postdata
            $this->pda_postdata[$this->pda_id_field] = $this->pda_id;
            foreach( $this->pda2web_field_map as $pda_field => $web_field ) {
                // for toplevel entities, make sure the fields exist 
                // before writing them
                if ( isset($this->db_result[$web_field]) ) {
                    $this->pda_postdata[$pda_field] = $this->db_result[
                        $web_field
                    ];
                }//if
            }//foreach
        }//else

        return true;
    }//to_pda_postdata


    /**
     * Return true iff the result set for this object contains the given
     * field.  Note that from_mysql_db should be called prior to using
     * this method.
     *
     * @todo make sure this works for fields set to the empty string, or
     *       any other value which evaluates to false
     *
     * @param string the field in question
     * @return bool true iff the given field is in this object's result set
     */
    function has_field( $field ) {
        if ( is_array($this->db_result) ) {
            return array_key_exists($field, $this->db_result);
        }//if

        return false;
    }//has_field


    /**
     * Write the object's fields to the FISDAP mysql database
     *
     * @warning: this method assumes that gpc_magic_quotes is set, 
     *           and therefore does nothing to protect against sql
     *           injection
     * @todo: make sure the "complete shift" check works as expected
     */
    function to_mysql_db() {
        //insert a new entry if the web id is 0.  otherwise, update the
        //existing entry.
        $query = '';
        if ( !$this->web_id ) {
            $query = 'INSERT INTO ';
        } else {
            $query = 'UPDATE ';
        }//else
        $query .= $this->db_name.' SET ';

        // build the set of key/value pairs to insert
        foreach( $this->db_result as $key => $val ) {
            if ( $key != $this->web_id_field ) {
                $query .= $key . '="' .$val. '", ';
            }//if
        }//foreach

        //chop off the final comma and space
        $query = substr( $query, 0, strlen($query)-2 ); 

        // add a WHERE clause if we're updating
        if ( $this->web_id ) {
            $query .= ' WHERE '.$this->web_id_field.'="'.$this->web_id.'"';
        }//if

        //print_r($this);
        //die('query = "'.$query.'"');
        $result = mysql_query( $query, $this->connection );

        if ( !$result ) {
            // send myself an error report if the insert/update fails
            $subject = 'DbObj.to_mysql_db: failed to insert/update '.
                $this->my_name().' #'.$this->pda_id;
            $message = 'The query:'."\n".
                $query."\n\n".
                'The mysql error message:'."\n".
                mysql_error($this->connection);
            //$message = wordwrap( $message, 70 );
            mail( MAIL_TO, $subject, $message );
            $this->email_status_report(
                'DbObj.to_mysql_db: Affected object status report'
            );
            return false;
        }//if

        // for now, I want to receive mail whenever a DbObj is
        // inserted/updated via the pda interface
        if ( $this->pda_id ) {
            //mail(MAIL_TO,
            //     'PDA Shift #'.$this->pda_id.' inserted/updated',
            //     'The query is:'."\n".$query);
        }//if

        // if inserting, set the entity's web id to the newly inserted id
        if ( !$this->web_id ) {
            $this->web_id = mysql_insert_id($this->connection);
        }//if

        // it's inefficient, but it's probably best to repopulate the
        // object from the database to make sure we have the latest
        // fields
        $this->from_mysql_db( );

        return $result;
    }//to_mysql_db


    /**
     * Email myself a status report on this object (postdata, 
     * fields, etc)
     *
     * Note: the boolean parameter to var_export causes the function
     *       to return its output rather than printing it.
     */
    function email_status_report($subject = 'DbObj Status Report') {
        $to = 'smartin@fisdap.net';
        $message = 'This is a generic DbObj status report.'."\n";

        $message .= 'Web id: '.$this->web_id . "\n";
        $message .= 'Web id field: '.$this->web_id_field . "\n\n";
        $message .= 'PDA id: '.$this->pda_id . "\n";
        $message .= 'PDA id field: '.$this->pda_id_field . "\n\n";
        $message .= 'parent entity: '.$this->parent_entity . "\n";
        $message .= "\n\n";

        $message .= 'children : ' . "\n" . var_export( $this->children, true );
        $message .= "\n\n";

        $message .= 'PDA postdata:'. "\n" . var_export( $this->pda_postdata, true ); 
        $message .= "\n\n";

        $message .= 'pda2web field map:' . "\n" . var_export( $this->pda2web_field_map, true ); 
        $message .= "\n\n";

        $message .= 'Web fields:' . "\n" .  var_export( $this->db_result, true ); 
        $message .= "\n\n";

        $server_data = $_SERVER;
        unset( $server_data['PHP_AUTH_PW'] );
        $message .= '_SERVER data (sans password):'. "\n" . var_export( $server_data, true ); 
        $message .= "\n\n";

        mail( $to, $subject, $message );
    }//email_status_report

    /**
     * Create a simple form for entering one of these things into the
     * database.  Note: this seems more suited to be a static method,
     * but we require an instance to be able to access the
     * pda2web_field_map
     * 
     */
    function create_add_form() {
        $add_page = '
            <html>
              <head>
              <style type="text/css">
                  label {
                      float:left;
                      width:15em;
                      text-align:left;
                  }
                  input {
                      float:left;
                      width:15em;
                      text-align:left;
                  }
                  div.spacer_row {
                      clear:both;
                      height:1px;
                  }
                </style>
                <title>
                  '.$this->my_name().' creation form
                </title>
              </head>
              <body>
              <form name="AddForm" id="AddForm" method="POST" action="pda_shift_backend.php">';

        if ( !$this->pda_id ) $this->pda_id = -1;
        if ( $this->parent_entity ) {
            $add_page .= '
                <input type="hidden" 
                       name="'.$this->parent_entity.'_id" 
                       id="'.$this->parent_entity.'_id" 
                       value="-1" />';
        }//if

        foreach( array_keys($this->pda2web_field_map) as $key ) {
            if ( $this->parent_entity ) {
                $input_name = $this->my_name().$this->pda_id.$key;
            } else {
                $input_name = $key;
            }//else
            $add_page .= '
                <div class="spacer_row"></div>
                <label for="'.$input_name.'">'.$key.'</label>
                <input name="'.$input_name.'" 
                       id="'.$input_name.'" 
                       type="text" 
                       value="" /> 
                <div class="spacer_row"></div>';
        }//foreach
        
        $add_page .= '
                <p><input type="submit" value="Submit" /></p>
              </form>
              </body>
            </html>';
        return $add_page;
    }//create_add_form


    function get_pda_id() {
        return $this->pda_id;
    }//get_pda_id
}//DbObj

?>
