<?php

/**
 * This class models a FISDAP Patient Complaint entry
 *
 */
class FISDAP_Complaint extends DataEntryDbObj {    

    /** 
     * Create a new Complaint with the specified id and the specified db link
     */
    function FISDAP_Complaint( $id, $connection ) {
        $this->pda_id_field = 'PtComp_id';
        $this->DbObj('PtComplaintData',                                    //table name
                $id,                                                       //id
                'Data_id',                                                 //id field
                'Shift',                                                   //parent entity name
                array('EvalSession'),                                      //children
                $connection );                                             //database connection
        $field_map = array(
                'Shift_id' => 'Shift_id',
                'Student_id' => 'Student_id',
                'Run_id' => 'Run_id',
                'Assess_id' => 'Asses_id',
                'Complaint_id' => 'Complaint_id',
                'SubjectType_id' => 'SubjectType_id'
                ); 
        $this->set_field_map( $field_map );
    }//constructor FISDAP_Complaint
}//class FISDAP_Complaint

?>
