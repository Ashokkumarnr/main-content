<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FisdapDateTime.inc');

/**
 * Associative options.
 * Handles an array with keys and typed values.
 * Can handle either numeric or named keys.
 */
final class AssociativeOptions {
	private $options;

	/**
	 * Constructor.
	 * @param array | null $options The associative options.
	 */
	public function __construct($options) {
		Assert::is_true(
			Test::is_null($options) ||
			Test::is_array($options));

		$this->options = Convert::to_array($options);
	}

	/**
	 * Retrieve a boolean value.
	 * @param mixed $key The option key.
	 * @param boolean $default The default value if not specified.
	 * @return boolean The value.
	 */
	public function get_boolean($key, $default=true) {
		$this->assert_key($key);
		Assert::is_boolean($default);
		
		$value = $this->get_value($key, $default);
		Assert::is_boolean($value);
		return $value;
	}

	/**
	 * Retrieve a date value.
	 * Note: due to class type hints not allowing me to declare an object, 
	 * passing NULL for the default results in FisdapDate::not_set().
	 * @param mixed $key The option key.
	 * @param null | FisdapDate $default The default value if not specified.
	 * @return FisdapDate The value.
	 */
	public function get_date($key, $default=null) {
		$this->assert_key($key);
		Assert::is_true(
			Test::is_null($default) ||
			Test::is_a($default, 'FisdapDate'));

		if (is_null($default)) {
			$default = FisdapDate::not_set();
		}

		$value = $this->get_value($key, $default);
		Assert::is_a($value, 'FisdapDate');
		return $value;
	}

	/**
	 * Retrieve a date/time value.
	 * Note: due to class type hints not allowing me to declare an object, 
	 * passing NULL for the default results in the FisdapDate::not_set().
	 * @param mixed $key The option key.
	 * @param null | FisdapDateTime $default The default value if not specified.
	 * @return FisdapDate The value.
	 */
	public function get_date_time($key, $default=null) {
		$this->assert_key($key);
		Assert::is_true(
			Test::is_null($default) ||
			Test::is_a($default, 'FisdapDateTime'));

		if (is_null($default)) {
			$default = FisdapDateTime::not_set();
		}

		$value = $this->get_value($key, $default);
		Assert::is_a($value, 'FisdapDateTime');
		return $value;
	}

	/**
	 * Retrieve a time value.
	 * Note: due to class type hints not allowing me to declare an object, 
	 * passing NULL for the default results in FisdapTime::not_set().
	 * @param mixed $key The option key.
	 * @param null | FisdapDateTime $default The default value if not specified.
	 * @return FisdapDate The value.
	 */
	public function get_time($key, $default=null) {
		$this->assert_key($key);
		Assert::is_true(
			Test::is_null($default) ||
			Test::is_a($default, 'FisdapTime'));

		if (is_null($default)) {
			$default = FisdapTime::not_set();
		}

		$value = $this->get_value($key, $default);
		Assert::is_a($value, 'FisdapTime');
		return $value;
	}

	/**
	 * Retrieve an integer value.
	 * @param mixed $key The option key.
	 * @param int $default The default value if not specified.
	 * @return int The value.
	 */
	public function get_int($key, $default=0) {
		$this->assert_key($key);
		Assert::is_int($default);
		
		$value = $this->get_value($key, $default);
		Assert::is_int($value);
		return $value;
	}

	/**
	 * Retrieve a string value.
	 * @param mixed $key The option key.
	 * @param string $default The default value if not specified.
	 * @return string The value.
	 */
	public function get_string($key, $default='') {
		$this->assert_key($key);
		Assert::is_string($default);
		
		$value = $this->get_value($key, $default);
		Assert::is_string($value);
		return $value;
	}

	/**
	 * Retrieve an array value.
	 * @param mixed $key The option key.
	 * @param array $default The default value if not specified.
	 * @return array The value.
	 */
	public function get_array($key, $default=array()) {
		$this->assert_key($key);
		Assert::is_array($default);
		
		$value = $this->get_value($key, $default);
		Assert::is_array($value);
		return $value;
	}

	/**
	 * Determine if an option is specified.
	 * @param mixed $key The option key.
	 * @return boolean TRUE if specified.
	 */
	public function contains($key) {
		$this->assert_key($key);
		return array_key_exists($key, $this->options);
	}

	/**
	 * Retrieve the raw value.
	 * @param mixed $key The option key.
	 * @param mixed $default The default value if not specified.
	 * @return mixed The value.
	 */
	public function get_value($key, $default=null) {
		$this->assert_key($key);

		if ($this->contains($key)) return $this->options[$key];
		return $default;
	}

	public function assert_key($key) {
		Assert::is_true(
			Test::is_string($key) ||
			Test::is_int($key));
	}
}
?>
