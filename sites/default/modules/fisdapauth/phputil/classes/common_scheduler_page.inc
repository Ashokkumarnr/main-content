<?php
require_once('phputil/classes/common_page.inc');
require_once('phputil/classes/common_user.inc');
require_once('phputil/inst.html');
require_once('phputil/session_functions.php');
require_once('phputil/session_data_functions.php');
require_once('phputil/stushiftentry.html');

/**
 * A web page that is common to the scheduler.
 * This is a standard page that has a context menu.
 */
class common_scheduler_page extends common_page
{
    var $connection;
    var $instructor = null;
    var $user;

    /**
     * Constructor.
     * @param title The page title or null.
     * @param redirect_url The URL to keep redirecting to or null.
     * @param connection The DB connection.
     */
	function common_scheduler_page( $title, $redirect_url, &$connection) 
    {
        $this->connection = $connection;
        $this->user = new common_user(null);

        if($this->user->is_instructor())
        {
            $this->instructor = new common_instructor($this->user->get_instructor_id());
        }

        parent::common_page($title, $redirect_url);
    }

    /**
     * Overridden.
     */
	function display_header() 
    {
        // Add the java script for the menus.
        $is_instructor_01 = $this->user->is_instructor() ? '1' : '0';    
        $js = <<<EOI
var Instructor = {$is_instructor_01};
	
function limitHit(type)
{
    alert("This account has a limited number of " + type + 
        " shifts available to it.\\nYou are unable to add additional " + type + 
        " shifts on this account due to the fact that you have reached this limit.");
}
EOI;
        $this->add_javascriptcode($js);

        parent::display_header();

        // Define a simple table layout for the menu and the real content.
?>
<table border="0" width="100%">
  <tr><td id="contentleft" valign="top" style="width: 95px; text-align: left">
<?php $this->display_side_menu_(); ?>
  </td><td id="contentright" valign="top">
<?php
    }

    /**
     * Overridden.
     */
    function display_footer()
    {
?>
</td></tr></table>
<?php
        parent::display_footer();
    }

    /**
     * Redirect to the InfoFrame.
     * @param url The URL to use.
     * @return The URL going through the scheduler.
     */
    function get_info_area_url($url)
    {
        return FISDAP_WEB_ROOT . 'scheduler/scheduler.php?goToPage=' . $url;
    }

    function display_side_menu_()
    {
?>
<table cellpadding="0" cellspacing="0" border="0" align="center">
<tr><td>
	<img src="images/schedmenu.gif" border="0">
</td></tr>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('shiftlist.html'); ?>">
    <img src="images/pickred.gif" border="0">
  </a>
</td></tr>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('pickcal.html'); ?>">
      <img src="images/pickcalmenu.gif" border="0">
  </a>
</td></tr>
<?php
        if($this->user->is_instructor())
        {
            $this->display_instructor_menu_();
        }
        else
        {
            $this->display_student_menu_();
        }
?>
</table>
<?php
    }

    function display_student_menu_()
    {
        global $REMOTE_USER;

        $statement = 'SELECT Class_Year,ClassMonth,Student_id' .
            " FROM StudentData WHERE UserName='{$REMOTE_USER}'";
        $results = $this->connection->query($statement);
        if(count($results) == 1)
        {
            $result =& $results[0];
            $Class_Year = check_session_value($result['Class_Year'], 'scheduler_class_year');
            $ClassMonth = check_session_value($result['ClassMonth'], 'scheduler_class_month');
            $Student_id = check_session_value($result['Student_id'], 'scheduler_student_id');
	    }

        $statement = 'SELECT Student_id, MaxFieldShifts, MaxClinicShifts, MaxLabShifts' .
            " FROM StudentData where UserName='{$REMOTE_USER}'";
        $results = $this->connection->query($statement);
        if(count($results) != 1)
        {
            echo "Unable to locate user[{$REMOTE_USER}].";
            exit;
        }

        $result = &$results[0];
        $id = $result['Student_id'];
        $fieldLimit = $result['MaxFieldShifts'];
        $clinicLimit = $result['MaxClinicShifts'];
        $labLimit = $result['MaxLabShifts'];

        $statement = "SELECT Count(*) FROM ShiftData WHERE Student_id={$id} AND Type='field'";
        $results = $this->connection->query($statement);
        $numField = $results[0]['Count(*)'];
        	
        $statement = 'SELECT Count(*) FROM ShiftData' .
            " WHERE Student_id={$id} AND Type='clinical'";
        $results = $this->connection->query($statement);
        $numClinic = $results[0]['Count(*)'];
	
        $statement = "SELECT Count(*) FROM ShiftData WHERE Student_id={$id} AND Type='lab'";
        $results = $this->connection->query($statement);
        $numLab = $results[0]['Count(*)'];

        if($StuFieldShiftEntry == 1 || $StuClinicShiftEntry == 1 || $StuLabShiftEntry == 1)
        {
?>
<tr><td>
  <img border="0" src="../shift/images/adddata.gif" alt="Add Data"><br>
</td></tr>
<?php
        }
        if($StuFieldShiftEntry == 1)
        {	
            if($numField < $fieldLimit)
            {
?>
<tr><td>
  <a href="../shift/field/shift.html?Shift_id=-1&fromScheduler=1" TARGET='InfoArea'>
    <img border="0" src="../shift/images/fi.gif" alt="Field">
  </a><br>
</td></tr>
<?php
            }
            else if($fieldLimit < 0)
            {
?>
<tr><td>
  <a href="../shift/field/shift.html?Shift_id=-1&fromScheduler=1" TARGET='InfoArea'>
    <img border="0" src="../shift/images/fi.gif" alt="Field">
  </a><br>
</td></tr>
<?php
            }
            else
            {
?>
<tr><td>
  <a href="javascript:limitHit('field');">
    <img border="0" src="../shift/images/fi.gif" alt="Field">
  </a><br>
</td></tr>
<?php
            }
        }

        if($StuClinicShiftEntry == 1)
        {
            if($numClinic < $clinicLimit)
            {
?>
<tr><td>
  <a href="../shift/clinical/shift.html?Shift_id=-1&fromScheduler=1" target="InfoArea">
    <img border="0" src="../shift/images/cl.gif" alt="Clinical">
  </a><br>
</td></tr>
<?php
            }
            else if($clinicLimit < 0)
            {
?>
<tr><td>
  <a href="../shift/clinical/shift.html?Shift_id=-1&fromScheduler=1" target="InfoArea">
  <img border="0" src="../shift/images/cl.gif" alt="Clinical">
  </a><br>
</td></tr>
<?php
            }
            else
            {
?>
<td><td>
  <a hreF="javascript:limitHit('clinical');">
  <img border="0" src="../shift/images/cl.gif" alt="Clinical">
  </a><br>
</td></tr>
<?php
            }
        }

        if($StuLabShiftEntry == 1)
        {
            if($numLab < $labLimit)
            {
?>
<tr><td>
  <a hreF="../shift/clinical/shift.html?Shift_id=-1&shiftType=lab" target="InfoArea">
    <img border="0" src="../shift/images/la.gif" alt="Lab">
  </a><br>
</td></tr>
<?php
            }
            else if($labLimit < 0)
            {
?>
<tr><td>
  <a href="../shift/clinical/shift.html?Shift_id=-1&shiftType=lab" target="InfoArea">
    <img border="0" src="../shift/images/la.gif" alt="Lab">
  </a><br>
</td></tr>
<?php
            }
            else
            {
?>
<tr><td>
  <a hreF="javascript:limitHit('lab');">
    <img border="0" src="../shift/images/la.gif" alt="Lab">
  </a><br>
</td></tr>
<?php
	        }
        }
    }

    function display_instructor_menu_()
    {
        $canAdminClinic = $this->instructor->get_permission('canAdminClinic'); 
        $canAdminField = $this->instructor->get_permission('canAdminField'); 
        $canAdminLab = $this->instructor->get_permission('canAdminLab'); 
        $canAdminProgram = $this->instructor->get_permission('canAdminProgram'); 

        if ($canAdminClinic || $canAdminField || $canAdminLab)
        {
?>
<tr><td>
  <img src="images/adddata.gif" border="0">
</td></tr>
<?php
        }

        if ($canAdminField) 
        {	
?>
<tr><td>
<a href="<?php echo $this->get_info_area_url('fshiftframes.html"'); ?>">
    <img src="images/fishiftmenu.gif" border="0">
  </a>
</td></tr>
<?php
        }

        if ($canAdminClinic) 
        {
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('clshiftframes.html'); ?>">
    <img src="images/clshiftmenu.gif" border="0">
  </a>
</td></tr>
<?php
        }

        if ($canAdminLab) 
        {
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('lashiftframes.html'); ?>">
    <img src="images/lashiftmenu.gif" border="0">
  </a>
</td></tr>
<?php
        }

        if ($canAdminProgram || $canAdminClinic || $canAdminField || $canAdminLab)
        {
?>
<tr><td>
  <img src="images/tradedroplabel.gif" border="0">
</td></tr>
<?php
        }

        if ($canAdminProgram)
        {
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('schedulerPrefs.html'); ?>">
    <img src="images/settings.gif" border="0">
  </a>
</td></tr>
<?php
        }

        if ($canAdminClinic || $canAdminField || $canAdminLab)
        {
?>
<tr><td>
  <a href="<?php echo $this->get_info_area_url('tradeApproval.html'); ?>">
    <img src="images/approval.gif" border="0">
  </a>
</td></tr>
<?php
        }

        if ($canAdminProgram)
        {
            $shareNum = 0;
?>
<tr><td>
<a href="<?php echo $this->get_info_area_url('../admin/startSharing.html'); ?>">
    <img border="0" src="images/ShareSites.gif" alt="Share Sites">
  </a>
</td></tr>
<?php
        }
    }

    /**
     * Display an error page.
     * @param msg The error message.
     */
    function display_error_page($msg)
    {
        $this->display_header();
        $this->display_pagetitle();
?>
<p align="center" class="largetext">
  An error has occurred: <?php echo $msg; ?>
</p>
<?php
        $this->display_footer();
    }

    /**
     * Display a page indicating the user is not authorized to view this page.
     * @param contact The person to contact for help, may be null or empty.
     */
    function display_unauthorized_user_page()
    {
        $this->display_header();
        $this->display_pagetitle();

        $contact = $this->user->is_instructor() ? 
            $this->instructor->get_contact() :
            'an administrator';

        if(is_null($contact) || (trim($contact) == ''))
        {
            $contact = 'an instructor';
        }

?>
<p align="center" class="largetext">
Currently you do not have access to view this page.
  Please contact <?php echo $contact; ?> for access.
</p>
<?php
        $this->display_footer();
    }
}
?>
