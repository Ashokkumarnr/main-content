<?php
/**
 * A map of items (key->value). 
 * <pre>
 * Developers:  ONLY BASIC FUNCTIONALITY HAS BEEN TESTED HERE
 * Developers:  The SimpleMap for some odd reason is faster, even
 *              for very large maps.  There is some investigating
 *              to do here.
 * </pre>
 */
abstract class AMap {
    /**
     * Determine if an item is in the list.
     * @param string|int $key The key.
     * @return boolean TRUE if the key is in the list.
     */
    abstract public function contains($key);

    /**
     * Determine the location of an item in the list.
     * @param string|int $key The key.
     * @return mixed The value, or null if not found.
     */
    abstract public function get($key);

    /**
     * Insert an item into the list.
     * If the key already exists, it's value is overwritten.
     * @param string|int $key The key.
     * @param mixed $value The value.
     */
    abstract public function put($key, $value);

    /**
     * Determine the number of entries in the map.
     * @return int The number of entries.
     */
    abstract public function size();
}

/**
 * A simple PHP associative array.
 */
class SimpleMap extends AMap {
    private $map = array();

    /**
     * Constructor.
     * @param array $array The initial values.
     */
    public function SimpleMap(&$array=array()) {
        foreach ($array as $key=>$value) {
            $this->map->put($key, $value);
        }
    }

    public function size() {
        return count($this->map);
    }

    public function contains($key) {
        return array_key_exists($key, $this->map);
    }

    public function get($key) {
        if ($this->contains($key)) {
            return $this->map[$key];
        }

        return null;
    }

    public function put($key, $value) {
        $this->map[$key] = $value;
    }
}

/**
 * A list that whose keys are sorted in ascending order.
 * The value is stored directly, NOT by reference.
 * The key must currently support the &lt; operator.
 * Comparators would be a nice addition.
 * Developers, I have tried using an object instead of array(key,value)
 * and the object is MUCH slower.
 */
class SortedKeyMap extends AMap {
    private $map = array();       // array of sorted array(key,value)
    private $size = 0;

    /**
     * Constructor.
     * @param array $array The initial values.
     */
    public function SortedKeyMap(&$array=array()) {
        foreach ($array as $key=>$value) {
            $this->put($key, $value);
        }
    }

    public function size() {
        return $this->size;
    }

    public function contains($key) {
        return !is_null($this->binary_search($key));
    }

    public function get($key) {
        $entry = $this->binary_search($key);
        if (is_null($entry)) return null;

        return $entry[1];
    }

    public function put($key, $value) {
        $this->binary_insertion($key, $value);
    }

    /**
     * Verify the integrity of the list.
     */
    private function verify_integrity() {
        for ($i = 1; $i < $this->size; ++$i) {
            if ($this->map[$i-1][0] > $this->map[$i][0]) {
                die('Integrity violated.');
            }
        }
    }

    /**
     * Perform a binary search on a sorted list with non-scalar values.
     * The keys in the list should be the same type as the search item.
     * @param mixed $key The key to search for.
     * @return array|null The map entry if found or null if not.
     */
    private function binary_search($key) {
        $high = $this->size - 1;
        $low = 0;

        while ($low <= $high) {
            // Integer division.
            $n = $high + $low;
            $i = ($n - ($n % 2)) / 2;

            if ($this->map[$i][0] < $key) {
                $low = $i + 1;
            }
            elseif ($this->map[$i][0] > $key) {
                $high = $i - 1;
            }
            else {
                return $this->map[$i];
            }
        }

        return null;
    }

    /**
     * Perform a binary insertion on a sorted list with non-scalar values.
     * @param mixed $key The key to search for.
     * @param mixed $value The value to add.
     */
    private function binary_insertion($key, $value) {
        $entry = array($key, $value);
        $high = $this->size - 1;
        $low = 0;
        $insert = 0;

        // Special cases.
        if ($this->size) {
            // New first element?
            if ($this->map[0][0] >= $key) {
                array_splice($this->map, 0, 0, array($entry));
                ++$this->size;
                return;
            }

            // New last element?
            if ($this->map[$high][0] <= $key) {
                $this->map[$this->size] = $entry;
                ++$this->size;
                return;
            }
        }

        while ($low <= $high) {
            // Integer division.
            $n = $high + $low;
            $i = ($n - ($n % 2)) / 2;

            if ($this->map[$i][0] < $key) {
                $low = $i + 1;
                $insert = $low;
            }
            elseif ($this->map[$i][0] > $key) {
                $high = $i - 1;
                $insert = $i;
            }
            else {
                $insert = $i;
                break;
            }
        }

        // Insert it.
        array_splice($this->map, $insert, 0, array($entry));
        ++$this->size;
    }
}
?>
