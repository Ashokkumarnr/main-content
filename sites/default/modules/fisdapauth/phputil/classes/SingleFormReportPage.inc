<?php
/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2007.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/
require_once('phputil/classes/common_form.inc');
require_once('phputil/classes/common_page.inc');
require_once('phputil/classes/common_report.inc');
require_once('phputil/classes/report_utilities.inc');
require_once('phputil/classes/MessageArea.inc');
require_once('phputil/classes/SingleFormPage.inc');

/**
 * A class that handles a single form with a submit button and an optional
 * cancel button.  After submitting, a report is generated.
 * 
 * As long as you follow the template, there isn't much to do to set up a page.
 *
 * You must provide:
 * - create_form()
 * - process_cancel() or set_cancel_url() if you use a CANCEL button
 * - display_report() to display the report
 * - get_destination() to provide the report destination
 *
 * See the subclass for further options.
 *
 */
abstract class SingleFormReportPage extends SingleFormPage {
    private $page;

    const NOW_GENERATING_NAME = 'nowgenerating';

    /**
     * Constructor.
     * @param common_page $page The page to use.
     */
    public function __construct($page) {
        $this->page = $page;
        parent::__construct($page);
    }

    /**
     * Retrieve the destination for the report.
     * @return string The destination, i.e. 'html', 'pdf', etc.
     */
    abstract protected function get_destination(); 

    /**
     * Display the report.
     * The page header, page title, and page footer are displayed for you.
     */
    abstract protected function display_report();

    public function process_default() {
        common_utilities::set_scriptvalue(self::NOW_GENERATING_NAME, false);
        parent::process_default();
    }

    public function process_form() {
        $this->get_message_area()->clear_all();

        $attributes = $this->get_attributes();
        $form = $this->create_form($attributes);
        $this->set_form($form);

        // Cancelled?
        $button = $form->get_pressed_button();
        $cancelled = !is_null($button) && 
            ($button->get_type() == common_form_button::CANCEL);
        if ($cancelled) {
            if ($this->process_cancel()) return;
        }

        // Generating the report?
        $generating_report = 
            common_utilities::get_scriptvalue(self::NOW_GENERATING_NAME);
        if ($generating_report) {
            common_utilities::set_scriptvalue(self::NOW_GENERATING_NAME, false);
            $this->display_the_report();
            return;
        }

        // Must have been submitted.
        $validated = $form->process();
        if ($validated) {
            $destination = $this->get_destination();
            if (strtoupper($destination) == 'HTML') {
                $this->display_the_report();
                return;
            }

            // We let the user know that report generating is pending.
            common_utilities::set_scriptvalue(self::NOW_GENERATING_NAME, true);

            $report_utils = new report_utilities($this->page);
            $url = $_SERVER['PHP_SELF'] . '?dispatch_id=' . SingleFormPage::FORM_NAME;
            $report_utils->display_nowgenerating_page($destination, $url);
            return;
        }

        $html = $form->get_generated_html_form();
        if (!is_null($html)) {
            $this->page->display_header(); 
            $this->page->display_pagetitle();
            echo $html;
            $this->page->display_footer();
        }
    }

    private function display_the_report() {
        $destination = strtoupper($this->get_destination());
        if (($destination == 'HTML') || ($destination == 'TEXT')) {
            $this->page->display_header();

            if ($destination == 'TEXT') {
                $this->page->display_pagetitle();
            }
        }

        $this->display_report();

        if (($destination == 'HTML') || ($destination == 'TEXT')) {
            $this->page->display_footer();
        }
    }
}
?>
