<?php


/**
 * This is a controller to dispatch actions concerning 
 * the item review form
 */
class TestItemReviewFormController extends Controller {

    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     * Create a form controller for the given form and input.
     */
    function TestItemReviewFormController() {
        parent::Controller();
    }//TestItemReviewFormController

    ////////////////////////////////////////////////////////////
    // Public instance methods
    ////////////////////////////////////////////////////////////


    function dispatch($request) {
        // delegate the request to the appropriate controller,
        // based on the review mode (consensus or assignment)
        $controller = '';

        $request_array = $request->get_input();
        
        // check the display mode 
        if ( in_array($request->get_action(),array('edit','view')) ) {
            // if the action is 'edit' or 'view,' we can set the display mode 
            // from the stored review
            $review =& TestItemReview::find_by_id($request_array['review_id']);
            if ( !$review ) {
                die(
                    'We could not find the test item review you requested.  '.
                    'Please go back and try again.'
                );
            } else {
                $mode = $review->get_mode();
            }//else
        } else {
            // get the mode from the request
            $mode = $this->get_display_mode($request_array);
        }//else


        // handle assignment versus consensus separately
        if ( $mode == 'assignment' ) {
            $controller =& new AssignmentTestItemReviewFormController();
        } else {
            $controller =& new ConsensusTestItemReviewFormController();
        }//else

        return $controller->dispatch($request);
    }//dispatch


    function get_display_mode($input) {
        if ( isset($input['mode']) &&
             in_array($input['mode'],array('consensus','assignment')) ) {
            $mode = $input['mode'];
        } else {
            die('<br>Invalid display mode specified ('.htmlentities($input['mode']).').<br>');
        }//else

        return $mode;
    }//get_display_mode

}//class TestItemReviewFormController

?>
