<?php

require_once('phputil/classes/model/TestItemReviewFactory.php');


/**
 * This controller handles requests to the item review
 * form in assignment mode
 */
class AssignmentTestItemReviewFormController extends Controller {

    ////////////////////////////////////////////////////////////
    // Private instance variables
    ////////////////////////////////////////////////////////////

    /** the InputValidationSet to use on the input */
    var $input_validation_set;

    /** the mysql link resource to use for database connectivity */
    var $connection;

    /** the assignment object */
    var $assignment;

    /** the creator object for all reviews and review forms*/
    var $review_creator;


    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     * Create a form controller for the given form and input.
     */
    function AssignmentTestItemReviewFormController() {
        $this->Controller();

        $this->review_creator =& new TestItemReviewFactory();

        // get the database connection
        $connection_obj =& FISDAPDatabaseConnection::get_instance();
        $this->connection = $connection_obj->get_link_resource();

        // set the users who are allowed to edit assignment reviews
        $this->allowed_editors = array(
            'lbriguglio',
            'erikh',
            'csoucheray',
	    'mbowen2',
            'mjohnson',
						'sdesombre',
						'khanson',
            'dpage'
        );       

        // set the actions requests are allowed to invoke
        $this->allowed_actions = array('display','submit','edit','view');
    }//AssignmentTestItemReviewFormController


    ////////////////////////////////////////////////////////////
    // Public instance methods
    ////////////////////////////////////////////////////////////

    /**
     *
     */
    function display($request) {
        if ( $this->verify_assignment($request) ) {
            // display the blank form
            $item_id = $this->assignment->get_item_id();
            $item =& TestItem::find_by_id($item_id);
            $review_form =& $this->review_creator->create_form($item,'assignment');
            $review_form->populate_with_defaults();
            $review_form->populate(array('assignment_id'=>$request['assignment_id'])); 
            $item_review_page =& new TestItemReviewPage($item_id,$review_form);
            $item_review_page->display();
        }//if
    }//display


    /**
     * Add a descriptive header to the given review page,
     * so we can tell who created the review, when it was, etc.
     */
    function prepare_page_for_editing(&$page,&$review) {
        $header =& $page->get_header();

        list($creation_date,$creation_time) = explode(' ',$review->get_created_at());
        
        $review_id_container =& new HTMLTag(
            'div',
            array(
                'id'=>'review_id_display_container'
            )    
        );
        $review_id_text =& new HTMLText(
            'Review #'.$review->get_id()
        );
        $review_id_container->append_child($review_id_text);
        $header->append_child($review_id_container);

        $review_info_container =& new HTMLTag(
            'div',
            array(
                'id'=>'review_info_container'
            )
        );
        $review_info_text =& new HTMLText(
            'Created by '.$review->get_reviewer().' at '.
            $creation_time.' on '.$creation_date
        );
        $review_info_container->append_child($review_info_text);
        $header->append_child($review_info_container);

        // get rid of the review list on edit/view
        $review_form =& $page->get_review_form();
        $review_form->delete_children_by_selector(
            new IDSelector(
                'review_list_container'
            )
        );
    }//prepare_page_for_editing


    /**
     * View an existing assignment review
     *
     * @todo refactor as much of the common behavior between this 
     *       and the edit action into smaller, separate methods
     */
    function view($request) {
        // display the form, filled in (but not editable)
        $review_id_validator = new IntegerValidator('review_id');
        $review_id_validator->validate($request);

        if ( $review_id_validator->has_errors() ) {
            // display the first error message
            $errors = $review_id_validator->get_errors();
            $first_error = $errors[0];
            die($first_error->get_message());
        } else {
            $review_id = $request['review_id'];
            $review =& $this->review_creator->create_review($review_id);
            $item_id = $review->get_item_id();
        }//else

        $item =& TestItem::find_by_id($item_id);
        $review_form =& $this->review_creator->create_form($item,'assignment');
        $item_review_page =& new TestItemReviewPage($item_id,$review_form);
        $this->prepare_page_for_editing($item_review_page,$review);
        $review_form->populate($review->to_assoc_array());
        $review_form->populate(array('assignment_id'=>$request['assignment_id'])); 

        //$review_form->disable_all_inputs();
        $review_form->disable_submission();
        $item_review_page->display();
    }//view


    /**
     *
     */
    function edit($request) {
        if ( in_array(strtolower($_SERVER['PHP_AUTH_USER']), $this->allowed_editors) ) {
            // allow selected users to edit assignment reviews
            $review_id_validator = new IntegerValidator('review_id');
            $review_id_validator->validate($request);

            if ( $review_id_validator->has_errors() ) {
                // display the first error message
                $errors = $review_id_validator->get_errors();
                $first_error = $errors[0];
                die($first_error->get_message());
            } else {
                $review_id = $request['review_id'];
                $review =& $this->review_creator->create_review($review_id);
                $item_id = $review->get_item_id();
            }//else

            $item =& TestItem::find_by_id($item_id);
            $review_form =& $this->review_creator->create_form($item,'assignment');
            $item_review_page =& new TestItemReviewPage($item_id,$review_form);
            $this->prepare_page_for_editing($item_review_page,$review);

            $review_form->populate($review->to_assoc_array());
            $item_review_page->display();
        } else { 
            // users not in the allowed_users list will be shown the review
            // form with a disabled submit button
            $this->view($request);
        }//else
    }//edit

    
    /**
     *
     */
    function submit($request) {
        if ( ( $request['id'] && 
               in_array($_SERVER['PHP_AUTH_USER'],$this->allowed_editors)
             ) || $this->verify_assignment($request) ) {
            $item =& TestItem::find_by_id($request['item_id']);
            $review_form =& $this->review_creator->create_form($item,'assignment');
            $item_review =& $this->review_creator->create_review_from_contents($request);
            $item_review->set_assignment_id($request['assignment_id']);
            if ( $review_id = $item_review->save() ) {
                // success!  
                if ( $request['id'] && 
                    in_array($_SERVER['PHP_AUTH_USER'],$this->allowed_editors) ) {
                    // show the edited review
                    $request = Controller::strip_slashes_from_postdata($request);
                    $review_page = new TestItemReviewPage(
                        $request['item_id'],
                        $review_form
                    );
                    $message = 'Successfully updated the review.';
                    $review_form->populate($request);
                    $review_form->add_generic_error(new ValidationError($message));

                    $review =& $this->review_creator->create_review($review_id);
                    $this->prepare_page_for_editing($review_page,$review);

                    $review_page->display();
                } else {
                    // go to the success page
                    header('Location: assignment_review_success.php');
                }//else
            } else {
                // failure!  display the form with error messages
                $request = Controller::strip_slashes_from_postdata($request);
                $review_page = new TestItemReviewPage($request['item_id'],$review_form);
                $review_form->populate($request);
                $review_form->add_generic_error(
                    new ValidationError(
                        'Your submission could not be processed.  Please check '.
                        'the form below for any errors.'
                    )
                );
                $review_form->add_errors($item_review->get_errors());
                $review_page->display();
            }//else
        }//if
    }//submit


    /**
     *
     */
    function set_assignment_field($assignment_id) {
        $this->assignment =& TestItemReviewAssignment::find_by_id($assignment_id);
    }//set_assignment_field
    

    /**
     * Get the InputValidationSet for the form
     * 
     * @return InputValidationSet the validation set for the form
     */
    function &get_input_validation_set() {
        return $this->input_validation_set;
    }//get_input_validation_set


    /**
     * Set the InputValidationSet for the form
     *
     * @param InputValidationSet $set the validation set to use
     */
    function set_input_validation_set(&$set) {
        $this->input_validation_set =& $set;
    }//set_input_validation_set


    /**
     *
     */
    function validate_assignment_id($request) {
        // validate the assignment id
        $assignment_id_validator = new IntegerValidator('assignment_id');
        $assignment_id_validator->validate($request);

        if ( $assignment_id_validator->has_errors() ) {
            // display the first error message
            $errors = $assignment_id_validator->get_errors();
            $first_error = $errors[0];
            die($first_error->get_message());
        } else {
            $assignment_id = $request['assignment_id'];
            $this->set_assignment_field($assignment_id);
        }//else
    }//validate_assignment_id


    /**
     * Make sure the assignment is legit (attached to this user,
     * valid IDs, etc)
     */
    function verify_assignment($request) {
        // make sure there is an open assignment for the given 
        // user and item.  if the assignment is already complete,
        // exit with an error.

        $this->validate_assignment_id($request);

        // get the assignment, if there is one
        if ( $this->assignment && 
             is_numeric($this->assignment->get_item_id()) &&
             $this->assignment->user_id == getUserAuthID($_SERVER['PHP_AUTH_USER']) &&
             $this->assignment->completed_on == '0000-00-00' ) {
             return true;
        }//if

        die(
            '<div style="padding:2em;margin-left:10%;margin-right:10%;width:auto">'.
            'You do not appear to have an assignment for that item. '.
            'You may have already completed this assignment, or your assignment '.
            'may have been deleted.  Please return to My FISDAP to see your '.
            'updated list of assignments.  Thank you for your participation in this '.
            'project.'.
            '</div>'
        );
    }//verify_assignment


    /**
     * Create a review form based on the item's project type 
     *
     * @todo put this logic in the factory classes
     */
    function &get_review_form($item_id) {
        if ( !is_numeric($item_id) ) {
            die(
                'Invalid item id received.  Please go back and try again.'
            );
        }//if
       
        $item =& TestItem::find_by_id($item_id);
        if ( !$item ) {
            die(
                'We couldn\'t find the item to which this assignment is '.
                'supposed to be linked.  Please try again.'
            );
        } else {
            // get the type of assignment review form to use, defaulting to 
            // DEFAULT_ASSIGNMENT_REVIEW 
            $project =& $item->get_project();
            $review_type = ($project) ? 
                $project->get_assignment_review_type() :
                DEFAULT_ASSIGNMENT_REVIEW;

            // build and return the appropriate type of review form
            switch($review_type) {
                case SINGLETON_ASSIGNMENT_REVIEW:
                    return new SingletonAssignmentTestItemReviewForm($item_id);
                case DEFAULT_ASSIGNMENT_REVIEW:
                    return new AssignmentTestItemReviewForm($item_id);
            }//switch
        }//else

        return false;
    }//get_review_form
}//class AssignmentTestItemReviewFormController

?>
