<?php
require_once('phputil/classes/contactus.inc');
require_once('phputil/classes/FisdapLogger.inc');
require_once('phputil/exceptions/FisdapExceptionUtils.inc');
require_once('phputil/exceptions/FisdapRuntimeException.inc');

/**
 * Handle errors that occur unexpectedly.
 * The <code>init()</code> method should be called once to set up the trapping
 * of unhandled errors.
 * After appropriately handling the problem, the program will die.
 */
final class FisdapErrorHandler {

	/**
	 * Throw any PHP errors that would normally be reported as exceptions 
	 * instead.
	 *
	 * <b>Only for development use.</b>
	 *
	 * This is useful if you would like more information than what the error 
	 * provides, like a full stack 
	 * trace. Be aware that execution will halt on the first uncaught 
	 * exception.
	 */
	public static $errors_as_exceptions = false;

    /**
     * Initialize error handling.
     */
    public static function init() {
        set_exception_handler(array(__CLASS__, 'handle_exception'));
        set_error_handler(array(__CLASS__, 'handle_error'));

        assert_options(ASSERT_ACTIVE, 1);
        assert_options(ASSERT_BAIL, 0);
        assert_options(ASSERT_CALLBACK, array(__CLASS__, 'handle_assertion'));
        assert_options(ASSERT_QUIET_EVAL, 0);
        assert_options(ASSERT_WARNING, 0);
    }

    /**
     * Handle an exception and then die.
     * @param Exception $exception The underlying exception.
     */
    public static function handle_exception($exception) {
		$logger = FisdapLogger::get_logger();
		$logger->exception($exception);
    }

    /**
     * Handle an error and then die.
     * @param int $level The error level.
     * @param string $text The error text.
     * @param string $file The file where the error occurred.
     * @param int $line The line number where the error occurred.
     * @param array $context The symbol table in use at the time of the error.
     * @throws FisdapRuntimeException
     */
    public static function handle_error($level, $text, $file, $line, $context) {
        // If this appears to come from a statement that had a '@' prepended,
        // simply ignore the error.
        $error_reporting = (int) ini_get('error_reporting');
        if (!$error_reporting) return;
		// If error reporting for this level is turned off, ignore it
		if (!($error_reporting & $level)) return;

		if (self::$errors_as_exceptions) {
			throw new ErrorException($text, 0, $level, $file, $line);
			return;
		}

		// Choose the level we want to log at
		if ($level & (E_NOTICE | E_WARNING | E_USER_NOTICE | E_USER_WARNING)) {
			$new_level = FisdapLogger::WARN;
		} else {
			$new_level = FisdapLogger::ERR;
		}

		// Log this error
		$logger = FisdapLogger::get_logger();
		$logger->log($text, $new_level, array('trace_offset' => 1));

		// If it was serious, bail
		if (!($level & (E_NOTICE | E_WARNING | E_USER_NOTICE | E_USER_WARNING))) {
			exit(1);
		}
    }

    /**
     * Handle an assertion that failed and then die.
     * @param string $file The file where the assertion failed.
     * @param int $line The line number where the assertion failed.
     * @param string|null $condition The text containing the condition.
     * @throws FisdapRuntimeException
     */
    public static function handle_assertion($file, $line, $condition) {
        $msg = 'Assertion failed';

        if (!is_null($condition) && (trim($condition) != '')) {
            $msg .= ": $condition";
        }

        throw new FisdapRuntimeException($msg);
    }
}
?>
