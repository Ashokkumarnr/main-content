<?php

require_once('phputil/bls_skill_constants.php');

/**
 * This class models a FISDAP Assessment entry
 *
 */
class FISDAP_Assessment extends DataEntryDbObj {    

    /** 
     * Create a new Assessment with the specified id and the specified
     * db link
     */
    function FISDAP_Assessment( $id, $connection ) {
        $this->pda_id_field = 'Assess_id';
        $this->DbObj(
            'AssesmentData',          //table name
            $id,                      //id
            'Asses_id',               //id field
            'Shift',                  //parent entity name
            array('EvalSession'),     //children
            $connection               //database connection
        ); 

        // @todo: what about "Syncopal" and "EntryTime" in MySQL ?
        $field_map = array( 
            'Shift_id' => 'Shift_id',
            'Student_id' => 'Student_id',
            'Years' => 'Age',
            'Months' => 'Months',
            'Gender' => 'Gender',
            'Ethnicity' => 'Ethnicity',
            'Diag1' => 'Diagnosis',
            'Diag2' => 'Diag2',
            'LOC' => 'LOC',
            'MOI' => 'MOI',
            'SystolicBP' => 'SystolicBP',
            'DiastolicBP' => 'DiastolicBP',
            'SubjectType_id' => 'SubjectType_id'
        );
        $this->set_field_map( $field_map );
    }//constructor FISDAP_Assessment

    /**
     * fix complaints and bls entries attached to the assessment
     */
    function postprocess_pda_submission() {
        $my_name = $this->my_name();

        //strip the Assess[ID] prefix from the complaint fields, and add
        //the Assess id and a Run id of 0
        $num_complaints = 8;

        //get the regex for finding Complaints 
        $complaint_obj = new FISDAP_Complaint(0,null);
        $complaint_obj->set_pda_postdata($this->pda_postdata);
        $complaint_entity_name = $complaint_obj->my_name();
        $min_id = $complaint_obj->get_next_pda_id();

        // replace the Assess[ID] prefix with the PtComp[ID] prefix
        for( $i = 0 ; $i < $num_complaints ; $i++,$min_id-- ) {
            $complaint_key = $my_name . $this->pda_id . 'Complaint'. $i;
            $new_complaint_key = $complaint_entity_name 
                . $min_id 
                . 'Complaint_id';

            if ( !isset($this->pda_postdata[$complaint_key]) ) continue;
            $complaint_val = $this->pda_postdata[$complaint_key];
            unset($this->pda_postdata[$complaint_key]);

            if ( $complaint_val > 0 ) {
                //only insert the new entry if the complaint was checked
                $this->pda_postdata[$new_complaint_key] = $complaint_val;

                // add the Assess id, Shift id, Student_id, and Run id fields
                $this->pda_postdata[
                    $complaint_entity_name.$min_id.'Shift_id'
                ] = $this->get_field('Shift_id');
                $this->pda_postdata[
                    $complaint_entity_name.$min_id.'Student_id'
                ] = $this->get_field('Student_id');
                $this->pda_postdata[
                    $complaint_entity_name.$min_id.$this->pda_id_field
                ] = $this->pda_id;
                $this->pda_postdata[
                    $complaint_entity_name.$min_id.'Run_id'
                ] = 0;
                $this->pda_postdata[
                    $complaint_entity_name.$min_id.'SubjectType_id'
                ] = $this->get_field('SubjectType_id');
            }//if
        }//for

        $bls_obj = new FISDAP_BLS_Skill(0,NULL); //create a dummy BLS skill to get its attributes
        $bls_obj->set_pda_postdata($this->pda_postdata);
        $bls_entity_name = $bls_obj->my_name();
        $min_id = $bls_obj->get_next_pda_id();
        $bls_skill_modifier = -3;
        $pda_bls_fields = array( 
            'Exam'      => PATIENT_EXAM,
            'Interview' => PATIENT_INTERVIEW
        ); 

        foreach( $pda_bls_fields as $skill_field => $skill_code ) {
            //create a postdata entry compatible with the BLS class
            // performed_by ==  0 -> none,   1 -> perf,   2 -> obs

            $skill_postdata_key = $my_name.$this->pda_id.$skill_field;
            $performed_by = $this->pda_postdata[$skill_postdata_key];
            unset($this->pda_postdata[$skill_postdata_key]); 

            if ( $performed_by ) {
                //if they performed or observed the skill, insert an
                //entry the BLSSkill class can read
                $new_bls_key_base = $bls_entity_name.($min_id--);
                $this->pda_postdata[
                    $new_bls_key_base.'Run_id'
                ] = 0;
                $this->pda_postdata[
                    $new_bls_key_base.'Student_id'
                ] = $this->get_field('Student_id');
                $this->pda_postdata[
                    $new_bls_key_base.'Shift_id'
                ] = $this->get_field('Shift_id');
                $this->pda_postdata[
                    $new_bls_key_base.'Assess_id'
                ] = $this->pda_id;
                $this->pda_postdata[
                    $new_bls_key_base.'SubjectType_id'
                ] = $this->get_field('SubjectType_id');
                $this->pda_postdata[
                    $new_bls_key_base.'SkillCode'
                ] = $skill_code;
                $this->pda_postdata[
                    $new_bls_key_base.'PerformedBy'
                ] = $performed_by;
                $this->pda_postdata[
                    $new_bls_key_base.'SkillModifier'
                ] = $bls_skill_modifier;
            }//if
        }//foreach

        //get rid of any fields we haven't yet dealt with
        parent::postprocess_pda_submission();
    }//postprocess_pda_submission

    
    /**
     *
     */
    function to_mysql_db() {
        // translate a diastolic bp of 'P' to -2
        if ( $this->get_field('DiastolicBP') == 'P' ) {
            $this->set_field('DiastolicBP',-2);
        }//if
        parent::to_mysql_db();
    }//to_mysql_db
}//class FISDAP_Assessment

?>
