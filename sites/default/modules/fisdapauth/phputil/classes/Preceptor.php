<?php 

require_once('phputil/preceptor_utils.php');
require_once('phputil/classes/logger/Logger.php');
require_once('phputil/classes/logger/SyslogOutputLogListener.php');


/**
 * This class models a FISDAP Preceptor entry
 * 
 * @todo: (re)design this class
 */
class FISDAP_Preceptor extends DataEntryDbObj {    
    /** associative array with result set from ProgramPreceptorData */
    var $program_links; 

    /** a Logger to which to send system messages */
    var $logger;


    /** 
     * Create a new Preceptor with the specified id and the specified db link
     */
    function FISDAP_Preceptor( $id, $connection ) {
        $this->pda_id_field = 'Preceptor_id';
        $this->DbObj(
            'PreceptorData',      //table name
            $id,                  //id
            'Preceptor_id',       //id field
            'Shift',              //parent entity name
            array('Run'),         //children
            $connection           //database connection
        );

        $this->logger =& Logger::get_instance();

        $field_map = array( 
            'FirstName'=>'FirstName',
            'LastName'=>'LastName',
            'AmbServ_id'=>'AmbServ_id',
            'HPhone'=>'HPhone',
            'WPhone'=>'WPhone',
            'Pager'=>'Pager',
            'EmailAddress'=>'EmailAddress',
        );
        $this->set_field_map( $field_map );
        $this->program_links = array(); 
    }//constructor FISDAP_Preceptor

    /**
     * Because we need to insert data into two tables (PreceptorData and
     * ProgramPreceptorData), we need to modify the behavior of the
     * stock DbObj.  We also need to check for existing preceptors with
     * the same name, since we don't want people adding duplicate
     * preceptors.
     */
    function to_mysql_db() {
        $this->logger->log(
            'entering Preceptor::to_mysql_db()',
            $this->logger->DEBUG
        );

        // check for an existing entry with the same name
        if ( $existing_preceptor = get_preceptor_by_name( 
                 $this->get_field('FirstName'), 
                 $this->get_field('LastName'), 
                 $this->get_field('AmbServ_id'),
                 $this->connection 
             ) 
           ) {
            // if there's already a preceptor by this name, populate
            // this object from the existing entry
            $this->logger->log(
                'found pre-existing preceptor... populating',
                $this->logger->DEBUG
            );

            // populate the fields from PreceptorData
            $this->web_id = $existing_preceptor['Preceptor_id'];
            foreach( array_keys($this->db_result) as $key ) {
                $this->set_field($key, $existing_preceptor[$key]);
            }//foreach

            // before checking the ProgramPreceptorData, make sure there
            // are entries linking the preceptor to all associated
            // programs
            $this->logger->log('checking program links', $this->logger->DEBUG);
            $num_existing_links = count($existing_preceptor['Programs']);
            $num_program_links = count($this->program_links);
            $new_program_links = array(); 

            $this->logger->log(
                $num_existing_links . ' existing program links found',
                $this->logger->DEBUG
            );

            $this->logger->log(
                $num_program_links . ' new program links found',
                $this->logger->DEBUG
            );

            for( $i=0; $i < $num_program_links ; $i++ ) {
                $program_link = $this->program_links[$i];
                $found_existing_link = false;
                for ( $j=0 ; $j < $num_existing_links ; $j++ ) {
                    $existing_link = $existing_preceptor['Programs'][$j];
                    $this->logger->log(
                        'found link to prog #' . $existing_link['Program_id'],
                        $this->logger->DEBUG
                    );

                    if ( $existing_link['Program_id'] == $program_link[
                             'Program_id'
                         ] 
                       ) {
                        $this->logger->log(
                            'found existing link to prog #' 
                            . $program_link['Program_id'],
                            $this->logger->DEBUG
                        );
                        $found_existing_link = true;
                        break;
                    }//if
                }//for
                if ( !$found_existing_link ) {
                    // we need to insert a link to this program
                    $this->logger->log(
                        'inserting new preceptor link to prog #' 
                        . $program_link['Program_id'],
                        $this->logger->DEBUG
                    );
                    
                    $new_program_links[] = $program_link;
                    link_preceptor_to_program( 
                        $this->web_id, 
                        $program_link['Program_id'], 
                        $program_link['Active'], 
                        $this->connection 
                    ); 
                }//if
            }//for

            // populate the fields from ProgramPreceptorData
            foreach( $existing_preceptor['Programs'] as $program_link ) {
                $new_program_links[] = $program_link;
            }//foreach
            $this->program_links = $new_program_links;
        } else {
            // if there's no existing preceptor by this name, insert new
            // entries
           
            //use the stock DbObj function to insert into PreceptorData
            if ( !parent::to_mysql_db() ) return false;
          
            //insert the ProgramPreceptorData entries
            foreach( $this->program_links as $program_link ) {
                link_preceptor_to_program( 
                    $this->web_id, 
                    $program_link['Program_id'], 
                    $program_link['Active'], 
                    $this->connection 
                ); 
            }//foreach
        }//else

        $this->logger->log(
            'done with Preceptor::to_mysql_db()',
            $this->logger->DEBUG
        );
    }//to_mysql_db


    /**
     * Link this preceptor to the given program, defaulting to an
     * "active" status.  Note that this function does not change the
     * MySQL database.  For changes to take effect, to_mysql_db should
     * be called after calls to this function.
     *
     * @param int a program id (ostensibly a positive integer)
     * @param int (optional) active status (0 => inactive, 1 => active)
     */
    function link_to_program( $program_id, $is_active = 1 ) {
        // make sure there's not already a link to the given program
        $num_program_links = count($this->program_links);
        for( $i=0; $i < $num_program_links ; $i++ ) {
            $program_link = $this->program_links[$i];
            if ( $program_link['Program_id'] == $program_id ) {
                // if there's a link to the given program id, abort
                // @todo: what if the Active settings are different?
                return false;
            }//if
        }//for

        // if there isn't an entry already, add a new one
        $this->program_links[] = array( 'Preceptor_id' => $this->web_id,
                                        'Program_id'   => $program_id,
                                        'Active'       => $is_active );
        return true;
    }//link_to_program

    /**
     * We need to override this method to take the program links into
     * account, as they are external to the db_result array.
     */
    function to_string() {
        return var_export($this->db_result,true).
               "\n".
               var_export($this->program_links,true);
    }//to_string

}//class FISDAP_Preceptor

?>
