<?php
/*---------------------------------------------------------------------*
  |                                                                     |
  |      Copyright (C) 1996-2010.  This is an unpublished work of       |
  |                       Headwaters Software, Inc.                     |
  |                          ALL RIGHTS RESERVED                        |
  |      This program is a trade secret of Headwaters Software, Inc.    |
  |      and it is not to be copied, distributed, reproduced, published |
  |      or adapted without prior authorization                         |
  |      of Headwaters Software, Inc.                                   |
  |                                                                     |
* ---------------------------------------------------------------------*/

require_once('phputil/classes/model/UserAbstractModel.inc');
require_once('phputil/classes/DateTime.inc');
require_once('phputil/classes/model/AccountTypeModel.inc');
require_once('phputil/handy_utils.inc');


class InstructorDataModel extends UserAbstractModel {
	private $instructor_id;
	private $first_name;
	private $last_name;
	private $user_name;
	private $program_id;
	private $email;
	private $email_event_flag;
	private $primary_contact_flag;
	private $email_list_flag;
	private $work_phone;
	private $cell_phone;
	private $pager;    
	private $bigbro_emails_flag;
	private $clinical_bigbro_email;
	private $permissions;
	private $accepted_agreement_flag;
	private $lab_big_bro_emails_flag;
	private $reviewer_flag;
	private $active_reviewer;
	private $reviewer_notes;

	const DB_TABLE = "InstructorData";
	const ID_FIELD = "Instructor_id";

	public function get_user_type() {
		return 'instructor';
	}

	public function __construct($id=null) {
		$ret = self::construct_helper($id, self::DB_TABLE, self::ID_FIELD);
		parent::__construct($id);
		return $ret;
	}

	public function get_fieldmap() {
		return array(
			'FirstName' => 'first_name',
			'LastName' => 'last_name',
			'UserName' => 'user_name',
			'ProgramId' => 'program_id',
			'Email' => 'email',
			'EmailEventFlag' => 'email_event_flag',
			'PrimaryContact' => 'primary_contact_flag',
			'EmailList' => 'email_list_flag',
			'OfficePhone' => 'work_phone',
			'CellPhone' => 'cell_phone',
			'Pager' => 'pager',
			'BigBroEmails' => 'bigbro_emails_flag',
			'ClinicalBigBroEmails' => 'clinical_bigbro_emails_flag',
			'Permissions' => 'permissions',
			'AcceptedAgreement' => 'accepted_agreement_flag',
			'LabBigBroEmails' => 'lab_big_bro_emails_flag',
			'Reviewer' => 'reviewer_flag',
			'ActiveReviewer' => 'active_reviewer',
			'ReviewerNotes' => 'reviewer_notes'
		);
	}

	protected function load_batch_validation_rules() {
		$this->fields = array(
			'first_name' => array(
				'max_len' => 30,
				'is_required' => true,
				'min_len' => 1
			),
			'last_name' => array(
				'max_len' => 30,
				'is_required' => true,
				'min_len' => 1
			),
			'username' => array(
				'max_len' => 24,
				'is_required' => true,
				'min_len' => 5
			),
			'email' => array(
				'max_len' => 50,
				'is_required' => true,
				'min_len' => 7
			)	
		);
	}

	public function set_all_to_defaults() {

	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "InstructorDataModel", self::ID_FIELD);
	}

	public function get_summary() {
		return "Instructor Data Model";
	}

	public static function get_by_username($username) {
		$connection = FISDAPDatabaseConnection::get_instance();

		$query = "SELECT * FROM ".self::DB_TABLE." where `UserName`='$username'";
		$res = $connection->query($query);

		if (count($res)>0) {
			return new self($res[0]);
		} else {
			return null;
		}
	}

	public function set_first_name($val) {
		return $this->set_helper("string", $val, "first_name");
	}

	public function set_last_name($val) {
		return $this->set_helper("string", $val, "last_name");
	}

	public function set_user_name($val) {
		return $this->set_helper("string", $val, "user_name");
	}

	public function set_program_id($val) {
		return $this->set_helper("int", $val, "program_id");
	}

	public function set_email($val) {
		return $this->set_helper("string", $val, "email");
	}

	public function set_email_event_flag($val) {
		return $this->set_helper("int", $val, "email_event_flag");
	}

	public function set_primary_contact_flag($val) {
		return $this->set_helper("int", $val, "primary_contact_flag");
	}

	public function set_email_list_flag($val) {
		return $this->set_helper("int", $val, "email_list_flag");
	}

	public function set_work_phone($val) {
		return $this->set_helper("string", $val, "work_phone");
	}

	public function set_cell_phone($val) {
		return $this->set_helper("string", $val, "cell_phone");
	}

	public function set_pager($val) {
		return $this->set_helper("string", $val, "pager");
	}

	public function set_bigbro_emails_flag($val) {
		return $this->set_helper("string", $val, "bigbro_emails_flag");
	}

	public function set_reviewer_flag($val) {
		return $this->set_helper("int", $val, "reviewer_flag");
	}

	public function set_active_reviewer($val) {
		return $this->set_helper("string", $val, "active_reviewer");
	}

	public function set_reviewer_notes($val) {
		return $this->set_helper("string", $val, "reviewer_notes");
	}
}
?>
