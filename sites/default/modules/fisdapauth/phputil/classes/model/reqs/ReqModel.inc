<?php 
require_once('phputil/classes/Assert.inc'); 
require_once('phputil/classes/model/AbstractModel.inc'); 
require_once('phputil/classes/model/reqs/ReqExpirationStrategyModel.inc'); 
require_once('phputil/classes/model/reqs/ReqStatusModel.inc'); 
require_once('phputil/classes/model/reqs/ReqTrainingModel.inc'); 
require_once('phputil/classes/reqs/ExpirationStrategyFactory.inc'); 

/** 
 * This class models the req_Requirement table.  
 */ 
final class ReqModel extends AbstractModel { 
    const DB_TABLE = 'req_Requirement'; 
    const ID_COLUMN = 'id'; 
    
    protected $creation_date; 
    protected $deleted; 
    protected $description; 
    protected $expiration_date_day_increment; 
    protected $expiration_date_editable; 
    protected $expiration_date_month_increment; 
    protected $expiration_date_required; 
    protected $expiration_date_strategy_id; 
    protected $expiration_date_year_increment; 
    protected $program_id;
    protected $public;
    protected $root_id;
    protected $shared_control;
    protected $status_id;
    protected $title;
    protected $training_id;
    protected $training_url;
    protected $version;

    const CREATION_DATE_FIELD = 'creation_date';
    const DELETED_FIELD = 'deleted';
    const DESCRIPTION_FIELD = 'description';
    const EXPIRATION_DATE_DAY_INCREMENT_FIELD = 'expiration_date_day_increment';
    const EXPIRATION_DATE_EDITABLE_FIELD = 'expiration_date_editable';
    const EXPIRATION_DATE_MONTH_INCREMENT_FIELD = 'expiration_date_month_increment';
    const EXPIRATION_DATE_REQUIRED_FIELD = 'expiration_date_required';
    const EXPIRATION_DATE_STRATEGY_ID_FIELD = 'expiration_date_strategy_id';
    const EXPIRATION_DATE_YEAR_INCREMENT_FIELD = 'expiration_date_year_increment';
    const PROGRAM_ID_FIELD = 'program_id';
    const PUBLIC_FIELD = 'public';
    const ROOT_ID_FIELD = 'root_id';
    const SHARED_CONTROL_FIELD = 'shared_control';
    const STATUS_ID_FIELD = 'status_id';
    const TITLE_FIELD = 'title';
    const TRAINING_ID_FIELD = 'training_id';
    const TRAINING_URL_FIELD = 'training_url';
    const VERSION_FIELD = 'version';

	public function __construct($data=null) {
        $this->construct_helper($data, self::DB_TABLE, self::ID_COLUMN);
    }

    /**
     * Retrieve the creation date.
     * @return FisdapDateTime The date / time.
     */
    public function get_creation_date() {
        return FisdapDateTime::create_from_sql_string($this->creation_date);
    }

    /**
     * Set the due date.
     * @param FisdapDateTime $date_time The date / time.
     */
    public function set_creation_date(FisdapDateTime $date_time) {
        $this->set_helper('datetime', $date_time, 'creation_date');
    }

	/**
	 * Determine if the user requirement has been deleted.
	 * @return int | null The deleted, 0 for not, otherwise deleted.
	 */
	public function get_deleted() {
        return $this->deleted;
    }

	/**
	 * Determine if the user requirement has been deleted.
	 * @return boolean | null TRUE if deleted.
	 */
	public function is_deleted() {
        return ($this->deleted != 0);
    }

	/**
	 * Indicate whether the user requirement is deleted.
	 * @param boolean $deleted TRUE if deleted.
	 */
	public function set_deleted($deleted) {
        Assert::is_boolean($deleted);

        $this->set_helper('bool', $deleted, 'deleted');
    }

    /**
     * Retrieve the description.
     * @return string | null The description.
     */
    public function get_description() {
        return $this->description;
    }

    /**
     * Set the description.
     * @param string $description The description.
     */
    public function set_description($description) {
        Assert::is_not_empty_trimmed_string($description);

        $this->set_helper('string', trim($description), 'description');
    }

    /**
     * Retrieve the expiration date day increment.
     * @return int | null The expiration date day increment.
     */
    public function get_expiration_date_day_increment() {
        return $this->expiration_date_day_increment;
    }

    /**
     * Set the expiration date day increment.
     * @param int | null  $increment The expiration date day increment.
     */
    public function set_expiration_date_day_increment($increment) {
        Assert::is_true(Test::is_int($increment) || Test::is_null($increment));

        $this->set_helper('int', $increment, 'expiration_date_day_increment');
    }

    /**
     * Retrieve the expiration date editable status.
     * @return boolean | null The expiration date editable status.
     */
    public function get_expiration_date_editable() {
        return $this->expiration_date_editable;
    }

    /**
     * Determine if the expiration date is editable.
     * @return boolean TRUE if the date is editable.
     */
    public function is_expiration_date_editable() {
        return ($this->expiration_date_editable != 0);
    }

    /**
     * Set the expiration date editable status.
     * @param boolean $editable TRUE if the date is editable.
     */
    public function set_expiration_date_editable($editable) {
        Assert::is_boolean($editable);

        $this->set_helper('bool', $editable, 'expiration_date_editable');
    }

    /**
     * Retrieve the expiration date month increment.
     * @return int | null The expiration date month increment.
     */
    public function get_expiration_date_month_increment() {
        return $this->expiration_date_month_increment;
    }

    /**
     * Set the expiration date month increment.
     * @param int | null  $increment The expiration date month increment.
     */
    public function set_expiration_date_month_increment($increment) {
        Assert::is_true(Test::is_int($increment) || Test::is_null($increment));

        $this->set_helper('int', $increment, 'expiration_date_month_increment');
    }

    /**
     * Retrieve the expiration date required status.
     * @return boolean | null The expiration date required status.
     */
    public function get_expiration_date_required() {
        return $this->expiration_date_required;
    }

    /**
     * Determine if the expiration date is required.
     * @return boolean TRUE if the date is required.
     */
    public function is_expiration_date_required() {
        return ($this->expiration_date_required != 0);
    }

    /**
     * Set the expiration date required status.
     * @param boolean $required TRUE if the date is required.
     */
    public function set_expiration_date_required($required) {
        Assert::is_boolean($required);

        $this->set_helper('bool', $required, 'expiration_date_required');
    }

	/**
	 * Retrieve the expiration date strategy as an object.
	 * This expiration strategy ID must be valid.
	 * @return ExpirationDateStrategy The strategy.
	 */
	public function get_expiration_date_strategy() {
		$strategy_id = $this->get_expiration_date_strategy_id();

		Assert::is_int($strategy_id);

		$model = new ReqExpirationStrategyModel($this->get_expiration_date_strategy_id());

		$strategy = ExpirationStrategyFactory::create($model->get_class_name());
		$strategy->set_day_increment($this->get_expiration_date_day_increment()); 
		$strategy->set_month_increment($this->get_expiration_date_month_increment()); 
		$strategy->set_year_increment($this->get_expiration_date_year_increment()); 
		return $strategy;
	}

	/**
	 * Retrieve the expiration date strategy ID.
	 * @return int | null A reference to the req_ExpirationStrategy table.
	 */
	public function get_expiration_date_strategy_id() {
        return $this->expiration_date_strategy_id;
    }

	/**
	 * Set the expiration date strategy ID.
	 * @param int $id A reference to the req_ExpirationStrategy table.
	 */
	public function set_expiration_date_strategy_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'expiration_date_strategy_id');
    }

    /**
     * Retrieve the expiration date year increment.
     * @return int | null The expiration date year increment.
     */
    public function get_expiration_date_year_increment() {
        return $this->expiration_date_year_increment;
    }

    /**
     * Set the expiration date year increment.
     * @param int | null  $increment The expiration date year increment.
     */
    public function set_expiration_date_year_increment($increment) {
        Assert::is_true(Test::is_int($increment) || Test::is_null($increment));

        $this->set_helper('int', $increment, 'expiration_date_year_increment');
    }

	/**
	 * Retrieve the program ID.
	 * @return int | null A reference to the ProgramData table.
	 */
	public function get_program_id() {
        return $this->program_id;
    }

	/**
	 * Set the program ID.
	 * @param int $id A reference to the ProgramData table.
	 */
	public function set_program_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'program_id');
    }

	/**
	 * Determine if the requirement is public.
	 * @return int | null The public status, 0 for not, otherwise public.
	 */
	public function get_public() {
        return $this->public;
    }

	/**
	 * Determine if the requirement is public.
	 * @return boolean | null TRUE if public.
	 */
	public function is_public() {
        return ($this->public != 0);
    }

	/**
	 * Indicate whether the requirement is public.
	 * @param boolean $public TRUE if public.
	 */
	public function set_public($public) {
        Assert::is_boolean($public);

        $this->set_helper('bool', $public, 'public');
    }

	/**
	 * Retrieve the root ID.
	 * @return int | null A reference to the req_Requirement table.
	 */
	public function get_root_id() {
        return $this->root_id;
    }

	/**
	 * Set the root ID.
	 * Pass &lt; 0 is this is the root element.
	 * @param int $id A reference to the req_Requirement table.
	 */
	public function set_root_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'root_id');
    }

	/**
	 * Determine if the requirement under shared control.
	 * @return int | null The shared status, 0 for not, otherwise shared control.
	 */
	public function get_shared_control() {
        return $this->shared_control;
    }

	/**
	 * Determine if the requirement is under shared control.
	 * @return boolean | null TRUE if shared control.
	 */
	public function is_shared_control() {
        return ($this->shared_control != 0);
    }

	/**
	 * Indicate whether the requirement is under shared control.
	 * @param boolean $shared_control TRUE if shared control.
	 */
	public function set_shared_control($shared_control) {
        Assert::is_boolean($shared_control);

        $this->set_helper('bool', $shared_control, 'shared_control');
    }

	/**
	 * Retrieve the status ID.
	 * @return int | null A reference to the req_Status table.
	 */
	public function get_status_id() {
        return $this->status_id;
    }

	/**
	 * Set the status ID.
	 * @param int $id A reference to the req_Status table.
	 */
	public function set_status_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'status_id');
    }

    /**
     * Retrieve the title.
     * @return string | null The title.
     */
    public function get_title() {
        return $this->title;
    }

    /**
     * Set the title.
     * @param string $title The title.
     */
    public function set_title($title) {
        Assert::is_not_empty_trimmed_string($title);

        $this->set_helper('string', trim($title), 'title');
    }

	/**
	 * Retrieve the training ID.
	 * @return int | null A reference to the req_Training table.
	 */
	public function get_training_id() {
        return $this->training_id;
    }

	/**
	 * Set the training ID.
	 * @param int $id A reference to the req_Training table.
	 */
	public function set_training_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'training_id');
    }

    /**
     * Retrieve the training URL.
     * @return string | null The training URL.
     */
    public function get_training_url() {
        return $this->training_url;
    }

    /**
     * Set the training URL.
     * @param string $url The training URL.
     */
    public function set_training_url($url) {
        Assert::is_string($url);

        $this->set_helper('string', trim($url), 'training_url');
    }

    /**
     * Retrieve the version.
     * @return int | null The version.
     */
    public function get_version() {
        return $this->version;
    }

    /**
     * Set the version.
     * @param int $version The version.
     */
    public function set_version($version) {
        Assert::is_int($version);
        Assert::is_true($version > 0);

        $this->set_helper('int', $version, 'version');
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, self::ID_COLUMN);
    }

    public function get_summary() {
        return 'Req[' . $this->get_id() . ']';
    }

    public function get_fieldmap() {
        return array(
            'CreationDate' => self::CREATION_DATE_FIELD,
            'Deleted' => self::DELETED_FIELD,
            'Description' => self::DESCRIPTION_FIELD,
            'ExpirationDayIncrement' => self::EXPIRATION_DATE_DAY_INCREMENT_FIELD,
            'ExpirationEditable' => self::EXPIRATION_DATE_EDITABLE_FIELD,
            'ExpirationMonthIncrement' => self::EXPIRATION_DATE_MONTH_INCREMENT_FIELD,
            'ExpirationRequired' => self::EXPIRATION_DATE_REQUIRED_FIELD,
            'ExpirationStrategy' => self::EXPIRATION_DATE_STRATEGY_ID_FIELD,
            'ExpirationYearIncrement' => self::EXPIRATION_DATE_YEAR_INCREMENT_FIELD,
            'Program_id' => self::PROGRAM_ID_FIELD,
            'Public' => self::PUBLIC_FIELD,
            'RootId' => self::ROOT_ID_FIELD,
            'SharedControl' => self::SHARED_CONTROL_FIELD,
            'Status' => self::STATUS_ID_FIELD,
            'Title' => self::TITLE_FIELD,
            'Training' => self::TRAINING_ID_FIELD,
            'TrainingURL' => self::TRAINING_URL_FIELD,
            'Version' => self::VERSION_FIELD
        );
    }
}
?>
