<?php
require_once('phputil/classes/model/AbstractEnumeratedModel.inc');

/**
 * This class models the reqs_Fulfillment table.
 */
final class ReqFulfillmentModel extends AbstractEnumeratedModel {
    const DB_TABLE = 'req_Fulfillment';

    const ID_IDENTIFIED = 1;
    const ID_IDENTIFIED_OR_COMPARABLE = 2;
    const ID_IDENTIFIED_OR_APPROVED_COMPARABLE = 3;

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    /**
     * Determine if this is identified.
     * @return boolean TRUE if this is identified.
     */
    public function is_identified() {
        return $this->get_id() == self::ID_IDENTIFIED;
    }

    /**
     * Determine if this is identified or comparable.
     * @return boolean TRUE if this is identified or comparable.
     */
    public function is_identified_or_comparable() {
        return $this->get_id() == self::ID_IDENTIFIED_OR_COMPARABLE;
    }

    /**
     * Determine if this is identified or approved comparable.
     * @return boolean TRUE if this is identified or approved comparable.
     */
    public function is_identified_or_approved_comparable() {
        return $this->get_id() == self::ID_IDENTIFIED_OR_APPROVED_COMPARABLE;
    }

    /**
     * Retrieve information to create prompts.
     * @return array A list of req_RequiredModel sorted by ascending position.
     */
    public static function get_prompt_info() {
        return self::do_get_prompt_info(__CLASS__);
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'ReqFulfillment[' . $this->get_id() . ']';
    }

    public static function get_special_ids() {
        return array(
            'ID_IDENTIFIED' => self::ID_IDENTIFIED,
            'ID_IDENTIFIED_OR_APPROVED_COMPARABLE' => self::ID_IDENTIFIED_OR_APPROVED_COMPARABLE,
            'ID_IDENTIFIED_OR_COMPARABLE' => self::ID_IDENTIFIED_OR_COMPARABLE
        );
    }
}
?>
