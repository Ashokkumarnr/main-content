<?php
require_once('phputil/classes/model/AbstractEnumeratedModel.inc');

/**
 * This class models the reqs_Required table.
 */
final class ReqRequiredModel extends AbstractEnumeratedModel {
    const DB_TABLE = 'req_Required';

    const ID_OPTIONAL = 1;
    const ID_REQUIRED = 2;
    const ID_REQUIRED_BEFORE_SHIFT = 3;
    const ID_REQUIRED_BEFORE_SHIFT_SIGNUP = 4;

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    /**
     * Determine if this is optional.
     * @return boolean TRUE if this is optional.
     */
    public function is_optional() {
        return $this->get_id() == self::ID_OPTIONAL;
    }

    /**
     * Determine if this is required.
     * @return boolean TRUE if this is required.
     */
    public function is_required() {
        return $this->get_id() == self::ID_REQUIRED;
    }

    /**
     * Determine if this is required before running a shift.
     * @return boolean TRUE if this is required before running a shift.
     */
    public function is_required_before_shift() {
        return $this->get_id() == self::ID_REQUIRED_BEFORE_SHIFT;
    }

    /**
     * Determine if this is required before signing up for a shift.
     * @return boolean TRUE if this is required before signing up for a shift.
     */
    public function is_required_before_shift_signup() {
        return $this->get_id() == self::ID_REQUIRED_BEFORE_SHIFT_SIGNUP;
    }

    /**
     * Retrieve information to create prompts.
     * @return array A list of req_RequiredModel sorted by ascending position.
     */
    public static function get_prompt_info() {
        return self::do_get_prompt_info(__CLASS__);
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'ReqRequired[' . $this->get_id() . ']';
    }

    public static function get_special_ids() {
        return array(
            'ID_OPTIONAL' => self::ID_OPTIONAL,
            'ID_REQUIRED' => self::ID_REQUIRED,
            'ID_REQUIRED_BEFORE_SHIFT' => self::ID_REQUIRED_BEFORE_SHIFT,
            'ID_REQUIRED_BEFORE_SHIFT_SIGNUP' => self::ID_REQUIRED_BEFORE_SHIFT_SIGNUP
        );
    }
}
?>
