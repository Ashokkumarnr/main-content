<?php
require_once('phputil/classes/model/AbstractEnumeratedModel.inc');

/**
 * This class models the reqs_Status table.
 */
final class ReqStatusModel extends AbstractEnumeratedModel {
    const DB_TABLE = 'req_Status';

    const ID_ACTIVE = 1;
    const ID_ARCHIVED = 2;

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    /**
     * Determine if this is active.
     * @return boolean TRUE if this is active.
     */
    public function is_active() {
        return $this->get_id() == self::ID_ACTIVE;
    }

    /**
     * Determine if this is required.
     * @return boolean TRUE if this is required.
     */
    public function is_archived() {
        return $this->get_id() == self::ID_ARCHIVED;
    }

    /**
     * Retrieve information to create prompts.
     * @return array A list of req_RequiredModel sorted by ascending position.
     */
    public static function get_prompt_info() {
        return self::do_get_prompt_info(__CLASS__);
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'ReqStatus[' . $this->get_id() . ']';
    }

    public static function get_special_ids() {
        return array(
            'ID_ACTIVE' => self::ID_ACTIVE,
            'ID_ARCHIVED' => self::ID_ARCHIVED
        );
    }
}
?>
