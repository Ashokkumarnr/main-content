<?php
require_once('phputil/classes/model/AbstractEnumeratedModel.inc');

/**
 * This class models the reqs_Training table.
 */
final class ReqTrainingModel extends AbstractEnumeratedModel {
    const DB_TABLE = 'req_Training';

    const ID_INSTRUCTOR_REVIEWED = 1;
    const ID_ON_THE_JOB = 2;
    const ID_ONLINE = 3;

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    /**
     * Determine if this is instructor reviewed.
     * @return boolean TRUE if this is instructor reviewed.
     */
    public function is_instructor_reviewed() {
        return $this->get_id() == self::ID_INSTRUCTOR_REVIEWED;
    }

    /**
     * Determine if this is on the job.
     * @return boolean TRUE if this is on the job.
     */
    public function is_on_the_job() {
        return $this->get_id() == self::ID_ON_THE_JOB;
    }

    /**
     * Determine if this is online.
     * @return boolean TRUE if this is online.
     */
    public function is_online() {
        return $this->get_id() == self::ID_ONLINE;
    }

    /**
     * Retrieve information to create prompts.
     * @return array A list of req_RequiredModel sorted by ascending position.
     */
    public static function get_prompt_info() {
        return self::do_get_prompt_info(__CLASS__);
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'ReqTraining[' . $this->get_id() . ']';
    }

    public static function get_special_ids() {
        return array(
            'ID_INSTRUCTOR_REVIEWED' => self::ID_INSTRUCTOR_REVIEWED,
            'ID_ON_THE_JOB' => self::ID_ON_THE_JOB,
            'ID_ONLINE' => self::ID_ONLINE
        );
    }
}
?>
