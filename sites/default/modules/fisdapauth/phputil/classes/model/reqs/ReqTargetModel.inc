<?php
require_once('phputil/classes/model/AbstractEnumeratedModel.inc');

/**
 * This class models the reqs_Target table.
 */
final class ReqTargetModel extends AbstractEnumeratedModel {
    const DB_TABLE = 'req_Target';

    const ID_ALL_STUDENTS = 1;
    const ID_ALL_INSTRUCTORS = 2;
    const ID_PROGRAM_STUDENTS = 4;
    const ID_PROGRAM_INSTRUCTORS = 8;

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    private static function should_include($mask, $id) {
        Assert::is_int($mask);
        Assert::is_int($id);

        return !!($mask & $id);
    }

    /**
     * Determine if this is all students.
     * @return boolean TRUE if this is all students.
     */
    public function is_all_students() {
        return $this->get_id() == self::ID_ALL_STUDENTS;
    }

    /**
     * Determine if this is all instructors.
     * @return boolean TRUE if this is all instructors.
     */
    public function is_all_instructors() {
        return $this->get_id() == self::ID_ALL_INSTRUCTORS;
    }

    /**
     * Determine if this is the program students.
     * @return boolean TRUE if this is the program students.
     */
    public function is_program_students() {
        return $this->get_id() == self::ID_PROGRAM_STUDENTS;
    }

    /**
     * Determine if this is program instructors.
     * @return boolean TRUE if this is program instructors.
     */
    public function is_program_instructors() {
        return $this->get_id() == self::ID_PROGRAM_INSTRUCTORS;
    }

    /**
     * Determine if all students are included.
     * @param int $mask The bit mask to test.
     * @return boolean TRUE if all students are included.
     */
    public static function include_all_students($mask) {
        return self::should_include($mask, self::ID_ALL_STUDENTS);
    }

    /**
     * Determine if this is all instructors.
     * @param int $mask The bit mask to test.
     * @return boolean TRUE if this is all instructors.
     */
    public static function include_all_instructors($mask) {
        return self::should_include($mask, self::ID_ALL_INSTRUCTORS);
    }

    /**
     * Determine if this is the program students.
     * @param int $mask The bit mask to test.
     * @return boolean TRUE if this is the program students.
     */
    public static function include_program_students($mask) {
        return self::should_include($mask, self::ID_PROGRAM_STUDENTS);
    }

    /**
     * Determine if this is program instructors.
     * @param int $mask The bit mask to test.
     * @return boolean TRUE if this is program instructors.
     */
    public static function include_program_instructors($mask) {
        return self::should_include($mask, self::ID_PROGRAM_INSTRUCTORS);
    }

    /**
     * Retrieve information to create prompts.
     * @return array A list of req_RequiredModel sorted by ascending position.
     */
    public static function get_prompt_info() {
        return self::do_get_prompt_info(__CLASS__);
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'ReqTarget[' . $this->get_id() . ']';
    }

    public static function get_special_ids() {
        return array(
            'ID_ALL_STUDENTS' => self::ID_ALL_STUDENTS,
            'ID_PROGRAM_STUDENTS' => self::ID_PROGRAM_STUDENTS,
            'ID_ALL_INSTRUCTORS' => self::ID_ALL_INSTRUCTORS,
            'ID_PROGRAM_INSTRUCTORS' => self::ID_PROGRAM_INSTRUCTORS
        );
    }
}
?>
