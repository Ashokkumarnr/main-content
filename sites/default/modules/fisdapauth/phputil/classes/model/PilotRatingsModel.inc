<?php

require_once("RateDataModel.inc");
require_once("ModelFactory.inc");

/**
 * Model to group together pilot_ratings tied to a particular run or assessment
 *
 * @TODO Needs Unit Tests.
 */

class PilotRatings {

	protected $ratings;
	protected $shift_id;
	protected $linked_table;
	protected $link_id;
	
	public function __construct($link_id = null, $linked_table = null) {

		if ($linked_table && $link_id) {
			$pilot_ratings = ModelFactory::getRatingsByLinkId($link_id, $linked_table);
			foreach ($pilot_ratings as $pilot_rating) {
				$this->ratings[$pilot_rating->get_rater()][$pilot_rating->get_type()] = $pilot_rating;
			}
	
			$this->set_link_id($link_id);
			$this->set_linked_table($linked_table);
		} else {
			$types = PilotRating::get_type_names();
			$raters = PilotRating::get_rater_names();
			foreach ($types as $t_id=>$type) {

				foreach ($raters as $r_id=>$rater) {
					$pilot_rating = new PilotRating();
					$pilot_rating->set_type($t_id);
					$pilot_rating->set_rater($r_id);

					$this->ratings[$r_id][$t_id] = $pilot_rating;
				}
			}
		}
	}

	public function set_shift_id($id) {
		Assert::is_int($id);

		$this->shift_id = $id;
	}

	public function set_link_id($id) {
		Assert::is_int($id);
		
		$this->link_id = $id;
	}

	public function set_linked_table($table) {
		Assert::is_string($table);

		$this->linked_table = $table;
	}

	/**
	 * method to set the rating and rater of a particular type
	 *
	 * @param $type int the type of rating
	 * @param $rating int the rating
	 * @rater $rater int the type of rater
	 */
	public function set_rating($type, $rating, $rater) {
		Assert::is_int($type);
		Assert::is_int($rating);
		Assert::is_int($rater);
		//Assert::is_true(in_array($type, array_keys($this->ratings)));

		$this->ratings[$rater][$type]->set_rating($rating);
	}

	public function get_shift_id() {
		return $this->shift_id;
	}

	public function get_link_id() {
		return $this->link_id;
	}

	public function get_linked_table() {
		return $this->linked_table;
	}

	public function get_ratings() {
		return $this->ratings;
	}

	public function remove() {
		$ratings = HandyArrayUtils::flatten_values($this->get_ratings());

		foreach ($ratings as $rating) {
			$rating->remove();
		}
	}

	public function save() {
		$ratings = HandyArrayUtils::flatten_values($this->get_ratings());

		foreach ($ratings as $rating) {
			$rating->set_shift_id($this->get_shift_id());
			$rating->set_link_id($this->get_link_id());
			$rating->set_linked_table($this->get_linked_table());
			$rating->save();
		}

	}




















}
?>
