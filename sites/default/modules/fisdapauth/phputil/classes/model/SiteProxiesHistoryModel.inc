<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/model/AbstractHistoryModel.inc');

/**
 * This class models the SiteProxiesHistory table in the DB.
 */
final class SiteProxiesHistoryModel extends AbstractHistoryModel {
    protected $site_proxies_id;

    const DB_TABLE = 'SiteProxiesHistory';

    const SITE_PROXIES_ID_FIELD = 'site_proxies_id';

    /**
     * Constructor.
     * @param int|array|null $data The ID, the SQL row, or null for a new object.
     */
    public function __construct($data=null) {
        parent::__construct($data, self::DB_TABLE);
    }

    /**
     * Retrieve the site proxies ID.
     * @return int A reference into the SiteProxies table.
     */
    public function get_site_proxies_id() {
        return $this->site_proxies_id;
    }

    /**
     * Set the site proxies ID.
     * @param int $id A reference into the SiteProxies table.
     */
    public function set_site_proxies_id($id) {
        Assert::is_int($id);
		$this->set_helper('int', $id, 'site_proxies_id');
    }

    /**
     * Retrieve the objects from a SQL result set.
     * @param array of rows $results The SQL results.
     * @return array The objects.
     */
    public static function getBySQL($results) {
        return parent::sql_helper($results, __CLASS__, parent::ID_COLUMN);
    }

    public function get_summary() {
        return 'SiteProxiesHistory[' . $this->get_id() . ']';
    }

    public function get_fieldmap() {
        $map = array(
            'SiteProxies_id' => self::SITE_PROXIES_ID_FIELD
        );

        return array_merge($map, parent::get_fieldmap());
    }

    /**
     * Retrieve all the history entries.
     * @return array A list of objects sorted on entry time ascending. 
     */
    public static function get_all_entries() {
        return parent::do_get_all_entries(__CLASS__);
    }

	/**
	 * Retrive all the history entries by ID.
	 * @param int $id The ID.
     * @return array A list of objects sorted on entry time ascending. 
	 */
	public static function get_all_by_site_proxies_id($id) {
		Assert::is_int($id);
		
        return parent::do_get_all_by_id(__CLASS__, self::SITE_PROXIES_ID_FIELD, $id);
	}
}
?>
