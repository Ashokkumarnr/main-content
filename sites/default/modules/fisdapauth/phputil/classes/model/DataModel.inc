<?php

/*----------------------------------------------------------------------*
  |                                                                      |
  |     Copyright (C) 1996-2006.  This is an unpublished work of         |
  |                      Headwaters Software, Inc.                       |
  |                         ALL RIGHTS RESERVED                          |
  |     This program is a trade secret of Headwaters Software, Inc.      |
  |     and it is not to be copied, distributed, reproduced, published,  |
  |     or adapted without prior authorization                           |
  |     of Headwaters Software, Inc.                                     |
  |                                                                      |
 *----------------------------------------------------------------------*/

require_once("AbstractModel.inc");

/*
 * Provides additional functionality to models that will need to refer to a shift, 
 * run, student, subject, and performed by. Generally, this object is intended to 
 * be used with tracking-type objects like IV, BLS Skill, etc.
 *
 * In addition to the fields and methods inherited from AbstractModel, this object:
 *
 * Provides the following fields:
 *		id, performed, subject, shift_id, run_id, student_id
 *
 * Provides the following methods:
 *		set: performed_by, subject_type, shift_id, run_id, student_id
 *		static: get_subject_types
 *
 */

abstract class DataModel extends AbstractModel {

	protected $performed_by;
	protected $subject_type;
	protected $shift_id;
	protected $run_id;
	protected $student_id;
	protected $assessment_id;

	function __construct() {
		//Defaults, many of which will probably be overwritten by the inheriting 
		//class. Don't depend on these!
		$this->set_helper('int', -1, 'student_id');
		$this->set_helper('int', -1, 'shift_id');
		$this->set_helper('int', -1, 'run_id');
		$this->set_helper('int', 0, 'performed_by');
		$this->set_helper('int', -1, 'assessment_id');
	}

	/**
	 * @param mixed $_performed Must be 0, 1, "student", or "team"
	 * @return boolean
	 * @TODO untested
	 */
	public function set_performed_by($setting) {
		$id = self::id_or_string_helper(strtolower($setting), "get_performed_states");
		return $this->set_helper("int", $id, "performed_by");
	}

	public function set_run_id($run) {
		//if ($run > 0) {
			return $this->set_helper("int", $run, "run_id");
		//}
		//return false;
	}

	public function set_shift_id($shift) {
		if ($shift > 0) {
			return $this->set_helper("int", $shift, "shift_id");
		}
		return false;
	}

	public function set_student_id($student) {
		if ($student > 0) {
			return $this->set_helper("int", $student, "student_id");
		}
		return false;
	}

	public function set_assessment_id($assessment_id) {
		if (is_numeric($assessment_id)) {
			return $this->set_helper('int', $assessment_id, 'assessment_id');
		}

		return false;
	}

	/**
	 * @param $type mixed An int or a string that corresponds to a type defined in the SubjectTypeTable.
	 * @TODO untested
	 */
	public function set_subject_type($type) {
		$id = $this->id_or_string_helper($type, "get_subject_types");
		return $this->set_helper("int", $id, "subject_type");
		//STUB do we need to adjust for the manikin vs non manikin shifts?
	}

	public function get_subject_type_desc() {
		$types = self::get_subject_types();

		return $types[$this->get_subject_type()];
	}

	public function get_performed_by_desc() {
		$states = self::get_performed_states();

		return ucfirst($states[$this->get_performed_by()]);
	}

	/**
	 * Returns an array that defines the values representing who performed a skill.
	 * @TODO untested
	 */
	public static function get_performed_states() {
		return array (0 => "student", 1 => "team");
	}

	/**
	 * Returns an array of subject types as defined in the database.
	 * @TODO untested
	 */
	public static function get_subject_types() {
		if (!$_GLOBAL["subject_types_cache"]) $_GLOBAL["subject_types_cache"] = self::data_table_helper("SubjectTypeTable", "SubjectType_id", "SubjectTypeName", "SubjectType_id");
		return $_GLOBAL["subject_types_cache"];
	}

	/**
	 * Returns an array of the subject type groups.
	 * @TODO untested
	 */
	public static function get_subject_type_groups() {
		return array("Human - Live", "Human - Cadaver", "Animal - Live", "Animal - Cadaver", "Manikin");
	}

	public static function get_subject_type_group($id) {
		$query = "SELECT * FROM SubjectTypeTable WHERE SubjectType_id = $id";
		$connection = FISDAPDatabaseConnection::get_instance();
		$result = $connection->query($query);

		return $result[0]['SubjectTypeGroup'];
	}

	/**
	 * Returns an array of arrays like 
	 * Array(SubjectTypeGroup1 => Array(SubjectType1, SubjectType2), SubjectTypeGroup2 => ...)
	 * @TODO untested
	 */
	public static function get_subject_types_by_group() {
		if (!$_GLOBAL["subject_types_by_group_cache"]) {
			$query = "SELECT * FROM SubjectTypeTable ORDER BY SubjectTypeGroup";
			$connection =& FISDAPDatabaseConnection::get_instance();
			$rows = $connection->query($query);
			$return = array();
			foreach ($rows as $row) {
				$return[$row["SubjectTypeGroup"]][$row["SubjectType_id"]] = $row["SubjectTypeName"];
			}
			$_GLOBAL["subject_types_by_group_cache"] = $return;
		}
		return $_GLOBAL["subject_types_by_group_cache"];
	}
}

?>
