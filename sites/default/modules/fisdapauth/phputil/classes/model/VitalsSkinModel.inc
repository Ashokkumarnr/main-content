<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/

require_once("AbstractModel.inc");

class VitalsSkin extends AbstractModel {

	protected $vitals_id;
	protected $skin;

	const dbtable = "VitalsSkinData";
	const idfield = "VitalsSkin_id";

	public function __construct($id=null) {
		return $this->construct_helper($id, self::dbtable, self::idfield);
	}

	public function set_vitals_id($vitals_id) {
		return $this->set_helper('int', $vitals_id, 'vitals_id');
	}

	public function set_skin($skin) {
		return $this->set_helper('int', $skin, 'skin');
	}

	public function get_summary() {
		//STUB	
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "VitalsSkin", self::idfield);
	}

	public function get_fieldmap() {
		return array(
			'Vitals_id' => 'vitals_id',
			'Skin_id' => 'skin',
			);
	}
}
