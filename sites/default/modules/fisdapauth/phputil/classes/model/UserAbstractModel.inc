<?php
/*---------------------------------------------------------------------*
  |                                                                     |
  |      Copyright (C) 1996-2010.  This is an unpublished work of       |
  |                       Headwaters Software, Inc.                     |
  |                          ALL RIGHTS RESERVED                        |
  |      This program is a trade secret of Headwaters Software, Inc.    |
  |      and it is not to be copied, distributed, reproduced, published |
  |      or adapted without prior authorization                         |
  |      of Headwaters Software, Inc.                                   |
  |                                                                     |
* ---------------------------------------------------------------------*/

require_once('phputil/classes/model/AbstractModel.inc');
require_once('phputil/classes/FieldValidatorClasses.php');
require_once('phputil/handy_utils.inc');

/* New models need to:
	RECOMMENDED:
	  define protected function load_fields()
		$this -> fields = array (
			'fieldname' => array (
				VALIDATION => array (
					'min_len' => 1
		.......
		validation
	
	OPTIONAL:
		default values creation
			override: public function set_all_to_defaults()
				use setters to set values
	
		custom validation
			use: validate_fields()

*/

/* TODO
	- override SetHelper to keep track of changes since last validation
*/

/** Overview of field validations. Two levels:
 *  - model level used by setter methods. They use assert functions that throw exceptions if data is not valid
 *  - validator class based methods. These collect validation information and create reports of problems with data. They don't throw exceptions.
 *
 *  Overview of validator class based level:
 *  - they use Validator classes to collect validation results
 *  - validator class only collects failed results or warnings. It's your responsibility to run correct validation tests to get correct results.
 *    (most important is to be aware of two strategies described below. ex: Don't run validate_fields method if you use set_values_by_row_if_valid method)
 *	If you don't run any validation for a field it means it doesn't need to be validated. Validator will return success.
 *
 *  - Two main strategies of conductiong validations:
 *	- by running it when all data is set in the model using $this->validate_fields Data needs to be set by:
 *	  - using individual setters  OR
 *	  - set_values_by_row  method (passing array of values for whole row)
 *	  WARNING: you need to pass valid field types if you use this function otherwise assertions will cause failure
 *	- by validating values before using setters, using set_values_by_row_if_valid Recommended to pass unverified data (directly from user input)
 *	  - set_values_by_row_if_valid validates all data given to it and only uses setters if all fields pass tests.
 */
abstract class UserAbstractModel extends AbstractModel
{
	const VALIDATION = 'validation';

	const DB_TABLE = '';

	/* Distinguishes two validation methods used.
	 * Purpose of this variable is to prevent from running validations twice.
	 * Values: 	not_set	- no validation was used yet
	 *		'OUTSIDE'	- outside model - before setter functions are used to set values (function: set_values_by_row_if_valid)
	 *		'INSIDE'	- when values in the model are used for validation
	 */
	protected $validation_method;

	/* Field properties. Each model needs to define load_fields method
	 *	to populate fields array
	 * test: is_array($this->fields) means that the value $fields is already loaded
	 */
	protected static $fields;

	// Validator object providing validations
	protected $validator;

	protected $in_dev_mode;

	/**
	 * Call this from inherited classes after calling constructor_helper
	 */
	public function __construct($id=null) {
		//$ret = parent::__construct($id);
		$this->in_dev_mode = (HandyServerUtils::get_environment()=='development');

		// set default values if new record
		if ($this->get_id() <= 0) {
			$this->set_all_to_defaults();
		}
		return $ret;
	}

	// all children need to return self::DB_TABLE
	//	abstract public function get_table_name();

	// FUNCTIONS SERVING PURPOSE OF IMPORTING DATA IN BATCHES
	/* Field properties & validations defined in each model
		models need to populate $this->fields array
		These validations don't run through assert. They return
		validations object.
		WARNING: THIS INITIATES / RESETS ALL THE RESULTS TOO!
	 */

	protected function create_once_validator_object() {
		if (!is_object($this->validator)) {
			// set up validator object
			$this->validator = new FieldInputValidator($this->get_table_name(), $this->fields);
		}
	}	

	/** loads:  (default) batch import validation rules that should be defined in each child model  
	 * 	or  user defined validation rules 
	 */
	public function load_validation_rules($validation_rules='BATCH') {
		$fields = $this->get_fieldmap();

		if ($validation_rules=='BATCH') {
			$this->load_batch_validation_rules();
		} else if (is_array($validation_rules)) {
			// make sure field names are correct
			foreach ($validation_rules as $field => $vals) {
				if (!in_array($fields, $field)) {
					throw new FisdapRuntimeException("No such field: $field in table model");
				}
			}
			$this->fields = $validation_rules;
		} else {
			throw new FisdapRuntimeException("Argument must be array");
		}
		$this->create_once_validator_object();
	}

	/*
	// to process single extra-validation through each setter
	protected function process_extra_validation($field_name) {

	}


	// uses setter to set whole row of data, takes array ('fieldn_name' = 'value') .as argument
	public function set_row_of_data() {
	}
	 */

	public function get_validation_results() {
		if ($this->validator) {
			return $this->validator->get_results();
		} else {
			return null;
		}
	}

	/**
	 * @return FieldInputValidator object containing all results of validations
	 */
	public function get_validator_object() {
		if ($this->validator) {
			return $this->validator;
		} else {
			return null;
		}
	}


	/** 
	 * Override this function to set validation rules
	 */
	protected function load_batch_validation_rules() {
		$this->fields = array();
	}

	/**
	 * Sets values for current row using values passed in $vals array
	 * array must contain valid field names as keys
	 * $throws FisdapInvalidArgumentException if invalid fieldname is passed which doesn't have its setter method.
	 */
	public function set_values_by_row($vals) {
		foreach ($vals as $field => $val) {
			$fx = "set_".$field;
			if (!method_exists($this, $fx)) {
				throw new FisdapInvalidArgumentException("Method '$fx' in ".get_class($this)." class doesn't exist");
			}
			$this->$fx($val);
		}
	}


	/**
	 * Sets values for current row using values passed in $vals array
	 * array must contain valid field names as keys
	 * function uses validator functions. If value isn't valid function won't fail.
	 * Instead it will report results through validator object
	 */
	public function set_values_by_row_if_valid($vals) {
		// record validation method
		$this->validation_method = 'OUTSIDE';

		// load validation rules if not already loaded
		if (!is_array($this->fields)) {
			$this->load_validation_rules();
		}

		$new_vals = array();
		$fields = $this->get_fieldmap();

		$this->create_once_validator_object();
		foreach ($vals as $field_name => $fieldval) {
			if (!$this->validate_field($field_name, $fieldval)) $fails++;
		}

		if ($this->validator->is_valid()) {
			if ($fails) {	// just test of validator->is_valid function that suspected malfunctioning
				throw new FisdapException ("What the heck.. something's wrong with validator->is_valid function. $fails fails encountered and is_valid returns true\n");
			}
			$this->set_values_by_row($vals);
		}
		return (!($fails));
	}	


	/* all field values are set to their defauls when blank new record is created
		models that want default values should call the setters here
	 */
	public function set_all_to_defaults() {
	}

	public function get_field_details() {
		return ($this->fields);
	}

	/**
	 * Override this function to define custom validations
	 * @return boolean validation result
	 */
	public function do_custom_validations() {
	}


	/* this should be extended if special validations need to take place.
	 */
	public function validate_fields() {
		$is_valid = $this->auto_validate_fields();

		return $is_valid;
	}

	/* uses validator class and field validation rules defined in each model class
	protected function auto_validate_fields() {
	 */
	public function auto_validate_fields() {
		$this->load_validation_rules();	// load batch validation rules

		//$this->validations->show_field_rules();
		foreach ($this->fields as $field_name => $rules) {
			//foreach ($rules as $rule_name => $rule_val) {
			$getter_fx = 'get_'.$field_name;
			if (!is_callable(array($this, $getter_fx))) {
				throw new FisdapInvalidArgumentException("No getter for $getter_fx");
			}
			eval ("\$fieldval = \$this->$getter_fx();");
			$this->validate_field($field_name, $fieldval);
		}
		return $this->validator->is_valid();
	}

	/**
	 * Validates field against set rules
	 * Main purpose is to validate BEFORE setters are used which may fail whole script
	 * @return boolean validation result for the field, returns true if no validation rule(s) present
	 */
	protected function validate_field($field_name, $fieldval) {
		// if validation rule present
		if (isset($this->fields[$field_name])) {
			$rules = $this->fields[$field_name];
			$result = $this->validator->validate($fieldval, $rules, $field_name);
		} else {
			$result = true;
		}
		return $result;
	}
}

?>
