<?php

require_once('phputil/classes/model/TestItemReview.php');

function test_reading($connection) {
    return false;
}//test_reading

function test_insertion($connection) {
    return false;
}//test_insertion

function test_updating($connection) {
    return false;
}//test_updating

function test_deletion($connection) {
    $review =& TestItemReview::find_by_id(2);
    if ( !$review ) {
        return false;
    }//if

    $review->delete();
    return true;
}//test_deletion

?>
