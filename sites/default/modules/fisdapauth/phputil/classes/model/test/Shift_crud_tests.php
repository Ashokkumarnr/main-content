<?php

require_once('phputil/classes/Shift.php');

function test_reading($connection) {
    return false;
}//test_reading

function test_insertion($connection) {
    return false;
}//test_insertion

function test_updating($connection) {
    return false;
}//test_updating

function test_deletion($connection) {
    $shift =& new FISDAP_Shift(1215378, $connection->get_link_resource());
    if ( !$shift ) {
        echo "test_deletion:: couldn't load shift from database\n";
        return false;
    }//if

    if ( !$shift->delete() ) {
        echo "test_deletion:: couldn't delete shift\n";
        return false;
    }//if

    return true;
}//test_deletion

?>
