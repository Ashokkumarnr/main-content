<?php

require_once('phputil/classes/model/TestItem.php');
require_once('phputil/classes/model/TestCreationProject.php');

/**
 *
 */
class MockTestItem extends TestItem {
    var $my_mock_reviews;
    var $my_mock_was_saved;
    var $my_mock_project;

    function MockTestItem($assignment_review_type) {
        $this->TestItem();
        $this->project_id = 666;
        $this->my_mock_reviews = array();
        $this->my_mock_was_saved=false;
        $this->logger =& Logger::get_instance();

        if ( $assignment_review_type == SINGLETON_ASSIGNMENT_REVIEW ) {
            $this->my_mock_project = new SingletonTestCreationProject();
        } else if ( $assignment_review_type == DEFAULT_ASSIGNMENT_REVIEW ) {
            $this->my_mock_project = new DefaultTestCreationProject();
        } else {
            die(
                'Invalid assignment review type: "'.$assignment_review_type.'"'
            );
        }//else
    }//MockTestItem

    function save() {
        $this->my_mock_was_saved = true;
    }//save

    function &get_project() {
        return $this->my_mock_project;
    }//get_project

    function get_reviews() {
        return $this->my_mock_reviews;
    }//get_reviews

    function set_reviews($reviews) {
        $this->my_mock_reviews = $reviews;
    }//set_reviews
}//class MockTestItem

?>
