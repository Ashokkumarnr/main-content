<?php

/** defines the TEST_ITEM data type id constant, among other things */
require_once('phputil/asset_functions.html');

/** the standard FISDAP database connection abstraction */
require_once('phputil/classes/FISDAPDatabaseConnection.php');

/** the TestItem model class */
require_once('phputil/classes/model/TestItem.php');

/** the TestItemReviewAssignment model class */
require_once('phputil/classes/model/TestItemReviewAssignment.php');

/** the standard OO logging mechanism */
require_once('phputil/classes/logger/Logger.php');


/**
 * This class models test item review assignments.
 */
class TestItemReviewAssignment {

    ///////////////////////////////////////////////////////////////////////////
    // Private instance variables
    ///////////////////////////////////////////////////////////////////////////

    var $id;
    var $item_id;
    var $user_id;
    var $due_on;
    var $assigned_on;
    var $completed_on;
    var $field_map;
    var $is_active;
    var $logger;
    var $connection_obj;
	var $group_assignment;


    ///////////////////////////////////////////////////////////////////////////
    // Constructors
    ///////////////////////////////////////////////////////////////////////////

    /**
     * 
     */
    function TestItemReviewAssignment() {
        $this->logger =& Logger::get_instance();
        $this->connection_obj =& FISDAPDatabaseConnection::get_instance();

        $this->field_map = array(
            'db'=>array(
                'ReviewAssignment_id'=>'id',
                'DateReviewDue'=>'due_on',
                'DateReviewReceived'=>'completed_on',
                'DateReviewAssigned'=>'assigned_on',
                'UserAuth_id'=>'user_id',
                'Item_id'=>'item_id',
                'Active'=>'is_active',
				'GroupAssignment'=>'group_assignment'
            )
        );
    }//TestItemReviewAssignment


    ///////////////////////////////////////////////////////////////////////////
    // Public instance methods
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Delete this assignment from the database only if it is an individual assignment
     */
    function delete() {
        if ( $this->id ) {
            $query = 'DELETE FROM ReviewAssignmentData '.
                     'WHERE ReviewAssignment_id="'.$this->id.'" AND GroupAssignment=0 LIMIT 1';
            $this->connection_obj->query($query);
        }//if
    }//delete
    

    /**
     * Returns a list of all TestItemReviewAssignments for 
     * which the comparator object's matches method returns true.
     *
     * @param  string $where_clause (optional) the where clause to use in the query
     * @return array 
     */
    function find_all($where_clause='') {
        // build the query (with optional where clause)
        $query = 'SELECT reviews.*, assets.Data_id as Item_id '.
                 'FROM ReviewAssignmentData reviews, '.
                      'Asset_def assets '.
                 'WHERE assets.DataType_id="'.TEST_ITEM.'" '.
                 'AND   assets.AssetDef_id=reviews.AssetDef_id';
        if ( $where_clause ) {
            $query .= ' AND '.$where_clause;
        }//if
        $query .= ' ORDER BY reviews.DateReviewDue, Item_id';

        // connect to the database and run the query
        $connection_obj =& FISDAPDatabaseConnection::get_instance();
        $result_set = $connection_obj->query($query);
        $all_assignments = array();
        if ( $result_set ) {
            foreach( $result_set as $row ) {
                $assignment =& TestItemReviewAssignment::from_assoc_array($row);
                $all_assignments[] = $assignment;
            }//foreach
        }//if
        return $all_assignments;
    }//find_all


    /**
     * Read a TestItemReviewAssignment from the MySQL database, given
     * an id.
     *
     * @param int $id the id of the assignment to return
     */
    function &find_by_id($id) {
        if( !is_numeric($id) ) {
            return false;
        }//if

        $query = 'SELECT reviews.*, '.
                        'assets.Data_id as Item_id '.
                 'FROM ReviewAssignmentData reviews, '.
                      'Asset_def assets '.
                 'WHERE reviews.ReviewAssignment_id="'.$id.'" '.
                 'AND assets.AssetDef_id=reviews.AssetDef_id';
        $connection_obj =& FISDAPDatabaseConnection::get_instance();
        $result_set = $connection_obj->query($query);
        if ( !$result_set || count($result_set) != 1 ) {
            return false;
        }//if

        return TestItemReviewAssignment::from_assoc_array($result_set[0]);
    }//find_by_id


    /**
     * Return all the assignments for the given item
     *
     * @param int    $item_id      the id of the item whose assignments we want
     * @param string $where_clause (optional) the where clause to use in the query
     */
    function find_all_by_item($item_id, $where_clause='') {
        if ( !is_numeric($item_id) ) {
            die('TestItemReviewAssignment::find_all_by_item: item id must be numeric');
        }//if

        // get the asset id for the given item
        $asset_id = getTestItemAsset_id($item_id);
        if ( !$asset_id ) {
            return false;
        }//if
    
        if ( $where_clause ) {
            $where_clause = ' AND '.$where_clause;
        }//if
        return TestItemReviewAssignment::find_all(
            'reviews.AssetDef_id="'.$asset_id.'"'.$where_clause
        );
    }//find_all_by_item


    /** 
     * Return all the assignments for the user with the given username
     *
     * @param string $user_name the username of the person whose assignments
     *        we want 
     * @param string $where_clause (optional) the where clause to use
     *        in the query
     */
    function find_all_by_username($user_name, $where_clause='') {
        $user_auth_id = getUserAuthID($user_name);
        if ( $user_auth_id <= 0 ) {
            die(
                'TestItemReviewAssignment::find_all_by_username: '
                . 'Error getting user auth id'
            );
        }//if

        if ( $where_clause ) {
            $where_clause = ' AND '.$where_clause;
        }//if
        return TestItemReviewAssignment::find_all(
            'reviews.UserAuth_id="'.$user_auth_id.'"'.$where_clause
        );
    }//find_all_by_username


    /**
     * Delete all unnecessary assignments for the given item.  See is_necessary, below.
     * 
     * @param int $item_id a test item id
     */
    function delete_unnecessary_assignments_for_item( $item_id ) {
        $assignments = TestItemReviewAssignment::find_all_by_item($item_id);
        if ( $assignments ) {
            foreach( $assignments as $assignment ) {
                if ( !$assignment->is_necessary() ) {
                    $assignment->delete();
                }//if
            }//foreach
        }//if
    }//delete_unnecessary_assignments_for_item


    function &from_assoc_array($db_row) {
        // set this object's fields from the given associative
        // array (presumably from the database)
        $assignment =& new TestItemReviewAssignment();
        $db_field_map = $assignment->field_map['db'];
        foreach( $db_field_map as $db_field=>$obj_field ) {
            $assignment->$obj_field = $db_row[$db_field];
        }//foreach
        return $assignment;
    }//from_assoc_array


    /**
     * Determine if the assignment is necessary, in the sense of whether
     * or not it matters for the review process.  We do not need additional
     * reviews for items that have already been marked as "needs revision," 
     * "valid," or "failed."
     *
     * Assignments that have been completed are also necessary, as are 
     * inactive assignments.
     *
     * @todo this should throw an exception on error, not die (PHP5)
     * @return bool true iff the assignment is needed for the review process
     */
    function is_necessary() {
        $item =& TestItem::find_by_id($this->item_id);
        if ( !$item ) {
            die('Error:  test item #'.$this->item_id.' could not be found.');
        }//if

        return ($this->is_complete() ||  
                !$this->is_active()  ||
                $item->get_validity() == UNDETERMINED_ITEM_VALIDITY); 
    }//is_necessary


    /**
     * Returns a value which evaluates to true iff the assignment is
     * active.  Otherwise, returns a value which evaluates to false.
     * Currently, these values are 0 and 1, but the values may change
     * in the future.
     */
    function is_active() {
        return $this->is_active;
    }//is_active


    /**
     * Returns true iff the assignment has been completed, false otherwise.
     */
    function is_complete() {
        return ($this->completed_on != '0000-00-00');
    }//is_complete

	function is_group() {
		return ($this->group_assignment == 1);
	}

    ///////////////////////////////////////////////////////////////////////////
    // Standard, unexciting observers
    ///////////////////////////////////////////////////////////////////////////

    function get_id() {
        return $this->id;
    }//get_id
    

    function get_assigned_on() {
        return $this->assigned_on;
    }//get_assigned_on


    function get_due_on() {
        return $this->due_on;
    }//get_due_on


    function get_completed_on() {
        return $this->completed_on;
    }//get_completed_on


    function get_item_id() {
        return $this->item_id;
    }//get_item_id
    

    function get_user_id() {
    	return $this->user_id;
    }//get_user_id
    
}//class TestItemReviewAssignment

?>
