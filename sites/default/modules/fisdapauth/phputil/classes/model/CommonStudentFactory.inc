<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FisdapCaches.inc');
require_once('phputil/classes/FisdapSimpleCache.inc');
require_once('phputil/classes/model/CommonUserFactory.inc');
require_once('phputil/exceptions/FisdapException.inc');

/**
 * A factory for students.
 */
final class CommonStudentFactory {
	const ID_PREFIX = 'id:';
	const USER_NAME_PREFIX = 'username:';

	private $cache;

	protected function __construct() {
		$this->cache = new FisdapSimpleCache('CommonStudent');
	}

	/**
	 * Retrieve the singleton.
	 * @return The factory.
	 */
	public static function get_instance() {
		static $instance;

		if (is_null($instance)) {
			$instance = new CommonStudentFactory();
			FisdapCaches::get_instance()->register($instance->cache);
		}

		return $instance;
	}

	/**
	 * Retrieve the underlying cache.
	 * @return FisdapCache The underlying cache.
	 */
	public function get_cache() {
		return $this->cache;
	}

    /**
     * Retrieve a user by a variety of sources.
	 * <ul>
	 * <li>If the source is an int, it is the student ID.
	 * <li>Source may also be common_student, common_user, a
	 * user name, or null for the logged in user.
	 * </ul>
	 * @param mixed $source The model source.
	 * @param boolean $must_exist TRUE if the user must be found.
	 * @return common_user | null The user (null if $must_exist=false)
	 * @throws FisdapException If the model can't be retrieved if 
	 * $must_exist=true.
     */
    public function get_by_source($source=null, $must_exist=true) {
		global $REMOTE_USER; 

		Assert::is_boolean($must_exist);
        Assert::is_true(
            Test::is_a($source, 'common_student') ||
            Test::is_a($source, 'common_user') ||
            Test::is_int($source) ||
            Test::is_string($source) ||
            Test::is_null($source));

		$this->cache->clear_hit();

        if ($source instanceof common_student) {
			$this->add($source);
            return $source;
        }

		$student = null;
		$user = null;
		$user_factory = CommonUserFactory::get_instance();

		try {
			if (Test::is_int($source)) {
				$key = self::ID_PREFIX . $source;
				if ($this->cache->contains($key)) {
					return $this->cache->get($key);
				}

				$student = new common_student($source);
			}

			elseif (is_string($source)) {
				$user = $user_factory->get_by_source($source, $must_exist);
			}

			elseif ($source instanceof common_user) {
				$user = $user_factory->get_by_source($source, $must_exist);
			}

			// No source.
			else {
				$source = "REMOTE_USER[$REMOTE_USER]";
				$user = $user_factory->get_by_source($REMOTE_USER, $must_exist);
			}
		}
		catch (FisdapException $e) {
			if ($must_exist) {
				throw new FisdapException(self::get_msg($source, $user, $student), $e);
			}

			$student = null;
			$user = null;
		}

		// If we got a user, try to load the student.
		if (is_null($student) && !is_null($user)) {
			if (!$user->is_valid()) {
				throw new FisdapException(self::get_msg($source, $user, $student));
			}

			if (!$user->is_student()) {
				throw new FisdapException(self::get_msg($source, $user, $student));
			}

			return $this->get_by_source($user->get_student_id(), $must_exist);
		}

		if ($must_exist) {
			if (is_null($student)) {
				throw new FisdapException(self::get_msg($source, $user, $student));
			}
		}

		if (!is_null($student)) {
			$this->add($student);
		}

		return $student;
    }

	/**
	 * Add a student to the cache.
	 * @param common_student $student The student.
	 */
	private function add(common_student $student) {
		$this->cache->put(self::ID_PREFIX. $student->get_student_id(), $student);
		$this->cache->put(self::USER_NAME_PREFIX. $student->get_username(), $student);
	}

	private static function get_msg($source, $user, $student) {
		$msgs = array();

		if (!Test::is_object($source)) {
			$msgs[] = "source[$source]";
		}

		if (!is_null($user)) {
			if ($user->is_valid()) {
				$msgs[] = 'user name[' . $user->get_username() . ']';
				$msgs[] = 'user ID[' . $user->get_UserAuthData_idx() . ']';
				$msgs[] = 'user student ID[' . $user->get_student_id() . ']';
			}
			else {
				$msgs[] = 'invalid user';
			}
		}

		$msg = 'Unable to retrieve student';
		if (count($msgs)) {
			$msg .= '; ' . join(', ', $msgs);
		}

		return $msg;
	}
}
?>
