<?php

/*----------------------------------------------------------------------*
  |                                                                      |
  |     Copyright (C) 1996-2010.  This is an unpublished work of         |
  |                      Headwaters Software, Inc.                       |
  |                         ALL RIGHTS RESERVED                          |
  |     This program is a trade secret of Headwaters Software, Inc.      |
  |     and it is not to be copied, distributed, reproduced, published,  |
  |     or adapted without prior authorization                           |
  |     of Headwaters Software, Inc.                                     |
  |                                                                      |
*----------------------------------------------------------------------*/

require_once("Model.inc");

/**
 * @author Kate Hanson
 * 
 */
class ScheduledTest extends AbstractModel {

	/**
	 * The id of the exam to be scheduled, as defined in the MoodleTestData table
	 */
	protected $test_id;
	/**
	 * The start date of when the exam will be taken
	 */
	protected $start_date;
	/**
	 * The end date of when the exam will be taken
	 */
	protected $end_date;
	/**
	 * Full name of the contact person
	 */
	protected $contact_name;
	/**
	 * Phone number of the contact person
	 */
	protected $contact_phone;
	/**
	 * Email address of the contact person
	 */
	protected $contact_email;
	/**
	 * Any notes about the scheduled test
	 */
	protected $test_notes;
	/**
	 * A serialized array of student ids of the students scheduled to take the exam
	 */
	protected $scheduled_students;
	/**
	 * The program id for the program which is scheduling the exam
	 */
	protected $program_id;
	/**
	 * Class year selected in the student selector used to pick which students will take the exam. -1 if 'All' is selected; 1996 if 'Left Program' is selected.
	 */	
	protected $class_year;
	/**
	 * Class month selected in the student selector used to pick which students will take the exam. 0 if 'All' is selected.
	 */
	protected $class_month;
	/**
	 * Class section year selected in the student selector used to pick which students will take the exam. -1 if 'All' is selected.
	 */	
	protected $class_section_year;
	/**
	 * ID of the class section selected in the student selector used to pick which students will take the exam. -1 if 'All' is selected.
	 */
	protected $class_section;
	/**
	 * Serialized array of the certification level(s) picked in the student selector used to pick which students will take the exam.
	 */
	protected $cert_level;
	/**
	 * 'Active' status of the scheduled exam: 1 if the scheduled exam is still visible, 0 if it has been 'deleted'.
	 */
	protected $active;
	/**
	 * Flag for whether the students can access their own scores on their own: 1 if scores are published, 0 if not.
	 */
	protected $publish_scores;
	/**
	 * Flag denoting whether or not the scheduling instructor has agreed to our pilot testing terms when scheduling a pilot test
	 *
	 * This field is part of a project that is still in development and is not currently used on the mainline
	 */
	protected $agreed_to_pilot;
	/**
	 * ID of the instructor who has agreed to our pilot testing terms when scheduling a pilot test
	 *
	 * This field is part of a project that is still in development and is not currently used on the mainline
	 */
	protected $pilot_agreed_on;
	/**
	 * Date that the instructor agreed to our pilot testing terms when scheduling a pilot test
	 *
	 * This field is part of a project that is still in development and is not currently used on the mainline
	 */
	protected $pilot_agreed_by;

	const DB_TABLE_NAME = "ScheduledTests";
	const ID_FIELD = "ScheduledTest_id";

	/**
	 * Constructs a new ScheduledTest object
	 *
	 * Uses the inherited construct_helper function to make an instance of ScheduledTest.
	 *
	 * @param int|string $scheduled_test_id The unique ScheduledTest_id of the scheduled test you want to use.
	 * @return ScheduledTest
	 */
	public function __construct($scheduled_test_id) {
		$this->construct_helper($scheduled_test_id, self::DB_TABLE_NAME, self::ID_FIELD);
	}

	/**
	 * STUB If we need an easy summary of some aspect of this model, we can write it here
	 *
	 * @return string "ScheduledTest X"
	 */
	public function get_summary() {
		return 'ScheduledTest '.$this->test_id;
	}

	/**
	 * Maps the fields in the ScheduledTests table to the corresponding properties of this object
	 *
	 * The fieldmap is used in AbstractModel to create the helper functions, save/clone an object, etc...
	 */
	public function get_fieldmap() {
		$map =  array(
			'Test_id' => 'test_id',
			'StartDate' => 'start_date',
			'EndDate' => 'end_date',
			'ContactName' => 'contact_name',
			'ContactPhone' => 'contact_phone',
			'ContactEmail' => 'contact_email',
			'TestNotes' => 'test_notes',
			'ScheduledStudents' => 'scheduled_students',
			'Program_id' => 'program_id',
			'ClassYear' => 'class_year',
			'ClassMonth' => 'class_month',
			'ClassSectionYear' => 'class_section_year',
			'ClassSection' => 'class_section',
			'CertLevel' => 'cert_level',
			'Active' => 'active',
			'PublishScores' => 'publish_scores',
			'AgreedToPilot' => 'agreed_to_pilot',
			'PilotAgreedOn' => 'pilot_agreed_on',
			'PilotAgreedBy' => 'pilot_agreed_by'
		);
		return $map;
	}

	/**
	 * Creates an array of ScheduledTest objects based on the results of a MySQL query
	 *
	 * This purpose of this function is to create multiple ScheduledTest objects based on a single
	 * database hit. Since you need info on all fields in order to create an object, only complete
	 * ScheduledTests rows will work.
	 *
	 * @param result A result set containing on or more complete ScheduledTests rows. Both mysql 
	 * resource and array formats are accepted.
	 * @return array An array of ScheduledTest objects
	 */
	public static function getBySQL($result) {
		return parent::sql_helper($result, 'ScheduledTest', self::ID_FIELD);
	}

	/**
	 * Lists which fields can be set with magical setters
	 *
	 * We may need to go back at some point and write explicit setters for some of these fields, if there's logic involved
	 */
	public function get_settable_fields() {
		return array('test_id' => 'int',
			'end_date' => 'date',
			'contact_name' => 'string',
			'contact_phone' => 'string',
			'contact_email' => 'string',
			'test_notes' => 'string',
			'program_id' => 'int',
			'class_year' => 'int',
			'class_month' => 'int',
			'class_section_year' => 'int',
			'class_section' => 'int',
			'active' => 'int',
			'publish_scores' => 'int',
			'agreed_to_pilot' => 'int',
			'pilot_agreed_on' => 'datetime',
			'pilot_agreed_by' => 'string');
	}

	/**
	 * Set the start_date field of the object
	 *
	 * Checks to make sure the input is not null, then uses to set_helper to make sure
	 * it is a valid date before setting the field
	 *
	 * @param date $start_date A valid, not-null date in string format 
	 */
	public function set_start_date($start_date) {
		Assert::is_not_null($start_date);
		$fisdap_start_date = new FisdapDate($start_date);
		Assert::is_true($fisdap_start_date != FisdapDate::create_from_ymd_string('0000-00-00'));

		return $this->set_helper("date", $start_date, "start_date");
	}

	/**
	 * Set the scheduled_students field of the object
	 *
	 * Checks to make sure the input is an array of integers, then converts
	 * the student id array into a serialized array, then saves it to the scheduled_students field
	 * so that it can ultimately be saved in the ScheduledStudents column of the ScheduledTests table.
	 *
	 * @param array $student_array An array of student ids of the students scheduled to take the test
	 */
	public function set_scheduled_students($student_array) {
		Assert::is_array($student_array);
		foreach ($student_array as $i=>$tmp_student_id) {
			Assert::is_int($tmp_student_id);
			$student_array[$i] = strval($tmp_student_id);
		}
		$student_string = serialize($student_array);
		return $this->set_helper("string", $student_string, "scheduled_students");
	}

	/**
	 * Set the cert_level field of the object
	 *
	 * Checks to make sure the input is an array of valid certification levels, then converts the
	 * cert_level array into a serialized array, then saves it to the cert_level field
	 * so that it can ultimately be saved in the CertLevel column of the ScheduledTests table.
	 *
	 * @param array $cert_level_array An array of the certification level(s) picked in the student selector used to pick which students will take the exam. 
	 */
	public function set_cert_level($cert_level_array) {
		$cert_level_types = self::data_table_helper("AccountTypeTable", "AccountType_id", "AccountType_label");  
		Assert::is_array($cert_level_array);
		foreach ($cert_level_array as $i=>$tmp_cert_level) {
			Assert::is_in_array($tmp_cert_level, $cert_level_types);
			$cert_level_array[$i] = strval($tmp_cert_level);
		}
		$cert_level_string = serialize($cert_level_array);
		return $this->set_helper("string", $cert_level_string, "cert_level");
	}
	
	/**
	 * Get the start_date field of the object
	 *
	 * Gets the start_date field and makes sure it is not null or empty 
	 *
	 * @return string $start_date An array of student ids of the students scheduled to take the test
	 */
	public function get_start_date() {
		$start_date = $this->start_date;
		$fisdap_start_date = new FisdapDate($start_date);
		Assert::is_true($fisdap_start_date != FisdapDate::create_from_ymd_string('0000-00-00'));

		return $start_date;
	}

	/**
	 * Get the scheduled_students field of the object
	 *
	 * Gets the serialized student id array from the scheduled_students field and returns an unserialized array
	 *
	 * @return array $student_array An array of student ids of the students scheduled to take the test
	 */
	public function get_scheduled_students() {
		$student_array =  unserialize($this->scheduled_students);
		if ($student_array === FALSE) {
			return NULL;
		}
		return $student_array;
	}

	/**
	 * Get the cert_level field of the object
	 *
	 * Gets the serialized cert_level array from the cerl_level field and returns an unserialized array
	 *
	 * @return array $cert_level_array An array of the certification level(s) picked in the student selector used to pick which students will take the exam. 
	 */
	public function get_cert_level() {
		$cert_level_array = unserialize($this->cert_level);
		if ($cert_level_array === FALSE) {
			return NULL;
		}
		return $cert_level_array;
	}
}

?>
