<?php

require_once('phputil/classes/model/TestItemReview.php');
require_once('phputil/classes/model/TestCreationProjectFactory.php');
require_once('phputil/classes/logger/Logger.php');
require_once('phputil/test_item_validity_constants.php');
require_once('phputil/test_data_functions.php');
require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/classes/model/TestItemReviewFactory.php');


/**
 * This class encapsulates test items
 */
class TestItem {

	////////////////////////////////////////////////////////////
	// Private instance variables
	////////////////////////////////////////////////////////////

	var $id;
	var $validity;
	var $moodle_id;
	var $project_id;
	var $item_type_id;
	var $scenario_id;
	var $previous_version_id;
	var $stem;
	var $received_on;
	var $validated_on;
	var $paid_for;
	var $reference_text;
	var $field_map;
	var $logger;
	var $reset_validated_date;
	var $changed_fields;
	var $connection_obj;


	////////////////////////////////////////////////////////////
	// Constructors
	////////////////////////////////////////////////////////////

	/**
	 * Create an empty TestItem
	 */
	public function TestItem() {
		$this->id = 0;
		$this->reset_validated_date = false;
		$this->changed_fields = array();

		$this->connection_obj =& FISDAPDatabaseConnection::get_instance();
		$this->logger =& Logger::get_instance();

		$this->field_map = array(
			'db' => array(
				'Item_id'=>'id',
				'ValidStatus'=>'validity',
				'Moodle_id'=>'moodle_id',
				'Project_id'=>'project_id',
				'ItemType_id'=>'item_type_id',
				'Scenario_id'=>'scenario_id',
				'PreviousVersion_id'=>'previous_version_id',
				'Stem'=>'stem',
				'DateReceived'=>'received_on',
				'DateValidated'=>'validated_on',
				'PaidFlag'=>'paid_for',
				'ReferenceText'=>'reference_text'
			)
		);
	}//TestItem


	////////////////////////////////////////////////////////////
	// Public instance methods
	////////////////////////////////////////////////////////////

	/**
	 * Set our validity status from the reviews that have been filled 
	 * out for this item.  
	 *
	 * @param bool $email_warnings (optional) if true (the default), we'll 
	 *                             send email notifications of errors
	 * @return int the updated validity status
	 */
	public function update_review_status($email_warnings=true) {
		// ask the project, if any, to update our validity
		$project =& $this->get_project();
		if ($project ) {
			$this->logger->log(
				'TestItem#'.$this->id.': updating validity status from reviews',
				$this->logger->WARNING
			);
			return $project->update_item_review_status($this, $email_warnings);
		} else {
			$this->logger->log(
				'TestItem#'.$this->id.': can\'t update status for item with '.
				'no project (#'.$this->project_id.')',
					$this->logger->WARNING
				);
			return false;
		}//else
	}//update_review_status


	/**
	 * Returns an INSERT/UPDATE SQL query for saving this item to the 
	 * database.  Note that only the fields that have been changed via
	 * the mutators (see the set_* methods below) will be used for update
	 * queries, while all fields will be used for inserts.
	 *
	 * @todo come up with a decent test for this method, to make sure changed
	 *       fields are written to the query properly
	 * @return string an SQL statement to save the item to the database
	 */
	public function get_save_query() {
		if (!$this->changed_fields ) {
			return '';
		}//if

		if ($this->id ) {
			$query = 'UPDATE ';
		} else {
			$query = 'INSERT INTO ';
		}//else

		$query .= 'Item_def SET ';

		foreach ($this->field_map['db'] as $db_field=>$obj_field ) {
			// if we need to reset the validation date, set it to NOW() to let
			// MySQL insert the current date
			if ($obj_field == 'validated_on' && $this->reset_validated_date ) {
				$query .= $db_field.'=NOW(), ';
				continue;
			}//if

			// if this is an update, make sure the given field has been changed (via the 
			// public mutator API).  otherwise, put all fields in the INSERT query.
			if ( 
				// @WARNING the following line failed for fields changed to the empty
				// string, or some other "false" value.  we need to make sure
				// we didn't screw anything up by removing that logic.
				//
				// isset($this->$obj_field) && 
				$obj_field != 'id' && 
				(!$this->id || 
				in_array($obj_field, array_keys($this->changed_fields))) ) {
					$query .= $db_field.'="'.addslashes($this->$obj_field).'", ';
				}//if
		}//foreach

		// chop the extra two trailing characters (', ')
		$query = substr($query, 0, strlen($query)-2);
		if ($this->id ) {
			$query .= ' WHERE Item_id="'.$this->id.'"';
		}//if

		return $query;
	}//get_save_query


	/**
	 * Get the TestItem with the given id, by reference.
	 *
	 * @param int $id a test item id
	 * @return TestItem the test item with the given id (or false on error)
	 */
	public function &find_by_id($id) {
		if (!is_numeric($id) ) {
			return false;
		}//if

		$connection_obj =& FISDAPDatabaseConnection::get_instance();

		$query = 'SELECT * '.
			'FROM Item_def '.
			'WHERE Item_id="'.$id.'"';
		$result_set = $connection_obj->query($query);
		if (!is_array($result_set) || count($result_set) != 1 ) {
			return false;
		}//if

		return TestItem::from_assoc_array($result_set[0]);
	}//find_by_id


	/**
	 * Get all of the test items with the given validity status
	 *
	 * @param int $status a validity status number
	 * @return array all reviews with the given status number, or false
	 *               on error or empty set
	 */
	public function find_all_by_validity($status) {
		$items = array();

		if (!is_numeric($status) ) {
			return false;
		}//if

		$connection_obj =& FISDAPDatabaseConnection::get_instance();
		$query = 'SELECT * FROM Item_def '.
			'WHERE ValidStatus="'.$status.'"';
		$result_set = $connection_obj->query($query);
		if (!is_array($result_set) ) {
			return false;
		}//if

		foreach ($result_set as $row ) {
			$item =& TestItem::from_assoc_array($row);
			$items[] = $item;
		}//foreach

		return $items;
	}//find_all_by_validity


	/**
	 * Create a TestItem from the given associative array (formatted 
	 * like the result of calling mysql_fetch_assoc for a row in the
	 * Item_def table).  Note that the given array's field names should
	 * match the column names in the database, not the object fields.
	 *
	 * @param array $db_row an associative array representing a test item
	 * @return TestItem a TestItem object with the same content as the 
	 *                  given array
	 */
	public function &from_assoc_array($db_row) {
		$test_item =& new TestItem();
		foreach ($db_row as $field=>$value ) {
			$obj_field = $test_item->field_map['db'][$field];
			$test_item->$obj_field = $value;
		}//foreach
		return $test_item;
	}//from_assoc_array


	/**
	 * Write this test item to the database, using either an INSERT or
	 * UPDATE statement as appropriate.  
	 * 
	 * @warning insert handling hasn't been implemented yet.  the newly
	 *          inserted id should replace the object's id field 
	 * @warning this seems to fail when there were no changes to the
	 *          item
	 */
	public function save() {
		// if nothing changed, return 
		if (!$this->changed_fields ) {
			return false;
		}//if

		if (!$this->id ) {
			die('TestItem->save(): inserting new items not yet implemented');
		}//if

		$this->connection_obj->query($this->get_save_query());

		// reset the changed field indicators
		$this->reset_validated_date = false;
		$this->changed_fields = array();
		return true;
	}//save


	/**
	 * Get a list af all the reviews for this item
	 *
	 * @todo write this to return instances of the appropriate 
	 *       TestItemReview subclass
	 * 
	 * @return array the reviews for this item
	 */
	public function get_reviews() {
		$factory = new TestItemReviewFactory();
		return $factory->create_all_reviews_by_item($this->id);
	}//get_reviews


	/**
	 * Return the project associated with this item
	 */
	public function &get_project() {
		$project_factory =& new TestCreationProjectFactory();
		return ($this->project_id && $this->project_id > 0) ? 
			$project_factory->create_project($this->project_id) : 
		false;
	}//get_project

	/**
	 * Get a list of blueprints for this item
	 *
	 * @return an associative array of the blueprint(s), 
	 * with tbp_id keyed to blueprint Name, false if no
	 */
	public function get_blueprint_names() {
		$query = "SELECT tbp_id FROM TestBPItems WHERE Item_id=$this->id"; 
		$results = $this->connection_obj->query($query);
		if (count($results) === 0) {
			return false;
		}
		foreach ($results as $row) {
			$tbp_id = $row['tbp_id'];
			$bp_name = fetch_bp_name($tbp_id);
			$blueprints[$tbp_id] = $bp_name;
		}
		return $blueprints;
	}

	///////////////////////////////////////////////////////////////////////////
	// Mutators / Observers
	///////////////////////////////////////////////////////////////////////////

	/**
	 * Set the validity status to the given value, and make sure the
	 * validated_on field is updated with the current date if the item
	 * is saved.  Note that if the new status is equal to the current 
	 * value, this is a no-op.
	 * 
	 * @param int $status the new status
	 */
	public function set_validity($status) {
		if ($status != $this->validity ) {
			$this->validity = $status;
			$this->changed_fields['validity'] = 1;
			$this->changed_fields['validated_on'] = 1;
			$this->reset_validated_date = true;
		}//if
	}//set_validity

	public function get_validity() {
		return $this->validity;
	}//get_validity

	public function set_id($id) {
		$this->id = $id;
	}//set_id

	public function set_stem($stem) {
		if ($stem != $this->stem) {
			$this->stem = $stem;
			$this->changed_fields['stem'] = 1;
		}//if
	}//set_stem

	public function set_item_type_id($item_type_id) {
		if ($item_type_id != $this->item_type_id) {
			$this->item_type_id = $item_type_id;
			$this->changed_fields['item_type_id'] = 1;
		}//if
	}//set_item_type_id

	public function set_project_id($project_id) {
		if ($project_id != $this->project_id) {
			$this->project_id = $project_id;
			$this->changed_fields['project_id'] = 1;
		}//if
	}//set_project_id

	public function set_scenario_id($scenario_id) {
		if ($scenario_id != $this->scenario_id) {
			$this->scenario_id = $scenario_id;
			$this->changed_fields['scenario_id'] = 1;
		}//if
	}//set_scenario_id;

	public function set_received_on($received_on) {
		if ($received_on != $this->received_on) {
			$this->received_on = $received_on;
			$this->changed_fields['received_on'] = 1;
		}//if
	}//set_received_on

	public function set_reference_text($reference_text) {
		if ($reference_text != $this->reference_text) {
			$this->reference_text = $reference_text;
			$this->changed_fields['reference_text'] = 1;
		}//if

	}//set_reference_text

	public function set_validated_on($validated_on) {
		if ($validated_on != $this->validated_on) {
			$this->validated_on = $validated_on;
			$this->changed_fields['validated_on'] = 1;
		}//if
	}//set_validated_on

	public function set_paid_for($paid_for) {
		if ($paid_for != $this->paid_for) {
			$this->paid_for = $paid_for;
			$this->changed_fields['paid_for'] = 1;
		}//if
	}//set_paid_for


	public function get_id() {
		return $this->id;
	}//get_id

	public function get_stem() {
		return $this->stem;
	}//get_stem

	public function get_item_type_id() {
		return $this->item_type_id;
	}//get_item_type_id

	public function get_project_id() {
		return $this->project_id;
	}//get_project_id

	public function get_scenario_id() {
		return $this->scenario_id;
	}//get_scenario_id

	public function get_received_on() {
		return $this->received_on;
	}//get_received_on

	public function get_reference_text() {
		return $this->reference_text;
	}//get_reference_text

	public function get_validated_on() {
		return $this->validated_on;
	}//get_validated_on

	public function get_paid_for() {
		return $this->paid_for;
	}//get_paid_for
}//class TestItem

?>
