<?php

require_once('phputil/classes/model/ConsensusTestItemReviewFactory.php');
require_once('phputil/classes/model/AssignmentTestItemReviewFactory.php');
require_once('phputil/classes/model/SingletonTestItemReviewFactory.php');


/**
 * This class is responsible for creating all subclasses of TestItemReview,
 * given a review mode and an item id.  
 */
class TestItemReviewFactory {
    var $factories;

    function TestItemReviewFactory() {
        $this->factories = array(
            new ConsensusTestItemReviewFactory(),
            new AssignmentTestItemReviewFactory(),
            new SingletonTestItemReviewFactory()
        );
    }//TestItemReviewFactory


    /**
     *
     * @todo refactor this to run the query from within the TestItemReview
     *       class itself.
     */
    function &create_review($id) {
        if ( !is_numeric($id) ) {
            die(
                'create_review: error: could not create review... invalid ID '
                . 'received.'
            );
        }//if

        $connection =& FISDAPDatabaseConnection::get_instance();

        // AAAAAH!  Inside information!  This, and the query it goes in, should
        // be put somewhere inside the TestItemReview class
        $table = 'ItemReviews';
        $result_set = $connection->query(
            'SELECT * FROM '.$table.' WHERE id="'.$id.'"'
        );
        if ( is_array($result_set[0]) ) {
            $review_row = $result_set[0];
            return $this->create_review_from_contents($review_row);
        } else {
            die(
                'create_review: error: review #'.$id
                . ' not found in the database'
            );
        }//else
    }//create_review


    /**
     * Interpret the associative array (assumed to be of the 
     * same format as a database row) as an appropriate type
     * of review object
     */
    function &create_review_from_contents($assoc_array) {
        foreach ( $this->factories as $factory ) {
            $review =& $factory->create_review($assoc_array);
            if ( $review ) {
                return $review;
            }//if
        }//foreach

        die('None of the available factories was able to create a review');
    }//create_review_from_contents


    /**
     *
     */
    function &create_form($item,$mode) {
        foreach ( $this->factories as $factory ) {
            $review =& $factory->create_form($item,$mode);
            if ( $review ) {
                return $review;
            }//if
        }//foreach

        die('None of the available factories was able to create a form');
    }//create_form


    /**
     *
     * @todo can I overload this, or otherwise check for an item versus an item
     *       id?
     */
    function create_all_reviews_by_item($item_id) {
        if ( !is_numeric($item_id) ) {
            die('An invalid item ID was received');
        }//if

        $connection =& FISDAPDatabaseConnection::get_instance();

        // AAAAAH!  Inside information!  figure out how to refactor me!
        $table = 'ItemReviews';
        $result_set = $connection->query(
            'SELECT * FROM '.$table.' WHERE item_id="'.$item_id.'"'
        );

        if ( is_array($result_set) ) {
            $reviews = array();
            foreach( $result_set as $row ) {
                $created_review = $this->create_review_from_contents($row);
                if ( $created_review ) { 
                    $reviews[] = $created_review; 
                } else {
                    die(
                        'Failed to create a review for row with ID='.$row['id']
                    );
                }//else
            }//foreach
            return $reviews;
        } else {
            return false;
        }//else
    }//create_all_reviews_by_item
}//class TestItemReviewFactory

?>
