<?php
interface Model {
	/**
	 * Create a model
	 *
	 * If an id is provided, it will fetch an existing model from the database. 
	 * If no id is given, a new model will be created.
	 *
	 * @param int $id the id of the model
	 */
	//public function __construct($id=null);
    /**
     * Save or update the model in the database.
     * @return int The model ID.
     * @throws FisdapDatabaseException if an error occurs.
     */
    public function save();
    /**
     * Remove the model.
     * @throws FisdapDatabaseException if an error occurs.
     */
    public function remove();
	/**
	 * Creates a set of objects based on a MySQL result set so the database
	 * doesn't need to be hit more than once.
	 *
	 * @param array $result A result set containing one or more rows. The rows should
	 * be complete IVData records, and the result set CANNOT use alias names.
	 * @return array An array of Model objects.
	 */
	static public function getBySQL($result);
}
