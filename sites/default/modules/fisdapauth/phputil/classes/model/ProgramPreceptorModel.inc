<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/

require_once("AbstractModel.inc");

class ProgramPreceptor extends AbstractModel {

	protected $program_id;
	protected $preceptor_id;
	protected $active;

	const dbtable = "ProgramPreceptorData";
	const idfield = "ProPrecep_id";

	public function __construct($id=null) {
		return $this->construct_helper($id, self::dbtable, self::idfield);
	}

	public function set_program_id($program_id) {
		return $this->set_helper('int', $program_id, 'program_id');
	}

	public function set_preceptor_id($preceptor_id) {
		return $this->set_helper('int', $preceptor_id, 'preceptor_id');
	}

	public function set_active($active) {
		return $this->set_helper('int', $active, 'active');
	}

	public function get_summary() {
		//STUB	
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "programpreceptor", self::idfield);
	}

	public function get_fieldmap() {
		return array(
			'Program_id' => 'program_id',
			'Preceptor_id' => 'preceptor_id',
			'Active' => 'active'
			);
	}
}
