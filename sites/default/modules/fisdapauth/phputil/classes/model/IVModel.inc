<?php

require_once("DataModel.inc");
require_once("interfaces.inc");
/** 
 * Public member functions:
 *
 *	- save()
 *	- get_/set_: attempts, blood_draw, fluid_type, gauge, ivorio, site, success, time
 *		
 * Public static functions:
 *
 *	- get_iv_fluids()
 *	- get_iv_sites()
 *
 * Implementation Notes:
 *  - Fields are all stored as they are in the database, but setters return useful
 *   values unless db values are explicitly requested.
 *  - When a field that needs to be inserted in the DB is changed, that field is
 *   added to the $changed_fields array. It will then be updated upon the next
 *   call to save().
 */

class IV extends DataModel {

	protected $attempts;
	protected $blood_draw;
	protected $fluid_type;
	protected $gauge;
	protected $ivorio;
	protected $site;
	protected $success;
	protected $time;
	const dbtable = "IVData";
	const id_field = "IV_id";

	public function __construct($_id=null) {
		parent::__construct();
		if (!$this->construct_helper($_id, self::dbtable, self::id_field)) {
			return false;
		}
	}

	public function get_fieldmap() {
		$array = array();
		$array["Run_id"] = "run_id";
		$array["Shift_id"] = "shift_id";
		$array["Student_id"] = "student_id";
		$array["IVorIO"] = "ivorio";
		$array["PerformedBy"] = "performed_by";
		$array["IVLocation"] = "site";
		$array["FluidType"] = "fluid_type";
		$array["Success"] = "success";
		$array["BloodDraw"] = "blood_draw";
		$array["IVGage"] = "gauge";
		$array["NumAttempts"] = "attempts";
		$array["IVTime"] = "time";
		$array["SubjectType_id"] = "subject_type";
		$array["Assessment_id"] = "assessment_id";
		return $array;
	}

	/**
	 * @param mixed $_setting is 1, 0, "iv", or "io"
	 * @return boolean
	 */
	public function set_ivorio($_setting) {
		$this->set_helper("int", $this->id_or_string_helper($_setting, "get_ivorio_string"), "ivorio");
	}

	public static function get_ivorio_string() {
		return array(0 => "iv",	1 => "io");
	}

	/** 
	 * @param $_site either an int or a string that corresponds with the IvSite table
	 */
	public function set_site($_site) {
		return $this->set_helper("int", $this->id_or_string_helper($_site, "get_iv_sites"), "site");
	}

	/**
	 * @return int The id of the fluid used
	 */
	public function get_fluid_type() {
		return $this->fluid_type;
	}

	/**
	 * @param mixed $_type Must be an int or a string that corresponds to a known type.
	 */
	public function set_fluid_type($_type) {
		return $this->set_helper("int", $this->id_or_string_helper($_type, "get_iv_fluids"), "fluid_type");
	}

	/* 
	 * @param mixed $_success Must be 0, 1, true, or false.
	 */
	public function set_success($_success) {
		if ($_success == 1 || $_success == 0 || $_success == true || $_success = false) {
			return $this->set_helper("bool", $_success, "success");
		}
			return false;
	}

	/** 
	 * @param mixed $_draw Must be 1, 0, true, or false.
	 */
	public function set_blood_draw($_draw) {
		if ($_draw == 1 || $_draw == 0 || $_draw == true || $_draw == false) {
			return $this->set_helper("bool", $_draw, "blood_draw");
		}
		return false;
	}

	/**
	 * @param int $_gauge where 14 <= $_gauge <= 24, $_gauge divisible by 2
	 */
	public function set_gauge($_gauge) {
		if (($_gauge > 13 && $_gauge < 25) && ($_gauge%2 == 0)) {
			return $this->set_helper("int", $_gauge, "gauge");
		}
		return false;
	}

	/**
	 * @param int $_attempts
	 * @todo Is there a max number of attempts?
	 */
	public function set_attempts($_attempts) {
		return $this->set_helper("int", $_attempts, "attempts");
	}

	/**
	 * @todo document
	 */
	public function set_time($time) {
		//do some stuff to convert $time to a string
		$time_obj = new FisdapTime($time);
		$time = $time_obj->get_military_time();
		return $this->set_helper("string", $time, "time");
	}

	/* 
	 * Returns an array in display order with "id => site name"
	 *
	 * @TODO untested
	 */
	public static function get_iv_sites() {
		return self::data_table_helper("IvSiteTable", "IvSite_id", "IvSite", "DisplayOrder");
	}

	/* 
	 * Returns an array in display order with "id => fluid name"
	 *
	 * @TODO untested
	 */
	public static function get_iv_fluids() {
		return self::data_table_helper("IvFluidTable", "IvFluid_id", "IvFluid", "DisplayOrder");
	}

	/**
	 * @todo can we move this to the parent class and use constants for the class-specific info (ID field name)?
	 */
	public static function getBySQL($result) {
		return self::sql_helper($result, "iv", self::id_field);
	}

	public function get_summary() {
		$summary = null;

		//Print Success
		if ($this->success) {
			$summary .= "Successful ";
		} else {
			$summary .= "Unsuccessful ";
		}

		//Print IVorIO
		if ($this->ivorio) {
			$summary .= "IO";
		} else {
			$summary .= "IV";
		}
		return $summary;
	}

	public function get_mobile_summary() {
		$sites = self::get_iv_sites();
		$summary = "<div style='font-weight:bold;'>";
		
		//Print Success
		if ($this->success) {
			$summary .= "Successful ";
		} else {
			$summary .= "Unsuccessful ";
		}

		//Print IVorIO
		if ($this->ivorio) {
			$summary .= "IO ";
		} else {
			$summary .= "IV ";
		}

		if ($this->get_performed_by() == 0) {
			$summary .= "(Performed)";
		} else if ($this->get_performed_by() == 1) {
			$summary .= "(Observed)";
		}

		$summary .= "</div><div>";
		$summary .= $this->gauge." gauge, ".$sites[$this->site].", ".$this->attempts." attempts</div>";
		return $summary;	
	}

	public function get_hook_ids() {
		Assert::is_true($this->shift_id > 0);

		$shift = new Shift($this->shift_id);
		$shift_type = $shift->get_type();

		if ($shift_type == 'field') {
			return array(26, 27);
		} else if ($shift_type == 'clinical') {
			return array(56, 57);
		} else if ($shift_type == 'lab') {
			return array(79, 80);
		} else {
			return array();
		}	
	}
}
?>
