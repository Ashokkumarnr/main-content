<?php

require_once("DataModel.inc");
require_once("interfaces.inc");

/*
 * CLASS Airway
 *
 * Provides the following methods:
 *   get/set airway_type, et_size, success, time
 *   inherited: 
 *     get id
 *     get/set performed, subject, run_id, shift_id, student_id
 *   static:  
 *     getAirwaysBySQL
 *     get_airway_types
 *   static inherited:
 *     get_subject_types
 */
class Airway extends DataModel {

	/**
	 * FIELDS
	 */
	protected $et_size;
	protected $success;
	protected $time;
	protected $type;
	protected $attempts;
	const dbtable = "ALSAirwayData";
	const id_field = "Airway_id";

	/** 
	 * CONSTRUCTOR
	 */
	public function __construct($id=null) {
		parent::__construct();
		if (!$this->construct_helper($id, self::dbtable, self::id_field)) {
			return false;
		}
	}

	/**
	 * METHODS
	 */

	public function set_attempts($attempts) {
		return $this->set_helper("int", $attempts, "attempts");
	}

	public function set_etsize($size) {
		/* If size ends in .0 or .5 and 2 < size < 10 */
		if (is_numeric($size) && ($size % .5) == 0 && $size >= 2 && $size <= 10) {
			return $this->set_helper('float', $size, "et_size");
		}
			return false;
	}

	public function set_success($success) {
		return $this->set_helper("bool", $success, "success");
	}

	public function set_time($time) {
		//$this->time = $time;
		$time_obj = new FisdapTime($time);
		$time = $time_obj->get_military_time();
		return $this->set_helper("string", $time, "time");
	}

	public function set_type($type) {
		$result = $this->id_or_string_helper($type, "get_airway_types");
		return $this->set_helper("int", $result, "type");
	}

	public static function get_airway_types() {
		return self::data_table_helper("ALSAirTypeTable", "ALSAirType_id", "ALSAirType");
	}

	public function get_hook_ids() {
		Assert::is_true($this->shift_id > 0);

		$shift = new Shift($this->shift_id);
		$shift_type = $shift->get_type();

		if ($shift_type == 'field') {
			return array(36);
		} else if ($shift_type == 'clinical') {
			return array(59);
		} else if ($shift_type == 'lab') {
			return array(82);
		} else {
			return array();
		}	
	}

	/**
	 * FUNCTION getBySQL
	 *
	 * Creates Airway objects based on a MySQL result set so the database doesn't need to be hit more than once.
	 *
	 * @param result A result set containing on or more complete ALSAirwayData rows. The rows should be complete ALSAirwayData records, or we have to hit the DB anyway, which completely and totally defeats the purpose. 
	 * @pre The result set CANNOT use alias names.
	 * @return array An array of Airwayss.
	 * @TODO untested
	 * @TODO can we move this to the parent class and use constants for the class-specific info (ID field name)?
	 */
	public static function getBySQL($result) {
		return self::sql_helper($result, "airway", self::id_field);
	}

	public function get_summary() {
		$types = self::get_airway_types();
		$summary = null;
		
		//Print Success
		if ($this->success) {
			$summary .= "Successful ";
		} else {
			$summary .= "Unsuccessful ";
		}

		$summary .= $types[$this->type]." ";

		return $summary;
	}

	public function get_mobile_summary() {
		$types = self::get_airway_types();
		$summary = "<div style='font-weight:bold;'>";
		
		//Print Success
		if ($this->success) {
			$summary .= "Successful ";
		} else {
			$summary .= "Unsuccessful ";
		}

		$summary .= $types[$this->type]." ";

		if ($this->get_performed_by() == 0) {
			$summary .= "(Performed)";
		} else if ($this->get_performed_by() == 1) {
			$summary .= "(Observed)";
		}

		$summary .= "</div><div>";
		$summary .= $this->attempts." attempts</div>";
		return $summary;	
		//STUB
	}


	public function get_fieldmap() {
		return array("Run_id" => "run_id",
				"Student_id" => "student_id",
				"PerformedBy" => "performed_by",
				"NumAttempts" => "attempts",
				"Success" => "success",
				"Type" => "type",
				"ETSize" => "et_size",
				"Shift_id" => "shift_id",
				"ETTime" => "time",
				"SubjectType_id" => "subject_type",
				"Assessment_id"=>"assessment_id",
			);
	}
}
?>
