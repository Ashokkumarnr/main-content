<?php

// where should these constants be placed permanently?
define('DEFAULT_ASSIGNMENT_REVIEW',1);
define('SINGLETON_ASSIGNMENT_REVIEW',2);


/**
 * This class models testing projects (e.g., MHP, 
 * ERP, OSPE, etc).
 */
class TestCreationProject {
    var $FAVORABLE_VALUE_THRESHOLD = 1;
    var $assignment_review_type = DEFAULT_ASSIGNMENT_REVIEW;
	var $active;
    var $id;
    var $name;
    var $abbreviation;
    var $field_map;
    var $logger;

    /**
     *
     */
    function TestCreationProject() {
        $this->logger =& Logger::get_instance();
        $this->field_map = array(
            'db' => array(
				'Active'=>'active',
                'Project_id'=>'id',
                'ProjectName'=>'name',
                'ProjectAbbrev'=>'abbreviation',
                'AssignmentReviewType'=>'assignment_review_type',
            )
        );
    }//TestCreationProject


    /**
     *
     */
    function get_assignment_review_type() {
        return $this->assignment_review_type;
    }//get_assignment_review_type



    ///////////////////////////////////////////////////////////////////////////
    // culled functions ... maybe useful sometime? 
    ///////////////////////////////////////////////////////////////////////////

    ///**
    // * Helper function for the validity status updater.  Here, we handle
    // * items with exactly one consensus review.
    // *
    // * @param TestItemReview $review a consensus review for this item
    // * @return int the updated validity status
    // */
    //function update_status_from_consensus_review($review) {
    //    if ( $review->is_favorable() ) {
    //        $this->logger->log(
    //            'TestItem#'.$item->id.': favorable consensus review... validating.',
    //            $this->logger->INFO
    //        );
    //        $item->set_validity(VALIDATED_ITEM_VALIDITY);
    //        $item->save();
    //        return VALIDATED_ITEM_VALIDITY;
    //    } else {
    //        $this->logger->log(
    //            'TestItem#'.$item->id.': unfavorable consensus review... needs revision',
    //            $this->logger->INFO
    //        );
    //        $item->set_validity(NEEDS_REVISION_ITEM_VALIDITY);
    //        $item->save();
    //        return NEEDS_REVISION_ITEM_VALIDITY;
    //    }//else
    //}//update_status_from_consensus_review


    ///**
    // * Helper function for the review validity updater.  Here, we handle
    // * items that have enough assignment reviews to make a validity determination.
    // *
    // * @param array $reviews a list of assignment reviews for this item
    // * @return int the updated validity status
    // */
    //function update_status_from_assignment_reviews($reviews) {
    //    // for each of the keys in the expected ratios array, check 
    //    // the ratio of reviews that have a corresponding value of 
    //    // 1 or greater against the expected ratio
    //    $dummy_review = new TestItemReview();
    //    foreach( $dummy_review->expected_ratios as $item=>$ratio ) {
    //        $num_favorable_answers = 0;   
    //        foreach( $reviews as $review ) {
    //            if ( intval($review->$item) >= 1 ) {
    //                $num_favorable_answers++;
    //            }//if
    //        }//foreach

    //        if ( ( $found_ratio = ($num_favorable_answers / count($reviews))*100 ) < $ratio ) {
    //            // mark the item as NEEDS_REVISION and exit
    //            $this->logger->log(
    //                'TestItem#'.$item->id.': '.$item.' was '.$found_ratio.'% (< '.$ratio.'%). marking item as needing revision',
    //                $this->logger->INFO
    //            );
    //            $item->set_validity(NEEDS_REVISION_ITEM_VALIDITY);
    //            $item->save();
    //            return NEEDS_REVISION_ITEM_VALIDITY;
    //        }//if

    //        $this->logger->log(
    //            'TestItem#'.$item->id.': '.$item.' was '.$found_ratio.'%, expected >= '.$ratio.'%',
    //            $this->logger->DEBUG
    //        );
    //    }//foreach

    //    // if the ratios met our standards, mark the item as VALID 
    //    $this->logger->log(
    //        'TestItem#'.$item->id.': passed validation.  updating item status to valid',
    //        $this->logger->INFO
    //    );
    //    $item->set_validity(VALIDATED_ITEM_VALIDITY);
    //    $item->save();
    //    return VALIDATED_ITEM_VALIDITY;
    //}//update_status_from_assignment_reviews
}//class TestCreationProject
?>
