<?php

/*----------------------------------------------------------------------*
  |                                                                      |
  |     Copyright (C) 1996-2006.  This is an unpublished work of         |
  |                      Headwaters Software, Inc.                       |
  |                         ALL RIGHTS RESERVED                          |
  |     This program is a trade secret of Headwaters Software, Inc.      |
  |     and it is not to be copied, distributed, reproduced, published,  |
  |     or adapted without prior authorization                           |
  |     of Headwaters Software, Inc.                                     |
  |                                                                      |
 *----------------------------------------------------------------------*/

require_once("Model.inc");

/**
 * @author Ian Young
 * @todo maybe we should just be using data_table_helper instead
 */
class gender extends AbstractModel {

	protected $id;
	protected $string;

	const dbtable = "GenderTable";
	const idfield = "Gender_id";

	//TODO static? this doesn't seem right
	public function __construct($id=null) {
		return self::construct_helper($id, self::dbtable, self::idfield);
	}

	public function get_summary() {
		return $this->get_string();
	}

	public static function getBySQL($result) {
		return parent::sql_helper($result, "gender", self::id_field);
	}

	public function get_fieldmap() {
		return array(
			//"Gender_id" => "id",
				'GenderTitle' => 'string'
				);
	}

}

?>
