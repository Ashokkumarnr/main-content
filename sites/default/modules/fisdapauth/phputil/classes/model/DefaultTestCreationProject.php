<?php

require_once('TestCreationProject.php');

class DefaultTestCreationProject extends TestCreationProject {
    function DefaultTestCreationProject() {
        parent::TestCreationProject();
        $this->assignment_review_type = DEFAULT_ASSIGNMENT_REVIEW;
    }//DefaultTestCreationProject


    /**
     * Return the array of reviews, with any that are not strictly assignment
     * or consensus weeded out.
     *
     */
    function cull_funky_reviews($reviews) {
        if ( $reviews ) {
            $culled_indices = array();
            $num_reviews = count($reviews);
            for( $i=0 ; $i < $num_reviews ; $i++ ) {
                $review =& $reviews[$i];
                if ( !in_array(
                         $review->get_mode(),
                         array('assignment','consensus')
                     ) ) {
                    $culled_indices[] = $i;
                } else if ( 
                    $review->get_mode() == 'assignment' && 
                    $review->get_assignment_review_type() != 
                    DEFAULT_ASSIGNMENT_REVIEW 
                ) {
                    $culled_indices[] = $i;
                }//else if
            }//for
            foreach( $culled_indices as $index ) {
                unset($reviews[$index]);
            }//foreach
        }//if
        return array_values($reviews);
    }//cull_funky_reviews


    /**
     * Add N-1 duplicates of each consensus review, where N is the number
     * of people in the review group.  Note that this will only clone the 
     * reviews, and will not modify the number_in_group setting.
     */
    function split_consensus_reviews($reviews) {
        $additional_reviews = array();
        if ( $reviews ) {
            foreach( $reviews as $review ) {
                if ( $review->get_mode() == 'consensus' ) {
                    for( $i=1 ; $i < $review->get_number_in_group() ; $i++ ) {
                        $review_clone =& $review->php4_compat_clone();
                        $additional_reviews[] =& $review_clone;
                    }//for
                }//if
            }//foreach
        }//if

        if( $additional_reviews ) {
            foreach( $additional_reviews as $additional_review ) {
                $reviews[] =& $additional_review;
            }//foreach
        }//if

        return $reviews;
    }//split_consensus_reviews


    function update_item_review_status(&$item,$email_warnings=true) {
        // get all of the reviews 
        $reviews = $item->get_reviews();

        // cull any reviews that aren't straight-up assignment or consensus
        $reviews = $this->cull_funky_reviews($reviews);

        // treat consensus reviews as multiple reviews for the purpose of
        // validity calculations
        $reviews = $this->split_consensus_reviews($reviews);

        // if there aren't enough reviews to make a distinction, set the
        // validity to undetermined (unless there's a review that says the
        // answer is incorrect, or that the item is biased)
        if ( !is_array($reviews) || count($reviews) < 3 ) {
            return $this->handle_fewer_than_three_reviews($item,$reviews);
        }//if
        
        // yay! there are enough reviews to make a validity distinction
        return $this->update_item_status_from_reviews($item,$reviews);
    }//update_item_review_status


    /** 
     *
     */
    function handle_fewer_than_three_reviews(&$item,$reviews) {
        if ( is_array($reviews) ) {
            $dummy_review = new TestItemReview();
            $expected_ratios = $dummy_review->get_expected_ratios();

            $negative_thresholds = $expected_ratios;
            array_walk(
                $negative_thresholds,
                create_function(
                    '&$ratio,$key',
                    '$ratio = 100-intval($ratio);'
                )
            );
            if ( !is_array($negative_thresholds) ) {
                echo 'error generating negative thresholds';
                exit(1);
            }//if

            foreach( $negative_thresholds as $question=>$ratio ) {
                // tally values not meeting the acceptable threshold
                $num_unfavorable_answers = 0;   
                foreach( $reviews as $review ) {
                    if ( intval($review->$question) < 
                         $this->FAVORABLE_VALUE_THRESHOLD ) {
                        $num_unfavorable_answers++;
                    }//if
                }//foreach

                if ( ( $found_ratio = ($num_unfavorable_answers / 3)*100 ) > 
                     $ratio ) {
                    // mark the item as NEEDS_REVISION and exit
                    $item->set_validity(NEEDS_REVISION_ITEM_VALIDITY);
                    $item->save();
                    return NEEDS_REVISION_ITEM_VALIDITY;
                }//if
            }//foreach
        }//if

        $item->set_validity(UNDETERMINED_ITEM_VALIDITY);
        $item->save();
        return UNDETERMINED_ITEM_VALIDITY;
    }//handle_fewer_than_three_reviews


    /**
     * Given an array of 3 or more review objects, set the validity of this
     * item.
     *
     * @param array $reviews 3 or more reviews to consider
     */
    function update_item_status_from_reviews(&$item,$reviews) {
        $dummy_review = new TestItemReview();
        $expected_ratios = $dummy_review->get_expected_ratios();
        foreach( $expected_ratios as $question=>$ratio ) {
            // tally values meeting the acceptable threshold
            $num_favorable_answers = 0;   
            foreach( $reviews as $review ) {
                if ( intval($review->$question) >= 
                     $this->FAVORABLE_VALUE_THRESHOLD ) {
                    $num_favorable_answers++;
                }//if
            }//foreach

            // abort if the ratio didn't meet our expectations
            if ( ( $found_ratio = ($num_favorable_answers / 
                                   count($reviews))*100 
                 ) < $ratio ) {
                // mark the item as NEEDS_REVISION and exit
                $this->logger->log(
                    'TestItem#'.$item->id.': was '.$found_ratio.
                    '% (< '.$ratio.'%). marking item as needing revision',
                    $this->logger->INFO
                );
                $item->set_validity(NEEDS_REVISION_ITEM_VALIDITY);
                $item->save();
                return NEEDS_REVISION_ITEM_VALIDITY;
            }//if

            $this->logger->log(
                'TestItem#'.$item->id.': '.$question.' = '.$found_ratio.'%, '.
                'expected >= '.$ratio.'%',
                $this->logger->DEBUG
            );
        }//foreach

        // if the ratios met our standards, mark the item as VALID 
        $this->logger->log(
            'TestItem#'.$item->id.': passed validation.  updating item '
            . 'status to valid',
            $this->logger->INFO
        );
        $item->set_validity(VALIDATED_ITEM_VALIDITY);
        $item->save();
        return VALIDATED_ITEM_VALIDITY;
    }//update_item_status_from_reviews


    ///////////////////////////////////////////////////////////////////////////
    // Module: test_creation_project_db_layer.php
    //
    // This module provides the database layer functions for the test creation
    // project model classes.  These are static methods, and must know the 
    // name of the current class when they are invoked.  Without this module,
    // the TestCreationProject subclasses would all generate 
    // TestCreationProject instances instead of instances of the appropriate 
    // subclasses.
    ///////////////////////////////////////////////////////////////////////////
    
    
    /**
     * Fetch the project with the given ID
     */
    function &find_by_id($id) {
        if ( !is_numeric($id) ) {
            return false;
        }//if
    
        $connection_obj =& FISDAPDatabaseConnection::get_instance();
    
        $query = 'SELECT * '.
                 'FROM ProjectTable '.
                 'WHERE Project_id="'.$id.'"';
        $result_set = $connection_obj->query($query);
        if ( !is_array($result_set) || count($result_set) != 1 ) {
            return false;
        }//if
    
        return DefaultTestCreationProject::from_assoc_array($result_set[0]);
    }//find_by_id
    
    
    function &from_assoc_array($db_row) {
        $project =& new DefaultTestCreationProject();
        foreach( $db_row as $field=>$value ) {
            $obj_field = $project->field_map['db'][$field];
            $project->$obj_field = $value;
        }//foreach
        return $project;
    }//from_assoc_array
    
    ///////////////////////////////////////////////////////////////////////////
    // END MODULE
    ///////////////////////////////////////////////////////////////////////////

}//class DefaultTestCreationProject

?>
