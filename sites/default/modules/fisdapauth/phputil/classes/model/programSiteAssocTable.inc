<?php
require_once('phputil/classes/model/abstractTable.inc');
require_once('phputil/classes/model/programSiteAssoc.inc');

/**
 * This class models the DB table ProgramSiteAssoc.
 */
class ProgramSiteAssocTable extends AbstractTable
{
    /**
     * Constructor.
     * @param connection The DB connection.
     */
    function ProgramSiteAssocTable(&$connection)
    {
        parent::AbstractTable($connection, 'ProgramSiteAssoc');
    }

    /**
     * Retrieve a program site associativity.
     * @param id The assoc ID.
     * @return A single ProgramSiteAssoc object, or null if the item was not found.
     */
    function retrieveByAssocId($id)
    {
        if(is_null($id)) return null;

        $statement = 'SELECT * FROM ' .  $this->getTableName() .
            " WHERE Assoc_id={$id}";
        $connection =& $this->getConnection();
        $results = $connection->query($statement);
        if(count($results) != 1) return null;

        return ProgramSiteAssoc::createFromResult($connection, $results[0]);
    }

    /**
     * Determine whether a users program can administer a shared site.
     * @param programId The program ID.
     * @param ambulanceServiceId The ambulance service ID.
     * @return TRUE if the users program can administer the shared site.
     */
    function canCreateSharedShifts($programId, $ambulanceServiceId)
    {
        if (is_null($programId)) return false;
        if (is_null($ambulanceServiceId)) return false;

        $statement = 'SELECT count(*) as N FROM ' .  $this->getTableName() .
            " WHERE Program_id={$programId} AND Site_id={$ambulanceServiceId}" .
            '   AND Approved=1 AND Main=1';
        $connection =& $this->getConnection();
        $results = $connection->query($statement);
        if(count($results) != 1) return false;

        $main = $results[0]['N'];
        return ($main == 1);
    }
}
?>
