<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/

require_once("AbstractModel.inc");

class PilotRating extends AbstractModel {

	protected $type;
	protected $rater;
	protected $rating;
	protected $linked_table;
	protected $link_id;
	protected $shift_id;

	const dbtable = "rate_Data";
	const idfield = "rate_id";

	public function __construct($id=null) {
		return $this->construct_helper($id, self::dbtable, self::idfield);
	}

	public function set_type($type) {
		return $this->set_helper('int', $this->id_or_string_helper($type, 'get_type_names'), 'type');
	}

	public function set_rater($rater) {
		return $this->set_helper('int', $this->id_or_string_helper($rater, 'get_rater_names'), 'rater');
	}

	public function set_rating($rating) {
		return $this->set_helper('int', $rating, 'rating');
	}

	public function set_linked_table($table) {
		return $this->set_helper('string', $table, 'linked_table');
	}

	public function set_link_id($id) {
		return $this->set_helper('int', $id, 'link_id');
	}

	public function set_shift_id($id) {
		return $this->set_helper('int', $id, 'shift_id');
	}

	public function get_summary() {
		//STUB	
	}

	public static function get_type_names() {
		$names = parent::data_table_helper("rate_Type", "type_id", "type_name");
		return $names;
	}

	public static function get_rater_names() {
		$names = parent::data_table_helper("rate_Rater", "rater_id", "rater_name");
		return $names;
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "PilotRating", self::idfield);
	}

	public function get_fieldmap() {
		return array(
			'rate_type' => 'type',
			'rate_rater' => 'rater',
			'rating' => 'rating',
			'rate_link_table' => 'linked_table',
			'rate_link_id' => 'link_id',
			'Shift_id' => 'shift_id',
		);
	}
}
