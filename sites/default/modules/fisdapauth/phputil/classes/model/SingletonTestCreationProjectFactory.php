<?php

require_once('phputil/classes/model/SingletonTestCreationProject.php');

class SingletonTestCreationProjectFactory {
    function &create_project($assoc_array) {
        if ( $assoc_array['AssignmentReviewType'] == SINGLETON_ASSIGNMENT_REVIEW ) {
            return SingletonTestCreationProject::from_assoc_array($assoc_array);
        }//if 

        return false;
    }//create_project
}//SingletonTestCreationProjectFactory

?>
