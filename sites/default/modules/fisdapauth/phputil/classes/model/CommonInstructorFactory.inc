<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/FisdapCaches.inc');
require_once('phputil/classes/FisdapSimpleCache.inc');
require_once('phputil/classes/model/CommonUserFactory.inc');
require_once('phputil/exceptions/FisdapException.inc');

/**
 * A factory for instructors.
 */
final class CommonInstructorFactory {
	const ID_PREFIX = 'id:';
	const USER_NAME_PREFIX = 'username:';

	private $cache;

	protected function __construct() {
		$this->cache = new FisdapSimpleCache('CommonInstructor');
	}

	/**
	 * Retrieve the singleton.
	 * @return The factory.
	 */
	public static function get_instance() {
		static $instance;

		if (is_null($instance)) {
			$instance = new CommonInstructorFactory();
			FisdapCaches::get_instance()->register($instance->cache);
		}

		return $instance;
	}

	/**
	 * Retrieve the underlying cache.
	 * @return FisdapCache The underlying cache.
	 */
	public function get_cache() {
		return $this->cache;
	}

	/**
	 * Retrieve a user by a variety of sources.
	 * <ul>
	 * <li>If the source is an int, it is the instructor ID.
	 * <li>Source may also be common_instructor, common_user, a
	 * user name, or null for the logged in user.
	 * </ul>
	 * @param mixed $source The model source.
	 * @param boolean $must_exist TRUE if the user must be found.
	 * @return common_user | null The user (null if $must_exist=false)
	 * @throws FisdapException If the model can't be retrieved if 
	 * $must_exist=true.
	 */
	public function get_by_source($source=null, $must_exist=true) {
		global $REMOTE_USER;

		Assert::is_boolean($must_exist);
		Assert::is_true(
			Test::is_a($source, 'common_instructor') ||
			Test::is_a($source, 'common_user') ||
			Test::is_int($source) ||
			Test::is_string($source) ||
			Test::is_null($source));

		$this->cache->clear_hit();

		if ($source instanceof common_instructor) {
			$this->add($source);

			if (!$source->is_valid()) {
				if ($must_exist) {
					throw new FisdapException(self::get_msg($source, null, $source));
				}

				return null;
			}

			return $source;
		}

		$instructor = null;
		$user = null;
		$user_factory = CommonUserFactory::get_instance();

		try {
			if (Test::is_int($source)) {
				$key = self::ID_PREFIX . $source;
				if ($this->cache->contains($key)) {
					return $this->cache->get($key);
				}

				$instructor = new common_instructor($source);
			} elseif (is_string($source)) {
				$key = self::USER_NAME_PREFIX. $source;
				if ($this->cache->contains($key)) {
					return $this->cache->get($key);
				}

				$user = $user_factory->get_by_source($source, $must_exist);
			} elseif ($source instanceof common_user) {
				$user = $user_factory->get_by_source($source, $must_exist);
			} else {
				$source = "REMOTE_USER[$REMOTE_USER]";
				$user = $user_factory->get_by_source($REMOTE_USER, $must_exist);
			}
		}
		catch (FisdapException $e) {
			if ($must_exist) {
				throw new FisdapException(self::get_msg($source, $user, $instructor), $e);
			}

			$instructor = null;
			$user = null;
		}

		// If we got a user, try to load the instructor.
		if (is_null($instructor) && !is_null($user)) {
			if (!$user->is_valid() || !$user->is_instructor()) {
				throw new FisdapException(self::get_msg($source, $user, $instructor));
			}

			return $this->get_by_source($user->get_instructor_id(), $must_exist);
		}

		if ($must_exist) {
			if (is_null($instructor) || !$instructor->is_valid()) {
				throw new FisdapException(self::get_msg($source, $user, $instructor));
			}
		}

		if (!is_null($instructor)) {
			$this->add($instructor);

			if (!$instructor->is_valid()) {
				return null;
			}
		}

		return $instructor;
	}

	/**
	 * Get all the instructors for a given program
	 */
	public function get_instructors_by_program($program_id) {
		Assert::is_true(Test::is_int($program_id));

		$return_array = array();

		$query = "SELECT Instructor_id from InstructorData " .
			"WHERE ProgramId=$program_id ORDER BY LastName";

		$connection = FISDAPDatabaseConnection::get_instance();
		$results = $connection->query($query);

		// Create a user for each ID we get in return
		foreach ($results as $row) {
			$return_array[] = self::get_by_source($row['Instructor_id']);
		}

		return $return_array;
	}


	/**
	 * Add an instructor to the cache.
	 * @param common_instructor $instructor The instructor.
	 */
	private function add(common_instructor $instructor) {
		if (!$instructor->is_valid()) {
			return;
		}

		$this->cache->put(self::ID_PREFIX. $instructor->get_instructor_id(), $instructor);
		$this->cache->put(self::USER_NAME_PREFIX . $instructor->get_username(), $instructor);
	}

	private static function get_msg($source, $user, $instructor) {
		$msgs = array();

		if (!Test::is_object($source)) {
			$msgs[] = "source[$source]";
		}

		if (!is_null($user)) {
			if ($user->is_valid()) {
				$msgs[] = 'user name[' . $user->get_username() . ']';
				$msgs[] = 'user ID[' . $user->get_UserAuthData_idx() . ']';
				$msgs[] = 'user instructor ID[' . $user->get_instructor_id() . ']';
			} else {
				$msgs[] = 'invalid user';
			}
		}

		if (!is_null($instructor)) {
			if ($instructor->is_valid()) {
				$msgs[] = 'instructor user name[' . $instructor->get_username() . ']';
				$msgs[] = 'instructor ID[' . $instructor->get_instructor_id() . ']';
			} else {
				$msgs[] = 'invalid instructor';
			}
		}

		$msg = 'Unable to retrieve instructor';
		if (count($msgs)) {
			$msg .= '; ' . join(', ', $msgs);
		}

		return $msg;
	}
}
?>
