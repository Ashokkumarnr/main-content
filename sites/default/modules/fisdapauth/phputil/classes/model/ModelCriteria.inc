<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/model/AbstractModel.inc');
require_once('phputil/classes/model/OrderBy.inc');

/**
 * Criteria are used for things like search criteria.
 * This class all but omits the need for many factory queries on a AbstractModel.
 * If we had a schema available to the AbstractModel, this class could literally
 * allow more criteria, such as LIKE, IN, etc. and build the query directly.
 *
 * This class wraps any model and delegates the calls to it.
 * In addition, it keeps track of which setters have been called 
 * so that a criteria user can use only those fields that have 
 * been set.  This is helpful especially in the cases where the actual
 * model provides default values and does NOT allow NULL as a value.
 *
 * This class also provides support for setting the ID.
 *
 * Usage:
 * - Invoke the setters on items you want to query.
 * - Pass the criteria to the SQL builder.
 * - The builder will build the SQL depending on which setters were invoked. 
 *
 * <pre>
 * Typical usage of this class is as follows; not that you can use 
 * set_not_in...() anywhere you see set_in().
 *
 * By default, all clauses are AND'ed together.  To OR, use the 
 * set_or_criteria().  Note that all clauses in the criteria are at the same 
 * parenthesis level.
 *
 * $c = new ModelCriteria(new a-model-class());
 * 
 * // If this is a select, and we want to restrict the returned fields.
 * $c->set_field_names(array(fn1, fn2, ...));
 * $c->add_field_name(field_name);
 *
 * // If you wish to restrict the number of results.
 * $c->set_statement_limit($max_rows);
 *
 * // If you are using this criteria to build a statement involving multiple
 * // tables, you will need an alias.
 * $c->set_table_name_alias($alias);
 *
 * // Restrict field 'f1' to the values(1,2,3).
 * $c->set_in('f1', array(1,2,3));
 *
 * // .... condition is now: 
 * //      f1 IN (1,2,3)
 *
 * // Restrict field 'f2' to be in the results of another SELECT statement.
 * $c2 = new ModelCritiera(new b-model-class());
 * $c2->set_f10(13);
 * $c2->add_field_name('fu');
 *
 * $c->set_in_criteria('f2', $c2);
 *
 * // .... condition is now: 
 * //      f1 IN (1,2,3) AND f2 IN (SELECT fu FROM ... WHERE f10=13)
 *
 * // If you want to negate some criteria.
 * $c3 = new ModelCritiera(new a-model-class());
 * $c3->set_f9(5);
 * $c3->set_in('f8', array(-1,-2));
 *
 * $c->add_not_criteria($c3);
 *
 * // .... condition is now: 
 * //      f1 IN (1,2,3) AND f2 IN (SELECT fu FROM ... WHERE f10=13) 
 * //      AND NOT(f9=5 AND f8 IN (-1,-2))
 *
 * // If you want to OR a set of critieria.
 * $c4 = new ModelCritiera(new a-model-class());
 * $c4->set_f7(3);
 * $c4->set_not_in('f6', array(null, 5)
 *
 * $c->add_or_criteria($c4);
 *
 * // .... condition is now: 
 * //      f1 IN (1,2,3) AND f2 IN (SELECT fu FROM ... WHERE f10=13) 
 * //      AND NOT(f9=5 AND f8 IN (-1,-2))
 * //      OR (f7=3 AND f6 NOT IN (null,5))
 *
 * // Need to add your own clause  Note that these clauses are all at the 
 * // same level..
 * $c->add_and_clause('foo=345');
 * $c->add_or_clause('foo=456');
 *
 * // Want distinct results?
 * $c->set_distinct(true);
 * </pre>
 */
final class ModelCriteria {
    private $and_clauses = array();
    private $distinct = false;
    private $field_names = array();
    private $id;
    private $in = array();                  // field name -> array of values
    private $in_criteria = array();         // field name -> criteria
    private $limit;
    private $model;
    private $not_criteria = array();        // array of criteria
    private $not_in = array();              // field name -> array of values
    private $not_in_criteria = array();     // field name -> criteria
    private $or_criteria = array();        // array of criteria
    private $or_clauses = array();
    private $table_name_alias;

    /**
     * The setters namer, the name is stored as the value.
     */
    private $methods_called = array();

    /**
     * Order by, an array of OrderBy.
     */
    private $order_by = array();

    /**
     * Constructor.
     * @param AbstractModel $model The underlying model.
     */
	public function __construct(AbstractModel $model) {
        $this->model = $model;
	}

    /**
     * Should the results be distinct?
     * @return boolean TRUE if distinct.
     */
    public function is_distinct() {
        return $this->distinct;
    }

    /**
     * Indicate whether distinct results should be returned.
     * @param boolean $distinct TRUE if distinct results should be returned.
     */
    public function set_distinct($distinct) {
        Assert::is_boolean($distinct);
        $this->distinct = $distinct;
    }

    /**
     * Retieve the clauses to AND.
     * @return array The clauses.
     */
    public function get_and_clauses() {
        return $this->and_clauses;
    }

    /**
     * Add a clauses to AND.
     * @return string $clause The clause to AND.
     */
    public function add_and_clause($clause) {
        Assert::is_not_empty_trimmed_string($clause);
        $this->and_clauses[] = $clause;
    }

    /**
     * Retieve the clauses to OR.
     * @return array The clauses.
     */
    public function get_or_clauses() {
        return $this->or_clauses;
    }

    /**
     * Add a clauses to OR.
     * @return string $clause The clause to OR.
     */
    public function add_or_clause($clause) {
        Assert::is_not_empty_trimmed_string($clause);
        $this->or_clauses[] = $clause;
    }

    /**
     * Retieve the NOT criteria.
     * @return array The criterias.
     */
    public function get_not_criteria() {
        return $this->not_criteria;
    }

    /**
     * Add a NOT criteria.
     * The criteria will be formed as a number of clauses and NOT'ed with the 
     * this criteria..
     * @param ModelCriteria $criteria The criteria.
     */
    public function add_not_criteria(ModelCriteria $criteria) {
        $this->not_criteria[] = $criteria;
    }

    /**
     * Retieve the OR criteria.
     * @return array The criteria.
     */
    public function get_or_criteria() {
        return $this->or_criteria;
    }

    /**
     * Set the OR criteria.
     * The criteria will be formed as a number of clauses and OR'ed with the 
     * this criteria..
     * @param ModelCriteria $criteria The criteria.
     */
    public function add_or_criteria(ModelCriteria $criteria) {
        $this->or_criteria[] = $criteria;
    }

    /**
     * Retieve the NOT IN criteria.
     * @return array The criterias (field->criteria).
     */
    public function get_not_in_criteria() {
        return $this->not_in_criteria;
    }

    /**
     * Set the NOT IN criteria.
     * The criteria will be formed as a select statement.
     * If criteria is specified, it should normally have exactly 1 field name 
     * set.
     * @param string $field_name The field name this pertains to.
     * @param ModelCriteria | null $criteria The criteria or null to clear.
     */
    public function set_not_in_criteria($field_name, $criteria) {
        Assert::is_string($field_name);
        Assert::is_true(
            Test::is_null($criteria) || Test::is_a($criteria, 'ModelCriteria'));

        if (is_null($criteria)) {
            unset($this->not_in_criteria[$field_name]);
        }
        else {
            $this->not_in_criteria[$field_name] = $criteria;
        }
    }

    /**
     * Retieve the IN criteria.
     * @return array The criterias (field->criteria).
     */
    public function get_in_criteria() {
        return $this->in_criteria;
    }

    /**
     * Set the IN criteria.
     * The criteria will be formed as a select statement.
     * If criteria is specified, it should normally have exactly 1 field name 
     * set.
     * @param string $field_name The field name this pertains to.
     * @param ModelCriteria | null $criteria The criteria or null to clear.
     */
    public function set_in_criteria($field_name, $criteria) {
        Assert::is_string($field_name);
        Assert::is_true(
            Test::is_null($criteria) || Test::is_a($criteria, 'ModelCriteria'));

        if (is_null($criteria)) {
            unset($this->in_criteria[$field_name]);
        }
        else {
            $this->in_criteria[$field_name] = $criteria;
        }
    }

    /**
     * Retieve the IN values.
     * @return array The values (field->array(values)).
     */
    public function get_in() {
        return $this->in;
    }

    /**
     * Set the IN values.
     * @param string $field_name The field name this pertains to.
     * @param array | null $values The values or null to clear.
     */
    public function set_in($field_name, $values) {
        Assert::is_string($field_name);
        Assert::is_true(Test::is_null($values) || Test::is_array($values));

        if (is_null($values)) {
            unset($this->in[$field_name]);
        }
        else {
            $this->in[$field_name] = $values;
        }
    }

    /**
     * Retieve the NOT IN values.
     * @return array The values (field->array(values)).
     */
    public function get_not_in() {
        return $this->not_in;
    }

    /**
     * Set the NOT IN values.
     * @param string $field_name The field name this pertains to.
     * @param array | null $values The values or null to clear.
     */
    public function set_not_in($field_name, $values) {
        Assert::is_string($field_name);
        Assert::is_true(Test::is_null($values) || Test::is_array($values));

        if (is_null($values)) {
            unset($this->not_in[$field_name]);
        }
        else {
            $this->not_in[$field_name] = $values;
        }
    }

    /**
     * Get the alias for the table name.
     * @return string | null The table name alias or null for none.
     */
    public function get_table_name_alias() {
        return $this->table_name_alias;
    }

    /**
     * Set the table name alias.
     * @param string | null The table name alias to use to null for none.
     */
    public function set_table_name_alias($name) {
        Assert::is_true(
            Test::is_not_empty_trimmed_string($name) || Test::is_null($name));

        if (!is_null($name)) {
            $name = trim($name);
        }

        $this->table_name_alias = $name;
    }

    /**
     * Retrieve the fields to include.
     * @return array The fields or the empty array for none.
     */
    public function get_field_names() {
        return $this->field_names;
    }

    /**
     * Sets the field names to include.
     * @param array $names The field names or the empty array for none.
     */
    public function set_field_names($names) {
        Assert::is_array($names);

        $this->field_names = $names;
    }

    /**
     * Add a field name to include.
     * @param string $name The field name to include.
     */
    public function add_field_name($name) {
        Assert::is_not_empty_trimmed_string($name);

        $this->field_names[] = trim($name);
    }

    /**
     * Retrieve the ordering.
     * @return array Each element is OrderBy.
     */
    public function get_order_by() {
        return $this->order_by;
    }

    /**
     * Add an order by criteria.
     * @param OrderBy $order_by The order by clause.
     */
    public function add_order_by(OrderBy $order_by) {
        $this->order_by[] = $order_by;
    }

    /**
     * Determine if a method was called.
     * Normally the name of a setter is passed, i.e. set_fubar.
     * A static method can be tested too.
     * @param $method_name The setter.
     */
    public function was_called($method_name) {
        return in_array($method_name, $this->methods_called);
    }

    /**
     * Delegate a method call to the underlying model.
     * Calls to non existent functions will generate a PHP error.
     * @param string $function The name of the function.
     * @param mixed $params The parameters passed to the function.
     * @return mixed | null The value returned by the function call.
     */
	public function __call($function, $params) {
        $this->track($function);
        return call_user_func_array(array($this->model, $function), $params);
    }

    /**
     * Delegate a static call to the underlying model.
     * Calls to non existent functions will generate a PHP error.
     * Not available until PHP 5.3.0.
     * @param string $function The name of the function.
     * @param mixed $params The parameters passed to the function.
     * @return mixed | null The value returned by the function call.
     */
	public static function __callStatic($function, $params) {
        $this->track($function);
        return call_user_func_array(array($this->model->get_class(), $function), $params);
    }

    /**
     * Get the ID.
     * @return int | null The ID.
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * Set the ID.
     * @param int $id The ID.
     */
    public function set_id($id) {
        Assert::is_int($id);

        $this->track('set_id');
        $this->id = $id;
    }

    /**
     * Get the statement limit.
     * @return int | null The limit or NULL for no limit.
     */
    public function get_statement_limit() {
        return $this->limit;
    }

    /**
     * Set the statement limit.
     * @param int | null $limit The limit or NULL for no limit.
     */
    public function set_statement_limit($limit) {
        Assert::is_true(Test::is_int($limit) || Test::is_null($limit));

        $this->limit = $limit;
    }

    /**
     * Retrieve the underlying model.
     * You can normally just call getters/setters via this class, however all 
     * calls are tracked.
     * @return AbstractModel The model.
     */
    public function get_model() {
        return $this->model;
    }

    private function track($function) {
        if (!$this->was_called($function)) {
            $this->methods_called[] = $function;
        }
    }
}
?>
