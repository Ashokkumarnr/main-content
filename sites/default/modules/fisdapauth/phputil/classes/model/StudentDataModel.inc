<?php

/*----------------------------------------------------------------------*
  |                                                                      |
  |     Copyright (C) 1996-2010.  This is an unpublished work of         |
  |                      Headwaters Software, Inc.                       |
  |                         ALL RIGHTS RESERVED                          |
  |     This program is a trade secret of Headwaters Software, Inc.      |
  |     and it is not to be copied, distributed, reproduced, published,  |
  |     or adapted without prior authorization                           |
  |     of Headwaters Software, Inc.                                     |
  |                                                                      |
*----------------------------------------------------------------------*/

require_once('phputil/classes/model/UserAbstractModel.inc');
require_once('phputil/classes/DateTime.inc');
require_once('phputil/classes/model/AccountTypeModel.inc');
require_once('phputil/handy_utils.inc');


class StudentDataModel extends UserAbstractModel {
	protected $first_name;
	protected $last_name;
	protected $program;
	protected $program_id;
	protected $mentor_id;
	protected $username;
	protected $box_number;
	protected $address;
	protected $city;
	protected $state;
	protected $zip_code;
	protected $home_phone;
	protected $work_phone;
	protected $email;
	protected $pager;
	protected $birth_date;
	protected $gender;
	protected $ethnicity;
	protected $class_year;
	protected $emt_year;
	protected $class_month;
	protected $research_consent;
	protected $good_data_flag;
	protected $class_assigned;
	protected $max_field_shifts;
	protected $max_clinic_shifts;
	protected $max_lab_shifts;
	protected $cell_phone;
	protected $contact_phone;
	protected $contact_name;
	protected $contact_relation;
	protected $testing_expiration_date;
	protected $default_goal_set_it;

	const VALIDATION = 'validation';
	const DB_TABLE = "StudentData";
	const ID_FIELD = "Student_id";

	public function get_user_type() {
		return 'student';
	}

	public function __construct($id=null) {
		$ret = self::construct_helper($id, self::DB_TABLE, self::ID_FIELD);
		parent::__construct($id);
		return $ret;
	}

	public function get_fieldmap() {
		return array(
			'FirstName' => 'first_name',
			'LastName' => 'last_name',
			'Program' => 'program',
			'Program_id' => 'program_id',
			'Mentor_id' => 'mentor_id',
			'UserName' => 'username',
			'Box_Number' => 'box_number',
			'Address' => 'address',
			'City' => 'city',
			'State' => 'state',
			'ZipCode' => 'zip_code',
			'HomePhone' => 'home_phone',
			'WorkPhone' => 'work_phone',
			'EmailAddress' => 'email',
			'Pager' => 'pager',
			'BirthDate' => 'birth_date',
			'Gender' => 'gender',
			'Ethnicity' => 'ethnicity',
			'Class_Year' => 'class_year',
			'EMT_Year' => 'emt_year',
			'ClassMonth' => 'class_month',
			'ResearchConsent' => 'research_consent',
			'GoodDataFlag' => 'good_data_flag',
			'ClassAssigned' => 'class_assigned',
			'MaxFieldShifts' => 'max_field_shifts',
			'MaxClinicShifts' => 'max_clinic_shifts',
			'MaxLabShifts' => 'max_lab_shifts',
			'CellPhone' => 'cell_phone',
			'ContactPhone' => 'contact_phone',
			'ContactName' => 'contact_name',
			'ContactRelation' => 'contact_relation',
			'TestingExpDate' => 'testing_expiration_date',
			'DefaultGoalSet_id' => 'default_goal_set_it'
		);
	}
	protected function load_batch_validation_rules() {
		$this->fields = array(
			'first_name' => array(
				'max_len' => 30,
				'is_required' => true,
				'is_required' => true,
				'min_len' => 1
			),
			'last_name' => array(
				'max_len' => 30,
				'is_required' => true,
				'min_len' => 1
			),
			'username' => array(
				'max_len' => 24,
				'is_required' => true,
				'is_valid_fisdap_username' => true
			),
			'email' => array(
				'max_len' => 50,
				'is_required' => false,
				'min_len' => 0
			),
			'class_year' => array(
				'is_numeric_or_null' => true 
			),
			'class_month' => array(
				'is_numeric_or_null' => true
			),
			'address' => array(
				'max_len' => 50,
				'min_len' => 1
			),
			'city' => array(
				'max_len' => 50,
				'min_len' => 1
			),
			'state' => array(
				'is_us_state' => 1
			),
			'zip_code' => array(
				'max_len' => 20,
				'min_len' => 5
			),
			'home_phone' => array(
				'max_len' => 30,
			),
			'work_phone' => array(
				'max_len' => 30,
			),
			'pager' => array(
				'max_len' => 30,
			),
			'gender' => array(
				'is_valid_fisdap_gender' => true
			),
			'birth_date' => array(
				'max_len' => 20,
			),
			'contact_phone' => array(
				'max_len' => 30,
			),
			'contact_name' => array(
				'max_len' => 50,
			),
			'contact_relation' => array(
				'max_len' => 50,
			)
		);
	}

	// default values for new rows go here
	public function set_all_to_defaults() {

	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "StudentDataModel", self::ID_FIELD);
	}

	public function get_summary() {
		return "Student Data Model";
	}

	public static function get_by_username($username) {
		$connection = FISDAPDatabaseConnection::get_instance();

		$query = "SELECT * FROM ".self::DB_TABLE." where `UserName`='$username'";
		$res = $connection->query($query);

		if (count($res)>0) {
			return new self($res[0]);
		} else {
			return null;
		}
	}





	// no setters for: good_data_flag, class_assigned, default_goal_set_it
	public function set_first_name($val) {
		return $this->set_helper("string", $val, "first_name");
	}

	public function set_last_name($val) {
		return $this->set_helper("string", $val, "last_name");
	}

	public function set_program($val) {
		return $this->set_helper("string", $val, "program");
	}

	public function set_program_id($val) {
		return $this->set_helper("int", $val, "program_id");
	}

	public function set_mentor_id($val) {
		return $this->set_helper("int", $val, "mentor_id");
	}

	public function set_username($val) {
		return $this->set_helper("string", $val, "username");
	}

	public function set_box_number($val) {
		return $this->set_helper("int", $val, "box_number");
	}

	public function set_address($val) {
		return $this->set_helper("string", $val, "address");
	}

	public function set_city($val) {
		return $this->set_helper("string", $val, "city");
	}

	public function set_state($val) {
		return $this->set_helper("string", $val, "state");
	}

	public function set_zip_code($val) {
		return $this->set_helper("string", $val, "zip_code");
	}

	public function set_home_phone($val) {
		return $this->set_helper("string", $val, "home_phone");
	}

	public function set_work_phone($val) {
		return $this->set_helper("string", $val, "work_phone");
	}

	public function set_email($val) {
		return $this->set_helper("string", $val, "email");
	}

	public function set_pager($val) {
		return $this->set_helper("string", $val, "pager");
	}

	public function set_birth_date($val) {
		return $this->set_helper("string", $val, "birth_date");
	}

	// accepts any capitalization of M/F/Male/Female and saves as capital M or F
	public function set_gender($val) {
		// db: M F U X null ''
		Assert::is_string($val);

		$val = strtoupper($val);

		if ($val=='MALE') {
			$val = 'M';
		} else if ($val=='FEMALE') {
			$val = 'F';
		}

		return $this->set_helper("string", $val, "gender");
	}

	public function set_ethnicity($val) {
		// db: 1..7 or '' | null (varchar field)
		return $this->set_helper("string", $val, "ethnicity");
	}

	public function set_class_year($val) {
		return $this->set_helper("int", $val, "class_year");
	}

	public function set_emt_year($val) {
		return $this->set_helper("int", $val, "emt_year");
	}

	public function set_class_month($val) {
		return $this->set_helper("string", $val, "class_month");
	}

	public function set_research_consent($val) {
		// db: varchar: yes/no
		return $this->set_helper("string", $val, "research_consent");
	}

	public function set_max_field_shifts($val) {
		return $this->set_helper("int", $val, "max_field_shifts");
	}

	public function set_max_clinic_shifts($val) {
		return $this->set_helper("int", $val, "max_clinic_shifts");
	}

	public function set_max_lab_shifts($val) {
		return $this->set_helper("int", $val, "max_lab_shifts");
	}

	public function set_cell_phone($val) {
		return $this->set_helper("string", $val, "cell_phone");
	}

	public function set_contact_phone($val) {
		return $this->set_helper("string", $val, "contact_phone");
	}

	public function set_contact_name($val) {
		return $this->set_helper("string", $val, "contact_name");
	}

	public function set_contact_relation($val) {
		return $this->set_helper("string", $val, "contact_relation");
	}

	public function set_testing_expiration_date($val) {
		return $this->set_helper("date", $val, "testing_expiration_date");
	}
}
?>
