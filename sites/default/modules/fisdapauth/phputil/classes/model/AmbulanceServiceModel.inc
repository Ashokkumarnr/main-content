<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/

require_once('phputil/classes/model/AbstractModel.inc');

/**
 * @todo Someone who knows things should look through the list of fields and
 *  a) tell me what all those phone numbers are
 *  b) tell me if all of these fields should be settable
 */
class AmbulanceService extends AbstractModel {
	protected $name;
	protected $id;
	protected $contact_name;
	protected $contact_title;
	protected $address;
	protected $city;
	protected $postal_code;
	protected $region;
	protected $country;
	protected $b_phone;
	protected $dir_phone;
	protected $disp_phone;
	protected $fax_number;
	protected $medical_director;
	protected $abbreviation;
    protected $owner_program_id;
	protected $type;

	const dbtable = "AmbulanceServices";
	const idfield = "AmbServ_id";

	public function __construct($id=null) {
		$this->construct_helper($id, self::dbtable, self::idfield);
	}

    /**
     * Retrieve the ID of the program owning the service.
     * @return int | null The owning program.
     */
    public function get_owner_program_id() {
        return $this->owner_program_id;
    }

    /**
     * Set the ID of the program owning the service.
     * @return int The owning program or -1 for none.
     */
    public function set_owner_program_id($id) {
        Assert::is_int($id);

        $this->set_helper('int', $id, 'owner_program_id');
    }

	/**
	 * @todo Some of the names are prefixed by spaces in the db
	 * ({@link http://trac.fisdapoffice.int/trac/fisdap/ticket/1003 ticket 
	 * #1003}). So we fix that here.  If that problem gets resolved, we can
	 * get rid of this extra stuff.
	 */
	public function get_name() {
		return trim($this->name);
	}

	public function get_summary() {
        return 'AmbulanceService[' . $this->get_id() . ']';
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "ambulance_service", self::idfield);
	}

	public function get_settable_fields() {
		return array('name' => 'string',
			'contact_name' => 'string',
			'contact_title' => 'string',
			'address' => 'string',
			'city' => 'string',
			'postal_code' => 'string',
			'region' => 'string',
			'country' => 'string',
			'b_phone' => 'string',
			'dir_phone' => 'string',
			'disp_phone' => 'string',
			'fax_number' => 'string',
			'medical_director' => 'string',
			'abbreviation' => 'string',
			'type' => 'string');
	}

	public function get_fieldmap() {
		return array(
			'AmbServName' => 'name',
			'ContactName' => 'contact_name',
			'ContactTitle' => 'contact_title',
			'Address' => 'address',
			'City' => 'city',
			'PostalCode' => 'postal_code',
			'Region' => 'region',
			'Country' => 'country',
			'BPhone' => 'b_phone',
			'DirPhone' => 'dir_phone',
			'DispPhone' => 'disp_phone',
			'FaxNumber' => 'fax_number',
			'Medical_Director' => 'medical_director',
			'AmbServAbrev' => 'abbreviation',
            'OwnerProgram_id' => 'owner_program_id',
			'Type' => 'type');
	}
}
