<?php

require_once('TestCreationProject.php');

/**
 * This class models testing projects that only require one assignment
 * review for a validation designation.
 */
class SingletonTestCreationProject extends TestCreationProject {
    function SingletonTestCreationProject() {
        parent::TestCreationProject();
        $this->assignment_review_type = SINGLETON_ASSIGNMENT_REVIEW;
    }//SingletonTestCreationProject


    /**
     * @todo find a clean way to sort the reviews by date, so we can 
     *       avoid relying on the IDs being sequential
     */
    function update_item_review_status(&$item,$email_warnings=true) {
        // get all of the reviews 
        $reviews = $item->get_reviews();

        if ( $newest_singleton = $this->find_newest_singleton_review(
                 $reviews
             ) ) {
            // update the status from the latest singleton review
            $updated_status = ($newest_singleton->is_favorable()) ? 
                VALIDATED_ITEM_VALIDITY :
                NEEDS_REVISION_ITEM_VALIDITY;
            $item->set_validity($updated_status);
            $item->save();
            $this->logger->log(
                'TestItem#'.$item->get_id().': singleton review! set '.
                'status to '.$updated_status,
                $this->logger->DEBUG
            );
            return $updated_status;
        } else {
            // no singleton reviews... set the status to "undetermined"
            $item->set_validity(UNDETERMINED_ITEM_VALIDITY);
            $item->save();
            $this->logger->log(
                'TestItem#'.$item->get_id().': no singleton reviews... '
                . 'setting status to undetermined',
                $this->logger->DEBUG
            );
            return UNDETERMINED_ITEM_VALIDITY;
        }//else
    }//update_item_review_status


    /**
     * Returns the newest singleton review in the given list, if any.
     */
    function find_newest_singleton_review($reviews) {
        $newest_singleton = null;
        if ( $reviews ) {
            foreach( $reviews as $review ) {
                if ( $review->get_assignment_review_type() == 
                     SINGLETON_ASSIGNMENT_REVIEW ) {
                    if ( !$newest_singleton ) {
                        $newest_singleton = $review;
                    } else if ( $review->get_id() > 
                                $newest_singleton->get_id() ) {
                        $newest_singleton = $review;
                    }//if
                }//if
            }//foreach
        }//if

        return $newest_singleton;
    }//find_newest_singleton_review


    ///////////////////////////////////////////////////////////////////////////
    // Module: test_creation_project_db_layer.php
    //
    // This module provides the database layer functions for the test creation
    // project model classes.  These are static methods, and must know the 
    // name of the current class when they are invoked.  Without this module,
    // the TestCreationProject subclasses would all generate 
    // TestCreationProject instances instead of instances of the appropriate 
    // subclasses.
    ///////////////////////////////////////////////////////////////////////////
    
    
    /**
     * Fetch the project with the given ID
     */
    function &find_by_id($id) {
        if ( !is_numeric($id) ) {
            return false;
        }//if
    
        $connection_obj =& FISDAPDatabaseConnection::get_instance();
    
        $query = 'SELECT * '.
                 'FROM ProjectTable '.
                 'WHERE Project_id="'.$id.'"';
        $result_set = $connection_obj->query($query);
        if ( !is_array($result_set) || count($result_set) != 1 ) {
            return false;
        }//if
    
        return SingletonTestCreationProject::from_assoc_array($result_set[0]);
    }//find_by_id
    
    
    function &from_assoc_array($db_row) {
        $project =& new SingletonTestCreationProject();
        foreach( $db_row as $field=>$value ) {
            $obj_field = $project->field_map['db'][$field];
            $project->$obj_field = $value;
        }//foreach
        return $project;
    }//from_assoc_array
    
    ///////////////////////////////////////////////////////////////////////////
    // END MODULE
    ///////////////////////////////////////////////////////////////////////////

}//class SingletonTestCreationProject

?>
