<?php

require_once('AbstractModel.inc');

/** 
 * Represents a program in the DB.
 * #######################################################################
 * This class only handles get functions until all fields are implemented.
 * #######################################################################
 */
class ProgramData extends AbstractModel {
    protected $abbreviation;
    protected $active;
    protected $billing_contact;
    protected $billing_address;
    protected $billing_address2;
    protected $billing_address3;
    protected $billing_city;
    protected $billing_fax;
    protected $billing_phone;
    protected $billing_country;
    protected $billing_state;
    protected $billing_zip_code;
    protected $can_buy_accounts;
    protected $can_create_clinical_shifts;
    protected $can_create_field_shifts;
    protected $can_create_lab_shifts;
    protected $can_pick_clinical_shifts;
    protected $can_pick_field_shifts;
    protected $can_pick_lab_shifts;
    protected $can_view_full_calendar;
    protected $class_size;
    protected $class_start_dates;
    protected $clinical_drops_code;
    protected $clinical_trades_code;
    protected $customer_id;
    protected $email;
    protected $field_drops_code;
    protected $field_trades_code;
    protected $id;
    protected $late_clinical_hours;
    protected $late_data_entry_reminders;
    protected $late_field_hours;
    protected $late_lab_hours;
    protected $lab_drops_code;
    protected $lab_trades_code;
	protected $name;
    protected $order_id;
    protected $program_address;
    protected $program_address2;
    protected $program_address3;
    protected $program_city;
    protected $program_contact;
    protected $program_country;
    protected $program_fax;
    protected $program_phone;
    protected $program_state;
    protected $program_zip_code;
    protected $requires_purchase_order;
    protected $send_critical_thinking_questions;
    protected $students_set_absent_with_permission;
    protected $type;
    protected $url;
    protected $use_blank_narrative;
    protected $use_narrative;
    protected $use_scheduler;

	const DB_TABLE_NAME = 'ProgramData';
	const ID_FIELD = 'Program_id';

    private static $months = array(
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    );

	public function __construct($data) {
		$this->construct_helper($data, self::DB_TABLE_NAME, self::ID_FIELD);
	}

	public function get_fieldmap() {
		$map = array(
            'Active' => 'active',
            'BigBroInc' => 'late_field_hours',
            'BigBroStuReminders' => 'late_data_entry_reminders',
            'BillingContact' => 'billing_contact',
            'BillingAddress' => 'billing_address',
            'BillingAddress2' => 'billing_address2',
            'BillingAddress3' => 'billing_address3',
            'BillingCity' => 'billing_city',
            'BillingFax' => 'billing_fax',
            'BillingPhone' => 'billing_phone',
            'BillProgramCountry' => 'billing_country',
            'BillingState' => 'billing_state',
            'BillingZip' => 'billing_zip_code',
            'BlankNarrative' => 'use_blank_narrative',
            'CanBuyAccounts' => 'can_buy_accounts',
            'ClassSize' => 'class_size',
            'ClassStartDates' => 'class_start_dates',
            'ClinicalBigBroInc' => 'late_clinical_hours',
            'ClinicalDropsCode' => 'clinical_drops_code',
            'ClinicalTradesCode' => 'clinical_trades_code',
            'ContactEmail' => 'email',
            'CustomerId' => 'customer_id',
            'DropsCode' => 'field_drops_code',
            'LabBigBroInc' => 'late_lab_hours',
            'LabDropsCode' => 'lab_drops_code',
            'LabTradesCode' => 'lab_trades_code',
            'OrderId' => 'order_id',
            'ProgramAbrv' => 'abbreviation',
            'ProgramAddress' => 'program_address',
            'ProgramAddress2' => 'program_address2',
            'ProgramAddress3' => 'program_address3',
            'ProgramCity' => 'program_city',
            'ProgramContact' => 'program_contact',
            'ProgramCountry' => 'program_country',
            'ProgramFax' => 'program_fax',
		    'ProgramName' => 'name',
            'ProgramPhone' => 'program_phone',
            'ProgramType' => 'type',
            'ProgramState' => 'program_state',
            'ProgramWebsite' => 'url',
            'ProgramZip' => 'program_zip_code',
            'RequiresPO' => 'requires_purchase_order',
            'Scheduler' => 'use_scheduler',
            'SendStuEvents' => 'send_critical_thinking_questions',
            'StudentEnterShift' => 'can_create_field_shifts',
            'StudentsSetAbsentWithPermission' => 'students_set_absent_with_permission',
            'StuEnterClinicShifts' => 'can_create_clinical_shifts',
            'StuEnterLabShifts' => 'can_create_lab_shifts',
            'StuPickClinic' => 'can_pick_clinical_shifts',
            'StuPickField' => 'can_pick_field_shifts',
            'StuPickLab' => 'can_pick_lab_shifts',
            'StuViewFullCal' => 'can_view_full_calendar',
            'TradesCode' => 'field_trades_code',
            'UseNarrative' => 'use_narrative'
        );

		return $map;
	}

    /**
     * Retrieve the starting months.
     * @return array The month names.
     */
    public function get_class_start_dates() {
        if (is_null($this->class_start_dates)) return array();

        // In the database, I have seen:
        // - duplicate months
        // - months with no delimeter
        // - months delimted with a comma
        // - unknown words
        // - numbers
        // The easiest way to catch them all is to find if the 
        // month name is contained in the DB field.
        $value = strtolower(trim($this->class_start_dates));
        $month_names = array();
        foreach (self::$months as $month) {
            if (strpos($value, strtolower($month)) !== false) {
                $month_names[] = $month;
            }
        }

        return $month_names;
    }

	/**
	 * Creates objects based on a MySQL result set so the database doesn't need to be hit more than once.
	 *
	 * @param result A result set containing on or more complete rows. The rows should be complete records, or we have to hit the DB anyway, which completely and totally defeats the purpose. 
	 * @pre The result set CANNOT use alias names.
	 * @return array An array of objects.
	 * @todo can we move this to the parent class and use constants for the class-specific info (ID field name)?
	 */
	public static function getBySQL($result) {
		return parent::sql_helper($result, 'ProgramData', self::ID_FIELD);
	}

	public function get_summary() {
		$s = 'Program(' . $this->get_id() . '=' . $this->get_name() . ') ';
		return $s;
	}

    public function get_can_create_clinical_shifts() {
        // This field is 0=true in the DB.
        if ($this->can_create_clinical_shifts) return false;
        return true;
    }

    public function get_can_create_field_shifts() {
        // This field is 0=true in the DB.
        if ($this->can_create_field_shifts) return false;
        return true;
    }

    public function get_can_create_lab_shifts() {
        // This field is 0=true in the DB.
        if ($this->can_create_lab_shifts) return false;
        return true;
    }

    public function set_abbreviation($abbreviation) {
        $this->set_helper("string", $abbreviation, "abbreviation");
    }

    public function set_active($active) {
        $this->set_helper("bool", $active, "active");
    }

    public function set_billing_contact($billing_contact) {
        $this->set_helper("string", $billing_contact, "billing_contact");
    }

    public function set_billing_address($billing_address) {
        $this->set_helper("string", $billing_address, "billing_address");
    }

    public function set_billing_address2($billing_address2) {
        $this->set_helper("string", $billing_address2, "billing_address2");
    }

    public function set_billing_address3($billing_address3) {
        $this->set_helper("string", $billing_address3, "billing_address3");
    }

    public function set_billing_city($billing_city) {
        $this->set_helper("string", $billing_city, "billing_city");
    }

    public function set_billing_fax($billing_fax) {
        $this->set_helper("string", $billing_fax, "billing_fax");
    }

    public function set_biling_phone($biling_phone) {
        $this->set_helper("string", $biling_phone, "biling_phone");
    }

    public function set_billing_country($billing_country) {
        $this->set_helper("string", $billing_country, "billing_country");
    }

    public function set_billing_state($billing_state) {
        $this->set_helper("string", $billing_state, "billing_state");
    }

    public function set_billing_zip_code($billing_zip_code) {
        $this->set_helper("string", $billing_zip_code, "billing_zip_code");
    }

    public function set_can_buy_accounts($can_buy_accounts) {
        $this->set_helper("bool", $can_buy_accounts, "can_buy_accounts");
    }

    public function set_can_create_clinical_shifts($can_create_clinical_shifts) {
        // This field is 0=true in the DB.
        $can_create_clinical_shifts = !$can_create_clinical_shifts; 
        $this->set_helper("bool", $can_create_clinical_shifts, "can_create_clinical_shifts");
    }

    public function set_can_create_field_shifts($can_create_field_shifts) {
        // This field is 0=true in the DB.
        $can_create_field_shifts = !$can_create_field_shifts; 
        $this->set_helper("bool", $can_create_field_shifts, "can_create_field_shifts");
    }

    public function set_can_create_lab_shifts($can_create_lab_shifts) {
        // This field is 0=true in the DB.
        $can_create_lab_shifts = !$can_create_lab_shifts; 
        $this->set_helper("bool", $can_create_lab_shifts, "can_create_lab_shifts");
    }

    public function set_can_pick_clinical_shifts($can_pick_clinical_shifts) {
        $this->set_helper("bool", $can_pick_clinical_shifts, "can_pick_clinical_shifts");
    }

    public function set_can_pick_field_shifts($can_pick_field_shifts) {
        $this->set_helper("bool", $can_pick_field_shifts, "can_pick_field_shifts");
    }

    public function set_can_pick_lab_shifts($can_pick_lab_shifts) {
        $this->set_helper("bool", $can_pick_lab_shifts, "can_pick_lab_shifts");
    }

    public function set_can_view_full_calendar($can_view_full_calendar) {
        $this->set_helper("bool", $can_view_full_calendar, "can_view_full_calendar");
    }

    public function set_class_size($class_size) {
        $this->set_helper("int", $class_size, "class_size");
    }

    /**
     * Set the class starting months.
     * @param array $months The month names.
     */
    public function set_class_start_dates($months) {
        $month_names = array();
        foreach (self::$months as $month) {
            foreach ($months as $m) {
                if (is_null($m)) continue;

                $m = trim($m);
                if (!strcasecmp($m, $month)) {
                    $month_names[] = $month;
                    break;
                }
            }
        }

        $s = join(', ', $month_names);
        $this->set_helper("string", $s, "class_start_dates");
    }

    public function set_clinical_drops_code($clinical_drops_code) {
        $this->set_helper("int", $clinical_drops_code, "clinical_drops_code");
    }

    public function set_clinical_trades_code($clinical_trades_code) {
        $this->set_helper("int", $clinical_trades_code, "clinical_trades_code");
    }

    public function set_customer_id($customer_id) {
        $this->set_helper("string", $customer_id, "customer_id");
    }

    public function set_email($email) {
        $this->set_helper("string", $email, "email");
    }

    public function set_field_drops_code($field_drops_code) {
        $this->set_helper("int", $field_drops_code, "field_drops_code");
    }

    public function set_field_trades_code($field_trades_code) {
        $this->set_helper("int", $field_trades_code, "field_trades_code");
    }

    public function set_late_clinical_hours($late_clinical_hours) {
        $this->set_helper("int", $late_clinical_hours, "late_clinical_hours");
    }

    public function set_late_data_entry_reminders($late_data_entry_reminders) {
        $this->set_helper("bool", $late_data_entry_reminders, "late_data_entry_reminders");
    }

    public function set_late_field_hours($late_field_hours) {
        $this->set_helper("int", $late_field_hours, "late_field_hours");
    }

    public function set_late_lab_hours($late_lab_hours) {
        $this->set_helper("int", $late_lab_hours, "late_lab_hours");
    }

    public function set_lab_drops_code($lab_drops_code) {
        $this->set_helper("int", $lab_drops_code, "lab_drops_code");
    }

    public function set_lab_trades_code($lab_trades_code) {
        $this->set_helper("int", $lab_trades_code, "lab_trades_code");
    }

    public function set_name($name) {
        $this->set_helper("string", $name, "name");
    }

    public function set_order_id($order_id) {
        $this->set_helper("string", $order_id, "order_id");
    }

    public function set_program_address($program_address) {
        $this->set_helper("string", $program_address, "program_address");
    }

    public function set_program_address2($program_address2) {
        $this->set_helper("string", $program_address2, "program_address2");
    }

    public function set_program_address3($program_address3) {
        $this->set_helper("string", $program_address3, "program_address3");
    }

    public function set_program_city($program_city) {
        $this->set_helper("string", $program_city, "program_city");
    }

    public function set_program_contact($program_contact) {
        $this->set_helper("int", $program_contact, "program_contact");
    }

    public function set_program_country($program_country) {
        $this->set_helper("string", $program_country, "program_country");
    }

    public function set_program_fax($program_fax) {
        $this->set_helper("string", $program_fax, "program_fax");
    }

    public function set_program_phone($program_phone) {
        $this->set_helper("string", $program_phone, "program_phone");
    }

    public function set_program_state($program_state) {
        $this->set_helper("string", $program_state, "program_state");
    }

    public function set_program_zip_code($program_zip_code) {
        $this->set_helper("string", $program_zip_code, "program_zip_code");
    }

    public function set_requires_purchase_order($requires_purchase_order) {
        $this->set_helper("bool", $requires_purchase_order, "requires_purchase_order");
    }

    public function set_send_critical_thinking_questions($send_critical_thinking_questions) {
        $this->set_helper("bool", $send_critical_thinking_questions, "send_critical_thinking_questions");
    }

    public function set_students_set_absent_with_permission($students_set_absent_with_permission) {
        $this->set_helper("bool", $students_set_absent_with_permission, "students_set_absent_with_permission");
    }

    public function set_type($type) {
        $this->set_helper("int", $type, "type");
    }

    public function set_url($url) {
        $this->set_helper("string", $url, "url");
    }

    public function set_use_blank_narrative($use_blank_narrative) {
        // This field is bass ackwards.
        $use_blank_narrative = !$use_blank_narrative; 
        $this->set_helper("bool", $use_blank_narrative, "use_blank_narrative");
    }

    public function set_use_narrative($use_narrative) {
        $this->set_helper("bool", $use_narrative, "use_narrative");
    }

    public function set_use_scheduler($use_scheduler) {
        $this->set_helper("bool", $use_scheduler, "use_scheduler");
    }
}
?>
