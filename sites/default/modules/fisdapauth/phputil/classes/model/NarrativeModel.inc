<?php

/* This is a copy of OtherALS
 * It might work.
 */

require_once("DataModel.inc");

class Narrative extends DataModel {

	/*********
	 * FIELDS
	 *********/
	protected $narrative_text;
	protected $assessment_id;
	const dbtable = "NarrativeData";
	const id_field = "Narrative_id";

	/**************
	 * CONSTRUCTOR
	 **************/

	public function __construct($id=null) {
		parent::__construct();
		$this->set_helper("int", -1, "asses_id");
		if (!$this->construct_helper($id, self::dbtable, self::id_field)) {
			return false;
		}
	}

	/************
	 * FUNCTIONS
	 ************/

	public function set_narrative($nar) {
		return $this->set_helper("string", $nar, "narrative_text");
	}

	public function set_asses_id($id) {
		if (is_numeric($id)) {
			return $this->set_helper("int", $id, "assessment_id");
		} else {
			return false;
		}
	}

	public function get_narrative() {
		return stripslashes($this->narrative_text);
	}


	public function get_summary() {
		return stripslashes(substr($this->narrative_text,0,30))."...";
	}

	public function get_mobile_summary() {
		//STUB
	}

	public function get_hook_ids(){
		return array();
	}

	/**
	 * renamed the asses_id field to assessment_id
	 * this function is for legacy code
	 */
	public function get_asses_id() {
		return $this->get_assessment_id();
	}	

	/*
	 * FUNCTION getBySQL
	 *
	 * Creates Med objects based on a MySQL result set so the database doesn't need to be hit more than once.
	 *
	 * @param result A result set containing on or more complete MedData rows. The rows should be complete MedData records, or we have to hit the DB anyway, which completely and totally defeats the purpose. 
	 * @pre The result set CANNOT use alias names.
	 * @return array An array of Meds.
	 * @TODO untested
	 * @TODO can we move this to the parent class and use constants for the class-specific info (ID field name)?
	 */
	public static function getBySQL($result) {
		return parent::sql_helper($result, "narrative", self::id_field);
	}

	public function get_fieldmap() {
		return array("Run_id" => "run_id",
				"Asses_id" => "assessment_id",
				"Student_id" => "student_id",
				"Text" => "narrative_text",
				"Shift_id" => "shift_id");
	}

}

?>
