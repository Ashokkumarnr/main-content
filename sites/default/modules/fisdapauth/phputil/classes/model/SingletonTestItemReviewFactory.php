<?php

require_once('phputil/classes/html_builder/form_builder/SingletonAssignmentTestItemReviewForm.php');
require_once('phputil/classes/model/SingletonAssignmentTestItemReview.php');

class SingletonTestItemReviewFactory {
    function &create_review($assoc_array) {
        $mode = $assoc_array['mode'];
        $type = $assoc_array['AssignmentReviewType'];
        if ( $mode == 'assignment' && 
             $type == SINGLETON_ASSIGNMENT_REVIEW ) {
            return SingletonAssignmentTestItemReview::from_assoc_array(
                $assoc_array
            );
        }//if

        return false;
    }//create_review

    function &create_form($item,$mode) {
        $project =& $item->get_project();

        if ( !$project || 
             ( $mode == 'assignment' && 
               $project->get_assignment_review_type() == SINGLETON_ASSIGNMENT_REVIEW ) ) {
            return new SingletonAssignmentTestItemReviewForm(
                $item->get_id()
            );
        }//if

        return false;
    }//create_form
}//class SingletonTestItemReviewFactory

?>
