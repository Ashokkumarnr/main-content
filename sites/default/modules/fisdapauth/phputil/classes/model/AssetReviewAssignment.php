<?php

/**
 *
 */
class AssetReviewAssignment {

    ////////////////////////////////////////////////////////////
    // Private instance variables
    ////////////////////////////////////////////////////////////
    
    var $id;
    var $asset_id
    var $due_on;
    var $assigned_on;
    var $completed_on;
    var $completed_form_id;
    var $user_id;


    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////
    
    /**
     *
     */
    function AssetReviewAssignment($id) {
        $this->id = $id;
        if ( $id > 0 ) {
            $this->from_mysql_db();
        }//if
    }//AssetReviewAssignment


    ////////////////////////////////////////////////////////////
    // Public instance methods
    ////////////////////////////////////////////////////////////
    
    /**
     *
     */
    function from_mysql_db() {
    }//from_mysql_db
    
    
    /**
     *
     */
    function set_asset_id($asset_id) {
        $this->asset_id = $asset_id;
    }//set_asset_id

    
    /**
     *
     */
    function set_due_on($due_on) {
        $this->due_on = $due_on;
    }//set_due_on

    
    /**
     *
     */
    function set_assigned_on($assigned_on) {
        $this->assigned_on = $assigned_on;
    }//set_assigned_on

    
    /**
     *
     */
    function set_user_id($user_id) {
        $this->user_id = $user_id;
    }//set_user_id

    
    /**
     *
     */
    function set_completed_on($completed_on) {
        $this->completed_on = $completed_on;
    }//set_completed_on

    
    /**
     *
     */
    function set_completed_form_id($completed_form_id) {
        $this->completed_form_id = $completed_form_id;
    }//set_completed_form_id
}//class AssetReviewAssignment

?>
