<?php

/*----------------------------------------------------------------------*
  |                                                                      |
  |     Copyright (C) 1996-2010.  This is an unpublished work of         |
  |                      Headwaters Software, Inc.                       |
  |                         ALL RIGHTS RESERVED                          |
  |     This program is a trade secret of Headwaters Software, Inc.      |
  |     and it is not to be copied, distributed, reproduced, published,  |
  |     or adapted without prior authorization                           |
  |     of Headwaters Software, Inc.                                     |
  |                                                                      |
*----------------------------------------------------------------------*/

require_once('phputil/classes/Assert.inc');
require_once("Model.inc");

/**
 * @author Kate Hanson
 * 
 */
class MoodleTestData extends AbstractModel {

	/**
	 * The id of the blueprint for the exam, as defined in the TestBluePrints table
	 */
	protected $blueprint_id;
	/**
	 * The quiz id for this test, as dictated by Moodle. 
	 *
	 * NOTE: this is the id we should most often use in the custom constructor because we use this
	 * number to identify tests in our system. It must therefore be unique and is
	 * therefore sometimes modified by the MoodleIDMod.
	 */
	protected $moodle_quiz_id;
	/**
	 * The name of the test
	 */
	protected $test_name;
	/**
	 * The active status of the test (eg; active, retired, piloting, etc.)
	 */
	protected $active;
	/**
	 * The id of the course that this test has been placed in in Moodle
	 */
	protected $moodle_course_id;
	/**
	 * Flag which denotes whether or not detailed blueprint is available for learning prescriptions and the like.
	 */
	protected $show_details;
	/**
	 * The name of the moodle database which hosts this test 
	 */
	protected $moodle_database;
	/**
	 * The prefix used for all the tables in the moodle database that hosts this test
	 */
	protected $moodle_prefix;
	/**
	 * Flag which denotes whether students can automatically see their scores for this test
	 */
	protected $publish_scores;
	/**
	 * A modifier which is added to the moodle quiz id.
	 *
	 * The modfier which is added to the moodle quiz id so that all tests have a unique moodle quiz id, even if they come
	 * from different moodles. This modifier is vital because we identify test by thier moodle quiz id in our system.
	 */	
	protected $moodle_id_mod;
	/**
	 * The certification level that this test is intended for
	 */
	protected $cert_level;
	/**
	 * Class section year selected in the student selector used to pick which students will take the exam. -1 if 'All' is selected.
	 */	

	const DB_TABLE_NAME = "MoodleTestData";
	const ID_FIELD = "TestData_id";

	/**
	 * Constructs a new MoodleTestData object
	 *
	 * @param int|string $test_data_id The unique TestData_id of the test you want to get data for.
	 * @return MoodleTestData
	 */
	public function __construct($test_data_id) {
		$this->construct_helper($test_data_id, self::DB_TABLE_NAME, self::ID_FIELD);
	}

	/**
	 * Constructs a new MoodleTestData object, using the moodle quiz id
	 *
	 * This is the constructor we will probably use the most often.
	 *
	 * @param int|string $moodle_quiz_id The unique MoodleQuiz_id of the test you want to get data for.
	 * @return MoodleTestData
	 */
	public static function construct_by_moodle_quiz_id($moodle_quiz_id) {
		Assert::is_int($moodle_quiz_id);

		$connection = FISDAPDatabaseConnection::get_instance();
		$query = "SELECT TestData_id FROM MoodleTestData where MoodleQuiz_id=$moodle_quiz_id";
		$result = $connection->query($query);

		if (count($result) != 1) {
			throw new FisdapInvalidArgumentException("ID {MoodleQuiz_id}[$moodle_quiz_id] does not exist");
		}

		$test_data_id = $result[0]['TestData_id'];

		return new MoodleTestData($test_data_id);
	}

	/**
	 * STUB If we need an easy summary of some aspect of this model, we can write it here
	 *
	 * @return string "MoodleTestData for [testame]"
	 */
	public function get_summary() {
		return 'MoodleTestData for '.$this->test_name;
	}

	/**
	 * Maps the fields in the MoodleTestData table to the corresponding properties of this object
	 *
	 * The fieldmap is used in AbstractModel to create the helper functions, save/clone an object, etc...
	 */
	public function get_fieldmap() {
		$map =  array(
			'Blueprint_id' => 'blueprint_id',
			'MoodleQuiz_id' => 'moodle_quiz_id',
			'TestName' => 'test_name',
			'Active' => 'active',
			'MoodleCourse_id' => 'moodle_course_id',
			'ShowDetails' => 'show_details',
			'MoodleDatabase' => 'moodle_database',
			'MoodlePrefix' => 'moodle_prefix',
			'PublishScores' => 'publish_scores',
			'MoodleIDMod' => 'moodle_id_mod',
			'CertLevel' => 'cert_level'
		);
		return $map;
	}

	/**
	 * Creates an array of MoodleTestData objects based on the results of a MySQL query
	 *
	 * This purpose of this function is to create multiple MoodleTestData objects based on a single
	 * database hit. Since you need info on all fields in order to create an object, only complete
	 * MoodleTestDatas rows will work.
	 *
	 * @param result A result set containing one or more complete MoodleTestDatas rows. Both mysql 
	 * resource and array formats are accepted.
	 * @return array An array of MoodleTestData objects
	 */
	public static function getBySQL($result) {
		return parent::sql_helper($result, 'MoodleTestData', self::ID_FIELD);
	}

	/**
	 * Lists which fields can be set with magical setters
	 *
	 * We may need to go back at some point and write explicit setters for some of these fields, if there's logic involved
	 */
	public function get_settable_fields() {
		return array('blueprint_id' => 'int',
			'moodle_quiz_id' => 'int',
			'test_name' => 'string',
			'active' => 'int',
			'moodle_course_id' => 'int',
			'show_details' => 'int',
			'moodle_database' => 'string',
			'moodle_prefix' => 'string',
			'publish_scores' => 'int',
			'moodle_id_mod' => 'int');
	}

	/**
	 * This custom setter accepts the string ('Active', 'Retired', etc.) or the numeric code
	 */
	public function set_active($status) {
		$id = $this->id_or_string_helper($status, "get_status_types");
		if ($id) {
			$this->set_helper("int", $id, "active");
			return true;
		} else {
			throw new FisdapInvalidArgumentException('Invalid status type: '.$status);
		}
	}

	/**
	 * Returns a string description of the status ('Active', 'Retired', etc.) instead of the numeric code 
	 */
	public function get_active_description() {
		$status_types = self::get_status_types();
		return $status_types[$this->active];
	}

	public static function get_status_types() {
		return self::data_table_helper("TestStatusTable", "StatusType_id", "StatusName");
	}

	/**
	 * This custom setter only accepts the appropriate strings ('paramedic', 'emt-b', etc.)
	 */
	public function set_cert_level($cert_level) {
		Assert::is_string($cert_level);
		$cert_level_options = self::data_table_helper("CertLevelOptions", "CertLevel_id", "CertLevelVal");
		if (in_array($cert_level, $cert_level_options)) {
			$this->set_helper("string", $cert_level, "cert_level");
			return true;
		} else {
			throw new FisdapInvalidArgumentException('Invalid status type: '.$status);
		}
	}


}

?>
