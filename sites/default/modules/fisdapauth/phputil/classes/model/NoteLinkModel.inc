<?php

require_once('AbstractModel.inc');

class NoteLink extends AbstractModel {
    /*********
     * FIELDS
     *********/

    protected $note_id;
    protected $table_name;
    protected $table_index;
    protected $table_id;
    const dbtable = "note_link";
    const id_field = "note_link_id";

    /**************
     * CONSTRUCTOR
     **************/

    public function __construct($id=null) {
        if (!$this->construct_helper($id, self::dbtable, self::id_field)) {
            return false;
        }
    }

    /**********
     * METHODS
     **********/

    public function set_note_id($id) {
        return $this->set_helper("int", $id, "note_id");
    }

    public function set_table_name($name) {
        return $this->set_helper('string', $name, 'table_name');
    }

    public function set_table_index($index) {
        return $this->set_helper('string', $index, 'table_index');
    }

    public function set_table_id($id) {
        return $this->set_helper('int', $id, 'table_id');
    }

    public function get_summary() {
        //STUB
    }

    public static function getBySQL($result) {
        return self::sql_helper($result, "notelink", self::id_field);
    }

    public function get_fieldmap() {
        return array(
            'note_link_note_id' => 'note_id',
            'note_link_table_name' => 'table_name',
            'note_link_table_index' => 'table_index',
            'note_link_table_id' => 'table_id',
        );
    }
}

?>
