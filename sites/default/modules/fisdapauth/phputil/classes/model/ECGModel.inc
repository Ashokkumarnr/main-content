<?php

require_once("DataModel.inc");

/* 
 * CLASS ecg
 * 
 * Public Methods:
 *   get/set: rhythm, pacing, defib, sync
 *   save
 *   remove
 *   get_summary
 *   static getBySQL
 *   static get_ecg_rhythms
 *   static get_ecg_interventions
 */

class ECG extends DataModel { 

	/*********
	 * FIELDS
	 *********/

	protected $rhythm;
	protected $defib;
	protected $pacing;
	protected $sync;
	protected $lead;
	const dbtable = "EKGData";
	const id_field = "EKG_id";

	/**************
	 * CONSTRUCTOR
	 **************/

	public function __construct($id=null) {
		parent::__construct();
		if (!$this->construct_helper($id, self::dbtable, self::id_field)) {
			return false;
		}				
	}

	/**********
	 * METHODS
	 **********/

	public function set_rhythm($r) {
		return $this->set_helper("int", $this->id_or_string_helper($r, "get_ecg_rhythms"), "rhythm");
	}

	public function set_defib() {
		$this->remove_interventions();
		return $this->set_helper('bool', true, "defib");
	}

	public function set_pacing() {
		$this->remove_interventions();
		return $this->set_helper('bool', true, "pacing");
	}

	public function set_sync() {
		$this->remove_interventions();
		return $this->set_helper('bool', true, "sync");
	}

	public function set_lead($lead) {
		return $this->set_helper('bool', $lead, 'lead');
	}

	public function remove_interventions() {
		$this->set_helper('bool',false,'defib');
		$this->set_helper('bool',false,'pacing');
		$this->set_helper('bool',false,'sync');

		return true;
	}

	public function get_summary() {
		//STUB
		$r = self::get_ecg_rhythms();
		$s = $r[$this->rhythm];
		return $s;
	}

	public function get_mobile_summary() {
		$r = self::get_ecg_rhythms();
		$rhythm = $r[$this->rhythm];
		$summary = "<div style='font-weight:bold;'>$rhythm ";
		if ($this->get_performed_by() == 0) {
			$summary .= "(Performed)";
		} else {
			$summary .= "(Observed)";
		}
		$summary .= "</div>";

		//Add intervention
		if ($this->defib) {
			$summary .= "<div>defibrillation</div>";
		} else if ($this->pacing) {
			$summary .= "<div>transcutaneous pacing</div>";
		} else if ($this->sync) {
			$summary .= "<div>Synchronized cardioversion</div>";
		}

		return $summary;
	}

	public static function get_ecg_rhythms() {
		return self::data_table_helper("EKGRhythmTable", "EKGRhythm_id", "EKGRhythmTitle");
	}

	public function get_ecg_interventions() {
		return array("Defib"=>$this->defib,
					"Pacing"=>$this->pacing,
					"Sync"=>$this->sync);
	}

	public function get_hook_ids() {
		Assert::is_true($this->shift_id > 0);

		$shift = new Shift($this->shift_id);
		$shift_type = $shift->get_type();

		if ($shift_type == 'field') {
			return array(38);
		} else if ($shift_type == 'clinical') {
			return array(46);
		} else if ($shift_type == 'lab') {
			return array(78);
		} else {
			return array();
		}	
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "ecg", self::id_field);
	}

	public function get_fieldmap() {
		return array("Run_id" => "run_id",
					"Student_id" => "student_id",
					"PerformedBy" => "performed_by",
					"Rhythm" => "rhythm",
					"Defibrillation" => "defib",
					"Pacing" => "pacing",
					"Sync" => "sync",
					"Shift_id" => "shift_id",
					"SubjectType_id" => "subject_type",
					"TwelveLead" => "lead",
					"Assessment_id"=>"assessment_id",
				);
		//removed: "EKG_id" => "id",
	}
}

?>
