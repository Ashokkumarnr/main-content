<?php

require_once('AbstractModel.inc');
require_once('NoteLinkModel.inc');
require_once('programData.inc');
require_once('Zend/Mail.php');

class ProgramNote extends AbstractModel {
	/*********
	 * FIELDS
	 *********/

	protected $note_content;
	protected $timestamp;
	protected $note_link;

	const dbtable = "note";
	const id_field = "note_id";

	/**************
	 * CONSTRUCTOR
	 **************/

	public function __construct($id=null) {
		$connection = FISDAPDatabaseConnection::get_instance();

		if (is_array($id)) {
			$id = $id[self::id_field];
		}

		if ($id == null) {
			$this->note_link = new NoteLink();
		} else {
			$query = "SELECT * FROM note_link WHERE note_link_note_id = $id";
			$result = $connection->query($query);

			if (count($result) == 1) {
				$array = NoteLink::getBySQL($result);
				$this->note_link = $array[0];
			}
		}

		if (!$this->construct_helper($id, self::dbtable, self::id_field)) {
			return false;
		}
	}

	/**********
	 * METHODS
	 **********/

	public function set_note_content($content) {
		return $this->set_helper("string", $content, "note_content");
	}

	public function get_note_content() {
		if ($this->note_content) {
			return stripslashes($this->note_content);
		}
	}

	public function set_program_id($id) {
		$this->note_link->set_table_id($id);
	}

	public function get_program_id() {
		return $this->note_link->get_table_id();
	}

	public function get_summary() {
		//STUB
	}

	public static function getBySQL($result) {
		return self::sql_helper($result, "notelink", self::id_field);
	}

	public function get_fieldmap() {
		return array(
			'note_created_on' => 'timestamp',
			'note_content' => 'note_content',
		);
	}

	protected function send_email() {
		$program = new ProgramData($this->get_program_id());
		$program_name = $program->get_name();

		if ($this->get_id() == null) {
			$subj = 'FISDAP Notes Added for '.$program_name;
			$body = "A new note has been added in FISDAP<br><br>";
		} else {
			$subj = 'FISDAP Notes Update for '.$program_name;
			$body = "A FISDAP note has been edited<br><br>";
		}

		$body .= $this->get_note_content();

		$mail = new Zend_Mail();
		$mail->setFrom('support@fisdap.net');
		$mail->addTo('support@fisdap.net');
		$mail->setSubject($subj);
		$mail->setBodyHtml($body);
		$mail->send();
	}

	public function save($send_email = true) {
		Assert::is_not_null($this->note_link->get_table_id());

		//Send email before saving so we can determine if this is new or an update
		if ($send_email) {
			$this->send_email();
		}

		//save parent and get id
		parent::save();
		$note_id = $this->get_id();

		//set link stuff
		$this->note_link->set_note_id($note_id);
		$this->note_link->set_table_name('ProgramData');
		$this->note_link->set_table_index('Program_id');
		$this->note_link->save();
	}

	public function remove() {
		$this->note_link->remove();
		parent::remove();
	}
}

?>
