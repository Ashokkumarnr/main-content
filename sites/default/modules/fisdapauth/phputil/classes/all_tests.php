<?php

ini_set('include_path','.:../..');

// The SimpleTest PHP unit testing framework
require_once('lib/simpletest/unit_tester.php');
require_once('lib/simpletest/reporter.php');
require_once('lib/simpletest/mock_objects.php');


$test = &new GroupTest('All tests');
$test->addTestFile('html_builder/test/HTMLTagTester.php');
$test->addTestFile('html_builder/css/test/IDSelectorTester.php');
$test->addTestFile('html_builder/css/test/ClassSelectorTester.php');
$test->addTestFile('html_builder/css/test/TagSelectorTester.php');
$test->addTestFile('html_builder/form_builder/test/FormTester.php');
$test->addTestFile('http/test/RequestTester.php');
$test->addTestFile('controller/test/ControllerTester.php');
$test->addTestFile('logger/test/LoggerTester.php');
$test->addTestFile('validation/test/IntegerValidatorTester.php');
$test->addTestFile('validation/test/IntegerRangeValidatorTester.php');
$test->addTestFile('validation/test/StringWhitelistValidatorTester.php');
$test->addTestFile('model/test/TestItemReviewTester.php');
$test->addTestFile('model/test/TestItemTester.php');

// test the database dump library
$test->addTestFile('../test/database_dump_lib_tests.php');

// test the string utils
$test->addTestFile('../test/string_utils_tests.php');

// test the session functions
$test->addTestFile('../test/session_functions_tests.php');

// Test the Quickbooks Import/Export utilities
$test->addTestFile('phputil/test/quickbook_utils_test.php');

$test->run(new TextReporter());

?>
