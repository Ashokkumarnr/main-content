<?php

require_once('phputil/nusoap.php');
require_once('phputil/avantgo_utils.php');
require_once('phputil/classes/logger/Logger.php');
require_once('phputil/classes/model/FISDAPUser.php');
require_once('phputil/classes/logger/SyslogOutputLogListener.php');

/** 
 * This class encapsulates the Avantgo SOAP server's agsoap
 * object.
 */
class AvantgoSoapAdmin {
    /** the admin user's login name */
    var $admin_user_name = 'admin';

    /** 
     * the base64-encoded, blowfish-encrypted admin user password.  we should
     * be careful not to expose this file's contents over the network
     * unencrypted.
     */
    //var $admin_user_password = '/+aHJYBj0RM=';
    var $admin_user_password = 'TvJqKE+b+QoTdKjJVHAOaA==';

    /** the nusoap client object */
    var $client;

    /** the proxy object made from the avantgo WSDL specification */
    var $proxy;

    /** the user id returned when we log in */
    var $admin_user_id;

    /** the avantgo soap server namespace */
    var $namespace = 'urn:AvantgoWebAPI';

    /** */
    var $soap_headers;

    /** */
    var $logger;

    /** */
    var $avantgo_hostname;

    /** */
    var $is_connected;


    /**
     * Create an AvantgoSoapAdmin to talk to the given pda host.
     */
    function AvantgoSoapAdmin() {
        // set up a logger
        $this->logger =& Logger::get_instance();
		$this->logger->set_level(999);
		$this->logger->add_listener(new SyslogOutputLogListener());
 
        // get the WSDL description and proxy object
        $this->avantgo_hostname = get_pda_hostname();
        $wsdl_server = 'http://'.$this->avantgo_hostname.':8094/avantgoapi.xml';
	$this->logger->log(
                'AvantgoSoapAdmin: connectiong to '.
                $this->avantgo_hostname,
                $this->logger->DEBUG
            );

        $this->client = new soap_client($wsdl_server, true);

        if ( $err = $this->client->getError() ) {

	$this->logger->log(
                'AvantgoSoapAdmin: Connection Error '.
                $this->client->getError(),
                $this->logger->DEBUG
            );

            $this->is_connected = false;
            return;
        }//if

        $this->is_connected = true;

	$this->logger->log(
                'AvantgoSoapAdmin: connected is '.
                $this->is_connected,
                $this->logger->DEBUG
            );

        $this->proxy = $this->client->getProxy();

        $this->server = $server;
        //$this->client = new soap_client($this->server);

        $this->admin_user_id = $this->proxy->loginUser(
            $this->admin_user_name,
            $this->admin_user_password
        );

	$this->logger->log(
                'AvantgoSoapAdmin: user id is '.
                $this->admin_user_id["userId"],
                $this->logger->DEBUG
            );

        // parse the headers to get the session id
        $this->soap_headers = $this->proxy->getHeaders();
        $this->proxy->setHeaders($this->soap_headers);
    }//AvantgoSoapAdmin


    /**
     * Returns the contents of the given user's mal configuration file,
     * as a string
     * 
     * @warning we'll have to be careful about what we allow passwords to be
     *          (spaces, quotes, etc can open security holes).
     * @param string $username the user whose mal file we want
     * @param string $password the user's password
     * @return string the user's mal file contents
     */
    function get_mal_file($username, $password) {
        // sanitize the username
        if ( !preg_match('/[a-zA-Z0-9_]+/',$username) ) {
            die('get_mal_file: invalid username given');
        }//if

        $mal_file_contents = ''
            . '<?xml version="1.0"?> '
            . '<MALServer '
                . 'hostname="' . $this->avantgo_hostname . '" '
                . 'port="80" '
                . 'username="' . $username . '" '
                . 'password="' . $password . '" '
                . 'passwordIsHashed="FALSE" '
                . 'passwordIsEncoded="FALSE" '
                . 'nonce="" '
                . 'disabled="FALSE" '
                . 'friendlyName="M-Business Anywhere" '
                . 'userUrl="http://' . $this->avantgo_hostname . ':8091/" '
                . 'description="The server is running." '
                . 'confirmationcaption="Connected to FISDAP Handheld" '
                . 'serverUri="/sync" '
                . 'sendDeviceInfo="TRUE" '
                . 'hashPassword="TRUE" '
                . 'requestPassword="FALSE" '
                . 'ConnectSecurely="FALSE" '
                . 'AllowSecureConnection="FALSE" '
                . 'ConnectSecureOnly="FALSE" '
                . 'DeviceSecureOverride="FALSE" '
                . 'confirmation="'
                    . 'Your desktop is now ready to sync to your FISDAP '
                    . 'Handheld account [username: ' . $username . ']."'
                . '> '
            . '</MALServer>';

        return $mal_file_contents;
    }//get_mal_file


    /** 
     * Get any errors that might've occurred on the last request
     */
    function get_error() {
        return $this->proxy->getError();
    }//get_error


    /**
     * Ensure that the user is in the default group, so they'll receive
     * the Tracking portion of our application
     *
     * @param int $avantgo_user_id the user's avantgo id
     */
    function add_user_to_primary_group($avantgo_user_id) {
        if ( !$this->remove_user_from_group(
                 $avantgo_user_id,
                 AVANTGO_OLD_PRIMARY_GROUP_ID
             ) ) {
            return false;
        } else if ( !$this->remove_user_from_group(
                 $avantgo_user_id,
                 AVANTGO_OLD_PRIMARY_XML_GROUP_ID
             ) ) {
            return false;
        }//if

        return $this->add_user_to_group(
            $avantgo_user_id,
            AVANTGO_EMS_TRACKING_GROUP_ID
        );
    }//add_user_to_primary_group


    /**
     * Put the user in the pocketpc group, and make sure that the
     * user is no longer in the palm group.
     *
     * @param int $avantgo_user_id the user's avantgo id
     */
    function add_user_to_pocketpc_group($avantgo_user_id) {
        if ( !$this->remove_user_from_group(
                 $avantgo_user_id,
                 AVANTGO_PALM_OS_GROUP_ID
             ) ) {
            return false;
        }//if
        
        return $this->add_user_to_group(
            $avantgo_user_id,
            AVANTGO_POCKETPC_GROUP_ID
        );
    }//add_user_to_pocketpc_group


    /**
     * Put the user in the palm group, and make sure that the
     * user is no longer in the pocketpc group.
     *
     * @param int $avantgo_user_id the user's avantgo id
     */
    function add_user_to_palm_group($avantgo_user_id) {
        if ( !$this->remove_user_from_group(
                 $avantgo_user_id,
                 AVANTGO_POCKETPC_GROUP_ID
             ) ) {
            return false;
        }//if
        
        return $this->add_user_to_group(
            $avantgo_user_id,
            AVANTGO_PALM_OS_GROUP_ID
        );
    }//add_user_to_palm_group


    /**
     * Get a given user's password
     *
     * @warning this function calls a M-Biz SOAP API function that
     * currently seems not to do anything...
     *
     * @param int $avantgo_user_id the user's avantgo id
     * @return string the user's password
     */
    function get_user_password($avantgo_user_id) {
        $user_info = $this->get_user_info($avantgo_user_id);
        if ( $user_info ) {
            return $user_info['password'];
        }//if

        return false;
    }//get_user_password


    /**
     * Get a user's avantgo information
     *
     * @param int $avantgo_user_id the user's avantgo id
     * @return array
     */
    function get_user_info($avantgo_user_id) {
        $user_info = $this->proxy->userGetInfo($avantgo_user_id);
        if ( $error = $this->proxy->getError() ) {
            $this->logger->log(
                'AvantgoSoapAdmin: could not get information for '
                . 'user #' . $avantgo_user_id . ' '
                . '(user: "'.$_SERVER['PHP_AUTH_USER'].'")',
                $this->logger->ERROR
            );
            return false;
        }//if

        if ( is_array($user_info) ) {
            return $user_info;
        }//if 

        return false;
    }//get_user_info


    /**
     * Get information about the groups to which a user belongs.
     *
     * @param int $avantgo_user_id the user's avantgo id
     * @return array
     */
    function get_user_groups($avantgo_user_id) {
        $group_info = $this->proxy->userGetGroups($avantgo_user_id);
        if ( $error = $this->proxy->getError() ) {
            $this->logger->log(
                'AvantgoSoapAdmin: could not get group information for '
                . 'user #' . $avantgo_user_id . ' '
                . '(user: "'.$_SERVER['PHP_AUTH_USER'].'")',
                $this->logger->ERROR
            );
            return false;
        }//if

        $group_ids = array();
        if ( is_array($group_info) && count($group_info) ) {
            foreach ( $group_info as $group ) {
                $group_ids[] = $group['groupId'];
            }//foreach

            return $group_ids;
        }//if 

        return false;
    }//get_user_groups


    /**
     * Add the user to the given group.  Note that if the user is already in 
     * the given group, no action will be taken, but a success value will be 
     * returned.
     *
     * @param int $avantgo_user_id the user's avantgo id
     * @param int $group_id        the group in which to put the user
     * @return bool true if the SOAP call to the PDA server succeeded, or
     *              false otherwise.  the SOAP call should only fail if
     *              there was a connection problem or session error, so
     *              we may not be absolutely sure the user is in the
     *              given group after this call is executed.
     */
    function add_user_to_group($avantgo_user_id, $group_id) {
        $this->proxy->groupAddUser($group_id,$avantgo_user_id);
        if ( $error = $this->proxy->getError() ) {
            $this->logger->log(
                'AvantgoSoapAdmin: could not add user to group #'.$group_id.' '.
                '(user: "'.$_SERVER['PHP_AUTH_USER'].'")',
                $this->logger->ERROR
            );
            return false;
        }//if

        return true;
    }//add_user_to_group


    /**
     * Remove the user from the given group.  
     *
     * @param int $avantgo_user_id the user's avantgo id
     * @param int $group_id        the group from which to remove the user
     * @return bool true if the SOAP call to the PDA server succeeded, or
     *              false otherwise.  the SOAP call should only fail if
     *              there was a connection problem or session error, so
     *              we may not be absolutely sure the user has been
     *              removed from the given group.
     */
    function remove_user_from_group($avantgo_user_id, $group_id) {
        $this->proxy->groupRemoveUser($group_id, $avantgo_user_id);
        if ( $error = $this->proxy->getError() ) {
            $this->logger->log(
                'AvantgoSoapAdmin: could not remove user from group #'
                . $group_id . ' '
                . '('
                . 'user: "' . $_SERVER['PHP_AUTH_USER'] . '", '
                . 'uid: #' . $avantgo_user_id
                . ')',
                $this->logger->ERROR
            );
            return false;
        }//if

        return true;
    }//remove_user_from_group


    /** 
     * Create a new avantgo user account with the given properties.
     *
     * @param string $username the new login name
     * @param string $first_name the user's first name
     * @param string $last_name the user's last name
     * @param string $password the user's password
     * @param string $comment (optional) a comment to use for the account
     */
    function create_user( $username,  
                          $first_name, 
                          $last_name,
                          $password,
                          $comment='' ) {
        $new_user_id = $this->proxy->userCreate(
            array(
                'userName'=>$username,
                'firstName'=>$first_name,
                'lastName'=>$last_name,
                'password'=>$password,
                'comment'=>$comment
            )
        );

        return $new_user_id;       
    }//create_user


    /**
     * Ensure that there's a user account for the student with the 
     * given FISDAP username
     *
     * @return int the avantgo user id of the new user, or false on error
     */
    function create_account_for_connected_user() {
        $user =& FISDAPUser::find_by_username($_SERVER['PHP_AUTH_USER']);
        if ( !$user ) {
            $this->logger->log(
                'AvantgoSoapAdmin: could not get user info from mysql '.
                '(user: "'.$_SERVER['PHP_AUTH_USER'].'")',
                $logger->ERROR
            );
            return false;
        }//if

        $user_id = $this->proxy->userCreate(
            array(
                'userName'=>strtolower($_SERVER['PHP_AUTH_USER']),
                'firstName'=>$user->get_first_name(),
                'lastName'=>$user->get_last_name(),
                'password'=>$_SERVER['PHP_AUTH_PW'],
                'comment'=>'-- this user was created automatically --'
            )
        );
        return $user_id;
    }//create_account_for_connected_user


    /**
     * Make the FISDAP Handheld server's password for the given 
     * FISDAPUser match the given password (which is assumed to 
     * be the FISDAP password for the given user).
     *
     * @return int the new avantgo user id
     */
    function synchronize_user_password($avantgo_user_id, $new_password) {
        // get the old user information
        $user_info = $this->get_user_info($avantgo_user_id);
        if ( !$user_info || !is_array($user_info) ) {
            return false;
        }//if

        // remove the old user (aborting if none)
        $this->proxy->userDelete($avantgo_user_id);
        
        // create a new account with the old user's information,
        // but with the new password
        $new_user_id = $this->create_user( 
            $user_info['userName'],  
            $user_info['firstName'],  
            $user_info['lastName'],  
            $new_password,
            $user_info['comment']
        );
        if ( !$new_user_id ) {
            return false;
        }//if

        // add the new account to the proper groups
        if ( is_array($user_info['groupArray']) ) {
            foreach ( $user_info['groupArray'] as $group ) {
                $success = $this->add_user_to_group(
                    $new_user_id,
                    $group['groupId']
                );
                if ( !$success ) {
                    return false;
                }//if
            }//foreach
        }//if
        
        return $new_user_id;
    }//synchronize_user_password


    /**
     * Return the user_id of the user with the given avantgo 
     * username.  
     *
     * @param string $username the username to search for
     * @return int the id of the user, or false if none found
     */
    function get_user_id($username) {
        $found_users = $this->proxy->userFindUsers(
            $username,                 // username
            '',                        // first name
            '',                        // last name
            '0',                       // start position
            '100',                     // max results
            '',                        // group id
            '',                        // users not in group
            ''                         // admins not in group
        );
        if ( $this->proxy->getError() ) {
            $this->logger->log(
                'setuppda.html: could not get user information from PDA '.
                'SOAP server (user: "'.$username.'")',
                $logger->ERROR
            );
            return false;
        }//if
        
        // loop over the users returned, looking for an exact match to the
        // username
        $avantgo_user_id = false;
        if ( $found_users['users'] ) {
            foreach( $found_users['users'] as $user ) {
                if ( $user['userName'] == $username ) {
                    $avantgo_user_id = $user['userId'];
                    break;
                }//if
            }//foreach
        }//if
        return $avantgo_user_id;
    }//get_user_id


    /**
     * Returns a list of all PDA server users.
     *
     * @return array a list of all user objects from the PDA server
     */
    function get_all_users() {
        $fetch_limit = 50;
        $all_users = array();
        $has_more_users = true;
        $position = 0;
        while ( $has_more_users ) {
            $user_result = $this->proxy->userFindUsers(
                '',            // username
                '',            // first name
                '',            // last name
                $position,     // start position
                $fetch_limit,  // max results per query
                '',            // group id
                '',            // users not in group
                ''             // admins not in group
            );
            if ( $this->proxy->getError() ) {
                $this->logger->log(
                    'setuppda.html: could not get users from PDA SOAP server',
                    $logger->ERROR
                );
                return false;
            }//if

            if ( !is_array($user_result) || 
                 !is_array($user_result['users']) ||
                 !count($user_result['users']) 
               ) {
               $has_more_users = false;
            } else {
                foreach( $user_result['users'] as $user ) {
                    $all_users[] = $user;
                }//foreach
                $position += $fetch_limit;
            }//if
        }//while
        
        return $all_users;
    }//get_all_users


    function update_user($avantgo_user_id, 
                         $new_username,
                         $new_firstname,
                         $new_lastname) {
        $this->proxy->userUpdate(
            $avantgo_user_id,
            array(
                'userName' => $new_username,
                'firstName' => $new_firstname,
                'lastName' => $new_lastname
            )
        );

        if ( $error = $this->proxy->getError() ) {
            $this->logger->log(
                'AvantgoSoapAdmin: could not update username for '
                . 'user #' . $avantgo_user_id . ' '
                . '(user: "'.$_SERVER['PHP_AUTH_USER'].'")',
                $this->logger->ERROR
            );
            return false;
        }//if

        return true;
    }//update_user


    /**
     * Returns the hostname of the avantgo server (depending on whether
     * a development or production environment was detected).
     *
     * @return string the avantgo pda server hostname 
     */
    function get_avantgo_hostname() {
        return $this->avantgo_hostname;
    }//get_avantgo_hostname


    /**
     * Did we successfully connect to the PDA server?
     */
    function is_connected() {
        return $this->is_connected;
    }//is_connected
}//class AvantgoSoapAdmin

?>
