<?php

/**
 * GridForm.inc
 *
 * Creates a form of checkboxes formatted in a grid.
 *
 * @author Eryn O'Neil
 * @email eoneil@fisdap.net
 * @date 15 July 2009
 * @todo Incorporate this into the (much more robust) common_table/common_form libraries
 *
 */

require_once("../phputil/classes/common_utilities.inc");
require_once("../phputil/classes/common_user.inc");
require_once("../phputil/classes/common_form.inc");
require_once("../phputil/classes/common_page.inc");
require_once("../phputil/classes/domainprompts.inc");
require_once("../phputil/classes/domainpromptstudentspicker.inc");
require_once("../phputil/dbconnect.html");
require_once("../phputil/price.php");
require_once("../phputil/user_utils.php");
require_once("../phputil/program_utils.php");
require_once("account_functions.inc");

class GridForm {

	/*********
	 * FIELDS
	 *********/
	
	const FORM_NAME = 'gridForm';

	private $JS_FILENAME; // Must be included on the page!
	private $CSS_FILENAME;

	protected $submit_url; // String. Where the form submits to.
	protected $is_bundle_discount; // Boolean. True if any kind of bundle discount is offered.
	protected $bundle_discounts_array; // Array of discounts. In the form "hidden field name" => "account type". Will be computed and placed as hidden variables for JS to access.
	protected $current_program; // Int. Program ID
	protected $left_heading; // String or null. The title over the leftmost column (can be null)
	protected $column_headings; // Array of strings. The titles to go along the top, as 'one word id' => 'Display'
	protected $row_array; // Multi-dimensional array. Generated based off of the input to add_row_data(...)
	protected $show_price; // Boolean. True if this form needs to compute and display a running total
	protected $hidden_fields; // Array of key-value pairs to be included as hidden fields
	protected $submit_button1 = 'Submit'; // String. Button text
	protected $submit_button2; // String or null. Button text if there is a separate button, null if there is none
	protected $totalPrice = 0.0; // Int or float.
	protected $useImageForChecked;
	protected $checkedImagePath;


	function __construct($currentProgram) {
		$this->submit_url = $_SERVER['PHP_SELF'];
		$this->is_bundle_discount = false;
		$this->bundle_discounts_array = array();
		$this->currentProgram = $currentProgram;
		$this->column_headings = array();
		$this->row_array = array();
		$this->show_price = true; // This defaults to true because submission without it is not implemented yet
		$this->hidden_fields = array();
		$this->submit_button1 = 'Submit';
		$this->totalPrice = 0.0;
		$this->useImageForChecked = false;
		$this->JS_FILENAME = FISDAP_WEB_ROOT . 'admin/grid-accounts.js';
		$this->CSS_FILENAME = FISDAP_WEB_ROOT . 'admin/orderpages.css';
	}

	public function get_js_filename() {
		return $this->JS_FILENAME;
	}
	public function get_css_filename() {
		return $this->CSS_FILENAME;
	}

	public function set_submission_url($url) {
		$this->submit_url = $url;
	}

	function set_bundle_discount($product1, $product2, $discount) {
		$this->is_bundle_discount = true;
		$this->bundle_discounts_array[] = array($product1, $product2, $discount);
	}

	function set_left_heading($heading) {
		$this->left_heading = $heading; 
	}

	/**
	 * A multi-dimensional array containing an array for each heading. 
	 * Array( Array( [0] => ID name,
	 *               [1] => Display name),
	 *        Array( [0] => ID name...
	 *      )
	 * etc. 
	 * The ID name must be alpha-numeric and lowercase. It is used by html, css, etc.
	 */
	function set_column_headings($headingArray) {
		Assert::is_array($headingArray);
		foreach ($headingArray as $a) {
			Assert::is_array($a);
		}
		$this->column_headings = $headingArray;
	}

	/**
	 * Add a row to the grid. 
	 *
	 * @param string $displaytext The text to be displayed in the leftmost column
	 * @param string $useriden_1 Text used to identify this row (for example, a username or account type)
	 * @param string $useriden_2 Text used to identify this row (for example, a username or account type)
	 * @param array $status An array with an element for each checbox. For each element, a bit packed int where 0 = unchecked, 1 = checked, 2 = disabled. 
	 */
	function add_row($displayname, $useriden_1, $useriden_2, $status) {
		Assert::is_array($status);
		$this->row_array[] = array($displayname, $useriden_1, $useriden_2, $status);
	}

	public function set_show_price($doitornot) {
		Assert::is_boolean($doitornot);
	}

	function add_hidden_field($key, $value) {
		$this->hidden_fields[$key] = $value;
	}

	function set_submit_button_1_text($text) {
		$this->submit_button1 = $text;
	}

	function set_submit_button_2_text($text) {
		$this->submit_button2 = $text;
	}

	function set_checked_image_url($url) {
		$this->useImageForChecked = true;
		$this->checkedImagePath = $url;
	}

	function generate_form_html() {
		$s = ""; //Here we go.

		$s .= '<form name="' . self::FORM_NAME . '" id="' . self::FORM_NAME . '" method="POST" action="' . $this->submit_url . "\">\n";
		if ($this->is_bundle_discount) {
			foreach ($this->bundle_discounts_array as $fieldname => $accounttype) {
				$s .= self::generate_hidden_field($fieldname, GetSchedulerBundleDiscount($this->currentProgram, $accounttype));
			}
		}
		$s .= '<div id = "tablediv">';
		$s .= "<table class='selectupgrades mediumtext' cellspacing=5 cellpadding=5 width='100%'>\n";
		$s .= '<tr><th>' . $this->left_heading . "</th>\n";
		foreach ($this->column_headings as $row) {
			$s .= '<th class="mediumbold">' . $row[1] . "</th>\n";
		}
		$s .= "</tr>\n"; 

			//And checkboxes for each user, named like "userid-productname"
			for ($j=0; $j < sizeof($this->row_array); $j++) {
				$row_title = $this->row_array[$j][0];
				$user_iden1 = $this->row_array[$j][1]; //The user can pass in anything that is helpful for these fields. In account ordering, this will be a username
				$user_iden2 = $this->row_array[$j][2]; // In account ordering, this will be an account type.
				$checks = $this->row_array[$j][3]; // If each box is checked or disabled

				$s .= "<tr class='checkbox" . $j%2 . "'>\n";
				$s .= "<td class='name'>$row_title</td>\n";
				for ($k = 0; $k < count($this->column_headings); $k++) {
					$heading_id = $this->column_headings[$k][0];
					$status = $checks[$k];
					if ($status & 1) $checked = true;
					if ($status & 2) $greyed = true;
					$s .= '<td class="box">' . self::generate_box($user_iden1, $user_iden2, $heading_id, $checked, $greyed) . "</td>\n";
				}
				$s .= "</tr>\n\n";
			}

		// Make the "check all/none" boxes
		$s .= "<tr class='checkbox" . $j%2 . "'>\n";
		$s .= "<td class='name'></td>\n"; 
		foreach ($this->column_headings as $row) {
			$heading_id = $row[0];
			$display_heading = $row[1];
			$s .= '<td class="jsbox">' . self::generate_js_box($display_heading, $heading_id) . "</td>\n";
		}

		$s .= "</table></div>\n";

		// Price Total boxes
		if ($this->show_price) {
			$s .= '<div class="price"><div class="whiteBorder"><span class="largetext">';
			$s .= 'Total: <span id="totalDisplayArea" style="largetext">' . show_as_currency($totalPrice) . "</span></span></div></div>\n";
		}

		foreach ($this->hidden_fields as $key => $value) {
			$s .= self::generate_hidden_field($key, $value);
		}

		$s .= '<div id="gridSubmit">';

		$s .= "<input type='button' id='gridsubmit1' value='$this->submit_button1' ";
		if ($this->show_price) $s .= "onClick=\"formValidationAndSubmission('" . self::FORM_NAME . "', 'invoice')\"";
		$s .= ">\n";

		if (isset($this->submit_button2)) {
			$s .= "<input type='button' value='$this->submit_button2'";
			if ($this->show_price) $s .= "onClick=\"formValidationAndSubmission('" . self::FORM_NAME . "', 'credit')\"";
			$s .= ">\n";
		}
		$s .= "</div></form>\n";

		return $s;

	}

	/** 
	 * FUNCTION generate_box -
	 *
	 * Generates a checkbox for the big chart on this page, naming it like
	 * "Xusername-accounttype_product", or displays a checkmark image if the
	 * user already has access to the product.
	 *  
	 * @param $currentUser a user object
	 * @param $product a product string compatible with the common_user->get_product_access function
	 * @param $checked an optional boolean that will override the checked-ness of this box
	 * @param $greyed an optional boolean that will override the greyed-out-ness of this box
	 */
	private function generate_box($iden1, $iden2, $iden3, $checked, $greyed=false) {

		$price = $this->get_price($iden1, $iden2, $iden3);

		$name = 'X' . $iden1 . '-' . $iden2 . '_' . $iden3;
		if ($checked && $this->useImageForChecked) {
				$returnString = "<img src='../images/check_icon_sm.png' id='$name' name='$name'>";
		}
		else {
			$returnString = "<input type='checkbox' name='$name' id='$name'";
			if ($checked == true) {
				$returnString .= ' CHECKED';
				$this->totalPrice += $price;
			}
			if($greyed==true) {
				$returnString .= ' DISABLED';
			}

			/*		if ($product == "pda") {
					$returnString = $returnString . " onload=\"pdaValidation('$name')\"";
					}  */

			$returnString .= " onClick=\"clickFunctions('$name', $price)\">";
		}

		return $returnString;

	}

	/**
	 * Generates a checkbox used with the javascript "checkUncheckSome" function.
	 * Names the box jsSelectAll<publicProductName>, and uses the string
	 * <internalProductCode>String. 
	 *
	 * @param $publicProductName the name used in the "Select All Product" string, most likely capitalized
	 * @param $internalProductCode the name used in the comma delineated string for the js script, based
	 *         based on the codes we use in the common_user.inc get_product_access function, most likely
	 *         lowercase
	 * @param $greyed an optional field saying whether this box should be greyed (unselectable)
	 */
	private function generate_js_box($displayName, $id, $greyed=false) {

		$controllerNameAll = "jsSelectAll" . $id;
		$controllerNameNone = "jsSelectNone" . $id;
		//tringName = $internalProductCode . "String";

		$returnString = "<a href='javascript: void(0)' name='$controllerNameAll' onClick=\"checkAllBoxes('" . self::FORM_NAME . "', '$id')\">";
		$returnString .= "All</a><br>\n";
		$returnString .= "<a href='javascript: void(0)' name='$controllerNameNone' onClick=\"uncheckAllBoxes('" . self::FORM_NAME . "', '$id')\">";
		$returnString .= "None</a>";

		return $returnString;

	}

	/**
	 * Generates hidden fields for easy form population
	 */
	private function generate_hidden_field($id, $value) {

		$returnString = "<input type='hidden' name='$id' id='$id' value='$value'>";

		return $returnString;

	}

	private function get_price($iden1, $iden2, $iden3) {
		//STUB
		return 50;
	}

}
?>
