<?php

require_once('phputil/classes/html_builder/form_builder/Form.php');
require_once('phputil/classes/html_builder/form_builder/FormQuestion.php');
require_once('phputil/classes/validation/ValidationError.php');


/**
 * This class contains unit tests for the Form class
 */
class FormTester extends UnitTestCase {
    function setUp() {
        // make a mock FormQuestion to test adding questions to a form
        Mock::generate('FormQuestion');

        // a couple dummy forms
        $this->form1 =& new Form('form1action.php','POST');
        $this->form2 =& new Form('form2action.php','GET');

        // some dummy form questions
        $this->question1 =& new MockFormQuestion($this);
        $this->question2 =& new MockFormQuestion($this);
        $this->question3 =& new MockFormQuestion($this);
        $this->question4 =& new MockFormQuestion($this);
        $this->question5 =& new MockFormQuestion($this);
  
        // make the questions look like they contain their namesake inputs
        $this->question1->setReturnValue('contains_named_input',true,array('question1'));
        $this->question2->setReturnValue('contains_named_input',true,array('question2'));
        $this->question3->setReturnValue('contains_named_input',true,array('question3'));
        $this->question4->setReturnValue('contains_named_input',true,array('question4'));
        $this->question5->setReturnValue('contains_named_input',true,array('question5'));

        // make the questions look like they support get_elements_by_selector
        $this->question1->setReturnValue('get_elements_by_selector',array(&$this->question1));
        $this->question2->setReturnValue('get_elements_by_selector',array(&$this->question2));
        $this->question3->setReturnValue('get_elements_by_selector',array(&$this->question3));
        $this->question4->setReturnValue('get_elements_by_selector',array(&$this->question4));
        $this->question5->setReturnValue('get_elements_by_selector',array(&$this->question5));

        // add the questions to the forms
        $this->form1->append_child($this->question1);
        $this->form1->append_child($this->question2);
        $this->form1->append_child($this->question3);

        $this->form2->append_child($this->question4);
        $this->form2->append_child($this->question5);
    }//setUp

   
    function tearDown() {
    }//tearDown


    function test_add_error() {
        // the test error to add
        $error_obj =& new ValidationError('this is a test error');

        // we'll try to add errors to these questions
        $this->question1->expectOnce('add_error',array(&$error_obj));
        $this->question2->expectOnce('add_error',array(&$error_obj));
        $this->question5->expectOnce('add_error',array(&$error_obj));

        // these questions will never get errors added
        $this->question3->expectNever('add_error');
        $this->question4->expectNever('add_error');

        // add a couple errors to form 1
        $this->form1->add_error($error_obj,'question1');
        $this->form1->add_error($error_obj,'question2');

        // add a couple errors to form 2
        $this->form2->add_error($error_obj,'question5');

        $this->question1->tally();
        $this->question2->tally();
        $this->question3->tally();
        $this->question4->tally();
        $this->question5->tally();
    }//test_add_error

}//class FormTester  

?>
