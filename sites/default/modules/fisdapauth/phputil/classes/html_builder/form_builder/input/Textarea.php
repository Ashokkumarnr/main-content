<?php

/**
 *
 */
class Textarea extends FormInput {
    /**
     * 
     */
    function Textarea($name) {
        $this->HTMLTag(
            'textarea',
            array(
                'name'=>$name,
            )
        );
    }//Textarea

    /**
     *
     */
    function populate($input) {
        if ( isset($input[$this->get_attribute('name')]) ) {
            $can_change_whitespace = false;
            $this->append_child(new HTMLText($input[$this->get_attribute('name')],$can_change_whitespace));
        }//if
    }//populate


    /**
     * Textareas should have no spaces between the opening and closing
     * tags
     */
    function to_string($depth=0) {
        $child_depth = 0;
        $prefix = $this->get_prefix_string($depth);
        $output = $prefix . $this->get_opening_tag();
        if ( $this->has_children() ) {
            $output .= htmlentities($this->get_child_html($child_depth));
        }//if
        $output .= $this->get_closing_tag() . "\n";
        return $output;
    }//to_string


    function get_child_html($depth) {
        $output = '';
        if ( $this->children ) {
            foreach( $this->children as $child ) {
                $output .= $child->to_string($depth);
            }//foreach
        }//if
        return $output;
    }//get_child_html
}//class Textarea
?>
