<?php

/**
 * This class encapsulates shared behaviors between the input elements whose
 * values are set by setting the value attribute to the prescribed input.
 */
class ValueAttrInput extends FormInput {

   //////////////////////////////////////////////////////////// 
   // Public instance methods 
   //////////////////////////////////////////////////////////// 

    /**
     * If this element has an entry in the given input array, set the
     * value attribute to the value in the input.
     *
     * @param array $inputs hash of name/value pairs (e.g., form postdata)
     */
    function populate( $inputs ) {
        if ( isset($inputs[$this->get_attribute('name')]) ) {
            $this->set_attribute('value',htmlentities($inputs[$this->get_attribute('name')]));
        }//if
    }//populate
}//class ValueAttrInput

?>
