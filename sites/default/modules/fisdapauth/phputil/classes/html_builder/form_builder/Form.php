<?php

// the html model classes
require_once('phputil/classes/html_builder/HTMLChunk.php');
require_once('phputil/classes/html_builder/HTMLTag.php');
require_once('phputil/classes/html_builder/HTMLText.php');
require_once('phputil/classes/html_builder/css/ClassSelector.php');

// the form model classes
require_once('phputil/classes/html_builder/form_builder/FormQuestion.php');
require_once('phputil/classes/html_builder/form_builder/input/FormInput.php');
require_once('phputil/classes/html_builder/form_builder/input/SelectMenu.php');
require_once('phputil/classes/html_builder/form_builder/input/CheckedAttrInput.php');
require_once('phputil/classes/html_builder/form_builder/input/ValueAttrInput.php');
require_once('phputil/classes/html_builder/form_builder/input/TextInput.php');
require_once('phputil/classes/html_builder/form_builder/input/Textarea.php');
require_once('phputil/classes/html_builder/form_builder/input/HiddenInput.php');
require_once('phputil/classes/html_builder/form_builder/input/RadioButton.php');
require_once('phputil/classes/html_builder/form_builder/FormField.php');
require_once('phputil/classes/html_builder/form_builder/YesNoFormQuestion.php');
require_once('phputil/classes/html_builder/form_builder/input/YesNoRadioGroup.php');


/**
 * This class encapsulates an HTML form.  This provides a simple API
 * for form creation, display, validation, and processing.
 */
class Form extends HTMLTag {

    ////////////////////////////////////////////////////////////
    //  Private instance variables
    ////////////////////////////////////////////////////////////

    /** The form's global notification area, for error messages, etc */
    var $notification_area;


    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     * Create a Form with the given action and method
     *
     * @param string $action the URL to send the form input to
     * @param string $method either GET or POST
     */
    function Form( $action, $method ) {
        $this->HTMLTag(
            'form',
            array(
                'action'=>$action,
                'method'=>$method
            )
        );

        $this->has_errors = false;

        // add the generic error notification area
        $this->notification_area =& new HTMLTag('div',array('class'=>'notification_area'));
        $this->append_child($this->notification_area);

        // add the action input field
        // @todo ensure that no other action field is allowed on the form
        $this->notification_area->append_child(new HiddenInput('action','submit'));
    }//Form


    ////////////////////////////////////////////////////////////
    // Public Instance Methods
    ////////////////////////////////////////////////////////////

    /**
     * Use the given associative array of errors (keyed by the
     * corresponding input names) as the form's error messages.
     *
     * @param array $errors the errors to display
     */
    function add_errors( $errors ) {
        if ( $errors ) {
            foreach( $errors as $field=>$field_errors ) {
                // loop over the children, adding each error as we 
                // find the corresponding field
                if ( $field_errors ) {
                    foreach( $field_errors as $error ) {
                        $this->add_error($error,$field);
                    }//foreach
                }//if
            }//foreach
        }//if
    }//add_errors


    /**
     * Add the given error's message to the global notification 
     * area.
     *
     * @param ValidationError $error the error to display
     */
    function add_generic_error( $error ) {
        $container =& new HTMLTag('div',array('class'=>'error_message'));
        $container->append_child(new HTMLText($error->get_message()));
        $this->notification_area->append_child($container);
    }//add_generic_error

    /**
     * Add some custom javascript to the form
     *
     * @param string $code the code to put in the javascript container
     */
    function add_javascript($code) {
	    $script_container =& new HTMLTag('script',array('type'=>'text/javascript'));
	    $script_container->append_child(new HTMLText($code));
	    $this->notification_area->append_child($script_container);
    }


    /** 
     * Add the given error to the form.
     *
     * @param ValidationError $error the error to add
     * @param string          $field (optional) the input field
     */
    function add_error( $error, $field='' ) {
        if ( !$field ) {
            $this->add_generic_error($error);
        } else {
            $children = $this->get_elements_by_selector(new ClassSelector('form_question'));
            $num_children = count($children);
            $found_field = false;
            for( $i=0 ; $i < $num_children ; $i++ ) {
                $child =& $children[$i];
                if ( method_exists($child,'add_error') && 
                     method_exists($child,'contains_named_input') ) {
                    if ( $child->contains_named_input($field) ) {
                        $child->add_error($error);
                        $found_field = true;
                        break;
                    }//if
                }//if
            }//for

            // if no question contained the field, add the error 
            // to the form's global notification area
            if ( !$found_field ) {
                $this->add_generic_error($error);
            }//if
        }//else
    }//add_error
}//class Form

?>
