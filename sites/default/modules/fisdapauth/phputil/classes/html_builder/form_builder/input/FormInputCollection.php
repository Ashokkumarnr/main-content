<?php

/**
 * This class encapsulates html components which collect 
 * input from the user.  This class provides input validation
 * and not much else.  It is up to subclasses to implement the
 * display and to_string methods.
 */
class FormInputCollection extends HTMLTag {

    ////////////////////////////////////////////////////////////
    //  Private instance variables
    ////////////////////////////////////////////////////////////

    /** the validators to use on these inputs */
    var $validators;

    /** the input elements contained herein */
    var $inputs;

    /** true iff there were errors validating this collection */
    var $has_errors;


    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /** 
     * Create a collection containing the given input elements.
     *
     * @param array $responses the FormInput elements to put in the collection
     */
    function FormInputCollection($responses) {
        // add the given FormInputs to the collection
        $this->add_responses($responses);
    }//FormInputCollection


    ////////////////////////////////////////////////////////////
    // Public Instance Methods
    ////////////////////////////////////////////////////////////

    /**
     * Add the given FormInputs to this tag, and take note of any
     * inputs (for validation).
     *
     * @param array $responses the FormInput elements to put in the collection
     */
    function add_responses($responses) {
        if ( $responses ) {
            $num_responses = count($responses);
            for( $i=0 ; $i < $num_responses ; $i++ ) {
                $response =& $responses[$i];

                // keep track of all inputs in the responses
                $response_inputs = $response->get_inputs();
                $num_response_inputs = count($response_inputs);
                for( $j=0 ; $j < $num_response_inputs ; $j++ ) {
                    $input =& $response_inputs[$j];   
                    $this->inputs[] =& $input;
                }//for

                $this->append_child(&$response);
            }//for
        }//if
    }//add_responses
    

    /** 
     * Get an array of all FormInputs for this collection
     *
     * @return array all FormInputs contained within this collection
     */
    function get_inputs() {
        return $this->inputs;
    }//get_inputs
    

    /** 
     * Append the error message at the end of the collection. 
     * Subclasses should override this to get more flexible error
     * handling.
     *
     * @param string $message the message to report
     */
    function add_error_message( $message ) {
        $this->has_errors = true;
        $this->children[] = new HTMLText($message); 
    }//add_error_message


    /** 
     * Append the given validator to our internal list
     *
     * @param FormInputValidator $validator a validator to use on this collection
     */
    function add_validator( &$validator ) {
        $this->validators[] =& $validator;
    }//add_validator


    /** 
     * Return true iff there were errors in validation
     *
     * @return bool true iff there were errors in validation
     */
    function has_errors() {
        return $this->has_errors;
    }//has_errors
    

    /** 
     * Validate the collection against the given submission
     *
     * @param array $postdata the input to validate against
     */
    function validate($postdata) {
        $i = 0;
        $num_validators = count($this->validators);
        for( $i=0 ; $i < $num_validators ; $i++ ) {
            $validator =& $this->validators[$i];
            $validator->validate($postdata);
            if( $num_errors = $validator->has_errors() ) {
                $errors = $validator->get_errors();
                for( $j=0 ; $j < $num_errors ; $j++ ) {
                    $error =& $errors[$j];
                    $this->add_error_message($error->get_message());
                }//for
            }//if
        }//for
    }//validate

}//FormInputCollection

?>
