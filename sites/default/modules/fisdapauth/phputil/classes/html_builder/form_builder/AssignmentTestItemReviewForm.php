<?php

require_once('phputil/classes/model/TestCreationProject.php');
require_once('phputil/classes/html_builder/form_builder/Form.php');
require_once('phputil/classes/html_builder/css/IDSelector.php');

/**
 *
 */
class AssignmentTestItemReviewForm extends Form {

    var $item_id;
    var $connection_obj;

    /**
     *
     */
    function AssignmentTestItemReviewForm($item_id) {
        $this->item_id = $item_id;

        $this->connection_obj =& FISDAPDatabaseConnection::get_instance();

        $this->Form('item_review.php','POST');

        // the container for all form elements
        $container_element =& new HTMLTag('div',array());
        $this->append_child($container_element);

        // the hidden display mode field
        $display_mode_input =& new HiddenInput( 'mode', 'assignment' );
        $container_element->append_child($display_mode_input);

        // the hidden assignment id field
        $assignment_id_input =& new HiddenInput( 'assignment_id', '0' );
        $container_element->append_child($assignment_id_input);

        // the hidden AssignmentReviewType field
        $assignment_type_input =& new HiddenInput( 
            'AssignmentReviewType',
            DEFAULT_ASSIGNMENT_REVIEW 
        );
        $container_element->append_child($assignment_type_input);

        // for individuals, use a hidden input for the reviewer and number_in_group
        // fields
        $reviewer_input =& new HiddenInput( 'reviewer', $_SERVER['PHP_AUTH_USER'] );
        $container_element->append_child($reviewer_input);
        $number_in_group_input =& new HiddenInput( 'number_in_group', 1 );
        $container_element->append_child($number_in_group_input);

        // the hidden item id field
        $item_id_input =& new HiddenInput( 'item_id', $item_id );
        $container_element->append_child($item_id_input);

        // the hidden review id field
        $review_id_input =& new HiddenInput( 'id', ((isset($review_id)) ? $review_id : '0') );
        $container_element->append_child($review_id_input);

        $question =& new YesNoFormQuestion(
            '1. Is the item STEM grammatically and structurally correct?',
            'stem_grammar_correct' 
        );
        $question->set_attribute('id','question_1');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '2. Do the DISTRACTORS match the stem grammatically and structurally?',
            'distractors_match_grammar' 
        );
        $question->set_attribute('id','question_2');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '3. Are the DISTRACTORS less correct, yet plausible?',
            'distractors_less_correct' 
        );
        $question->set_attribute('id','question_3');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '4. Are the DISTRACTORS similar in appearance and content, yet distinct?',
            'distractors_similar_yet_distinct' 
        );
        $question->set_attribute('id','question_4');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '5. Is the answer CORRECT in all circumstances?',
            'answer_always_correct' 
        );
        $question->set_attribute('id','question_5');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '6. Is the item free from UNNECESSARY complication?',
            'item_free_from_complication' 
        );
        $question->set_attribute('id','question_6');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '7. Is this item UNBIASED toward all groups and regions?',
            'item_unbiased' 
        );
        $question->set_attribute('id','question_7');
        $container_element->append_child($question);

        $kap_question_name = 'item_measures_k_a_p';
        $question =& new FormQuestion(
            '8. Does this item measure knowledge, application, or problem solving abilities?',
            array(
                new FormField(
                    new RadioButton(
                        $kap_question_name,
                        'k'
                    ),
                    'Knowledge: Recall of facts, terms, & basic concepts<br>'
                ),
                new FormField(
                    new RadioButton(
                        $kap_question_name,
                        'a'
                    ),
                    'Application: Using knowledge to assess a <i>new</i> situation<br>'
                ),
                new FormField(
                    new RadioButton(
                        $kap_question_name,
                        'p'
                    ),
                    'Problem Solving: Evaluation, analysis, and judgment'
                )
            )
        );
        $question->set_attribute('id','question_8');
        $container_element->append_child($question);

        $question =& new FormQuestion(
            '9. What is the likelihood that a MINIMALLY QUALIFIED candidate would get this correct?',
            array(
                new FormField(
                    new TextInput(
                        'percent_minimally_qualified',
                        '',
                        '3'
                    ),
                    '% '
				)
            )
        );
        $question->set_attribute('id','question_9');
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '10. Does the test item\'s LEVEL OF DIFFICULTY correspond to the '.
                'level of difficulty as used on the job?',
            'level_of_difficulty_matches_job' 
        );
        $question->set_attribute('id','question_10');
        $container_element->append_child($question);

        $memorize_question_name = 'important_to_memorize';
        $question =& new FormQuestion(
            '11. On the first day of the job, how important is it that the '.
                'candidate MEMORIZE the information/skill tested?',
            array(
                new FormField(
                    new RadioButton(
                        $memorize_question_name,
                        '0'
                    ),
                    'not necessary; can be looked up without impacting patient care <br>'
                ),
                new FormField(
                    new RadioButton(
                        $memorize_question_name,
                        '1'
                    ),
                    'important; patient care may be negatively impacted <br>'
                ),
                new FormField(
                    new RadioButton(
                        $memorize_question_name,
                        '2'
                    ),
                    'essential; patient care will be negatively impacted'
                )
            )
        );
        $question->set_attribute('id','question_11');
        $container_element->append_child($question);


        $rationale_textarea =& new Textarea('explain_rationale');
        $rationale_textarea->set_attribute('id','explain_rationale');
        $rationale_textarea->set_attribute('cols',100);
        $rationale_textarea->set_attribute('rows',15);
        $question =& new FormQuestion(
            'Please explain your rationale for marking "no" on any of the Yes/No questions above:',
            array(
                new FormField(
                    $rationale_textarea
                )
            )
        );
        $question->set_attribute('id','explain_rationale_question');
        $container_element->append_child($question);


        $proposed_edit_textarea =& new Textarea('proposed_edit');
        $proposed_edit_textarea->set_attribute('id','proposed_edit');
        $proposed_edit_textarea->set_attribute('cols',100);
        $proposed_edit_textarea->set_attribute('rows',15);
        $question =& new FormQuestion(
            'If necessary, use the following box to edit the item so that it better '.
            'meets the above criteria.',
            array(
                new FormField(
                    $proposed_edit_textarea
                )
            )
        );
        $question->set_attribute('id','proposed_edit_question');
        $container_element->append_child($question);

        $submit_button_is_singleton = true;
        $submit_button = new HTMLTag(
            'input',
            array(
                'id'=>'submit_button',
                'type'=>'submit',
                'value'=>'Submit'
            ),
            $submit_button_is_singleton
        );
        $container_element->append_child($submit_button);
    }//AssignmentTestItemReviewForm


    /**
     *
     */
    function disable_submission() {
        $matching_elements = $this->get_elements_by_selector(
            new IDSelector('submit_button')
        );

        $submit_button =& $matching_elements[0];
        if ( !$submit_button ) {
            die(
                'We encountered an error disabling edits to this form.  '.
                'Please go back and try again.'
            );
        }//if

        $submit_button->set_attribute('disabled','disabled');
    }//disable_submission


    /**
     *
     */
    function populate_with_defaults() {
        // since this is a consensus form, default the yes-no values to yes to 
        // speed up the process a little
        $yes_no_question = new YesNoFormQuestion('','');
        $YES_VALUE = $yes_no_question->YES_VALUE;
        $default_values = array(
            'level_of_difficulty_matches_job'=>$YES_VALUE, 
            'item_unbiased'=>$YES_VALUE, 
            'item_free_from_complication'=>$YES_VALUE, 
            'answer_always_correct'=>$YES_VALUE, 
            'stem_grammar_correct'=>$YES_VALUE, 
            'distractors_match_grammar'=>$YES_VALUE, 
            'distractors_less_correct'=>$YES_VALUE, 
            'distractors_similar_yet_distinct'=>$YES_VALUE,
            'proposed_edit'=> get_test_item_plaintext(
                $this->item_id,
                $this->connection_obj->get_link_resource()
            )
        );
        $this->populate($default_values);
    }//populate_with_defaults
}//class AssignmentTestItemReviewForm

?>
