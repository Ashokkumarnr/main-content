<?php

require_once('phputil/classes/html_builder/form_builder/Form.php');

// Define DB vals for account types we use in reviews
define('EMTB', 1);
define('PARAMEDIC', 3);

class ConsensusTestItemReviewForm extends Form {
    function ConsensusTestItemReviewForm($item_id) {
        $this->Form('item_review.php','POST');

        // the container for all form elements
        $container_element =& new HTMLTag('div',array());
        $this->append_child($container_element);

        // the hidden display mode field
        $display_mode_input =& new HiddenInput( 'mode', 'consensus' );
        $container_element->append_child($display_mode_input);

        // the hidden item id field
        $item_id_input =& new HiddenInput( 'item_id', $item_id );
        $container_element->append_child($item_id_input);

        // the hidden review id field
        $review_id_input =& new HiddenInput( 'id', ((isset($review_id)) ? $review_id : '0') );
        $container_element->append_child($review_id_input);

        // let consensus reviewers edit the items they're reviewing
        $edit_item_link_text = '<div>If necessary, you may <strong>'.
                               '<a href="javascript:edit_item('.$item_id.')">edit</a>'.
                               '</strong> this item.</div>';
        $edit_item_link =& new HTMLText($edit_item_link_text);
        $container_element->append_child($edit_item_link);
        
        // allow selection of the group/individual submitting the form
  /*      $number_selector = new SelectMenu( 'number_in_group' );
        $MIN_GROUP_SIZE = 1;
        $MAX_GROUP_SIZE = 10;
        for( $i=$MIN_GROUP_SIZE ; $i <= $MAX_GROUP_SIZE ; $i++ ) {
            $option =& new HTMLTag('option',array('value'=>"$i"));
            $option_text =& new HTMLText("$i");
            $option->append_child($option_text);
            $number_selector->append_child($option);
        }//for

        $number_selector_field = new FormField( $number_selector, '' );

        $number_in_group_question = new FormQuestion(
            'Number of people in the group',
            array(
                $number_selector_field
            )
        );
        $container_element->append_child($number_in_group_question);

        $reviewer_selector = new SelectMenu( 'reviewer' );

        $reviewers = array(
            'Mighty Mouse','Underdog','Hong Kong Phooey','Wonder Woman','Supergirl',
            'She-Ra','Storm','Rogue','Catwoman','The Bionic Woman',
            'Peter Parker','Batman','Spiderman','Superman','James Bond'
        );

        foreach( $reviewers as $reviewer ) {
            $option =& new HTMLTag('option',array('value'=>"$reviewer"));
            $option_text =& new HTMLText("$reviewer");
            $option->append_child($option_text);
            $reviewer_selector->append_child($option);
        }//foreach
        $reviewer_selector_field = new FormField( $reviewer_selector, '' );
        $reviewer_question = new FormQuestion(
            'Reviewer:',
            array(
                $reviewer_selector_field
            )
        );
        $container_element->append_child($reviewer_question);
*/
        $question =& new YesNoFormQuestion(
            '1. Is the item STEM grammatically and structurally correct?',
            'stem_grammar_correct' 
        );
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '2. Do the DISTRACTORS match the stem grammatically and structurally?',
            'distractors_match_grammar' 
        );
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '3. Are the DISTRACTORS less correct, yet plausible?',
            'distractors_less_correct' 
        );
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '4. Are the DISTRACTORS similar in appearance and content, yet distinct?',
            'distractors_similar_yet_distinct' 
        );
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '5. Is the answer CORRECT in all circumstances?',
            'answer_always_correct' 
        );
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '6. Is the item free from UNNECESSARY complication?',
            'item_free_from_complication' 
        );
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '7. Is this item UNBIASED toward all groups and regions?',
            'item_unbiased' 
        );
        $container_element->append_child($question);

        $kap_question_name = 'item_measures_k_a_p';
        $question =& new FormQuestion(
            '8. Does this item measure knowledge, application, or problem solving abilities?',
            array(
                new FormField(
                    new RadioButton(
                        $kap_question_name,
                        'k'
                    ),
                    'Knowledge: Recall of facts, terms, & basic concepts<br>'
                ),
                new FormField(
                    new RadioButton(
                        $kap_question_name,
                        'a'
                    ),
                    'Application: Using knowledge to assess a <i>new</i> situation<br>'
                ),
                new FormField(
                    new RadioButton(
                        $kap_question_name,
                        'p'
                    ),
                    'Problem Solving: Evaluation, analysis, and judgment'
                )
            )
        );
        $container_element->append_child($question);

        $question =& new FormQuestion(
            '9. What is the likelihood that a MINIMALLY QUALIFIED candidate would get this correct?',
            array(
                new FormField(
                    new TextInput(
                        'percent_minimally_qualified',
                        '',
                        '3'
                    ),
					'% '
				),
				new FormField(
					new RadioButton(
						CUT_SCORE_CERT,
						EMTB
					),
					'EMT-B'
				),
				new FormField(
					new RadioButton(
						CUT_SCORE_CERT,
						PARAMEDIC
					),
					'Paramedic'
				)
            )
        );
        $container_element->append_child($question);

        $question =& new YesNoFormQuestion(
            '10. Does the test item\'s LEVEL OF DIFFICULTY correspond to the '.
                'level of difficulty as used on the job?',
            'level_of_difficulty_matches_job' 
        );
        $container_element->append_child($question);

        $memorize_question_name = 'important_to_memorize';
        $question =& new FormQuestion(
            '11. On the first day of the job, how important is it that the '.
                'candidate MEMORIZE the information/skill tested?',
            array(
                new FormField(
                    new RadioButton(
                        $memorize_question_name,
                        '0'
                    ),
                    'not necessary; can be looked up without impacting patient care <br>'
                ),
                new FormField(
                    new RadioButton(
                        $memorize_question_name,
                        '1'
                    ),
                    'important; patient care may be negatively impacted <br>'
                ),
                new FormField(
                    new RadioButton(
                        $memorize_question_name,
                        '2'
                    ),
                    'essential; patient care will be negatively impacted'
                )
            )
        );
        $container_element->append_child($question);

	$number_selector = new SelectMenu( 'number_in_group' );
        $MIN_GROUP_SIZE = 1;
        $MAX_GROUP_SIZE = 10;
        for( $i=$MIN_GROUP_SIZE ; $i <= $MAX_GROUP_SIZE ; $i++ ) {
            $option =& new HTMLTag('option',array('value'=>"$i"));
            $option_text =& new HTMLText("$i");
            $option->append_child($option_text);
            $number_selector->append_child($option);
        }//for

        $number_selector_field = new FormField( $number_selector, '' );

        $number_in_group_question = new FormQuestion(
            'Number of people in the group',
            array(
                $number_selector_field
            )
        );
        $container_element->append_child($number_in_group_question);

        $reviewer_selector = new SelectMenu( 'reviewer' );

        $reviewers = array(
            'Mighty Mouse','Underdog','Hong Kong Phooey','Wonder Woman','Supergirl',
            'She-Ra','Storm','Rogue','Catwoman','The Bionic Woman',
            'Peter Parker','Batman','Spiderman','Superman','James Bond'
        );

        foreach( $reviewers as $reviewer ) {
            $option =& new HTMLTag('option',array('value'=>"$reviewer"));
            $option_text =& new HTMLText("$reviewer");
            $option->append_child($option_text);
            $reviewer_selector->append_child($option);
        }//foreach
        $reviewer_selector_field = new FormField( $reviewer_selector, '' );
        $reviewer_question = new FormQuestion(
            'Reviewer:',
            array(
                $reviewer_selector_field
            )
        );
        $container_element->append_child($reviewer_question);



        $submit_button_is_singleton = true;
        $submit_button = new HTMLTag(
            'input',
            array(
                'id'=>'submit_button',
                'type'=>'submit',
                'value'=>'Submit'
            ),
            $submit_button_is_singleton
        );
        $container_element->append_child($submit_button);
    }//ConsensusTestItemReviewForm


    /**
     *
     */
    function populate_with_defaults() {
        // since this is a consensus form, default the yes-no values to yes to 
        // speed up the process a little
        $yes_no_question = new YesNoFormQuestion('','');
        $YES_VALUE = $yes_no_question->YES_VALUE;
        $default_values = array(
            'level_of_difficulty_matches_job'=>$YES_VALUE, 
            'item_unbiased'=>$YES_VALUE, 
            'item_free_from_complication'=>$YES_VALUE, 
            'answer_always_correct'=>$YES_VALUE, 
            'stem_grammar_correct'=>$YES_VALUE, 
            'distractors_match_grammar'=>$YES_VALUE, 
            'distractors_less_correct'=>$YES_VALUE, 
            'distractors_similar_yet_distinct'=>$YES_VALUE
        );
        $this->populate($default_values);
    }//populate_with_defaults
}//class ConsensusTestItemReviewForm

?>
