<?php

/**
 * This class encapsulates shared behaviors between the input elements whose
 * values are set by setting the 'checked' attribute to 'checked.'  Currently,
 * the only such elements are checkboxes and radio buttons.
 */
class CheckedAttrInput extends FormInput {

   //////////////////////////////////////////////////////////// 
   // Public instance methods 
   //////////////////////////////////////////////////////////// 

    /**
     * Check this element if its value matches the value, if any, stored in
     * the given array of inputs for this element's name.
     *
     * @param array $inputs hash of name/value pairs (e.g., form postdata)
     */
    function populate( $inputs ) {
        if ( isset($inputs[$this->get_attribute('name')]) && 
             $inputs[$this->get_attribute('name')] == $this->get_attribute('value') ) {
            $this->set_attribute('checked','checked');
        }//if
    }//populate
}//class CheckedAttrInput

?>
