<?php 

/**
 * This class models a radio button group for answering yes/no
 * questions.  The values returned are 0 for no and 1  for yes.
 * This is a convenience specialization of the FormInput class.
 */
class YesNoRadioGroup extends HTMLTag {

    ////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////

    /**
     * Create a yes/no radio button group using inputs with the given name
     *
     * @param string $name the name to use for the radio group input tags
     */
    function YesNoRadioGroup( $name ) {
        $this->FormInput(
            'radio',
            array(
                array(
                    'name'=> $name,
                    'value'=> '1', 
                    'text'=> 'Yes'
                ),
                array(
                    'name'=> $name,
                    'value'=> '0', 
                    'text'=> 'No'
                )
            )
        );
    }//YesNoRadioGroup
}//class YesNoRadioGroup

?>
