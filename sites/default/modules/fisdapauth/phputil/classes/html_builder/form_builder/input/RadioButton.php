<?php

/**
 * This class models an individual radio button.
 */
class RadioButton extends CheckedAttrInput {

    //////////////////////////////////////////////////////////// 
    // Constructors
    //////////////////////////////////////////////////////////// 

    /**
     * Create a radio button with the given name and value.
     *
     * @param string $name  the button's name 
     * @param string $value the button's value 
     */
    function RadioButton( $name, $value ) {
        $is_singleton = true;
        $this->HTMLTag(
            'input',
            array(
                'type'=>'radio',
                'name'=>$name,
                'value'=>$value
            ),
            $is_singleton
        );
    }//RadioButton

}//class RadioButton

?>
