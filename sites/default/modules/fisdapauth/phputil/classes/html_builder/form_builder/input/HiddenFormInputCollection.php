<?php

/**
 * This class is designed to contain collections of related
 * hidden form inputs that can be validated in the same way.
 */
class HiddenFormInputCollection extends FormInputCollection {
}//class HiddenFormInputCollection

?>
