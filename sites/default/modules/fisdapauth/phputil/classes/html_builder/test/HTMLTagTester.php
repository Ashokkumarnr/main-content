<?php

// the HTMLTag class 
require_once('phputil/classes/html_builder/HTMLTag.php');
require_once('phputil/classes/html_builder/HTMLText.php');
require_once('phputil/classes/html_builder/css/IDSelector.php');
require_once('phputil/classes/html_builder/css/ClassSelector.php');



/**
 * This class contains unit tests for the HTMLTag class
 */
class HTMLTagTester extends UnitTestCase {
    function setUp() {
        // we'll use this to pretend to be an HTMLTag subclass with a
        // reimplementation of the populate method (as in the FormInput
        // classes).
        Mock::generate('HTMLTag');

        // Create a couple instances to play with
        $this->div_tag = new HTMLTag('div',array());
        $this->input_tag = new HTMLTag(
            'input',
            array(
                'type'=>'hidden',
                'name'=>'test_input'
            ),
            true
        );

        // an array of all the inputs (including selects and textareas) 
        // in the test html structure
        $this->inputs = array();
        $this->inputs[] =& new HTMLTag(
            'input',
            array('id'=>'test_1','name'=>'test_1','class'=>'class_1')
        );
        $this->inputs[] =& new HTMLTag(
            'textarea',
            array('id'=>'test_2','name'=>'test_2','class'=>'class_1')
        );
        $this->inputs[] =& new HTMLTag(
            'select',
            array('id'=>'test_7','name'=>'test_7','class'=>'class_5')
        );
        $this->inputs[] =& new HTMLTag(
            'input',
            array('id'=>'test_8','name'=>'test_8','class'=>'class_5')
        );

        // create a test div with some children
        $this->parent_div =& new HTMLTag('div',array());
        $this->parent_div->append_child($this->inputs[0]);
        $this->parent_div->append_child($this->inputs[1]);
        $this->parent_div->append_child(new HTMLText('This is test text'));
        $this->parent_div->append_child(
            new HTMLTag(
                'span',
                array('id'=>'test_3','name'=>'test_3','class'=>'class_2')
            )
        );
        $this->parent_div->append_child(
            new HTMLTag(
                'p',
                array('id'=>'test_4','name'=>'test_4','class'=>'class_1')
            )
        );
        $this->parent_div->append_child(new HTMLText('This is test text'));
        $this->parent_div->append_child(
            new HTMLTag(
                'div',
                array('id'=>'test_5','name'=>'test_5','class'=>'class_3')
            )
        );
        $this->parent_div->append_child(
            new HTMLTag(
                'div',
                array('id'=>'test_6','name'=>'test_6','class'=>'class_5')
            )
        );
        $this->parent_div->append_child($this->inputs[2]);
        $this->parent_div->append_child($this->inputs[3]);
    }//setUp

   
    function tearDown() {
    }//tearDown
  

    function test_constructor() {
    }//test_constructor


    /**
     * Tests the get_tag_name function
     */
    function test_get_tag_name() {
        $this->assertEqual($this->div_tag->get_tag_name(),'div');
        $this->assertEqual($this->input_tag->get_tag_name(),'input');
    }//test_get_tag_name


    /**
     * Tests the append_child and get_child_at functions
     */
    function test_child_management() {
        // append some strings
        $children = array('test1','test2','test3','test4');
        foreach( $children as $child_text ) {
            $this->div_tag->append_child($child_text);
        }//foreach

        // make sure there's the right number of children
        $children = $this->div_tag->get_children();
        $num_children = count($children);
        $this->assertEqual($num_children,count($children));

        // make sure the children match what we put in
        for( $i=0 ; $i < $num_children ; $i++ ) {
            $child = $this->div_tag->get_child_at($i);
            $this->assertEqual($child,$children[$i]);
        }//for
    }//test_child_management


    /**
     *
     */
    function test_attribute_management() {
        $test_attributes = array(
            'checked'=>'checked',
            'style'=>'border: 1px solid gray; width:30em;',
            'test_attribute'=>'this is a test value'
        );
        foreach( $test_attributes as $key=>$value ) {
            $this->div_tag->set_attribute($key,$value);
            $this->assertEqual($value,$this->div_tag->get_attribute($key));
        }//foreach
    }//test_attribute_management


    /**
     *  Make sure the populate method recurses as expected
     */
    function test_populate() {
        // test adding a child to the single div
        $test_input = array('one'=>1,'two'=>2);
        $fake_input =& new MockHTMLTag($this);
        $fake_input->expectOnce('populate',array($test_input));
        $this->div_tag->append_child($fake_input);
        $this->div_tag->populate($test_input);
        $fake_input->tally();

        // test adding a grandchild to a tag with several children
        $fake_grandchildren = array();
        $children = $this->parent_div->get_children();
        $num_children = count($children);
        for( $i=0 ; $i < $num_children ; $i++ ) {
            $child =& $children[$i];
            if ( method_exists($child,'populate') && method_exists($child,'append_child') ) {
                $fake_grandchild =& new MockHTMLTag($this);
                $fake_grandchild->expectOnce('populate',array($test_input));
                $child->append_child($fake_grandchild);
                $fake_grandchildren[] =& $fake_grandchild;
            }//if
        }//for
        $this->parent_div->populate($test_input);

        // check the expected counts of populate() calls on 
        // the grandchildren
        $num_grandchildren = count($fake_grandchildren);
        for( $i=0 ; $i < $num_grandchildren ; $i++ ) {
            $fake_grandchild =& $fake_grandchildren[$i];
            $fake_grandchild->tally();
        }//for
    }//test_populate


    /**
     *
     */
    function test_contains_named_tag() {
        // add a grandparent to test 
        $grandparent =& new HTMLTag('p',array('name'=>'test_paragraph'));
        $grandparent->append_child($this->parent_div);

        // make sure the function returns true on a tag with the given name
        $this->assertTrue($grandparent->contains_named_tag($grandparent->get_attribute('name')));
    
        // make sure the parent div notices all its children, and that the 
        // grandparent can find 'em, too
        $children = $this->parent_div->get_children();
        $num_children = count($children);
        for( $i=0 ; $i < $num_children ; $i++ ) {
            $child =& $children[$i];
            if ( method_exists( $child, 'get_attribute' ) ) {
                $this->assertTrue($this->parent_div->contains_named_tag($child->get_attribute('name')));
                $this->assertTrue($grandparent->contains_named_tag($child->get_attribute('name')));
            }//if
        }//for
    }//test_contains_named_tag


    /**
     *
     */
    function test_get_elements_by_selector() {
        //
        // test the '*' selector, which should return all children (and grandchildren, etc)
        //


        //
        // test the '<tag_name>' selector
        //




        //
        // test the '<tag_name>#<id>' selector
        //


        //
        // test the '#<id>' selector
        //

        // look for the children of the parent div by their IDs
        $child_ids = array('test_1','test_2','test_3','test_4','test_5');
        foreach( $child_ids as $child_id ) {
            // select the child with the current id
            $child_array = $this->parent_div->get_elements_by_selector(new IDSelector($child_id));

            // make sure the result has exactly one entry
            $this->assertTrue(
                is_array($child_array) && count($child_array)==1,
                'wrong result array for the selector "#'.$child_id.'"'
            );

            if ( is_array($child_array) && count($child_array)==1 ) { 
                // get the child and test its id attribute against the selected value
                $child =& $child_array[0];
                $this->assertEqual(
                    $child->get_attribute('id'),
                    $child_id,
                    'wrong child for selector "#'.$child_id.'"'
                );
            } else {
                var_export($child_array);
                echo "<br>\n";
                echo "<br>\n";
            }//else
        }//foreach


        //
        // test class selector
        //
        // @todo finish this test
        //

        // should be 3 of these
        $class_1_children = $this->parent_div->get_elements_by_selector(new ClassSelector('class_1'));
        $num_class_1_children = count($class_1_children);
        $this->assertEqual($num_class_1_children,3,'class selector returned wrong number of children');
        $expected_class_1_children = array('test_1','test_2','test_4');
        for( $i=0 ; $i < $num_class_1_children ; $i++ ) {
            $this->assertEqual(
                $class_1_children[$i]->get_attribute('id'),
                $expected_class_1_children[$i],
                'class selector returned wrong child'
            );
        }//for

        // should be 1 of these
        $class_2_children = $this->parent_div->get_elements_by_selector(new ClassSelector('class_2'));
        $num_class_2_children = count($class_2_children);
        $this->assertEqual($num_class_2_children,1,'class selector returned wrong number of children');
        $this->assertEqual($class_2_children[0]->get_attribute('id'),'test_3','class selector returned wrong child');

        // should be 1 of these
        $class_3_children = $this->parent_div->get_elements_by_selector(new ClassSelector('class_3'));
        $num_class_3_children = count($class_3_children);
        $this->assertEqual($num_class_3_children,1,'class selector returned wrong number of children');
        $this->assertEqual($class_3_children[0]->get_attribute('id'),'test_5','class selector returned wrong child');

        // should be none of these
        $class_4_children = $this->parent_div->get_elements_by_selector(new ClassSelector('class_4'));
        $num_class_4_children = count($class_4_children);
        $this->assertEqual($num_class_4_children,0,'class selector returned wrong number of children');


        //
        // test descendant selector (e.g., 'p span')
        //

    }//test_get_elements_by_selector


    function test_delete_children_by_selector() {
        // test deleting the class_1 elements
        $class_selector = new ClassSelector('class_1');
        $this->parent_div->delete_children_by_selector($class_selector);
        $this->assertEqual(
            count(
                $this->parent_div->get_elements_by_selector($class_selector)
            ),
            0,
            __LINE__." -- One or more elements were not deleted"
        );

        // test deleting the element with id test_7 (a select menu)
        $id_selector = new IDSelector('test_7');
        $this->parent_div->delete_children_by_selector($id_selector);
        $this->assertEqual(
            count(
                $this->parent_div->get_elements_by_selector($id_selector)
            ),
            0,
            __LINE__." -- One or more elements were not deleted"
        );
    }//test_delete_children_by_selector


    /**
     * Here, we test getting elements by selector in conjunction with deletion
     */
    function test_deleting_and_getting_elements() {
        // delete test_2, then try to get test_8
        $this->parent_div->delete_children_by_selector(
            new IDSelector(
                'test_2'
            )
        );

        // get an element below the deletion
        $test_8_results = $this->parent_div->get_elements_by_selector(
            new IDSelector(
                'test_8'
            )
        );
        $this->assertTrue(
            is_array($test_8_results)&&count($test_8_results)==1,
            __LINE__.': couldn\'t get element lower in the tree after deleting one higher up'
        );
         
        // get an element above the deletion
        $test_1_results = $this->parent_div->get_elements_by_selector(
            new IDSelector(
                'test_1'
            )
        );
        $this->assertTrue(
            is_array($test_1_results)&&count($test_1_results)==1,
            __LINE__.': couldn\'t get element higher in the tree after deleting one lower down'
        );

        // try to delete another element 
        $this->parent_div->delete_children_by_selector(
            new IDSelector(
                'test_7'
            )
        );

        // get an element below the deletion
        $test_8_results = $this->parent_div->get_elements_by_selector(
            new IDSelector(
                'test_8'
            )
        );
        $this->assertTrue(
            is_array($test_8_results)&&count($test_8_results)==1,
            __LINE__.': couldn\'t get element lower in the tree after deleting one higher up'
        );
         
        // get an element above the deletion
        $test_1_results = $this->parent_div->get_elements_by_selector(
            new IDSelector(
                'test_1'
            )
        );
        $this->assertTrue(
            is_array($test_1_results)&&count($test_1_results)==1,
            __LINE__.': couldn\'t get element higher in the tree after deleting one lower down'
        );
    }//test_deleting_and_getting_elements


    function test_disable_all_inputs() {
        // try disabling the inputs and checking the "disabled" attributes 
        // of inputs we created earlier
        $this->parent_div->disable_all_inputs();        
        foreach( $this->inputs as $input ) {
            $this->assertEqual(
                $input->get_attribute('disabled'),
                'disabled',
                'disabled attribute not set on a child input'
            );
        }//foreach
    }//test_disable_all_inputs
}//class HTMLTagTester

?>
