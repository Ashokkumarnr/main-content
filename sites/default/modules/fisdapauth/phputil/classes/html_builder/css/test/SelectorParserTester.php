<?php

// defines FISDAP_ROOT constant
require_once('../../../../../dbconnect.html');

// the HTMLTag class 
require_once(FISDAP_ROOT.'phputil/classes/html_builder/css/SelectorParser.php');

// The SimpleTest PHP unit testing framework
require_once(FISDAP_ROOT.'phputil/simpletest-1.0.0/unit_tester.php');
require_once(FISDAP_ROOT.'phputil/simpletest-1.0.0/reporter.php');
require_once(FISDAP_ROOT.'phputil/simpletest-1.0.0/mock_objects.php');


/**
 * This class contains unit tests for the HTMLTag class
 */
class SelectorParserTester extends UnitTestCase {
    function setUp() {
    }//setUp

   
    function tearDown() {
    }//tearDown
  

}//class SelectorParserTester  


// Run the test case
$tester = &new SelectorParserTester();
$tester->run( new HtmlReporter() );
?>
