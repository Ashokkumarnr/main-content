<?php

class ClassSelector {
    var $class;

    function ClassSelector($class) {
        $this->class = $class;
    }//ClassSelector

    
    /**
     * Return true iff the given tag matches this selector
     *
     * @param  HTMLTag $tag
     * @return bool
     */
    function matches( $tag ) {
        $classes = explode(' ',$tag->get_attribute('class'));
        if ( $classes ) {
            foreach( $classes as $class ) {
                if ( $class == $this->class ) {
                    return true;
                }//if
            }//foreach
        }//if

        return false;
    }//matches
}//class ClassSelector

?>
