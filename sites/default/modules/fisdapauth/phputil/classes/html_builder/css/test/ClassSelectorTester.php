<?php

require_once('phputil/classes/html_builder/HTMLTag.php');
require_once('phputil/classes/html_builder/css/ClassSelector.php');

/**
 * This class contains unit tests for the ClassSelector class
 */
class ClassSelectorTester extends UnitTestCase {
    function setUp() {
        $this->tag_1 =& new HTMLTag(
            'div',
            array(
                'id'=>'tag_1',
                'class'=>'class_1',
                'dummy_attr'=>'dummy_attr'
            )
        );

        $this->tag_2 =& new HTMLTag(
            'div',
            array(
                'id'=>'tag_2',
                'class'=>'class_2',
            )
        );

        $this->tag_3 =& new HTMLTag(
            'div',
            array(
                'id'=>'tag_3',
                'dummy'=>'dummy'
            )
        );

        $this->tag_4 =& new HTMLTag(
            'div',
            array(
                'class'=>'class_1 class_2 class_3',
            )
        );
    }//setUp

   
    function tearDown() {
    }//tearDown


    function test_matches() {
        // test tags with classes
        $tag_1_class_selector =& new ClassSelector('class_1');
        $tag_2_class_selector =& new ClassSelector('class_2');
        $tag_3_class_selector =& new ClassSelector('class_3');
        $tag_4_class_selector =& new ClassSelector('class_4');
        $this->assertTrue($tag_1_class_selector->matches($this->tag_1),'selector failed to match a known class');
        $this->assertFalse($tag_2_class_selector->matches($this->tag_1),'selector matched a different class');

        $this->assertTrue($tag_2_class_selector->matches($this->tag_2),'selector failed to match a known class');
        $this->assertFalse($tag_1_class_selector->matches($this->tag_2),'selector matched a different class');

        // test tags without classes
        $this->assertFalse($tag_1_class_selector->matches($this->tag_3),'selector matched a tag that has no class attribute');
        $this->assertFalse($tag_2_class_selector->matches($this->tag_3),'selector matched a tag that has no class attribute');

        // test tags with multiple classes
        $this->assertTrue($tag_1_class_selector->matches($this->tag_4),'selector failed to match a known class (multi-class tag)');
        $this->assertTrue($tag_2_class_selector->matches($this->tag_4),'selector failed to match a known class (multi-class tag)');
        $this->assertTrue($tag_3_class_selector->matches($this->tag_4),'selector failed to match a known class (multi-class tag)');
        $this->assertFalse($tag_4_class_selector->matches($this->tag_4),'selector matched incorrect class (multi-class tag)');
    }//test_matches
}//class ClassSelectorTester  

?>
