<?php

class TagSelector {
    var $tag;

    function TagSelector($tag) {
        $this->tag = $tag;
    }//TagSelector

    
    /**
     * Return true iff the given tag matches this selector
     *
     * @param  HTMLTag $tag
     * @return bool
     */
    function matches( $tag ) {
        return ($this->tag == '*') ? true : ($tag->get_tag_name() == $this->tag);
    }//matches
}//class TagSelector

?>
