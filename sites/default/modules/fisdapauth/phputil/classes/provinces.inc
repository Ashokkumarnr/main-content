<?php
/**
 * A factory for obtaining the provinces.
 */
class Provinces {
    /**
     * Retrieve the province information.
     * @return array Each element is an array(province name, 2 letter 
     * abbreviation).
     */
    public static function get_province_info() {
        return self::get_data();
    }

    /**
     * Retrieve the province data.
     * @return array Each element is an array(province name, 2 letter 
     * abbreviation).
     */
    private static function get_data() {
        $data = array(
            array('Alberta','AB'),
            array('British Columbia','BC'),
            array('Manitoba','MB'),
            array('New Brunswick','NB'),
            array('Newfoundland and Labrador','NF'),
            array('Northwest Territories','NT'),
            array('Nova Scotia','NS'),
            array('Nunavut','NU'),
            array('Ontario','ON'),
            array('Prince Edward Island','PE'),
            array('Quebec','PQ'),
            array('Saskatchewan','SK'),
            array('Yukon','YT')
        ); 

        return $data;
    }
}
?>
