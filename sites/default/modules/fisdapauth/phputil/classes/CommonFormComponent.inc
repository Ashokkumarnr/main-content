<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/

/**
 * @todo documentation blurb
 * @author Ian Young
 * @package CommonInclude
 */

/**
 * A component in CommonForm
 *
 * Anything that lives as a modular feature
 * of CommonForm and responds to information and directives from the form.
 *
 * These are added to a form with the {@link CommonForm::add() add()} method.
 * @todo review all docs for this class
 */
interface CommonFormComponent {
	/**
	 * Set the form's state
	 *
	 * Update this component with the state of the form that owns it.
	 * @param array
	 * @see CommonForm::update_state()
	 */
	public function set_form_state($state);
	/**
	 * Display the component
	 *
	 * @return string
	 */
	public function display();
	/**
	 * Get the width of this component
	 *
	 * @return int the width, in columns
	 */
	public function get_column_width();
	/**
	 * Respond to a submit handler
	 *
	 * When the form has been submitted, we call this to instruct this component
	 * to do any processing it needs to do and store things in session space.
	 */
	public function respond_formsubmithandler();
	/**
	 * Get the name of this component
	 * @return string
	 */
	public function get_name();
	/**
	 * Inform this component that the input was bad.
	 *
	 * Generally setting this to true means that this component will display in red.
	 *
	 * @param boolean
	 */
	public function set_badinput($badinput);
	/**
	 * Get any listener relationships for this component.
	 *
	 * When a prompt changes, any prompts listed as listening to it 
	 * will have {@link respond_ajax()} called. If a listening prompt changes 
	 * during this process, the listeners for this prompt will also be called.
	 *
	 * All listeners will also be triggered when a form is created initially.
	 *
	 * In either case, a prompt's respond_ajax() method may be called multiple 
	 * times (once for each listened-to prompt that changed), but it will only 
	 * be called when all listening dependencies are satisfied, so it is safe to 
	 * compute the result once and cache it for subsequent calls.
	 *
	 * @return array an array mapping prompt names to arrays of components listening to them
	 */
	public function get_listeners();
	/**
	 * Respond to a prompt change that we were listening for.
	 *
	 * If a given component will never itself listen to another component, it's 
	 * acceptable to implement this as a stub.
	 *
	 * @param CommonPrompt $prompt the prompt that changed
	 */
	public function respond_ajax($prompt);

}

/**
 * Defines a prompt section
 */
class CommonSection implements CommonFormComponent {

	/**
	 * The section text
	 */
	protected $text;
	/**
	 * The section name
	 */
	protected $name;
	/**
	 * Array of prompts we're listening to
	 */
	protected $listento;
	/**
	 * The width of this section measured in columns
	 */
	protected $column_width;

	/**
	 * Create a new instance of the common_section class
	 * @param string $text The text to be displayed as the section title.
	 */

	public function __construct($text) {

		$this->set_text($text); // Set the section text
		$this->set_name('mysection' . common_utilities::get_unique_suffix()); // Set the section name
		$this->listento = array(); // Set the listento array to null
		$this->set_column_width(1); // Set the width of this section measured in columns

	}

	public function get_name() {
		return $this->name;
	}

	public function get_column_width() {
		return $this->column_width;
	}

	/**
	 * Set bad input
	 *
	 * For now this method is a stub in common_section.  At some point we
	 * might consider allowing sections to indicate bad input.
	 */
	public function set_badinput($bool) {
	}

	/**
	 * Set the section text
	 */
	public function set_text($text) {

		$this->text = $text;

	}

	/**
	 * Set the section name
	 */
	public function set_name($name) {

		if ($name == null) $name = $this->text . "_section";

		$this->name = $name;

	}

	/**
	 * Set the width of this section measured in columns
	 * @param int $column_width The width of the section heading measured
	 * in columns. Either 1, 2, or 3. Default: 1
	 */
	public function set_column_width($column_width) {

		if (!in_array($column_width,array(1,2,3))) {
			common_utilities::displayFunctionError('Invalid column width');
		}

		$this->column_width = $column_width;

	}

	public function get_listeners() {
		return array();
	}

	/**
	 * Display the section
	 */
	public function display() {

		$output = null; // The HTML of the prompt section

		$text = $this->text;

		$output .= '<div class="common_section"><span class="largetext">' . $text . "</span></div>" . "\n";

		return $output;

	}

	/**
	 * Validate the section
	 */
	public function is_valid() {

		return true; // Section headings validate, always

	}

	public function respond_ajax($prompt) {
		// For now we do nothing
	}

	public function set_form_state($state) {
		// We don't care at all about state, we're a section heading!
	}

	public function respond_formsubmithandler() {
		// Nothing to do - we're just a section heading!
	}

}

