<?php
/**
 * A list of items. 
 * <pre>
 * Developers:  ONLY BASIC FUNCTIONALITY HAS BEEN TESTED HERE
 * </pre>
 */
abstract class AList {
    /**
     * Determine if an item is in the list.
     * @param mixed value The value to find.
     * @return boolean TRUE if the item is in the list.
     */
    public function contains($value) {
        return $this->index_of($value) >= 0;
    }

    /**
     * Determine the location of an item in the list.
     * @param mixed $value The value to find.
     * @return int The position in the list or -1 if not found.
     */
    abstract public function index_of($value);

    /**
     * Insert an item into the list.
     * @param mixed $value The value to insert.
     */
    abstract public function insert($value);

    /**
     * Retrieve the number of items in the list.
     * @return int The list size.
     */
    abstract public function size();
}

/**
 * A list that is sorted in ascending order.
 */
class SortedList extends AList {
    private $list;
    private $size;

    /**
     * Constructor.
     * @param array $list The underlying sorted list.
     * @param int $sort_flags The sort flags.
     */
    public function SortedList($list) {
        $this->list = $list;
        $this->size = count($this->list);
    }

    public function size() {
        return $this->size;
    }

    public function index_of($value) {
        return $this->binary_search($value);        
    }

    /**
     * Perform a binary search on a sorted list with scalar values.
     * The items in the list should be the same type as the search item.
     * @param mixed $value The item to search for.
     * @return integer The index in the list of the item, or less than 0 if not
     * found.
     */
    private function binary_search($value) {
        $high = $this->size - 1;
        $low = 0;
   
        while ($low <= $high) {
            // Integer division.
            $n = $high + $low;
            $i = ($n - ($n % 2)) / 2;

            if ($this->list[$i] < $value) {
                $low = $i + 1;
            }
            elseif ($this->list[$i] > $value) {
                $high = $i - 1;
            }
            else {
                return $i;
            }
        }

        return -1;
    }

    public function insert($value) {
        // Keep the list sorted.  We start from the back end.
        for ($i = 0; $i < $this->size; ++$i) {
            if ($value < $this->list[$i]) {
                array_splice($this->list, $i, 0, $value);
                ++$this->size;
                return;
            }
        }

        $this->list[$this->size] = $value;
        ++$this->size;
    }
}

/**
 * An unsorted list that is implemented as an associative array. 
 */
class UnsortedList extends AList {
    private $list;

    /**
     * Constructor.
     * @param array $list The undeflying list.
     */
    public function UnsortedList($list) {
        $this->list = $list;
    }

    public function index_of($value) {
        $n = $this->size();

        for ($i = 0; $i < $n; ++$i) {
            if ($this->list[$i] == $value) return $i;
        }

        return -1;
    }

    public function insert($value) {
        $this->list[] = $value;
    }

    public function size() {
        return count($this->list);
    }
}
?>
