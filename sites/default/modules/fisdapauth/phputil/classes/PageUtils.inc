<?php
/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2007.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/common_page.inc');

/**
 * Adds functionality to a common page.
 */
final class PageUtils {
    private $max_indent_levels = 0;

    const INDENT_CLASS_NAME = 'common_prompt_indent';

    /**
     * Constructor.
     * @param common_page $page The page to use.
     */
    public function __construct($page) {
        Assert::is_a($page, 'common_page');
        $this->page = $page;
    }

    /**
     * Adds indentation CSS rules.
     * @param int $each The number of units for CSS to indent each, i.e. 2.
     * @param string $units The units for CSS, i.e. 'em'.
     * @param int max_levels The max number of levels of indentation.
     */
    public function add_indentation_css($each, $units, $max_levels) {
        Assert::is_int($each);
        Assert::is_string($units);
        Assert::is_int($max_levels);

        // Compute the CSS indentation levels.
        $css = '';
        for ($i = 1; $i <= $max_levels; ++$i) {
            $css .= '.' . self::INDENT_CLASS_NAME . $i . ' { margin-left: ' .
                ($each * $i) . $units . " }\n";
        }

        $this->page->add_stylesheetcode($css);
        $this->max_indent_levels = $max_levels;
    }
    /**
     * Retrieve the CSS class name for an indentation level.
     * @param int $level The level of indentation, 1-n.
     * @return string|null The CSS class name or null.
     */
    public function get_indent_class($level) {
        Assert::is_int($level);
        $level = Convert.to_int($level);
        Assert::is_true($level <= self::max_indent_levels);

        if ($level < 1) return '';
        return self::INDENT_CLASS_NAME . $level;
    }
}
?>
