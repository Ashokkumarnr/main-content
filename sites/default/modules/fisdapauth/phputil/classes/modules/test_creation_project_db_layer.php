///////////////////////////////////////////////////////////////////////////
// Module: test_creation_project_db_layer.php
//
// This module provides the database layer functions for the test creation
// project model classes.  These are static methods, and must know the 
// name of the current class when they are invoked.  Without this module,
// the TestCreationProject subclasses would all generate 
// TestCreationProject instances instead of instances of the appropriate 
// subclasses.
///////////////////////////////////////////////////////////////////////////


/**
 * Fetch the project with the given ID
 */
function &find_by_id($id) {
    if ( !is_numeric($id) ) {
        return false;
    }//if

    $connection_obj =& FISDAPDatabaseConnection::get_instance();

    $query = 'SELECT * '.
             'FROM ProjectTable '.
             'WHERE Project_id="'.$id.'"';
    $result_set = $connection_obj->query($query);
    if ( !is_array($result_set) || count($result_set) != 1 ) {
        return false;
    }//if

    return --++-- Class Name --++--::from_assoc_array($result_set[0]);
}//find_by_id


function &from_assoc_array($db_row) {
    $project =& new --++-- Class Name --++--();
    foreach( $db_row as $field=>$value ) {
        $obj_field = $project->field_map['db'][$field];
        $project->$obj_field = $value;
    }//foreach
    return $project;
}//from_assoc_array

///////////////////////////////////////////////////////////////////////////
// END MODULE
///////////////////////////////////////////////////////////////////////////
