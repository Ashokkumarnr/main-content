<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/common_user.inc');
require_once('phputil/classes/FisdapLogger.inc');
require_once('phputil/classes/model/SiteProxiesHistoryModel.inc');
require_once('phputil/classes/model/SiteProxiesModel.inc');
require_once('phputil/classes/reqs/ReqUtils.inc');

/**
 * The business logic for a site proxy.
 * No security is performed here.  If you call the method you get to use it.
 */
final class SiteProxies {
    private $logger;
    private $proxy;
    private $user;

    const TYPE_NAME = 'Proxy';

	/**
	 * Constructor.
	 * @param SiteProxiesModel $proxy The underlying proxy.
	 * @param common_user $user The user.
	 */
	public function __construct(SiteProxiesModel $proxy, common_user $user) {
        $this->proxy = $proxy;
        $this->user = $user;
        $this->logger = FisdapLogger::get_logger();
    }

    private function get_log_prefix() {
        return 'SiteProxies[' . $this->proxy->get_id() . ']';
    }

	/**
	 * Create the proxy in the DB.
	 * @param string | null $history_text The text to use for the history entries.
	 * @throws FisdapDatabaseException if a problem occurs.
	 */
	public function create($history_text=null) {
        ReqUtils::verify_does_not_exist($this->proxy, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Created');

        $this->proxy->save();
        $this->add_history($history_text);

        $this->logger->debug($this->get_log_prefix() . ': created');
    }

	/**
	 * Delete the proxy from the DB.
	 * The entry is simply marked as deleted in the DB.
	 * @param string | null $history_text The text to use for the history entries.
	 * @throws FisdapDatabaseException if a problem occurs.
	 */
	public function delete($history_text=null) {
        ReqUtils::verify_exists($this->proxy, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Deleted');

        $this->logger->debug($this->get_log_prefix() . ': delete started');

        $this->proxy->set_deleted(true);
        $this->proxy->save();
        $this->add_history($history_text);

        $this->logger->debug($this->get_log_prefix() . ': delete complete');
    }

	/**
	 * Update the proxy in the DB.
	 * @param string | null $history_text The text to use for the history entries.
	 * @throws FisdapDatabaseException if a problem occurs.
	*/
    public function update($history_text=null) {
        ReqUtils::verify_exists($this->proxy, self::TYPE_NAME);
        $history_text = ReqUtils::get_description($history_text, 'Modified');

        $this->logger->debug($this->get_log_prefix() . ': update started');

        $this->proxy->save();
        $this->add_history($history_text);

        $this->logger->debug($this->get_log_prefix() . ': update complete');
    }

    private function add_history($description) {
        $description = trim($description);

        $history = new SiteProxiesHistoryModel();
        $history->set_description($description);
        $history->set_entry_time(FisdapDateTime::now());
        $history->set_site_proxies_id($this->proxy->get_id());
        $history->set_user_id($this->user->get_UserAuthData_idx());
        $history->save();
    }
}
?>
