<?php
/*----------------------------------------------------------------------*
 |                                                                      |
 |     Copyright (C) 1996-2006.  This is an unpublished work of         |
 |                      Headwaters Software, Inc.                       |
 |                         ALL RIGHTS RESERVED                          |
 |     This program is a trade secret of Headwaters Software, Inc.      |
 |     and it is not to be copied, distributed, reproduced, published,  |
 |     or adapted without prior authorization                           |
 |     of Headwaters Software, Inc.                                     |
 |                                                                      |
 *----------------------------------------------------------------------*/
require_once('phputil/classes/common_table.inc');
require_once('phputil/classes/common_miniform.inc');

/**
 * A customizable table
 *
 * Creates a common_table that can show or hide some columns depending on
 * what the user chooses.
 *
 * @todo we should figure out a good way to store per-user prefs for this table
 * even when the user logs out.
 * @package CommonInclude
 * @author Ian Young
 */
abstract class customizable_table extends common_table {

	/**
	 * column status
	 *
	 * Tracks the hidden and/or required status of columns. Maps column indices
	 * to a bitwise combination of {@link COL_HIDDEN} and {@link COL_REQUIRED} flags.
	 * @var array
	 */
	private $column_status;

	/**
	 * Is table customizable?
	 *
	 * If true, user will see controls to show or hide columns as desired.
	 * @var boolean
	 */
	private $customizable;

	// Constants used in flagging columns
	/**
	 * Flag a column as hidden
	 */
	const COL_HIDDEN = 1;
	/**
	 * Flag a column as required (i.e. not hideable)
	 */
	const COL_REQUIRED = 2;

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Is a column marked as hidden internally?
	 *
	 * Note that hidden and invisible are two different things. Hidden columns
	 * have been hidden by the user through the customization form.
	 * Invisible columns have been made so by the developer, and are only used
	 * in operations on the table.  An invisible column will almost definitely
	 * report that it is not hidden.
	 *
	 * @param string a column index
	 * @return boolean
	 */
	public function is_column_hidden($colidx) {
		if ($this->column_status[$colidx] & self::COL_HIDDEN) {
			return true;
		}
		return false;
	}

	/**
	 * Is a column required?
	 *
	 * @param string a column index
	 * @return boolean
	 */
	public function is_column_required($colidx) {
		if ($this->column_status[$colidx] & self::COL_REQUIRED) {
			return true;
		}
		return false;
	}

	/**
	 * Set customizable status
	 *
	 * @param boolean
	 */
	public function set_customizable($boolean) {
		$this->customizable = $boolean;
	}

	/**
	 * Is this table customizable?
	 *
	 * @return boolean
	 */
	public function is_customizable() {
		return $this->customizable;
	}

	/**
	 * Set a column's hidden status
	 *
	 * @param string $colidx a column index
	 * @param boolean $hidden true if column should be hidden, false if column
	 * should be shown
	 */
	public function set_column_hidden($colidx, $hidden=true) {
		// Sanity check!
		if ($hidden && $this->column_status[$colidx] & self::COL_REQUIRED) {
			common_utilities::displayFunctionError("You can't hide a required column! Bad bad bad!");
		} else if ($hidden) {
			// Toggle the hidden flag on
			$this->column_status[$colidx] |= self::COL_HIDDEN;
		} else {
			// Toggle the hidden flag off
			$this->column_status[$colidx] &= ~self::COL_HIDDEN;
		}
	}

	/**
	 * Set a column's required status
	 *
	 * @param string $colidx a column index
	 * @param boolean $required true if column should be required
	 */
	public function set_column_required($colidx, $required=true) {
		if ($required) {
			$this->column_status[$colidx] |= self::COL_REQUIRED;
		} else {
			$this->column_status[$colidx] &= ~self::COL_REQUIRED;
		}
	}

	/**
	 * @see common_table_plus::process()
	 */
	public function process() {
		if ($this->is_customizable()) {
			// Build and process the customization form
			$this->customize_form = $this->get_customize_form();
			$valid = $this->customize_form->process();

			$cols = common_utilities::get_scriptvalue('columns');
			if (!$cols) $cols = array();
			foreach ($this->columninfo as $colidx => $thiscolinfo) {
				if (!$this->is_column_required($colidx)) {
					$this->set_column_hidden($colidx, !in_array($colidx, $cols));
				}
			}

			// If we're responding to a change in customizations, populate
			if ($_POST['event_type'] == 'customize_submit') {
				$this->populate();
			}
		}

		return parent::process();
	}

	/**
	 * {@inheritdoc }
	 */
	protected function print_column_heading($thiscolumn, $widthphrase=null) {
		// Only print this column if it isn't hidden
		if ($this->is_column_hidden($thiscolumn['name']) != true) {
			return parent::print_column_heading($thiscolumn, $widthphrase);
		}
	}

	/**
	 * {@inheritdoc }
	 */
	protected function print_cell($thisrow, $thiscolumn, $width_phrase, $height_phrase) {
		if ($this->is_column_hidden($thiscolumn['name']) != true) {
			return parent::print_cell($thisrow, $thiscolumn, $width_phrase, $height_phrase);
		}
	}

	/**
	 * Get the id for the div encapsulating the customization options
	 *
	 * @return string the div id
	 */
	protected function get_customize_div_id() {
		return $this->get_prefix() . '_customize_opts';
	}

	public function display_html(&$common_dest) {
		if ($this->is_customizable()) {
			$str = '<div id="'.$this->get_customize_div_id().'" class="interactive_table_customize">';
			$str .= $this->get_customize_link('customize_show');
			$str .= '</div>';
			$common_dest->add($str);
		}
		parent::display_html($common_dest);
	}

	public function respondAJAX() {
		if ($_POST['event_type'] == 'customize_show') {
			$icon = $this->get_customize_link('customize_hide');
			$form_output = $this->customize_form->get_generated_html_form();
			echo_ajax('div', $this->get_customize_div_id(), $icon . $form_output);
			return true;
		} else if ($_POST['event_type'] == 'customize_hide' || $_POST['event_type'] == 'customize_submit') {
			$icon = $this->get_customize_link('customize_show');
			echo_ajax('div', $this->get_customize_div_id(), $icon);
			if ($_POST['event_type'] == 'customize_submit') {
				echo_ajax('div', $this->get_inner_div_id(), $this->print_table());
			}
			return true;
		} else {
			return parent::respondAJAX();
		}
	}

	/**
	 * Get customization link
	 *
	 * Provides the HTML to make a link that will show/hide the optional
	 * columns form on a click.
	 *
	 * @param string $dispatchid The dispatch id that a click on our link should generate.
	 * @return string The HTML that makes up our link
	 */
	private function get_customize_link($dispatchid) {
		$output = '<a class="common_table_customize"';
		$output .= " title=\"Choose which columns to show and hide\"";
		$output .= " onClick=\"linkHandler('interactivetable_event', null, { table_id: '" . $this->get_table_id() . "', event_type: '$dispatchid'});\">";
		$output .= '<span>Customize these columns</span>';
		$output .= '</a>';
		return $output;
	}

	/**
	 * Get the customization form
	 *
	 * Builds the customization form this table will use to allow users to
	 * show/hide certain columns.
	 *
	 * @return common_form
	 */
	private function get_customize_form() {
		$newform = new common_miniform();
		$newform->set_dispatch_id('interactivetable_event');
		$newform->set_mode('query');
		$newform->set_name($this->get_prefix().'_customize_form');
		$newform->set_submitvalue('Done');
		$newform->set_layout('submit_float', 'right');

		$event = new common_prompt('event_type', 'customize_submit', null, null, 'hidden', false);
		$newform->add($event, 1);
		$tableid = new common_prompt('table_id', $this->get_table_id(), null, null, 'hidden', false);
		$newform->add($tableid, 1);

		$col_prompt = new common_prompt('columns', null, 'Optional columns:', null, 'checkbox', false);
		foreach ($this->columninfo as $colidx => $thiscol) {
			// Only show columns that aren't required
			if (!$this->is_column_required($colidx)) {
				// Checked if the column is currently shown
				$col_prompt->set_promptset($colidx, $thiscol['text'], !$this->is_column_hidden($colidx), null, false);
			}
		}

		$newform->add($col_prompt, 1);

		return $newform;
	}

}
