<?php

/**
 * This class models a link between an eval and 
 * a FISDAP datum (IV, Shift, etc)
 */
class FISDAP_DataEvalLink extends DataEntryDbObj {    
    var $tables_to_classes;

    /** 
     * Create a new DataEvalLink with the specified id and the specified
     * db link
     */
    function FISDAP_DataEvalLink( $id, $connection ) {
        $this->DbObj(
            'DataEvalLinks',    //table name
            $id,                //id
            'DataEvalLink_id',  //id field
            'Shift',            //parent entity name
            array(),            //children
            $connection         //database connection
        );
        $field_map = array(
            'DataEvalLink_id' => 'DataEvalLink_id', 
            'EvalSession_id' => 'EvalSession_id',  
            'dataTableName' => 'dataTableName',   
            'Data_id' => 'Data_id',         
            'EvalHookDef_id' => 'EvalHookDef_id'
        ); 
        $this->set_field_map( $field_map );

        $this->tables_to_classes = array(
            'ShiftData'=>'Shift', 'RunData'=>'Run',
            'AssesmentData'=>'Assessment', 'IVData'=>'IV',
            'EKGData'=>'EKG', 'ALSAirwayData'=>'Airway',
            'MedData'=>'Med', 'OtherALSData'=>'OtherALS',
            'PtComplaintData'=>'Complaint', 'BLSSkillsData'=>'BLS_Skill' 
        );
    }//constructor FISDAP_DataEvalLink

    /**
     * Get the name of the class this object links an eval to, using the
     * dataTableName field to lookup the associated class name in our
     * tables_to_classes array.
     */
    function get_assoc_class_name() {
        return $this->tables_to_classes[$this->get_field('dataTableName')];
    }//get_assoc_class_name
}//class FISDAP_DataEvalLink
?>
