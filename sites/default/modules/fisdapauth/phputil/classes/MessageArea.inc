<?php
/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2007.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/browser_action.inc');
require_once('phputil/classes/common_page.inc');

/**
 * A message area is a place on a page that displays error and status messages.
 * When defining an area, you can specify the minimum number of lines to 
 * display.  This may result in a better user experience as the screen will not 
 * shift on the user.
 *
 * Typical usage:
 * $m = new MessageArea();
 * $m->set_error_msg_params('errors', 1);
 * $m->set_status_msg_params('status', 0);
 * ....
 * $m->update_msgs();
 */
final class MessageArea {
    private $error_msg_id;
    private $error_msgs = array();
    private $min_error_msgs;
    private $min_status_msgs;
    private $status_msgs = array();
    private $status_msg_id;

    /**
     * Set the error message parameters.
     * @param string|null $id The DIV ID or null for no display.
     * @param int $min The minimum number of lines to allocate.
     */
    public function set_error_msg_params($id, $min) {
        $this->error_msg_id = $id;
        $this->min_error_msgs = $min;
    }

    /**
     * Set the status message parameters.
     * @param string|null $id The DIV ID or null for no display.
     * @param int $min The minimum number of lines to allocate.
     */
    public function set_status_msg_params($id, $min) {
        $this->status_msg_id = $id;
        $this->min_status_msgs = $min;
    }

    /**
     * Append an error message.
     * @param string|null $msg The message.
     */
    public function add_error_msg($msg) {
        if (is_null($this->error_msg_id)) return;
        if (is_null($msg) || (trim($msg) == '')) return;
        $this->error_msgs[] = $msg;
    }

    /**
     * Append an internal error message.
     * @param string|null $msg The message.
     */
    public function add_internal_error_msg($msg) {
        if (is_null($this->error_msg_id)) return;
        if (is_null($msg) || (trim($msg) == '')) return;
        $this->error_msgs[] = "Internal error: $msg";
    }

    /**
     * Append a status error message.
     * @param string|null $msg The message.
     */
    public function add_status_msg($msg) {
        if (is_null($this->status_msg_id)) return;
        if (is_null($msg) || (trim($msg) == '')) return;
        $this->status_msgs[] = $msg;
    }

    /**
     * Clear all the messages.
     */
    public function clear_all() {
        $this->error_msgs = array();
        $this->status_msgs = array();
    }

    /**
     * Update the status area by displaying any messages during an AJAX callback.
     */
    public function update_msgs() {
        if (!is_null($this->error_msg_id)) {
            send_browser_action(new InnerHtmlBrowserAction(
                $this->error_msg_id, $this->get_error_msgs_html()));
        }

        if (!is_null($this->status_msg_id)) {
            send_browser_action(
                new InnerHtmlBrowserAction(
                    $this->status_msg_id, $this->get_status_msgs_html()));
        }
    }

    private function get_error_msgs_html() {
        if (is_null($this->error_msg_id)) return '';

        return self::build_msg_html($this->error_msgs, $this->min_error_msgs);
    }

    private function get_status_msgs_html() {
        if (is_null($this->status_msg_id)) return '';

        return self::build_msg_html($this->status_msgs, $this->min_status_msgs);
    }

    /**
     * Retrieve the HTML for the message area.
     * @return string The HTML.
     */
    public function get_html() {
        $html = '';

        if (!is_null($this->error_msg_id)) {
            $text = $this->get_error_msgs_html(); 
            $html .= <<<EOI
<p id="$this->error_msg_id" align="center" class="largetext" style="color: red">$text</p>
EOI;
        }

        if (!is_null($this->status_msg_id)) {
            $text = $this->get_status_msgs_html();
            $html .= <<<EOI
<p id="$this->status_msg_id" align="center" class="largetext" style="color: black">$text</p>
EOI;
        }

        return $html;
    }

    /**
     * Build up the HTML for a list of messages.
     * @param array $msgs The messages.
     * @param int $min The minimum number of messages to display (empty lines 
     * may be used.
     * @return string The messages as HTML.
     */
    private static function build_msg_html($msgs, $min) {
        // We try to display an area that won't like cause the page
        // to shift up and down when it changes.
        $list = $msgs;
        $n = count($msgs);
        for ($i = $n; $i < $min; ++$i) {
            $list[] = '<br>';
        }

        return join('<br>', $list);
    }
}
?>
