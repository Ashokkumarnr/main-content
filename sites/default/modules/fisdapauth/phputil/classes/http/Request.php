<?php


/**
 * This class models requests (ostensibly to a given controller) as a collection
 * of inputs (keys => values).
 */
class Request {
    /** the input (i.e., $_REQUEST) */
    var $input;
    

    /**
     * Create a Request from the given input array
     *
     * @param array $input our input to the controller (should contain the 
     *                     "action" field)
     */
    function Request() {
        if ( $_GET ) {
            $this->input = $_GET;
        } else {
            $this->input = $_POST;
        }//else
    }//Request
    
    
    /**
     * Returns the action for this request (e.g., 'display' or 'submit').
     *
     * @return string the action to execute on the controller
     */
    function get_action() {
        if ( isset($this->input['action']) ) {
            return $this->input['action'];
        } else {
            return false;
        }//else
    }//get_action
   
    
    /**
     * Returns the input, minus the "action" field (which should only be 
     * accessed via get_action above).
     *
     * @see Request::get_action
     * @return array the input array, minus the "action" field
     */
    function get_input() {
        $input = $this->input;
        unset($input['action']);
        return $input;
    }//get_input


    /**
     *
     */
    function get_unescaped_input() {
        if ( get_magic_quotes_gpc() ) {
            return Request::strip_slashes_from_input($this->get_input());
        } else {
            return $this->get_input();
        }//else
    }//get_unescaped_input


    /**
     *
     */
    function get_escaped_input() {
        if ( get_magic_quotes_gpc() ) {
            return $this->get_input();
        } else {
            return Request::add_slashes_to_input($this->get_input());
        }//else
    }//get_escaped_input


    /**
     *
     */
    function strip_slashes_from_input($input) {
        return array_map('stripslashes',$input);
    }//strip_slashes_from_input


    /**
     *
     */
    function add_slashes_to_input($input) {
        return array_map('addslashes',$input);
    }//add_slashes_to_input
}//class Request


?>
