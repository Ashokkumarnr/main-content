<?php


require_once('phputil/classes/http/Request.php');


/**
 * This class contains unit tests for the Request class
 */
class RequestTester extends UnitTestCase {
    
    function setUp() {
    }//setUp

   
    function tearDown() {
    }//tearDown

    
    function test_get_input_and_action() {
        $empty_input = array();
        $action_only_input = array('action'=>'test_action');
        $full_input_no_action = array(
            'input_1'=>'val_1',
            'input_2'=>'val_2'
        );
        $full_input_with_action = array(
            'action'=>'test_action',
            'input_1'=>'val_1',
            'input_2'=>'val_2'
        );
        
        // try some GET requests
        $_GET = $empty_input;
        $empty_request =& new Request();
        $this->assertEqual($empty_request->get_input(),array());
        $this->assertEqual($empty_request->get_action(),'');
        
        $_GET = $action_only_input;
        $action_only_request =& new Request();
        $this->assertEqual($action_only_request->get_input(),array());
        $this->assertEqual($action_only_request->get_action(),$action_only_input['action']);
        
        // try some POST requests
        $_GET = array();
        $_POST = $full_input_no_action;
        $full_input_no_action_request =& new Request();
        $this->assertEqual($full_input_no_action_request->get_input(),$full_input_no_action);
        $this->assertEqual($full_input_no_action_request->get_action(),'');
        
        $_POST = $full_input_with_action;
        $full_input_action_request =& new Request();
        $this->assertEqual($full_input_action_request->get_input(),$full_input_no_action);
        $this->assertEqual($full_input_action_request->get_action(),$full_input_with_action['action']);
    }//test_get_input_and_action
}//class RequestTester  

?>
