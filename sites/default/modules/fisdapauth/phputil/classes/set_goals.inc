<?php 
/**
 * A form to allow the user to pick which set of goals to add/edit
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author Erik Hanson
 * @author Brian Peterson
 * @copyright 1996-2007 Headwaters Software, Inc.
 *
 */
require_once('common_form.inc');
require_once('common_page.inc');
require_once('phputil/inst.html');
require_once('phputil/get_inst_perm.html');

// The default value of a prompt's 'size' option.
define('PROMPT_DEFAULT_SIZE', 35);

define('NEW_GOAL_NAME_FIELD_NAME', 'new_goal_name');
define('SINGLE_GOAL_FORM_NAME', 'goalform');

/**
 * Encapsulate utilities for setting goals.
 */
class SetGoalUtils {
	/**
	 * Determine if this the single goal form.
	 * @return TRUE if this is the single goal form.
	 */
	/* static */ function is_single_goal_form() {
		$form_id = SetGoalUtils::get_array_value($_REQUEST, 'formid', ''); 
		return ($form_id == SINGLE_GOAL_FORM_NAME);
	}

	/**
	 * Retrieve an array value.
	 * @param list The list to check.
	 * @param key The key to locate.
	 * @return The value if found, otherwise the default.
	 */
	function get_array_value(&$list, $key, $default=null) {
		$value = $default;
		if (isset($list[$key])) {
			$value = $list[$key];
		}

		return $value;
	}
}

/**
 * A factory to retrieve an action object.
 */
class ActionFactory {
	var $actions = array(
		'create' => 'CreateAction', 
		'delete' => 'DeleteAction', 
		'edit' => 'EditAction', 
		'select' => 'SelectionAction', 
		'view' => 'ViewAction'
	);

	var $connection;

	/**
	 * Create an action based on the way the page was invoked.
	 * @param connection The FISDAP connection.
	 * @param page The common page.
	 * @return The action.
	 */
	function create_action(&$connection, &$page) {
		$action = SetGoalUtils::get_array_value($_REQUEST, 'action', null);
		if (is_null($action) && SetGoalUtils::is_single_goal_form()) {
			$action = common_utilities::get_scriptvalue('action');
		}

		if (is_null($action)) {
			$action = '';
		}

		if (isset($this->actions[$action])) {
			$class = $this->actions[$action];
			$action_object = new $class($action, $connection, $page);
		} else {
			$action_object = new SelectionAction(
				'select', $connection, $page);
		}

		$action_object = &$this->check_permissions($action_object);
		return $action_object;
	}

	/**
	 * Determine if the user has permission for the action.
	 * @param action The requested action.
	 * @return The action to use.
	 */
	function check_permissions(&$action) {
		global $canAdminProgram;
		global $Instructor;

		// Must be an instructor.
		$connection = &$action->get_connection();
		if ($Instructor != 1) {
			$msg = 'You have to be an instructor to view this page.';
			return new ErrorAction('notinstructor', 
				$connection, $action->get_page(), $msg);
		}

		// Can the user administer the program?
		if ($action->needs_update_permission() && ($canAdminProgram != 1)) {
			$program_id = $action->get_program_id();

			$statement = 'SELECT I.FirstName, I.LastName' .
				' FROM InstructorData I, ProgramData P' .
				" WHERE P.Program_id={$program_id} AND" .
				' P.Program_id=I.ProgramId AND' .
				' P.ProgramContact = I.Instructor_id';
			$rows = $connection->query($statement);
			if (count($rows) == 1) {
				$name = '"' . $rows[0]['FirstName'] . ' ' . 
					$rows[0]['LastName']. '"';
			} else {
				$name = 'your instructor';
			}

			$msg = 'You do not have access to set or edit goals.' .
				"  Please contact {$name} for access.";
			return new ErrorAction('permission',
				$connection, $action->get_page(), $msg);
		}

		// Permitted.
		return $action;
	}
}

/* abstract */ class Action {
	var $connection;
	var $errors = array();
	var $name;
	var $page;

	/**
	 * Constructor.
	 * @param name The action name.
	 * @param connection The FISDAP connection.
	 * @param page The common page.
	 */
	function Action($name, &$connection, &$page) {
		$this->connection = &$connection;
		$this->name = $name;
		$this->page = &$page;
	}

	/**
	 * Retrieve the program ID.
	 * @return The program id.
	 */
	function get_program_id() {
		global $Program_id;
		return $Program_id;
	}

	/**
	 * Retrieve any error messages that have occurred.
	 * @return An array of 0 or more error messages.
	 */
	function get_errors() {
		return $this->errors;
	}

	/**
	 * Add an error message.
	 * @param msg The error message.
	 */
	function add_error($msg) {
		if (is_null($msg)) return;
		$this->errors[] = $msg;
	}

	/**
	 * Retreve the action's name.
	 * @return The action name.
	 */
	function get_name() {
		return $this->name;
	}

	/**
	 * Retrieve the common page.
	 * @return The common page.
	 */
	function &get_page() {
		return $this->page;
	}

	/**
	 * Retrieve the DB connection.
	 * @return The DB connection.
	 */
	function &get_connection() {
		return $this->connection;
	}

	/**
	 * Process the action.
	 */
	/* abstract */ function process() {
		die('Implement Action.process()');
	}

	/**
	 * Determine if the operation neeeds update permission.
	 * @return TRUE if the operation needs update permission.
	 */
	function needs_update_permission() {
		return false;
	}
}

/**
 * Defines an action taken on a single set of goals.
 */
/* abstract */ class GoalAction extends Action {
	/**
	 * Constructor.
	 * @param name The action name.
	 * @param connection The FISDAP connection.
	 * @param page The common page.
	 */
	function GoalAction($name, &$connection, &$page) {
		parent::Action($name, $connection, $page);
	}

	/**
	 * Determine whether a password is needed.
	 * @return TRUE if authorization is needed.
	 */
	/* abstract */ function needs_authorization() {
		// At the moment, we always indicate that no password
		// is needed.  The old code used to require one, but
		// common_form appears to disable all prmopts when
		// delete mode is on, thus the password can not be
		// entered.
		// return $this->authorize;
		return false;
	}

	/**
	 * Determine if the entire form (except the name) is read only.
	 * @return TRUE if the entire form is read only.
	 */
	/* abstract */ function is_read_only() {
		return true;
	}

	/**
	 * Determine if the name field must be unique.
	 * @return TRUE if the name field must be unique.
	 */
	function check_name_for_uniqueness() {
		return false;
	}

	/**
	 * Determine if the name of the goal is read only.
	 * @return TRUE if the goal name is read only.
	 */
	/* abstract */ function is_name_read_only() {
		return true;
	}

	/**
	 * Add buttons to a form.
	 * @param form The form to use.
	 */
	function add_buttons(&$form) {
		die('Implement GoalAction.add_buttons()');
	}

	/**
	 * Create the SQL for an insert or update set phrase.
	 * @return The SQL as a string, i.e. SET xxx=yyy, aaa=bbb, ...
	 */
	function get_set_phrase() {
		$kvs = array();

		$preferences = $this->get_default_preferences();
		foreach ($preferences as $name=>$preference) {
			if (!$preference->can_be_default()) continue;

			$value = 
				common_utilities::get_scriptvalue($name);
			if ($preference->is_boolean()) {
				$value = !strcasecmp('yes', $value) ? 1 : 0;
			}

			$kvs[] = "{$name}={$value}";
		}

		$sql = 'SET ' . join(', ', $kvs);
		return $sql;
	}

	function disable(&$common_obj, $disable) {
		if ($disable) {
			$common_obj->disable();
		}
	}

	/**
	 * Lock the table for write access.
	 * @return TRUE if the table was locked.
	 */
	/* protected */ function lock_table() {
		$locked = true;

		$statement = 'LOCK TABLES JRCPrefsData WRITE';
		$connection =& $this->get_connection();
		if (!$connection->lock($statement)) {
			write_session_flash('error', 'Locking tables failed.');
			$locked = false;
		}

		return $locked;
	}

	/**
	 * Unlock the table.
	 * @return TRUE if the table was unlocked.
	 */
	/* protected */ function unlock_tables() {
		$unlocked = true;

		$statement = 'UNLOCK TABLES';
		$connection =& $this->get_connection();
		if (!$connection->unlock($statement)) {
			write_session_flash('error', 'Unlocking tables failed.');
			$unlocked = false;
		}

		return $unlocked;
	}

	/**
	 * Create a checkboxtext prompt.
	 * @param name The preference name.
	 * @param preferences An associative array of preferences.
	 * @param read_only Indicates if the field is read only.
	 * @param text The text that goes along with the checkbox.
	 */
	function create_checkbox_prompt($name, &$preferences, $read_only, $text) {
		// The name MUST be a preference.
		if (!isset($preferences[$name])) {
			die("Checkbox preference[{$name}] does NOT exist.");
		}

		$preference = &$preferences[$name];

		// Due to the way a single checkbox is displayed,
		// where the required (*) is on a different line
		// than the checkbox, we will use a radio button.
		$value = $preference->get_boolean_value() ? 'Yes' : 'No';
		$checkbox = new common_prompt($name, $value,
			$text, null, 'yesno', !$read_only);
		// todo php5 $checkbox->set_option('cols', 2);

		$this->disable($checkbox, $read_only);
		return $checkbox;
	}

	/**
	 * Create a text prompt for a non-negative integer.
	 * @param name The preference name.
	 * @param preferences An associative array of preferences.
	 * @param default_preferences  An associative array of preferences.
	 * @param read_only Indicates if the field is read only.
	 * @param text The text that goes along with the checkbox or null to
	 *      compute it.
	 */
	function create_non_negative_text_prompt($name, &$preferences, &$default_preferences, $read_only, $text=null) {
		$prompt = $this->create_text_prompt($name, $preferences,
			$default_preferences, $read_only, $text);

		// Verify the number is valid.
		if (!$read_only) {
			$prompt->set_option('validate_integer', array(0,null));
		}

		return $prompt;
	}

	/**
	 * Create a text prompt.
	 * @param name The preference name.
	 * @param preferences An associative array of preferences.
	 * @param default_preferences  An associative array of preferences.
	 * @param read_only Indicates if the field is read only.
	 * @param text The text that goes along with the checkbox or null to
	 *      compute it.
	 */
	function create_text_prompt($name, &$preferences, &$default_preferences, $read_only, $text=null) {
		// The name MUST be a preference.
		if (!isset($preferences[$name])) {
			die("Text preference[{$name}] does NOT exist.");
		}

		$preference = &$preferences[$name];

		// The field name on the form is just a 
		// manipulation of the name.
		if (is_null($text)) {
			$text = str_replace('_', ' ', $name);
		}

		$prompt = new common_prompt(
			$name, $preference->get_value(), $text, null,
			'text', !$read_only);
		$this->disable($prompt, $read_only);

		// Set the prompt size if possible.
		if (isset($default_preferences[$name])) {
			$default = $default_preferences[$name];
			if ($default->has_size()) { 
				$size = $default->get_size();
				$prompt->set_option('size', min($size, PROMPT_DEFAULT_SIZE));
				$prompt->set_option('maxlength', $size);
			}
		}

		return $prompt;
	}

	/**
	 * Create the setDefaults() javascript method.
	 * @param default_preferences The defaults to use.
	 */
	function create_set_defaults(&$default_preferences) {
		$show_missing = false; // Set 'true' after a DB change to test
		$page = &$this->get_page();

		$code = array(
			'function setDefaults()',
			'{',
			'  changeIndicator("content");',
			'  f = document.forms["' . SINGLE_GOAL_FORM_NAME . '"];',
			'  if (!f) return;'
		);

		foreach ($default_preferences as $name=>$preference) {
			// We test for the existence of the value just in case
			// we have a logic error between this method and the form
			// creation method.
			if ($preference->can_be_default()) {
				$element = "f.{$name}";

				// Boolean?
				if ($preference->is_boolean()) {
					if ($preference->get_boolean_value()) {
						$element .= "_0";
					} else {
						$element .= "_1";
					}

					$code[] = "  if ({$element})" .
						" {$element}.checked = true;";
				} else {
					// Non booleans.
					$value = $preference->get_value(); 
					$code[] = "  if ({$element})" .
						" {$element}.value = '{$value}';";
				}

				if ($show_missing) {
					$code[] = '  else alert(\'Unable to set default' .
						" value for {$name}');";
				}
			}
		}

		$code[] = '  return false;';
		$code[] = '}';
		$page->add_javascriptcode(join("\n", $code));
	}

	/**
	 * Create the form that displays or edit a single goal.
	 * @return The form.
	 */
	function create_form() {
		$page = &$this->get_page();
		$page->enable_common_form();

		// Create the form.
		$form = new common_form();
		$form->set_column_layout(array(2));
		$form->set_display_function('display_goal_form');
		$form->set_name(SINGLE_GOAL_FORM_NAME);

		// We only need validation if the fields are changeable.
		if (!$this->is_read_only() || !$this->is_name_read_only()) {
			$form->set_validate_function('validate_goal_form');
		}

		$default_preferences = &$this->get_default_preferences();
		$preferences = &$this->get_preferences();

		// On editable pages, we offer the user a way to reset the
		// values to the defaults.
		if (!$this->is_read_only()) {
			$this->create_set_defaults($default_preferences);
		}

		if (!count($preferences)) {
			$preferences = $default_preferences;
		}

		// Define an empty section which is used as a separator.
		$empty_section = new common_section(null);
		$empty_section->set_column_width(2);

		// Name section.
		$read_only = $this->is_name_read_only();
		$prompt = $this->create_text_prompt('Type',
			$preferences, $default_preferences, $read_only);

		// ... even though we use the DB 'Type' field, this is the name.
		$prompt->set_name(NEW_GOAL_NAME_FIELD_NAME);
		$prompt->set_value($this->get_goal_name());

		if ($read_only) {
			$prompt->set_text('Name');
		} else {
			$prompt->set_text('Name (changing performs a rename)');
		}

		if (!$read_only) {
			$prompt->set_subtext('Letters, numbers, spaces, dashes (-), and' .
				' underscores (_) are permitted');
			$prompt->set_option('validate_regex', 
				'^[0-9a-zA-Z][- _0-9a-zA-Z]*$');
		}
		$prompt->set_column_width(2);
		$form->add($prompt, 1);

		// Skills section.
		$read_only = $this->is_read_only();
		$skills = new common_section('Skills');
		$skills->set_column_width(2);
		$form->add($skills, 1);

		// ... med admin
		$prompt_box = new common_promptbox(array(1,1));
		$prompt = $this->create_non_negative_text_prompt('Med_Admin',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... et intubation
		$prompt = $this->create_non_negative_text_prompt('ET_Intubation',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... iv success
		$prompt = $this->create_non_negative_text_prompt('IV_Success',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... live intubation
		$prompt = $this->create_non_negative_text_prompt('Live_Intubation',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... ventilation
		$prompt = $this->create_non_negative_text_prompt('Ventilation',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);
		$form->add($prompt_box, 1);

		// Ages section.
		$prompt_box = new common_promptbox(array(1,1));
		$ages = new common_section('Ages');
		$ages->set_column_width(2);
		$form->add($ages, 1);

		// ... team lead
		$text = 'Require team lead for age based assessment to' .
			' count in the field';
		$checkbox = $this->create_checkbox_prompt('RequireAge_TL',
			$preferences, $read_only, $text);
		$checkbox->set_column_width(2);
		$form->add($checkbox, 1);
		$form->add($empty_section, 1);

		// ... peds
		$prompt = $this->create_non_negative_text_prompt('Peds',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... new born
		$prompt = $this->create_non_negative_text_prompt('New_Born',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... adults
		$prompt = $this->create_non_negative_text_prompt('Adults',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... infant
		$prompt = $this->create_non_negative_text_prompt('Infant',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... geriatrics
		$prompt = $this->create_non_negative_text_prompt('Geriatrics',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... toddler
		$prompt = $this->create_non_negative_text_prompt('Toddler',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... preschooler
		$prompt = $this->create_non_negative_text_prompt('Preschooler',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... school age
		$prompt = $this->create_non_negative_text_prompt('School_Age',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... adolescents
		$prompt = $this->create_non_negative_text_prompt('Adolescents',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);
		$form->add($prompt_box, 1);

		// ... peds cutoff age
		$form->add($empty_section, 1);
		$prompt_box = new common_promptbox(array(1,1));
		$prompt = $this->create_non_negative_text_prompt('Peds_Cutoff_Age',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... adult cutoff age
		$prompt = $this->create_non_negative_text_prompt('Adult_Cutoff_Age',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... adolescent start age
		$prompt = $this->create_non_negative_text_prompt('Adolescent_Start_Age',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);
		$form->add($prompt_box, 1);

		// Pathology section.
		$prompt_box = new common_promptbox(array(1,1));
		$pathology = new common_section('Pathology');
		$pathology->set_column_width(2);
		$form->add($pathology, 1);

		// ... team lead
		$text = 'Require team lead for pathology based assessment to' .
			' count in the field';
		$checkbox = $this->create_checkbox_prompt('RequirePathology_TL',
			$preferences, $read_only, $text);
		$checkbox->set_column_width(2);
		$form->add($checkbox, 1);
		$form->add($empty_section, 1);

		// ... obstetrics
		$prompt = $this->create_non_negative_text_prompt('Obstetrics',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... trauma
		$prompt = $this->create_non_negative_text_prompt('Trauma',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... psychiatric
		$prompt = $this->create_non_negative_text_prompt('Psychiatric',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... cardiac
		$prompt = $this->create_non_negative_text_prompt('Cardiac',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... cardiac arrest
		$prompt = $this->create_non_negative_text_prompt('Cardiac_Arrest',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... cva
		$prompt = $this->create_non_negative_text_prompt('CVA',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... medical
		$prompt = $this->create_non_negative_text_prompt('Medical',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... neuro
		$prompt = $this->create_non_negative_text_prompt('Neuro',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... path respiratory
		$prompt = $this->create_non_negative_text_prompt('Path_Respiratory',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);
		$form->add($prompt_box, 1);

		// Complaints section.
		$prompt_box = new common_promptbox(array(1,1));
		$complaints = new common_section('Complaints');
		$complaints->set_column_width(2);
		$form->add($complaints, 1);

		// ... team lead
		$text = 'Require team lead for complaint  based assessment to' .
			' count in the field';
		$checkbox = $this->create_checkbox_prompt('RequireComplaint_TL',
			$preferences, $read_only, $text);
		$checkbox->set_column_width(2);
		$form->add($checkbox, 1);
		$form->add($empty_section, 1);

		// ... chest pain
		$prompt = $this->create_non_negative_text_prompt('Chest_Pain',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... comp respiratory
		$prompt = $this->create_non_negative_text_prompt('Comp_Respiratory',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... pediatric respiratory
		$prompt = $this->create_non_negative_text_prompt(
			'Pediatric_Respiratory',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... syncope
		$prompt = $this->create_non_negative_text_prompt('Syncope',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... abdominal
		$prompt = $this->create_non_negative_text_prompt('Abdominal',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... ams
		$prompt = $this->create_non_negative_text_prompt('AMS',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... general weakness
		$prompt = $this->create_non_negative_text_prompt('General_Weakness',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 1);

		// ... headache blurred vision
		$prompt = $this->create_non_negative_text_prompt(
			'Headache_Blurred_Vision',
			$preferences, $default_preferences, $read_only);
		$prompt_box->add($prompt, 2);

		// ... dizzyness
		$prompt = $this->create_non_negative_text_prompt('Dizzyness',
			$preferences, $default_preferences, $read_only);
		$prompt->set_text('Dizziness');     // fix DB column spelling
		$prompt_box->add($prompt, 1);
		$form->add($prompt_box, 1);

		// Team lead section.
		$prompt_box = new common_promptbox(array(1,1));
		$team_lead = new common_section('Team Lead');
		$team_lead->set_column_width(2);
		$form->add($team_lead, 1);

		// ... team lead
		$text = 'Require assessment for team lead to' .
			' count in the field';
		$checkbox = $this->create_checkbox_prompt('RequireTeamLead_Assess',
			$preferences, $read_only, $text);
		$checkbox->set_column_width(2);
		$form->add($checkbox, 1);
		$form->add($empty_section, 1);

		// ... number of team leads
		$prompt = $this->create_non_negative_text_prompt('Team_Lead',
			$preferences, $default_preferences, $read_only, 
			'Number of Team Leads');
		$prompt_box->add($prompt, 1);

		// The password field.
		if ($this->needs_authorization()) {
			$prompt = new common_prompt(PASSWORD_FIELD_NAME, '', 
				'Password', null, 'password', true);
			$prompt_box->add($prompt, 2);
		}
		$form->add($prompt_box, 1);

		// Add the hidden action.
		$prompt = new common_prompt('action', $this->get_name(), '', null,
			'hidden', false);
		$prompt->set_column_width(2);
		$form->add($prompt, 1);

		// Add the hidden goal name.
		$prompt = new common_prompt('goal_name', $this->get_goal_name(), 
		'', null, 'hidden', false);
		$prompt->set_column_width(2);
		$form->add($prompt, 1);

		$this->add_buttons($form);
		return $form;
	}

	/**
	 * Create the preferences from row data.
	 * @param rows The row data.
	 * @return The preferences.
	 */
	function create_preferences_from_rows(&$rows) {
		$preferences = array();
		return $preferences;
	}

	/**
	 * Retrieve the preferences.
	 * @return An associative array of key(name), * value(GoalPreference) or 
	 * an empty array if there are none.
	 */
	function get_preferences() {
		$connection = &$this->get_connection();
		$program_id = $this->get_program_id();
		$goal_name = $this->get_goal_name(); 

		$statement = 'SELECT * FROM JRCPrefsData WHERE' .
			" Program_id={$program_id} AND Type='{$goal_name}'";
		$rows = $connection->query($statement);

		if (count($rows) == 1) {
			foreach ($rows[0] as $name=>$value) {
				$preference = new GoalPreference($name, $value);
				$preferences[$name] = $preference;
			}
		} else {
			$preferences = array();
		}

		return $preferences;
	}

	/**
	 * Retrieve the default preferences.
	 * @return An associative array of key(name), * value(GoalPreference).
	 */
	function get_default_preferences() {
		$connection = $this->get_connection();
		$statement = 'SHOW FULL COLUMNS FROM JRCPrefsData'; 
		$rows = $connection->query($statement);

		$n = count($rows);
		for ($i = 0; $i < $n; ++$i) {
			$name = $rows[$i]['Field'];

			// Determine the size of the column.
			// The size is contained in the type, i.e. smallint(5).
			$size = null;
			$type = $rows[$i]['Type'];

			$matches = array();
			$n_matches = ereg('^.*\(([0-9]+)\)', $type, $matches);
			if (($n_matches !== false) && (count($matches) > 1)) {
				$size = (int)$matches[1];
			}

			$preference = new GoalPreference(
				$name, $rows[$i]['Default'], $size);
			$preferences[$name] = $preference;
		}

		return $preferences;
	}

	/**
	 * Retrieve the name of the goals.
	 * @return The goal name or null if not found.
	 */
	function get_goal_name() {
		$name = SetGoalUtils::get_array_value($_REQUEST, 'goal_name', null);

		// When displaying a single goal, first look in the 
		// hidden goal name field.
		if (is_null($name) && SetGoalUtils::is_single_goal_form()) {
			$name = common_utilities::get_scriptvalue('goal_name');
		}

		if (is_null($name)) {
			$name = '';
		}

		return $name;
	}

	/**
	 * Redirect to the goals home page.
	 */
	function go_home() {
		$url = $_SERVER['SCRIPT_NAME'];
		header('Location: ' . $url);
	}

	/**
	 * Require that the goal_name parameter be specified, or redirect.
	 */
	function require_goal_name() {
		$name = $this->get_goal_name();
		if (is_null($name) || ($name == '')) {
			$this->go_home();
		}
	}

	/**
	 * Determine if the goal name is unique.
	 * Uniqueness is checked only if necessary.
	 * @return TRUE if the name is unique.
	 */
	function is_goal_name_unique() {
		$unique = true;
		if ($this->check_name_for_uniqueness()) {
			$new_goal_name = common_utilities::get_scriptvalue(
				NEW_GOAL_NAME_FIELD_NAME);
			$program_id = $this->get_program_id();

			$connection = &$this->get_connection();
			$statement = 'SELECT Type FROM JRCPrefsData WHERE' .
				" Program_id={$program_id} AND Type='{$new_goal_name}'";

			$rows = $connection->query($statement);

			$unique = (count($rows) == 0);
		}

		return $unique;
	}
}

class ErrorAction extends Action {
	var $message;

	/**
	 * Constructor.
	 * @param name The action name.
	 * @param connection The FISDAP connection.
	 * @param page The common page.
	 * @param message The message to be displayed.
	 */
	function ErrorAction($name, &$connection, &$page, $message) {
		parent::Action($name, $connection, $page);
		$this->message = $message;
	}

	function process() {
		$page = &$this->get_page();

		$css = <<<END
.goalserror {margin-top: 200px; color: red; text-align:center}
END;

		$page->add_stylesheetcode($css);
		$page->set_title('Goals Error');
		$page->display_header();
		$page->display_pagetitle();
?>
<div class="goalserror largetext">
<?php
		echo $this->message;
?>
</div>
<?php
		$page->display_footer();
	}
}

class SelectionAction extends Action {
	/**
	 * Constructor.
	 * @param name The action name.
	 * @param connection The FISDAP connection.
	 * @param page The common page.
	 */
	function SelectionAction($name, &$connection, &$page) {
		parent::Action($name, $connection, $page);
	}

	function process() {
		$page = &$this->get_page();
		$page->set_title('Select Goals');

		// If have left a bit of legacy formatted code here
		// as you never know when we will add a search
		// box (again).
		display_select_form('above');
		display_select_form('below');
	}
}

class CreateAction extends GoalAction {
	/**
	 * Constructor.
	 * @param name The action name.
	 * @param connection The FISDAP connection.
	 * @param page The common page.
	 */
	function CreateAction($name, &$connection, &$page) {
		parent::GoalAction($name, $connection, $page);
	}

	function is_read_only() {
		return false;
	}

	function is_name_read_only() {
		return false;
	}

	function check_name_for_uniqueness() {
		return $this->should_save_goals_();
	}

	function add_buttons(&$form) {
		$form->set_submitvalue('Create');
		$form->set_mode('update');
	}

	function process() {
		// Set the title.
		$page = &$this->get_page();
		$page->set_title('Create Goals');

		$form = &$this->create_form();
		if ($form->process()) {
			if ($this->should_save_goals_()) {
				$this->create_goals();
			}

			$this->go_home();
		}
	}

	function should_save_goals_() {
		$button_name = common_utilities::get_scriptvalue(
			'submitform');
		return ($button_name == 'Create');
	}

	function needs_update_permission() {
		return true;
	}

	/**
	 * Create a set of goals in the DB.
	 */
	function create_goals() {
		// Even though we checked for a unique goal name during
		// validation, we need to check here also.
		if (!$this->lock_table()) {
			return;
		}

		// Create the new set of goals.
		$goal_name = common_utilities::get_scriptvalue(
			NEW_GOAL_NAME_FIELD_NAME);
		$program_id = $this->get_program_id();

		// The goal name must be unique.
		if ($this->is_goal_name_unique()) {
			$statement = 'INSERT INTO JRCPrefsData ' .
				$this->get_set_phrase() .
				", Program_id={$program_id}, Type='{$goal_name}'";
			$connection =& $this->get_connection();
			$n = $connection->insert($statement);
			if ($n != 1) {
				write_session_flash('error', 'Unable to create goals.');
			}
		}

		// Release our locks.
		$this->unlock_tables();
	}
}

class DeleteAction extends GoalAction {
	/**
	 * Constructor.
	 * @param name The action name.
	 * @param connection The FISDAP connection.
	 * @param page The common page.
	 */
	function DeleteAction($name, &$connection, &$page) {
		parent::GoalAction($name, $connection, $page);
	}

	function is_read_only() {
		return true;
	}

	function is_name_read_only() {
		return true;
	}

	function add_buttons(&$form) {
		$form->enable_delete_button();
		$form->set_mode('delete');
	}

	function process() {
		$this->require_goal_name();

		// Set the title.
		$page = &$this->get_page();
		$title = 'Delete "' . $this->get_goal_name() . '" Goals';
		$page->set_title($title);

		$form = &$this->create_form();
		if ($form->process()) {
			$button_name = common_utilities::get_scriptvalue(
				'submitform');
			if ($button_name == 'Delete') {
				$this->delete_goals();
			}

			$this->go_home();
		}
	}

	function delete_goals() {
		$program_id = $this->get_program_id();
		$goal_name = $this->get_goal_name();

		$connection =& $this->get_connection();
		$statement = 'DELETE FROM JRCPrefsData WHERE' .
			" Program_id={$program_id} AND Type='{$goal_name}' LIMIT 1";
		$n = $connection->purge($statement);
		if ($n != 1) {
			write_session_flash('error', 'Unable to delete goals..');
		}
	}

	function needs_update_permission() {
		return true;
	}
}

class EditAction extends GoalAction {
	/**
	 * Constructor.
	 * @param name The action name.
	 * @param connection The FISDAP connection.
	 * @param page The common page.
	 */
	function EditAction($name, &$connection, &$page) {
		parent::GoalAction($name, $connection, $page);
	}

	function is_read_only() {
		return false;
	}

	function is_name_read_only() {
		// Since common form is unable to preserve a disabled
		// value when an error occurs, this form is currently
		// used for editing also.
		return false;
	}

	function check_name_for_uniqueness() {
		// We check the name if goals are being saved AND
		// the goal name was modified.
		$check_name = false;
		if ($this->should_save_goals_()) {
			$old_goal_name = $this->get_goal_name();
			$new_goal_name = common_utilities::get_scriptvalue(
				NEW_GOAL_NAME_FIELD_NAME);
			if ($old_goal_name != $new_goal_name) {
				$check_name = true;
			}
		}

		return $check_name;
	}

	function add_buttons(&$form) {
		$form->set_submitvalue('Save');
		$form->set_mode('update');
	}

	function process() {
		$this->require_goal_name();

		// Set the title.
		$page = &$this->get_page();
		$title = 'Edit "' . $this->get_goal_name() . '" Goals';
		$page->set_title($title);

		$form = &$this->create_form();
		if ($form->process()) {
			if ($this->should_save_goals_()) {
				$this->edit_goals();
			}

			$this->go_home();
		}
	}

	function should_save_goals_() {
		$button_name = common_utilities::get_scriptvalue(
			'submitform');
		return ($button_name == 'Save');
	}

	function edit_goals() {
		$old_goal_name = $this->get_goal_name();
		$new_goal_name = common_utilities::get_scriptvalue(
			NEW_GOAL_NAME_FIELD_NAME);
		if ($old_goal_name == $new_goal_name) {
			$this->update_goals(null);
		} else {
			$this->update_goals($new_goal_name);
		}
	}

	/**
	 * Change a set of goals in the DB.
	 * @param new_name If specified, the goals are renamed also.
	 */
	function update_goals($new_name=null) {
		$goal_name = $this->get_goal_name();
		$program_id = $this->get_program_id();

		if (is_null($new_name)) {
			$rename_clause = '';
		} else {
			$rename_clause = ", Type='{$new_name}'";
		}

		// If the goals are being renamed also, we need to ensure
		// the goal name is still unique.
		$renaming = ($rename_clause != '');
		if ($renaming && !$this->lock_table()) {
			return;
		}

		// The goal name must be unique.
		if (!$renaming || $this->is_goal_name_unique()) {
			$statement = 'UPDATE JRCPrefsData ' .
				$this->get_set_phrase() .
				$rename_clause .
				" WHERE Program_id={$program_id} AND Type='{$goal_name}'" .
				' LIMIT 1';
			$connection =& $this->get_connection();
			$connection->update($statement);
		}

		// ... unless we check to see if the data actually changed,
		//     we can't check for an error as the update may return false
		//     if no data has changed.

		// Release our locks.
		if ($renaming) {
			$this->unlock_tables();
		}
	}

	function needs_update_permission() {
		return true;
	}
}

class ViewAction extends GoalAction {
	/**
	 * Constructor.
	 * @param name The action name.
	 * @param connection The FISDAP connection.
	 * @param page The common page.
	 */
	function ViewAction($name, &$connection, &$page) {
		parent::GoalAction($name, $connection, $page);
	}

	function is_read_only() {
		return true;
	}

	function is_name_read_only() {
		return true;
	}

	function needs_update_permission() {
		return false;
	}

	function add_buttons(&$form) {
		$form->set_submitvalue('Back');
		$form->set_mode('update');
	}

	function process() {
		$this->require_goal_name();

		// Set the title.
		$page = &$this->get_page();
		$title = 'View "' . $this->get_goal_name() . '" Goals';
		$page->set_title($title);

		$form = &$this->create_form();
		if ($form->process()) {
			$this->go_home();
		}
	}
}

/**
 * A DB preference.
 */
class GoalPreference {
	var $name;
	var $size;
	var $value;

	// Items that are booleans.
	var $boolean_names = array(
		'RequireAge_TL',
		'RequirePathology_TL',
		'RequireComplaint_TL',
		'RequireTeamLead_Assess'
	);

	// Items that should NOT be defaults.
	var $not_default_names = array(
		'Pref_id',
		'Program_id',
		'Type'
	);

	/**
	 * Constructor.
	 * @param name The name of the preference.
	 * @param value The value of the preference.
	 */
	function GoalPreference($name, $value, $size=null) {
		$this->name = $name;
		$this->size = $size;
		$this->value = $value;
	}

	/**
	 * Retrieve the name.
	 * @return The name.
	 */
	function get_name() {
		return $this->name;
	}

	/**
	 * Retrieve the boolean value, valid for boolean preferences.
	 * @return TRUE or FALSE.
	 */
	function get_boolean_value() {
		return ($this->value == 1);
	}

	/**
	 * Retrieve the value.
	 * @return The value.
	 */
	function get_value() {
		return $this->value;
	}

	/**
	 * Determine if this preference is a boolean preference.
	 * @return TRUE if the preference is a boolean.
	 */
	function is_boolean() {
		return in_array($this->get_name(), $this->boolean_names);
	}

	/**
	 * Determine if the preference can be a default value.
	 * @return TRUE if the preference can be a default value.
	 */
	function can_be_default() {
		return !in_array($this->get_name(), $this->not_default_names);
	}

	/**
	 * Determine whether the size of the preference has been set.
	 * @return TRUE if the size has been set.
	 */
	function has_size() {
		return !is_null($this->size);
	}

	/**
	 * Retrieve the size of the preference.
	 * @return The size of the preference, usually in characters.
	 */
	function get_size() {
		return $this->size;
	}
}
?>
