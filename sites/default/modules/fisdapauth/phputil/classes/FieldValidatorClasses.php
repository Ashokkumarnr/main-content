<?php
/*---------------------------------------------------------------------*
  |                                                                     |
  |      Copyright (C) 1996-2010.  This is an unpublished work of       |
  |                       Headwaters Software, Inc.                     |
  |                          ALL RIGHTS RESERVED                        |
  |      This program is a trade secret of Headwaters Software, Inc.    |
  |      and it is not to be copied, distributed, reproduced, published |
  |      or adapted without prior authorization                         |
  |      of Headwaters Software, Inc.                                   |
  |                                                                     |
* ---------------------------------------------------------------------*/

/** Classes
 * 
 * Format of validation results used througout the two validator classes is:
 * $results = array (
 *   [0 = row_identifier] => Array (
 *   	['errors' or 'warnings'] => array (
 *   		['table or group description'] => array (
 *   			['field validated'] => 'Validation error/warning message'
 *   		)
 *   	)
 *   )
 * );
 */
class FieldInputValidator {
	private $vars; 

	/* rules structure: array(fx=>val, fx2=>val2, fx->array(vals)) 
		ex: array ('min_len'=>1, 'max_len'=>25)
	 */
	protected $rules;
	protected $results;

	// validation results object (all results collected)
	protected $validations;
	protected $rowid;		//rowid going to validations object

	// validation results of last field validation call (all rules for one field) 
	// validations passed if:  empty($this->last_result)
	protected $last_result = array();


	// instantiating table info
	protected $table;

	// field / variable name reported back as failing validation
	protected $caption;

	protected static $validation_message = array(
		'min_len' => '%1$s: \'%2$s\' is too short. It needs to be at least %3$s characters long',
		'max_len' => '%1$s: \'%2$s\' is too long. It needs to be %3$s characters long or less',
		'min_int' => '%1$s has to be %3$s or more',
		'max_int' => '%1$s has to equal no more than %3$s',
		'max_val' => '%1$s has to be %3$s or less',
		'min_val' => '%1$s has to be %3$s or more',
		'is_numeric_or_null' => '%1$s: \'%2$s\' has to be a number',
		'is_int_or_null' => '%1$s: \'%2$s\' has to be number (integer)',
		'is_us_state' => '%1$s: \'%2$s\' is not a valid US state',
		'is_valid_fisdap_username' => '%1$s: \'%2$s\' is not a valid FISDAP username. Only alphanumerical characters are allowed.',
		'is_valid_fisdap_gender' => '%1$s: \'%2$s\' is not a valid gender. Accepted values are \'M\', \'F\', \'Male\', \'Female\''
	);
	/*	
		message_overrides	Each validation rule will have default message for validation error/warning
		they can be overriden when instantiating
	 */
	public function __construct($table, $rules, $message_overrides = null) {
		$this->table = $table;
		$this->rules = $rules;

		$this->rowid = 0;

		// validations object
		$this->validations= new FieldInputValidatorResults();
	}


	// test only function 
	public function show_field_rules() {
		echo "Validation Rules in validator\n";
		print_r($this->rules);
	}


	/**
	 * Validates value against given rules
	 * $rules - array or rules ex:
	 * @return boolean validation result for this field for the set of rules
	 */
	public function validate($vars, $rules, $caption="Value") {
		Assert::is_array($rules);

		$this->vars = $vars;
		$this->rules = $rules;
		$this->last_result = array();
		$this->caption = $caption;

		foreach ($rules as $rule => $values) {
			$fx = "validate_".$rule;
			if (method_exists($this, $fx)) {
				$this->$fx($values);
			} else {
				throw new FisdapInvalidArgumentException("Invalid validation function called: \$this->$fx");
			}
		}

		return (empty($this->last_result));
	}

	/** results of validation performed by function validate
	 *
	 */
	public function get_last_result() {
		return $this->last_result;
	}

	/** Tells if all validations passed
	 *
	 */
	public function is_valid() {
		return $this->validations->is_row_valid($this->rowid);
	}

	/* returns results OBJECT
	 */
	public function get_results() {
		return $this->validations->get_results();
	}
	// todo: test - see how show_array display real results & tweak
	public function get_txt_validation_results() {
		//if (empty($this->results)) {
		//	return "";
		//} else {
		//	show_array($this->results);
		//}
	}

	// VALIDATION FUNCTIONS
	private function validation_failed($fx, $value) {	
		$msg = sprintf(self::$validation_message[$fx], $this->caption, $this->vars, $value);
		$this->validations->submit_validation_result($this->rowid, $this->table, $this->caption, $msg);
		$this->last_result[$fx] = $msg;
	}

	private function validate_min_len($value) {
		$fx = 'min_len';
		if (strlen($this->vars) < $value) {
			$this->validation_failed($fx, $value);
		}
	}

	private function validate_max_len($value) {
		$fx = 'max_len';
		if (strlen($this->vars) > $value) { 
			$this->validation_failed($fx, $value);
		}
	}

	private function validate_min_val($value) {
		$fx = 'min_val';
		if ($this->vars < $value) {
			$this->validation_failed($fx, $value);
		}
	}

	private function validate_max_val($value) {
		$fx = 'max_val';
		if ($this->vars > $value) {
			$this->validation_failed($fx, $value);
		}
	}
	private function validate_is_numeric_or_null($value) {
		$fx = 'is_numeric_or_null';
		if (((strlen($this->vars) != strlen((int)$this->vars)) && $this->vars != '') == $value) {
			$this->validation_failed($fx, $value);
		}
	}

	private function validate_is_int_or_null($value) {
		$fx = 'is_int_or_null';
		if ((!is_int($this->vars) && $this->vars!='') != $value) {
			$this->validation_failed($fx, $value);
		}
	}

	private function validate_is_valid_fisdap_username($value) {
		$fx = 'is_valid_fisdap_username';

		if (ctype_alnum($this->vars) && strlen($this->vars)>=5 != $value) {
			$this->validation_failed($fx, $value);
		}
	}

	private function validate_is_valid_fisdap_gender($value) {
		$fx = 'is_valid_fisdap_gender';

		$var = strtoupper($this->vars);

		if ($var=='MALE') {
			$var = 'M';
		} else if ($var=='FEMALE') {
			$var = 'F';
		}

		if (($var=='M' or $var=='F' or $var=='') != $value) {
			$this->validation_failed($fx, $value);
		}
	}

	private function validate_is_us_state($value) {
		$fx = 'is_us_state';
		$state_list = array('AL'=>"Alabama",  
			'AK'=>"Alaska",  
			'AZ'=>"Arizona",  
			'AR'=>"Arkansas",  
			'CA'=>"California",  
			'CO'=>"Colorado",  
			'CT'=>"Connecticut",  
			'DE'=>"Delaware",  
			'DC'=>"District Of Columbia",  
			'FL'=>"Florida",  
			'GA'=>"Georgia",  
			'HI'=>"Hawaii",  
			'ID'=>"Idaho",  
			'IL'=>"Illinois",  
			'IN'=>"Indiana",  
			'IA'=>"Iowa",  
			'KS'=>"Kansas",  
			'KY'=>"Kentucky",  
			'LA'=>"Louisiana",  
			'ME'=>"Maine",  
			'MD'=>"Maryland",  
			'MA'=>"Massachusetts",  
			'MI'=>"Michigan",  
			'MN'=>"Minnesota",  
			'MS'=>"Mississippi",  
			'MO'=>"Missouri",  
			'MT'=>"Montana",
			'NE'=>"Nebraska",
			'NV'=>"Nevada",
			'NH'=>"New Hampshire",
			'NJ'=>"New Jersey",
			'NM'=>"New Mexico",
			'NY'=>"New York",
			'NC'=>"North Carolina",
			'ND'=>"North Dakota",
			'OH'=>"Ohio",  
			'OK'=>"Oklahoma",  
			'OR'=>"Oregon",  
			'PA'=>"Pennsylvania",  
			'RI'=>"Rhode Island",  
			'SC'=>"South Carolina",  
			'SD'=>"South Dakota",
			'TN'=>"Tennessee",  
			'TX'=>"Texas",  
			'UT'=>"Utah",  
			'VT'=>"Vermont",  
			'VA'=>"Virginia",  
			'WA'=>"Washington",  
			'WV'=>"West Virginia",  
			'WI'=>"Wisconsin",  
			'WY'=>"Wyoming");

		// comparison to $value is done only for consistency, so technically 
		// user could request that vars is NOT valid state by setting $value to false
		if (!isset($state_list[strtoupper($this->vars)]) == $value) {
			$this->validation_failed($fx, $value);
		}
	}

	/* TO DO Function currently not used.
	 * Right now we're pretty much using all other functions to validate required fields
	 * Vision for this function and the whole class regarding this is:
	 * - each function should distinguish between: empty value / wrong value / correct value (ex min_len = 5:  '' - empty, 'abc' - wrong 'abcde' - correct
	 * - if field is required validation failures should go into 'errors' otherwise 'warnings', empty values should error out as required field
	 * - if field is not required: wrong value should create 'warning'. Correct or empty values are fine.
	 */
	private function validate_is_required($value) {
		return true;
	}
}














class FieldInputValidatorResults {
	/* results array()
	   id =>
	   'errors' or 'warnings' =>
	   <table>
	   <fieldname or category>
	   <id=val failing ex: 'min_len'> => 
	   Message, ex: Username too short, needs at least 1 character
	 */
	protected $results = array();

	/* used for: descriptions, keep track of all the validation requests for report purposes 
	 *  (all calls to this class set $results_meta[$rowid] upon first call
	 */
	protected $results_meta = array();

	// last row id used by user for adding validation results
	protected $last_row_id;

	public function __construct() {
		$this->results = array();
	}

	/*	2 modes: expecting either:
		$rowid, $table_name, $results FieldInputValidator	- list of results from object
		OR
		$rowid, $table/ident, - single result

	 */

	/* whole row validation result pass / failing
	 */
	public function is_row_valid($rowid) {	//$rowid = null) {
		// default to last_row_id
		//		if (is_null($rowid)) $rowid = $this->last_row_id;

		//		if (!isset($this->results[$rowid]) || (!isset($this->results[$rowid]['errors']))) {
		//			return true;
		//		}
		// bug fixing test only: want to make sure
		Assert::is_not_null($rowid);

		return (empty($this->results[$rowid]['errors']));
	}


	public function get_results() {
		return $this->results;
	}

	/* Row Description used on reports
	 */
	public function set_row_description($rowid, $description) {
		if (is_null($description)) {
			if (isset($this->results_meta[$rowid]['description'])) {
				unset ($this->results_meta[$rowid]['description']);
			}
		} else {
			$this->results_meta[$rowid]['description'] = $description;
		}
	}



	public function submit_validation_result($rowid, $table_or_ident, $field_or_cat, $message='', $warning_only=false) {
		Assert::is_not_null($rowid);
		$this->last_row_id = $rowid;

		// record $rowid for later reports
		if (!isset($this->results_meta[$rowid])) {
			$this->results_meta[$rowid] = array();
		}


		$validationtype = ($warning_only) ? 'warnings' : 'errors';
		$this->results[$rowid][$validationtype][$table_or_ident][$field_or_cat] = $message;
	}

	/*	saves validator object result(s) - 
		should be used by used by data models using UserAbstractModel

		validator results come in this format:
		array ('min_val' => 'problem with min val.. blah blah..'...)
	 */


	/* imports results from another $this object and merges it with results under $rowid
	   verify that $results provided are in correct format:
	   - first level array is numeric
	   - second level array has key of 'errors' or 'warnings' 
	   - array is four elements deep
	   It ignores data that doesn't comply with these requirements.
	   - optionally it throws exception - set $strict_import to true

	Modes: $rowid_or_mode value:
		numeric		- specifies row number to insert result (ONE ROW OF RESULTS makes sense
		although this function will import all multiple rows under requested rowid
		'merge'		- merges results using row_ids from resultset
		'append'	- appends rows to the end (not sure if this has any use)
	 */
	public function import_results($results, $rowid_or_mode, $strict_import = false) {
		$format_incorrect = '';

		// for append mode find last index of existing results
		if ($row_or_mode == 'append') {
			$last_results_id = 0;
			foreach ($this->results as $id => $rest) {
				if ($id > $last_results_id) {
					$last_results_id = $id;
				}
			}
		}

		if (!is_array($results)) {
			$format_incorrect = 'Is not array at all';
		} else {
			foreach ($results as $id => $err_or_warn_arr) {
				if (!is_array($err_or_warn_arr)) {
					$format_incorrect = 'missing 2nd array level -> err_or_warn level';
				} else {

					// mode decisions
					if (is_int($rowid_or_mode)) {
						$rowid = $rowid_or_mode;
					} else if ($rowid_or_mode == 'merge') {
						$rowid = $id;
					} else if ($row_or_mode == 'append') {
						$rowid = $last_results_id++;
					}

					foreach ($err_or_warn_arr as $err_or_warn => $table_arr) {
						if (!in_array($err_or_warn, array("errors", "warnings")) || (!is_array($table_arr))) {
							$format_incorrect = (is_array($table_arr))
								? "err_or_warn value must be 'errors' or 'warnings'"
								: "missing 3rd array level -> table description level";
						} else {
							foreach ($table_arr as $table => $field_arr) {
								if (!is_array($field_arr)) {
									$format_incorrect = "missing 4th array level -> field level";
								} else {
									foreach ($field_arr as $field => $validation_error_message) {
										$this->results[$rowid][$err_or_warn][$table][$field] = $validation_error_message;
										// 	echo "Merging validation results: $rowid - $err_or_warn $table $field - $validation_error_message \n";
									}
								}
							}
						}
					}
				}
			}
		}

		//	if ($format_incorrect) { echo "Invalid data format of validation results passed to function:\n\t$format_incorrect\n"; }

		if ($strict_import && $format_incorrect) {
			throw new FisdapInvalidArgumentException("Invalid data format of validation results passed to function - $format_incorrect");
		}
	}
	/*
	   public function submit_validator_object_result($results, $table_or_ident, $field_or_desc, $id=0) {
	   if (!empty($results)) {
		   foreach ($results as $condition_failed => $failure_details) {
			   $this->submit_validation_result($id, $table_or_ident, $field_or_desc." - ".$condition_failed);
		   }
	   }
	   }
	 */



	/*	Shows validation results:
		- when $rowid<>''	for one row identified by $rowid
		- when $rowid=''	for ALL rows
		Default behavior to $show_row_heading
		- Show -  when function requested to process all rows
		- Hide - when one row is requested
	*/
	public function show_validation_results_txt($rowid='', $show_row_heading = null) {
		echo $this->get_validation_results_txt($rowid = '', $show_row_heading);
	}
	public function get_validation_results_txt($rowid = '', $show_row_heading = null) {
		// set default heading showing/hiding behavior
		if (is_null($show_row_heading)) {
			$show_row_heading = ($rowid === '');
		}

		if ($rowid==='') {	// multiple rows
			foreach ($this->results_meta as $id => $vals) {
				$ret.=$this->get_validation_results_txt($id, $show_row_heading);
			}
		} else {	// one row
			if ($show_row_heading) {
				$ret.=isset($this->results_meta[$rowid]['description']) 
					? $this->results_meta[$rowid]['description'] : $rowid;
				if (isset($this->results[$rowid])) {
					$ret.="\n";
				}
			}

			// generate validation results
			if ($this->is_row_valid($rowid)) {
				$ret.=" Validation was successfull!\n";
			} else {
				if (!is_array($this->results[$rowid])) { //HEREEEEE
					throw new FisdapException ("PROBLEM in get_validations_results: row: '$rowid'\n");
				}
				foreach ($this->results[$rowid] as $errtype => $tablearr) {
					$errcapt = ($errtype == 'errors')? "ERROR   " : "WARNING ";
					foreach ($tablearr as $table => $field_arr) {
						foreach ($field_arr as $field => $problemdesc) {
							$ret.=sprintf("%9s %-15s %-20s %20s\n", $errcapt, $table, $field, $problemdesc);
						}
					}
				}
			}
		}
		return $ret;
	}
}

?>
