<?php
/**
 * A class that manages a prompt hierarchy.
 * <pre>
 * The class will handle:
 * - Showing / hiding prompts.
 * - Deleting any session values when the prompt is hidden.
 *
 * Common usage:
 *
 * // Create the form, making all prompts under control here 'visible'.
 * // $pr = new PromptHierarchy($form);
 * // ... set the relationships with $pr->set_relationship()
 * // $pr->init();
 * // $pr->show_or_hide_all();
 * // $form->process();
 *
 * Updates:
 * // The action could be made a composite.
 * </pre>
 */
class PromptHierarchy
{
    private $form;

    private $relationships = array();   // prompt-name => array(condition, prompt)
    private $on_show = array();         // prompt-name => action
    private $initially_shown = array();

    /**
     * Constructor.
     * @param common_form_base $form The form the prompts are in.
     */
    public function __construct($form) {
        $this->form = $form;
    }

    /**
     * Set up a relationship.
     * @param common_prompt $prompt The prompt the relationship applies to.
     * @param PromptHierarchyCondition $condition The condition.
     */
    public function set_relationship($prompt, $condition) {
        $this->relationships[$prompt->get_name()] = array($condition, $prompt);

        // Set the form parameters.
        $prompts = $condition->get_prompts();
        foreach ($prompts as &$subject) {
            $prompt->add_listento($subject->get_name(), array($this, 'update'));
        }
    }

    /**
     * Set the on-show action.
     * This action is performed whenever the prompt comes out of hiding during 
     * an AJAX callback.
     * @param common_prompt $prompt The prompt the relationship applies to.
     * @param PromptHierarchyAction $action The action to execute.
     */
    public function set_on_show_action($prompt, $action) {
        $this->on_show[$prompt->get_name()] = $action;
    }

    /**
     * Initialize the object.
     * Should be invoked after all relationships are defined.
     */
    public function init() {
        // We keep the status of whether the prompt is shown to avoid
        // redrawing a prompt.  Also, is a redrawn prompt didn't have it's
        // state sent to the server, as in the case of a checkbox listening
        // to no one, then redrawing the box will lose the check.
        foreach ($this->relationships as &$relationship) {
            $prompt =& $relationship[1];
            $this->initially_shown[$prompt->get_name()] = $this->is_shown($prompt);
        }
    }

    /**
     * Hide or show the prompts.
     */
    public function show_or_hide_all() {
        foreach ($this->relationships as &$relationship) {
            self::show_or_hide_by_relationship($relationship); 
        }
    }

    /**
     * Show or hide a component.
     * @param array $relationship The relationship.
     * @return boolean TRUE if the component is shown.
     */
    private static function show_or_hide_by_relationship($relationship) {
        $prompt =& $relationship[1];

        $show = $relationship[0]->can_show();
        if ($show) {
            $prompt->show();
        }
        else {
            $prompt->hide();

            // Since the prompt is hidden, wipe out its value.
            $prompt->delete_session_values();
        }

        return $show;
    }

    /**
     * Determine if a prompt is shown.
     * @param common_prompt $prompt The prompt.
     * @param boolean $default The value to return if the prompt is not handled 
     * here.
     * @return boolean TRUE if the prompt is shown.
     */
    public function is_shown($prompt, $default=true) {
        $name = $prompt->get_name();
        if (!isset($this->relationships[$name])) return $default;

        $relationship =& $this->relationships[$name];
        return ($relationship[0]->can_show()); 
    }

    /**
     * Called back for an AJAX event by the form.
     * @param common_prompt $prompt The prompt that is watching for a change.
     * @param string|null $ajax_id The ID of the prompt (component) that changed.
     * @param string|null $ajax_baseid The ID of the (base) prompt that changed.
     */
    public function update($prompt, $ajax_id, $ajax_baseid) {
        $name = $prompt->get_name();
        if (!isset($this->relationships[$name])) return false;

        $shown = self::show_or_hide_by_relationship($this->relationships[$name]); 

        // If the status hasn't changed, don't redraw it.
        if ($shown && $this->initially_shown[$name]) return false;
        if (!$shown && !$this->initially_shown[$name]) return false;

        $this->initially_shown[$name] = $shown;

        if ($shown) {
            if (isset($this->on_show[$name])) {
                $action = $this->on_show[$name];
                $action->execute();
            }
        }

        return true;
    }
}

/**
 * A condition.
 */
abstract class PromptHierarchyCondition {
    /**
     * Determine if a checkbox prompt is checked.
     * @param common_prompt $prompt The prompt to check.
     * @return TRUE if checked.
     */
    protected static function is_checked($prompt) {
        list($isset, $value) = $prompt->get_session_promptvalue();
        if (!$isset) return false;

        if (is_null($value)) return false;
        if (is_array($value)) {
            if (!count($value)) return false;
            $value = $value[0];
        }

        return (!is_null($value) && ($value != ''));
    }

    /**
     * Determine whether a prompt should show.
     * @return TRUE if the prompt should show.
     */
    abstract public function can_show();

    /**
     * Determine the prompts in the condition.
     * @return array The prompts in the condition.
     */
    abstract public function get_prompts();
}

/**
 * A condition that shows a prompt if a checkbox is checked.
 */
class PromptHierarchyShowIfCheckedCondition extends PromptHierarchyCondition {
    private $prompt;

    /**
     * Constructor.
     * @param common_prompt $prompt The prompt to check.
     */
    public function __construct($prompt) {
        $this->prompt = $prompt;
    }

    public function can_show() {
        return parent::is_checked($this->prompt);
    }

    public function get_prompts() {
        return array($this->prompt);
    }
}

/**
 * A condition that shows a prompt if all checkboxs are checked.
 */
class PromptHierarchyShowIfAllCheckedCondition extends PromptHierarchyCondition {
    private $prompts;

    /**
     * Constructor.
     * @param array of common_prompt $prompts The prompt to check.
     */
    public function __construct($prompts) {
        $this->prompts = $prompts;
    }

    public function can_show() {
        foreach ($this->prompts as &$prompt) {
            if (!parent::is_checked($prompt)) return false;
        }

        return true;
    }

    public function get_prompts() {
        return $this->prompts;
    }
}

/**
 * A condition that shows a prompt if any checkboxs are checked.
 */
class PromptHierarchyShowIfAnyCheckedCondition extends PromptHierarchyCondition {
    private $prompts;

    /**
     * Constructor.
     * @param array of common_prompt $prompts The prompt to check.
     */
    public function __construct($prompts) {
        $this->prompts = $prompts;
    }

    public function can_show() {
        foreach ($this->prompts as &$prompt) {
            if (parent::is_checked($prompt)) return true;
        }

        return false;
    }

    public function get_prompts() {
        return $this->prompts;
    }
}

/**
 * An action.
 */
interface PromptHierarchyAction {
    /**
     * Perform the action.
     */
    public function execute();
}

/**
 * An action that sets the prompts value.
 */
class PromptHierarchySetValueAction implements PromptHierarchyAction {
    private $prompt;
    private $value;

    public function __construct($prompt, $value) {
        $this->prompt = $prompt;
        $this->value = $value;
    }

    public function execute() {
        $this->prompt->set_session_promptvalue($this->value);
    }
}

/**
 * A composite action.
 */
class PromptHierarchyCompositeAction implements PromptHierarchyAction {
    private $actions = array();

    /**
     * Add an action.
     * @param PromptHierarchyAction $action The action.
     */
    public function add($action) {
        if (!($action instanceof PromptHierarchyAction)) {
            die(__CLASS__ . ': must add a PromptHierarchyAction');
        }

        $this->actions[] = $action;
    }

    public function execute() {
        foreach ($this->actions as $action) {
            $action->execute();
        }
    }
}
?>
