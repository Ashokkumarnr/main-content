<?php
require_once('phputil/classes/list.inc');

/**
 * A factory to create a list.
 */
interface ListFactory {
    /**
     * Create a list container.
     * @param mixed $list The underlying list.
     * @return List The list wrapper.
     */
    public function create($list);
}

/**
 * A factory to create a list sorted by the PHP sort() function.
 */
class PhpSortedListFactory implements ListFactory {
    private $sort_flags;

    /**
     * Constructor.
     * @param int $sort_flags The sort flags.
     */
    public function PhpSortedListFactory($sort_flags=SORT_REGULAR) {
        $this->sort_flags = $sort_flags;
    }

    public function create($list) {
        $sorted = $list;
        sort($sorted, $this->sort_flags);
        return new SortedList($sorted);
    }
}

/**
 * A factory to create a unsorted list.
 */
class UnsortedListFactory implements ListFactory {
    public function create($list) {
        return new UnsortedList($list);
    }
}
?>
