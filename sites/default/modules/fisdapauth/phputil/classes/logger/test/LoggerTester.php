<?php

require_once('phputil/classes/logger/Logger.php');
require_once('phputil/classes/logger/OutputLogListener.php');


class LoggerTester extends UnitTestCase {
    function setUp() {
        // we'll use a mock listener to test the logger
        Mock::generate('OutputLogListener');

        $this->logger =& Logger::get_instance();
    }//setUp


    function tearDown() {
        // clean up any listeners added in the course of a test
        $this->logger->clear_listeners();
    }//tearDown


    function test_log() {
        $message = 'this is a test message';
        $this->logger->set_level($this->logger->WARN);
        
        $listener1 =& new MockOutputLogListener($this);
        $listener1->expectOnce('update',array($message),'update not called');
        $this->logger->add_listener($listener1);

        $listener2 =& new MockOutputLogListener($this);
        $listener2->expectOnce('update',array($message),'update not called');
        $this->logger->add_listener($listener2);

        $listener3 =& new MockOutputLogListener($this);
        $listener3->expectOnce('update',array($message),'update not called');
        $this->logger->add_listener($listener3);

        // make sure the above messages made it to the listeners
        $this->logger->log($message,$this->logger->WARN);
        $listener1->tally();
        $listener2->tally();
        $listener3->tally();

        // try some messages that shouldn't be logged
        $listener4 =& new MockOutputLogListener($this);
        $listener4->expectNever('update','update was called erroneously');
        $this->logger->add_listener($listener4);
        $this->logger->log($message,$this->logger->DEBUG);
        $listener4->tally();
    }//test_log
}//class LoggerTester

?>
