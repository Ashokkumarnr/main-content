<?php

require_once('phputil/classes/validation/ErrorSet.php');
require_once('phputil/classes/validation/Validator.php');
require_once('phputil/classes/validation/InputValidator.php');
require_once('phputil/classes/validation/StringWhitelistValidator.php');


/**
 * This class tests the StringWhitelistValidator
 */
class StringWhitelistValidatorTester extends UnitTestCase {
    function test_validate() {
        $error_message = 'this is a test error message!';
        $whitelist = array(
            'validstring1',
            'validstring2',
            'validstring3'
        );
        $input = array(
            'validstring_1'=>'validstring1',
            'validstring_2'=>'validstring2',
            'validstring_3'=>'validstring3',
            'invalidstring_1'=>'invalidstring1',
            'invalidstring_2'=>'invalidstring2',
            'empty_string'=>''
        );


        //
        // these fields should pass validation
        //

        // a field not appearing in the input shouldn't cause errors (since
        // that's the job of the RequiredFieldValidator)

        $valid_fields = array( 'absent_field', 'validstring_1', 'validstring_2', 'validstring_3' );
        foreach( $valid_fields as $valid_field ) {
            $text_validator =& new StringWhitelistValidator($valid_field,$error_message);
            $text_validator->set_whitelist($whitelist);
            $text_validator->validate($input);
            $this->assertFalse($text_validator->has_errors(),'error validating absent field');
        }//foreach


        //
        // these fields should fail validation
        //

        $invalid_fields = array( 'invalidstring_1', 'invalidstring_2' );
        foreach( $invalid_fields as $invalid_field ) {
            $text_validator =& new StringWhitelistValidator($invalid_field,$error_message);
            $text_validator->set_whitelist($whitelist);
            $text_validator->validate($input);
            $this->assertTrue($text_validator->has_errors(),'error on invalid string');
            $text_errors = $text_validator->get_errors();
            $this->assertEqual(count($text_errors),1,'wrong number of errors returned for invalid input');
            $this->assertEqual($text_errors[0]->get_message(),$error_message,'error text unexpected');
        }//foreach
    }//test_validate

}//class StringWhitelistValidatorTester
?>
