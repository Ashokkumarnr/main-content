<?php

require_once('phputil/classes/validation/ErrorSet.php');
require_once('phputil/classes/validation/Validator.php');
require_once('phputil/classes/validation/InputValidator.php');
require_once('phputil/classes/validation/IntegerValidator.php');


/**
 * This class tests the IntegerValidator
 */
class IntegerValidatorTester extends UnitTestCase {
    function test_validate() {
        $error_message = 'this is a test error message!';
        $input = array(
            'text'=>'one',         // N text
            'bigint'=>'4568234',   // Y big integer
            'smallint'=>'3',       // Y small integer
            'negint'=>'-4',        // Y negative integer
            'empty'=>'',           // N empty string
            'float'=>'55.7'        // N float 
        );


        //
        // these fields should pass validation
        //

        // a field not appearing in the input shouldn't cause errors (since
        // that's the job of the RequiredFieldValidator)
        $absent_field_validator =& new IntegerValidator('absent_field',$error_message);
        $absent_field_validator->validate($input);
        $this->assertFalse($absent_field_validator->has_errors(),'error validating absent field');

        // big integer
        $bigint_validator =& new IntegerValidator('bigint',$error_message);
        $bigint_validator->validate($input);
        $this->assertFalse($bigint_validator->has_errors(),'error validating big integer ('.htmlentities($input['bigint']).')');

        // small integer
        $smallint_validator =& new IntegerValidator('smallint',$error_message);
        $smallint_validator->validate($input);
        $this->assertFalse($smallint_validator->has_errors(),'error validating small integer ('.htmlentities($input['smallint']).')');

        // negative integer
        $negint_validator =& new IntegerValidator('negint',$error_message);
        $negint_validator->validate($input);
        $this->assertFalse($negint_validator->has_errors(),'error validating negative integer ('.htmlentities($input['negint']).')');


        //
        // these fields should fail validation
        //

        // non-integer text
        $text_validator =& new IntegerValidator('text',$error_message);
        $text_validator->validate($input);
        $this->assertTrue($text_validator->has_errors(),'error validating non-integer text input ('.htmlentities($input['text']).')');
        $text_errors = $text_validator->get_errors();
        $this->assertEqual(count($text_errors),1,'wrong number of errors returned for text input');
        $this->assertEqual($text_errors[0]->get_message(),$error_message,'error text unexpected');

        // floating-point number 
        $float_validator =& new IntegerValidator('float',$error_message);
        $float_validator->validate($input);
        $this->assertTrue($float_validator->has_errors(),'error validating float input ('.htmlentities($input['float']).')');
        $float_errors = $float_validator->get_errors();
        $this->assertEqual(count($float_errors),1,'wrong number of errors returned for float input');
        $this->assertEqual($float_errors[0]->get_message(),$error_message,'error float unexpected');

        // empty string
        $empty_validator =& new IntegerValidator('empty',$error_message);
        $empty_validator->validate($input);
        $this->assertTrue($empty_validator->has_errors(),'error validating empty input ('.htmlentities($input['empty']).')');
        $empty_errors = $empty_validator->get_errors();
        $this->assertEqual(count($empty_errors),1,'wrong number of errors returned for empty input');
        $this->assertEqual($empty_errors[0]->get_message(),$error_message,'error empty unexpected');

    }//test_validate

}//class IntegerValidatorTester
?>
