<?php

require_once('phputil/classes/validation/ErrorSet.php');
require_once('phputil/classes/validation/Validator.php');
require_once('phputil/classes/validation/InputValidator.php');
require_once('phputil/classes/validation/IntegerRangeValidator.php');


/**
 * This class tests the IntegerRangeValidator
 */
class IntegerRangeValidatorTester extends UnitTestCase {
    function test_validate() {
        $error_message = 'this is a test error message!';
        $input = array(
            'text'=>'one',         // N text
            'empty'=>'',           // N empty string
            'float'=>'55.7',       // N float 

            'one'=>'1',            // N below range
            'two'=>'2',            // Y in range
            'three'=>'3',          // Y in range
            'four'=>'4',           // Y in range
            'five'=>'5'            // N above range
        );


        //
        // these fields should pass validation
        //

        // a field not appearing in the input shouldn't cause errors (since
        // that's the job of the RequiredFieldValidator)
        $absent_field_validator =& new IntegerRangeValidator('absent_field',2,4,$error_message);
        $absent_field_validator->validate($input);
        $this->assertFalse($absent_field_validator->has_errors(),'error validating absent field');

        // in range 
        $two_field_validator =& new IntegerRangeValidator('two_field',2,4,$error_message);
        $two_field_validator->validate($input);
        $this->assertFalse($two_field_validator->has_errors(),'error validating two field');

        // in range 
        $three_field_validator =& new IntegerRangeValidator('three_field',2,4,$error_message);
        $three_field_validator->validate($input);
        $this->assertFalse($three_field_validator->has_errors(),'error validating three field');

        // in range 
        $four_field_validator =& new IntegerRangeValidator('four_field',2,4,$error_message);
        $four_field_validator->validate($input);
        $this->assertFalse($four_field_validator->has_errors(),'error validating four field');

        //
        // these fields should fail validation
        //

        // non-integer text
        $text_validator =& new IntegerRangeValidator('text',2,4,$error_message);
        $text_validator->validate($input);
        $this->assertTrue($text_validator->has_errors(),'error validating non-integer text input ('.htmlentities($input['text']).')');
        $text_errors = $text_validator->get_errors();
        $this->assertEqual(count($text_errors),1,'wrong number of errors returned for text input');
        $this->assertEqual($text_errors[0]->get_message(),$error_message,'error text unexpected');

        // floating-point number 
        $float_validator =& new IntegerRangeValidator('float',2,4,$error_message);
        $float_validator->validate($input);
        $this->assertTrue($float_validator->has_errors(),'error validating float input ('.htmlentities($input['float']).')');
        $float_errors = $float_validator->get_errors();
        $this->assertEqual(count($float_errors),1,'wrong number of errors returned for float input');
        $this->assertEqual($float_errors[0]->get_message(),$error_message,'error float unexpected');

        // empty string
        $empty_validator =& new IntegerRangeValidator('empty',2,4,$error_message);
        $empty_validator->validate($input);
        $this->assertTrue($empty_validator->has_errors(),'error validating empty input ('.htmlentities($input['empty']).')');
        $empty_errors = $empty_validator->get_errors();
        $this->assertEqual(count($empty_errors),1,'wrong number of errors returned for empty input');
        $this->assertEqual($empty_errors[0]->get_message(),$error_message,'error empty unexpected');

        // below range
        $one_validator =& new IntegerRangeValidator('one',2,4,$error_message);
        $one_validator->validate($input);
        $this->assertTrue($one_validator->has_errors(),'error validating one input ('.htmlentities($input['one']).')');
        $one_errors = $one_validator->get_errors();
        $this->assertEqual(count($one_errors),1,'wrong number of errors returned for one input');
        $this->assertEqual($one_errors[0]->get_message(),$error_message,'error one unexpected');

        // above range
        $five_validator =& new IntegerRangeValidator('five',2,4,$error_message);
        $five_validator->validate($input);
        $this->assertTrue($five_validator->has_errors(),'error validating five input ('.htmlentities($input['five']).')');
        $five_errors = $five_validator->get_errors();
        $this->assertEqual(count($five_errors),1,'wrong number of errors returned for five input');
        $this->assertEqual($five_errors[0]->get_message(),$error_message,'error five unexpected');

    }//test_validate

}//class IntegerRangeValidatorTester
?>
