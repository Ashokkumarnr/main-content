<?php

require_once('ErrorSet.php');

/**
 *
 */
class ValidationSet extends ErrorSet {

    //////////////////////////////////////////////////////////// 
    // Private instance variables 
    //////////////////////////////////////////////////////////// 

    /** the child validators to run */
    var $children;


    //////////////////////////////////////////////////////////// 
    // Constructors 
    //////////////////////////////////////////////////////////// 

    /**
     *
     */
    function ValidationSet() {
    }//ValidationSet


    //////////////////////////////////////////////////////////// 
    // Public instance methods 
    //////////////////////////////////////////////////////////// 

    /**
     * Append the given validator to the end of this validator's 
     * children
     *
     * @param ValidationSet $child a validator to be added as a child
     */
    function append_child( &$child ) {
        $this->children[] =& $child;
    }//append_child


    /**
     * By default, just call the validation functions of all 
     * children.
     *
     * @param array $input an associative array of input
     */
    function validate($input) {
        $num_children = count($this->children);
        for( $i=0 ; $i < $num_children ; $i++ ) {
            $child =& $this->children[$i];
            $child->validate($input);
            if( $child->has_errors() ) {
                $this->has_errors = true;
            }//if
        }//for
    }//validate


    /**
     *
     */
    function get_children() {
        return $this->children;
    }//get_children

}//class ValidationSet

?>
