<?php

require_once('InputValidator.php');

/**
 * This validator only allows integer input
 */
class IntegerValidator extends InputValidator {

    //////////////////////////////////////////////////////////// 
    // Constructors
    //////////////////////////////////////////////////////////// 

    /**
     * Create a new IntegerValidator with the given field name and
     * the given error message (optionally).
     *
     * @param string $field_name    the name of the input to check
     * @param string $error_message (optional) the error message to use
     */
    function IntegerValidator( $field_name,
                               $error_message='Please enter a whole number' ) {
        $this->InputValidator($field_name,$error_message);
    }//IntegerValidator


    //////////////////////////////////////////////////////////// 
    // Public instance methods 
    //////////////////////////////////////////////////////////// 

    /** 
     * If there's a value for our field in the given input, make
     * sure it's an integer
     *
     * @param array $input the input to validate
     */
    function validate($input) {
        $integer_regex = '/^[-]?\d+$/';
        if ( isset($input[$this->get_field_name()]) && 
             !preg_match($integer_regex,$input[$this->get_field_name()]) ) {
            $error = new ValidationError($this->error_message);
            $this->add_error($error);
        }//if
    }//validate


    /** 
     * This is the Prototype method for Validators.  This returns a new
     * IntegerValidator with the same field name and error message as 
     * this instance.
     */
    function &php4_compat_clone() {
        return new IntegerValidator(
            $this->get_field_name(),
            $this->get_error_message()
        );
    }//php4_compat_clone
}//class IntegerValidator
?>
