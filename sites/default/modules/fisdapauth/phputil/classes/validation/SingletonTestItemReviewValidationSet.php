<?php

require_once('InputValidationSet.php');
require_once('RequiredFieldValidator.php');
require_once('IntegerRangeValidator.php');
require_once('StringWhitelistValidator.php');
require_once('phputil/classes/model/TestCreationProject.php');


class SingletonTestItemReviewValidationSet extends InputValidationSet {
    function SingletonTestItemReviewValidationSet() {
        // add the required question validators
        $required_fields = $this->get_required_fields();
        $this->add_common_validator(new RequiredFieldValidator(''),$required_fields);

        $this->add_yes_no_validators();
        $this->add_assignment_review_type_validator();
    }//SingletonTestItemReviewValidationSet


    /**
     * 
     * @todo provide clients such as this function a clean way to query the 
     *       valid assignment review settings (so we don't have to update 
     *       this code for every additional constant)
     */
    function add_assignment_review_type_validator() {
        $validator =& new StringWhitelistValidator('AssignmentReviewType'); 
        $validator->set_whitelist(
            array( 
                ''.DEFAULT_ASSIGNMENT_REVIEW, 
                ''.SINGLETON_ASSIGNMENT_REVIEW
            )
        );
        $this->append_child($validator);
    }//add_assignment_review_type_validator


    function add_yes_no_validators() {
        $NO_VALUE = 0;
        $YES_VALUE = 1;
        $this->add_common_validator(
            new IntegerRangeValidator(
                '',
                $NO_VALUE,
                $YES_VALUE,
                'Invalid input received.  Please select either yes or no.'
            ),
            $this->get_yes_no_fields()
        );
    }//add_yes_no_validators


    function get_required_fields() {
        return array(
            'id',
            'item_id',
            'number_in_group',
            'reviewer',
            'stem_grammar_correct',
            'distractors_match_grammar',
            'distractors_less_correct',
            'distractors_similar_yet_distinct',
            'answer_always_correct',
            'item_free_from_complication',
            'item_unbiased'
        );
    }//get_required_fields


    function get_yes_no_fields() {
        return array(
            'stem_grammar_correct',
            'distractors_match_grammar',
            'distractors_less_correct',
            'distractors_similar_yet_distinct',
            'answer_always_correct',
            'item_free_from_complication',
            'item_unbiased'
        );
    }//get_yes_no_fields
}//class SingletonTestItemReviewValidationSet
?>
