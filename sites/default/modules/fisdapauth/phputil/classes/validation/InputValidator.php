<?php

require_once('Validator.php');

/** 
 * This class is used to validate inputs from form submissions.
 */
class InputValidator extends Validator {
    
    //////////////////////////////////////////////////////////// 
    // Private instance variables 
    //////////////////////////////////////////////////////////// 

    /** the name of the form field to validate */
    var $field_name;

    /** the error message to use when validation fails */
    var $error_message;


    //////////////////////////////////////////////////////////// 
    // Constructors 
    //////////////////////////////////////////////////////////// 

    /**
     * Create an InputValidator for the given form field
     *
     * @param string $field_name    the name of the field to validate
     * @param string $error_message (optional) the error message to use if validation fails
     */
    function InputValidator( $field_name, $error_message='' ) {
        $this->field_name = $field_name;
        $this->error_message = $error_message;
    }//InputValidator 



    //////////////////////////////////////////////////////////// 
    // Public instance methods 
    //////////////////////////////////////////////////////////// 

    /**
     * Returns the field name we're validating
     * 
     * @return string the field name we're validating
     */
    function get_field_name() {
        return $this->field_name;
    }//get_field_name


    /** 
     * Set the field name to the given string.
     *
     * @param string $name the field name to validate
     */
    function set_field_name($name) {
        $this->field_name = $name;
    }//set_field_name


    /** 
     *
     */
    function get_error_message() {
        return $this->error_message;
    }//get_error_message
}//class InputValidator
?>
