<?php

require_once('InputValidator.php');

/**
 * This validator only allows integer input
 * within a given range
 */
class IntegerRangeValidator extends InputValidator {

    //////////////////////////////////////////////////////////// 
    // Private instance variables
    //////////////////////////////////////////////////////////// 

    /** the minimum acceptable input */
    var $min;

    /** the maximum acceptable input */
    var $max;


    //////////////////////////////////////////////////////////// 
    // Constructors
    //////////////////////////////////////////////////////////// 

    /**
     * Create a new IntegerRangeValidator with the given field name,
     * the given min, the given max,  and the given error message 
     * (optionally).
     *
     * @param string $field_name    the name of the input to check
     * @param string $error_message (optional) the error message to use
     */
    function IntegerRangeValidator( $field_name, 
                                    $min, 
                                    $max, 
                                    $error_message='') {
        if ( !$error_message ) {
            $error_message = 'Please enter a whole number between '.$min.
                             ' and '.$max;
        }//if
        $this->InputValidator($field_name,$error_message);
        $this->min = $min;
        $this->max = $max;
    }//IntegerRangeValidator


    //////////////////////////////////////////////////////////// 
    // Public instance methods 
    //////////////////////////////////////////////////////////// 

    /** 
     * If there's a value for our field in the given input, make
     * sure it's an integer, and that it's in the specified range
     *
     * @param array $input the input to validate
     */
    function validate($input) {
        $integer_regex = '/^[-]?\d+$/';
        if ( isset($input[$this->get_field_name()]) && 
             (!preg_match($integer_regex,$input[$this->get_field_name()]) ||
              $input[$this->get_field_name()] < $this->get_min() ||
              $input[$this->get_field_name()] > $this->get_max()) ) {
            $error = new ValidationError($this->error_message);
            $this->add_error($error);
        }//if
    }//validate


    /** 
     * Return the minimum acceptable integer
     */
    function get_min() {
        return $this->min;
    }//get_min


    /** 
     * Return the maximum acceptable integer
     */
    function get_max() {
        return $this->max;
    }//get_max


    /** 
     * This is the Prototype method for Validators.  This returns a new
     * IntegerRangeValidator with the same field name and error message as 
     * this instance.
     */
    function &php4_compat_clone() {
        return new IntegerRangeValidator(
            $this->get_field_name(),
            $this->get_min(),
            $this->get_max(),
            $this->get_error_message()
        );
    }//php4_compat_clone
}//class IntegerRangeValidator
?>
