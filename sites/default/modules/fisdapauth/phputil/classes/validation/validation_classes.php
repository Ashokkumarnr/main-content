<?php

/**
 * This file depends on all of the validation classes, for convenience.
 * Scripts needing access to the validation classes should require or 
 * include this file instead of requiring the individual files directly.
 */

// make sure the FISDAP_ROOT constant is set
if ( !defined('FISDAP_ROOT') ) {
    die($_SERVER['PHP_SELF'].': FISDAP_ROOT constant not set.  Exiting.');
}//if

require_once(FISDAP_ROOT.'phputil/classes/validation/ErrorSet.php');
require_once(FISDAP_ROOT.'phputil/classes/validation/Validator.php');
require_once(FISDAP_ROOT.'phputil/classes/validation/InputValidator.php');
require_once(FISDAP_ROOT.'phputil/classes/validation/ValidationSet.php');
require_once(FISDAP_ROOT.'phputil/classes/validation/InputValidationSet.php');
require_once(FISDAP_ROOT.'phputil/classes/validation/IntegerValidator.php');
require_once(FISDAP_ROOT.'phputil/classes/validation/IntegerRangeValidator.php');
require_once(FISDAP_ROOT.'phputil/classes/validation/ValidationError.php');
require_once(FISDAP_ROOT.'phputil/classes/validation/StringWhitelistValidator.php');
require_once(FISDAP_ROOT.'phputil/classes/validation/RequiredFieldValidator.php');

?>
