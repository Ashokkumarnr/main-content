<?php

require_once('InputValidator.php');

/**
 * This validator requires that the input contain the 
 * given field
 */
class RequiredFieldValidator extends InputValidator {

    //////////////////////////////////////////////////////////// 
    // Constructors
    //////////////////////////////////////////////////////////// 

    /**
     * Create a new RequiredFieldValidator with the given field name and
     * the given error message (optionally).
     *
     * @param string $field_name    the name of the input to check
     * @param string $error_message (optional) the error message to use
     */
    function RequiredFieldValidator( $field_name,
                                     $error_message='This is a required field' ) {
        $this->InputValidator($field_name,$error_message);
    }//RequiredFieldValidator


    //////////////////////////////////////////////////////////// 
    // Public instance methods 
    //////////////////////////////////////////////////////////// 

    /** 
     * Make sure the input has an entry for this field
     *
     * @param array $input the input to validate
     */
    function validate($input) {
        if ( !isset($input[$this->get_field_name()]) ) {
            $error = new ValidationError($this->error_message);
            $this->add_error($error);
        }//if
    }//validate


    /** 
     * This is the Prototype method for Validators.  This returns a new
     * RequiredFieldValidator with the same field name and error message as 
     * this instance.
     */
    function &php4_compat_clone() {
        return new RequiredFieldValidator(
            $this->get_field_name(),
            $this->get_error_message()
        );
    }//php4_compat_clone
}//class RequiredFieldValidator
?>
