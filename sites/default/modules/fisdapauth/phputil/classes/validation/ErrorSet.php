<?php

class ErrorSet {

    //////////////////////////////////////////////////////////// 
    // Private instance variables 
    //////////////////////////////////////////////////////////// 

    /** the errors encountered during validation */
    var $errors;

    /** true iff there were validation errors */
    var $has_errors;


    //////////////////////////////////////////////////////////// 
    // Constructors 
    //////////////////////////////////////////////////////////// 

    /** 
     *
     */
    function ErrorSet() {
    }//ErrorSet


    //////////////////////////////////////////////////////////// 
    // Public instance methods 
    //////////////////////////////////////////////////////////// 

    /** 
     * Add the given ValidationError to the list of errors
     *
     * @param ValidationError $error an error to append to the list
     */
    function add_error( &$error ) {
        $this->has_errors = true;
        $this->errors[] =& $error;
    }//add_error


    /** 
     * Returns a list of ValidationErrors, keyed by input name.
     *
     * @return array a list of ValidationErrors
     */
    function get_errors() {
        return $this->errors;
    }//get_errors


    /**
     * Returns true iff there were validation errors.
     *
     * @return bool true iff there were validation errors
     */
    function has_errors() {
        return $this->has_errors;
    }//has_errors
}//class ErrorSet

?>
