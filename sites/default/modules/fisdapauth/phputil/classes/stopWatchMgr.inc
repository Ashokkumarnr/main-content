<?php
require_once('phputil/classes/stopWatch.inc');

/**
 * This class manages stop watches.
 * <pre>
 * DEVELOPERS - ONLY BASIC FUNCTIONALITY HAS BEEN TESTED HERE.
 *
 * It provides an easy way to hold onto them in a static way so that they
 * can be used, for instance, each time a set of code is entered, i.e. a
 * method inside a class or object, a loop inside a class method, etc.
 * 
 * Typical usage:
 *
 *    function foo() {
 *      // Do NOT reuse old timer 
 *      $timer = StopWatchMgr::create('foo', true);     // auto starts
 *      ...
 *      $timer->stop();
 *    }
 *
 *    function foo() {
 *      // Reuse old timer for acummulation
 *      $timer = StopWatchMgr::retrieve('foo');
 *      for (...) {
 *        $timer->start();
 *        ...
 *        $timer->stop();
 *      }
 *    }
 * </pre>
 */
class StopWatchMgr
{
    private static $timers = array();

    /**
     * Constructor is hidden.
     */
	private function __construct() {
    }

    /**
     * Always create a brand new timer.
     * @param string $name The timer name.
     * @param boolean $start TRUE if the timer should be started.
     * @return StopWatch The timer.
     */
    public static function create($name, $start=false) {
        if (isset(self::$timers[$name])) {
            unset(self::$timers[$name]);
        }

        $timer = new StopWatch($name, $start);
        self::$timers[$name] = $timer;

        return $timer;
    }

    /**
     * Retrieve an existing time if possible, otherwise create one.
     * @param string $name The timer name.
     * @param boolean $start TRUE if the timer should be started.
     * @return StopWatch The timer.
     */
    public static function retrieve($name, $start=false) {
        if (isset(self::$timers[$name])) {
            $timer = self::$timers[$name];
            if ($start) {
                $timer->start();
            }
        }
        else {
            $timer = self::create($name, $start);
        }

        return $timer;
    }

    /**
     * Retrieve the timer output.
     * @return array Each element is a timer __toString() value.
     */
    public static function get_output() {
        $output = array();

        // Sort based on name.
        $names = array_keys(self::$timers);
        sort($names, SORT_STRING);

        foreach ($names as $name) {
            $output[] = self::$timers[$name]->__toString();
        }

        return $output;
    }

    /**
     * Stop all the timers.
     * If not running, nothing is done.
     */
    public static function stop_all() {
        foreach (self::$timers as $timer) {
            $timer->stop();
        }
    }
}
?>
