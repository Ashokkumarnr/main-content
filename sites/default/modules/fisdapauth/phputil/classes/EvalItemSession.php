<?php

/**
 * This class models a FISDAP Eval Item Session
 */
class FISDAP_EvalItemSession extends DataEntryDbObj {    

    /** 
     * Create a new Item Session with the specified id and the specified db link
     */
    function FISDAP_EvalItemSession( $id, $connection ) {
        $this->DbObj('Eval_ItemSessions',                                       //table name
                     $id,                                                       //id
                     'ItemSession_id',                                          //id field
                     'Shift',                                                   //parent entity name
                     array(),                                                   //children
                     $connection );                                             //database connection
        $field_map = array(
            'EvalSession_id' => 'EvalSession_id',  
            'ItemSession_id' => 'ItemSession_id',
            'Item_id' => 'ItemDef_id',     
            'Score' => 'Score',          
            'NonCritCount' => 'NonCritCount',   
            'Comment_id' => 'Comment_id',     
            'Timestamp' => 'Timestamp'      
            ); 
        $this->set_field_map( $field_map );
    }//constructor FISDAP_EvalItemSession
}//class FISDAP_EvalItemSession

?>
