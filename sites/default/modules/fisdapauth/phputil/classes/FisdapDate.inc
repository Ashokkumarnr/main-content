<?php
require_once('phputil/classes/Assert.inc');
require_once('phputil/classes/SqlValue.inc');
require_once('phputil/classes/common_utilities.inc');

/*
 * Holds a calendar date.
 * This holds default SQL dates like '0000-00-00' correctly.
 */
final class FisdapDate implements SqlValue {
	private $day;
	private $month;
    private $mutable;
	private $year;

    /**
     * Return the today.
     * @return FisdapDate Today.
     */
    public static function today() {
        return new FisdapDate();
    }

    /**
     * Retrieve the date as meaning "no date."
     * @return FisdapDate The date.
     */
    public static function not_set() {
        return FisdapDate::create_from_ymd_string('0000-00-00');
    }

    /**
     * Create from Unix timestamp.
     * @param int $timestamp The Unix timestamp.
     * @return FisdapDate The date.
     */
    public static function create_from_timestamp($timestamp) {
        Assert::is_int($timestamp);

        return FisdapDate::create_from_ymd_string(date('Y-m-d', $timestamp));
    }

    /**
     * Create from a year, month, and a day.
     * @param int $year The year.
     * @param int $month The month, 1-12.
     * @param int $day The day, 1-31.
     * @return FisdapDate The date.
     */
    public static function create_from_ymd($year, $month, $day) {
        Assert::is_int($year);
        Assert::is_int($month);
        Assert::is_int($day);

        return new FisdapDate($year, $month, $day);
    }

    /**
     * Create from a ymd string.
     * @param string $ymd Y-M-D.
     * @return FisdapDate The date.
     */
    public static function create_from_ymd_string($ymd) {
        Assert::is_not_empty_trimmed_string($ymd);

        return new FisdapDate(trim($ymd));
    }

    /**
     * Create from a SQL string.
     * @param string | null $s Y-M-D or null.
     * @return FisdapDate The date.
     */
    public static function create_from_sql_string($s) {
		Assert::is_true(
			Test::is_null($s) || 
			Test::is_not_empty_trimmed_string($s));

		if (is_null($s)) return self::not_set();
        return new FisdapDate(trim($s));
    }

    /**
     * Create from script values.
	 * The month is 1-12.
     * @param string $prefix The value prefix, i.e. startdate_.
	 * @param boolean $check_day TRUE if the day field is checked.
     * @return FisdapDate The date.
     */
    public static function create_from_script($prefix, $check_day=true) {
		Assert::is_string($prefix);
		Assert::is_boolean($check_day);

        // Sometimes different pieces of the date are not needed,
        // so we allow this.
        $month_set = false;
		if (common_utilities::is_scriptvalue($prefix . 'month')) {
			$month = common_utilities::get_scriptvalue($prefix . 'month'); 
			if (Test::is_int($month)) {
				$month_set = true;
				$month = Convert::to_int($month);
			}
		}

		$day_set = false;
		if ($check_day) {
			if (common_utilities::is_scriptvalue($prefix . 'day')) {
				$day = common_utilities::get_scriptvalue($prefix . 'day');
				if (Test::is_int($day)) {
					$day_set = true;
					$day = Convert::to_int($day);
				}
			}
		}
		else {
			$day = 1;
			$day_set = true;
		}
		
		$year_set = false;
		if (common_utilities::is_scriptvalue($prefix . 'year')) {
            $year = common_utilities::get_scriptvalue($prefix . 'year');
			if (Test::is_int($year)) {
				$year_set = true;
				$year = Convert::to_int($year);
			}
		}

		if ($month_set && $day_set && $year_set) {
			$date = new FisdapDate($year, $month, $day);
		}
		else {
			$date = FisdapDate::not_set();
		}

		return $date;
    }

	/** 
	 * Constructor.
     * @param int|string|null The year, Y-M-D, or null for today.
     * @param int|null The month.
     * @param int|null The day.
	 */
	public function __construct($year=null, $month=null, $day=null) {
		if (is_null($year)) {
			$this->set_year(date('Y'));
			$this->set_month(date('n'));
			$this->set_day(date('j'));
		}
		else if (is_null($month)) {
            Assert::is_not_empty_trimmed_string($year);
            $year = trim($year);

            $pieces = split('-', $year);
            Assert::is_true(count($pieces) == 3);

			$this->set_year($pieces[0]);
			$this->set_month($pieces[1]);
			$this->set_day($pieces[2]);
		}
		else {
			$this->set_year($year);
			$this->set_month($month);
			$this->set_day($day);
		}
	}

	public function __toString() {
		return 'FisdapDate[' . $this->get_MySQL_date() . ']';
	}

    /**
     * Is this a leap year?
     * @return boolean TRUE if this is a leap year.
     */
    public function is_leap_year() {
        if ($this->year % 4) return false;
        if ($this->year % 100) return true;
        return !($this->year % 400);
    }

    /**
     * How many days are in the month?
     * @return int The number of days in the month.
     */
    public function get_days_in_month() {
        static $days = array(31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

        if ($this->month != 2) return $days[$this->month-1];
        if ($this->is_leap_year()) return 29;
        return 28;
    }

	/**
	 * Determine if this object is set.
	 * @return boolean TRUE if the date has been set.
	 */
	public function is_set() {
        return $this->year && $this->month && $this->day;
	}

    /**
     * The internal date has changed.
     */
    private function date_changed() {
        // If any field is 0, we *assume* a SQL date and no operations can be
        // done on the date.
        $this->mutable = $this->is_set();
    }

    /**
     * Retrieve the date formatted for SQL.
     * @return $string The date as 'Y:m:d'
     */
    public function get_MySQL_date() {
		if ($this->is_set()) return $this->get_date();
		return '0-0-0';
    }

	/**
	 * Retrieve the date formatted for a UI.
	 * @return string The date as Y/M/D.
	 */
	public function get_ui_ymd() {
		if (!$this->is_set()) return '0/0/0';

		return $this->get_year() . '/' . $this->get2digits($this->month) .
			'/' . $this->get2digits($this->day);
	}

	private function get2digits($n) {
		if ($n < 10) return "0$n";
		return $n;
	}

	/**
	 * @todo document
	 * @todo test
	 */
	public function get_timestamp() {
		return strtotime($this->get_date());
	}

	/**
	 * @todo document
	 * @todo test
	 */
	public function get_formatted_date($format) {
		return date($format, $this->get_timestamp());
	}

	public function get_as_sql_value() {
		return $this->get_MySQL_date();
	}

    /**
     * Retrieve the date as a string.
     * @return $string The date as 'Y-m-d'
     */
	public function get_date() {
		return $this->year . '-' . $this->month . '-' . $this->day;
	}

    /**
     * Retrieve the year.
     * @return int The year.
     */
	public function get_year() {
		return $this->year;
	}

    /**
     * Retrieve the month.
     * @return int The month 1-12.
     */
	public function get_month() {
		return $this->month;
	}

    /**
     * Retrieve the day.
     * @return int The day 1-31.
     */
	public function get_day() {
		return $this->day;
	}

	private	function set_year($year) {
        Assert::is_int($year);

        $year = Convert::to_int($year);

        Assert::is_true(($year == 0) ||
            ($year == date('Y', mktime(0, 0, 0, 1, 1, $year))));

        $this->year = $year;
        $this->date_changed();
	}

	/**
	 * Should be called after {@link set_year()}.
	 */
	private function set_month($month) {
        Assert::is_int($month);

        $month = Convert::to_int($month);

        Assert::is_true(($month == 0) ||
            ($month == date('n', mktime(0, 0, 0, $month, 1, $this->get_year()))));

        $this->month = $month;
        $this->date_changed();
	}

	/**
	 * Should be called after {@link set_month()}.
	 */
	private function set_day($day) {
        Assert::is_int($day);

        $day = Convert::to_int($day);

        Assert::is_true(($day == 0) ||
            ($day == date('j', mktime(0, 0, 0, $this->get_month(), $day, $this->get_year()))));

        $this->day = $day;
        $this->date_changed();
	}

	/**
	 * Change the date by a given offset of years, months, or days.
	 * Offsets may be positive or negative.
	 *
	 * @param int $y The number of years to change by
	 * @param int $m The number of months to change by
	 * @param int $d The number of days to change by
	 * */
	public function change_date($y, $m, $d) {
        Assert::is_int($y);
        Assert::is_int($m);
        Assert::is_int($d);

        if (!$this->mutable) return;

		$stamp = mktime(0, 0, 0,
			$this->get_month() + $m,
			$this->get_day() + $d,
			$this->get_year() + $y);

		$this->set_year(date("Y", $stamp));
		$this->set_month(date("m", $stamp));
		$this->set_day(date("d", $stamp));
	}

	/**
	 * Change the date by the given number of years.
	 * @param int $offset The number of years to change by.
	 * @todo I think weird things are happening with large numbers here - 
	 * needs testing and debugging
	 * */
	public function change_year($offset) {
        Assert::is_int($offset);
		$this->change_date($offset, 0, 0);
	}

	/**
	 * Change the date by the given number of months.
	 * @param int $offset The number of months to change by.
	 * */
	public function change_month($offset) {
        Assert::is_int($offset);
		$this->change_date(0, $offset, 0);
	}

	/**
	 * Change the date by the given number of days.
	 * @param int $offset The number of days to change by.
	 * */
	public function change_day($offset) {
        Assert::is_int($offset);
		$this->change_date(0, 0, $offset);
	}

    /**
     * Compare this date with another.
     * @param FisdapDate $date The other date.
     * @return int Like a normal compare method.
     */
	public function compare($date) {
        Assert::is_a($date, 'FisdapDate');

		$diff = $this->get_year() - $date->get_year();
		if ($diff != 0) return $diff;

		$diff = $this->get_month() - $date->get_month();
		if ($diff != 0) return $diff;

		return $this->get_day() - $date->get_day();
	}

    /**
     * A comparator.
     * @param FisdapDate|string|null $a The first date.
     * @param FisdapDate|string|null $b The first date.
     * @return int Like a normal compare method.
     */
	public static function date_comparator($a, $b) {
        $a = Convert::to_a($a, 'FisdapDate');
        $b = Convert::to_a($b, 'FisdapDate');

		return $a->compare($b);
	}

	/**
	 * Retrieve a month name.
	 * @param int $month The month.
	 * @param int $offset The month for January.
	 * @return string The month name.
	 */
	public static function get_short_month_name($month, $offset=1) {
		static $names = array(
			'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
			'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

		Assert::is_int($month);
		Assert::is_int($offset);

		$i = $month - $offset;
		Assert::is_true($i >= 0);
		Assert::is_true($i < 12);

		return $names[$month-$offset];
	}

	/**
	 * Retrieve a long month name.
	 * @param int $month The month.
	 * @param int $offset The month for January.
	 * @return string The month name.
	 */
	public static function get_long_month_name($month, $offset=1) {
		static $names = array(
			'January', 'February', 'March', 'April', 'May', 'June',
			'July', 'August', 'September', 'October', 'November', 'December');

		Assert::is_int($month);
		Assert::is_int($offset);

		$i = $month - $offset;
		Assert::is_true($i >= 0);
		Assert::is_true($i < 12);

		return $names[$month-$offset];
	}
}
?>
