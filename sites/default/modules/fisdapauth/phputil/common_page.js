/*
 * common_page.js
 *
 * @author Warren Jacobson
 */

// Initialize the global
var global_state_parameters = new Hash();

/**
 * @author Eryn O'Neil
 * @todo hash.key = value doesn't work in prototype 1.6. Change it to hash.set(key, value) if we upgrade.
 */

function setAjaxState(key, value) {

	global_state_parameters[key] = value;
}

function ajaxSubmitHandler(id, caller) {

	var id;
	var dispatch_id;
	var scriptname;
	var parameters;
	var options;

	parameters = $H($(id).serialize(true));
	parameters.merge({ajax_common_form: 'ajaxSubmitHandler', id: id, formid: id});
	// Add the button to the submitted values
	parameters[caller.name] = caller.value;

    // Find the dispatch ID.
    var elements = document.getElementById(id).elements;
    for (var i=0, len=elements.length; i<len; i++){
        if (elements[i].name == 'dispatch_id') {
            dispatch_id = elements[i].value;
            break;
        }
    }

	var before_dispatch_function = function() {

		// Hide the buttons
		footerDiv = $(id + '_footer');
		buttons = $A(footerDiv.getElementsByClassName('button'));
		buttons.each( function(button) {
				button.hide();
				});

		// Show the throbber
	    $(id + '_throbber').style.display = "inline";

		// Disable all the elements
	    var elements = document.getElementById(id).elements;
	    for (var i=0, len=elements.length; i<len; i++){
		    elements[i].disabled = true;
	    }

		// Something to do with checklists
	    var allElements = document.getElementById(id).getElementsByTagName("*");
	    for (var i=0, len=allElements.length; i<len; i++) {
		    if (allElements[i].className == 'promptEnable') {
			    allElements[i].style.display = "none";
		    }
		    if (allElements[i].className == 'promptDisable') {
			    allElements[i].style.display = "inline";
		    }
	    }
    };

	scriptname = document.getElementById(id).action;
	options = null;
	respond_after_function = null;
    respond_before_function = null;

	dispatchHandler(dispatch_id,scriptname,parameters,options,
        respond_before_function,respond_after_function,before_dispatch_function);
}

function promptRespondHandler(promptid,formid) {
	var promptid;
	var formid;
	var options;

	var doNotEnableList = new Array();
	var doNotEnableIdx = 0;


	prompt_inputs = $(promptid + '_div').getElementsBySelector('input', 'select', 'textarea');
	parameters = $H(Form.serializeElements(prompt_inputs, true));

	scriptname = $(formid).action;

	parameters.merge({ajax_common_form: 'promptHandler', id: promptid, formid: formid});

	$(promptid + '_throbber').style.display = "block";

	var elements = $(formid).elements;
	for (var i=0, len=elements.length; i<len; i++) {
		if (elements[i].disabled == true) {
			doNotEnableList[doNotEnableIdx] = elements[i].id; doNotEnableIdx++;
		}
		else {
			elements[i].disabled = true;
		}
		if (elements[i].name == 'dispatch_id') {
			dispatch_id = elements[i].value;
		}
	}

	respond_before_function = function(dataArray) {
		var elements = $(formid).elements;
		for (var i = 0, leni = elements.length; i < leni; i++) {
			var foundElement = false;
			for (var j = 0, lenj = doNotEnableList.length; j < lenj; j++) {
				if (elements[i].id == doNotEnableList[j]) {
					foundElement = true;
				}
			}
			if (!foundElement) {
				elements[i].disabled = false;
			}
		} 
	}

	respond_after_function = function(dataArray) {
		$(promptid + '_throbber').style.display = "none";
	}

	dispatchHandler(dispatch_id,scriptname,parameters,options,respond_before_function,respond_after_function);


}
function promptHandler(id,baseid,formid,options) {

	var id;
	var baseid;
	var formid;
	var options;
	var dispatch_id;
	var submittedinput;

	var doNotEnableList = new Array();
	var doNotEnableIdx = 0;

	document.getElementById(baseid + '_throbber').style.display = "block";

	var elements = document.getElementById(formid).elements;
	for (var i=0, len=elements.length; i<len; i++) {
		if (elements[i].disabled == true) {
			doNotEnableList[doNotEnableIdx] = elements[i].id; doNotEnableIdx++;
		}
		else {
			elements[i].disabled = true;
		}
		if (elements[i].name == 'dispatch_id') {
			dispatch_id = elements[i].value;
		}
	}
	buttonFormId = formid + 'Buttons';
	buttonElements = document.getElementById(buttonFormId).elements;
	for (var i=0, len=buttonElements.length; i<len; i++) {
		if (buttonElements[i].className == 'button') {
			buttonElements[i].disabled = true;
		}
	}
	var allElements = document.getElementById(formid).getElementsByTagName("*");
	for (var i=0, len=allElements.length; i<len; i++) {
		if (allElements[i].className == 'promptEnable') {
			allElements[i].style.display = "none";
		}
		if (allElements[i].className == 'promptDisable') {
			allElements[i].style.display = "inline";
		}
	}

	submittedinput = $F(id);

	scriptname = document.getElementById(formid).action;
	options = null;
	parameters = $H({ajax_common_form: 'promptHandler', id: id, baseid: baseid, value: submittedinput, options: options, formid: formid});

	respond_before_function = function(dataArray) {
		var elements = document.getElementById(id).form.elements;
		for (var i = 0, leni = elements.length; i < leni; i++) {
			var foundElement = false;
			for (var j = 0, lenj = doNotEnableList.length; j < lenj; j++) {
				if (elements[i].id == doNotEnableList[j]) {
					foundElement = true;
				}
			}
			if (!foundElement) {
				elements[i].disabled = false;
			}
		} 
		buttonFormId = formid + 'Buttons';
		buttonElements = document.getElementById(buttonFormId).elements;
		for (var i=0, len=buttonElements.length; i<len; i++) {
			if (buttonElements[i].className == 'button') {
				buttonElements[i].disabled = false;
			}
		}
		var allElements = document.getElementById(formid).getElementsByTagName("*");
		for (var i=0, len=allElements.length; i<len; i++) {
			if (allElements[i].className == 'promptEnable') {
				allElements[i].style.display = "inline";
			}
			if (allElements[i].className == 'promptDisable') {
				allElements[i].style.display = "none";
			}
		}
	}

	respond_after_function = function(dataArray) {
		document.getElementById(baseid + '_throbber').style.display = "none";
	}

	dispatchHandler(dispatch_id,scriptname,parameters,options,respond_before_function,respond_after_function);

}

// Declare this as a stub so nothing breaks if it is called inappropriately
var uploadHandler = function() { }

function formSubmitHandler(id,formid) {
	// Essentially a wrapper for submitHandlerAjaxSubmit() that first checks for file
	// upload prompts and takes action as necessary

	var id;
	var formid;

	var iframe_name = 'upload_iframe';
	if (document.getElementById(iframe_name) == null) {
		// If there is no iframe, we submit the form normally
		submitHandlerAjaxSubmit(id,formid);
	} else {
		// If there is an iframe, that means the form had file uploading, handle that first

		var currform = document.getElementById(formid);

		// Alter the form elements to submit everything to the iframe
		// The uploadhandler will ignore the rest of the form elements, but we don't
		// need to save the old settings here because our next submit is AJAXy anyways
		currform.action = currform.uploadprompt_handler.value;
		currform.target = iframe_name;
		currform.method = 'POST';
		currform.enctype = "multipart/form-data";
		currform.submit();

		// The page that receives the file will call uploadHandler() when it is finished.
		uploadHandler = function() { submitHandlerAjaxSubmit(id,formid); }
	}
}

function submitHandlerAjaxSubmit(id,formid) {

	var id;
	var formid;
	var dispatch_id;
	var scriptname;
	var parameters;
	var options;

	parameters = $H($(formid).serialize(true));
	parameters.merge({ajax_common_form: 'formSubmitHandler', id: id, formid: formid});

	var elements = document.getElementById(formid).elements;
	for (var i=0, len=elements.length; i<len; i++) {
		if (elements[i].name == 'dispatch_id') {
			dispatch_id = elements[i].value;
		}
	}

	scriptname = document.getElementById(id).form.action;
	options = null;
	respond_before_function = null;

	respond_after_function = function(dataArray) {
		$(id).onclick = null;
		$(id).click();
	}

	dispatchHandler(dispatch_id,scriptname,parameters,options,respond_before_function,respond_after_function);

	return true;
}

function checklistHandler(id,action) {

	var id;
	var allOrNone;
	var idx;
	var selected;

	idx = 0; selected = 0;
	while (document.getElementById(id + '_' + idx)) {
        var e = document.getElementById(id + '_' + idx);
		if (action == 'All') {
			if (!e.disabled) e.checked = true;
		}
		else if (action == 'None') {
			if (!e.disabled) e.checked = false;
		}

        if (e.checked) selected = selected + 1;
		idx = idx + 1;
	}
	document.getElementById(id + '_selected_enabled').innerHTML = selected;
	document.getElementById(id + '_selected_disabled').innerHTML = selected;
}

function linkHandler(dispatch_id,scriptname,parameters) {

	var dispatch_id;
	var scriptname;
	var parameters;

	var options = null;
	var respond_before_function = null;
	var respond_after_function = null;

	dispatchHandler(dispatch_id,scriptname,$H(parameters),options,respond_before_function,respond_after_function);
}

/**
 * Make an AJAX request.
 * @param before_dispatch_function A function (name or null) to call before
 * dispatching.
 */
function dispatchHandler(dispatch_id,scriptname,parameters,options,
    respond_before_function,respond_after_function,before_dispatch_function) {

	var dispatch_id;
	var scriptname;
	var parameters;
	var options;


    if (before_dispatch_function != null) {
        before_dispatch_function();
    }

	if (scriptname == null) {
		scriptname = default_scriptname;
	}

	if (parameters == null) {
		parameters = $H();
	}

	parameters.merge({ajax: 'dispatchHandler', dispatch_id: dispatch_id});

	// Give values supplied in parameters priority over any values in global_state_parameters with the same key
	
	state_keys = global_state_parameters.keys();
	parameter_keys = parameters.keys();

	cleaned_state_keys = new Hash();
	for (currentkeyindex = 0; currentkeyindex < state_keys.length; currentkeyindex++) {
		currentkey = state_keys[currentkeyindex];
		if (parameter_keys.indexOf(currentkey) == -1) {
			cleaned_state_keys[currentkey] = global_state_parameters[currentkey];
		}
	}
	global_state_parameters = new Hash();
	parameters.merge(cleaned_state_keys);
	

	new Ajax.Request(scriptname, {
		method: 'post',
		parameters: parameters,
		onSuccess: function(transport) {

			var response = transport.responseText || "error";
			if (response != "error") {
				var dataArray = new Array();
				var responseArray = response.split('**!!**');
				for (targetIdx = 0; targetIdx < responseArray.length; targetIdx++) {
					var data = responseArray[targetIdx].split('**!**');

					var type = data[0];
					if (type == 'data') {
						dataArray[data[1]] = data[2];
					}
				}
				if (respond_before_function != null) {
					respond_before_function(dataArray);
				}

                var results = {};
                var any_results = false;

				for (targetIdx = 0; targetIdx < responseArray.length; targetIdx++) {
					var source = responseArray[targetIdx];
					var data = source.split('**!**');
					var type = data[0];

					if (type == 'innerHtml') {
						var eId = data[1];
						var mode = data[3];
						var content = data[2];
						switch (mode) {
							case 'prepend':
								new Insertion.Top($(eId), content);
								break;
							case 'append':
								new Insertion.Bottom($(eId), content);
								break;
							case 'overwrite':
								$(eId).innerHTML = content;
								break;
						}
						changeIndicator(data[1]);
					}
					else if (type == 'alert') {
						alert(data[1]);
					}
					else if (type == 'js') {
						eval(data[1]);
					}
                    else if (type == 'confirm') {
                        results[data[2]] = (confirm(data[1]) ? '1' : '0');
                        any_results = true;
                    }
                    else if (type == 'data') {
                        // Already handled.
                    }
                    else if (type == 'complete') {
                        // Done.
                    }
                    else if (type == 'error') {
                        // If there is a content area, use it.
                        var id = 'content';
						var e = document.getElementById(id);
						if (e) {
                            e.innerHTML = data[1];
						    changeIndicator(id);
                        }
                        else {
                            document.body.innerHTML = data[1];
                        }
                    }
                    else {
                        // Unknown operation.
                        alert("Unrecognizable AJAX response:\n\n" + source);
                    }
				}

                // If there were any results created, send 'em back.
                if (any_results) {
                    parameters.merge(results);
                    dispatchHandler(dispatch_id,scriptname,parameters,options,
                        null,null,before_dispatch_function); 
                }

				if (respond_after_function != null) {
					respond_after_function(dataArray);
				}
			}
			else {
				alert("An ajax error has occured. Please contact FISDAP to report the problem.");
			}

		}
	});
}

function changeIndicator(div_id) {

	var div_id;

	new Effect.Highlight(document.getElementById(div_id),{startcolor: '#dddddd', endcolor: '#ffffff', restorecolor: '#ffffff', duration: 1.0});

}

function redirect_with_delay(redirectUrl) {

	setTimeout("window.location.href = '" + redirectUrl + "'", 1000);

}

/**
 * Handle a keypress event.
 * @param submitButtonId The ID of the submit button or NULL to disallow submits.
 * @param e The event.
 */
function handleKeyPress(submitButtonId, e)
{
    // We better have an event or we are lost.
    // I hate to just allow the key without knowing what it is,
    // but we can't disallow all keys if we have a bug.
    if(!e) e = window.event;
    if(!e) return true;

    // Any key other than an ENTER key is allowed.
    var c;
    if(e.which) c = e.which;
    else if(e.keyCode) c = e.keyCode;
    else return true;

    if( c != Event.KEY_RETURN ) return true;

    // We have the ENTER, or RETURN key.
    // By default, we will not allow it except under
    // certain conditions.
    var srcElement = Event.element(e);
    if(!srcElement) return false;

    // In a text area, this key stays in the text area.
    var tagName = srcElement.tagName.toUpperCase();
    if( tagName == 'TEXTAREA' ) return true;

    // If we aren't on an input element, we will deny it.
    var onInput = (tagName == 'INPUT');

    // Include here any tag names for which an ENTER
    // can be processed.
    var onAnyInput = onInput;
    if( !onAnyInput ) return false;

    // If positioned on a button, we allow the return.
    // Under normal circumstances this should never occur.
    if( onInput )
    {
        var type = srcElement.type.toUpperCase();
        if((type == 'BUTTON') || (type == 'SUBMIT')) return true;
    }

    // Find the submit button so we can click it.
    if(!submitButtonId) return false;

    submitButton = document.getElementById(submitButtonId);
    if(!submitButton) return false;

    // Schedule the button to be clicked.
    // Be careful not to leak memory.
    setTimeout(
        function() 
        { 
            submitButton = document.getElementById(submitButtonId);
            if(!submitButton) return;
            
            submitButton.click(); 
        }, 
        50);
    return false;
}


/*--------------------------------------------------------------------*/
/* Stuff for old common_page only. Can be removed once we have a new site */

var canUseLink = 1; var fromCal = 1; var fromList = 0;

/*
 * FUNCTION MM_swapImage - Display a new image upon mouseover
 */

function MM_swapImage() {
	var i,j=0,x,a=MM_swapImage.arguments; 
	document.MM_sr=new Array; 
	for (i=0; i<(a.length-2); i+=3) {
		if ((x=MM_findObj(a[i])) != null) {
			document.MM_sr[j++]=x; 
			if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];
		}
	}
}

/*
 * FUNCTION MM_swapImgRestore - Put the original image back upon completion of mouseover
 */

function MM_swapImgRestore() {
	var i,x,a=document.MM_sr; 
	for (i=0; a && i < a.length && (x=a[i]) && x.oSrc; i++) {
		x.src = x.oSrc;
	}
}

/*
 * FUNCTION MM_preloadImages - Load mouseover images from web server
 */

function MM_preloadImages() {
	var d=document; 
	if (d.images) {
		if (!d.MM_p) d.MM_p=new Array();
		var i, j=d.MM_p.length, a=MM_preloadImages.arguments; 
		for (i=0; i<a.length; i++) {
			if (a[i].indexOf("#") != 0) {
				d.MM_p[j]=new Image; 
				d.MM_p[j++].src=a[i];
			}
		}
	}
}

/*
 * JAVASCRIPT FUNCTION MM_findObj - Find an existing object even in parent frame
 */

function MM_findObj(n, d) {
	var p,i,x;
	if (!d) d=document;
	if ((p=n.indexOf("?")) > 0 && parent.frames.length) {
		d = parent.frames[n.substring(p+1)].document;
		n = n.substring(0,p);
	}
	if (!(x=d[n])&&d.all) x=d.all[n];
	for (i=0; !x&&i<d.forms.length; i++) {
		x=d.forms[i][n];
	}
	for (i=0; !x&&d.layers&&i<d.layers.length; i++) {
		x = MM_findObj(n,d.layers[i].document);
	}
	if (!x && d.getElementById) x=d.getElementById(n);
	return x;
}
/*
 * FUNCTION checkLink - Hack in case they attempt to leave Data Entry without submitting changes
 */

function checkLink() {
	if (canUseLink == 0) {
		if (confirm("You are attempting to leave the Data Entry page but you have not submitted any data. If you leave now any data you have entered in the forms will be lost. If you are sure you want to leave this page click OK, otherwise click cancel.")){ 
			canUseLink = 1;
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return true;
	}
}

/* End stuff for old common_page only. */
/*--------------------------------------------------------------------*/
