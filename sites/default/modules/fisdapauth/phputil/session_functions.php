<?php

/**
* session_functions.php - Functions to set and get session variables
*/

require_once('phputil/session_config.php');
require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/user_utils.php');
require_once('phputil/string_utils.php');
require_once('phputil/settings_functions.php');

/**
* FUNCTION get_initial_target
*
* Given an absolute path from the WWW root, returns a relative path
* suitable for redirect_to($page), or false if the requested initial
* target page isn't in the whitelist.  This function is used to
* determine the valid paths into FISDAP on an initial login.
* 
* Note: "My FISDAP" should *not* appear in the whitelist, as it is
* handled separately by default (if it's placed in the whitelist, we
* lose the "Welcome to FISDAP" initial login message).
*
* @param $request string the page being requested at log-in time
* @return string a relative path to the requested page, if allowed.  false
*                otherwise
*/

function get_initial_target($request) {

// @todo refactor this into a function that queries the database to
// get the whitelist of valid paths

    $whitelist = array( 'shift/index.html', 'shift/evals/itemsForRevision.html', 
						'shift/evals/testItemHome.html', 'reports/index.html', 
						'resources/forms.html', 'admin/index.html', 'admin/order.html', 
						'admin/pdaStart.html', 'scheduler/tradeReply.html','admin/to_community.php', 
						'scheduler/schedulercont.html', 'testing/testDetails.html',
						'admin/to_research_forum.php','community/course/view.php?id=4',
						'admin/session_signup.php');

// check for an anchor in the URL (e.g., 'foo/foo.html#footer')
// @todo REFACTOR ME INTO MY OWN FUNCTION!

    $anchor_start = strpos($request, '#');
    $anchor = ($anchor_start !== false) ? 
        substr($request, $anchor_start) : 
        '';

    // filter out the query string, if any
    $request_body = ($anchor_start !== false) ? 
        substr($request, 0, $anchor_start) :
        $request;
    $request_tokens = explode('?', $request_body);
    $page = $request_tokens[0];
    $query = (isset($request_tokens[1])) ? 
        '?' . $request_tokens[1] :
        '';

    foreach ( $whitelist as $valid_page ) {
        if ( str_ends_with($page, $valid_page) ) {
            return $valid_page . $query . $anchor;
        }//if
    }//foreach

    return false;
}//get_initial_target


/**
* FUNCTION check_text_cookie - Make sure that the test cookie worked.
* 
* @todo check whether or not we should delete the test cookie.  logging
*       in as a new user while already logged in doesn't seem to work
*       when we delete the cookie.  is there a problem with just
*       leaving the cookie in place?
*/

function check_test_cookie() {

	if ( !isset($_COOKIE['FISDAP_TEST']) || $_COOKIE['FISDAP_TEST'] != 'test' ) {
		redirect_to('auth/cookie_error.php');
	}

}


/**
* FUNCTION set_test_cookie - Set a test cookie to make sure our session cookie will work.
*/

function set_test_cookie() {

	setcookie('FISDAP_TEST', 'test', 0, '/');

}


/**
* FUNCTION read_session_flash - Returns the array of messages from the last page, deleting the
* messages from the session (to keep them from propagating).
*/

function read_session_flash() {

	$flash = false;
	if ( isset($_SESSION) && isset($_SESSION[SESSION_KEY_PREFIX . 'flash']) && (    !isset($_SESSION[SESSION_KEY_PREFIX . 'flash_read']) || !$_SESSION[SESSION_KEY_PREFIX . 'flash_read']) ) {
		$flash = $_SESSION[SESSION_KEY_PREFIX . 'flash'];
	}

	delete_session_flash(); // delete any flash notices (double-check this method)

	return $flash;

}

/**
* FUNCTION delete_session_flash - Remove all key pairs from the session flash array
*/

function delete_session_flash() {

	if (isset($_SESSION[SESSION_KEY_PREFIX . 'flash']) ) {
		if (is_array($_SESSION[SESSION_KEY_PREFIX . 'flash']) ) {

// try to get rid of all the child data first

			foreach (array_keys( $_SESSION[SESSION_KEY_PREFIX . 'flash']) as $key ) {
				unset($_SESSION[SESSION_KEY_PREFIX . 'flash'][$key]);
			}
		}

// now, try to unset the array itself

		unset($_SESSION[SESSION_KEY_PREFIX . 'flash']);
	}

// get rid of the bool that tells whether or not the flash array has
// been read

	if (isset($_SESSION[SESSION_KEY_PREFIX . 'flash_read']) ) {
		$_SESSION[SESSION_KEY_PREFIX . 'flash_read'] = true;
		unset($_SESSION[SESSION_KEY_PREFIX . 'flash_read']);
	}

}


/**
* FUNCTION write_session_flash - Write the given message to the flash notices for the next page.
*/

function write_session_flash($key, $message) {

	$flash = array();
	if (isset($_SESSION) && isset($_SESSION[SESSION_KEY_PREFIX . 'flash']) ) {
		$flash = $_SESSION[SESSION_KEY_PREFIX . 'flash'];
	}

	$flash[$key] = $message;
	$_SESSION[SESSION_KEY_PREFIX . 'flash'] = $flash;
	$_SESSION[SESSION_KEY_PREFIX . 'flash_read'] = false;

}


/**
* FUNCTION on_session_start - Start the session (currently a no-op).
*/

function on_session_start($save_path, $session_name) {

}


/**
* FUNCTION on_session_end - Close the session (currently a no-op).
*/

function on_session_end() {

}


/**
* FUNCTION on_session_read - Read the session from the database.
*/

function on_session_read($id) {

	$connection = FISDAPDatabaseConnection::get_instance();
	$mysql_link = $connection->get_link_resource();

	if ( !preg_match('/[a-f0-9]+/', strtolower($id)) ) {
		return false; // Exit early!
	}

	$query = "SELECT data FROM " . FISDAP_SESSION_TABLE . " " . "WHERE id='$id' " . "AND expires_at > NOW()";
	$result = mysql_query($query, $mysql_link);
	if (!$result || mysql_num_rows($result) != 1 ) { 
		$result = false;
	}

	if ($result ) {
		$row = mysql_fetch_assoc($result);
		return $row['data']; // Exit early!
	}
	else {
		return $result; // Exit early!
	}

}


/**
* FUNCTION get_mysql_session_date_expression
* Return a string containing a mysql expression that evaluates to the
* session expiration time.
*/

function get_mysql_session_date_expression() {

    return 'DATE_ADD('
        .      'DATE_ADD('
        .          'DATE_ADD('
        .              'NOW(), '
        .              'INTERVAL '
        .              SESSION_EXPIRATION_INTERVAL_HOURS 
        .              ' HOUR'
        .          '), '
        .          'INTERVAL '
        .          SESSION_EXPIRATION_INTERVAL_MINUTES
        .          ' MINUTE'
        .      '), '
        .      'INTERVAL '
        .      SESSION_EXPIRATION_INTERVAL_SECONDS 
        .      ' SECOND'
        .  ') ';

}


/**
* FUNCTION on_session_write - Write the session data to the database.
*/

function on_session_write($id, $data) {

    $connection = FISDAPDatabaseConnection::get_instance();
    $mysql_link = $connection->get_link_resource();

    $escaped_data = addslashes($data);
    $insert_query = "INSERT INTO " . FISDAP_SESSION_TABLE . " "
        . "SET "
        .     "id='$id', "
        .     "expires_at=" . get_mysql_session_date_expression() . ", "
        .     "data='$escaped_data'";

    $update_query  = "UPDATE " . FISDAP_SESSION_TABLE . " "
        . "SET "
        .     "data='$escaped_data', "
        .     "expires_at=" . get_mysql_session_date_expression() . " "
        .     "WHERE id='$id'";
    
    // insert a new session, or update the existing session if the
    // insert failed
    mysql_query($insert_query, $mysql_link);
    $mysql_error = mysql_error($mysql_link);
    if ( $mysql_error ) {
        mysql_query($update_query, $mysql_link);

        $mysql_error = mysql_error($mysql_link);
        if ( $mysql_error ) {
            error_log("Session update error: " . mysql_error($mysql_link));
            echo("Session update error: " . mysql_error($mysql_link));
        }//if
    }//if

}


/**
* FUNCTION on_session_destroy - Delete the session from the database 
*/

function on_session_destroy($id) {

    $connection = FISDAPDatabaseConnection::get_instance();
    $mysql_link = $connection->get_link_resource();

    mysql_query(
        "DELETE FROM " . FISDAP_SESSION_TABLE . " WHERE id='$id'",
        $mysql_link
    );

}

/**
* FUNCTION on_session_gc - Garbage collect stale sessions 
*/

function on_session_gc($max_lifetime) {

    $connection = FISDAPDatabaseConnection::get_instance();
    $mysql_link = $connection->get_link_resource();

    mysql_query(
        "DELETE FROM " . FISDAP_SESSION_TABLE . " WHERE expires_at <= NOW()",
        $mysql_link
    );

}

/**
* FUNCTION configure_session_handlers
*
* Set the above functions as the session-handling routines, and
* configure session-specific settings.
*/

function configure_session_handlers() {

    session_set_save_handler(
        "on_session_start", "on_session_end", "on_session_read",
        "on_session_write", "on_session_destroy", "on_session_gc"
    );

}


/**
* FUNCTION add_query_info_to_session
*
* Add information about the remote user's IP address, etc, to the 
* session, for easier debugging after the fact.
*
*/

function add_query_info_to_session() {

    $_SESSION[SESSION_KEY_PREFIX . 'ip_address'] = $_SERVER['REMOTE_ADDR'];
    $_SESSION[SESSION_KEY_PREFIX . 'request_uri'] = $_SERVER['REQUEST_URI'];
    $_SESSION[SESSION_KEY_PREFIX . 'user_agent'] = $_SERVER['HTTP_USER_AGENT'];
    $_SESSION[SESSION_KEY_PREFIX . 'method'] = $_SERVER['REQUEST_METHOD'];
    $_SESSION[SESSION_KEY_PREFIX . 'referer'] = $_SERVER['HTTP_REFERER'];
    $_SESSION[SESSION_KEY_PREFIX . 'request_time'] = date('Ymd H:i:s');

}

/**
* FUNCTION start_fisdap_session - Start a database-driven FISDAP session with the current user.
*/

function start_fisdap_session() {

    configure_session_handlers();
    session_start();

    add_query_info_to_session();

    // if the user's session has expired, and the expired session contains
    // login info, go back to the home page (reloading the whole shebang to get
    // rid of any "you are logged in as ..." messages in the top bar).
    if ( $expired_session = has_expired_session() ) {
        if ( is_array($expired_session) 
             && isset($expired_session['username']) ) {
            write_session_flash(
                'login_message',
                get_session_expiration_message()
            );
            redirect_to('auth/js_redirect_to_login.php');
        }//if
    }//if

}//start_fisdap_session


/**
* FUNCTION has_expired_session
*
* Returns the first expired session data iff there is an expired session with
* the current session id.  If the expired session has no data, we return true.
*
* Note: start_fisdap_session must be called prior to using this function
*/

function has_expired_session() {

    $connection = FISDAPDatabaseConnection::get_instance();

    // escape the session id just in case...
    $session_id = addslashes(session_id());

    if ( $session_id ) {
        $query = 'SELECT * '
            . 'FROM ' . FISDAP_SESSION_TABLE . ' '
            . 'WHERE id="'.$session_id.'" '
            . 'AND   expires_at<=NOW()'
            . 'ORDER BY expires_at DESC';
        $result_set = $connection->query($query);
        if ( is_array($result_set) && count($result_set) ) {
            if ( is_array($result_set[0]) && isset($result_set[0]['data']) ) {
                return $result_set[0]['data'];
            } else {
                return true;
            }//else
        }//if
    }//if

    return false;

}

/**
* FUNCTION has_valid_session - Return true if there is a valid session
*
* Note: start_fisdap_session must be called prior to using this function
*/

function has_valid_session() {

    if ( isset($_COOKIE['cookname']) &&
	 isset($_COOKIE['cookpass'])) {
	$_SESSION[SESSION_KEY_PREFIX . 'username'] = $_COOKIE['cookname'];
	$_SESSION[SESSION_KEY_PREFIX . 'password'] = $_COOKIE['cookpass'];
    }

    if ( isset($_SESSION[SESSION_KEY_PREFIX . 'username']) &&
         isset($_SESSION[SESSION_KEY_PREFIX . 'password']) ) {
        return true;
    }//if

    return false;

}

/**
* FUNCTION get_session_length_string - Return a human-readable string containing the session length.
*/

function get_session_length_string() {

    $seconds_message = (SESSION_EXPIRATION_INTERVAL_SECONDS) ? 
        SESSION_EXPIRATION_INTERVAL_SECONDS . ' seconds' : 
        '';

    $minutes_message = (SESSION_EXPIRATION_INTERVAL_MINUTES) ? 
        SESSION_EXPIRATION_INTERVAL_MINUTES . ' minutes' : 
        '';
    $minutes_message .= ($minutes_message && $seconds_message) ? 
        ', ' :
        '';

    $hours_message = (SESSION_EXPIRATION_INTERVAL_HOURS) ? 
        SESSION_EXPIRATION_INTERVAL_HOURS . ' hour' : 
        '';
    if ( $hours_message && SESSION_EXPIRATION_INTERVAL_HOURS > 1 ) {
        $hours_message .= 's';
    }//if
    $hours_message .= ($hours_message 
                       && ($minutes_message || $seconds_message)) ? 
        ', ' : 
        ' ';

    return $hours_message . $minutes_message . $seconds_message;

}

/**
* FUNCTION write_session_field - Write the given key/value pair to the session.
*/

function write_session_field($key, $value) {

	$_SESSION[SESSION_KEY_PREFIX . 'userdef'][$key] = $value;

}


/**
* FUNCTION read_session_field - Read the value, if any, stored with the given key in the session array.
*/

function read_session_field($key) {

	if (is_session_field($key)) {
		return $_SESSION[SESSION_KEY_PREFIX . 'userdef'][$key];
	}
	return null;

}

/**
* FUNCTION is_session_field - Does a specific key exist as a session variable?
*/

function is_session_field($key) {

	$isset = isset($_SESSION[SESSION_KEY_PREFIX . 'userdef'][$key]);
	return $isset;

}


/**
* FUNCTION delete_session_field - Get rid of the given key in the session array.
*
* Note: arrays, objects, etc should be set to false, or some other scalar value, before calling this function.
*/

function delete_session_field($key) {

	if (is_session_field($key)) {
		unset($_SESSION[SESSION_KEY_PREFIX . 'userdef'][$key]);
	}

}

/**
* FUNCTION get_session_expiration_message - Return a string explaining session expiration when we dump people back to the login screen.
*/

function get_session_expiration_message() {

	return 'Your session has expired. For security reasons, users are automatically logged out after a period of ' . get_session_length_string() . '. Please log in to continue.';

}


/**
* FUNCTION fudge_session_globals - Set globals that legacy scripts use to determine the username and password of the remote user.
*/

function fudge_session_globals() {

// we'll need to set these globals to fool some old scripts into thinking
// that we're still using HTTP Basic authentication

    global $REMOTE_USER;
    global $PHP_AUTH_USER;
    global $PHP_AUTH_PW;

    if ( is_masquerade() ) {
        // if a staff member is pretending to be someone else, set the
        // username accordingly, and set the password to the empty
        // string
        $PHP_AUTH_USER            = get_masquerade_username();
        $_SERVER['PHP_AUTH_USER'] = get_masquerade_username();

        $REMOTE_USER              = get_masquerade_username();
        $_SERVER['REMOTE_USER']   = get_masquerade_username();

        $PHP_AUTH_PW              = '';
        $_SERVER['PHP_AUTH_PW']   = '';
    } else {
        // default to setting the username and password from the session
        $PHP_AUTH_USER            = $_SESSION[SESSION_KEY_PREFIX . 'username'];
        $_SERVER['PHP_AUTH_USER'] = $_SESSION[SESSION_KEY_PREFIX . 'username'];

        $REMOTE_USER              = $_SESSION[SESSION_KEY_PREFIX . 'username'];
        $_SERVER['REMOTE_USER']   = $_SESSION[SESSION_KEY_PREFIX . 'username'];

        $PHP_AUTH_PW              = $_SESSION[SESSION_KEY_PREFIX . 'password'];
        $_SERVER['PHP_AUTH_PW']   = $_SESSION[SESSION_KEY_PREFIX . 'password'];
    }//else

}//fudge_session_globals


/**
* FUNCTION clear_masquerade - Clear the session's masquerade settings
*/

function clear_masquerade() {

	$_SESSION[SESSION_KEY_PREFIX . 'mask_user'] = false;
	unset($_SESSION[SESSION_KEY_PREFIX . 'mask_user']);

}


/**
* FUNCTION set_masquerade_username - Set the masquerade username to the given string
*/

function set_masquerade_username($username) {

	$_SESSION[SESSION_KEY_PREFIX . 'mask_user'] = $username;

}

/**
* FUNCTION get_masquerade_username - Returns the username as which the current user is masquerading
*/

function get_masquerade_username() {

	return $_SESSION[SESSION_KEY_PREFIX . 'mask_user'];

}

/**
* FUNCTION require_valid_session - Restrict the current page to users who've already logged in.
*/

function require_valid_session($params=false,$mobile=false) {

    global $REMOTE_USER;
    global $PHP_AUTH_USER;
    global $PHP_AUTH_PW;
   // global $dbConnect;

    $connection = FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    // currently, the only valid parameter is the session_cache_limiter
    // setting
    if ( is_array($params) && isset($params['session_cache_limiter']) ) {
        session_cache_limiter($params['session_cache_limiter']);
    }//if

    // configure our session handlers and start a session
    start_fisdap_session();

    // check the session credentials
    if ( has_valid_session() ) {
        // fudge the globals dealing with user authentication, for
        // compatibility with existing scripts
        if ( false ) {
            // why doesn't this work?  the function seems not to be
            // defined correctly, as it bails whenever I use this line,
            // even though there are no parse errors   ~sm
            fudge_session_globals();
        } else {
            if ( is_masquerade() ) {
                // if a staff member is pretending to be someone else,
                // set the username accordingly, and set the password to
                // the empty string
                $PHP_AUTH_USER            = get_masquerade_username();
                $_SERVER['PHP_AUTH_USER'] = get_masquerade_username();

                $REMOTE_USER              = get_masquerade_username();
                $_SERVER['REMOTE_USER']   = get_masquerade_username();

                $PHP_AUTH_PW              = '';
                $_SERVER['PHP_AUTH_PW']   = '';
            } else {
                // default to setting the username and password from the
                // session
                $PHP_AUTH_USER            = $_SESSION[SESSION_KEY_PREFIX . 'username'];
                $_SERVER['PHP_AUTH_USER'] = $_SESSION[SESSION_KEY_PREFIX . 'username'];

                $REMOTE_USER              = $_SESSION[SESSION_KEY_PREFIX . 'username'];
                $_SERVER['REMOTE_USER']   = $_SESSION[SESSION_KEY_PREFIX . 'username'];

                $PHP_AUTH_PW              = $_SESSION[SESSION_KEY_PREFIX . 'password'];
                $_SERVER['PHP_AUTH_PW']   = $_SESSION[SESSION_KEY_PREFIX . 'password'];
            }//else
        }//else
    } else {
        // check for an expired session
        $expired_session = has_expired_session();
        if ( $expired_session ) {
            $login_message = get_session_expiration_message();
        } else {
            $login_message = 'Please enter your username and password '
                . 'to continue.';
        }//else

        write_session_flash('login_target', $_SERVER['REQUEST_URI']);
	write_session_flash('login_message', $login_message);
	if (!$mobile) {
		redirect_to('auth/js_redirect_to_login.php');
	} else {
		redirect_to('mobile_login.php');
	}
    }//else

}//require_valid_session

/**
* FUNCTION fisdap_auth_extend_session_expiration - Update the expiry time on the session with the current session, if any
*/

function fisdap_auth_extend_session_expiration() {

    if ( $session_id = session_id() ) {
       $connection = FISDAPDatabaseConnection::get_instance();
       $query = 'UPDATE ' . FISDAP_SESSION_TABLE . ' '
           . 'SET expires_at=' . get_mysql_session_date_expression()
           . 'WHERE id="' . $session_id . '"';
       $connection->query($query);
    }//if

}
            
/**
* FUNCTION redirect_to
*
* Send a Location header to redirect to the given page.  The path should be
* from the FISDAP root directory (e.g., 'admin/student_stuff.php'), with 
* no leading slash.
*/

function redirect_to($page) {

	header('Location: ' . FISDAP_WEB_ROOT . $page);
	exit(0); // Exit early!

}

/**
* FUNCTION is_masquerade - Return true if this session's user is a staff member masquerading as another user.
*/

function is_masquerade() {

	return ( isset($_SESSION[SESSION_KEY_PREFIX . 'mask_user']) && $_SESSION[SESSION_KEY_PREFIX . 'mask_user']);

}


/**
* FUNCTION unserialize_session_data - Read the session data from the given string, returning an array of the same form as $_SESSION.
*
* This function was cribbed from the php.net manual for unserialize(), then edited heavily.
*/

function unserialize_session_data($serialized_string) {

    $session = array();
    $variables = preg_split(
        "/(\w+)\|/",
        $serialized_string,
        -1,
        PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE 
    );

    if ( !is_array($variables) ) {
        return array();
    }//if

    $num_variables = count($variables);
    for( $i = 0 ; $i < $num_variables ; $i += 2 ) {
        $session[$variables[$i]] = unserialize($variables[$i+1]);
    }//for

    return $session;

}

/**
* FUNCTION print_username_for_session
*/

function print_username_for_session($id) {

    $raw_data = on_session_read($id);
    if ( $raw_data ) {
        $session = unserialize_session_data($raw_data);

        if ( isset($session['username']) ) {
            echo $session['username'] . "\n";
        }//if
    }//if

}

/**
* FUNCTION get_url_for
*
* Returns a string containing a relative path to the given document,
* which is itself specified as a relative path, from the FISDAP Web
* root.
*/

function get_url_for($page) {

	return FISDAP_WEB_ROOT . $page;

}

/**
 * Returns a standard link tag for the given page.
 */
function link_to($page, $link_text, $target=null)
{
	$url = get_url_for($page);
	$link = '<a href="' . $url . '"';
	if($target!=null) {
		$link .= ' target="' . $target . '"';
	}
	$link .= '>' . $link_text . '</a>';
    return $link;
}//link_to

?>
