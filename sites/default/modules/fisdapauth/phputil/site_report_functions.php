<?php
require_once('phputil/classes/FISDAPDatabaseConnection.php');

function get_runs_per_student($function,$program,$base,$startdate,$enddate)
{
    $connection =& FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    $select =   "select ".$function."(Runs) AS NUMBER "
	.	"FROM "
	.	"("
	.	"select count(RunData.Run_id) AS Runs "
	.	"FROM "
	.	"RunData,"
	.	"StudentData, "
	.	"ShiftData "
	.	"WHERE "
	.	"RunData.Student_id = StudentData.Student_id "
	.	"AND "
	.	"StudentData.Program_id = $program, "
	.	"AND "
	.	"ShiftData.Shift_id = RunData.Shift_id "
	.	"AND "
	.	"ShiftData.StartBase_id = $base "
	.	"AND "
	.	"ShiftData.StartDate > '$startdate' "
	.	"AND "
	.	"ShiftData.StartDate <= '$enddate' "
	.	"GROUP BY "
	.	"RunData.Shift_id"
	.	") "
	.	"AS "
	.	"Selected";
    $result = mysql_query($select,$dbConnect);
    $count = mysql_num_rows($result);
    if($count == 1)
    {
	return mysql_result($result,0,"NUMBER");
    }
    else
    {
	return false;
    }
}

function get_shifts_per_student($function,$program,$base,$startdate,$enddate)
{
    $connection =& FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    $select =   "select ".$function."(Shifts) AS NUMBER "
        .       "FROM "
        .       "("
	.	"select count(ShiftData.Shift_id) AS Shifts "
	.	"FROM "
	.	"ShiftData, "
	.	"StudentData "
	.	"WHERE "
	.	"ShiftData.Student_id=StudentData.Student_id "
	.	"AND "
	.	"StudentData.Program_id = $program "
	.	"AND "
	.	"ShiftData.StartBase_id = $base "
	.	"AND ShiftData.StartDate <= '$enddate' "
	.	"AND "
	.	"ShiftData.StartDate > '$startdate' "
	.	"GROUP BY "
	.	"ShiftData.Student_id"
	.       ") "
        .       "AS "
        .       "Selected";
    $result = mysql_query($select,$dbConnect);
    $count = mysql_num_rows($result);
    if($count == 1)
    {
        return mysql_result($result,0,"NUMBER");
    }
    else
    {
        return false;
    }
}

function get_runs_per_shift($function,$program,$base,$startdate,$enddate)
{
    $connection =& FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    $select =   "select ".$function."(Runs) AS NUMBER "
        .       "FROM "
        .       "("
	.	"select count(RunData.Run_id) AS Runs "
	.	"FROM "
	.	"RunData, "
	.	"StudentData, "
	.	"ShiftData "
	.	"WHERE "
	.	"RunData.Student_id = StudentData.Student_id "
	.	"AND "
	.	"StudentData.Program_id = $program "
	.	"AND "
	.	"ShiftData.Shift_id = RunData.Shift_id "
	.	"AND "
	.	"ShiftData.StartBase_id = $base "
	.	"AND "
	.	"ShiftData.StartDate > '$startdate' "
	.	"AND "
	.	"ShiftData.StartDate <= '$enddate' "
	.	"GROUP BY "
	.	"RunData.Shift_id "
	.       ") "
        .       "AS "
        .       "Selected";

    $result = mysql_query($select,$dbConnect);
    $count = mysql_num_rows($result);
    if($count == 1)
    {
        return mysql_result($result,0,"NUMBER");
    }
    else
    {
        return false;
    }
}

function get_shifts_at_base($program,$base,$startdate,$enddate)
{
    $connection =& FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    $select =   "select "
	.	"count(ShiftData.Shift_id) AS Shifts "
	.	"FROM "
	.	"ShiftData, "
	.	"StudentData "
	.	"WHERE "
	.	"ShiftData.Student_id = StudentData.Student_id "
	.	"AND "
	.	"StudentData.Program_id = $program "
	.	"AND "
	.	"ShiftData.StartBase_id = $base "
	.	"AND "
	.	"ShiftData.StartDate <= '$enddate' "
	.	"AND "
	.	"ShiftData.StartDate > '$startdate'";
    $result = mysql_query($select,$dbConnect);
    return mysql_result($result,0,"Shifts");
}

function get_runs_at_base($program,$base,$startdate,$enddate)
{
    $connection =& FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    $select =   "select "
        .       "count(RunData.Run_id) AS Runs "
        .       "FROM "
        .       "ShiftData, "
	.	"RunData, "
        .       "StudentData "
        .       "WHERE "
        .       "ShiftData.Student_id = StudentData.Student_id "
        .       "AND "
        .       "StudentData.Program_id = $program "
        .       "AND "
        .       "ShiftData.StartBase_id = $base "
        .       "AND "
	.	"ShiftData.Shift_id = RunData.Shift_id "
	.	"AND "
        .       "ShiftData.StartDate <= '$enddate' "
        .       "AND "
        .       "ShiftData.StartDate > '$startdate'";
    $result = mysql_query($select,$dbConnect);
    return mysql_result($result,0,"Runs");
}

function get_avg_hours_per_shift($program,$base,$startdate,$enddate)
{
    $connection =& FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    $select =   "select avg(ShiftData.Hours) AS avg_hours "
	.	"FROM "
	.	"ShiftData, "
	.	"StudentData "
	.	"WHERE "
	.	"ShiftData.Student_id = StudentData.Student_id "
	.	"AND "
	.	"StudentData.Program_id = $program "
	.	"AND "
	.	"ShiftData.StartBase_id = $base "
	.	"AND "
        .       "ShiftData.StartDate <= '$enddate' "
        .       "AND "
        .       "ShiftData.StartDate > '$startdate'";
	
    $result = mysql_query($select,$dbConnect);
    return mysql_result($result,0,"avg_hours");
}



function get_base_cardiac_arrests($program,$base,$startdate,$enddate)
{
    $connection =& FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    $select =   "SELECT "
        .       "count(RunData.Run_id) "
        .       "FROM "
        .       "RunData, "
        .       "StudentData, "
        .       "ShiftData "
        .       "WHERE "
        .       "RunData.Student_id = StudentData.Student_id "
        .       "AND "
        .       "StudentData.Program_id = $program "
        .       "AND "
        .       "("
        .       "RunData.Diagnosis = 4 "
        .       "|| "
        .       "RunData.Diag2 = 4"
        .       ") "
        .       "AND "
        .       "RunData.Shift_id = ShiftData.Shift_id "
        .       "AND "
        .       "ShiftData.StartBase_id = $base "
        .       "AND "
        .       "ShiftData.StartDate <= '$enddate' "
        .       "AND "
        .       "ShiftData.StartDate > '$startdate'";
    $result = mysql_query($select,$dbConnect);
    return mysql_result($result,0,0);
}

function get_base_trauma_calls($program,$base,$startdate,$enddate)
{
    $connection =& FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    $select =   "select count(RunData.Run_id) AS NumRuns "
	.	"FROM "
	.	"RunData, "
	.	"StudentData, "
	.	"ShiftData, "
	.	"DiagnosisTable "
	.	"WHERE "
	.	"RunData.Student_id = StudentData.Student_id "
	.	"AND "
	.	"StudentData.Program_id = $program "
	.	"AND "
	.	"RunData.Diagnosis = DiagnosisTable.Diag_id "
	.	"AND "
	.	"DiagnosisTable.NSCType = 'trauma' "
	.	"AND "
	.	"RunData.Shift_id=ShiftData.Shift_id "
	.	"AND "
	.	"ShiftData.StartBase_id = $base "
	.	"AND "
	.	"ShiftData.StartDate <= '$enddate' "
	.	"AND "
	.	"ShiftData.StartDate > '$startdate'";
    $result = mysql_query($select,$dbConnect);
    return mysql_result($result,0,"NumRuns");
}

function get_base_cardiac_calls_no_arrest($program,$base,$startdate,$enddate)
{
    $connection =& FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    $select =   "select count(RunData.Run_id) AS Runs "
	.	"FROM "
	.	"RunData, "
	.	"StudentData, "
	.	"ShiftData "
	.	"WHERE "
	.	"RunData.Student_id=StudentData.Student_id "
	.	"AND "	
	.	"StudentData.Program_id = $program "
	.	"AND "
	.	"("
	.	"RunData.Diagnosis = 3 "
	.	"|| "
	.	"RunData.Diag2 = 3"
	.	") "
	.	"AND "
	.	"RunData.Diagnosis != 4 "
	.	"AND "
	.	"RunData.Diag2 != 4 "
	.	"AND "
	.	"RunData.Shift_id=ShiftData.Shift_id "
	.	"AND "
	.	"ShiftData.StartBase_id = $base "
	.	"AND "
	.	"ShiftData.StartDate <= '$enddate' "
	.	"AND "
	.	"ShiftData.StartDate > '$startdate'";

    $result = mysql_query($select,$dbConnect);
    return mysql_result($result,0,"Runs");
}

function get_base_peds_calls($program,$base,$startdate,$enddate,$peds_cutoff)
{
    $connection =& FISDAPDatabaseConnection::get_instance();
    $dbConnect = $connection->get_link_resource();

    $select =   "select count(RunData.Run_id) AS Runs "
	.	"FROM "
	.	"RunData, "
	.	"StudentData, "
	.	"ShiftData "
	.	"WHERE "
	.	"RunData.Student_id = StudentData.Student_id "
	.	"AND "
	.	"StudentData.Program_id = $program "
	.	"AND "
	.	"RunData.Age < $peds_cutoff "
	.	"AND "
	.	"RunData.Shift_id = ShiftData.Shift_id "
	.	"AND "
	.	"ShiftData.StartBase_id = $base "
	.	"AND "
	.	"ShiftData.StartDate <= '$enddate' "
	.	"AND "
	.	"ShiftData.StartDate > '$startdate'";
    $result = mysql_query($select,$dbConnect);
    return mysql_result($result,0,"Runs");
}

function get_program_active_sites($type,$program)
{
    $connection =& FISDAPDatabaseConnection::get_instance();

    $select =   "SELECT ProgramSiteData.AmbServ_id "
	.	"FROM "
	.	"ProgramSiteData, "
	.	"AmbulanceServices "
	.	"WHERE "
	.	"ProgramSiteData.Program_id = $program "
	.	"AND "
	.	"ProgramSiteData.Active = 1 "
	.	"AND "
	.	"ProgramSiteData.AmbServ_id = AmbulanceServices.AmbServ_id "
	.	"AND "
	.	"AmbulanceServices.Type = '$type'";

    return $connection->query($select);
}

function get_program_active_site_bases($site,$program)
{
    $connection =& FISDAPDatabaseConnection::get_instance();

    $select =   "SELECT ProgramBaseData.Base_id "
	.	"FROM "
	.	"ProgramBaseData, "
	.	"AmbServ_Bases "
	.	"WHERE "
	.	"ProgramBaseData.Program_id = $program "
	.	"AND "
	.	"ProgramBaseData.Active = 1 "
	.	"AND "
	.	"ProgramBaseData.Base_id = AmbServ_Bases.Base_id "
	.	"AND "
	.	"AmbServ_Bases.AmbServ_id=$site";
    return $connection->query($select);
}
?>
