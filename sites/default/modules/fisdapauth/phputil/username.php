<?php

/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2007.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/

/*
 * 	This function will generate a new User Name given the FirstName, LastName, and Id
 * 	The type is either Student, or Instructor.
 */
function GenUserName($FirstNameIn, $LastNameIn, $Id, $Type) { 	
	$TmpName = strtr($FirstNameIn, "`~!@#-:$%_<>\"^&*()_+-=~<>?.,'[]{}|", "                            ");  //remove these characters
	$FirstName = str_replace(" ", "", $TmpName);		// Remove spaces
	
	$TmpName = strtr($LastNameIn,  "`~!@#-:$%_<>\"^&*()_+-=~<>?.,'[]{}|", "                            ");  //remove these characters
	$LastName = str_replace(" ", "", $TmpName);		// Remove spaces
	
	if ($Type == "Student") {
		$TmpName = substr($FirstName, 0, 1) . substr($LastName, 0,3) . $Id;
	} else {
		$TmpName = substr($FirstName, 0, 1) . substr($LastName, 0,5) . $Id;
	}
	$UserName = str_replace(" ", "", $TmpName);		// Remove spaces
	
	return $UserName;
}


function GetUserNameCount($UserName) {
	global $dbConnect;
	$query = "Select idx from UserAuthData where email like '$UserName%' and email not REGEXP '$UserName\[a-z]+' ";
	//$query = "Select * from UserAuthData where email like '$UserName'";

	$result = mysql_query($query, $dbConnect);
	$count = mysql_num_rows($result);

	return $count;
}


function GetDrupalUserNameCount($UserName) {
	$drupal_link = mysql_connect('localhost', 'drupal', 'dRuPaLr0ckS');
	if (!$drupal_link) {
	    die('Not connected : ' . mysql_error());
	}
	
	$drupal_db_selected = mysql_select_db('drupal_fisdap', $drupal_link);
	if (!$drupal_db_selected) {
	    die ('Can\'t use drupal_fisdap : ' . mysql_error());
	}
	
	$drupal_user_result = mysql_query("SELECT name FROM users WHERE name = '$UserName'", $drupal_link);
	$count = mysql_num_rows($drupal_user_result);
	
	mysql_close($drupal_link);
	
	return $count;
}


// Is this username unique, and properly formed?
function IsUserNameUnique($UserName) {
	$UCount = GetUserNameCount($UserName);
	$drupal_user_count = GetDrupalUserNameCount($UserName);
	
	// We can't do this until PHP 5
	// $BadCharacter = strpbrk($UserName, "`~!@#-:$%_<>\"^&*()_+-=~<>?.,'[]{}|");
	$BadCharacter = false;
	
	if ($UCount == 0 && $drupal_user_count == 0 && $BadCharacter == false) {
		return true;
	}
	
	return false;
}


function GenNewNamefromRoot($Root) {
	global $RootsTried;

	if ($RootsTried[$Root] == 1) {
		return "";
	}
	
	$ReturnVal = "";
	$TestUserName = $Root;
	$NameCount = GetUserNameCount($TestUserName);
	$NameCount += GetDrupalUserNameCount($TestUserName);
	$LoopCount = $NameCount;
	$LoopLimit = $LoopCount + 50;
	
	while ($NameCount > 0  && $LoopCount <= $LoopLimit) {
		$TestUserName = $Root . $LoopCount;
		$NameCount = GetUserNameCount($TestUserName);
		$NameCount += GetDrupalUserNameCount($TestUserName);
		$LoopCount++;
	}
	
	if ($NameCount == 0 && $LoopCount <= 50) {
		$ReturnVal = $TestUserName . "<br>";
	}
	$RootsTried[$Root] = 1;
	return $ReturnVal;
}


function GetUserNameOptions($UserNameIn, $FirstNameIn, $LastNameIn) {	
	$OptionsList = "";
	$LoopCount = 1;
	$TryName = '';

	// remove spaces and special characters from username and convert to lowercase
	$TmpName = strtr($UserNameIn, "`~!@#-:$%_<>\"^&*()_+-=~<>?.,'[]{}|\\", "                             ");  //remove these characters
	$Tmp2Name = str_replace(" ", "", $TmpName);		// Remove spaces
	$TryUserName = strtolower($Tmp2Name);			// make lower case
	
	// remove spaces and special characters from first name and convert to lowercase
	$TmpName = strtr($FirstNameIn, "`~!@#-:$%_<>\"^&*()_+-=~<>?.,'[]{}|\\", "                             ");  //remove these characters
	$Tmp2Name = str_replace(" ", "", $TmpName);		// Remove spaces
	$FirstName = strtolower($Tmp2Name);			// make lower case
	
	// remove spaces and special characters from last name and convert to lowercase
	$TmpName = strtr($LastNameIn,  "`~!@#-:$%_<>\"^&*()_+-=~<>?.,'[]{}|\\", "                             ");  //remove these characters
	$Tmp2Name = str_replace(" ", "", $TmpName);		// Remove spaces
	$LastName = strtolower($Tmp2Name);			// make lower case

	// UserName1234
	$OptionsList = $OptionsList . GenNewNamefromRoot($TryUserName);
	
	// Mjohnson12345
	$TmpName = substr($FirstName, 0, 1) . $LastName;
	$UserName = str_replace(" ", "", $TmpName);		// Remove spaces
	$OptionsList = $OptionsList . GenNewNamefromRoot($UserName);

	// MikeJ12345
	$TmpName = $FirstName. substr($LastName, 0, 1);
	$UserName = str_replace(" ", "", $TmpName);		// Remove spaces
	$OptionsList = $OptionsList . GenNewNamefromRoot($UserName);

	// Mike12345
	$TmpName = $FirstName;
	$UserName = str_replace(" ", "", $TmpName);		// Remove spaces
	$OptionsList = $OptionsList . GenNewNamefromRoot($UserName);

	// Johnson12345
	$TmpName = $LastName;
	$UserName = str_replace(" ", "", $TmpName);		// Remove spaces
	$OptionsList = $OptionsList . GenNewNamefromRoot($UserName);

	return $OptionsList;
}


function UpdateInstructorUserName($OldUserName, $NewUserName) {
	global $dbConnect;
	
	// User Auth Data
	$UpdateStr = "Update UserAuthData set email='$NewUserName' where email='$OldUserName' limit 1";
	 mysql_query($UpdateStr, $dbConnect);

	// InstructorData
	$UpdateStr = "Update InstructorData set UserName='$NewUserName' where UserName='$OldUserName' limit 1";
	mysql_query($UpdateStr, $dbConnect);

	// ItemReviews 
	$UpdateStr = "Update ItemReviews set reviewer='$NewUserName' where reviewer='$OldUserName'";
	mysql_query($UpdateStr, $dbConnect);

}


function UpdateStudentUserName($OldUserName, $NewUserName) {
	global $dbConnect;
	
	// User Auth Data
	$UpdateStr = "Update UserAuthData set email='$NewUserName' where email='$OldUserName' limit 1";
	mysql_query($UpdateStr, $dbConnect);

	// StudentData
	$UpdateStr = "Update StudentData set UserName='$NewUserName' where UserName='$OldUserName' limit 1";
	mysql_query($UpdateStr, $dbConnect);
}

?>
