<?php

$connection = FisdapDatabaseConnection::get_instance();

/**
 * Function to generate a list of all the LOC options from the database
 *
 * @author Sam Tape stape@fisdap.net
 */
function gen_loc_list($selected_id = -1) {
	global $connection;

	$query = "SELECT * FROM LOCTable ORDER BY DisplayOrder";
	$result = $connection->query($query);

	if (count($result) <= 0) {
		echo "<option value=-1>No Data Available</option>";
	}

	foreach ($result as $loc) {
		$id = $loc['LOC_id'];
		$title = $loc['LOCName'];

		if ($id == $selected_id) {
			$selected = 'selected';
		} else {
			$selected = null;
		}

		echo "<option value='$id' $selected>$title</option>";

	}
}







?>
