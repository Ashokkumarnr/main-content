<?php
require_once('phputil/classes/model/VitalsModel.inc');

function get_pulse_quality_options($selected_id = -1) {

	//$query = 'SELECT * FROM VitalsDataPulseQualityEnum';
	//$result = $connection->query($query);
	$quality_names = Vitals::get_pulse_quality_names();

	foreach ($quality_names as $id=>$quality) {
		if ($selected_id == $id) {
			$selected = 'selected';
		} else {
			$selected = null;
		}

		echo "<option value='$id' $selected>$quality</option>";
	}
}

function get_resp_quality_options($selected_id = -1) {
	
	$quality_names = Vitals::get_resp_quality_names();

	foreach ($quality_names as $id=>$quality) {
		if ($selected_id == $id) {
			$selected = 'selected';
		} else {
			$selected = null;
		}

		echo "<option value='$id' $selected>$quality</option>";
	}
}

function generate_lung_sounds_v2($lung_sounds, $cols = 1) {
	require_once('phputil/classes/model/VitalsModel.inc');	
	$lung_sounds_names = Vitals::get_lung_sounds_names();
	unset($lung_sounds_names[-1]);

	echo "<style>.prompt_cell { float:left; padding:.3em; width:6em; }</style>";

	$i = 1;
	foreach ($lung_sounds_names as $id=>$name) {
		if (in_array($id, $lung_sounds)) {
			$checked = 'checked';
		} else {
			$checked = null;
		}

		echo "<div class='prompt_cell'><input type='checkbox' id='lung_sounds' name='lung_sounds[]' value='$id' $checked>$name</div>";

		if ($i % $cols == 0) {
			echo "<div style='clear:both;'></div>";
		}

		$i++;
	}
}

function generate_skins_v2($skins, $cols = 1) {
	require_once('phputil/classes/model/VitalsModel.inc');	
	$skin_names = Vitals::get_skin_names();
	unset($skin_names[-1]);

	echo "<style>.prompt_cell { float:left; padding:.3em; }</style>";

	$i = 1;
	foreach ($skin_names as $id=>$name) {
		if (in_array($id, $skins)) {
			$checked = 'checked';
		} else {
			$checked = null;
		}

		echo "<div class='prompt_cell'><input type='checkbox' id='skins' name='skins[]' value='$id' $checked>$name</div>";

		if ($i % $cols == 0) {
			echo "<div style='clear:both;'></div>";
		}

		$i++;
	}
}




?>
