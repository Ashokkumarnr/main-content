<?php

require_once("phputil/site_base_names.html");
require_once("phputil/get_insts.html");
require_once("phputil/email_tag.html");

$assign_programs = array(1,287,384,3,72,90,313,174,165,265,267,271,272,304,306,307,308,309,310,393,82,5,177,57,12,49,221,12,4,11,418,171,477,588,442,779,595);

function gen_precep_name_str($idstr)
{
    $return_string = "";
    $db =& FISDAPDatabaseConnection::get_instance();
    if($idstr != "")
    {
    	$precep_ids = explode(",",$idstr);
    	for($i=0;$i<sizeof($precep_ids);$i++)
    	{
	    $sel = "SELECT FirstName, LastName FROM PreceptorData WHERE Preceptor_id=$precep_ids[$i]";
	    $name_array = $db->query($sel);
	    if($return_string == "")
	    {
	        $return_string = $name_array[0]["FirstName"] . " " . $name_array[0]["LastName"];
	    }
	    else
	    {
	        $return_string .= ", " . $name_array[0]["FirstName"] . " " . $name_array[0]["LastName"];
	    }
	}
    }
    return $return_string;
}

function get_event_preceptors($event)
{
    $return_array = array();
    $db =& FISDAPDatabaseConnection::get_instance();
    $select = "SELECT Preceptor_id FROM EventPreceptorData WHERE Event_id=$event";
    $result_array = $db->query($select);
    for($i=0;$i<sizeof($result_array);$i++)
    {
	$return_array[]=$result_array[$i]["Preceptor_id"];
    }
    return $return_array;
}

function check_over_shift_limit($Student_id, $Type)
{
	$db =& FISDAPDatabaseConnection::get_instance();

	$chk_max = "select count(*) as NumShifts from ShiftData where Student_id=$Student_id AND Type='$Type'";
        $chk_res = $db->query($chk_max);
	$chk_count = $chk_res[0]["NumShifts"];

        $type_max_arr = array('field'=>'MaxFieldShifts','clinical'=>'MaxClinicShifts','lab'=>'MaxLabShifts');

        $get_max = "SELECT ".$type_max_arr[$Type]." as MaxVal FROM StudentData where Student_id=$Student_id";
        $get_max_res = $db->query($get_max);
	$max_val = $get_max_res[0]["MaxVal"];

        if($max_val > 0)
        {
            	if($chk_count >= $max_val)
            	{
                	return 1;
            	}
		else
		{
			return 0;
		}
       	}
	else
	{
		return 0;
	}
}

function assign_student_to_event($student,$event)
{
    $db =& FISDAPDatabaseConnection::get_instance();
    $select = "SELECT * FROM EventData WHERE Event_id=$event";
    $result_set = $db->query($select);
    if(sizeof($result_set) == 1)
    {
        $stu_select = "SELECT count(*) FROM ShiftData WHERE Event_id=$event AND Student_id=$student";
	$stu_result = $db->query($stu_select);
	$count = $stu_result[0]["count(*)"];
	$over_shift_limit = check_over_shift_limit($student, $result_set[0]["Type"]);
	if($count == 0 && $over_shift_limit==0)
	{
	    $insert = "INSERT INTO "
	    .	"ShiftData SET "
	    .	"Student_id = $student, "
	    .	"AmbServ_id = " . $result_set[0]["AmbServ_id"] . ", "
	    .	"StartBase_id = " . $result_set[0]["StartBase_id"] . ", "
	    .	"StartDate = '" . $result_set[0]["StartDate"] . "', "
	    .	"StartTime = " . $result_set[0]["StartTime"] . ", "
	    .	"EndTime = " . $result_set[0]["EndTime"] . ", "
	    .	"Hours = " . $result_set[0]["Hours"] . ", "
	    .	"Type = '" . $result_set[0]["Type"] . "', "
	    .	"Event_id = $event";
	    //echo $insert;
	    $db->query($insert);
	    if(true)
	    {
	    $StuSelect = "Select FirstName, LastName, EmailAddress, Program_id FROM StudentData where Student_id=$student";
           // echo "$StuSelect\n";
            $StuResult = mysql_query($StuSelect, $db->get_link_resource());
            $StuCount = mysql_num_rows($StuResult);
            if($StuCount == 1)
            {
                $StuFirst = mysql_result($StuResult, 0, "FirstName");
                $StuLast = mysql_result($StuResult, 0, "LastName");
                $Email = mysql_result($StuResult, 0, "EmailAddress");
		$Program_id = mysql_result($StuResult, 0, "Program_id");
            }
	    $AmbName = getAmbName($result_set[0]["AmbServ_id"], $db->get_link_resource(), "name");
            $BaseName = getBaseName($result_set[0]["StartBase_id"], $db->get_link_resource());
	    $ListMsg = "FISDAP Shift Notification\n\n";
            $ListMsg = $ListMsg . "$StuFirst $StuLast has been assigned to a shift at "
		.$result_set[0]["StartTime"]." lasting ".
       		$result_set[0]["Hours"] . " hours long on ".
	    	$result_set[0]["StartDate"]." at $AmbName, $BaseName.\n";
            $ListMsg = $ListMsg . "\nNotes for this shift: " .$result_set[0]["Notes"];
            $ListMsg = signMessages($ListMsg,$student,$Program_id,$db->get_link_resource());

            $listSubj = "ShiftAssigned: $StuFirst $StuLast, ".$result_set[0]["StartDate"]." at ".
		$result_set[0]["StartTime"].", $AmbName, $Base_Name";

            $Subj = "Shift Assigned: ".$result_set[0]["StartDate"]." at ".$result_set[0]["StartTime"].", $AmbName, $BaseName";

            $From = "From:fisdap-robot@fisdap.net";

            $StuMsg = "FISDAP Shift Notification\n\n";
            $StuMsg = $StuMsg . "You have been assigned a shift at ".$result_set[0]["StartTime"].
		" on ".$result_set[0]["StartDate"]." that lasts ".
		$result_set[0]["Hours"]." hours at $AmbName, $BaseName";
            $StuMsg = $StuMsg . "\n\nNotes for this shift are: ".$result_set[0]["Notes"]."";

            $StuMsg = signMessages($StuMsg,$student,$Program_id,$db->get_link_resource());
            mail($result_set[0]["EmailList"],$listSubj,$ListMsg,$From);
            mail($Email, $Subj,$StuMsg,$From);
	    return true;
	    }
	    else
	    {
		return false;
	    }
	}
	else {
		return false;	
	}
    }
    else
    {
	return false;
    }
}

?>
