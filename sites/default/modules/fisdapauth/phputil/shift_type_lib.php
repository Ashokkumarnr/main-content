<?php

function check_shiftType() {
    global $shiftType;
    if( !isset($_REQUEST['shiftType']) ) {
        $_REQUEST['shiftType'] = 'clinical';
        $shiftType = 'clinical';
    } else {
        if ( !($_REQUEST['shiftType'] == 'clinical' ||
               $_REQUEST['shiftType'] == 'lab' ||
               $_REQUEST['shiftType'] == 'field') ) {
            $_REQUEST['shiftType'] = 'clinical';
            $shiftType = 'clinical';
        }//if
    }//else
}//get_shiftType

function set_display_context($shiftType) {
    if ( !defined('DISPLAY_CONTEXT') ) {
        if ( $shiftType == 'clinical' ) {
            define('DISPLAY_CONTEXT','CLINICAL');
        } else if ( $shiftType == 'lab' ) {
            define('DISPLAY_CONTEXT','LAB');
        } else if ( $shiftType == 'field' ) {
            define('DISPLAY_CONTEXT','FIELD');
        } else {
            define('DISPLAY_CONTEXT','UNKNOWN');
        }//else 
    }//if
}//set_display_context
?>
