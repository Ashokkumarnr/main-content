<?php

/**
 * This file contains utilities for manipulating and executing database
 * queries and result sets
 *
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *  
 *                                                                           *
 *                                                                           *
 * @author Sam Martin <smartin@fisdap.net>                                   *
 * @copyright 1996-2007 Headwaters Software, Inc.                            *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 */


/** 
 * This is the generic replacement variable for replacing values in
 * queries.  This should be set to something that won't ever crop up in
 * our queries.
 */
define('QUERY_VARIABLE_PLACEHOLDER', '###?###');


/**
 * Run the given query for each of the given values, replacing the
 * variable ###?### with each value in turn.  The result is the 
 * aggregate of all of the result sets of the individual queries.
 *
 */
function query_foreach($template, $values) {
    $connection =& FISDAPDatabaseConnection::get_instance();
    $results = array();
    foreach ( $values as $value ) {
        $query = str_replace(QUERY_VARIABLE_PLACEHOLDER, $value, $template);
        $result = $connection->query($query);
        if ( $result && is_array($result) ) {
            $results[] = array_merge($results, $result);       
        }//if
    }//foreach

    return $results;
}//query_foreach


/**
 * Run the given queries, aggregating their result sets into a single
 * array.  For this to make sense, each query should return the same
 * columns.
 */
function aggregate_queries($queries) {
    if ( !is_array($queries) ) {
        return false;
    }//if

    $connection =& FISDAPDatabaseConnection::get_instance();
    $results = array();

    foreach ( $queries as $query ) {
        $result = $connection->query($query);
        if ( $result && is_array($result) ) {
            $results = array_merge($results, $result);
        }//if
    }//foreach

    return $results;
}//aggregate_queries

?>
