<?php 

/**
 * FILE DESCRIPTION
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *        Copyright (C) 1996-2007.  This is an unpublished work of           *
 *                         Headwaters Software, Inc.                         *
 *                            ALL RIGHTS RESERVED                            *
 *        This program is a trade secret of Headwaters Software, Inc. and    *
 *        it is not to be copied, distributed, reproduced, published, or     *
 *        adapted without prior authorization of Headwaters Software, Inc.   *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           
 * @package default                                                          
 * @author                                                                   
 * @copyright 1996-2007 Headwaters Software, Inc.                            
 *                                                                           
 */
 
 /**
  * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
   */
 
// Declaring valid date character, minimum year and maximum year
$dtCh= "-";
$minYear=1900;
$maxYear=2000;
   
function isInteger($intString)
{
    for ($i = 0; $i < strlen($intString); $i++)
    {   // Check that current character is number.
        $tempChar = substr($intString,$i,1);
        if ((($tempChar < "0") || ($tempChar > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}//isInteger($intString)
                                            
function stripCharsInBag($charStr, $bag)
{
    $returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for ($i = 0; $i < strlen($charStr); $i++)
    {
        $tempChar = substr($charStr,$i,1);
        if ($tempChar != $bag)
        {
            $returnString=$returnString.$tempChar;
        }
    }
    // echo "the stripped string is: $returnString\n";
    return $returnString;
}//stripCharsInBag($charStr, $bag)

function daysInFebruary($year)
{
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return 
    (
        (
            ($year % 4 == 0) && 
            ((!($year % 100 == 0)) || ($year % 400 == 0))
        ) 
        ? 29 
        : 28 
     );
}//daysInFebruary($year)

function DaysArray($months) 
{
    for ($i = 1; $i <= $months; $i++) 
    {
        $this[$i] = 31;
        if ($i==4 || $i==6 || $i==9 || $i==11) 
        {
            $this[$i] = 30;
        }
        if ($i==2) 
        {   
            $this[$i] = 29;
        }
    } 
    return $this;
}//DaysArray($months)

function isDate($dtStr, $dateType,$maxYear=2000)
{
    $dtCh= "-";
    $minYear=1900;
   // $maxYear=2000;

    $daysInMonth = DaysArray(12);
    $pos1=strpos($dtStr, $dtCh);
    $pos2=strpos($dtStr, $dtCh,$pos1+1);
    $strYear=substr($dtStr,0,$pos1);
    $strMonth=substr($dtStr,$pos1+1,$pos2-($pos1+1));
    $strDay=substr($dtStr,$pos2+1);
    $strYr=$strYear;
    
//    echo "pos1 = $pos1\n";
//   echo "pos2 = $pos2\n";
//   echo "year = $strYear\n";
//    echo "month = $strMonth\n";
//    echo "day = $strDay\n";
//    echo "year = $strYr\n";
//    if ($strDay.charAt(0)=="0" && strlen($strDay)>1) 
//    {
//        $strDay=substr($strDay,1);
//    }
//    if ($strMonth.charAt(0)=="0" && strlen($strMonth) >1) 
//    {
//        $strMonth=substr($strMonth,1);
//    }
//    for ($i = 1; $i <= 3; $i++) 
//    {
//        if ($strYr.charAt(0)=="0" && strlen($strYr) >1) 
//        {
//            $strYr=substr($strYr,1);
//        }
//    }//for
    $month=intval($strMonth);
    $day=intval($strDay);
    $year=intval($strYr);
    if (!$pos1 || !$pos2)
    {
        // echo "The format should be: yyyy-mm-dd ";
        return false;
    }
    if (strlen($strMonth)<1 || $month<1 || $month>12)
    {
        // echo "Please enter a valid month ";
        return false;
    }
    if (    strlen($strDay)<1 || $day<1 || $day>31 
        ||  ($month==2 && $day>daysInFebruary($year)) 
        ||  $day > $daysInMonth[$month])
    {
        // echo "Please enter a valid day ";
        return false;
    }
    if (strlen($strYear) != 4 || $year==0 || $year<$minYear || $year>$maxYear)
    {
        // echo "Please enter a valid 4 digit year between ".$minYear." and ".$maxYear." ";
        return false;
    }
    if (
        strpos($dtStr,$dtCh,$pos2+1)>$pos2 || 
        !isInteger(stripCharsInBag($dtStr, $dtCh))
        )
    {
        // echo "Please enter a valid date ";
        return false;
    }
    
    return true;
}
//isDate($dtStr)

//function ValidateForm()
//{
//    var dt=document.frmSample.txtDate
//    if (isDate(dt.value)==false)
//    {
//        dt.focus()
//        return false
//    }
//    return true
//}//ValidateForm()
?>
