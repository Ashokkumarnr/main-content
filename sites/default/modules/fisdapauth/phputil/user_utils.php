<?php
//-------------------------------------------------------------------------->
//--                                                                      -->
//--      Copyright (C) 1996-2005.  This is an unpublished work of        -->
//--                       Headwaters Software, Inc.                      -->
//--                          ALL RIGHTS RESERVED                         -->
//--      This program is a trade secret of Headwaters Software, Inc.     -->
//--      and it is not to be copied, distributed, reproduced, published, -->
//--      or adapted without prior authorization                          -->
//--      of Headwaters Software, Inc.                                    -->
//--                                                                      -->
//-------------------------------------------------------------------------->

require_once('phputil/classes/FISDAPDatabaseConnection.php');
require_once('phputil/classes/common_user.inc');


/**
 *
 */
function &get_remote_user() {
	$remote_user =& FISDAPUser::find_by_username(
			strtolower($_SERVER['PHP_AUTH_USER'])
			);

	if ( !$remote_user ) {
		die(
				'We could not fetch your user information.  Please try again.  '
				. 'If this problem persists, please contact FISDAP Support for '
				. 'assistance.  We apologize for the inconvenience.'
		   );
	}//if

	return $remote_user;
}//get_remote_user


/**
 * test whether or not the given string is a valid username
 * (usernames must be alphanumeric)
 * 
 * @param string the proposed username
 * @return bool true if the string is a valid username, false otherwise
 */
function check_username($username) {
	//make sure the username is alphanumeric
	$username_pattern = '/^[a-zA-Z0-9]+$/';
	return preg_match($username_pattern,$username);
}//check_username


/**
 * Returns true iff the given username exists in the database.
 */
function username_exists($username) {
	if ( !check_username($username) ) {
		return false;
	}//if

	$connection =& FISDAPDatabaseConnection::get_instance();
	$query = 'SELECT email from UserAuthData where email="' . $username . '"';
	$result = $connection->query($query);
	if ( is_array($result) && count($result) ) {
		return true;
	} else {
		return false;
	}//else
}//username_exists


/**
 * Return an associative array of information about the user
 * (the result of calling mysql_fetch_assoc() on the relevant
 * row)
 *
 * @param string the username of the user whose info we want
 * @return array an associative array of user info from the db
 * @depricated Don't use this any more! Use common_user.
 */
function get_user_info($username) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$user = new common_user($username);

	if ( !$user->is_valid() ) return false;

	//    //get the basic common info from UserAuthData
	//    $query = "SELECT * FROM UserAuthData ".
	//             "WHERE email='$username'";
	//    $result = mysql_query( $query, $dbConnect );
	//    if ( !$result || mysql_num_rows($result) != 1 )  return false;

	$user_info = $user->UserAuthData;

	//get the student- or instructor-specific data
	if ( $user->is_instructor() ) {
		//Instructors
		$query = "SELECT * FROM InstructorData WHERE UserName='$username'"; 
		$result = mysql_query( $query, $dbConnect );
		if ( !$result || mysql_num_rows($result) != 1 )  return false;
		$info_array = mysql_fetch_assoc($result);

		//insert all the info from the db into the result array, but
		//change the key 'ProgramId' to 'Program_id' for homogeneity
		foreach( $info_array as $key=>$val ) {
			if ( $key == 'ProgramId' ) 
				$user_info['Program_id'] = $val;
			else 
				$user_info[$key] = $val;
		}//foreach
	} else {
		//Students
		$query = "SELECT * FROM ".
			"StudentData ".
			"WHERE UserName='$username'";
		$result = mysql_query( $query, $dbConnect );
		if ( !$result || mysql_num_rows($result) != 1 ) {
			return false;
		}//if
		$info_array = mysql_fetch_assoc($result);

		//insert all the info from the db into the result array
		foreach( $info_array as $key => $val ) {
			$user_info[$key] = $val;
		}//foreach

		//insert account type and pda access info, too
		$student = new common_student($user->get_student_id());
		$user_info['AccountType'] = $student->get_account_type();
		if ($student->get_product_access('pda')) {
			$user_info['PDAAccess'] = 1;
		}
		else { $user_info['PDAAccess'] = 0; }
	}//else

	return $user_info;
}//get_user_info


/**
 * Determine if the given password is correct for the given user
 *
 * @param string the username whose password we're checking
 * @param string the password to test
 * @return bool true if the password matched, false otherwise
 */
function check_user_password($username, $password) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	if ( !check_username($username) ) return false;

	//get the stored hashed password

	$query = "SELECT epass FROM UserAuthData WHERE email='$username' and `enabled`=1";

	$result = mysql_query( $query, $dbConnect);
	if ( !$result || mysql_num_rows($result) != 1) return false;

	$row = mysql_fetch_assoc($result);
	$stored_epass = $row['epass'];

	//generate a hashed password from the proposed string
	$proposed_epass = crypt( $password, $stored_epass );

	//make sure the hashed strings match
	if( $proposed_epass != $stored_epass) return false;
	return true;
}//check_user_password


/**
 * Get a list of user IDs and names for the students that have not yet 
 * graduated from the given program.
 *
 * @param  int   the id of the program whose students we're interested in
 * @return array an array of arrays containing the student IDs, firstnames, 
 *               and lastnames of the current students 
 */
function get_current_students($program_id,$year,$month) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	if ( !is_numeric($program_id) || 
			!is_numeric($year) ||
			!is_numeric($month) ) return false;

	$query = "SELECT StudentData.*,AccountType FROM StudentData, SerialNumbers ".
		"WHERE StudentData.Student_id = SerialNumbers.Student_id ".
		"AND StudentData.Program_id='$program_id' ".
		"AND Class_Year>=$year ".
		"AND IF(Class_Year=$year,".
		"ClassMonth>=$month,".
		"ClassMonth>0) ".
		"ORDER BY lastname,firstname,username";
	$result = mysql_query($query,$dbConnect);
	if ( !$result ) return false;

	$students = array();
	while( $row = mysql_fetch_assoc($result) ) {
		$students[] = array($row['UserName'],
				$row['FirstName'],
				$row['LastName'],
				$row['Student_id'],
				$row['ClassMonth'],
				$row['Class_Year'],
				common_utilities::get_cert_shortdesc($row['AccountType']));
	}//while

	return $students;
}//get_current_students


/**
 * Get an array of arrays containing instructor IDs, firstnames, and lastnames
 * for all instructors in the given program.
 *
 * @param int    the id of the program to look in
 * @return array an array of arrays with usernames, firstnames, and 
 *               lastnames of all instructors in the program
 */
 function get_instructors($program_id) {
  	$connection =& FISDAPDatabaseConnection::get_instance();
  	$dbConnect = $connection->get_link_resource();
  
  	if ( !is_numeric($program_id) ) return false;
  
  	$query = "SELECT FirstName,LastName,UserName,Instructor_id FROM InstructorData ".
  		"WHERE ProgramId='$program_id' ".
  		"ORDER BY LastName,FirstName,UserName";
  	$result = mysql_query($query,$dbConnect);
  	if ( !$result ) return false; 
  
  	$instructors = array();
  	while ($row = mysql_fetch_assoc($result)) {
  		$instructors[] = array('username' => $row['UserName'],
 				'firstname' => $row['FirstName'],
  				'lastname' => $row['LastName'],
  				'id' => $row['Instructor_id']);
  	}//while
  
  	return $instructors;
 }//get_instructors

/**
 * Return student information for the first 20 students in 
 * the given program whose names are like the given search 
 * strings.
 *
 * @param string part of a student first name
 * @param string part of a student last name
 * @param int    the program whose students we'll search
 */
function student_search_by_name( $first_name, $last_name, $program_id ) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	if ( !is_numeric($program_id) ) return false;

	$first_name_like = ($first_name)? '%'.$first_name.'%' : '%'; 
	$last_name_like = ($last_name)? '%'.$last_name.'%' : '%'; 

	$query = "SELECT * FROM StudentData ".
		"WHERE FirstName LIKE '$first_name_like' ".
		"AND LastName LIKE '$last_name_like' ".
		"AND Program_id='$program_id' ".
		"ORDER BY LastName,FirstName ";
	$result = mysql_query($query,$dbConnect);
	if ( !$result ) return false;

	$students = array();

	while( $row = mysql_fetch_assoc($result) ) {
		$students[] = $row;
	}//while

	return $students;
}//student_search_by_name


/**
 * Return true iff the given student id exists in the database. 
 */
function student_exists($id) {
	if ( !is_numeric($id) ) {
		return false;
	}//if

	$connection =& FISDAPDatabaseConnection::get_instance();
	$query = 'SELECT Student_id '
		.    'FROM StudentData '
		.    'WHERE Student_id="' . $id . '"';
	$result = $connection->query($query);

	return is_array($result) && count($result);
}//student_exists


/**
 * Returns the numeric student id for the student with the given
 * username, if any.
 */
function get_student_id_by_username($username) {
	$connection_obj =& FISDAPDatabaseConnection::get_instance();
	$query = 'SELECT Student_id '.
		'FROM StudentData '.
		'WHERE UserName="'.$username.'"'; 
	$user_rows = $connection_obj->query($query);
	if ( !is_array($user_rows) || count($user_rows) != 1 ) {
		return false;
	}//if

	return $user_rows[0]['Student_id']; 
}//get_student_id_by_username


/**
 * 
 */
function get_student_info_by_username($username) {
	$connection_obj =& FISDAPDatabaseConnection::get_instance();

	$query="SELECT * FROM StudentData,UserAuthData WHERE `StudentData`.`UserName`=`UserAuthData`.`email` and UserName like '%$username%'";

	$user_rows = $connection_obj->query($query);
	if ( ! $user_rows ) {
		return false;
	} else {
		//$user_data=mysql_fetch_assoc($user_rows);
		return $user_rows;
	}
}//get_student_info_by_username

function get_student_info_by_id($student_id) {
	$connection_obj =& FISDAPDatabaseConnection::get_instance();

	$query="select * from StudentData,SerialNumbers where `StudentData`.`Student_id`=`SerialNumbers`.`Student_id` and `StudentData`.`Student_id`=$student_id";

	$user_rows = $connection_obj->query($query);
	if ( ! $user_rows ) {
		return false;
	} else {
		//$user_data=mysql_fetch_assoc($user_rows);
		return $user_rows; //hopefully only 1
	}
}//get_student_info_by_username

/**
 * Returns the username for the student with the given
 * student id, if any.
 */
function get_student_username_by_id($Student_id) {
	$connection_obj =& FISDAPDatabaseConnection::get_instance();
	$query = 'SELECT UserName '.
		'FROM StudentData '.
		'WHERE Student_id="'.$Student_id.'"'; 
	$user_rows = $connection_obj->query($query);
	if ( !is_array($user_rows) || count($user_rows) != 1 ) {
		return false;
	}//if

	return $user_rows[0]['UserName']; 
}//get_student_id_by_username


function get_notes($table_name,$table_id) {
	$connection_obj =& FISDAPDatabaseConnection::get_instance();
	$query="select * from `note_link`,`note` where `note_link_note_id`=`note_id` and `note_link_table_name`='$table_name' and `note_link_table_id`=$table_id";
	$note_rows = $connection_obj->query($query);
	if ( $note_rows == null ) {
		return false;
	}//if

	$note_id=$note_rows[0]['note_id'];
	$note_content=$note_rows[0]['note_content'];
	$return_array=array("note_id"=>$note_id, "note_content"=>$note_content);

	return $return_array;

}

function insert_note($table_name, $table_index, $table_id, $notes) {
	$connection_obj =& FISDAPDatabaseConnection::get_instance();
	$note_insert="insert into note(`note_id`,`note_created_on`,`note_content`) values (NULL,CURRENT_TIMESTAMP,'$notes')";
	$connection_obj->query($note_insert);
	$new_note_id=$connection_obj->get_last_insert_id();

	$note_link_insert="insert into note_link(`note_link_id`,`note_link_note_id`,`note_link_table_name`,`note_link_table_index`,`note_link_table_id`) values (NULL,'$new_note_id','$table_name','$table_index','$table_id')";
	$connection_obj->query($note_link_insert);
	$new_note_link_id=$connection_obj->get_last_insert_id();
	if ($new_note_link_id > 0) { return true; }
	else { return false; } //if the new note link does not have an id, there was a problem
}

function update_note($note_id, $content) {
	$connection_obj =& FISDAPDatabaseConnection::get_instance();
	$update_note="update `note` set `note_content`='$content' where `note_id`='$note_id' limit 1";
	$connection_obj->query($update_note);
}

function enable_student($username,$student_id) {
	$connection_obj =& FISDAPDatabaseConnection::get_instance();

	$query="select * from `StudentData` where `UserName`='$username'";
	$student_row = $connection_obj->query($query);
	$prog_id = (int) $student_row[0]['Program_id'];
	$prog_id = abs( $prog_id );

	$update1="update `StudentData` set `Program_id`='$prog_id' where `Student_id` =$student_id limit 1";
	$connection_obj->query($update1);

	$update2="update `UserAuthData` set `enabled`=1 where `email`='$username' limit 1";
	$connection_obj->query($update2);
	
}//*/

function disable_student($username,$student_id) {
	$connection_obj =& FISDAPDatabaseConnection::get_instance();
//echo "<br>user:$username<br>studentid: $student_id";
	$query="select * from `StudentData` where `UserName`='$username'";

	$student_row = $connection_obj->query($query);
	$prog_id = (int) $student_row[0]['Program_id'];
	if ($prog_id > 0) $prog_id =  "-".$prog_id;

	$update1="update `StudentData` set `Program_id`='$prog_id' where `Student_id` =$student_id limit 1";
	$connection_obj->query($update1);

	$update2="update `UserAuthData` set `enabled`=0 where `email`='$username' limit 1";
	$connection_obj->query($update2);
	
}//*/

/**
 * Returns true if the student with the given id is in the
 * current user's program, false is not.
 */
function student_in_users_program($Student_id) {
	$user = new common_user($REMOTE_USER);
	$student = new common_user(get_student_username_by_id($Student_id));

	return ( $user->program_id() == $student->program_id() );

} // end function student_in_users_program

/**
 * Returns the full name for the user with the given
 * user auth id, if any.
 */
function get_user_fullname_by_idx($idx) {
	$connection_obj =& FISDAPDatabaseConnection::get_instance();
	$query = 'SELECT firstname, lastname '.
			 'FROM UserAuthData '.
			 'WHERE idx="'.$idx.'"'; 
	$user_rows = $connection_obj->query($query);
	if ( !is_array($user_rows) || count($user_rows) != 1 ) {
		return false;
	}//if

	$fullname = $user_rows[0]['firstname']." ".$user_rows[0]['lastname'];
	return $fullname; 
}//get_user_fullname_by_idx

/*
 * Returns a program's customer ID based on a program ID.
 */
function get_customer_id($program_id) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$query = "SELECT CustumerId FROM ProgramData WHERE Program_id=$program_id";
	$result = $connection->query($query);
	if (count($result) == 1) {
		return $result[0]['CustumerId'];
	}
	else {
		return null;
	}
}

function check_textarea($textarea) {
	$validated=true;
	
	if (strpos($textarea,">") != false) { $validated=false; }
	else if (strpos($textarea,"<") != false) { $validated=false; }
	else if (strpos($textarea,"--") != false) { $validated=false; }
	else if (strpos($textarea,";") != false) { $validated=false; }

	return $validated;
}

/*
 * Given two student IDs, returns true if the first student is a
 * TA for a class section which includes the second student
 */
function is_TA($TA_Student_id, $Student_id) {
	$connection =& FISDAPDatabaseConnection::get_instance();
	$dbConnect = $connection->get_link_resource();

	$CS_query = "SELECT Section_id ".
				"FROM SectTAs ".
				"WHERE TA_Student_id=$TA_Student_id";
	$CS_result = $connection->query($CS_query);
	if (count($CS_result) > 0) {
		$numCS = count($CS_result);
		for ($i=0;$i<$numCS;$i++) {
			$tmp_Section_id = $CS_result[$i]['Section_id'];
			$student_query = "SELECT * FROM SectStudents ".
							 "WHERE Student_id=$Student_id ".
							 "AND Section_id=$tmp_Section_id";
			$student_result = $connection->query($student_query);
			if (count($student_result) >= 1) {
				return true;
			}
		}
		return false;
	}
	else {
		return false;
	}
}

?>
