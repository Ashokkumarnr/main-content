<?php


/**
 * Output message in pdf: userful when giving message instead of outputting pdf
 * and there is no browser window to output html to.
 */
function prn_pdf ($message, $title = 'ERROR') {
	$pdf_report = new common_report($title, '', '', '');
	$pdf_report->add (new common_textblock ($message, 'medium', 'center'));
	$pdf_report->display ('pdf');
}


function html_var_dump_simple($var, $return=false) {
	$str = str_replace("\n", "<br/>\n", var_export($var, true));
	if ($return) {
		return $str; 
	} else {
		echo $str;
	}
}

function html_var_dump($var, $return=false) {
	$list = explode("\n", var_export($var, true));

	$tabs=0;
	foreach ($list as $i => $val) {
		echo str_repeat("&nbsp; &nbsp; ", $tabs)." $val<br/ >\n";
		if ($val == 'array (' || $val == '  array (') {
			$tabs++;
		}

		if ($val == '),' || $val =='  ),' && $tabs>0) {
			$tabs--;
		}
	}
}

function prn($str) {
	echo "$str <br/ >\n";
}

function prnc($str) {
	echo "<center>$str </center><br/ >\n";
}

function prnh ($str) {
	echo "<br /><b>$str</b><br/ >\n";
}


function tf($b) {
	return($b)?'TRUE':'FALSE';
}




?>
