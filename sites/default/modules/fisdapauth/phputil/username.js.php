<?php
/****************************************************************************
*                                                                           *
*         Copyright (C) 1996-2007.  This is an unpublished work of          *
*                          Headwaters Software, Inc.                        *
*                             ALL RIGHTS RESERVED                           *
*         This program is a trade secret of Headwaters Software, Inc.       *
*         and it is not to be copied, distributed, reproduced, published,   *
*         or adapted without prior authorization                            *
*         of Headwaters Software, Inc.                                      *
*                                                                           *
****************************************************************************/
?>

/**** checkUserName
 *
 * This function will make sure that the user name is of the correct form
 */
 
    function checkUserName( UserStr_element)
     {  
         var UserNameLength = UserStr_element.value.length;
         
         if( UserNameLength < 3 || UserNameLength > 20)
         {  UserStr_element.value = "";
             alert ("FISDAP user names must be between 3 and 20 characters long.\nPlease choose another user name.");
             return -1;
         }
          
         if( UserStr_element.value.indexOf("\"") >= 0)
         {   UserStr_element.value = "";
	            alert( "You cannot use double quotes ( \" ) in your user name.\nPlease choose another user name.");
              return -2;
         }

         if( UserStr_element.value.indexOf("&") >= 0
          || UserStr_element.value.indexOf(".") >= 0
          || UserStr_element.value.indexOf("~") >= 0
          || UserStr_element.value.indexOf("!") >= 0
          || UserStr_element.value.indexOf("@") >= 0
          || UserStr_element.value.indexOf("#") >= 0
          || UserStr_element.value.indexOf("$") >= 0
          || UserStr_element.value.indexOf("%") >= 0
          || UserStr_element.value.indexOf("^") >= 0
          || UserStr_element.value.indexOf("_") >= 0
          || UserStr_element.value.indexOf("*") >= 0
          || UserStr_element.value.indexOf("?") >= 0
          || UserStr_element.value.indexOf("'") >= 0
          || UserStr_element.value.indexOf(",") >= 0
          || UserStr_element.value.indexOf("(") >= 0
          || UserStr_element.value.indexOf(")") >= 0
          || UserStr_element.value.indexOf("+") >= 0
          || UserStr_element.value.indexOf("=") >= 0
          || UserStr_element.value.indexOf(" ") >= 0 
          || UserStr_element.value.indexOf("-") >= 0 
          || UserStr_element.value.indexOf(":") >= 0 
          || UserStr_element.value.indexOf("|") >= 0   
          || UserStr_element.value.indexOf("/") >= 0
          || UserStr_element.value.indexOf("\"") >= 0
          || UserStr_element.value.indexOf("\\") >= 0
          || UserStr_element.value.indexOf("<") >= 0
          || UserStr_element.value.indexOf(">") >= 0
          || UserStr_element.value.indexOf("`") >= 0   )
         {  UserStr_element.value = ""; 
	            alert( "You can only use letters and numbers in your user name.\nPlease choose another user name.");
              return -2;    
         }
     }
