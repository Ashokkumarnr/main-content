<?php

//require_once('dbconnect.html');//uncomment to do testing

if ( !defined('FISDAP_ROOT') ) die('DbObj.php: required phputil/dbconnect.html not loaded');
require_once(FISDAP_ROOT.'phputil/table_desc.php');

/**
 * This class is a simple wrapper for common database
 * operations.  Simple table joins are accomplished by
 * passing table names and column mappings into the 
 * constructor, or as arguments to the join_to method.
 * Database columns are retrieved through the get_field
 * method, using the form TABLE.'_'.COLUMN for joined
 * tables (to avoid name collisions between table columns).
 *
 * @todo: make the joins a single parameter (an assoc 
 *        array containing an assoc array), so that 
 *        adding a remove_join method will be easier,
 *        and to make the constructor a little clearer.
 */
class DbObj {
    var $db_result; // the result from mysql_fetch_assoc()
    var $assoc_table; // the table to read from
    var $assoc_id_column; // the id column in the table
    var $assoc_id; // this element's id
    var $dbConnect; // the database connection to use
    var $query; // the query used to get the object from the database
    var $has_changed; // true if the object has changed since the last read() 
    var $joined_tables; // table names to join with
    var $join_mapping; // mapping of matching columns 
                       // ( e.g., array("Shifts.student_id"=>"Students.id") )
    var $insert_query;

    /**
     * Create a new DbObj mapping to the database row described by the given 
     * information
     */
    function DbObj($table,$id_column,$id,$dbConnect,
                   $joined_tables=NULL,$mapping=NULL) {
        $this->assoc_table = $table;
        $this->assoc_id_column = $id_column;
        $this->assoc_id = $id;
        $this->dbConnect = $dbConnect;
        $this->joined_tables = $joined_tables;
        $this->join_mapping = $mapping;

        $this->recreate_select_statement();
        //$this->generate_insert_statement();

        //die('query is: "'.$this->query."\"\n");

        $this->read();
    }//DbObj


    /**
     * Join this object with the given table, using the given 
     * mapping (e.g., "ProgramData.Program_id"=>"StudentData.Program_id").
     * This exposes the contents of the relevant column from the joined 
     * table to this object's get_field() method
     *
     * @param  string the name of the table to join to 
     * @param  array  an assoc array of key/value pairs naming matching columns
     * @return a boolean success value
     */
    function join_to($table,$mapping) {
        //if we haven't already joined with any tables, create the array
        if ( !$this->joined_tables ) 
            $this->joined_tables = array();

        //ditto for the mapping array
        if ( !$this->join_mapping )
            $this->join_mapping = array();

        //add the table to the array of joins, and add the corresponding map
        $this->joined_tables[] = $table;
        array_merge($this->join_mapping,$mapping);

        //incorporate the newly joined tables into the select query
        $this->recreate_select_statement();   
    }//join_to

    /**
     * Recreate the select query to get the object from the db.  This 
     * is used to incorporate new joins into the object's result field.
     */
    function recreate_select_statement() {
        $column_clause = $this->assoc_table . ".*";
        $from_clause = $this->assoc_table;
        $where_clause = $this->assoc_id_column . "='" . $this->assoc_id . "'";

        //loop over the tables, adding all their columns to our select statement,
        //aliased as TABLE.'_'.COLUMN
        if ( $this->joined_tables && $this->join_mapping ) {
            foreach( $this->joined_tables as $joined_table ) {
                $table_desc = table_desc($joined_table);
                if( $table_desc && is_array($table_desc) ) { 
                    foreach( $table_desc as $desc_row ) {
                        $column_clause .= ', '.$joined_table.'.'.$desc_row['Field'].
                                          ' as '.$joined_table.'_'.$desc_row['Field'];
                    }//foreach
                }//if

                $from_clause .= ", $joined_table";
            }//foreach

            foreach( $this->join_mapping as $col_1=>$col_2 ) {
                $where_clause .= " AND $col_1=$col_2";
            }//foreach
        }//if 

        $this->query = "SELECT $column_clause ".
                       "FROM $from_clause " .
                       "WHERE $where_clause";

        $this->has_changed = true;
    }//recreate_select_statement


    /**
     * Populate the object's $db_result array from the database,
     * returning false on error or when the number of rows returned
     * was more than 1.
     */
    function read() {
        //echo "DbObj->read(): query = ".$this->query."<br>";
        $result = mysql_query($this->query,$this->dbConnect);
        if ( !$result || mysql_num_rows($result)!=1 ) return false;
        $this->db_result = mysql_fetch_assoc($result);
        return true;
    }//read

    /**
     * Given a column name (e.g., "Program_id" or "Student_id"),
     * return the column contents.
     *
     * @param  string the name of a db column  
     * @return string the value for the given column (if any)
     */
    function get_field($column_name) {
        if ( !$this->has_changed ) {
            //get the database result first, then look at the requested column
            if ( !$this->read() ) 
                die('DbObj.get_field(): database lookup failed');
        }//if
        return $this->db_result[$column_name];
    }//get_field
}//class DbObj


//testing

//$foo = new DbObj('ShiftData',
//                 'Shift_id',
//                 820020,
//                 $dbConnect,
//                 array("StudentData","ProgramData"),
//                 array("StudentData.Student_id"=>"ShiftData.Student_id",
//                       "StudentData.Program_id"=>"ProgramData.Program_id"));
//
//echo "DbObj foo has Shift_id = " . $foo->get_field('Shift_id') . "\n";
//echo "DbObj foo has Student Program_id = " . $foo->get_field('StudentData_Program_id') . "\n";
//echo "DbObj foo has Student Program name = " . $foo->get_field('ProgramData_ProgramName') . "\n";
//
?>
