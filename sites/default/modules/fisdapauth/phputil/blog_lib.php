<?php
require_once('phputil/classes/FisdapLogger.inc');
require_once('phputil/string_utils.php');

function print_post($post)
{
	static $extensions_to_mime_types = array(
		'mp3' => 'audio/mpeg'
	);

    echo "<div class='preceptext'>\n";
    echo "<div class='largetext preceptext'>\n";
    echo "<div style='float:left;margin-right:10px'>\n";
	if($post['post_picture_url']!='')
	{
		echo "<img src='".$post['post_picture_url']."' height=94>\n";
	}
	else
	{
	    echo "<img src='../images/Listening.png'>\n";
    	}
	echo "</div>\n";    echo "<div style='padding:10px'>".$post['post_title'];
	echo "<span class='smalltext'>&nbsp;&nbsp;<a href='".$post['file_url']."'>Download</a></span>\n";  
    echo "<br><span class='smalltext'>\n";
    echo " posted: " . $post['post_date'];
    echo " by: " . get_user_fullname_by_idx($post['blog_poster_id']);
    echo "</span></div>\n";
    echo "\n</div>\n";
    echo "<div class='mediumtext preceptext'>\n";
    echo $post['post_text'];    
    echo "<div class='spacer_row'></div><br>\n";
    echo "<div class='preceptext' id='".$post['post_id']."_div'>\n";

	$url = $post['file_url'];
	$version = '';

	$extension = '???';
	$mime_type = null;
	$path = parse_url($url, PHP_URL_PATH);

	if (is_string($path)) {
		$extension = strtolower(pathinfo($url, PATHINFO_EXTENSION));

		if (isset($extensions_to_mime_types[$extension])) {
			$mime_type = $extensions_to_mime_types[$extension];
		}
	}

	// Set up the associative parameter list.
	$params = array(
		'autoplay' => 'true',
		'controller' => 'true',
		'loop' => 'false'
	);

	$post_id = $post['post_id'];
	if (is_null($mime_type)) {
		FisdapLogger::get_logger()->warn("Blog post ID[$post_id] extension[$extension] has no" .
			' corresponding MIME type');
	}
	else {
		$params['type'] = $mime_type;
	}

	$items = array();
	foreach ($params as $name=>$value) {
		$items[] = '"' . $name . '","' . $value . '"';
	}

	$params_string = join(',', $items);
	$post_id = $post['post_id'];

	echo <<<EOI
<script language="javascript">
var text_$post_id = QT_ReturnOBJECT("$url","320","20","$version",$params_string);
</script>
<input type="button" name="listen" value="Listen Now!" 
	onClick="replace_text('${post_id}_div', text_$post_id);">
EOI;

    if($post['url_file_duration']!='')
    {
        echo " <span class='smalltext'>( ".$post['url_file_duration']." )</span>";
    }
    echo "</div>\n";
    echo "</div>\n";
    echo "<p>\n";
    echo "<p>\n";
    echo "</div>\n";
}

?>
