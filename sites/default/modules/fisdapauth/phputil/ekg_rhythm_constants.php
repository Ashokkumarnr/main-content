<?php

/**
 * This file constants for the IDs of the various EKG rhythm types stored in
 * the database
 */

define('EKG_NORMAL_SINUS_RHYTHM'        , '25'); 
define('EKG_SINUS_WITH_PVCS'            ,  '1'); 
define('EKG_VENTRICULAR_FIBRILLATION'   ,  '2'); 
define('EKG_V_TACH_WITH_PULSE'          ,  '3'); 
define('EKG_V_TACH_NO_PULSE'            ,  '4'); 
define('EKG_FIRST_DEGREE_BLOCK'         ,  '5'); 
define('EKG_SECOND_DEGREE_BLOCK_1'      ,  '6'); 
define('EKG_SECOND_DEGREE_BLOCK_2'      ,  '7'); 
define('EKG_THIRD_DEGREE_BLOCK'         ,  '8'); 
define('EKG_JUNCTIONAL_RHYTHM'          ,  '9'); 
define('EKG_JUNCTIONAL_TACHYCARDIA'     , '10'); 
define('EKG_PACED_RHYTHM'               , '11'); 
define('EKG_ATRIAL_FIBRILLATION'        , '12'); 
define('EKG_ATRIAL TACHYCARDIA'         , '13'); 
define('EKG_SVT'                        , '14'); 
define('EKG_SINUS_BRADYCARDIA'          , '15'); 
define('EKG_SINUS_PAUSE'                , '16'); 
define('EKG_SINUS_WITH_PACS'            , '17'); 
define('EKG_SINUS_WITH_PACS_AND_PVCS'   , '18'); 
define('EKG_ASYSTOLE'                   , '19'); 
define('EKG_VENTRICULAR_STANDSTILL'     , '20'); 
define('EKG_IDIOVENTRICULAR'            , '21'); 
define('EKG_WANDERING_ATRIAL_PACEMAKER' , '22'); 
define('EKG_SINUS_TACHYCARDIA'          , '23'); 
define('EKG_ATRIAL_FLUTTER'             , '24'); 

?>
