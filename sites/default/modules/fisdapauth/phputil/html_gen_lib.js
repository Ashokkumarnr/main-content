/**
 * This file contains convenience functions for generating
 * HTML DOM elements and adding them to documents.
 *
 * Note: if including prototype.js, make sure to do so
 * before including this file, since the prototype library
 * will try to overwrite the $() function otherwise (with
 * nasty errors)
 */


/**
 * Add a div of class 'spacerRow' to the given element.
 * This assumes that there is a CSS declaration giving 
 * the clear:both property to all divs of class spacerRow.
 */
function html_add_spacer_row(element) {
    var spacerRow = document.createElement('div');
    spacerRow.className = 'spacerRow';
    element.appendChild(spacerRow);
    return spacerRow;
}//html_add_spacer_row

/**
 * add a text node to the given element
 */
function html_add_text(text,element) {
    var text_node = document.createTextNode(text);
    element.appendChild(text_node);
    return text_node;
}//html_add_text


/**
 * add an arbitrary node (with optional attributes) to the given element
 * (note that attrs is an array of arrays, where the subarrays are of type
 * [attribute,value])
 */
function html_add_element(type,element,attrs) {
    var new_element = document.createElement(type);
    if ( attrs ) {
        var i=0;
        for( i=0; i < attrs.length; i++ ) {
            try {
                var attr = attrs[i][0];
                var val = attrs[i][1];
                new_element[attr] = val;
                //new_element.setAttribute(attrs[i][0],attrs[i][1]);
            } catch( e ) { }
        }//for
    }//if
    element.appendChild(new_element);
    return new_element;
}//html_add_element


/**
 * insert an arbitrary node (with optional attributes) into the given element
 */
function html_insert_element(type,element,attrs) {
    var new_element = document.createElement(type);
    if ( attrs ) {
        var i=0;
        for( i=0; i < attrs.length; i++ ) {
            try {
                var attr = attrs[i][0];
                var val = attrs[i][1];
                new_element[attr] = val;
                //new_element.setAttribute(attrs[i][0],attrs[i][1]);
            } catch( e ) { }
        }//for
    }//if
    element.parentNode.insertBefore(new_element,element);
    return new_element;
}//html_insert_element

/**
 * Add the contents of the given array to the given table,
 * assuming each element of the array to be a new table 
 * cell.
 */
function add_table_row(table,row_array) {
    var i = 0; 
    var table_element = $(table);
    var tr = html_add_element('tr',table_element);
    for( i=0; i < row_array.length ; i++ ) {
        var new_td = html_add_element('td',tr);
        html_add_text(row_array[i],new_td);
    }//for
    return tr;
}//add_table_row

/**
 * Remove all the children of the given element
 */
function remove_all_children(elt) {
    var elt_node = $(elt);
    while ( elt_node.childNodes.length > 0 ) {
        elt_node.removeChild(elt_node.firstChild);
    }//while
}//remove_all_children


//define the $() shortcut if not already present
//@todo make sure this construct actually works
if ( typeof $ == 'undefined' ) {
    $ = function( elt_id ) {
            return document.getElementById(elt_id);
    };
}//if


/**
 * Set the value of the radio button group whose first element has the given id
 *
 * Note that elements 1 through n (with the first element being element 0) 
 * should have IDs element_id+1 through element_id+n
 */
function set_radio_group_value( first_element_id, value ) {
    var elt = $(first_element_id);
    if ( elt.value == value ) { 
        elt.checked = true;
        return true;
    }//if

    var i = 1;
    for( i=1; $(first_element_id+'x'+i) ; i++) {
        var elt = $(first_element_id+'x'+i);
        if ( elt.value == value ) {
            elt.checked = true;
            return true;
        }//if
    }//for

    //@todo throw an exception if we get here, rather than using the NULL value
    return false; 
}//set_radio_group_value


/**
 * Get the value of the radio button group whose elements have the given name
 */
function get_radio_group_value( group_name ) {
    var radio_group = document.getElementsByName( group_name );
    var i = 0;
    for( i=0 ; i < radio_group.length ; i++ ) {
        var elt = radio_group[i];
        if ( elt.checked ) {
            return elt.value;
        }//if
    }//for

    //@todo throw an exception if we get here, rather than using the NULL value
    return null; 
}//get_radio_group_value

/**
 * Given the id of a select box, return the text for the option
 * with the given value
 *
 * If the given value is not found, return false
 * @todo: we should probably throw an exception instead of returning
 *        false when this fails
 */
function get_select_text_for_value( element_id, val ) {
    var i = 0;
    var select_elt = $(element_id);
    if ( select_elt ) {
        var select_opts = select_elt.options;
        var select_len = select_opts.length;
        for( i = 0 ; i < select_len ; i++ ) {
            var opt = select_opts[i];
            if ( opt.value == val ) {
                return opt.text;
            }//if
        }//for
    }//if
    return false;
}//get_select_text_for_value

/**
 * Get the value of the given form element (or group
 * of elements, as in the case of radio buttons).
 *
 * @warning: pay attention to the checkbox handling... it's a little confusing 

 * @param string  element_id            the html id of the element whose value we want
 */
function get_form_element_value( element_id ) {
    var elt = $(element_id);

    if ( !elt ) {
        return null;
    }//if

    //the value of a checkbox in this context is whether or not it 
    //is checked (0 = unchecked, 1 = checked)
    if ( elt.type && elt.type == 'checkbox' ) {
        return (elt.checked) ? 1 : 0;
    }//if

    //@todo: figure out why this is necessary with avantgo
    //this *shouldn't* be necessary, but avantgo is WEIRD when it comes 
    //to creating javascript objects from the source HTML
    if ( elt.type && elt.type == 'select-one' ) {
        if ( elt.selectedIndex < 0 ) {
            elt.selectedIndex = 0;
        }//if
        return elt.options[elt.selectedIndex].value;
    }//if

    //return the value attr unless it's a radio button, in which case we need
    //to figure out which one is checked and return that element's value
    return (elt.type && elt.type=='radio') ? get_radio_group_value(element_id):
                                             elt.value;
}//get_form_element_value

/**
 * Set the value of the given form element (or group
 * of elements, as in the case of radio buttons).
 *
 * @todo: make this throw exceptions on error
 */
function set_form_element_value( element_id, value ) {
    print_debug_info('setting '+element_id+' to '+value);
    var elt = $(element_id);

    if ( !elt ) return false;

    //the value of a checkbox in this context is whether or not it 
    //is checked (0 = unchecked, 1 = checked)
    if ( elt.type && elt.type == 'checkbox' ) {
        // If the value parses as an int, use the int value (since 
        // the string "0" evaluates to true)
        var value_as_int = parseInt(value);
        if ( !isNaN(value_as_int) ) {
            value = value_as_int;
        }//if

        // Convert possibly non-boolean expressions into explicit booleans
        // when setting the checked property, just to be safe.
        elt.checked = (value) ? true:false; 
        return true;
    }//if

    if ( elt.type && elt.type == 'select-one' ) {
        print_debug_info( element_id+' is a select-one' );
        //loop over the select box to find the option with the right value
        var i = 0;
        for( i=0 ; i < elt.options.length ; i++ ) {
            if ( elt.options[i].value == value ) {
                print_debug_info(element_id+' => "'+value+'", index "'+i+'"');
                elt.selectedIndex = i;
                return true;
            }//if
        }//for
        print_debug_info(element_id + ': val not found');
        print_debug_info(element_id + ': setting index to 0');
        elt.selectedIndex = 0;
    }//if

    //return the value attr unless it's a radio button, in which case we need
    //to figure out which one is checked and return that element's value
    if (elt.type && elt.type=='radio') {
        set_radio_group_value(element_id,value);
    } else {
        elt.value=value;
    }//else

    return true;
}//set_form_element_value

/**
 * Test for the existence of the debug area
 */
function have_debug_area() {
   return ( $('debugLoggingArea') ) ? true : false;
}//have_debug_area

/**
 * Add a div to the given element that we can print debugging information to
 */ 
function create_log_area(parent_elt) {
    if ( !have_debug_area() ) {
        var log_container = html_add_element(
            'div',
            parent_elt,
            [['style','border:1px solid red;margin-left:3px;margin-right:3px;width:auto;margin-top:1em;']]);
        return html_add_element(
            'p',
            log_container,
            [['id','debugLoggingArea'],
            ['style','font-size:70%']]);
    }//if
}//create_log_area

/**
 * Add a line to the debugging log 
 */
function print_debug_info( line ) {
    if ( have_debug_area() ) {
        var debug_area = $('debugLoggingArea');
        html_add_text(line,debug_area);
        html_add_element('br',debug_area);
    }//if
}//print_debug_info


/**
 * This class can be used to create a simple timer that counts
 * the number of seconds between being started and stopped.
 */
function Timer() {
    this.start_time = false;
    this.end_time = false;
}

Timer.prototype.start = function() {
    this.start_time = new Date().getTime(); 
    return this.start_time;
};

Timer.prototype.end = function() {
    if ( this.start_time ) {
        this.end_time = new Date().getTime();
        return this.end_time;
    }//if
    return false;
};

Timer.prototype.get_seconds_elapsed = function() {
    if ( this.start_time && this.end_time ) {
        return (this.end_time - this.start_time)/1000;
    } else {
        return false;
    }//else
};
