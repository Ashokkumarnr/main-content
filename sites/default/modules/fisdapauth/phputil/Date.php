<?php
//-------------------------------------------------------------------------->
//--                                                                      -->
//--      Copyright (C) 1996-2005.  This is an unpublished work of        -->
//--                       Headwaters Software, Inc.                      -->
//--                          ALL RIGHTS RESERVED                         -->
//--      This program is a trade secret of Headwaters Software, Inc.     -->
//--      and it is not to be copied, distributed, reproduced, published, -->
//--      or adapted without prior authorization                          -->
//--      of Headwaters Software, Inc.                                    -->
//--                                                                      -->
//-------------------------------------------------------------------------->

/**
 * This class is intended to make time/date operations easier.
 *
 * @todo: add support for timezone offsets ( ? )
 * @deprecated We're now using phputil/classes/DateTime.inc
 * @ignore
 */ 
class Date {
    var $timestamp;

    /**
     * Create a Date from the given date/time, or use the current date
     * if there are no args
     */
    function Date($h=NULL,$m=NULL,$s=NULL,$y=NULL,$m=NULL,$d=NULL) {
        $this->timestamp = ($h) ? mktime($h,$m,$s,$y,$m,$d) : time();
    }//Date

    /**
     * Return the date formatted as a string of the form "Y-m-d H:i:s",
     * or formatted according to the given string, if any
     */
    function to_s($fmt=false) {
        return date(($fmt)?$fmt:"Y-m-d H:i:s",$this->timestamp);
    }//to_s

    /**
     * Returns the year.
     */
    function year() {
        return date('Y',$this->timestamp);
    }//year

    /**
     * Returns the month.
     */
    function month() {
        return date('m',$this->timestamp);
    }//month

    /**
     * Returns the day.
     */
    function day() {
        return date('d',$this->timestamp);
    }//day

    /**
     * Returns the hour.
     */
    function hour() {
        return date('H',$this->timestamp);
    }//hour

    /**
     * Returns the minute
     */
    function minute() {
        return date('i',$this->timestamp);
    }//minute

    /**
     * Returns the second
     */
    function second() {
        return date('s',$this->timestamp);
    }//second

    /**
     * Change the year by the given amount
     */
    function change_year($offset) {
        $this->timestamp = mktime($this->hour(),
                                   $this->minute(),
                                   $this->second(),
                                   ($this->year())+$offset,
                                   $this->month(),
                                   $this->day());
    }//change_year

    /**
     * Change the month by the given amount
     */
    function change_month($offset) {
        $this->timestamp = mktime($this->hour(),
                                   $this->minute(),
                                   $this->second(),
                                   $this->year(),
                                   ($this->month())+$offset,
                                   $this->day());
    }//change_month

    /**
     * Change the day by the given amount
     */
    function change_day($offset) {
        $this->timestamp = mktime($this->hour(),
                                   $this->minute(),
                                   $this->second(),
                                   $this->year(),
                                   $this->month(),
                                   ($this->day())+$offset);
    }//change_day

    /**
     * Change the hour by the given amount
     */
    function change_hour($offset) {
        $this->timestamp = mktime(($this->hour())+$offset,
                                   $this->minute(),
                                   $this->second(),
                                   $this->year(),
                                   $this->month(),
                                   $this->day());
    }//change_hour

    /**
     * Change the minute by the given amount
     */
    function change_minute($offset) {
        $this->timestamp = mktime($this->hour(),
                                   ($this->minute())+$offset,
                                   $this->second(),
                                   $this->year(),
                                   $this->month(),
                                   $this->day());
    }//change_minute

    /**
     * Change the second by the given amount
     */
    function change_second($offset) {
        $this->timestamp = mktime($this->hour(),
                                   $this->minute(),
                                   ($this->second())+$offset,
                                   $this->year(),
                                   $this->month(),
                                   $this->day());
    }//change_second

    /**
     * Returns a mysql-friendly date string
     */
    function mysql_date() {
    }//mysql_date

    /**
     * Returns a mysql-friendly datetime string
     */
    function mysql_datetime() {
    }//mysql_datetime
}//class Date
?>
